<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@home');

Route::prefix('{guard}')->name('guard.')->group(function () {
    // Auth::routes(['verify' => false]);
    Auth::routes();
    Route::get('/', 'ResourceController@home');
    Route::get('login/{provider}', 'Auth\SocialAuthController@redirectToProvider');

//     Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
// Route::get('email/verify/{id}/{hash}', 'Auth\VerificationController@verify')->name('verification.verify');
// Route::post('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
});
// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
