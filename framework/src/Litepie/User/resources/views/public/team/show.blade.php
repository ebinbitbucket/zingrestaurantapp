<div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2>{{__('Show')}} {!!$team->name!!}</h2>
        <div class="actions">
            <button type="button" class="btn btn-with-icon btn-link app-edit" data-id="{!!$team->getRouteKey()!!}"><i class="las la-save"></i>{{__('Edit')}}</button>
            <button type="button" class="btn btn-with-icon btn-link app-delete"  data-id="{!!$team->getRouteKey()!!}"><i class="las la-trash"></i>{{__('Delete')}}</button>
        </div>
    </div>
    {!!Form::vertical_open()
    ->id('app-form-show')
    ->class('app-form-show')
    ->method('PUT')
    ->action(guard_url('user/team/'. $team->getRouteKey()))!!}

    @include('user::team.partial.entry', ['mode' => 'show'])

    {!!Form::close()!!}
</div>
