<div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2>{{__('Edit')}} Sub Menu</h2>
        <div class="actions">
            <button type="button" class="btn btn-with-icon btn-link app-update" data-action='UPDATE' data-form='#edit-menu' data-load-to='#menu-entry' data-href='{!!guard_url('menu/submenu')!!}/{!!$menu->getRouteKey()!!}' id="btn-save"><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
            <button type="button" class="btn btn-with-icon btn-link app-cancel"data-action='CANCEL' data-load-to='#menu-entry' data-href='{!!guard_url('menu/submenu')!!}/{!!$menu->getRouteKey()!!}' id="btn-cancel"><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>
            
        </div>
    </div>

    {!!Form::vertical_open()
    ->method('PUT')
    ->id('app-form-edit')
    ->class('app-form-edit')
    ->enctype('multipart/form-menu')
    ->action(guard_url('menu/submenu/'. $menu->getRouteKey()))!!}
    {!!Form::token()!!}
    {!!Form::hidden('upload_folder')!!}
    
    @include('menu::admin.partial.submenu', ['mode' => 'edit'])

    {!!Form::close()!!}
</div>

    
