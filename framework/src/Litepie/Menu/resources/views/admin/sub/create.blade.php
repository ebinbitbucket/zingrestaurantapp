<div class="app-entry-form-wrap">
        <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
            <i class="lab la-product-hunt app-sec-title-icon"></i>
            <h2>{{__('Create')}} {{ trans('menu::menu.name') }}</h2>
            <div class="actions">
                <button type="button" class="btn btn-with-icon btn-link app-store"  data-action='CREATE' data-form='#create-menu' data-load-to='#menu-entry' data-href='{!!guard_url('menu/submenu')!!}'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-with-icon btn-link app-cancel"  data-action='CANCEL' data-load-to='#menu-entry' data-href='{!!guard_url('menu/submenu/0')!!}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>
            </div>
        </div>
        {!!Form::vertical_open()
        ->id('app-form-create')
        ->class('app-form-create')
        ->method('POST')
        ->files('true')
        ->action(guard_url('menu/submenu'))!!}
        {!!Form::token()!!}
        @include('menu::admin.partial.submenu', ['mode' => 'create'])

        {!! Form::close() !!}
    </div>
    <!-- <div class="nav-tabs-custom">
       
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">Menu</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#create-menu' data-load-to='#menu-entry' data-href='{!!guard_url('menu/submenu')!!}'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#menu-entry' data-href='{!!guard_url('menu/submenu/0')!!}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('create-menu')
        ->method('POST')
        ->action(guard_url('menu/submenu'))!!}
        {!! Form::token() !!}
            <div class="tab-content">
                @include('menu::admin.partial.submenu')
            </div>
        {!! Form::close() !!}
    </div> -->