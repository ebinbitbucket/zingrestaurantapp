@if($menu->key==null)
@include('menu::new')
@else
<div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2>{{__('Show')}} {!!$menu->name!!}</h2>
        <div class="actions">
            <button type="button" class="btn btn-success btn-sm app-submenu-create" data_val="{{$menu->id}}" data-action="NEW" data-load-to='#menu-entry'
                data-href="{{guard_url('menu/submenu/create?parent_id='.@$menu->getRouteKey())}}"><i
                    class="fa fa-plus-circle"></i> Sub Menu</button>
            @if($menu->id)
            <button type="button" class="btn btn-with-icon btn-link app-edit" data-id="{!!$menu->getRouteKey()!!}"><i
                    class="las la-save"></i>{{__('Edit')}}</button>
            <button type="button" class="btn btn-with-icon btn-link app-delete" data-id="{!!$menu->getRouteKey()!!}"><i
                    class="las la-trash"></i>{{__('Delete')}}</button>
            @endif
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                {!!Form::vertical_open()
                ->id('app-form-show')
                ->class('app-form-show')
                ->method('PUT')
                ->action(guard_url('menu/menu/'. $menu->getRouteKey()))!!}

                @include('menu::admin.partial.menu', ['mode' => 'show'])

                {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
@endif
<script>
$(document).ready(function() {
    $(".app-submenu-create").click(function(e) {
            e.preventDefault();   
            var id = $(this).attr('data_val');
        $("#app-content").load('{{guard_url('menu/submenu/create?parent_id=')}}'+id);
    });
});
</script>