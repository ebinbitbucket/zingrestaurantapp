<div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2>{{__('Edit')}} {!!$menu->name!!}</h2>
        <div class="actions">
            <button type="button" class="btn btn-success btn-sm app-submenu-create" data_val="{{$menu->id}}" data-action="NEW" data-load-to='#menu-entry'
                data-href="{{guard_url('menu/submenu/create?parent_id='.@$menu->getRouteKey())}}"><i
                    class="fa fa-plus-circle"></i> Sub Menu</button>
            <button type="button" class="btn btn-with-icon btn-link app-update" data-id="{!!$menu->getRouteKey()!!}"><i
                    class="las la-save"></i>{{__('Save')}}</button>
            <button type="button" class="btn btn-with-icon btn-link app-cancel" data-id="{!!$menu->getRouteKey()!!}"><i
                    class="las la-window-close"></i>{{__('Cancel')}}</button>
        </div>
    </div>

    {!!Form::vertical_open()
    ->method('PUT')
    ->id('app-form-edit')
    ->class('app-form-edit')
    ->enctype('multipart/form-menu')
    ->action(guard_url('menu/menu/'. $menu->getRouteKey()))!!}
    {!!Form::token()!!}
    {!!Form::hidden('upload_folder')!!}
    
    @include('menu::admin.partial.menu', ['mode' => 'edit'])

    {!!Form::close()!!}
</div>
<script>
$(document).ready(function() {
    $(".app-submenu-create").click(function(e) {
            e.preventDefault();   
            var id = $(this).attr('data_val');
        $("#app-content").load('{{guard_url('menu/submenu/create?parent_id=')}}'+id);
    });
});
</script>