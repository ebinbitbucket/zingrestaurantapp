
    <div class="app-entry-form-wrap">
        <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
            <i class="lab la-product-hunt app-sec-title-icon"></i>
            <h2>{{__('Create')}} {{ trans('menu::menu.name') }}</h2>
            <div class="actions">
                <button type="button" class="btn btn-with-icon btn-link app-store" data-id="{!!$menu->getRouteKey()!!}"><i
                        class="las la-save"></i>{{__('Create')}}</button>
                <button type="button" class="btn btn-with-icon btn-link app-cancel" data-id="{!!$menu->getRouteKey()!!}"><i
                class="las la-window-close"></i>{{__('Cancel')}}</button>
            </div>
        </div>
        {!!Form::vertical_open()
        ->id('app-form-create')
        ->class('app-form-create')
        ->method('POST')
        ->files('true')
        ->action(guard_url('menu/menu'))!!}
        {!!Form::token()!!}
        @include('menu::admin.partial.menu', ['mode' => 'create'])

        {!! Form::close() !!}
    </div>
