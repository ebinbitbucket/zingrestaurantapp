<div class="dd nestable-menu" id="nestable">
    <ol class="dd-list">
    @foreach ($menus as $menu)
        @if ($children = $menu->getChildren())
        <li class="dd-item dd3-item" data-id="{!!$menu->getRouteKey()!!}">
            <div class="dd-handle dd3-handle"><i class="las la-arrows-alt"></i></div>
            <div class="dd3-content">
                <a href="#" class="app-submenu-edit" data-action="LOAD" data-load-to='#menu-entry' data_val="{!!$menu->getRouteKey()!!}" data-href='{{guard_url('menu/submenu')}}/{!!$menu->getRouteKey()!!}'>
                    <span><i class="{!! !empty($menu->icon) ?  $menu->icon : '' !!}"></i> {!!$menu->name!!}</span>
                </a>
            </div>
            <ol class="dd-list">
            @include( 'menu::admin.menu.sub.nestable', array('menus' => $children))
               
            </ol>
        </li>
        @else
        <li class="dd-item dd3-item" data-id="{!!$menu->getRouteKey()!!}">
            <div class="dd-handle dd3-handle"><i class="las la-arrows-alt"></i></div>
            <div class="dd3-content">
                <a href="#" class="app-submenu-edit" data-action="LOAD" data-load-to='#menu-entry' data_val="{!!$menu->getRouteKey()!!}" data-href='{{guard_url('menu/submenu')}}/{!!$menu->getRouteKey()!!}'>
                    <span><i class="{!! !empty($menu->icon) ?  $menu->icon : '' !!}"></i> {!!$menu->name!!}</span>
                </a>
            </div>
        </li>
        @endif
        @endforeach
    </ol>
</div>
<script>
$(document).ready(function() {
    $(".app-submenu-edit").click(function(e) {
            e.preventDefault();   
            var id = $(this).attr('data_val');
        $("#app-content").load('{{guard_url('menu/submenu')}}'+'/'+id+'/edit');
    });
});
</script>