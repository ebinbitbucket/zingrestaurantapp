<div class="app-list-wrap">
    <div class="app-list-inner">
        <div class="app-list-header d-flex align-items-center justify-content-between">
            <h3>{{__('Menus')}}</h3>
            <div class="header-search">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="search-btn"><i class="las la-search"></i></span>
                <button type="button" class="settings" data-toggle="modal" data-target="#searchModal"><i class="las la-ellipsis-v"></i></button>
            </div>
            <button type="button" class="btn add-app-btn btn-icon btn-outline app-create" id="add_task_btn"><i class="las la-plus"></i></button>
        </div>

        <div class="app-list-wrap-inner ps ps--active-y" id="app-list">
            <div class="app-list-toolbar">
                <div class="app-list-pagination">
                    <a href="#"><i class="las la-angle-left"></i></a> 
                    &nbsp;
                    <span class="mr-5">Page 1 of 25</span>
                    <a href="#"><i class="las la-angle-left"></i></a>
                    <a href="#"><i class="las la-angle-right"></i></a>
                </div>
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <div class="dropdown-title">Settings</div>
                        <a class="dropdown-item" href="#"><i class="las la-inbox"></i>Show archived</a>
                        <a class="dropdown-item" href="#"><i class="las la-angle-down"></i>Expand all folders</a>
                        <a class="dropdown-item" href="#"><i class="las la-columns"></i>Layout size &amp; style</a>
                        <div class="dropdown-item d-flex align-items-center justify-content-between">
                            <span><i class="las la-fill-drip"></i>Dark sidebar</span>
                            <div class="custom-control custom-switch m-0 ml-5">
                                <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                <label class="custom-control-label" for="customSwitch1">&nbsp;</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @foreach($rootMenu as $menu)
          
            <div class="app-item {{$menu->getRouteKey()}}">
                <div class="app-info app-show" data-id="{{$menu->getRouteKey()}}">
                    <input type="checkbox" name="tasks_list" id="task_{{$menu->getRouteKey()}}">
                    <label class="app-project-name bg-secondary"
                        for="task_{{$menu->getRouteKey()}}">{{$menu['name'][0]}}</label>
                    <h3>{{$menu['name']}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress">{!! trans('menu::menu.options.status')[$menu['status']] !!}</span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-edit" data-id="{{$menu->getRouteKey()}}"></a>
                    <div class="dropdown">
                        <a href="#" class="btn fas fa-ellipsis-h dropdown-toggle" href="#" role="button"
                            id="app_quick_menu_{{$menu->getRouteKey()}}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="app_quick_menu_{{$menu->getRouteKey()}}">
                            <!-- <a class="dropdown-item" href="#appDetailModal" data-toggle="modal"
                                data-target="#appDetailModal"><i class="las la-eye"></i>View</a> -->
                            <a class="dropdown-item" href="#"><i class="las la-copy"></i>Copy</a>
                            <a class="dropdown-item app-delete"  data-id="{{$menu->getRouteKey()}}" href="#"><i class="las la-times text-danger"></i>Delete</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="app-detail-wrap" id="app-content">
    

    </div>
</div>
<script type="">
    var module_link = "{{guard_url('menu/menu')}}";
  
</script>