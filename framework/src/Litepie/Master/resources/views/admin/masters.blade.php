<div class="app-list-wrap">
    <div class="app-list-inner">
                        <div class="app-list-header d-flex align-items-center justify-content-between">
                            <h3>Tasks</h3>
                            <div class="header-search">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="search-btn"><i class="ik ik-search"></i></span>
                                <button type="button" class="settings" data-toggle="modal" data-target="#searchModal"><i class="fas fa-cog"></i></button>
                            </div>
                            <button type="button" class="btn add-app-btn btn-theme" id="add_task_btn"><i class="fas fa-plus"></i></button>
                        </div>
                        
                        <div class="app-list-toolbar">
                            <div class="app-list-pagination">
                                <span class="mr-5">1 of 25</span>
                                <a href="#"><i class="las la-angle-left"></i></a>
                                <a href="#"><i class="las la-angle-right"></i></a>
                            </div>
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-cog"></i></button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <div class="dropdown-title">Settings</div>
                                    <a class="dropdown-item" href="#"><i class="las la-inbox"></i>Show archived</a>
                                    <a class="dropdown-item" href="#"><i class="las la-angle-down"></i>Expand all folders</a>
                                    <a class="dropdown-item" href="#"><i class="las la-columns"></i>Layout size & style</a>
                                    <div class="dropdown-item d-flex align-items-center justify-content-between">
                                        <span><i class="las la-fill-drip"></i>Dark sidebar</span>
                                        <div class="custom-control custom-switch m-0 ml-5">
                                            <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                            <label class="custom-control-label" for="customSwitch1">&nbsp</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="app-list-wrap-inner">
                            @foreach($groups as $key => $group)
                                <div class="app-list-head">
                                    {{__("master::master.groups.{$key}.name")}}
                                </div>
                                @foreach($group as $k => $v)
                                    <div class="app-item selected" data-id="task_1">
                                        <div class="app-info">
                                            <input type="checkbox" name="tasks_list" id="task_1">
                                            <label class="app-project-name bg-secondary" for="task_1">B</label>
                                            <h3><a href="{{guard_url("masters/{$key}/{$v['type']}")}}">{{__("master::master.masters.{$v['type']}")}}
                                                <span class="pull-right badge bg-blue">{{@$count[$v['type']]}}</span>
                                            </a></h3>
                                            <div class="app-metas">
                                                <p>{{__("master::master.groups.{$key}.title")}}</p>
                                                <span class="badge badge-status in-progress">In Progress</span>
                                            </div>
                                        </div>
                                        <div class="app-actions">
                                            <a href="#" class="btn fas fa-pencil-alt"></a>
                                            <div class="dropdown">
                                                <a href="#" class="btn fas fa-ellipsis-h dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                                    <a class="dropdown-item" href="#appDetailModal" data-toggle="modal" data-target="#appDetailModal"><i class="las la-eye"></i>View</a>
                                                    <a class="dropdown-item" href="#"><i class="las la-copy"></i>Copy</a>
                                                    <a class="dropdown-item" href="#"><i class="las la-inbox"></i>Archive</a>
                                                    <a class="dropdown-item" href="#"><i class="las la-times text-danger"></i>Delete</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endforeach
                        </div>
                    </div>
    <div class="app-detail-wrap" id="app-content"></div>
</div>
<script type="">
    
    
</script>