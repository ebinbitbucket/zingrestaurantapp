<div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2> {!! trans('settings::setting.name') !!} <small> {!! trans('app.manage') !!} {!!
                trans('settings::setting.names') !!}</small></h2>
        <div class="actions">
            <button type="button" class="btn btn-with-icon btn-link app-update"><i
                    class="las la-save"></i>{{__('Save')}}</button>
        </div>
    </div>
    {!!Form::vertical_open()
    ->id('app-form-edit')
    ->class('app-form-edit')
    ->method('POST')
    ->files('true')
    ->action(guard_url('settings/mail'))!!}
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="app-entry-form-section" id="basic">
                    <div class="section-title">{!! trans('settings::setting.names') !!}</div>
                    <div class='row'>
                        <div class='col-md-12 col-sm-12'>
                            <div class='row'>
                                <div class='col-md-12 col-sm-12'>
                                    <fieldset>
                                        <div class="row">

                                            <div class="col-md-12">
                                                {!! Form::select('env[MAIL_DRIVER]')
                                                -> label(trans('settings::setting.label.mail.driver'))
                                                -> options(__('settings::setting.options.mail.driver'),
                                                env('MAIL_DRIVER'))!!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::text('env[MAIL_HOST]')
                                                -> label(trans('settings::setting.label.mail.host'))
                                                -> value(env('MAIL_HOST'))
                                                -> placeholder(trans('settings::setting.placeholder.mail.host'))!!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::text('env[MAIL_PORT]')
                                                -> label(trans('settings::setting.label.mail.port'))
                                                -> value(env('MAIL_PORT'))
                                                -> placeholder(trans('settings::setting.placeholder.mail.port'))!!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::text('env[MAIL_USERNAME]')
                                                -> label(trans('settings::setting.label.mail.user'))
                                                -> value(env('MAIL_USERNAME'))
                                                -> placeholder(trans('settings::setting.placeholder.mail.user'))!!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::text('env[MAIL_PASSWORD]')
                                                -> label(trans('settings::setting.label.mail.password'))
                                                -> value(env('MAIL_PASSWORD'))
                                                -> placeholder(trans('settings::setting.placeholder.mail.password'))!!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::select('env[MAIL_ENCRYPTION]')
                                                -> label(trans('settings::setting.label.mail.encryption'))
                                                -> options(__('settings::setting.options.mail.encryption'),
                                                env('MAIL_ENCRYPTION'))!!}
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>