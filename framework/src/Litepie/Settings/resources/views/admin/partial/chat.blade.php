<div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2> {!! trans('settings::setting.name') !!} <small> {!! trans('app.manage') !!} {!!
                trans('settings::setting.names') !!}</small></h2>
        <div class="actions">
            <button type="button" class="btn btn-with-icon btn-link app-update"><i
                    class="las la-save"></i>{{__('Save')}}</button>
        </div>
    </div>
    {!!Form::vertical_open()
    ->id('app-form-edit')
    ->class('app-form-edit')
    ->method('POST')
    ->files('true')
    ->action(guard_url('settings/chat'))!!}
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="app-entry-form-section" id="basic">
                    <div class="section-title">{!! trans('settings::setting.names') !!}</div>
                    <div class='row'>
                        <div class='col-md-12 col-sm-12'>
                            <div class='row'>
                                <div class='col-md-12 col-sm-12'>
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! Form::textarea('settings[main.chat.twalkto]')
                                                -> label(trans('settings::setting.label.chat.twalkto'))
                                                -> value(setting('main.chat.twalkto'))
                                                -> placeholder(trans('settings::setting.placeholder.chat.twalkto'))!!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::textarea('settings[main.chat.freshchat]')
                                                -> label(trans('settings::setting.label.chat.freshchat'))
                                                -> value(setting('main.chat.freshchat'))
                                                -> placeholder(trans('settings::setting.placeholder.chat.freshchat'))!!}
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>