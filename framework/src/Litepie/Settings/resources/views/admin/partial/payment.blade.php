<div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2> {!! trans('settings::setting.name') !!} <small> {!! trans('app.manage') !!} {!!
                trans('settings::setting.names') !!}</small></h2>
        <div class="actions">
            <button type="button" class="btn btn-with-icon btn-link app-update"><i
                    class="las la-save"></i>{{__('Save')}}</button>
        </div>
    </div>
    {!!Form::vertical_open()
    ->id('app-form-edit')
    ->class('app-form-edit')
    ->method('POST')
    ->files('true')
    ->action(guard_url('settings/payment'))!!}
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="app-entry-form-section" id="basic">
                    <div class="section-title">{!! trans('settings::setting.names') !!}</div>
                    <div class='row'>
                        <div class='col-md-12 col-sm-12'>
                            <div class='row'>
                                <div class='col-md-12 col-sm-12'>
                                    <fieldset>
                                        <legend>{{trans('settings::setting.label.payment.paypal')}}</legend>
                                        <div class="row">
                                            <div class="col-md-12">
                                                {!! Form::text('env[PAYPAL_CLIENT_ID]')
                                                -> label(trans('settings::setting.label.payment.paypal_client_id'))
                                                -> value(env('PAYPAL_CLIENT_ID'))
                                                ->
                                                placeholder(trans('settings::setting.placeholder.payment.paypal_client_id'))!!}
                                            </div>
                                            <div class="col-md-12">
                                                {!! Form::text('env[PAYPAL_SECRET]')
                                                -> label(trans('settings::setting.label.payment.paypal_client_secret'))
                                                -> value(env('PAYPAL_SECRET'))
                                                ->
                                                placeholder(trans('settings::setting.placeholder.payment.paypal_client_secret'))!!}
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>