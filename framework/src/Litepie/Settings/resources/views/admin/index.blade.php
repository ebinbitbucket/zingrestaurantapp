<div class="app-list-wrap">
    <div class="app-list-inner">
        <div class="app-list-header d-flex align-items-center justify-content-between">
            <h3>{!! trans('settings::setting.name') !!} <small> {!! trans('app.manage') !!} {!!
                    trans('settings::setting.names') !!}</small></h3>
          
        </div>

        <div class="app-list-wrap-inner ps ps--active-y" id="app-list">
           
            <h4>{{trans('settings::setting.title.general')}}</h4>
            <div class="app-item ">
                <div class="app-info app-show" data-id="main">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.main')[0]}}</label>
                    <h3>{{trans('settings::setting.title.main')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="main"></a>
                </div>
            </div>
            <div class="app-item ">
                <div class="app-info app-show" data-id="company">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.company')[0]}}</label>
                    <h3>{{trans('settings::setting.title.company')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="company"></a>
                </div>
            </div>
            <div class="app-item ">
                <div class="app-info app-show" data-id="theme">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.theme')[0]}}</label>
                    <h3>{{trans('settings::setting.title.theme')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="theme"></a>
                </div>
            </div>

            <h4>{{trans('settings::setting.title.subscription')}}</h4>
            <div class="app-item ">
                <div class="app-info app-show" data-id="social">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.team')[0]}}</label>
                    <h3>{{trans('settings::setting.title.team')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="social"></a>
                </div>
            </div>
            <div class="app-item ">
                <div class="app-info app-show" data-id="social">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.subscriptions')[0]}}</label>
                    <h3>{{trans('settings::setting.title.subscriptions')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="social"></a>
                </div>
            </div>

            <h4>{{trans('settings::setting.title.integration')}}</h4>
            <div class="app-item ">
                <div class="app-info app-show" data-id="social">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.social')[0]}}</label>
                    <h3>{{trans('settings::setting.title.social')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="social"></a>
                </div>
            </div>
            <div class="app-item ">
                <div class="app-info app-show" data-id="payment">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.payment')[0]}}</label>
                    <h3>{{trans('settings::setting.title.payment')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="payment"></a>
                </div>
            </div>
            <div class="app-item ">
                <div class="app-info app-show" data-id="mail">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.email')[0]}}</label>
                    <h3>{{trans('settings::setting.title.email')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="mail"></a>
                </div>
            </div>
            <div class="app-item ">
                <div class="app-info app-show" data-id="sms">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.sms')[0]}}</label>
                    <h3>{{trans('settings::setting.title.sms')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="sms"></a>
                </div>
            </div>
            <div class="app-item ">
                <div class="app-info app-show" data-id="chat">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.chat')[0]}}</label>
                    <h3>{{trans('settings::setting.title.chat')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="chat"></a>
                </div>
            </div>
            <div class="app-item ">
                <div class="app-info app-show" data-id="google">
                    <input type="checkbox" name="tasks_list" id="task_">
                    <label class="app-project-name bg-secondary"
                        for="">{{trans('settings::setting.title.google')[0]}}</label>
                    <h3>{{trans('settings::setting.title.google')}}</h3>
                    <div class="app-metas">
                        <p></p>
                        <span class="badge badge-status in-progress"></span>
                    </div>
                </div>
                <div class="app-actions">
                    <a href="#" class="btn fas fa-pencil-alt app-show" data-id="google"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="app-detail-wrap" id="app-content">


    </div>
</div>
<script type="">
    var module_link = "{{guard_url('settings/')}}";
    // $("#main-content").load("{{guard_url('settings/main')}}");
</script>