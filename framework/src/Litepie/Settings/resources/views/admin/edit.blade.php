<div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2>{{__('Edit')}} {!!$setting->name!!}</h2>
        <div class="actions">
            <button type="button" class="btn btn-with-icon btn-link app-update" data-id="{!!$setting->getRouteKey()!!}"><i class="las la-save"></i>{{__('Save')}}</button>
            <button type="button" class="btn btn-with-icon btn-link app-cancel" data-id="{!!$setting->getRouteKey()!!}"><i class="las la-window-close"></i>{{__('Cancel')}}</button>
        </div>
    </div>

    {!!Form::vertical_open()
    ->method('PUT')
    ->id('app-form-edit')
    ->class('app-form-edit')
    ->enctype('multipart/form-data')
    ->action(guard_url('settings/'. $setting->getRouteKey()))!!}

    @include('setting::setting.partial.entry', ['mode' => 'edit'])

    {!!Form::close()!!}
</div>