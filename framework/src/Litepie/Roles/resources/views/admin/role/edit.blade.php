    <div class="app-entry-form-wrap">
    <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2>{{__('Edit')}} {!!$role->name!!}</h2>
        <div class="actions">
            <button type="button" class="btn btn-with-icon btn-link app-update" data-id="{!!$role->getRouteKey()!!}"><i class="las la-save"></i>{{__('Save')}}</button>
            <button type="button" class="btn btn-with-icon btn-link app-cancel" data-id="{!!$role->getRouteKey()!!}"><i class="las la-window-close"></i>{{__('Cancel')}}</button>
        </div>
    </div>
        {!!Form::vertical_open()
        ->id('app-form-edit')
        ->class('app-form-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('roles/role/'. $role->getRouteKey()))!!}

        @include('roles::admin.role.partial.entry', ['mode' => 'edit'])

        {!!Form::close()!!}
    </div>