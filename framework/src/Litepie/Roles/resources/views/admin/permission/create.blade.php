
    <div class="app-entry-form-wrap">
        <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
        <i class="lab la-product-hunt app-sec-title-icon"></i>
        <h2>{{__('Create')}} {{ trans('permissions::permission.name') }}</h2>
        <div class="actions">
            <button type="button" class="btn btn-with-icon btn-link app-store"><i class="las la-save"></i>{{__('Create')}}</button>
        </div>
    </div>
        {!!Form::vertical_open()
        ->id('app-form-create')
        ->class('app-form-create')
        ->method('POST')
        ->files('true')
        ->action(trans_url('roles/permission'))!!}
        @include('roles::admin.permission.partial.entry', ['mode' => 'create'])
        {!! Form::close() !!}
    </div>