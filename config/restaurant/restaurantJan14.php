<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'restaurant',

    /*
     * Package.
     */
    'package'   => 'restaurant',

    /*
     * Modules.
     */
    'modules'   => ['restaurant', 
'addon', 
'category', 
'menu', 
'review'],

    'restaurant'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Restaurant::class,
            'table'                 => 'restaurants',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\RestaurantPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'name', 'category_name', 'description',  'address',  'phone',  'email',  'password',  'api_token',  'remember_token',  'price_range_min',  'price_range_max',  'keywords',  'zipcode',  'latitude',  'longitude', 'rating',  'logo','offer',  'gallery',  'delivery',  'type', 'ac_no', 'bank', 'IFSC', 'GST_no', 'delivery_charge', 'min_order_count','audio_alert','social_media_links','yelp_id','status','fetched', 'yelp_json','tax_rate', 'CCR','CCF','ZR','ZF','ZC', 'ESign','memobirdID','published','flag_special_instr','confirmation_mail','confirmation_text_mail','confirmation_fax_mail','delivery_limit','kitchen_view','catering','chain_list', 'votes','photos','google_photo_status','menus_masters','menus_masters_status','timings_status','min_order_amount_delivery', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/restaurant',
            'uploads'               => [
            
                    'logo' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                    'offer' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                    'gallery' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    // 'file' => [
                    //     'count' => 1,
                    //     'type'  => 'file',
                    // ],
            
            ],

            'casts'                 => [
            
                'images'    => 'array',
                'file'      => 'array',
                'logo'      => 'array',
                'offer'      => 'array',
                'gallery'   => 'array',
                'photos'   => 'array',
                'working_hours' => 'array',
                'social_media_links' => 'array',
                 'confirmation_mail' => 'array',
                'votes' => 'array',
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'delivery' => 'like',
                'category_name' => 'like',
                // 'price_range_min' => 'like',
                // 'price_range_max' => 'like',
                'keywords' => 'like',
                // 'address' =>'like',
                'working_hours' => 'like',
                'rating'  => '>=',
                'status',
                'type' => 'like',
                'published' => '=',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Restaurant',
        ],

    ],

    'addon'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Addon::class,
            'table'                 => 'restaurant_addons',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\AddonPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'name', 'price',  'slug', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/addon',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            
                'variations'    => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
                'restaurant_id' => '=',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Addon',
        ],

    ],

    'category'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Category::class,
            'table'                 => 'restaurant_categories',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\CategoryPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',   'name',  'preparation_time',  'slug','order_no', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/category',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
                'restaurant_id' => '=',
                'parent_id' => '=',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Category',
        ],

    ],

    'menu'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Menu::class,
            'table'                 => 'restaurant_menus',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\MenuPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'category_id',  'name',  'description',  'price',  'image',  'menu_variations', 'timing', 'offer',  'slug',  'status', 'youtube_link', 'home_show', 'serves', 'max_order_count','review','catering','master_category','search_text','master_search', 'user_id', 'user_type', 'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/menu',
            'uploads'               => [
            
                    'image' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            
            ],

            'casts'                 => [
            
                'images'    => 'array',
                'file'      => 'array',
                'image'    => 'array',
                'menu_variations'    => 'array',
                'timing'    => 'array',
                'offer'    => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
                'restaurant_id' => '=',
                'category_id' => '=',
                 'master_category' => '=',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Menu',
        ],

    ],

    'review'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Review::class,
            'table'                 => 'restaurant_reviews',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\ReviewPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id', 'restaurant_id',  'customer_id',  'order_id',  'rating',  'review','menu_id', 'user_id', 'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/review',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
                'json_user'    => 'array',
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Review',
        ],

    ],

    'addonvariations'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Addonvariations::class,
            'table'                 => 'addon_variations',
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'fillable'              => ['id',  'addon_id',  'name', 'description', 'price', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'upload_folder'         => 'restaurant/addon',
        ],
    ],

    'restauranttimings'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Restauranttimings::class,
            'table'                 => 'restaurant_timings',
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'fillable'              => ['id',  'restaurant_id',  'day', 'opening' , 'closing', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'upload_folder'         => 'restaurant/restaurant',
            'casts'                 => [
            
                'timing'    => 'array',
            
            ],
        ],
     ],
         'clientaddress'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Clientaddress::class,
            'table'                 => 'client_address',
            'dates'                 => [],
            'fillable'              => ['id',  'client_id',  'title', 'address' , 'code'],
            'upload_folder'         => 'restaurant/restaurant',
           
        ],
    ],
         'favourite'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Favourite::class,
            'table'                 => 'favourites',
            'dates'                 => [],
            'fillable'              => ['id',  'user_id',  'favourite_id', 'type' , 'created_at' ,'updated_at' ,'deleted_at'],
            'upload_folder'         => 'restaurant/restaurant',
           
        ],
    ],
];
