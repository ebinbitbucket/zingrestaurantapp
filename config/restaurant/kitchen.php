<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'restaurant',

    /*
     * Package.
     */
    'package'   => 'kitchen',

    /*
     * Modules.
     */
    'modules'   => ['kitchen'],

    
    'kitchen'       => [
        'model' => [
            'model'                 => \Restaurant\Kitchen\Models\Kitchen::class,
            'table'                 => 'restaurant_kitchens',
            'presenter'             => \Restaurant\Kitchen\Repositories\Presenter\KitchenPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id', 'fcm','device_id', 'restaurant_id',  'name','username',  'description',  'email' ,'last_accessed',  'password',  'api_token',  'remember_token',  'slug',  'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at', 'kitchen_status', 'stop_date','auto_refresh'],
            'translatables'         => [],
            'upload_folder'         => 'kitchen/kitchen',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Kitchen',
            'module'    => 'Kitchen',
        ],

    ],
];

