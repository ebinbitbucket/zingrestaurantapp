<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'restaurant',

    /*
     * Package.
     */
    'package'   => 'master',

    /*
     * Modules.
     */
    'modules'   => ['category', 
'cuisine', 
'master'],

    'category'       => [
        'model' => [
            'model'                 => \Restaurant\Master\Models\Category::class,
            'table'                 => 'master_categories',
            'presenter'             => \Restaurant\Master\Repositories\Presenter\CategoryPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'name',  'icon',  'user_id',  'user_type',  'created_at',  'updated_at',  'deleted_at'],
            'upload_folder'         => 'master/category',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Master',
            'module'    => 'Category',
        ],

    ],

    'cuisine'       => [
        'model' => [
            'model'                 => \Restaurant\Master\Models\Cuisine::class,
            'table'                 => 'cuisines',
            'presenter'             => \Restaurant\Master\Repositories\Presenter\CuisinePresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'name',  'image',  'filter_show',  'user_id',  'user_type',  'created_at',  'updated_at',  'deleted_at'],
            'upload_folder'         => 'master/cuisine',
            'uploads'               => [
            
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            
            ],

            'casts'                 => [
            
                'image'    => 'array',
                'file'      => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Master',
            'module'    => 'Cuisine',
        ],

    ],

    'master'       => [
        'model' => [
            'model'                 => \Restaurant\Master\Models\Master::class,
            'table'                 => 'masters',
            'presenter'             => \Restaurant\Master\Repositories\Presenter\MasterPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'name',  'description',  'image',  'cuisine','tags', 'slug', 'user_id',  'user_type',  'created_at',  'updated_at',  'deleted_at', 'display_status'],
            'upload_folder'         => 'master/master',
            'uploads'               => [
            
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            
            ],

            'casts'                 => [
            
                'image'    => 'array',
                'file'      => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Master',
            'module'    => 'Master',
        ],

    ],
];
