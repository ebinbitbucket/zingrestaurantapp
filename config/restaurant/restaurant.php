<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'restaurant',

    /*
     * Package.
     */
    'package'   => 'restaurant',

    /*
     * Modules.
     */
    'modules'   => ['restaurant', 
'addon', 
'category', 
'menu', 
'review'],

    'restaurant'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Restaurant::class,
            'table'                 => 'restaurants',
            'presenter'             => \Litecms\Restaurantapp\Repositories\Presenter\RestaurantPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at', 'stop_date'],
            'appends'               => [],
            'fillable'              => ['id', 'slug', 'name', 'category_name', 'description',  'address',  'phone',  'email',  'password',  'api_token',  'remember_token',  'price_range_min',  'price_range_max',  'keywords',  'zipcode',  'latitude',  'longitude', 'rating',  'logo','offer',  'gallery',  'delivery',  'type', 'ac_no', 'bank', 'IFSC', 'GST_no', 'delivery_charge', 'min_order_count','audio_alert','social_media_links','yelp_id','status','fetched', 'yelp_json','tax_rate', 'CCR','CCF','ZR','ZF','ZC', 'ESign','memobirdID','published','flag_special_instr','confirmation_mail','confirmation_text_mail','confirmation_fax_mail','delivery_limit','kitchen_view','catering','chain_list', 'votes','photos','google_photo_status','menus_masters','menus_masters_status','timings_status','min_order_amount_delivery', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at', 'title', 'subtitle', 'meta_tags', 'meta_keywords', 'meta_description', 'specialities', 'banner_image', 'spcialities_image', 'businesshours_image','explore_speciality','featured_food','locations' ,'theme_color', 'working_hours','bg_color','theme_font','website_status', 'stop_date', 'kichen_status', 'link','website_gallery','del_charge','theme_design', 'header_image', 'customer_email', 'customer_phone', 'review_image', 'contact_info','reminder','delay_call_number', 'menu_button','non_contact', 'offer_image','car_pickup','special_instr_placeholder','acc_date','website_domain','second_call_delay'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/restaurant',
            'uploads'               => [
            
                    'logo' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                    'offer' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                    'gallery' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'banner_image' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                     'spcialities_image' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                     'businesshours_image' => [
                        'count' => 2,
                        'type'  => 'image',
                    ],
                     'header_image' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                   'review_image' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                    'offer_image' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
            
            ],

            'casts'                 => [
            
                'images'    => 'array',
                'file'      => 'array',
                'logo'      => 'array',
                'offer'      => 'array',
                'gallery'   => 'array',
                'photos'   => 'array',
                'working_hours' => 'array',
                'social_media_links' => 'array',
                 'confirmation_mail' => 'array',
                'votes' => 'array',
                'banner_image' => 'array',
                'spcialities_image' => 'array',
                'businesshours_image' => 'array',
                'explore_speciality' => 'array',
                'locations'=>'array',
                'featured_food'=>'array',
                'link'=>'array',
                'website_gallery'=>'array',
                'header_image'=>'array',
                'review_image'=>'array',
                'del_charge'=>'array',
                'menu_button'=>'array',
                'offer_image'=>'array'
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'delivery' => 'like',
                'category_name' => 'like',
                // 'price_range_min' => 'like',
                // 'price_range_max' => 'like',
                'keywords' => 'like',
                // 'address' =>'like',
                'working_hours' => 'like',
                'rating'  => '>=',
                'status',
                'id',
                'type' => 'like',
                'published' => '=',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Restaurant',
        ],

    ],

    'addon'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Addon::class,
            'table'                 => 'restaurant_addons',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\AddonPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'name', 'price',  'slug', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/addon',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            
                'variations'    => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
                'restaurant_id' => '=',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Addon',
        ],

    ],

    'category'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Category::class,
            'table'                 => 'restaurant_categories',
            'presenter'             => \Litecms\Restaurantapp\Repositories\Presenter\CategoryPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id', 'stock_status',   'name',  'preparation_time',  'slug','order_no', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/category',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
                'restaurant_id' => '=',
                'parent_id' => '=',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Category',
        ],

    ],
    'order'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Order::class,
            'table'                 => 'orders',
            'presenter'             => \Litecms\Restaurantapp\Repositories\Presenter\OrderPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id', 'flag', 'name', 'mail_sent', 'email',  'phone',  'address',  'address_id', 'billing_address_id', 'restaurant_id',  'phone',  'street',  'zipcode',  'state_id',  'country_id',  'shipping_name',  'shipping_city',  'shipping_zipcode',  'shipping_email',  'shipping_address',  'shipping_phone',  'track_status',  'shipping',  'delivery_id',  'subtotal',  'tax',  'total','discount_points', 'discount_amount', 'order_status', 'payment_status',  'payment_methods',  'payment_details','loyalty_points', 'tip', 'tip_value', 'tip_value_cus', 'tax_rate','delivery_charge','CCR','CCF','ZR','ZF','ZC','feedback_mail_status','delay_mail_status', 'created_at',  'updated_at',  'deleted_at','apartment_no','delivery_instr','delivery_time','preparation_time','ready_time','order_type','min_order_difference', 'user_id', 'user_type','json_data', 'guest','non_contact','car_pickup','car_details','car_pickup_date','is_car_pickup','text_notification','call_count'],
            'translatables'         => [],
            'upload_folder'         => 'cart/order',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
           
                'car_details'    => 'array',
              /*   'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'restaurant_id' => '=',
                'id'  => '=',
                'phone'  => 'like',
                'total'  => 'like',
                'payment_status'  => 'like',
                'order_status'=> 'like',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Order',
        ],

    ],
    'cart'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Cart::class,
            'table'                 => 'carts',
            'presenter'             => \Litecms\Restaurantapp\Repositories\Presenter\CartPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'instance',  'content',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/cart',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Cart',
        ],

    ],

    'menu'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Menu::class,
            'table'                 => 'restaurant_menus',
            'presenter'             => \Litecms\Restaurantapp\Repositories\Presenter\MenuPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'stock_status','restaurant_id',  'category_id',  'name',  'description',  'price',  'image',  'menu_variations', 'timing', 'offer',  'slug',  'status', 'youtube_link', 'home_show', 'serves', 'max_order_count','review','catering','master_category','search_text','master_search','popular_dish_no', 'user_id', 'user_type', 'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/menu',
            'uploads'               => [
            
                    'image' => [
                        'count' => 1,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            
            ],

            'casts'                 => [
            
                'images'    => 'array',
                'file'      => 'array',
                'image'    => 'array',
                'menu_variations'    => 'array',
                'timing'    => 'array',
                'offer'    => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status'=>'=',
                'restaurant_id' => '=',
                'category_id' => '=',
                'stock_status' => '=',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Menu',
        ],

    ],

    'review'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Review::class,
            'table'                 => 'restaurant_reviews',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\ReviewPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id', 'restaurant_id',  'customer_id',  'order_id',  'rating',  'review','menu_id', 'user_id', 'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/review',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
                'json_user'    => 'array',
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Review',
        ],

    ],

    'addonvariations'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Addonvariations::class,
            'table'                 => 'addon_variations',
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'fillable'              => ['id',  'addon_id',  'name', 'description', 'price', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'upload_folder'         => 'restaurant/addon',
        ],
    ],

    'restauranttimings'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Restauranttimings::class,
            'table'                 => 'restaurant_timings',
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'fillable'              => ['id',  'restaurant_id',  'day', 'opening' , 'closing', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'upload_folder'         => 'restaurant/restaurant',
            'casts'                 => [
            
                'timing'    => 'array',
            
            ],
        ],
     ],
     
    'deliverytimings'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Deliverytimings::class,
            'table'                 => 'restaurant_delivery_timings',
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'fillable'              => ['id',  'restaurant_id',  'day', 'opening' , 'closing', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'upload_folder'         => 'restaurant/restaurant',
            'casts'                 => [
            
                'delivery_timing'    => 'array',
            
            ],
        ],
     ],
         'clientaddress'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Clientaddress::class,
            'table'                 => 'client_address',
            'dates'                 => [],
            'fillable'              => ['id',  'client_id',  'title', 'address' , 'code'],
            'upload_folder'         => 'restaurant/restaurant',
           
        ],
    ],
         'favourite'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Favourite::class,
            'table'                 => 'favourites',
            'dates'                 => [],
            'fillable'              => ['id',  'user_id',  'favourite_id', 'type' , 'created_at' ,'updated_at' ,'deleted_at'],
            'upload_folder'         => 'restaurant/restaurant',
           
        ],
    ],
     'location'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Location::class,
            'table'                 => 'locations',
            'dates'                 => [],
            'fillable'              => ['id',  'name',  'parent_id', 'slug' , 'type' ],
            'upload_folder'         => 'restaurant/restaurant',
            
        ],
    ],
    'push_notifications'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Pushnotifications::class,
            'table'                 => 'push_notifications',
            'dates'                 => ['created_at',  'deleted_at',  'updated_at'],
            'fillable'              => ['id', 'restaurant_id' , 'title',  'content', 'schedule_date','notification_sent_status','created_at',  'deleted_at',  'updated_at' ],
            'upload_folder'         => 'restaurant/restaurant',
            
        ],
    ],
    'restaurant_app'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Restaurantapp::class,
            'table'                 => 'restaurant_app',
            'dates'                 => ['created_at',  'deleted_at',  'updated_at'],
            'fillable'              => ['id', 'restaurant_id', 'title', 'subtitle', 'meta_tags', 'meta_keywords', 'meta_description', 'email', 'alternate_phone', 'gallery', 'featured_food', 'slider_images', 'locations','contact_info', 'menus', 'created_at',  'deleted_at',  'updated_at','points','point_percentage'],
            'upload_folder'         => 'restaurant/restaurant',
            'uploads'               => [
                    'slider_images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                     'featured_food' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                     'gallery' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
            ],

            'casts'                 => [
            
                'slider_images'    => 'array',
                'featured_food'      => 'array',
                'gallery'    => 'array',
                'locations'    => 'array',
                'menus'    => 'array',
            ],
        ],
    ],
    'details'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Details::class,
            'table'                 => 'order_details',
            'presenter'             => \Litecms\Restaurantapp\Repositories\Presenter\DetailsPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'order_id',  'menu_id',  'quantity',  'unit_price',  'price', 'menu_addon','special_instr','addons','variation',  'user_id',  'user_type',  'parameters',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/details',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            'addons' => 'array',
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Details',
        ],

    ],
];
