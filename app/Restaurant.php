<?php

namespace App;

use Litepie\User\Models\Client as BaseClient;

class Restaurant extends BaseClient 
{
    /**
     * Configuartion for the model.
     *
     * @var array
     */
    protected $config = 'restaurant.restaurant.restaurant.model';

    /**
     * Roles for the user type.
     *
     * @var array
     */
    protected $role = 'restaurant';
}
