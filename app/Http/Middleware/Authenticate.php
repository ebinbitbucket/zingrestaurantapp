<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request)
    {
        if ($request->filled('is_app')) {
            $is_app = $request['is_app'];
            $restaurant_id = $request['restaurant_id'];
            if($is_app == 1){
                return 'app/'.$restaurant_id;
            }
        }else{
            return route('login');
        }
        if (! $request->expectsJson()) {
            $guard = guard();

            if (empty($guard)) {
                config('auth.defaults.guard');
            }

            $guard= current(explode('.', $guard));
            return $guard . '/login';
        }
    }
}
