require('./scripts/modernizr-3.6.0.min.js');
require('bootstrap/dist/js/bootstrap.bundle.min.js');
window.toastr = require('toastr/toastr.js');
window.swal = require('sweetalert/dist/sweetalert.min.js');
require('jquery-validation/dist/jquery.validate.js' );
window.moment = require('moment/moment.js');
window.Mustache = require('mustache/mustache.js');
require('tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js');
require('daterangepicker/daterangepicker.js');

require('select2/dist/js/select2.full.js');
require('summernote/dist/summernote-bs4.js');
window.Dropzone = require('dropzone/dist/dropzone.js');
require('./scripts/Sortable.js');

