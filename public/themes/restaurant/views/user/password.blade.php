<div class="main-wrap auth-wrap">
    <section class="window-height section-block container-fluid">
        <div class="media-column col-md-7">
            <div class="bkg-img window-height" style="background-image: url('{{theme_asset('img/user-forgot.jpg')}}');"></div>
        </div>
        <div class="row">
            <div class="col-md-4 column">
                <div class="split-hero-content">
                    <div class="hero-content-inner">
                        <a href="{{url('/')}}"><img src="{{theme_asset('img/logo-round.png')}}" class="img-responsive logo" alt=""></a>
                        <h3>Reset Password</h3>
                        @include('notifications')
                        <div class="login-form-container">
                            {!!Form::vertical_open()
                            ->id('contact')
                            ->method('POST')
                            ->class('change-password')!!}
                                <div class="form-group">
                                    <label for="old_password">Current Password</label>
                                    {!! Form::password('old_password')
                                    ->required()
                                    ->placeholder('Enter your current password')
                                    ->raw() !!}
                                    <i class="flaticon-locked"></i>
                                </div>
                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    {!! Form::password('password')
                                    ->required()
                                    ->placeholder('Enter your new password')
                                    ->raw() !!}
                                    <i class="flaticon-key"></i>
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Confirm New Password</label>
                                    {!! Form::password('password_confirmation')
                                    ->required()
                                    ->placeholder('Confirm new password')
                                    ->raw() !!}
                                    <i class="flaticon-key"></i>
                                </div>
                                <div class="form-group mt-30">
                                    <button class="btn btn-theme" type="submit">Update Password</button>
                                </div>
                            {!! Form::close() !!}
                            <p class="text-small mt20">Back to  <a href="{{guard_url('login')}}">&nbsp; Home</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<style type="text/css">
    header, footer {
        display: none;
    }
</style>