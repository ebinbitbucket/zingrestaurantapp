@if (Session::has('message'))
    @if (session('code') < 200)
        <div class="alert alert-info">
            <button type="button" aria-hidden="true" class="close"  data-dismiss="alert" aria-label="close">
                <i class="icon-close"></i>
            </button>
            <span>
                <b></b><center> {{ session('message') }}</center></span>
        </div>
    @elseif  (session('code') < 300)
        <div class="alert alert-success">
            <button type="button" aria-hidden="true" class="close"  data-dismiss="alert" aria-label="close">
                <i class="icon-close"></i>
            </button>
            <span>
                <b> Success - </b> {{ session('message') }}</span>
        </div>
    @elseif  (session('code') < 400)
        <div class="alert alert-warning">
            <button type="button" aria-hidden="true" class="close"  data-dismiss="alert" aria-label="close">
                <i class="icon-close"></i>
            </button>
            <span>
                <b> Warning - </b> {{ session('message') }}</span>
        </div>
    @else
        <div class="alert alert-danger">
            <button type="button" aria-hidden="true" class="close"  data-dismiss="alert" aria-label="close">
                <i class="icon-close"></i>
            </button>
            <span>
                <b> Error - </b> {{ session('message') }}</span>
        </div>
    @endif
@endif

<!-- @if ($message = Session::get('message'))
        <div class="alert alert-danger" role="alert">
            <div class="container">
                <div class="alert-icon">
                    <i class="ion-ios-bell-outline"></i>
                </div>
                <strong></strong>fchujcjh {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">
                        <i class="ion-ios-close-outline"></i>
                    </span>
                </button>
            </div>
        </div>
{{ Session::forget('error') }}
@endif -->
@if (Session::has('errors'))
        <div class="alert alert-danger alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b> Error </b>
            <ul>
              @foreach(Session::get('errors')->all() as $message)
              <li>{{$message}} </li>
              @endforeach
            </ul>
        </div>
@endif

@if (Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b> Success </b>
            <div>{{Session::get('success')}}</div>
        </div>
@endif

@if (Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <b> Success </b>
            <div>{{Session::get('success')}}</div>
        </div>
@endif

@if (Session::has('status'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
         
            <div>{{Session::get('status')}}</div>
        </div>
@endif

@if ($errors->any())
        <div class="alert alert-danger" role="alert">
            <div class="container">
                <div class="alert-icon">
                    <i class="ion-ios-bell-outline"></i>
                </div>
                <strong>Oops!</strong>@if(!empty($errors->first('email'))){{$errors->first('email')}}<br/>@endif 
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">
                        <i class="ion-ios-close-outline"></i>
                    </span>
                </button>
            </div>
        </div>
@endif