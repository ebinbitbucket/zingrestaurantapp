
<section class="window-height section-block container-fluid">
                <div class="media-column col-md-7">
                    <div class="bkg-img window-height" style="background-image: url({{url('assets/img/login-bg-1.jpg')}});"></div>
                </div>
                <div class="row">
                    <div class="col-md-4 column">
                        <div class="split-hero-content">
                            <div class="hero-content-inner">
                                <a href="{{url('/')}}"><img src="{{url('assets/img/logo-round.png')}}" class="img-responsive logo" alt=""></a>
                                
                                <h3>Eatery Partner login</h3>
                                @include('notifications')
                                <div class="login-form-container mb-30">
                                    <form action="{{url('restaurant/login')}}" class="login-form" method="POST">
                                        @csrf
                                        <div class="form-group">
                                            <!--<label for="name">Username</label>-->
                                            <input type="text" class="form-control" id="name" name="email" placeholder="Username" required="">
                                            <i class="flaticon-user"></i>
                                        </div>
                                        <div class="form-group">
                                            <!--<label for="password">Password</label>-->
                                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" required="">
                                            <i class="flaticon-key"></i>
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                <label class="custom-control-label" for="customCheck1">Remember Me</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button class="btn btn-theme" type="submit" style="width:100%;">Login</button>
                                        </div>
                                    </form>
                                    <p class="text-small mt20">I forgot my password - <a href="{{url('restaurant/password/reset')}}" >Reset Here</a></p>
                                </div>
                                <h3 class="mb-10" style="font-size: 16px;">Not a Partner ?</h3>
                                <a href="{{url('restaurant/register')}}" class="btn btn-theme" style="padding: 6px 20px; padding-left: 6px;"><i class="z-icon mr-5" style="vertical-align: -4px;"></i>Join Zing</a>
                                <p class="mb-10 mt-10"><a style="font-size: 16px; font-weight: 700; color: #3e5569;" href="{{url('how-it-works')}}">Discover How it Works </a><a href="{{url('how-it-works')}}"><i class="fa fa-arrow-circle-right ml-5" style="font-size: 20px; color: #3e5569; vertical-align: -2px;"></i></a></a></p>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
<script type="text/javascript">
    function reset(){
        $('#name').val('');
        $('#email').val('');  
    }
</script>