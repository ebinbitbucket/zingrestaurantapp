<div class="main-wrap auth-wrap">
    <section class="window-height section-block container-fluid">
        <div class="media-column col-md-7">
            <div class="bkg-img window-height" style="background-image: url('{{theme_asset('img/user-forgot.jpg')}}');"></div>
        </div>
        <div class="row">
            <div class="col-md-4 column">
                <div class="split-hero-content">
                    <div class="hero-content-inner">
                        <a href="{{url('/')}}"><img src="{{theme_asset('img/logo-round.png')}}" class="img-responsive logo" alt=""></a>
                        <h3>Forgot Password</h3>
                        <p class="mb20">Enter your registered email address and we will send you a reset code.</p>
                        @include('notifications')
                        <div class="login-form-container">
                            {!!Form::vertical_open()
                            ->id('reset')
                            ->action(guard_url('password/email'))
                            ->method('POST')!!}
                                <div class="form-group">
                                    <label for="name">Email</label>
                                    {!! Form::email('email')
                                    ->required()
                                    ->placeholder('Enter your email')
                                    ->raw() !!}
                                    <i class="flaticon-mail"></i>
                                </div>
                                <div class="form-group mt-30">
                                    <button class="btn btn-theme" type="submit">Send Password</button>
                                </div>
                            {!! Form::close() !!}
                            <p class="text-small mt20">Back to  <a href="{{guard_url('login')}}">&nbsp; Login</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>