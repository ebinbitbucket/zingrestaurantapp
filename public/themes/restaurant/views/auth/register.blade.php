
<section class="window-height section-block container-fluid">
                <div class="media-column col-md-7">
                    <div class="bkg-img window-height" style="background-image: url({{url('assets/img/login-bg-2.jpg')}});"></div>
                </div>
                <div class="row">
                    <div class="col-md-4 column">
                        <div class="split-hero-content">
                            <div class="hero-content-inner">
                                <a href="{{url('/')}}"><img src="{{url('assets/img/logo-round.png')}}" class="img-responsive logo" alt=""></a>
                                <h3>Partner with us</h3>
                                <p class="mb20">Already have an account? <a href="{{url('restaurant/login')}}">Click Here</a></p>
                                @if (Session::has('validation_errors'))
    @foreach(Session::get('validation_errors') as $val_errors)
    @foreach($val_errors as $validation)

        <div class="alert alert-danger">
            <button type="button" aria-hidden="true" class="close"  data-dismiss="alert" aria-label="close">
                <i class="icon-close"></i>
            </button>
            <span>
                <b> Error - </b> {{ $validation }}</span>
        </div>
     @endforeach
     @endforeach
     {{ Session::forget('validation_errors') }}

@endif
                                <div class="login-form-container restaurant-register">
                                    {!!Form::vertical_open()
                                            ->id('register')
                                            ->method('POST')!!}

                                            {!! Form::text('name')
                                            ->required()
                                            ->label('Eatery Name')
                                            ->addClass('form-control')
                                            ->placeholder('Eatery Name') !!}

                                             {!! Form::text('address')
                                            ->required()
                                            ->id('txtaddress')
                                            ->label('Eatery Address')
                                            ->addClass('form-control')
                                            ->placeholder('Eatery Address') !!}

                                             {!! Form::text('contact_name')
                                            ->required()
                                            ->label('Contact Name')
                                            ->addClass('form-control')
                                            ->placeholder('Contact Name') !!}

                                            {!! Form::email('email')
                                            ->required()
                                            ->addClass('form-control')
                                            ->placeholder('Email') !!}

                                            {!! Form::tel('phone')
                                            ->required()
                                            ->addClass('form-control')
                                            ->placeholder('Mobile Number') !!}

                                            {!! Form::password('password')
                                            ->required()
                                            ->onGroupAddClass('mb-20 row')
                                            ->placeholder('Password')!!}

                                            {!! Form::password('password_confirmation')
                                            ->onGroupAddClass('mb-20 row')
                                            ->required()!!}
                                            <input type="hidden" name="latitude" id="latitude_address">
                                            <input type="hidden" name="longitude" id="longitude_address">
                                            <!-- <div class="form-group">
                                                <p>By clicking “Register Now,” you agree to <a href="{{url('terms-and-conditions.html')}}">Zing My Order General Terms and Conditions</a> and acknowledge you have read the <a href="{{url('privacy.html')}}">Privacy Policy</a>.</p>
                                            </div> -->
                                            <div class="custom-control custom-checkbox mb-20">
                                                <input type="checkbox" class="custom-control-input" required id="terms_Conditions">
                                                <label class="custom-control-label" for="terms_Conditions" style="font-size: 14px;">I agree to <a href="{{url('terms-and-conditions.html')}}">Zing My Order General Terms and Conditions</a> and acknowledge you have read the <a href="{{url('privacy.html')}}">Privacy Policy</label>
                                            </div>
                                             <div class="form-group">
                                                <button class="btn btn-theme" type="submit">Register Now</button>
                                            </div>
                                     {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
<script type="text/javascript">
     var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtaddress'));
        google.maps.event.addListener(places, 'place_changed', function () {
             geocodeAddress(geocoder);
        });
    });
    function geocodeAddress(geocoder) {
        var address = document.getElementById('txtaddress').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
</script>