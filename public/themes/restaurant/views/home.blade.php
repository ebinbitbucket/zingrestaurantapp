@include('restaurant::default.restaurant.partial.header')
<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon">
                <i class="flaticon-shopping-cart app-sec-title-icon"></i>
                <h1>Recent Orders</h1>
            </div>
        </div>
    </div>
</div>
