<aside class="main-nav">
    <div class="nav-inner">
    
        <div class="links-wrap">
            <a href="{{guard_url('/')}}" class="active"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/settings')}}"><i class="icon ion-settings"></i>Settings</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/menu-item-status')}}"><i class="icon ion-android-restaurant"></i>Menu Status</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
            <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i>Statement</a>
            <a href="{{guard_url('restaurant/website')}}"><i class="ion-android-clipboard"></i>Website</a>
            <a href="{{guard_url('restaurant/appdetails')}}"><i class="ion-android-clipboard"></i>App</a>
        </div>

    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-3 d-none d-lg-block">
                <aside class="dashboard-sidemenu">
                

                    <nav class="sidebar-nav">
                        <ul>
                            <li class="nav-head"><span class="head">Navigation</span></li>
                            <li class="active">
                                <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                            </li>
                            <li >
                                <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                            </li>
                            <li >
                                <a href="{{guard_url('restaurant/settings')}}"><i class="icon ion-card"></i><span> Settings</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/menu-item-status')}}"><i class="icon ion-android-restaurant"></i><span>Menu Status</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i><span>Eatery Schedule</span></a>
                            </li>
                             <li>
                                <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i><span>Kitchen</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i><span>Statement</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/website')}}"><i class="ion-android-clipboard"></i><span>Website</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/appdetails')}}"><i class="ion-android-clipboard"></i><span>App</span></a>
                            </li>

                        </ul>
                    </nav>

                </aside>
            </div>
            <div class="col-md-12 col-lg-9">
                <iframe height="900px" width="100%" style="display: none;" name="iframe_a" id="test"></iframe>
                <div id="dashboard">
                <div class="element-wrapper">
                    <h6 class="element-header">Dashboard</h6>
                    <div class="element-content">
                        <div class="row">
                            {{-- <div class="col-sm-4">
                                <a class="element-box el-tablo" href="#">
                                    <div class="label">Items Sold</div>
                                    <div class="value">{{Cart::totalorder('products')}}</div>
                                   <!--  <div class="trending trending-up-basic"><span>12%</span><i class="icon ion-arrow-up-b"></i></div> -->
                                </a>
                            </div>
                            <div class="col-sm-4">
                                <a class="element-box el-tablo" href="#">
                                    <div class="label">Total Sale</div>
                                    <div class="value">${!!number_format(Cart::totalorder('total'),2)!!}</div>

<!--                                                 <div class="trending trending-down-basic"><span>12%</span><i class="icon ion-arrow-down-b"></i></div>
-->                                            </a>
                            </div>
                            <div class="col-sm-4">
                                <a class="element-box el-tablo" href="#">
                                    <div class="label">New Orders</div>
                                    <div class="value">{{Cart::totalorder('neworders')}}</div>
                                    <!-- <div class="trending trending-down-basic"><span>9%</span><i class="icon ion-arrow-down-b"></i></div> -->
                                </a>
                            </div> --}}

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="element-wrapper">
                            <h6 class="element-header">New Orders</h6>
                            <div class="element-box p-0">
                                <div class="table-responsive">
                                    <table class="table table-lightborder">
                                        <thead>
                                            <tr>
                                                <th>Customer</th>
                                                <th>Items</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-right">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{-- @foreach(Cart::myorders() as $order)
                                            <tr>
                                                <td class="nowrap">{{$order->name}}</td>
                                                <td>
                                                    <div class="cell-image-list">
                                                        @foreach($order->detail as $detail)
                                                        @if((!empty($detail->menu)))
                                                              <div class="cell-img" style="background-image: url({{url($detail->menu->defaultImage('image'))}})"></div>
                                                       @endif
                                                        @endforeach
                                                    </div>
                                                </td>
                                                <td class="text-center">
                                                   {{$order->order_status}}
                                                </td>
                                                <td class="text-right">${{$order->total}}</td>
                                            </tr>
                                            @endforeach --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        @if(user()->ESign != 'Yes')
                        <div class=""><a class="btn btn-theme" style="width: 100px;" href="https://www.esigngenie.com/esign/onlineforms/fillOnlineForm?encformnumber=5WJyupgJZVPhGC8pY8x5VQ%3D%3D&type=link" target="iframe_a" onclick="esign();"> ESign</a></div>
                         @endif
                    </div>
                    <div class="col-md-6">
                        <div class="element-wrapper">
                            <h6 class="element-header">Menu Item Stats</h6>
                            <div class="element-box">
                                {{-- @foreach(Restaurant::showCategory(user()->id) as $category)
                                <div class="os-progress-bar primary">
                                    <div class="bar-labels">
                                        <div class="bar-label-left"><span class="bigger">{{$category->name}}</span></div>
                                        <div class="bar-label-right"><span class="info">{{$category->menu->count()}} Menu Items</span></div>
                                    </div>
                                    <div class="bar-level-1" style="width: 100%">
                                        <div class="bar-level-2" style="width: 70%"><div class="bar-level-3" style="width: 40%"></div></div>
                                    </div>
                                </div>
                                @endforeach --}}
                            </div>
                        </div>
                    </div>
                    
                 </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
$site = @$_SERVER['HTTP_REFERER'];
$url = url('restaurant/register');
if($site == $url){
    $var = 'yes';
}else{
    $var = 'no';
}
?>
<div class="modal fade welcome-modal" id="welcomModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close icon ion-close" data-dismiss="modal" aria-label="Close"></button>
            <div class="modal-header">
                <h2>Congratulations!<span>You are now a Zing Partner.</span></h2>
            </div>
            <div class="modal-body">
                <p class="mb-20">One of our Zing specialists will be contacting you to walk you through your dashboard and finish setting up your account.</p>
                <p>Feel free to explore your dashboard as you have full access If you want to go live without a Zing specialist you will need to send an email request to <a href="mailto:customersupport@zingmyorder.com">customersupport@zingmyorder.com</a> or your Zing specialist will help you with this.</p>
                <button type="button" class="btn btn-theme" data-dismiss="modal" style="min-width: 180px;">Go to Dasboard</button>
            </div>
        </div>
    </div>
</div>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #333;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #40b659;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<script type="text/javascript">
    function esign() {
        $('#test').show();
        $('#dashboard').hide();
    }
    // $("input").change(function() {
    //     if ($("#delivery_status").attr("checked")) {
    //         $('#delivery_status').removeAttr('checked');
    //         var delivery=$('#delivery_status').val('No');
    //     }
    //     else {
    //         var delivery=$('#delivery_status').val('Yes');
    //         $('#delivery_status').attr('checked','checked');
    //     }
    //     var status=$('#delivery_status').val(); console.log(status);
    //     $.ajax({
    //         type: "GET",
    //         url: "{{guard_url('restaurant/delivery-status?rest_id='.@user()->getRouteKey())}}"+'&&status='+status,
    //          // serializes the form's elements.
            
    //         });

    //     });
</script>
<script type="text/javascript">
    $(window).on('load',function(){

        if('{{$var}}' == 'yes')
        $('#welcomModal').modal('show')
    });
</script>