<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ Theme::getMetaTitle() }} | ZingMyOrder</title>
        <meta name="description" content="{{ Theme::getMetaDesctiption() }}">
        <meta name="keyword" content="{{ Theme::getMetaKeyword() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon" />
        <link rel="apple-touch-icon" href="{{url('apple-touch-icon.png')}}" />
        <link rel="apple-touch-icon" sizes="57x57" href="{{url('apple-touch-icon-57x57.png')}}" />
        <link rel="apple-touch-icon" sizes="72x72" href="{{url('apple-touch-icon-72x72.png')}}" />
        <link rel="apple-touch-icon" sizes="76x76" href="{{url('apple-touch-icon-76x76.png')}}" />
        <link rel="apple-touch-icon" sizes="114x114" href="{{url('apple-touch-icon-114x114.png')}}" />
        <link rel="apple-touch-icon" sizes="120x120" href="{{url('apple-touch-icon-120x120.png')}}" />
        <link rel="apple-touch-icon" sizes="144x144" href="{{url('apple-touch-icon-144x144.png')}}" />
        <link rel="apple-touch-icon" sizes="152x152" href="{{url('apple-touch-icon-152x152.png')}}" />
        <link rel="apple-touch-icon" sizes="180x180" href="{{url('apple-touch-icon-180x180.png')}}" />
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style type="text/css">
        html {
            display: table;
            height: 100%;
            width: 100%;
        }
        </style>
        {!! Theme::asset()->styles() !!}
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        {!! Theme::asset()->scripts() !!}
    </head>
    <body class="auth">
        <div class="main-wrap auth-wrap">
        {!! Theme::content() !!}
        </div>
        {!! Theme::asset()->container('footer')->scripts() !!}
    </body>
</html>
