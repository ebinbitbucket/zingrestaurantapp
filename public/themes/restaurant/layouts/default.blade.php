<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ Theme::getMetaTitle() }} | ZingMyOrder</title>
        <meta name="description" content="{{ Theme::getMetaDesctiption() }}">
        <meta name="keyword" content="{{ Theme::getMetaKeyword() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{url('favicon.ico')}}" type="image/x-icon" />
        <link rel="apple-touch-icon" href="{{url('apple-touch-icon.png')}}" />
        <link rel="apple-touch-icon" sizes="57x57" href="{{url('apple-touch-icon-57x57.png')}}" />
        <link rel="apple-touch-icon" sizes="72x72" href="{{url('apple-touch-icon-72x72.png')}}" />
        <link rel="apple-touch-icon" sizes="76x76" href="{{url('apple-touch-icon-76x76.png')}}" />
        <link rel="apple-touch-icon" sizes="114x114" href="{{url('apple-touch-icon-114x114.png')}}" />
        <link rel="apple-touch-icon" sizes="120x120" href="{{url('apple-touch-icon-120x120.png')}}" />
        <link rel="apple-touch-icon" sizes="144x144" href="{{url('apple-touch-icon-144x144.png')}}" />
        <link rel="apple-touch-icon" sizes="152x152" href="{{url('apple-touch-icon-152x152.png')}}" />
        <link rel="apple-touch-icon" sizes="180x180" href="{{url('apple-touch-icon-180x180.png')}}" />
       
        <link href="https://fonts.googleapis.com/css2?family=Kumbh+Sans:wght@300;400;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{theme_asset('css/app.css')}}">
        <link rel="stylesheet" href="{{theme_asset('css/main.css')}}">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="{{theme_asset('js/app.js')}}"></script>


    </head>

    <body>
        <!--[if lte IE 9]>
          <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <div class="app-wrap dashboard">
            {!! Theme::content() !!}
        </div>
    </body>
    
    <script src="{{theme_asset('js/main.js')}}"></script>
</html>
