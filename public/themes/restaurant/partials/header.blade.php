 <header class="main-header">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="container position-relative">
                        <a class="navbar-brand" href="{{url('/')}}">
                            <img src="{{url('assets/img/logo-round-big.png')}}" alt="logo" title="Zing My Order">
                        </a>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <!-- <div class="header-search-wrap">
                                <div class="search-wrap-inner">
                                    <div class="inner-item location">
                                        <i class="flaticon-pin"></i>
                                        <input type="text" class="form-control" id="location" placeholder="Delivery Address" value="New York">
                                    </div>
                                    <div class="inner-item category">
                                        <i class="flaticon-breakfast"></i>
                                        <select name="category" id="category" class="selectize">
                                            <option value="chinese" selected>Chinese</option>
                                            <option value="indian">Indian</option>
                                            <option value="italian">Italian</option>
                                            <option value="american">American</option>
                                            <option value="beer">Beer & Wine</option>
                                            <option value="juices">Juices</option>
                                            <option value="burgars">Burgars</option>
                                            <option value="coffee">Coffee</option>
                                        </select>
                                    </div>
                                    <div class="inner-item price">
                                        <span>Price:</span>
                                        <select id="price-pill" name="rating" autocomplete="off">
                                            <option value="1">$</option>
                                            <option value="2">$$</option>
                                            <option value="3">$$$</option>
                                            <option value="4">$$$$</option>
                                            <option value="5">$$$$$</option>
                                        </select>
                                    </div>
                                    <div class="inner-item rating">
                                        <span>Rating:</span>
                                        <select id="rating-pill" name="rating" autocomplete="off">
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-theme"><i class="flaticon-search"></i></button>
                            </div> -->
                            <ul class="navbar-nav ml-auto">
                                <!-- <li class="nav-item active">
                                    <a class="nav-link"  href="#signIn_signUp_Modal" data-toggle="modal" data-target="#signIn_signUp_Modal">Sign In</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn btn-secondary"  href="#signIn_signUp_Modal" data-toggle="modal" data-target="#signIn_signUp_Modal">Sign Up</a>
                                </li> -->
                            </ul>
                        </div>
                        <div class="nav-btn-wrap">
                            <div class="user-wrap">
                                <div class="dropdown">
                                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <h3><span>Welcome !</span>{{Auth::user()->name}}</h3>
                                        <img src="{{url(Auth::user()->defaultImage('logo'))}}" alt="">
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <div class="dropdown-item short-info" >
                                            <img class="avatar" src="{{url(Auth::user()->defaultImage('logo'))}}" alt="">
                                            <span class="user-info">
                                               <a href="{{guard_url('/')}}"> <h4 class="user-info-name">{{Auth::user()->name}}</h4>
                                                <p class="user-info-email">{{Auth::user()->email}}</p></a>
                                                <a href="{{guard_url('restaurant/accounts')}}"><span class="user-info-preferences">Account &amp; Preferences</span></a>
                                            </span>
                                        </div>
                                        <a class="dropdown-item" href="{{guard_url('restaurant/billing')}}">Account & Billing</a>
                                        <a class="dropdown-item" href="{{guard_url('cart/restaurant/orders')}}">Orders</a>
                                        <!-- <a class="dropdown-item" href="#">Notification</a>
                                        <a class="dropdown-item" href="#">Invoices</a> -->
                                        <div class="menu-footer">
                                            <a href="{{guard_url('password')}}">Reset Password</a>
                                            <a href="{{guard_url('logout')}}">Sign Out</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="main-nav-toggler">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                        
                    </div>
                </nav>
            </header>
            <div class="modal fade login-modal" id="signIn_signUp_Modal" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="login-wrap">
                                <div class="sign-in-wrap">
                                    <div class="wrap-inner">
                                            {!!Form::vertical_open()
            ->id('login')
            ->method('POST')
            ->action('client/login')!!}
                                            <div class="login-header text-center">
                                                <h2>Sign in</h2>
                                                <p>New to Zing My Order? <a href="#">Sign Up</a></p>
                                            </div>
                                            <div class="social-btn-wrap">
                                                <button type="button" class="btn btn-block btn-facebook"><i class="fa fa-facebook"></i> Sign in with Facebook</button>
                                                <button type="button" class="btn btn-block btn-google"><i class="fa fa-google"></i> Sign in with Facebook</button>
                                                <h3>OR<br><span class="login">Log in Using</span></h3>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <i class="flaticon-user"></i>
                                              {!! Form::email('email')
                ->required()
                ->raw() !!}
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <a href="#" class="forgot_pw">Forgot your password?</a>
                                                <i class="flaticon-key"></i>
                                               {!! Form::password('password')
                ->required()->raw()!!}
                                            </div>
                                            <div class="text-center login-footer">
                                                <button type="submit" class="btn btn-theme">Sign In</button>
                                            </div>
                                          {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>