<footer class="main-footer">
                <div class="footer-wdget-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="about-widget">
                                    <a href="index.html" class="logo"><img src="{{url('assets/img/logo.png')}}" alt=""></a>
                                   <!--  <p>Mauris sed mollis sapien. In posuere, lectus sit amet pulvinar ullamcorper.</p> -->
                                </div>
                                <div class="footer-social">
                                    <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a>
                                    <a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a>
                                    <a href="https://www.instagram.com" target="_blank"><i class="fa fa-instagram"></i></a>
                                    <a href="https://www.youtube.com/" target="_blank"><i class="fa fa-youtube"></i></a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget">
                                    <h5 class="widget-title">Get to Know Us</h5>
                                    <ul class="footer-links">
                                        <li><a href="{{url('about-us.html')}}">About Us</a></li>
                                       <!--  <li><a href="#">Careers</a></li>
                                        <li><a href="#">Blogs</a></li> -->
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget">
                                    <h5 class="widget-title">Let Us Help You</h5>
                                    <ul class="footer-links">
                                        <li><a href="{{guard_url('restaurant/accounts')}}">Account Details</a></li>
                                        <li><a href="{{guard_url('cart/restaurant/orders')}}">Order History</a></li>
<!--                                         <li><a href="#">Help</a></li>
 -->                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="widget">
                                    <h5 class="widget-title">Zing Network</h5>
                                    <ul class="footer-links">
                                        <li><a href="{{url('client/register')}}">Create User Account</a></li>
                                        <li><a href="{{url('restaurant/login')}}">Become a Zing Partner</a></li>
                                    </ul>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-privacy-payments">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('privacy.html')}}">Privacy</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{url('terms-and-conditions.html')}}">Terms & Conditions</a>
                                    </li>
                                   <!--  <li class="nav-item">
                                        <a class="nav-link" href="{{url('restaurants/mapview')}}">Sitemap</a>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="paymants-list text-md-right">
                                    <img src="img/credit-cards.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-copy">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <p>Copyrignts &copy; 2019 Zing My Order. All Rights Reserved</p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-150640780-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-150640780-1');
</script>                    