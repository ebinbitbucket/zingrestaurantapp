<div class="app-nav">
    <nav class="navbar nav flex-column">
        <a class="navbar-brand m-0 p-0" href="{{guard_url('/')}}">
            <img src="{{theme_asset('img/logo/logo.svg')}}" class="img-fluid" alt="">
        </a>
        <a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Notifications"><i class="las la-bell"></i></a>
        <a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Tasks"><i class="las la-list"></i></a>
        <a class="nav-link" href="#" data-toggle="tooltip" data-placement="right" title="" data-original-title="Projects"><i class="lab la-product-hunt"></i></a>

    </nav>
    <nav class="navbar nav flex-column user-nav-wrap">
        <div class="dropdown user-dropdown dropright">
            <button class="btn dropdown-toggle" type="button" id="user_dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">S</button>
            <div class="dropdown-menu" aria-labelledby="user_dropdown">
                <a class="dropdown-item user-info" href="{{guard_url('profile')}}">
                    <span class="user-avatar">S</span>
                    <span class="user-ac-info">
                        <h4>{{users('name')}}</h4>
                        <p>View Profile</p>
                    </span>
                </a>
                <div class="dropdown-divider"></div>
                <!-- <a class="dropdown-item" href="{{guard_url('user/user')}}"><i class="las la-users"></i>Users</a> -->
                <div class="dropdown has-child">
                                <a class="dropdown-item has-child" data-toggle="collapse" href="#userCollapse" role="button" aria-expanded="true" aria-controls="userCollapse"><i class="las la-user-circle"></i>Users</a>
                                <div class="collapse show" id="userCollapse" style="">
                                    <div class="dropdown-inner">
                                        <a class="dropdown-item" href="{{guard_url('roles/role')}}"><i class="las la-lock-open"></i>Roles &amp; Permissions</a>
                                        <a class="dropdown-item" href="{{guard_url('user/user')}}"><i class="las la-users"></i>Users</a>
                                        <a class="dropdown-item" href="{{guard_url('user/client')}}"><i class="las la-user-tie"></i>
                                        Clients</a>
                                        <a class="dropdown-item" href="{{guard_url('user/team')}}"><i class="las la-users"></i>
                                        Teams</a>
                                    </div>
                                </div>
                            </div>
                <a class="dropdown-item" href="{{guard_url('menu/menu')}}"><i class="las la-bars"></i>Menu</a>
                <a class="dropdown-item" href="{{guard_url('contact/contact')}}"><i class="las la-users"></i>Contact</a>
                <a class="dropdown-item" href="{{guard_url('settings')}}"><i class="las la-cog"></i>Settings</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{guard_url('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="las la-sign-out-alt"></i>Sign Out
                </a>
                <form id="logout-form" action="{{guard_url('logout')}}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </nav>
</div>