<div class="main-wrap auth-wrap">
    <section class="window-height section-block container-fluid">
        <div class="media-column col-md-7">
            <div class="bkg-img window-height" style="background-image: url('{{theme_asset('img/user-reset.jpg')}}');"></div>
        </div>
        <div class="row">
            <div class="col-md-4 column">
                <div class="split-hero-content">
                    <div class="hero-content-inner">
                        <a href="{{url('/')}}"><img src="{{theme_asset('img/logo-round.png')}}" class="img-responsive logo" alt=""></a>
                        <h3>Forgot Password</h3>
                        @include('notifications')
                        <div class="login-form-container">
                            {!!Form::vertical_open()
                            ->id('reset')
                            ->method('POST')
                            ->action(guard_url('password'))
                            !!}
                            {!! csrf_field() !!}
                            {!! Form::hidden('token')->value($token) !!}
                                <div class="form-group">
                                    <label for="name">Username</label>
                                    {!! Form::email('email','')->raw()!!}
                                    <i class="flaticon-user"></i>
                                </div>
                               <!--  <div class="form-group">
                                    <label for="password">Old Password</label>
                                    {!! Form::password('old_password', '')->raw()!!}
                                    <i class="flaticon-key"></i>
                                </div> -->
                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    {!! Form::password('password', '')->raw()!!}
                                    <i class="flaticon-key"></i>
                                </div>
                                <div class="form-group">
                                    <label for="password">Confirm Password</label>
                                    {!! Form::password('password_confirmation', '')->raw()!!}
                                    <i class="flaticon-key"></i>
                                </div>
                               <div class="form-group">
                                    <button class="btn btn-theme" type="submit">Update Password</button>
                                </div>
                            {!! Form::close() !!}
<!--                             <p class="text-small mt20">Back to <a href="{{guard_url('login')}}">&nbsp; Login</a></p>
 -->                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
</div>