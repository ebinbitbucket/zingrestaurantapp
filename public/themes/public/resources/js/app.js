
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const app = new Vue({
//     el: '#app'
// });
require('popper.js/dist/umd/popper.min.js');
require('bootstrap/dist/js/bootstrap.min.js');
require('toastr/toastr.js');
require('sweetalert/dist/sweetalert.min.js');
require('jquery-validation/dist/jquery.validate.js' );
require('slick-carousel/slick/slick.min.js');
require('jquery-bar-rating/dist/jquery.barrating.min.js');
require('../../assets/packages/selectize/selectize.min.js');
require('fotorama/fotorama.js');
require('lightgallery/dist/js/lightgallery.js');
require('lightgallery/dist/js/lightgallery.min.js');
require('../../assets/packages/cubeportfolio/cubeportfolio.js');
