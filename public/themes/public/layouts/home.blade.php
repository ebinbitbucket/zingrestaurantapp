<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ Theme::getMetaTitle() }}</title>
    <meta name="description" content="{{ Theme::getMetaDescription() }}">
    <meta name="keywords" content="{{ Theme::getMetaKeyword() }}">
    <meta name="tags" content="{{ Theme::getMetaTag() }}">
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />

    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;600;700&display=swap" rel="stylesheet">

    <link href="{{ url('themes/client/assets/css/default/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('themes/client/assets/css/default/fontawesome-all.min.css') }}" rel="stylesheet">
    {{--
    <link href="{{ url('themes/app/assets/css/default/main.css') }}" rel="stylesheet"> --}}
    <script src="{{ url('themes/client/assets/js/default/jquery.js') }}"></script>
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> --}}
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> --}}


</head>

<body class="theme-light">
    <div id="preloader">
        <div class="spinner-border color-highlight" role="status"></div>
    </div>

    {!! Theme::content('default') !!}

    {{-- Javscript will not work when we add bootstrap.min.js
    --}}
    <script src="{{ url('themes/client/assets/js/default/bootstrap.min.js') }}"></script>


    {{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> --}}

{{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script> --}}

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.2/owl.carousel.min.js"></script> --}}
    <script src="{{ url('themes/client/assets/js/default/custom.js') }}"></script>

{{-- <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"></script> --}}

</body>

</html>
