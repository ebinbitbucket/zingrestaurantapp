<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>  <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ Theme::getMetaTitle() }}</title>
        <meta name="description" content="{{ Theme::getMetaDescription() }}">
        <meta name="keywords" content="{{ Theme::getMetaKeyword() }}">
        <meta name="tags" content="{{ Theme::getMetaTag() }}">
        {{-- <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" /> --}}
        {{-- <meta name="viewport" content="width=500, initial-scale=1"> --}}
        {{-- <meta name="viewport" content="width=1024"> --}}
        {{-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> --}}
        <meta name="viewport" content="width=device-width, initial-scale=0.86, maximum-scale=3.0, minimum-scale=0.86">
        <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />
        <link rel="apple-touch-icon" href="apple-touch-icon.png" />
        
        <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;600;700&display=swap" rel="stylesheet">
       
        <link href="{{url('themes/app/assets/css/default/bootstrap.css')}}" rel="stylesheet">
        <link href="{{url('themes/app/assets/css/default/fontawesome-all.min.css')}}" rel="stylesheet">
        <link href="{{url('themes/app/assets/css/default/main.css')}}" rel="stylesheet">
        {{-- <script src="{{url('themes/app/assets/js/default/jquery.js')}}"></script> --}}
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    
    </head>
    {{-- <body  class="theme-light">
            <div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>

        {!! Theme::content('default') !!}

        <script src="{{url('themes/app/assets/js/default/bootstrap.min.js')}}"></script>
        <script src="{{url('themes/app/assets/js/default/custom.js')}}"></script>
    </body> --}}

    <body class="m-screen square-screen is-desktop">
        {!! Theme::content('default') !!}

        <!--[if lte IE 9]>
          <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        {{-- <div class="desktop-view">
        	<div class="wrap-a transitions-enabled">
        		<div class="wrap-b transitions-enabled">
                    <iframe id="preview-iframe" src="http://localhost/zingmyorder/app/638" class="wrap-c transitions-enabled">
                    
                    </iframe>
        		</div>
        	</div>
        </div> --}}

        {{-- <script src="{{url('themes/app/assets/js/default/bootstrap.min.js')}}"></script> --}}
        {{-- <script src="{{url('themes/app/assets/js/default/custom.js')}}"></script> --}}
    </body>
</html>


