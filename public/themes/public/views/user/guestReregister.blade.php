<div class="main-wrap auth-wrap">
    <section class="window-height section-block container-fluid">
        <div class="media-column col-md-7">
            <div class="bkg-img window-height" style="background-image: url('{{url('themes/client/assets/img/user-register.jpg')}}');"></div>
        </div>
        <div class="row">
            <div class="col-md-4 column">
                <div class="split-hero-content">
                    <div class="hero-content-inner">
                        <a href="{{url('/')}}"><img src="{{theme_asset('img/logo-round.png')}}" class="img-responsive logo" alt=""></a>
                        <h3>Register your account</h3>
                        <p class="mb20">Already have an account? <a href="{{url('client/login')}}">Click Here</a></p>
                        @include('notifications')
                        <div class="login-form-container">
                            {!!Form::vertical_open()
                            ->id('contact')
                            ->action(url('client/password'))
                            ->method('POST')
                            ->class('change-password')!!}
                            <div class="alert alert-warning" role="alert" id="message" style="display:none"><span id="error"></span></div>
                                <div class="form-group">
                                    {!! Form::text('name')
                                    ->readonly()
                                    ->id('name')
                                    ->placeholder('Name')
                                    ->value(@$user->name)
                                    ->raw() !!}
                                    <i class="ik ik-user"></i>
                                </div>

                                <div class="form-group">
                                    {!! Form::email('email')
                                    ->readonly()
                                    ->id('email')
                                    ->value(@$user->email)
                                    ->placeholder('Email')
                                    ->raw() !!}
                                    <i class="ik ik-mail"></i>
                                </div>

                                <div class="form-group">
                                    {!! Form::text('mobile')
                                    ->readonly()
                                    ->id('mobile')
                                    ->value(@$user->mobile)
                                    ->placeholder('Mobile')
                                    ->raw() !!}
                                    <i class="ik ik-phone-incoming"></i>
                                </div>
                                <input type="hidden" name="type" value="guest">
                                <div class="form-group">
                                    {!! Form::password('old_password')
                                    ->hidden()
                                    ->value('123456')
                                    -> label('')
                                    !!}
                                    </div>
                                    <div class="form-group">
                                    {!! Form::password('password')
                                    ->placeholder('Password')
                                    ->required()
                                    ->raw()
                                    !!}
                                    <i class="ik ik-lock"></i>
                                    </div>
                                    <div class="form-group">
                                    {!! Form::password('password_confirmation')
                                    ->required()
                                    ->raw()
                                    ->placeholder('Confirm Password')
                                    !!}
                                    <i class="ik ik-lock"></i>
                                    </div>
<!-- 
                                
                                <div class="form-group">
                                    <p>By clicking “Register Now,” you agree to <a href="{{url('terms-and-conditions.html')}}">Zing My Order General Terms and Conditions</a> and acknowledge you have read the <a href="{{url('privacy.html')}}">Privacy Policy</a>.</p>
                                </div> -->
                                <div class="form-group row">
                                    <button type="submit" class="btn btn-theme">Register</button>
                                    <a href="{{url('/')}}" class="btn btn-link" >Back</a></center>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
</div>
<style>
.main-wrap .main-header.checkout-header {
    display: none;
}
.main-wrap {
    padding-top: 0px;
}
.bottom-nav-mobile {
    display: none !important;
}
</style>
<script type="text/javascript">
    
</script>
