
<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <title>Casablanca Greek Mediterranean Restaurant</title>
    <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;600;700&display=swap" rel="stylesheet">


    <link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
</head>
<body class="theme-light">
    <div id="preloader"><div class="spinner-border color-highlight" role="status"></div></div>
    <div id="page">
        <div class="header header-auto-show header-fixed header-logo-center">
            <a href="index.html" class="header-title">
                <img src="images/logo.png" class="logo logo-color" style="height: 40px;" alt="">
                <img src="images/logo-w.png" class="logo logo-white" style="height: 40px;" alt="">
            </a>
            <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
            <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i class="fas fa-sun"></i></a>
            <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i class="fas fa-moon"></i></a>
        </div>
        <div id="footer-bar" class="footer-bar-6">
            <a href="menu.html"><i class="fa fa-utensils"></i><span>Menu</span></a>
            <a href="gallery.html"><i class="fa fa-images"></i><span>Gallery</span></a>
            <a href="index.html" class="circle-nav active-nav"><i class="fa fa-home"></i><span>Welcome</span></a>
            <a href="contact.html"><i class="fa fa-address-book"></i><span>Contacts</span></a>
            <a href="locations.html"><i class="fa fa-store"></i><span>Locations</span></a>
        </div>
        <div class="page-title page-title-fixed">
            <div>
                <h1>
                    <img src="images/logo.png" class="logo logo-color" style="height: 50px;" alt="">
                    <img src="images/logo-w.png" class="logo logo-white" style="height: 50px;" alt="">
                </h1>
            </div>
            <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i class="fa fa-moon"></i></a>
            <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i class="fa fa-lightbulb color-yellow-dark"></i></a>
            <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i class="fa fa-bars"></i></a>
        </div>
        <div class="page-title-clear"></div>
        <div class="page-content">
            <div class="content mt-0">
                <div class="single-slider owl-has-controls owl-carousel owl-no-dots">
                    <div data-card-height="200" class="card mb-0 rounded-m shadow-l" style="background-image: url('images/slide1.jpg')">
                        <div class="card-bottom text-center mb-3">
                            <h2 class="color-white font-900 mb-0">Welcome to Casablanca</h2>
                            <p class="under-heading color-white">Greek Mediterranean Cuisine Restaurant.</p>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                    <div data-card-height="200" class="card mb-0 rounded-m shadow-l" style="background-image: url('images/slide2.jpg')">
                        <div class="card-bottom text-center mb-3">
                            <h2 class="color-white font-900 mb-0">Delicious Food</h2>
                            <p class="under-heading color-white">Flexibility, Speed, Ease of Use.</p>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                    <div data-card-height="200" class="card mb-0 rounded-m shadow-l" style="background-image: url('images/slide3.jpg')">
                        <div class="card-bottom text-center mb-3">
                            <h2 class="color-white font-900 mb-0">Free Home Delivery</h2>
                            <p class="under-heading color-white">Almost before we knew it, we had left the ground.</p>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                </div>
            </div>
            <div class="content mt-0 mb-15">
                <h2 class="mb-4">Featured Items</h2>
            </div>
            <div class="single-center-slider owl-carousel owl-no-dots mb-4">
                <div>
                    <div class="card m-0 card-style" style="background-image: url('images/menu/popular1.png')" data-card-height="180">
                        <div class="card-top">
                            <a href="#" class="btn btn-xs bg-red-dark rounded-sm font-700 float-right mt-2 mr-2">Best Seller</a>
                        </div>
                        <div class="card-bottom">
                            <h3 class="color-white font-800 mb-3 pb-1 pl-3">$600</h3>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                    <p class="mb-0 mt-2 color-highlight font-600">Vegetarian • Indian • Pure veg</p>
                    <h4>Cheese corn Roll</h4>
                </div>
                <div>
                    <div class="card m-0 card-style" style="background-image: url('images/menu/popular2.png')" data-card-height="180">
                        <div class="card-top">
                            <a href="#" class="btn btn-xs bg-green-dark rounded-sm font-700 float-right mt-2 mr-2">Pure Veg</a>
                        </div>
                        <div class="card-bottom">
                            <h3 class="color-white font-800 mb-3 pb-1 pl-3">$320</h3>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                    <p class="mb-0 mt-2 color-highlight font-600">Vegetarian • Indian • Pure veg</p>
                    <h4>Mashed Potatoes</h4>
                </div>
                <div>
                    <div class="card m-0 card-style" style="background-image: url('images/menu/popular3.png')" data-card-height="180">
                        <div class="card-bottom">
                            <h3 class="color-white font-800 mb-3 pb-1 pl-3">$320</h3>
                        </div>
                        <div class="card-overlay bg-gradient"></div>
                    </div>
                    <p class="mb-0 mt-2 color-highlight font-600">Vegetarian • Indian • Pure veg</p>
                    <h4>Chicken Tikka Sub</h4>
                </div>

            </div>
            <div class="card card-style">
                <div class="content mb-0">
                    <p class="mb-n1 font-600 color-highlight">Handpicked by our Chef</p>
                    <h2 class="mb-4">Top Sellers</h2>
                    <div class="row mb-0">
                        <div class="col-5 pr-0">
                            <a href="#"><img src="images/menu/popular1.png" class="rounded-sm shadow-xl img-fluid"></a>
                        </div>
                        <div class="col-7">
                            <a href="#">
                                <h5 class="mb-0">Chicken pot pie</h5>
                                <span class="color-green-dark font-14">Veg</span>
                            </a>
                            <h4 class="mt-1 mb-n2 font-800">$100</h4>
                        </div>
                    </div>
                    <div class="row mb-0">
                        <div class="w-100 divider divider-margins mb-3 mt-3"></div>
                    </div>
                    <div class="row mb-0">
                        <div class="col-5 pr-0">
                            <a href="#"><img src="images/menu/popular2.png" class="rounded-sm shadow-xl img-fluid"></a>
                        </div>
                        <div class="col-7">
                            <a href="#">
                                <h5 class="mb-0">Burger Sliders</h5>
                                <span class="color-red-dark font-14">Non Veg</span>
                            </a>
                            <h4 class="mt-1 mb-n2 font-800">$150</h4>
                        </div>
                    </div>
                    <div class="row mb-0">
                        <div class="w-100 divider divider-margins mb-3 mt-3"></div>
                    </div>
                    <div class="row mb-0">
                        <div class="col-5 pr-0">
                            <a href="#"><img src="images/menu/popular3.png" class="rounded-sm shadow-xl img-fluid"></a>
                        </div>
                        <div class="col-7">
                            <a href="#">
                                <h5 class="mb-0">Chicken pot pie</h5>
                                <span class="color-green-dark font-14">Veg</span>
                            </a>
                            <h4 class="mt-1 mb-n2 font-800">$100</h4>
                        </div>
                    </div>
                    <div class="row mb-0">
                        <div class="w-100 divider divider-margins mb-3 mt-3"></div>
                    </div>
                    <div class="row mb-0">
                        <div class="col-5 pr-0">
                            <a href="#"><img src="images/menu/popular4.png" class="rounded-sm shadow-xl img-fluid"></a>
                        </div>
                        <div class="col-7">
                            <a href="#">
                                <h5 class="mb-0">Burger Sliders</h5>
                                <span class="color-red-dark font-14">Non Veg</span>
                            </a>
                            <h4 class="mt-1 mb-n2 font-800">$150</h4>
                        </div>
                    </div>
                    <a href="#" class="btn btn-full btn-sm rounded-s mb-3 font-600 bg-highlight mt-3">View All Menus</a>
                </div>

            </div>
        </div>

    

        <div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
            <div class="card rounded-0" style="background-image: url('images/menu-bg.jpg'); height:200px">
                <div class="card-top">
                    <a href="#" class="close-menu float-right mr-2 text-center mt-3 icon-40 notch-clear"><i class="fa fa-times color-white"></i></a>
                </div>
                <div class="card-bottom">
                    <h1 class="color-white pl-3 mb-2 font-28"><img src="images/logo-w.png" style="height: 60px;" alt=""></h1>
                </div>
                <div class="card-overlay bg-gradient"></div>
            </div>
            <div class="mt-4"></div>
            <h6 class="menu-divider">Menu</h6>
            <div class="list-group list-custom-small list-menu">
                <a id="nav-welcome" href="index.html">
                    <i class="fa fa-home gradient-orange color-white"></i>
                    <span>Home</span>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a id="nav-welcome" href="menu.html">
                    <i class="fa fa-utensils gradient-red color-white"></i>
                    <span>Menu</span>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a id="nav-homepages" href="gallery.html">
                    <i class="fa fa-images gradient-green color-white"></i>
                    <span>Gallery</span>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a id="nav-components" href="contact.html">
                    <i class="fa fa-address-book gradient-yellow color-white"></i>
                    <span>Contact</span>
                    <i class="fa fa-angle-right"></i>
                </a>
                <a id="nav-components" href="locations.html">
                    <i class="fa fa-store gradient-blue color-white"></i>
                    <span>Locations</span>
                    <i class="fa fa-angle-right"></i>
                </a>
            </div>
            <h6 class="menu-divider mt-4">Settings</h6>
            <div class="list-group list-custom-small list-menu">
                <a href="#" data-toggle-theme data-trigger-switch="switch-dark-mode">
                    <i class="fa fa-moon gradient-dark color-white"></i>
                    <span>Dark Mode</span>
                    <div class="custom-control scale-switch ios-switch highlight-switch switch-s">
                    <input type="checkbox" class="ios-input" id="switch-22">
                    <label class="custom-control-label" for="switch-1"></label>
                    </div>
                </a>
                <a href="#" class="btn btn-full btn-sm rounded-s ml-3 mb-3 font-600 bg-highlight mt-3">Order Online</a>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="scripts/jquery.js"></script>
    <script type="text/javascript" src="scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="scripts/custom.js"></script>
</body>
