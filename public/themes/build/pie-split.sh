#
# This will split up each Lavalite theme to its own github repo
#

./git-subsplit.sh init git@github.com:lavalite/themes.git
./git-subsplit.sh publish  --heads="develop master" --tags="v5.1.4 v5.1.5" admin:git@github.com:Litecms/Admin.git
./git-subsplit.sh publish  --heads="develop master" --tags="v5.1.4 v5.1.5"  public:git@github.com:Litecms/Public.git
./git-subsplit.sh publish  --heads="develop master" --tags="v5.1.4 v5.1.5"  client:git@github.com:Litecms/Client.git
rm -rf .subsplit/