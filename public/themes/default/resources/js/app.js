require('./scripts/modernizr-3.6.0.min.js');
require('bootstrap/dist/js/bootstrap.bundle.js');
require('timeago/jquery.timeago.js');
require('jquery-validation/dist/jquery.validate.js');

window.Sortable = require('sortablejs/Sortable.js');
window.toastr = require('toastr/toastr.js');
window.moment = require('moment/moment.js');
window.Mustache = require('mustache/mustache.js');
require('nestable/jquery.nestable.js');
require('summernote/dist/summernote-bs4.js');
window.swal = require('sweetalert/dist/sweetalert.min.js');
window.Dropzone = require('dropzone/dist/dropzone.js');
require('jquery-validation/dist/jquery.validate.js');
require('timeago/jquery.timeago.js');
require('tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js');
require('daterangepicker/daterangepicker.js');
require('select2/dist/js/select2.full.js');
require('./scripts/Sortable.js');
