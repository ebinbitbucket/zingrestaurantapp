<div class="main-wrap auth-wrap">
    <section class="window-height section-block container-fluid">
        <div class="media-column col-md-7">
            <div class="bkg-img window-height" style="background-image: url('{{url('themes/client/assets/img/user-register.jpg')}}');"></div>
        </div>
        <div class="row">
            <div class="col-md-4 column">
                <div class="split-hero-content">
                    <div class="hero-content-inner">
                        <a href="{{url('/')}}"><img src="{{theme_asset('img/logo-round.png')}}" class="img-responsive logo" alt=""></a>
                        <h3>Checkout as Guest</h3>
                        <p class="mb20">Already have an account? <a href="/client/login">Click Here</a></p>
                        @include('notifications')
                        <div class="login-form-container">
                            {!!Form::vertical_open()
                            ->id('register')
                             ->action(url('client/register'))
                            ->method('POST')!!}
                            <div class="alert alert-warning" role="alert" id="message" style="display:none"><span id="error"></span></div>
                                <div class="form-group">
                                    {!! Form::text('name')
                                                                ->required()
                                                                ->id('name')
                                                                ->placeholder('Name')
                                                                ->raw() !!}
                                    <i class="flaticon-user"></i>
                                </div>

                                <div class="form-group">
                                    {!! Form::email('email')
                                                                ->required()
                                                                ->id('email')
                                                                ->placeholder('Email')
                                                                ->raw() !!}
                                    <i class="flaticon-mail"></i>
                                </div>

                                <div class="form-group">
                                    {!! Form::text('mobile')
                                                                ->required()
                                                                ->id('mobile')
                                                                ->placeholder('Mobile')
                                                                ->raw() !!}
                                    <i class="flaticon-phone-call"></i>
                                </div>
                                 <input type="hidden" value="true" name="guest">
<!-- 
                                
                                <div class="form-group">
                                    <p>By clicking “Register Now,” you agree to <a href="{{url('terms-and-conditions.html')}}">Zing My Order General Terms and Conditions</a> and acknowledge you have read the <a href="{{url('privacy.html')}}">Privacy Policy</a>.</p>
                                </div> -->
                                <div class="form-group mt-20 row">
                                    <button type="button" id="register_submit" class="btn btn-theme">Guest Checkout</button>
                                    <a href="javascript:history.back()" class="btn btn-link" >Back</a></center>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
</div>
<style>
.main-wrap .main-header.checkout-header {
    display: none;
}
.main-wrap {
    padding-top: 0px;
}
.bottom-nav-mobile {
    display: none !important;
}
</style>
<script type="text/javascript">
    $(document).on('click','#register_submit',function(){ 
            var $f = $('#register');
            $.getJSON({
                type: 'POST',
                url: $f.attr('action'),
                data: $f.serialize(),
                success: function(data) {
                    console.log('hi');
                     $("#time_block").show();
                    $("#delivery_address_block").show();
                    $("#payment_block").show();
                   window.location.href= "{{url('cart/checkout')}}";

                },
                error: function(msg) {
                    console.log(msg.responseJSON.errors);
                    $('#message').fadeIn(function(){
                    s='';
                    items = msg.responseJSON.errors;
                    Object.keys(items).forEach(function(key){
                            if(items[key][0] == 'The email has already been taken.'){
                                s=s+'This email was registered with a Zing Account. Use another email to continue as Guest or use Zing Login to continue with your credentials'+'<br/>';
                            }
                            else{
                                s=s+items[key][0]+'<br/>';
                            }
                            

                        }); 
                    document.getElementById("error").innerHTML =

                            '<p>'+s+'</p>';

                    });
                }
            });
    });
</script>