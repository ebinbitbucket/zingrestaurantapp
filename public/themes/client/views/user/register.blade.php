   {!!Form::vertical_open()
    ->id('register')
    ->action(url('client/register'))
    ->method('POST')!!}
                 <!-- <div style="display:none" id="message"><h6><font color="#856404 " id="error">Error</font></h6></div> -->
                 <div class="alert alert-warning" role="alert" id="message" style="display:none"><span id="error"></span></div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
<!--                                 <label for="email">Enter Name</label>
 -->                                {!! Form::text('name')
                                ->required()
                                ->id('name')
                                ->placeholder('Name')
                                ->raw() !!}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
<!--                                 <label for="email">Enter Email Id</label>
 -->                                {!! Form::email('email')
                                ->required()
                                ->id('email')
                                ->placeholder('Email')
                                ->raw() !!}
                            </div>
                        </div>
                         <div class="col-sm-12">
                            <div class="form-group">
<!--                                 <label for="email">Mobile</label>
 -->                                {!! Form::text('mobile')
                                ->required()
                                ->id('mobile')
                                ->placeholder('Mobile')
                                ->raw() !!}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
<!--                                 <label for="password">Password</label>
 -->                                {!! Form::password('password')
                                ->placeholder('Password')
                                ->id('password')
                                ->required()->raw()!!}
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
<!--                                 <label for="password">Confirm Password</label>
 -->                                {!! Form::password('password_confirmation')
                                ->placeholder('Confirm Password')
                                ->id('password_confirmation')
                                ->required()->raw()!!}
                            </div>
                        </div>
                    </div>
                </div>
                <input class="form-control" type="hidden" name="_token" value="IVThaETufkSwK74Zsp5tVZqklgCh2Wcz51ccgqo3">
                <div class="col-md-12">
<!--                     <button type="button" id="register_submit" class="btn btn-theme btn-block btn-xs">Register</button>
 -->                    <center><button type="button" id="register_submit" class="btn btn-theme mr-10" style="min-width: 100px;">Register</button>
                        <!-- <button class="btn btn-theme mr-10" style="min-width: 100px;" id="login_submit">Login</button> -->
                                    <a href="{{url('cart/checkout')}}" class="btn btn-link" id="collapseLogin">Login</a></center>
<!--                                     <a href="#" class="btn btn-theme" id="collapseGuest" style="margin-top: 10px;width: 100%;background-color: #999;">GUEST CHECKOUT</a>
 -->                </div>


        
    {!! Form::close() !!}