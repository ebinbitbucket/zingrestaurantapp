
                               {!!Form::vertical_open()
    ->id('register')
    ->action(url('client/register'))
    ->method('POST')!!}                   <div class="col-md-12">
                                                   <div class="alert alert-warning" role="alert" id="message" style="display:none"><span id="error"></span></div>
                                               </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                {!! Form::text('name')
                                                                ->required()
                                                                ->id('name')
                                                                ->placeholder('Name')
                                                                ->raw() !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                {!! Form::email('email')
                                                                ->required()
                                                                ->id('email')
                                                                ->placeholder('Email')
                                                                ->raw() !!}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="form-group">
                                                                {!! Form::text('mobile')
                                                                ->required()
                                                                ->id('mobile')
                                                                ->placeholder('Mobile')
                                                                ->raw() !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input class="form-control" type="hidden" name="_token" value="IVThaETufkSwK74Zsp5tVZqklgCh2Wcz51ccgqo3">
                                                 <input class="form-control" type="hidden" name="guest" value="true">
                                                <div class="col-md-12">
                                                    <center><button type="button" id="register_submit" class="btn btn-theme mr-10" style="min-width: 100px;background-color: #999;">GUEST CHECKOUT</button>
                        <!-- <button class="btn btn-theme mr-10" style="min-width: 100px;" id="login_submit">Login</button> -->
                                    <a href="#" class="btn btn-link" id="collapseRegister">Sign up</a></center>
<!--                                     <a href="{{url('cart/checkout')}}" class="btn btn-theme" id="collapseLogin" style="margin-top: 10px;width: 100%;background-color: #999;">Login</a>
 -->                                                </div>
                                           
                                    {!! Form::close() !!}
