<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="" class="header-title"><img src="{{ theme_asset('img/logo.png') }}" style="height: 30px;" alt=""></a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>

    <div class="page-content pb-0">
        <div class="card rounded-0" data-card-height="cover">
            <div class="card-center pl-3">
                <div class="text-center mb-3">
                    <h1 class="font-40 font-800 pb-2">
                        @if (!empty($restaurant->logo))
                            <img src="{{ url(@$restaurant->defaultImage('logo')) }}" style="height: 40px;" alt="">
                        @endif
                    </h1>
                </div>
                @if (Session::has('user_exists'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-danger text-left">
                                <span><b> Error - </b> {{ Session::get('user_exists') }}</span>
                            </div>
                        </div>
                    </div>

                    {{ Session::forget('user_exists') }}
                @endif
                @if (Session::has('validation_errors'))
                <div class="row">
                    @foreach (Session::get('validation_errors') as $val_errors)
                        @foreach ($val_errors as $validation)
                            <div class="col-md-12">
                                <div class="text-danger text-left">
                                    <span><b> Error - </b> {{ $validation }}</span>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
                {{ Session::forget('validation_errors') }}
                @endif

                {!! Form::vertical_open()
                ->id('register')
                ->method('POST')
                ->action(url('client/register')) !!}
                {{ csrf_field() }}

                <div class="ml-3 mr-4 mb-n3">
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-user"></i>
                        <span class="input-style-1-inactive">Name</span>
                        <em>(required)</em>
                        {!! Form::text('name')
                        ->required()
                        ->placeholder('Name')
                        ->raw() !!}
                    </div>
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-at"></i>
                        <span>Enter your Email</span>
                        <em>(required)</em>
                        {!! Form::email('email')
                        ->required()
                        ->placeholder('Email')
                        ->raw() !!}
                    </div>
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-phone"></i>
                        <span>Enter your Mobile</span>
                        <em>(required)</em>
                        {!! Form::text('mobile')
                        ->required()
                        ->placeholder('Mobile')
                        ->raw() !!}
                    </div>
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-lock"></i>
                        <span>Choose a Password</span>
                        <em>(required)</em>
                        {{-- <input type="password" placeholder="Choose a Password">
                        --}}
                        {!! Form::password('password')
                        ->required()
                        ->placeholder('Password')
                        ->raw() !!}
                    </div>
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-lock"></i>
                        <span>Confirm your Password</span>
                        <em>(required)</em>
                        {!! Form::password('password_confirmation')
                        ->required()
                        ->placeholder('Confirm Password')
                        ->raw() !!}
                        {!! Form::hidden('restaurant_id')->value(@$restaurant->id) !!}
                        {!! Form::hidden('is_app')->value(1) !!}
                    </div>
                    <div class="clearfix"></div>
                    <div class="row pt-3 mb-3">
                        <div class="col-6 text-left">
                        </div>
                        <div class="col-6 text-right">
                            <a href="{{ url('app/' . $restaurant->id) }}">Sign In Here</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <button class="btn btn-center-l bg-highlight rounded-sm btn-l font-13 font-600 mt-5 border-0"
                    type="submit">Create Account</button>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

</div>
