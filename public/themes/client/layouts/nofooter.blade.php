<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>{{ Theme::getMetaTitle() }} - ZingMyOrder</title>
        <meta name="keyword" content="{{ Theme::getMetaKeyword() }}">
        <meta name="description" content="{{ Theme::getMetaDescription() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="{{asset('apple-touch-icon.png')}}">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Rancho" rel="stylesheet">

        {!! Theme::asset()->styles() !!}

        {!! Theme::asset()->scripts() !!}
    </head>
    <body>
        <div class="main-wrap">
            {!!Restaurant::gadget('restaurant.gadget.mycart')!!}
             {!! Theme::partial('headercheckout') !!}
        {!! Theme::content() !!}
        {!! Theme::asset()->container('footer')->scripts() !!}
        {!! Theme::asset()->container('extra')->scripts() !!}
        </div>
       
    </body>
</html>
