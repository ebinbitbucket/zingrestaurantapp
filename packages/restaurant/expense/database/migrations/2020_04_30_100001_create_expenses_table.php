<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateExpensesTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: expenses
         */
        Schema::create('expenses', function ($table) {
            $table->increments('id');
            $table->integer('restaurant_id')->nullable();
            $table->text('title')->nullable();
            $table->float('amount')->nullable();
            $table->date('date')->nullable();
            $table->text('files')->nullable();
            $table->text('upload_folder')->nullable();
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('expenses');
    }
}
