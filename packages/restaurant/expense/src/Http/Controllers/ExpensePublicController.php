<?php

namespace Restaurant\Expense\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Expense\Interfaces\ExpenseRepositoryInterface;

class ExpensePublicController extends BaseController
{
    // use ExpenseWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Expense\Interfaces\ExpenseRepositoryInterface $expense
     *
     * @return type
     */
    public function __construct(ExpenseRepositoryInterface $expense)
    {
        $this->repository = $expense;
        parent::__construct();
    }

    /**
     * Show expense's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $expenses = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$expense::expense.names'))
            ->view('expense::expense.index')
            ->data(compact('expenses'))
            ->output();
    }


    /**
     * Show expense.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $expense = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$expense->name . trans('expense::expense.name'))
            ->view('expense::expense.show')
            ->data(compact('expense'))
            ->output();
    }

}
