<?php

namespace Restaurant\Expense\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Expense\Http\Requests\ExpenseRequest;
use Restaurant\Expense\Interfaces\ExpenseRepositoryInterface;
use Restaurant\Expense\Models\Expense;

/**
 * Resource controller class for expense.
 */
class ExpenseResourceController extends BaseController
{

    /**
     * Initialize expense resource controller.
     *
     * @param type ExpenseRepositoryInterface $expense
     *
     * @return null
     */
    public function __construct(ExpenseRepositoryInterface $expense)
    {
        parent::__construct();
        $this->repository = $expense;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Expense\Repositories\Criteria\ExpenseResourceCriteria::class);
    }

    /**
     * Display a list of expense.
     *
     * @return Response
     */
    public function index(ExpenseRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Expense\Repositories\Presenter\ExpensePresenter::class)
                ->$function();
        }

        $expenses = $this->repository->paginate(15);

        return $this->response->setMetaTitle(trans('expense::expense.names'))
            ->view('expense::expense.index', true)
            ->data(compact('expenses', 'view'))
            ->output();
    }

    /**
     * Display expense.
     *
     * @param Request $request
     * @param Model   $expense
     *
     * @return Response
     */
    public function show(ExpenseRequest $request, Expense $expense)
    {

        if ($expense->exists) {
            $view = 'expense::expense.show';
        } else {
            $view = 'expense::expense.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('expense::expense.name'))
            ->data(compact('expense'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new expense.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(ExpenseRequest $request)
    {

        $expense = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('expense::expense.name')) 
            ->view('expense::expense.create', true) 
            ->data(compact('expense'))
            ->output();
    }

    /**
     * Create new expense.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(ExpenseRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $attributes['date']      =date('Y-m-d',strtotime($attributes['date']));
            $expense                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('expense::expense.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('expense/expense/' . $expense->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/expense/expense'))
                ->redirect();
        }

    }

    /**
     * Show expense for editing.
     *
     * @param Request $request
     * @param Model   $expense
     *
     * @return Response
     */
    public function edit(ExpenseRequest $request, Expense $expense)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('expense::expense.name'))
            ->view('expense::expense.edit', true)
            ->data(compact('expense'))
            ->output();
    }

    /**
     * Update the expense.
     *
     * @param Request $request
     * @param Model   $expense
     *
     * @return Response
     */
    public function update(ExpenseRequest $request, Expense $expense)
    {
        try {
            $attributes = $request->all();
            $attributes['date']      =date('Y-m-d',strtotime($attributes['date']));
            $expense->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('expense::expense.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('expense/expense/' . $expense->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('expense/expense/' . $expense->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the expense.
     *
     * @param Model   $expense
     *
     * @return Response
     */
    public function destroy(ExpenseRequest $request, Expense $expense)
    {
        try {

            $expense->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('expense::expense.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('expense/expense/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('expense/expense/' . $expense->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple expense.
     *
     * @param Model   $expense
     *
     * @return Response
     */
    public function delete(ExpenseRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('expense::expense.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('expense/expense'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/expense/expense'))
                ->redirect();
        }

    }

    /**
     * Restore deleted expenses.
     *
     * @param Model   $expense
     *
     * @return Response
     */
    public function restore(ExpenseRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('expense::expense.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/expense/expense'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/expense/expense/'))
                ->redirect();
        }

    }
    protected function searchRestaurants(Request $request)
    {

        $name        = $request->get('q');
        if(strpos($name,"'")!=false)
            $sq='UPPER(name) LIKE "%' . strtoupper($name) . '%"';
        else
            $sq="UPPER(name) LIKE '%" . strtoupper($name) . "%'";
            $restaurants = $this->restaurant->scopeQuery(function ($query) use ($sq) {
                return $query->orderBy('name', 'ASC')
                    ->whereRaw($sq)
                // ->where('name', 'like', '%'.$name.'%')
                    ->take(10);
            })->get();
        return $restaurants;
    }
}
