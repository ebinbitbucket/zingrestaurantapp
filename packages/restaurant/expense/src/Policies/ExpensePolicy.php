<?php

namespace Restaurant\Expense\Policies;

use Litepie\User\Contracts\UserPolicy;
use Restaurant\Expense\Models\Expense;

class ExpensePolicy
{

    /**
     * Determine if the given user can view the expense.
     *
     * @param UserPolicy $user
     * @param Expense $expense
     *
     * @return bool
     */
    public function view(UserPolicy $user, Expense $expense)
    {
        // if ($user->canDo('expense.expense.view') && $user->isAdmin()) {
            return true;
        // }

        return $expense->user_id == user_id() && $expense->user_type == user_type();
    }

    /**
     * Determine if the given user can create a expense.
     *
     * @param UserPolicy $user
     * @param Expense $expense
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('expense.expense.create');
    }

    /**
     * Determine if the given user can update the given expense.
     *
     * @param UserPolicy $user
     * @param Expense $expense
     *
     * @return bool
     */
    public function update(UserPolicy $user, Expense $expense)
    {
        if ($user->canDo('expense.expense.edit') && $user->isAdmin()) {
            return true;
        }

        return $expense->user_id == user_id() && $expense->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given expense.
     *
     * @param UserPolicy $user
     * @param Expense $expense
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Expense $expense)
    {
        return $expense->user_id == user_id() && $expense->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given expense.
     *
     * @param UserPolicy $user
     * @param Expense $expense
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Expense $expense)
    {
        if ($user->canDo('expense.expense.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given expense.
     *
     * @param UserPolicy $user
     * @param Expense $expense
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Expense $expense)
    {
        if ($user->canDo('expense.expense.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
