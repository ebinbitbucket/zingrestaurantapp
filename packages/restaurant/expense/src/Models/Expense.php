<?php

namespace Restaurant\Expense\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
// use Litepie\Revision\Traits\Revision;
use Litepie\Trans\Traits\Translatable;
class Expense extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable, PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.expense.expense.model';
     
     public function restaurant()
     {
        return $this->belongsTo('Restaurant\Restaurant\Models\Restaurant','restaurant_id');
     }


}
