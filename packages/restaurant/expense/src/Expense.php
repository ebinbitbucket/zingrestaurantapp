<?php

namespace Restaurant\Expense;

use User;

class Expense
{
    /**
     * $expense object.
     */
    protected $expense;

    /**
     * Constructor.
     */
    public function __construct(\Restaurant\Expense\Interfaces\ExpenseRepositoryInterface $expense,\Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface $restaurant)
    {
        $this->expense = $expense;
        $this->restaurant = $restaurant;
    }

    /**
     * Returns count of expense.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return  0;
    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.expense.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->expense->pushCriteria(new \Litepie\Restaurant\Repositories\Criteria\ExpenseUserCriteria());
        }

        $expense = $this->expense->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('expense::' . $view, compact('expense'))->render();
    }
    public function restaurants(){
        return $this->restaurant->pluck('name','id');
    }
}
