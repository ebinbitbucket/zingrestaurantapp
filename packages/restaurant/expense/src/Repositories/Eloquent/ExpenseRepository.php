<?php

namespace Restaurant\Expense\Repositories\Eloquent;

use Restaurant\Expense\Interfaces\ExpenseRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class ExpenseRepository extends BaseRepository implements ExpenseRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.expense.expense.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.expense.expense.model.model');
    }
}
