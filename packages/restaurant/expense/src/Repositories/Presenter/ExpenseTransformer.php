<?php

namespace Restaurant\Expense\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class ExpenseTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Expense\Models\Expense $expense)
    {
        return [
            'id'                => $expense->getRouteKey(),
            'key'               => [
                'public'    => $expense->getPublicKey(),
                'route'     => $expense->getRouteKey(),
            ], 
            // 'id'                => $expense->id,
            'restaurant_id'     => @$expense->restaurant->name,
            'title'             => $expense->title,
            'amount'            => $expense->amount,
            'date'              => $expense->date,
            'files'             => $expense->files,
            'upload_folder'     => $expense->upload_folder,
            'description'       => $expense->description,
            'created_at'        => $expense->created_at,
            'updated_at'        => $expense->updated_at,
            'deleted_at'        => $expense->deleted_at,
            'url'               => [
                'public'    => trans_url('expense/'.$expense->getPublicKey()),
                'user'      => guard_url('expense/expense/'.$expense->getRouteKey()),
            ], 
            'status'            => trans('app.'.$expense->status),
            'created_at'        => format_date($expense->created_at),
            'updated_at'        => format_date($expense->updated_at),
        ];
    }
}