<?php

namespace Restaurant\Expense\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class ExpensePresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ExpenseTransformer();
    }
}