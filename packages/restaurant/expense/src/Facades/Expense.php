<?php

namespace Restaurant\Expense\Facades;

use Illuminate\Support\Facades\Facade;

class Expense extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'restaurant.expense';
    }
}
