<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Expense package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for expense module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Expense',
    'names'         => 'Expenses',
];
