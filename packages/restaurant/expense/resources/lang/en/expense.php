<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for expense in expense package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  expense module in expense package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Expense',
    'names'         => 'Expenses',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Expenses',
        'sub'   => 'Expenses',
        'list'  => 'List of expenses',
        'edit'  => 'Edit expense',
        'create'    => 'Create new expense'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'restaurant_id'              => 'Please select restaurant',
        'title'                      => 'Please enter title',
        'amount'                     => 'Please enter amount',
        'date'                       => 'Please select date',
        'files'                      => 'Please enter files',
        'upload_folder'              => 'Please enter upload folder',
        'description'                => 'Please enter description',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant',
        'title'                      => 'Title',
        'amount'                     => 'Amount',
        'date'                       => 'Date',
        'files'                      => 'Files',
        'upload_folder'              => 'Upload folder',
        'description'                => 'Description',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'title'                      => ['name' => 'Title', 'data-column' => 1, 'checked'],
        'amount'                     => ['name' => 'Amount', 'data-column' => 2, 'checked'],
        'date'                       => ['name' => 'Date', 'data-column' => 3, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Expenses',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
