@extends('resource.create')
@php
$links['back'] = guard_url('expense/expense');
$links['form'] = $links['back'];
@endphp

    @include('expense::expense.partial.entry', ['mode' => 'create'])

@section('actions') 
<div>
    <input class="btn-large btn-primary btn" type="submit" data-action="STORE" data-form="form-create" value="{{__('app.create')}}"> 
    <input class="btn-large btn-inverse btn" type="reset" value="{{__('app.reset')}}">
</div>
@stop

