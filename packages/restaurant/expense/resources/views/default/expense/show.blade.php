@extends('resource.show')

@php
$links['back'] = guard_url('expense/expense');
$links['edit'] = guard_url('expense/expense') . '/' . $expense->getRouteKey() . '/edit';
@endphp


@section('content') 
    @include('expense::expense.partial.show', ['mode' => 'show'])
@stop
