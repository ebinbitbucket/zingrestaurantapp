@extends('resource.edit')
@php
$links['back'] = guard_url('expense/expense');
$links['form'] = guard_url('expense/expense') . '/' . $expense->getRouteKey();
@endphp

    @include('expense::expense.partial.entry', ['mode' => 'edit'])


@section('actions') 
<div>
    <input class="btn-large btn-primary btn" type="submit" data-action="UPDATE" data-form="form-edit" value="{{__('app.update')}}"> 
    <input class="btn-large btn-inverse btn" type="reset" value="{{__('app.reset')}}">
</div>
@stop