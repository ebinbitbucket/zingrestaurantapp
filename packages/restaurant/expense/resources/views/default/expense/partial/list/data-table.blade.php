            <table class="table" id="main-table" data-url="{!!guard_url('expense/expense?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="title">{!! trans('expense::expense.label.title')!!}</th>
                    <th data-field="amount">{!! trans('expense::expense.label.amount')!!}</th>
                    <th data-field="date">{!! trans('expense::expense.label.date')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>