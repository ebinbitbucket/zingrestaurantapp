<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
        <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
            <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i>Monthly Statement</a>
            <a class="active" href="{{guard_url('expense/expense')}}"><i class="fa fa-sticky-note-o"></i>Invoice</a>
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
            <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                <aside class="dashboard-sidemenu">
                    <nav class="sidebar-nav">
                        <ul>
                            <li class="nav-head"><span class="head">Navigation</span></li>
                            <li>
                                <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                            </li>
                            <li >
                                <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i><span>Eatery Schedule</span></a>
                            </li>
                             <li>
                                <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i><span>Kitchen</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i><span>Monthly Statement</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/website')}}"><i class="ion-android-clipboard"></i><span>Website</span></a>
                            </li>
                            <li  class="active">
                                <a href="{{guard_url('expense/expense')}}"><i class="fa fa-sticky-note-o"></i><span>Invoice</span></a>
                            </li>

                        </ul>
                    </nav>
                </aside>
            </div>
            <div class="col-md-12 col-lg-9">
                <div class="element-wrapper order-detail-wrap">
                    <div class="element-box">
                        <div class="element-info">
                            <div class="element-info-with-icon">
                                <div class="element-info-icon"><div class="icon ion-social-buffer"></div></div>
                                <div class="element-info-text">
                                    <h5 class="element-inner-header">Invoices</h5>
                                </div>
                                <div class="element-info-buttons element-add-buttons">
                                    <a href="{{guard_url('expense/expense/create')}}"><button  class="btn btn-theme ion-android-add add-addon" > </button></a>
                                </div>
                            </div>
                     </div>  
                     {!!Form::vertical_open()
                        ->id('restaurant-expense-create')
                        ->method('POST')
                        ->enctype('multipart/form-data')
                        ->action(guard_url('expense/expense/'))!!}

                     <div class='row'>
                            <div class='col-md-12 col-sm-6'>
                                   {!! Form::text('title')
                                   -> label(trans('expense::expense.label.title'))
                                   -> placeholder(trans('expense::expense.placeholder.title'))!!}
                            </div>
                            <div class='col-md-6 col-sm-6'>
                                   {!! Form::decimal('amount')
                                   -> label(trans('expense::expense.label.amount'))
                                   -> placeholder(trans('expense::expense.placeholder.amount'))!!}
                            </div>
                            <div class='col-md-6 col-sm-6'>
                                <label for='date' class='control-label'>{!!trans('expense::expense.label.date')!!}</label>
                                {!! Form::date('date')
                                -> placeholder(trans('expense::expense.placeholder.date'))
                                ->raw()!!}
                            </div>
                            
                            <!-- <div class='col-md-4 col-sm-6'>
                                   {!! Form::text('files')
                                   -> label(trans('expense::expense.label.files'))
                                   -> placeholder(trans('expense::expense.placeholder.files'))!!}
                            </div> -->
                            <div class='col-md-6 col-sm-6'>
                                   {!! Form::textarea('description')
                                   -> addClass('html-editor')
                                   -> label(trans('expense::expense.label.description'))
                                   -> placeholder(trans('expense::expense.placeholder.description'))!!}
                            </div>
                            <div class='col-md-6 col-sm-6'></br>
                            {!! $expense->files('file')
                                ->url($expense->getUploadUrl('files'))
                                ->mime(config('filer.allowed_extensions'))
                                ->dropzone()!!}
                            </div>
                            <input type="hidden" name="restaurant_id" value="{{@user()->id}}">
                            <div class="form-buttons-w text-center">
                                <center><button class="btn btn-theme" type="submit" style="width: 150px;"> Update</button></center>
<!--                            <button class="btn btn-danger" type="button"> Cancel</button>
-->                         </div>

                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
            
