<aside class="main-nav">
    <div class="nav-inner">
    @include('restaurant::default.restaurant.partial.mobile_menu')

    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
            <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-3 d-none d-lg-block">
                <aside class="dashboard-sidemenu">
                @include('restaurant::default.restaurant.partial.left_menu')

                </aside>
            </div>
            <div class="col-md-12 col-lg-9">
                <div class="element-wrapper order-detail-wrap">
                    <div class="element-box">
                        <div class="element-info">
                            <div class="element-info-with-icon">
                                <div class="element-info-icon"><div class="icon ion-social-buffer"></div></div>
                                <div class="element-info-text">
                                    <h5 class="element-inner-header">Invoices</h5>
                                </div>
                                <!-- <div class="element-info-buttons element-add-buttons">
                                    <a href="{{guard_url('expense/expense/create')}}"><button  class="btn btn-theme ion-android-add add-addon" > </button></a>
                                </div> -->
                            </div>
                        </div>  
                        <div id="stmnt_div">
                               <table class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Title</th>
                                        <th>Amount</th>
                                        <th>Date</th>
                                        <th>File</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $sl_no=1; @endphp
                                @forelse($expenses as $expense) 
                                @if($expense->restaurant_id==user()->id)
                                <tr>
                                    <td><p><a href="{{guard_url('expense/expense/'.$expense->getRouteKey())}}" style="color:black;">{{$sl_no}}</a></p></td>
                                    <td><p><a href="{{guard_url('expense/expense/'.$expense->getRouteKey())}}" style="color:black;">{{@$expense->title}}</a></p></td>
                                    <td><p><a href="{{guard_url('expense/expense/'.$expense->getRouteKey())}}" style="color:black;">{{@$expense->amount}}</a></p></td>
                                    <td><p><a href="{{guard_url('expense/expense/'.$expense->getRouteKey())}}" style="color:black;">{{@$expense->date}}</a></p></td>
                                    <td><p><a href="{{guard_url('expense/expense/'.$expense->getRouteKey())}}" style="color:black;">{!!$expense->files('files')->url("yufryty")->show();!!}</a></p></td>
                                </a>
                                </tr>
                                @php $sl_no=$sl_no+1; @endphp
                                @endif
                                @empty
                                @endif
                                <tr>
                                    <td colspan="5"><p>{{@$expenses->links()}}</p></td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
            
