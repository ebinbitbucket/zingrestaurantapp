            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::select('restaurant_id')
                       -> options(Expense::restaurants())
                       -> label(trans('expense::expense.label.restaurant_id'))
                       -> placeholder(trans('expense::expense.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-8 col-sm-6'>
                       {!! Form::text('title')
                       -> label(trans('expense::expense.label.title'))
                       -> placeholder(trans('expense::expense.placeholder.title'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('amount')
                       -> label(trans('expense::expense.label.amount'))
                       -> placeholder(trans('expense::expense.placeholder.amount'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   <div class='form-group'>
                     <label for='date' class='control-label'>{!!trans('expense::expense.label.date')!!}</label>
                     <div class='input-group pickdate'>
                        {!! Form::text('date')
                        -> placeholder(trans('expense::expense.placeholder.date'))
                        ->raw()!!}
                       <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                     </div>
                   </div>
                </div>

                <div class='col-md-12 col-sm-6'>
                {!! $expense->files('files')
                     ->url($expense->getUploadUrl('files'))
                     ->mime(config('filer.allowed_extensions'))
                     ->dropzone()!!}
                </div>

                <div class='col-md-12 col-sm-6'>
                       {!! Form::textarea('description')
                       ->addClass('html-editor')
                       -> label(trans('expense::expense.label.description'))
                       -> placeholder(trans('expense::expense.placeholder.description'))!!}
                </div>
            </div>