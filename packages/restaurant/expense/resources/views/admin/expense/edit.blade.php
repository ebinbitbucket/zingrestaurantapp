    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#expense" data-toggle="tab">{!! trans('expense::expense.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#expense-expense-edit'  data-load-to='#expense-expense-entry' data-datatable='#expense-expense-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#expense-expense-entry' data-href='{{guard_url('expense/expense')}}/{{$expense->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('expense-expense-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('expense/expense/'. $expense->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="expense">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('expense::expense.name') !!} [{!!$expense->name!!}] </div>
                @include('expense::admin.expense.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>