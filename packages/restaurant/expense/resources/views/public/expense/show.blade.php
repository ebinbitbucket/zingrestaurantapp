            @include('expense::expense.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('expense::expense.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($expense->defaultImage('images' , 'xl'))!!}" alt="{{$expense->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('expense::expense.label.id') !!}
                </label><br />
                    {!! $expense['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="restaurant_id">
                    {!! trans('expense::expense.label.restaurant_id') !!}
                </label><br />
                    {!! $expense['restaurant_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="title">
                    {!! trans('expense::expense.label.title') !!}
                </label><br />
                    {!! $expense['title'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="amount">
                    {!! trans('expense::expense.label.amount') !!}
                </label><br />
                    {!! $expense['amount'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="date">
                    {!! trans('expense::expense.label.date') !!}
                </label><br />
                    {!! $expense['date'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="files">
                    {!! trans('expense::expense.label.files') !!}
                </label><br />
                    {!! $expense['files'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="upload_folder">
                    {!! trans('expense::expense.label.upload_folder') !!}
                </label><br />
                    {!! $expense['upload_folder'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="description">
                    {!! trans('expense::expense.label.description') !!}
                </label><br />
                    {!! $expense['description'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('expense::expense.label.created_at') !!}
                </label><br />
                    {!! $expense['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('expense::expense.label.updated_at') !!}
                </label><br />
                    {!! $expense['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('expense::expense.label.deleted_at') !!}
                </label><br />
                    {!! $expense['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('expense::expense.label.restaurant_id'))
                       -> placeholder(trans('expense::expense.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('title')
                       -> label(trans('expense::expense.label.title'))
                       -> placeholder(trans('expense::expense.placeholder.title'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('amount')
                       -> label(trans('expense::expense.label.amount'))
                       -> placeholder(trans('expense::expense.placeholder.amount'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   <div class='form-group'>
                     <label for='date' class='control-label'>{!!trans('expense::expense.label.date')!!}</label>
                     <div class='input-group pickdate'>
                        {!! Form::text('date')
                        -> placeholder(trans('expense::expense.placeholder.date'))
                        ->raw()!!}
                       <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                     </div>
                   </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('files')
                       -> label(trans('expense::expense.label.files'))
                       -> placeholder(trans('expense::expense.placeholder.files'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('description')
                       -> label(trans('expense::expense.label.description'))
                       -> placeholder(trans('expense::expense.placeholder.description'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



