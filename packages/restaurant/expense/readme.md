Lavalite package that provides expense management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `restaurant/expense`.

    "restaurant/expense": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Restaurant\Expense\Providers\ExpenseServiceProvider::class,

And also add it to alias

    'Expense'  => Restaurant\Expense\Facades\Expense::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Restaurant\\ExpenseTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Restaurant\Expense\Providers\ExpenseServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Restaurant\Expense\Providers\ExpenseServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Restaurant\Expense\Providers\ExpenseServiceProvider" --tag="view"


## Usage


