<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'restaurant',

    /*
     * Package.
     */
    'package'   => 'expense',

    /*
     * Modules.
     */
    'modules'   => ['expense'],

    
    'expense'       => [
        'model' => [
            'model'                 => \Restaurant\Expense\Models\Expense::class,
            'table'                 => 'expense',
            'presenter'             => \Restaurant\Expense\Repositories\Presenter\ExpensePresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'title'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'title',  'amount',  'date',  'files',  'upload_folder',  'description',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'expense/expense',
            'uploads'               => [
            
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'files' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            
            ],

            'casts'                 => [
            
                'images'    => 'array',
                'files'      => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'title'  => 'like',
                'amount'  => '=',
                'restaurant_id'  => '=',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Expense',
            'module'    => 'Expense',
        ],

    ],
];
