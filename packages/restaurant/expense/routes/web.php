<?php

// Resource routes  for expense
Route::group(['prefix' => set_route_guard('web').'/expense'], function () {
    Route::resource('expense', 'ExpenseResourceController');
});

// Public  routes for expense
Route::get('expense/search', 'ExpensePublicController@searchRestaurants');
Route::get('expense/popular/{period?}', 'ExpensePublicController@popular');
Route::get('expenses/', 'ExpensePublicController@index');
Route::get('expenses/{slug?}', 'ExpensePublicController@show');

