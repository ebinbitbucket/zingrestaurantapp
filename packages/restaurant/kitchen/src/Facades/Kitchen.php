<?php

namespace Restaurant\Kitchen\Facades;

use Illuminate\Support\Facades\Facade;

class Kitchen extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'restaurant.kitchen';
    }
}
