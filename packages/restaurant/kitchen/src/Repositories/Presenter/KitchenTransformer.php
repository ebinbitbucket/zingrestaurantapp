<?php

namespace Restaurant\Kitchen\Repositories\Presenter;

use DateTime;
use League\Fractal\TransformerAbstract;

class KitchenTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Kitchen\Models\Kitchen $kitchen)
    {
        $datetime1 = new DateTime(); // 11 October 2013
        $datetime2 = new DateTime($kitchen->last_accessed); // 13 October 2013

        $interval = $datetime1->diff($datetime2);
        $diff     = $interval->format('%i');

        $then = date('Y-m-d H:i:m', strtotime($kitchen->last_accessed));

        $then = strtotime($then);

        $now = time();

        $difference = $now - $then;
        $days       = floor($difference / (60));

        if ($days < 10) {
            $color = 'badge-success';
        } elseif ($days > 60) {
            $color = 'badge-danger';
        } else {
            $color = 'badge-warning';
        }

        return [
            'id'             => $kitchen->getRouteKey(),
            'key'            => [
                'public' => $kitchen->getPublicKey(),
                'route'  => $kitchen->getRouteKey(),
            ],
            'restaurant_id'  => @$kitchen->restaurant->name,
            'name'           => $kitchen->name,
            'username'       => $kitchen->username,
            'audio_alert'    => @$kitchen->restaurant->audio_alert,
            'active_status'  => @$color,
            'description'    => $kitchen->description,
            'email'          => $kitchen->email,
            'password'       => $kitchen->password,
            'api_token'      => $kitchen->api_token,
            'remember_token' => $kitchen->remember_token,
            'slug'           => $kitchen->slug,
            'user_id'        => $kitchen->user_id,
            'user_type'      => $kitchen->user_type,
            'created_at'     => $kitchen->created_at,
            'deleted_at'     => $kitchen->deleted_at,
            'updated_at'     => $kitchen->updated_at,
            'url'            => [
                'public' => trans_url('kitchen/' . $kitchen->getPublicKey()),
                'user'   => guard_url('kitchen/kitchen/' . $kitchen->getRouteKey()),
            ],
            'status'         => trans('app.' . $kitchen->status),
            'created_at'     => format_date($kitchen->created_at),
            'updated_at'     => format_date($kitchen->updated_at),
        ];
    }

}
