<?php

namespace Restaurant\Kitchen\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class KitchenPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new KitchenTransformer();
    }
}