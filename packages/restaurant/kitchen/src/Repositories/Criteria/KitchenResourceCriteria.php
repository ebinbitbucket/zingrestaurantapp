<?php

namespace Restaurant\Kitchen\Repositories\Criteria;

use Litepie\Repository\Contracts\CriteriaInterface;
use Litepie\Repository\Contracts\RepositoryInterface;

class KitchenResourceCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
    	if(user()->hasRole('restaurant')){
    		$model = $model->where('restaurant_id',user_id());
    	}
    	
        
        return $model;
    }
}