<?php

namespace Restaurant\Kitchen\Repositories\Eloquent;

use Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class KitchenRepository extends BaseRepository implements KitchenRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.kitchen.kitchen.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.kitchen.kitchen.model.model');
    }
    public function findKitchensByRestaurent($restaurent_id){
        return $this->model->select('id','fcm')->where('restaurant_id',$restaurent_id)->orderBy('id','DESC')->get();


    }
}
