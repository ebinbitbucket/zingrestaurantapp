<?php

namespace Restaurant\Kitchen\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Kitchen\Http\Requests\KitchenRequest;
use Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface;
use Restaurant\Kitchen\Models\Kitchen;
use Restaurant\Restaurant\Models\Restaurant as RestaurantModel;
use Auth;
use Session;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;


/**
 * Resource controller class for kitchen.
 */
class KitchenResourceController extends BaseController
{

    /**
     * Initialize kitchen resource controller.
     *
     * @param type KitchenRepositoryInterface $kitchen
     *
     * @return null
     */
    public function __construct(KitchenRepositoryInterface $kitchen,RestaurantRepositoryInterface $restaurant)
    {
        parent::__construct();
        $this->repository = $kitchen;
       $this->restaurant = $restaurant;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Kitchen\Repositories\Criteria\KitchenResourceCriteria::class);
    }

    /**
     * Display a list of kitchen.
     *
     * @return Response
     */
    public function index(KitchenRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Kitchen\Repositories\Presenter\KitchenPresenter::class)
                ->$function();
        }

        $kitchens = $this->repository->paginate();

        return $this->response->title(trans('kitchen::kitchen.names'))
            ->view('kitchen::kitchen.index', true)
            ->data(compact('kitchens'))
            ->output();
    }

    /**
     * Display kitchen.
     *
     * @param Request $request
     * @param Model   $kitchen
     *
     * @return Response
     */
    public function show(KitchenRequest $request, Kitchen $kitchen)
    {

        if ($kitchen->exists) {
            $view = 'kitchen::kitchen.show';
        } else {
            $view = 'kitchen::kitchen.new';
        }

        return $this->response->title(trans('app.view') . ' ' . trans('kitchen::kitchen.name'))
            ->data(compact('kitchen'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new kitchen.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(KitchenRequest $request)
    {

        $kitchen = $this->repository->newInstance([]);
        return $this->response->title(trans('app.new') . ' ' . trans('kitchen::kitchen.name')) 
            ->view('kitchen::kitchen.create', true) 
            ->data(compact('kitchen'))
            ->output();
    }

    /**
     * Create new kitchen.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(KitchenRequest $request)
    {
        try {
            $attributes              = $request->all();
          //  dd($attributes);
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $attributes['api_token'] = str_random(60);
            $kitchen                 = $this->repository->create($attributes);

            if(user()->hasRole('restaurant')){
                return $this->response->message(trans('messages.success.created', ['Module' => trans('kitchen::kitchen.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('kitchen/restaurant/kitchen'))
                ->redirect();
            }
            return $this->response->message(trans('messages.success.created', ['Module' => trans('kitchen::kitchen.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('kitchen/kitchen/' . $kitchen->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/kitchen/kitchen'))
                ->redirect();
        }

    }

    /**
     * Show kitchen for editing.
     *
     * @param Request $request
     * @param Model   $kitchen
     *
     * @return Response
     */
    public function edit(KitchenRequest $request, Kitchen $kitchen)
    {
        return $this->response->title(trans('app.edit') . ' ' . trans('kitchen::kitchen.name'))
            ->view('kitchen::kitchen.edit', true)
            ->data(compact('kitchen'))
            ->output();
    }

    /**
     * Update the kitchen.
     *
     * @param Request $request
     * @param Model   $kitchen
     *
     * @return Response
     */
    public function update(KitchenRequest $request, Kitchen $kitchen)
    {
        try {
            $attributes = $request->all();
            $kitchen->update($attributes);
            if(user()->hasRole('restaurant')){
                return $this->response->message(trans('messages.success.created', ['Module' => trans('kitchen::kitchen.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('kitchen/restaurant/kitchen'))
                ->redirect();
            }
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('kitchen::kitchen.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('kitchen/kitchen/' . $kitchen->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('kitchen/kitchen/' . $kitchen->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the kitchen.
     *
     * @param Model   $kitchen
     *
     * @return Response
     */
    public function destroy(KitchenRequest $request, Kitchen $kitchen)
    {
        try {

            $kitchen->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('kitchen::kitchen.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('kitchen/kitchen/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('kitchen/kitchen/' . $kitchen->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple kitchen.
     *
     * @param Model   $kitchen
     *
     * @return Response
     */
    public function delete(KitchenRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('kitchen::kitchen.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('kitchen/kitchen'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/kitchen/kitchen'))
                ->redirect();
        }

    }

    /**
     * Restore deleted kitchens.
     *
     * @param Model   $kitchen
     *
     * @return Response
     */
    public function restore(KitchenRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('kitchen::kitchen.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/kitchen/kitchen'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/kitchen/kitchen/'))
                ->redirect();
        }

    }

    public function restaurant_kitchens(){
        $users = $this->repository->get();
        return $this->response->setMetaTitle("Kitchens")
             ->view('restaurant::default.restaurant.kitchen.restaurant_users')
            ->data(compact('users'))
            ->output();
    }

      public function kitchen_create(){
          $restaurant_timings=[];
        $users = $this->repository->get();
        return $this->response->setMetaTitle('Restaurant Details')
            ->view('restaurant::default.restaurant.kitchen.kitchen_create')
            ->data(compact('restaurant_timings'))
            ->layout('default')
            ->output();
    }

      public function kitchen_edit($key){
        $kitchen = $this->repository->findBySlug($key);
        return $this->response->setMetaTitle('Restaurant Details')
            ->view('restaurant::default.restaurant.kitchen.kitchen_edit')
            ->data(compact('kitchen'))
            ->layout('default')
            ->output();
    }
    public function kitchen_dashboard($slug){
        $key = crypt::decrypt($slug);
        $restaurant = $this->repository->findByField('id',$key)->first();
          Auth::guard('kitchen.web')->loginUsingId($restaurant->id);
           return redirect()->intended(trans_url('kitchen/cart/orders/kitchen_orders/new'));
    }
    
 public function kitchenStatus($kitchen_id)
    {
        $kitchen=Kitchen::find($kitchen_id);
        if($kitchen->kitchen_status=='Yes')
             $kitchen->kitchen_status='No';
        else
             $kitchen->kitchen_status='Yes';
        $kitchen->update(['kitchen_status'=>$kitchen->kitchen_status, 'stop_date' => null]);
        $restaurant=RestaurantModel::where('id',$kitchen->restaurant_id)->update(['kitchen_status'=>$kitchen->kitchen_status, 'stop_date' => null]);
        return $this->response
        ->url(url('/kitchen/cart/orders/kitchen_orders/new'))
        ->redirect();
    }
    public function loginAs()
    {        
     Session::forget('login_from_kitchen');
        $restaurant_id = user()->restaurant_id; 
        $restaurant = $this->restaurant->findByField('id',$restaurant_id)->first();
        Session::put('login_from_kitchen',$restaurant->id);
          Auth::guard('restaurant.web')->loginUsingId($restaurant->id);
           return redirect()->to(trans_url('restaurant/cart/restaurant/orders'));
        
    }

}
