<?php

namespace Restaurant\Kitchen\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface;
use Illuminate\Http\Request;
use Restaurant\Kitchen\Models\Kitchen;
use Restaurant\Restaurant\Models\Restaurant as RestaurantModel;
use Auth;
use Crypt;
class KitchenPublicController extends BaseController
{
    // use KitchenWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface $kitchen
     *
     * @return type
     */
    public function __construct(KitchenRepositoryInterface $kitchen)
    {
        $this->repository = $kitchen;
        parent::__construct();
    }

    /**
     * Show kitchen's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $kitchens = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->title(trans('$kitchen::$kitchen.names'))
            ->view('$kitchen::public.kitchen.index')
            ->data(compact('$kitchens'))
            ->output();
    }

    /**
     * Show kitchen's list based on a type.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function list($type = null)
    {
        $kitchens = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->title(trans('$kitchen::$kitchen.names'))
            ->view('$kitchen::public.kitchen.index')
            ->data(compact('$kitchens'))
            ->output();
    }

    /**
     * Show kitchen.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $kitchen = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->title($$kitchen->name . trans('$kitchen::$kitchen.name'))
            ->view('$kitchen::public.kitchen.show')
            ->data(compact('$kitchen'))
            ->output();
    }

    /**
     * Automatic login for kitchen
     *
     * @param string $api_token
     *
     * @return redirect
     */
 
    public function kitchenLogin($api_token){
        $restaurant = $this->repository
            ->findByField('api_token', $api_token)
            ->first();
        if(is_null($restaurant)){
            return view('kitchen::public.kitchen.timeout');
        }
        Auth::guard('kitchen.web')->loginUsingId($restaurant->id);
        return redirect(trans_url('kitchen/cart/app/new'));
    }
    
    /**
     * Automatic login for kitchen
     *
     * @param string $api_token
     *
     * @return redirect
     */
 
    public function checkLogin($api_token){
        $restaurant = $this->repository
            ->findByField('api_token', $api_token)
            ->first();
        if(is_null($restaurant)){
            $data['data'] = 'false';
            return $data;
        }
        $data['data'] = 'true';
        return $data;
    }
    public function kitchen_dashboard($slug){
        $key = crypt::decrypt($slug);
        $restaurant = $this->repository->findByField('id',$key)->first();
          Auth::guard('kitchen.web')->loginUsingId($restaurant->id);
           return redirect()->to(trans_url('kitchen/cart/orders/kitchen_orders/new'));
        //   return redirect()->intended(trans_url('kitchen/cart/orders/kitchen_orders/new'));
    }

        public function kitchenStatus($kitchen_id, Request $request)
    {
        
        $kitchen=Kitchen::find($kitchen_id);
        if($kitchen->kitchen_status=='Yes'){
            $kitchen->kitchen_status='No';
            $date = $request->get('stop_date');
            $kitchen->update(['kitchen_status'=>$kitchen->kitchen_status, 'stop_date' => $date]);
            $restaurant=RestaurantModel::where('id',$kitchen->restaurant_id)->update(['kitchen_status'=>$kitchen->kitchen_status, 'stop_date' => $date]);
        }
        else{
            $kitchen->kitchen_status='Yes';
            $kitchen->update(['kitchen_status'=>$kitchen->kitchen_status, 'stop_date' => null]);
            $restaurant=RestaurantModel::where('id',$kitchen->restaurant_id)->update(['kitchen_status'=>$kitchen->kitchen_status, 'stop_date' => null]);
        }
        return redirect()->back();
    }
     /**
     * Automatic login for kitchen
     *
     * @param string $api_token
     *
     * @return redirect
     */
 
    public function tokenCheck(Request $request){
        $api_token = $request->token;
        $fcm = $request->fcm;
        $device_id = $request->device_id;
        $kitchen = $this->repository
            ->findByField('api_token', $api_token)
            ->first();
        if(is_null($kitchen)){
            $data['data'] = 'false';
            return $data;
        }
        $kitchen->update(['fcm'=>$fcm,'device_id'=>$device_id]);
        $data['data'] = 'true';
        $data['auto_refresh'] = $kitchen->auto_refresh;
        return $data;
    }
}
