<?php

namespace Restaurant\Kitchen\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface;
use Laraecart\Cart\Interfaces\OrderRepositoryInterface;
use Restaurant\Kitchen\Models\Kitchen;
use Restaurant\Restaurant\Models\Restaurant;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
use Auth;
use Crypt;
use DB;
use Mail;
use Carbon\Carbon;
use Session;
use Illuminate\Http\Request;
use BandwidthLib;


class KitchenApiController extends BaseController
{
    // use KitchenWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface $kitchen
     *
     * @return type
     */
    public function __construct(KitchenRepositoryInterface $kitchen, OrderRepositoryInterface $order,RestaurantRepositoryInterface $restaurant)
    {
        ini_set('precision', 12);
        ini_set('serialize_precision', 10);
        $this->repository = $kitchen;
        $this->order = $order;
        $this->restaurant = $restaurant;
        parent::__construct();
    }

    /**
     * Show kitchen's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function todaysOrders()
    {
       // dd(user()->device_id);
        $restaurant_id = user()->restaurant_id; 
        $orders = $this->order
        ->scopeQuery(function($query) use ($restaurant_id) {
            return $query->with('detail', 'detail.menu')->whereIn('order_status', array('New Orders', 'Completed'))->orderBy('id','DESC')->where('restaurant_id', $restaurant_id)
            ->whereDate('delivery_time', '=', date('Y-m-d'));
        })->get();


        foreach($orders as $order){
            $order->restaurant_name=$order->restaurant->name;
            $order->address=@$order->delivery_address->full_address;
            foreach($order->detail as $detail){
                if(($detail->addons=="")&&($detail->addons==null)){
                    $detail->addons=[];
                } else{
                    $add=[];
                    foreach($detail->addons as $addon){
                        $add[] = array_map('strval', $addon);
                    }
                    $detail->addons=$add;

                }
    
    
            }


        }
        //dd($orders->toArray());

        return response()->json(['orders' => $orders]);
    }

    /**
     * Show kitchen's list based on a type.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function orderDetail($id = null)
    {
       // $order = $this->order->findByField('id', $id)->first();
        $order = $this->order->getOrderItems($id);
        $order->restaurant_name=$order->restaurant->name;
        $order->address=@$order->delivery_address->full_address;
        foreach($order->detail as $detail){
            if($detail->addons==""){
                $detail->addons=[];
            } else{
                $add=[];
                foreach($detail->addons as $addon){
                    $add[] = array_map('strval', $addon);
                }
                $detail->addons=$add;

            }


         }

        //dd(response()->json(['order' => $order]));
        
        return response()->json(['order' => $order]);
    }

    /**
     * Show kitchen's list based on a type.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function orderUpdate(Request $request,$id)
    {
        $order = $this->order->findByField('id', $id)->first();
        $flag = $request->flag;
        $is_car_pickup = $request->is_car_pickup;
        if(isset($request->is_car_pickup)){
            if($order->update(['is_car_pickup'=> $request->is_car_pickup])){
                return response()->json(['status' =>'Updated']);
    
            }
        }
        if($order->update(['flag'=>$flag])){
            return response()->json(['status' =>'Updated']);

        }

        
    }
    
    protected function kitchenStatus(Request $request,$status,$id)
    {
        $order = $this->order->findByField('id',$id)->first();
        $order_detail = $order->detail;
        $preperation_time = $request->prep_time;
        $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id',$order->restaurant->id)->orderBy('day')->orderBy('opening')->get();
               $weekMap = [
             0 => 'sun',
             1 => 'mon',
             2 => 'tue',
             3 => 'wed',
             4 => 'thu',
             5 => 'fri',
             6 => 'sat',
         ]; 
         $dayOfTheWeek = Carbon::now()->dayOfWeek;    
         $weekday = $weekMap[$dayOfTheWeek];   
         if($status == 'accept')
         {
            if($preperation_time){
                $newtimestamp = strtotime($order->delivery_time .'+'.$preperation_time.' minute');
              $order->update(['ready_time' => date('Y-m-d H:i:s', $newtimestamp),'preparation_time'=>$preperation_time]);     
  
             }

            $status = "Completed";
            $user = $order->user;
            //return view('cart::public.cart.ack',compact('order','order_detail','user','restaurant_timings','weekMap','dayOfTheWeek','weekday'));
            Mail::send('cart::public.cart.ack', ['order' => $order,'order_detail' => $order->detail, 'user' => $user, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday], function ($message) use ($user) {
               $message->from('support@zingmyorder.com','ZingMyOrder');
               $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
               $message->to($user->email)->subject('Zing Order Acknowledgement');
           });
           $this->sendTextMessage($order,'acknowledgement');

         }
         else if($status == 'pickup'){
            $user = $order->user;
             //return view('cart::public.cart.preparing',compact('order','order_detail','user','restaurant_timings','weekMap','dayOfTheWeek','weekday'));
             Mail::send('cart::public.cart.pickup', ['order' => $order,'order_detail' => $order->detail, 'user' => $user,'status' => $status, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday], function ($message) use ($user,$status) {
            $message->from('support@zingmyorder.com','ZingMyOrder');
            $message->to($user->email)->subject('Zing Order Ready For Pickup');
             });
             
             $order->update(['mail_sent' => $order->mail_sent+1]);     
             $this->sendTextMessage($order,'ready_for_pickup');

             return response()->json(['status' => 'email sented successfully']);
    
             //return ['order_status' => 'email mail sented successfully'];         
    
             // $user = new User();
             //     $user->email = $order->user->email;
             //     $user->notify(new CustomEmail('Order #'.$order->id.' Pickup', 'Your order No#'.$order->id.' is ready for pickup'));
        } else if($status == 'delivery'){
    
            $user = $order->user;
            //return view('cart::public.cart.preparing',compact('order','order_detail','user','restaurant_timings','weekMap','dayOfTheWeek','weekday'));
            Mail::send('cart::public.cart.delivery', ['order' => $order,'order_detail' => $order->detail, 'user' => $user,'status' => $status, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday], function ($message) use ($user,$status) {
           $message->from('support@zingmyorder.com','ZingMyOrder');
           $message->to($user->email)->subject('Zing Order Out For Delivery');
            });
            $order->update(['mail_sent' => $order->mail_sent+1]);     
            $this->sendTextMessage($order,'out_for_delivery');

            return response()->json(['status' => 'email sented successfully']);
    
        }

    $order->update(['order_status' => $status]);     

        return response()->json(['status' =>'Completed']);
    }

    public function loginAs()
    {        
     Session::forget('login_from_kitchen');
        $restaurant_id = user()->restaurant_id; 
        $restaurant = $this->restaurant->findByField('id',$restaurant_id)->first();
        Session::put('login_from_kitchen',$restaurant->id);
          Auth::guard('restaurant.web')->loginUsingId($restaurant->id);
           return redirect()->to(trans_url('restaurant/cart/restaurant/orders'));
        
    }

    public function kitchenTimeUpdate($id,$time)
    {
        $order = $this->order->findByField('id',$id)->first();
        //dd($order);
        $currentDeliveryTime=$order->delivery_time;
        $newtimestamp = strtotime($order->delivery_time.' + '.$time.' minute');
       $order->update(['delivery_time'=>date('Y-m-d H:i:s',$newtimestamp),'delay_min'=>$time]);

       // $order = $this->repository->findByField('id', hashids_decode($order_id))->first();
        $user   = $order->user;
         $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id', $order->restaurant->id)->orderBy('day')->orderBy('opening')->get();
        $weekMap            = [
            0 => 'sun',
            1 => 'mon',
            2 => 'tue',
            3 => 'wed',
            4 => 'thu',
            5 => 'fri',
            6 => 'sat',
        ];
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        $weekday      = $weekMap[$dayOfTheWeek];
       
       Mail::send('cart::public.cart.timeextend', ['order' => $order,'user'=>$user,'order_detail' => $order->detail, 'user' => $user, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday, 'newtimestamp' => $time,'currentDeliveryTime'=>$currentDeliveryTime], function ($message) use ($user) {
               $message->from('support@zingmyorder.com','ZingMyOrder');
               $message->to($user->email)->subject('Zing Order Delay');
           });
           $this->sendTextMessage($order,'time_extend');
           return response()->json(['status' =>'Completed']);
        }

        public function sendTextMessage($order,$type)
        {
    
            if($order->text_notification==1){
            $message="";
    $config = new BandwidthLib\Configuration(
        array(
            'messagingBasicAuthUserName' => getenv('BANDWIDTH_USER'),
            'messagingBasicAuthPassword' => getenv('BANDWIDTH_PASSWORD'),
            'voiceBasicAuthUserName' => 'username',
            'voiceBasicAuthPassword' => 'password',
            'twoFactorAuthBasicAuthUserName' => 'username',
            'twoFactorAuthBasicAuthPassword' => 'password'
            
        ) 
    );
    if($type=='acknowledgement'){
    if($order->order_type=='Pickup'){
          if($order->car_pickup==1){
            $url=trans_url('order/car-delivery/'.$order->getRoutekey());
            $message="Hope you're having a Zing day! Your food is now being prepared by ".$order->restaurant->name.". Current estimated Pickup Time is: ".date('h:i A', strtotime($order->ready_time)).". When you arrive at the restaurant click here ".$url." to let them know. Enjoy your meal.";
          }else{
            $message="Hope you're having a Zing day! Your food is now being prepared by ".$order->restaurant->name.". Current estimated Pickup Time is: ".date('h:i A', strtotime($order->ready_time)).". Enjoy your meal.";
          }
    }else{
    if($order->non_contact==1){
        $message="Hope you're having a Zing day! Your No-contact delivery order is now being prepared by ".$order->restaurant->name.". Current estimated Delivery Time is: ".date('h:i A', strtotime($order->ready_time)).". Enjoy your meal.";
          }else{
            $message="Hope you're having a Zing day! Your food is now being prepared by ".$order->restaurant->name.". Current estimated Pickup Time is: ".date('h:i A', strtotime($order->ready_time)).". Enjoy your meal.";
          }
    }
    }else if($type=='ready_for_pickup'){
        $message="Yay! Your food is Ready for Pick Up! We hope you enjoy your meal.";
    }else if($type=='out_for_delivery'){
        $message="Yay! Your food is out for delivery! We hope you enjoy your meal.";
    }else if($type=='time_extend'){
        $message="We are so sorry. There is an unexpected delay to your order. The current pick up time is now ".date('h:i A', strtotime($order->ready_time));
    }
    $ph_number = preg_replace("/[^0-9]/", "", $order->phone);

    $bclient = new BandwidthLib\BandwidthClient($config);
    $messagingClient = $bclient->getMessaging()->getClient();
    //dd($messagingClient);
    $messagingAccountId = getenv('BANDWIDTH_ID');
    $body = new BandwidthLib\Messaging\Models\MessageRequest();
    $body->from = "4692240641";
    $body->to = array("$ph_number");
    $body->applicationId = "9f5da62f-eb1f-4a53-ab1c-30ed21756eac";
    $body->text = $message;
    
    try {
        $response = $messagingClient->createMessage($messagingAccountId, $body);
        if($response->getResult()->id){
            return true;
        }
    } catch (Exception $e) {
        print_r($e);
        return false;
    
    }
    
            }
        }
}
