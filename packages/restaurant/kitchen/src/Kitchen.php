<?php

namespace Restaurant\Kitchen;

use User;

class Kitchen
{
    /**
     * $kitchen object.
     */
    protected $kitchen;

    /**
     * Constructor.
     */
    public function __construct(\Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface $kitchen)
    {
        $this->kitchen = $kitchen;
    }

    /**
     * Returns count of kitchen.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return  0;
    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.kitchen.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->kitchen->pushCriteria(new \Litepie\Restaurant\Repositories\Criteria\KitchenUserCriteria());
        }

        $kitchen = $this->kitchen->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('kitchen::' . $view, compact('kitchen'))->render();
    }
}
