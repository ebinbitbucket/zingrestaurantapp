<?php

namespace Restaurant\Kitchen\Policies;

use Litepie\User\Contracts\UserPolicy;
use Restaurant\Kitchen\Models\Kitchen;

class KitchenPolicy
{

    /**
     * Determine if the given user can view the kitchen.
     *
     * @param UserPolicy $user
     * @param Kitchen $kitchen
     *
     * @return bool
     */
    public function view(UserPolicy $user, Kitchen $kitchen)
    {
        if ($user->canDo('kitchen.kitchen.view') && $user->isAdmin()) {
            return true;
        }

        return $kitchen->user_id == user_id() && $kitchen->user_type == user_type();
    }

    /**
     * Determine if the given user can create a kitchen.
     *
     * @param UserPolicy $user
     * @param Kitchen $kitchen
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('kitchen.kitchen.create');
    }

    /**
     * Determine if the given user can update the given kitchen.
     *
     * @param UserPolicy $user
     * @param Kitchen $kitchen
     *
     * @return bool
     */
    public function update(UserPolicy $user, Kitchen $kitchen)
    {
        if ($user->canDo('kitchen.kitchen.edit') && $user->isAdmin() ) {
            return true;
        }
         if ($user->canDo('kitchen.kitchen.edit') && $user->isRestaurant() && $kitchen->restaurant_id == user_id()) {
            return true;
        }

        return $kitchen->user_id == user_id() && $kitchen->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given kitchen.
     *
     * @param UserPolicy $user
     * @param Kitchen $kitchen
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Kitchen $kitchen)
    {
        return $kitchen->user_id == user_id() && $kitchen->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given kitchen.
     *
     * @param UserPolicy $user
     * @param Kitchen $kitchen
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Kitchen $kitchen)
    {
        if ($user->canDo('kitchen.kitchen.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given kitchen.
     *
     * @param UserPolicy $user
     * @param Kitchen $kitchen
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Kitchen $kitchen)
    {
        if ($user->canDo('kitchen.kitchen.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
