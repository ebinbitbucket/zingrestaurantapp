<?php

namespace Restaurant\Kitchen\Providers;

use Illuminate\Support\ServiceProvider;

class KitchenServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'kitchen');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'kitchen');

        // Load migrations
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        // Call pblish redources function
        $this->publishResources();

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Bind facade
        $this->app->bind('restaurant.kitchen', function ($app) {
            return $this->app->make('Restaurant\Kitchen\Kitchen');
        });

                // Bind Kitchen to repository
        $this->app->bind(
            'Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface',
            \Restaurant\Kitchen\Repositories\Eloquent\KitchenRepository::class
        );

        $this->app->register(\Restaurant\Kitchen\Providers\AuthServiceProvider::class);
        
        $this->app->register(\Restaurant\Kitchen\Providers\RouteServiceProvider::class);
                
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['restaurant.kitchen'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../config/config.php' => config_path('restaurant/kitchen.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../resources/views' => base_path('resources/views/vendor/kitchen')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../resources/lang' => base_path('resources/lang/vendor/kitchen')], 'lang');

        // Publish public files and assets.
        $this->publishes([__DIR__ . '/public/' => public_path('/')], 'public');
    }
}
