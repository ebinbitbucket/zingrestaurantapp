<?php

namespace Restaurant;

use DB;
use Illuminate\Database\Seeder;

class KitchenTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('kitchens')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'kitchen.kitchen.view',
                'name'      => 'View Kitchen',
            ],
            [
                'slug'      => 'kitchen.kitchen.create',
                'name'      => 'Create Kitchen',
            ],
            [
                'slug'      => 'kitchen.kitchen.edit',
                'name'      => 'Update Kitchen',
            ],
            [
                'slug'      => 'kitchen.kitchen.delete',
                'name'      => 'Delete Kitchen',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/kitchen/kitchen',
                'name'        => 'Kitchen',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/kitchen/kitchen',
                'name'        => 'Kitchen',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'kitchen',
                'name'        => 'Kitchen',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Kitchen',
                'module'    => 'Kitchen',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'kitchen.kitchen.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
