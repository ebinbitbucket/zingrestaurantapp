<?php

// Resource routes  for kitchen
Route::group(['prefix' => set_route_guard('web').'/kitchen'], function () {
	Route::get('menu', 'KitchenPublicController@menu');
	Route::get('restaurant/kitchen', 'KitchenResourceController@restaurant_kitchens');
	Route::get('restaurant/create', 'KitchenResourceController@kitchen_create');
	Route::get('edit/{key?}', 'KitchenResourceController@kitchen_edit');
	Route::get('kitchen_dashboard/{slug?}', 'KitchenResourceController@kitchen_dashboard');
    Route::resource('kitchen', 'KitchenResourceController');
});

// Public  routes for kitchen
Route::get('kitchen/autologin/{api_token}', 'KitchenPublicController@kitchenLogin');
Route::get('kitchen/checklogin/{api_token}', 'KitchenPublicController@checkLogin');
Route::get('kitchen/popular/{period?}', 'KitchenPublicController@popular');
Route::get('kitchens/', 'KitchenPublicController@index');
Route::get('kitchens/{slug?}', 'KitchenPublicController@show');

