<?php

// Resource routes  for kitchen
Route::group(['prefix' => set_route_guard('web').'/kitchen'], function () {
	Route::get('menu', 'KitchenPublicController@menu');
	Route::get('restaurant/kitchen', 'KitchenResourceController@restaurant_kitchens');
	Route::get('restaurant/create', 'KitchenResourceController@kitchen_create');
	Route::get('edit/{key?}', 'KitchenResourceController@kitchen_edit');
	Route::get('kitchen_dashboard/{slug?}', 'KitchenPublicController@kitchen_dashboard');
	Route::get('restaurant/kitchen-status/{id?}', 'KitchenResourceController@kitchenStatus');
	Route::get('login-restaurant', 'KitchenResourceController@loginAs');
    Route::resource('kitchen', 'KitchenResourceController');
});

// Public  routes for kitchen
Route::get('kitchen/autologin/{api_token}', 'KitchenPublicController@kitchenLogin');
Route::get('kitchen/checklogin/{api_token}', 'KitchenPublicController@checkLogin');
Route::get('kitchen/popular/{period?}', 'KitchenPublicController@popular');
Route::get('kitchens/', 'KitchenPublicController@index');
Route::get('kitchens/{slug?}', 'KitchenPublicController@show');
Route::get('restaurant/kitchen/kitchen_dashboard/{slug?}', 'KitchenPublicController@kitchen_dashboard');
Route::get('restaurant/kitchen-status/{id?}', 'KitchenPublicController@kitchenStatus');
