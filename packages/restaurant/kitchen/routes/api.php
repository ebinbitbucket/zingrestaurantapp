<?php
Route::group(['prefix' => 'api'], function () {
    Route::group(['prefix' => set_route_guard('api') . '/cart'], function () {
        Route::middleware('auth:' . set_route_guard('api') . '.api')->group(function () {
           
            Route::get('orders', 'KitchenApiController@todaysOrders');
            Route::get('order/{id}', 'KitchenApiController@orderDetail');
            Route::get('order-update/{id}', 'KitchenApiController@orderUpdate');
            Route::get('order/{status}/{id}', 'KitchenApiController@kitchenStatus');
            Route::get('login-restaurant', 'KitchenApiController@loginAs');
            Route::get('order/delay/{id}/{time}', 'KitchenApiController@kitchenTimeUpdate');


        });
    });
    Route::get('kitchen/token-check', 'KitchenPublicController@tokenCheck');
});
