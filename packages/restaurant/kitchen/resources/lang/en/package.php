<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Kitchen package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for kitchen module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Kitchen',
    'names'         => 'Kitchens',
];
