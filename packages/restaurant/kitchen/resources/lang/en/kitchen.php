<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for kitchen in kitchen package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  kitchen module in kitchen package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Kitchen',
    'names'         => 'Kitchens',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Kitchens',
        'sub'   => 'Kitchens',
        'list'  => 'List of kitchens',
        'edit'  => 'Edit kitchen',
        'create'    => 'Create new kitchen'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'restaurant_id'              => 'Please select restaurant id',
        'name'                       => 'Please enter name',
        'description'                => 'Please enter description',
        'email'                      => 'Please enter email',
        'password'                   => 'Please enter password',
        'api_token'                  => 'Please enter api token',
        'remember_token'             => 'Please enter remember token',
        'slug'                       => 'Please enter slug',
        'user_id'                    => 'Please enter user id',
        'user_type'                  => 'Please enter user type',
        'created_at'                 => 'Please select created at',
        'deleted_at'                 => 'Please select deleted at',
        'updated_at'                 => 'Please select updated at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant id',
        'name'                       => 'Name',
        'description'                => 'Description',
        'email'                      => 'Email',
        'password'                   => 'Password',
        'api_token'                  => 'Api token',
        'remember_token'             => 'Remember token',
        'slug'                       => 'Slug',
        'user_id'                    => 'User id',
        'user_type'                  => 'User type',
        'created_at'                 => 'Created at',
        'deleted_at'                 => 'Deleted at',
        'updated_at'                 => 'Updated at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'restaurant_id'              => ['name' => 'Restaurant id', 'data-column' => 1, 'checked'],
        'name'                       => ['name' => 'Name', 'data-column' => 2, 'checked'],
        'description'                => ['name' => 'Description', 'data-column' => 3, 'checked'],
        'email'                      => ['name' => 'Email', 'data-column' => 4, 'checked'],
        'password'                   => ['name' => 'Password', 'data-column' => 5, 'checked'],
        'api_token'                  => ['name' => 'Api token', 'data-column' => 6, 'checked'],
        'remember_token'             => ['name' => 'Remember token', 'data-column' => 7, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Kitchens',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
