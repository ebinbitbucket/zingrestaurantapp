            @include('kitchen::public.kitchen.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('kitchen::public.kitchen.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($kitchen->defaultImage('images' , 'xl'))!!}" alt="{{$kitchen->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('kitchen::kitchen.label.id') !!}
                </label><br />
                    {!! $kitchen['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="restaurant_id">
                    {!! trans('kitchen::kitchen.label.restaurant_id') !!}
                </label><br />
                    {!! $kitchen['restaurant_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('kitchen::kitchen.label.name') !!}
                </label><br />
                    {!! $kitchen['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="description">
                    {!! trans('kitchen::kitchen.label.description') !!}
                </label><br />
                    {!! $kitchen['description'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="email">
                    {!! trans('kitchen::kitchen.label.email') !!}
                </label><br />
                    {!! $kitchen['email'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="password">
                    {!! trans('kitchen::kitchen.label.password') !!}
                </label><br />
                    {!! $kitchen['password'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="api_token">
                    {!! trans('kitchen::kitchen.label.api_token') !!}
                </label><br />
                    {!! $kitchen['api_token'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="remember_token">
                    {!! trans('kitchen::kitchen.label.remember_token') !!}
                </label><br />
                    {!! $kitchen['remember_token'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="slug">
                    {!! trans('kitchen::kitchen.label.slug') !!}
                </label><br />
                    {!! $kitchen['slug'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_id">
                    {!! trans('kitchen::kitchen.label.user_id') !!}
                </label><br />
                    {!! $kitchen['user_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_type">
                    {!! trans('kitchen::kitchen.label.user_type') !!}
                </label><br />
                    {!! $kitchen['user_type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('kitchen::kitchen.label.created_at') !!}
                </label><br />
                    {!! $kitchen['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('kitchen::kitchen.label.deleted_at') !!}
                </label><br />
                    {!! $kitchen['deleted_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('kitchen::kitchen.label.updated_at') !!}
                </label><br />
                    {!! $kitchen['updated_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('restaurant_id')
                    -> options(trans('kitchen::kitchen.options.restaurant_id'))
                    -> label(trans('kitchen::kitchen.label.restaurant_id'))
                    -> required()
                    -> placeholder(trans('kitchen::kitchen.placeholder.restaurant_id'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('kitchen::kitchen.label.name'))
                       -> placeholder(trans('kitchen::kitchen.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('description')
                    -> label(trans('kitchen::kitchen.label.description'))
                    -> placeholder(trans('kitchen::kitchen.placeholder.description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::email('email')
                       -> label(trans('kitchen::kitchen.label.email'))
                       -> required()
                       -> placeholder(trans('kitchen::kitchen.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::password('password')
                       -> label(trans('kitchen::kitchen.label.password'))
                       -> required()
                       -> placeholder(trans('kitchen::kitchen.placeholder.password'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('api_token')
                       -> label(trans('kitchen::kitchen.label.api_token'))
                       -> placeholder(trans('kitchen::kitchen.placeholder.api_token'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('remember_token')
                       -> label(trans('kitchen::kitchen.label.remember_token'))
                       -> placeholder(trans('kitchen::kitchen.placeholder.remember_token'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



