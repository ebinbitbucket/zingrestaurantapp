    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#kitchen" data-toggle="tab">{!! trans('kitchen::kitchen.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#kitchen-kitchen-edit'  data-load-to='#kitchen-kitchen-entry' data-datatable='#kitchen-kitchen-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#kitchen-kitchen-entry' data-href='{{guard_url('kitchen/kitchen')}}/{{$kitchen->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('kitchen-kitchen-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('kitchen/kitchen/'. $kitchen->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="kitchen">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('kitchen::kitchen.name') !!} [{!!$kitchen->name!!}] </div>
                @include('kitchen::admin.kitchen.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>