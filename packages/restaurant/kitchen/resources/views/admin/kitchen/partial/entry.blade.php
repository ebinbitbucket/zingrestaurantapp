            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('restaurant_id')
                    -> options(Restaurant::getRestaurant())
                    -> label(trans('kitchen::kitchen.label.restaurant_id'))
                    -> required()
                    -> placeholder(trans('kitchen::kitchen.placeholder.restaurant_id'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('kitchen::kitchen.label.name'))
                       -> placeholder(trans('kitchen::kitchen.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('description')
                    -> label(trans('kitchen::kitchen.label.description'))
                    -> placeholder(trans('kitchen::kitchen.placeholder.description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::email('email')
                       -> label(trans('kitchen::kitchen.label.email'))
                       -> required()
                       -> placeholder(trans('kitchen::kitchen.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::password('password')
                       -> label(trans('kitchen::kitchen.label.password'))
                       -> required()
                       -> placeholder(trans('kitchen::kitchen.placeholder.password'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                     {!! Form::text('fcm')
                     -> label('Fcm')
                     -> placeholder('Fcm')!!}
              </div>
              
            </div>