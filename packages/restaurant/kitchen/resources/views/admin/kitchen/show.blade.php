    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('kitchen::kitchen.name') !!}</a></li>
            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#kitchen-kitchen-entry' data-href='{{guard_url('kitchen/kitchen/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($kitchen->id )
                <?php
                                                        $parameter= Crypt::encrypt($kitchen->id);
                                                    ?>
                <a  href='{{ guard_url('kitchen/kitchen_dashboard') }}/{{$parameter}}' target="_blank"><button type="button" class="btn btn-primary btn-sm">Login As</button></a>
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#kitchen-kitchen-entry' data-href='{{ guard_url('kitchen/kitchen') }}/{{$kitchen->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#kitchen-kitchen-entry' data-datatable='#kitchen-kitchen-list' data-href='{{ guard_url('kitchen/kitchen') }}/{{$kitchen->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('kitchen-kitchen-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('kitchen/kitchen'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('kitchen::kitchen.name') !!}  [{!! $kitchen->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('kitchen::admin.kitchen.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>