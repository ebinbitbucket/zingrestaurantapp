Lavalite package that provides kitchen management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `restaurant/kitchen`.

    "restaurant/kitchen": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Restaurant\Kitchen\Providers\KitchenServiceProvider::class,

And also add it to alias

    'Kitchen'  => Restaurant\Kitchen\Facades\Kitchen::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Restaurant\\KitchenTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Restaurant\Kitchen\Providers\KitchenServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Restaurant\Kitchen\Providers\KitchenServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Restaurant\Kitchen\Providers\KitchenServiceProvider" --tag="view"


## Usage


