<?php

namespace Restaurant\Holiday;

use DB;
use Illuminate\Database\Seeder;

class HolidayTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('holidays')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'holiday.holiday.view',
                'name'      => 'View Holiday',
            ],
            [
                'slug'      => 'holiday.holiday.create',
                'name'      => 'Create Holiday',
            ],
            [
                'slug'      => 'holiday.holiday.edit',
                'name'      => 'Update Holiday',
            ],
            [
                'slug'      => 'holiday.holiday.delete',
                'name'      => 'Delete Holiday',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/holiday/holiday',
                'name'        => 'Holiday',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/holiday/holiday',
                'name'        => 'Holiday',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'holiday',
                'name'        => 'Holiday',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Holiday',
                'module'    => 'Holiday',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'holiday.holiday.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
