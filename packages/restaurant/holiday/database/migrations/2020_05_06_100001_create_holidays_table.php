<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateHolidaysTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: holidays
         */
        Schema::create('holidays', function ($table) {
            $table->increments('id');
            $table->integer('restaurant_id')->nullable();
            $table->string('name', 255)->nullable();
            $table->date('date')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('user_type', 60)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('holidays');
    }
}
