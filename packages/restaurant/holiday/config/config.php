<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'restaurant',

    /*
     * Package.
     */
    'package'   => 'holiday',

    /*
     * Modules.
     */
    'modules'   => ['holiday'],

    
    'holiday'       => [
        'model' => [
            'model'                 => \Restaurant\Holiday\Models\Holiday::class,
            'table'                 => 'holidays',
            'presenter'             => \Restaurant\Holiday\Repositories\Presenter\HolidayPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'name',  'date',  'user_id',  'user_type',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'holiday/holiday',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'restaurant_id'  => '=',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Holiday',
            'module'    => 'Holiday',
        ],

    ],
];
