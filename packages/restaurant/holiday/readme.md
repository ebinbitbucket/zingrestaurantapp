Lavalite package that provides holiday management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `restaurant/holiday`.

    "restaurant/holiday": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Restaurant\Holiday\Providers\HolidayServiceProvider::class,

And also add it to alias

    'Holiday'  => Restaurant\Holiday\Facades\Holiday::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Restaurant\\HolidayTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Restaurant\Holiday\Providers\HolidayServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Restaurant\Holiday\Providers\HolidayServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Restaurant\Holiday\Providers\HolidayServiceProvider" --tag="view"


## Usage


