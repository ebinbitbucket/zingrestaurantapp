<?php

namespace Restaurant\Holiday\Providers;

use Illuminate\Support\ServiceProvider;

class HolidayServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'holiday');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'holiday');

        // Load migrations
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        // Call pblish redources function
        $this->publishResources();

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();
        $this->registerHoliday();
        $this->registerFacade();
        $this->registerBindings();
        //$this->registerCommands();
    }


    /**
     * Register the application bindings.
     *
     * @return void
     */
    protected function registerHoliday()
    {
        $this->app->bind('holiday', function($app) {
            return new Holiday($app);
        });
    }

    /**
     * Register the vault facade without the user having to add it to the app.php file.
     *
     * @return void
     */
    public function registerFacade() {
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Holiday', 'Restaurant\Holiday\Facades\Holiday');
        });
    }

    /**
     * Register bindings for the provider.
     *
     * @return void
     */
    public function registerBindings() {
        // Bind facade
        $this->app->bind('restaurant.holiday', function ($app) {
            return $this->app->make('Restaurant\Holiday\Holiday');
        });

                // Bind Holiday to repository
        $this->app->bind(
            'Restaurant\Holiday\Interfaces\HolidayRepositoryInterface',
            \Restaurant\Holiday\Repositories\Eloquent\HolidayRepository::class
        );

        $this->app->register(\Restaurant\Holiday\Providers\AuthServiceProvider::class);
        
        $this->app->register(\Restaurant\Holiday\Providers\RouteServiceProvider::class);
            }

    /**
     * Merges user's and holiday's configs.
     *
     * @return void
     */
    protected function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php', 'restaurant.holiday'
        );
    }

    /**
     * Register scaffolding command
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\MakeHoliday::class,
            ]);
        }
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['restaurant.holiday'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../config/config.php' => config_path('restaurant/holiday.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../resources/views' => base_path('resources/views/vendor/holiday')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../resources/lang' => base_path('resources/lang/vendor/holiday')], 'lang');

        // Publish public files and assets.
        $this->publishes([__DIR__ . '/public/' => public_path('/')], 'public');
    }
}
