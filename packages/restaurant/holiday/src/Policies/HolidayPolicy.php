<?php

namespace Restaurant\Holiday\Policies;

use Litepie\User\Contracts\UserPolicy;
use Restaurant\Holiday\Models\Holiday;

class HolidayPolicy
{

    /**
     * Determine if the given user can view the holiday.
     *
     * @param UserPolicy $user
     * @param Holiday $holiday
     *
     * @return bool
     */
    public function view(UserPolicy $user, Holiday $holiday)
    {dd('hi');
        // if ($user->canDo('holiday.holiday.view') && $user->isAdmin()) {
            return true;
        // }

        return $holiday->user_id == user_id() && $holiday->user_type == user_type();
    }

    /**
     * Determine if the given user can create a holiday.
     *
     * @param UserPolicy $user
     * @param Holiday $holiday
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('holiday.holiday.create');
    }

    /**
     * Determine if the given user can update the given holiday.
     *
     * @param UserPolicy $user
     * @param Holiday $holiday
     *
     * @return bool
     */
    public function update(UserPolicy $user, Holiday $holiday)
    {
        if ($user->canDo('holiday.holiday.edit') && $user->isAdmin()) {
            return true;
        }

        return $holiday->user_id == user_id() && $holiday->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given holiday.
     *
     * @param UserPolicy $user
     * @param Holiday $holiday
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Holiday $holiday)
    {
        return $holiday->user_id == user_id() && $holiday->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given holiday.
     *
     * @param UserPolicy $user
     * @param Holiday $holiday
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Holiday $holiday)
    {
        if ($user->canDo('holiday.holiday.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given holiday.
     *
     * @param UserPolicy $user
     * @param Holiday $holiday
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Holiday $holiday)
    {
        if ($user->canDo('holiday.holiday.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
