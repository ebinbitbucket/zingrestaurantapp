<?php

namespace Restaurant\Holiday;

use User;

class Holiday
{
    /**
     * $holiday object.
     */
    protected $holiday;

    /**
     * Constructor.
     */
    public function __construct(\Restaurant\Holiday\Interfaces\HolidayRepositoryInterface $holiday)
    {
        $this->holiday = $holiday;
    }

    /**
     * Returns count of holiday.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return  0;
    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.holiday.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->holiday->pushCriteria(new \Litepie\Restaurant\Repositories\Criteria\HolidayUserCriteria());
        }

        $holiday = $this->holiday->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('holiday::' . $view, compact('holiday'))->render();
    }
}
