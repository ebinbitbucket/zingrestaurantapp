<?php

namespace Restaurant\Holiday\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class HolidayTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Holiday\Models\Holiday $holiday)
    {
        return [
            'id'                => $holiday->getRouteKey(),
            'key'               => [
                'public'    => $holiday->getPublicKey(),
                'route'     => $holiday->getRouteKey(),
            ], 
            // 'id'                => $holiday->id,
            'restaurant_id'     => @$holiday->restaurant->name,
            'name'              => $holiday->name,
            'date'              => $holiday->date,
            'user_id'           => $holiday->user_id,
            'user_type'         => $holiday->user_type,
            'created_at'        => $holiday->created_at,
            'updated_at'        => $holiday->updated_at,
            'deleted_at'        => $holiday->deleted_at,
            'url'               => [
                'public'    => trans_url('holiday/'.$holiday->getPublicKey()),
                'user'      => guard_url('holiday/holiday/'.$holiday->getRouteKey()),
            ], 
            'status'            => trans('app.'.$holiday->status),
            'created_at'        => format_date($holiday->created_at),
            'updated_at'        => format_date($holiday->updated_at),
        ];
    }
}