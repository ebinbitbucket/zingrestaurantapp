<?php

namespace Restaurant\Holiday\Repositories\Eloquent;

use Restaurant\Holiday\Interfaces\HolidayRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class HolidayRepository extends BaseRepository implements HolidayRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.holiday.holiday.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.holiday.holiday.model.model');
    }
}
