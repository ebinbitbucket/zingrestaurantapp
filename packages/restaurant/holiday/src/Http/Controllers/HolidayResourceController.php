<?php

namespace Restaurant\Holiday\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Holiday\Http\Requests\HolidayRequest;
use Restaurant\Holiday\Interfaces\HolidayRepositoryInterface;
use Restaurant\Holiday\Models\Holiday;

/**
 * Resource controller class for holiday.
 */
class HolidayResourceController extends BaseController
{

    /**
     * Initialize holiday resource controller.
     *
     * @param type HolidayRepositoryInterface $holiday
     *
     * @return null
     */
    public function __construct(HolidayRepositoryInterface $holiday)
    {
        parent::__construct();
        $this->repository = $holiday;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Holiday\Repositories\Criteria\HolidayResourceCriteria::class);
    }

    /**
     * Display a list of holiday.
     *
     * @return Response
     */
    public function index(HolidayRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Holiday\Repositories\Presenter\HolidayPresenter::class)
                ->$function();
        }

        $holidays = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('holiday::holiday.names'))
            ->view('holiday::holiday.index', true)
            ->data(compact('holidays', 'view'))
            ->output();
    }

    /**
     * Display holiday.
     *
     * @param Request $request
     * @param Model   $holiday
     *
     * @return Response
     */
    public function show(HolidayRequest $request, Holiday $holiday)
    {

        if ($holiday->exists) {
            $view = 'holiday::holiday.show';
        } else {
            $view = 'holiday::holiday.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('holiday::holiday.name'))
            ->data(compact('holiday'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new holiday.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(HolidayRequest $request)
    {

        $holiday = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('holiday::holiday.name')) 
            ->view('holiday::holiday.create', true) 
            ->data(compact('holiday'))
            ->output();
    }

    /**
     * Create new holiday.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(HolidayRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $attributes['date']      =date('Y-m-d',strtotime($attributes['date']));
            $holiday                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('holiday::holiday.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('holiday/holiday/' . $holiday->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/holiday/holiday'))
                ->redirect();
        }

    }

    /**
     * Show holiday for editing.
     *
     * @param Request $request
     * @param Model   $holiday
     *
     * @return Response
     */
    public function edit(HolidayRequest $request, Holiday $holiday)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('holiday::holiday.name'))
            ->view('holiday::holiday.edit', true)
            ->data(compact('holiday'))
            ->output();
    }

    /**
     * Update the holiday.
     *
     * @param Request $request
     * @param Model   $holiday
     *
     * @return Response
     */
    public function update(HolidayRequest $request, Holiday $holiday)
    {
        try {
            $attributes = $request->all();
            $attributes['date']      =date('Y-m-d',strtotime($attributes['date']));
            $holiday->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('holiday::holiday.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('holiday/holiday/' . $holiday->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('holiday/holiday/' . $holiday->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the holiday.
     *
     * @param Model   $holiday
     *
     * @return Response
     */
    public function destroy(HolidayRequest $request, Holiday $holiday)
    {
        try {

            $holiday->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('holiday::holiday.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('holiday/holiday/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('holiday/holiday/' . $holiday->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple holiday.
     *
     * @param Model   $holiday
     *
     * @return Response
     */
    public function delete(HolidayRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('holiday::holiday.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('holiday/holiday'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/holiday/holiday'))
                ->redirect();
        }

    }

    /**
     * Restore deleted holidays.
     *
     * @param Model   $holiday
     *
     * @return Response
     */
    public function restore(HolidayRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('holiday::holiday.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/holiday/holiday'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/holiday/holiday/'))
                ->redirect();
        }

    }

}
