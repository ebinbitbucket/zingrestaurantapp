<?php

namespace Restaurant\Holiday\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Holiday\Interfaces\HolidayRepositoryInterface;

class HolidayPublicController extends BaseController
{
    // use HolidayWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Holiday\Interfaces\HolidayRepositoryInterface $holiday
     *
     * @return type
     */
    public function __construct(HolidayRepositoryInterface $holiday)
    {
        $this->repository = $holiday;
        parent::__construct();
    }

    /**
     * Show holiday's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $holidays = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$holiday::holiday.names'))
            ->view('holiday::holiday.index')
            ->data(compact('holidays'))
            ->output();
    }


    /**
     * Show holiday.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $holiday = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$holiday->name . trans('holiday::holiday.name'))
            ->view('holiday::holiday.show')
            ->data(compact('holiday'))
            ->output();
    }

}
