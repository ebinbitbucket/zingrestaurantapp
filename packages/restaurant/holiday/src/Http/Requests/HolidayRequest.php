<?php

namespace Restaurant\Holiday\Http\Requests;

use App\Http\Requests\Request as FormRequest;

class HolidayRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->model = $this->route('holiday');

        if (is_null($this->model)) {
            // Determine if the user is authorized to access holiday module,
            // return $this->canAccess();
            return true;
        }

        if ($this->isWorkflow()) {
            // Determine if the user is authorized to change status of an entry,
            return $this->can($this->getStatus());
        }

        if ($this->isCreate() || $this->isStore()) {
            // Determine if the user is authorized to create an entry,
            // return $this->can('create');
            return true;
        }

        if ($this->isEdit() || $this->isUpdate()) {
            // Determine if the user is authorized to update an entry,
            return $this->can('update');
        }

        if ($this->isDelete()) {
            // Determine if the user is authorized to delete an entry,
            return $this->can('destroy');
        }

        // Determine if the user is authorized to view the module.
        // return $this->can('view');
        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isStore()) {
            // validation rule for create request.
            return [
                'date'=>'required|after:today',
            ];
        }

        if ($this->isUpdate()) {
            // Validation rule for update request.
            return [
                'date'=>'required|after:today',
            ];
        }

        // Default validation rule.
        return [

        ];
    }

    /**
     * Check whether the user can access the module.
     *
     * @return bool
     **/
    protected function canAccess()
    {
        return user()->canDo('holiday.holiday.view');
    }
}
