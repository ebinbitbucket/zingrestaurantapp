<?php

namespace Restaurant\Holiday\Facades;

use Illuminate\Support\Facades\Facade;

class Holiday extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'restaurant.holiday';
    }
}
