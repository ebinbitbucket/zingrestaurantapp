    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('holiday::holiday.name') !!}</a></li>
            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#holiday-holiday-entry' data-href='{{guard_url('holiday/holiday/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($holiday->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#holiday-holiday-entry' data-href='{{ guard_url('holiday/holiday') }}/{{$holiday->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#holiday-holiday-entry' data-datatable='#holiday-holiday-list' data-href='{{ guard_url('holiday/holiday') }}/{{$holiday->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('holiday-holiday-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('holiday/holiday'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('holiday::holiday.name') !!}  [{!! $holiday->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('holiday::admin.holiday.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>