    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#holiday" data-toggle="tab">{!! trans('holiday::holiday.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#holiday-holiday-edit'  data-load-to='#holiday-holiday-entry' data-datatable='#holiday-holiday-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#holiday-holiday-entry' data-href='{{guard_url('holiday/holiday')}}/{{$holiday->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('holiday-holiday-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('holiday/holiday/'. $holiday->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="holiday">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('holiday::holiday.name') !!} [{!!$holiday->name!!}] </div>
                @include('holiday::admin.holiday.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>