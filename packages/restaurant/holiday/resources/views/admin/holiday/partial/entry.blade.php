<div class='row'>
       <div class='col-md-4 col-sm-6'>
       {!! Form::select('restaurant_id')
              -> options(Expense::restaurants())
              -> label(trans('expense::expense.label.restaurant_id'))
              -> placeholder(trans('expense::expense.placeholder.restaurant_id'))!!}
       </div>
       <div class='col-md-4 col-sm-6'>
              {!! Form::text('name')
              -> label(trans('holiday::holiday.label.name'))
              -> placeholder(trans('holiday::holiday.placeholder.name'))!!}
       </div>

       <div class='col-md-4 col-sm-6'>
       <div class='form-group'>
       <label for='date' class='control-label'>{!!trans('holiday::holiday.label.date')!!}</label>
       <div class='input-group pickdate'>
              {!! Form::text('date')
              -> required('required')
              -> placeholder(trans('holiday::holiday.placeholder.date'))
              ->raw()!!}
              <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
       </div>
       </div>
       </div>
</div>