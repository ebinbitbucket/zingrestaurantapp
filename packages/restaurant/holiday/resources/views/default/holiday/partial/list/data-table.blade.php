            <table class="table" id="main-table" data-url="{!!guard_url('holiday/holiday?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="restaurant_id">{!! trans('holiday::holiday.label.restaurant_id')!!}</th>
                    <th data-field="name">{!! trans('holiday::holiday.label.name')!!}</th>
                    <th data-field="date">{!! trans('holiday::holiday.label.date')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>