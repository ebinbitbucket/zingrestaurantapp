
        </br>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="element-wrapper order-detail-wrap">
                    <div class="element-box">
                        <div class="element-info">
                            <div class="element-info-with-icon">
                                <div class="element-info-icon"><div class="icon ion-social-buffer"></div></div>
                                <div class="element-info-text">
                                    <h5 class="element-inner-header">Special Days</h5>
                                </div>
                                <!-- <div class="element-info-buttons element-add-buttons">
                                    <a href="{{guard_url('holidays/holidays/create')}}"><button  class="btn btn-theme ion-android-add add-addon" > </button></a>
                                </div> -->
                            </div>
                        </div>  
                        <div id="stmnt_div">
                               <table class="table">
                                <thead class="">
                                    <tr>
                                        <th>Sl No</th>
                                        <th>Name</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @php $sl_no=1; @endphp 
                                @forelse($holidays as $holiday) 
                                @if($holiday->restaurant_id==user()->id)
                                <tr>
                                    <td><p>{{$sl_no}}</a></p></td>
                                    <td><p>{{@$holiday->name}}</a></p></td>
                                    <td><p>{{@$holiday->date}}</a></p></td>
                                    <!-- <td><p>{{@$holiday->date}}</a></p></td> -->
                                </a>
                                </tr>
                                @php $sl_no=$sl_no+1; @endphp
                                @endif
                                @empty
                                @endif
                                <tr>
                                    <td colspan="5"><p>{{@$holidays->links()}}</p></td>
                                </tr>
                                </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
