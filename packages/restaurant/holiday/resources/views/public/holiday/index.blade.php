            @include('holiday::holiday.partial.header')

            <section class="grid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('holiday::holiday.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="main-area parent-border list-item">
                                @foreach($holidays as $holiday)
                                <div class="item border">
                                    <div class="feature">
                                        <a href="{{trans_url('holidays')}}/{{@$holiday['slug']}}">
                                            <img src="{{url($holiday->defaultImage('images'))}}" class="img-responsive center-block" alt="">
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h4><a href="{{trans_url('holiday')}}/{{$holiday['slug']}}">{{str_limit($holiday['title'], 300)}}</a> 
                                        </h4>
                                        <div class="metas mt20">
                                            <div class="tag pull-left">
                                                <a href="#" class="">Seo Tips</a>
                                            </div>
                                            <div class="date-time pull-right">
                                                <span><i class="fa fa-comments"></i>{{@$holiday->viewcount}}</span>
                                                <span><i class="fa fa-calendar"></i>{{format_date($holiday['posted_on'])}}</span>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="author">
                                            <div class="avatar pull-left">
                                                {{@$holiday->user->badge}}
                                            </div>
                                            <div class="actions">
                                                

                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                @endforeach
                            </div>
                            <div class="pagination text-center">
                            {{ $holidays->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </section> 