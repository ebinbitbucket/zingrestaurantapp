<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for holiday in holiday package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  holiday module in holiday package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Holiday',
    'names'         => 'Holidays',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Holidays',
        'sub'   => 'Holidays',
        'list'  => 'List of holidays',
        'edit'  => 'Edit holiday',
        'create'    => 'Create new holiday'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'restaurant_id'              => 'Please select restaurant',
        'name'                       => 'Please enter name',
        'date'                       => 'Please select date',
        'user_id'                    => 'Please enter user id',
        'user_type'                  => 'Please enter user type',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant',
        'name'                       => 'Name',
        'date'                       => 'Date',
        'user_id'                    => 'User id',
        'user_type'                  => 'User type',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'restaurant_id'              => ['name' => 'Restaurant id', 'data-column' => 1, 'checked'],
        'name'                       => ['name' => 'Name', 'data-column' => 2, 'checked'],
        'date'                       => ['name' => 'Date', 'data-column' => 3, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Holidays',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
