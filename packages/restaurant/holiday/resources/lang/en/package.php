<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Holiday package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for holiday module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Holiday',
    'names'         => 'Holidays',
];
