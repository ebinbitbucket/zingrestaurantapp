<?php

// Resource routes  for holiday
Route::group(['prefix' => set_route_guard('web').'/holiday'], function () {
    Route::resource('holiday', 'HolidayResourceController');
});

// Public  routes for holiday
Route::get('holiday/popular/{period?}', 'HolidayPublicController@popular');
Route::get('holidays/', 'HolidayPublicController@index');
Route::get('holidays/{slug?}', 'HolidayPublicController@show');

