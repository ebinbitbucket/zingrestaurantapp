<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateMenusTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: menus
         */
        Schema::create('menus', function ($table) {
            $table->increments('id');
            $table->integer('restaurant_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('name')->nullable();
            $table->string('description', 255)->nullable();
            $table->float('price')->nullable();
            $table->text('image')->nullable();
            $table->text('addons')->nullable();
            $table->string('slug', 255)->nullable();
            $table->enum('status', ['Show','Hide'])->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('menus');
    }
}
