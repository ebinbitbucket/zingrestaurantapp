<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: restaurants
         */
        Schema::create('restaurants', function ($table) {
            $table->increments('id');
            $table->string('name', 255)->nullable();
            $table->string('description', 255)->nullable();
            $table->text('address')->nullable();
            $table->string('phone', 25)->nullable();
            $table->string('email', 255)->nullable();
            $table->enum('price_range', ['1','2','3','4','5'])->nullable();
            $table->string('country', 255)->nullable();
            $table->string('city', 255)->nullable();
            $table->string('state', 255)->nullable();
            $table->string('location', 255)->nullable();
            $table->integer('zipcode')->nullable();
            $table->string('latitude', 255)->nullable();
            $table->string('longitude', 255)->nullable();
            $table->integer('rating')->nullable();
            $table->text('logo')->nullable();
            $table->text('gallery')->nullable();
            $table->enum('delivery', ['Yes', 'No'])->nullable();
            $table->enum('type', ['Indian', 'Chinese', 'Malabar', 'Italian', 'Arabic', 'Mexican'])->nullable();
            $table->text('working_hours')->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('restaurants');
    }
}
