<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateAddonsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: addons
         */
        Schema::create('addons', function ($table) {
            $table->increments('id');
            $table->integer('restaurant_id')->nullable();
            $table->string('name', 255)->nullable();
            $table->float('price')->nullable();
            $table->string('slug', 255)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('addons');
    }
}
