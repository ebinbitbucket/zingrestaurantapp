<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateReviewsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: reviews
         */
        Schema::create('reviews', function ($table) {
            $table->increments('id');
            $table->integer('customer_id')->nullable();
            $table->integer('order_id')->nullable();
            $table->integer('rating')->nullable();
            $table->string('review', 255)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('reviews');
    }
}
