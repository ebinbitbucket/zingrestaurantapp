<?php

namespace Restaurant\Restaurant;

use DB;
use Illuminate\Database\Seeder;

class AddonTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('addons')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'restaurant.addon.view',
                'name'      => 'View Addon',
            ],
            [
                'slug'      => 'restaurant.addon.create',
                'name'      => 'Create Addon',
            ],
            [
                'slug'      => 'restaurant.addon.edit',
                'name'      => 'Update Addon',
            ],
            [
                'slug'      => 'restaurant.addon.delete',
                'name'      => 'Delete Addon',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/restaurant/addon',
                'name'        => 'Addon',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/restaurant/addon',
                'name'        => 'Addon',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'addon',
                'name'        => 'Addon',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Restaurant',
                'module'    => 'Addon',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'restaurant.addon.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
