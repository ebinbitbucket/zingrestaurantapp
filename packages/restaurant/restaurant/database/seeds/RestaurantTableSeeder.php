<?php

namespace Restaurant\Restaurant;

use DB;
use Illuminate\Database\Seeder;

class RestaurantTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('restaurants')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'restaurant.restaurant.view',
                'name'      => 'View Restaurant',
            ],
            [
                'slug'      => 'restaurant.restaurant.create',
                'name'      => 'Create Restaurant',
            ],
            [
                'slug'      => 'restaurant.restaurant.edit',
                'name'      => 'Update Restaurant',
            ],
            [
                'slug'      => 'restaurant.restaurant.delete',
                'name'      => 'Delete Restaurant',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/restaurant/restaurant',
                'name'        => 'Restaurant',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/restaurant/restaurant',
                'name'        => 'Restaurant',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'restaurant',
                'name'        => 'Restaurant',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Restaurant',
                'module'    => 'Restaurant',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'restaurant.restaurant.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
