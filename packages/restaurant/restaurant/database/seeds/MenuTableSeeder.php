<?php

namespace Restaurant\Restaurant;

use DB;
use Illuminate\Database\Seeder;

class MenuTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('restauarnt_menus')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'restaurant.menu.view',
                'name'      => 'View Menu',
            ],
            [
                'slug'      => 'restaurant.menu.create',
                'name'      => 'Create Menu',
            ],
            [
                'slug'      => 'restaurant.menu.edit',
                'name'      => 'Update Menu',
            ],
            [
                'slug'      => 'restaurant.menu.delete',
                'name'      => 'Delete Menu',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/restaurant/menu',
                'name'        => 'Menu',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/restaurant/menu',
                'name'        => 'Menu',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'menu',
                'name'        => 'Menu',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Restaurant',
                'module'    => 'Menu',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'restaurant.menu.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
