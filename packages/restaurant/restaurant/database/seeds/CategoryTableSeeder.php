<?php

namespace Restaurant\Restaurant;

use DB;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('restauarnt_categories')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'restaurant.category.view',
                'name'      => 'View Category',
            ],
            [
                'slug'      => 'restaurant.category.create',
                'name'      => 'Create Category',
            ],
            [
                'slug'      => 'restaurant.category.edit',
                'name'      => 'Update Category',
            ],
            [
                'slug'      => 'restaurant.category.delete',
                'name'      => 'Delete Category',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/restaurant/category',
                'name'        => 'Category',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/restaurant/category',
                'name'        => 'Category',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'category',
                'name'        => 'Category',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Restaurant',
                'module'    => 'Category',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'restaurant.category.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
