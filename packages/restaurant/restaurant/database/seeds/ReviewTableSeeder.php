<?php

namespace Restaurant\Restaurant;

use DB;
use Illuminate\Database\Seeder;

class ReviewTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('reviews')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'restaurant.review.view',
                'name'      => 'View Review',
            ],
            [
                'slug'      => 'restaurant.review.create',
                'name'      => 'Create Review',
            ],
            [
                'slug'      => 'restaurant.review.edit',
                'name'      => 'Update Review',
            ],
            [
                'slug'      => 'restaurant.review.delete',
                'name'      => 'Delete Review',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/restaurant/review',
                'name'        => 'Review',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/restaurant/review',
                'name'        => 'Review',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'review',
                'name'        => 'Review',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Restaurant',
                'module'    => 'Review',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'restaurant.review.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
