<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for addon in restaurant package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  addon module in restaurant package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Addon',
    'names'         => 'Addons',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Addons',
        'sub'   => 'Addons',
        'list'  => 'List of addons',
        'edit'  => 'Edit addon',
        'create'    => 'Create new addon'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'restaurant_id'              => 'Please enter restaurant',
        'name'                       => 'Please enter name',
        'price'                      => 'Please enter price',
        'slug'                       => 'Please enter slug',
        'created_at'                 => 'Please select created at',
        'deleted_at'                 => 'Please select deleted at',
        'updated_at'                 => 'Please select updated at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant ',
        'name'                       => 'Name',
        'price'                      => 'Price',
        'slug'                       => 'Slug',
        'created_at'                 => 'Created at',
        'deleted_at'                 => 'Deleted at',
        'updated_at'                 => 'Updated at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'restaurant_id'              => ['name' => 'Restaurant id', 'data-column' => 1, 'checked'],
        'name'                       => ['name' => 'Name', 'data-column' => 2, 'checked'],
        'price'                      => ['name' => 'Price', 'data-column' => 3, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Addons',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
