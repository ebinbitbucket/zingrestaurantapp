<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for menu in restaurant package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  menu module in restaurant package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Menu',
    'names'         => 'Menus',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Menus',
        'sub'   => 'Menus',
        'list'  => 'List of menus',
        'edit'  => 'Edit menu',
        'create'    => 'Create new menu'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            'status'              => ['Show' => 'Show','Hide' =>  'Hide'],
            'required'            => ['Yes' => 'Yes','No' =>  'No'],
            'timing'              => ['mon' => 'Monday','tue' => 'Tuesday','wed' => 'Wednesday','thu' => 'Thursday','fri' => 'Friday','sat' => 'Saturday','sun' => 'Sunday'],
            'home_show'           => ['Yes' => 'Yes','No' =>  'No'],
            'stock_status'        => ['1' => 'On','0' =>  'Off'],
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'restaurant_id'              => 'Please enter restaurant',
        'category_id'                => 'Please enter category',
        'name'                       => 'Please enter menu name',
        'description'                => 'Please enter description',
        'price'                      => 'Please enter price',
        'image'                      => 'Please enter image',
        'addons'                     => 'Please enter addon details',
        'slug'                       => 'Please enter slug',
        'status'                     => 'Please select status',
        'created_at'                 => 'Please select created at',
        'deleted_at'                 => 'Please select deleted at',
        'updated_at'                 => 'Please select updated at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant',
        'category_id'                => 'Category',

        'name'                       => 'Menu Name',
        'description'                => 'Description',
        'price'                      => 'Price',
        'image'                      => 'Image',
        'addons'                     => 'Addon details',
        'slug'                       => 'Slug',
        'status'                     => 'Status',
        'created_at'                 => 'Created at',
        'deleted_at'                 => 'Deleted at',
        'updated_at'                 => 'Updated at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'restaurant_id'              => ['name' => 'Restaurant id', 'data-column' => 1, 'checked'],
        'category_id'                => ['name' => 'Category id', 'data-column' => 2, 'checked'],
        'name'                       => ['name' => 'Name', 'data-column' => 3, 'checked'],
        'price'                      => ['name' => 'Price', 'data-column' => 4, 'checked'],
        'addons'                     => ['name' => 'Addon details', 'data-column' => 5, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Menus',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
