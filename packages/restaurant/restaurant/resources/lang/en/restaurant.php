<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for restaurant in restaurant package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  restaurant module in restaurant package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Restaurant',
    'names'         => 'Restaurants',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Restaurants',
        'sub'   => 'Restaurants',
        'list'  => 'List of restaurants',
        'edit'  => 'Edit restaurant',
        'create'    => 'Create new restaurant',
         'transaction-log'    => 'Transaction Log'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            'price_range'         => ['1','2','3','4','5'],
            'delivery'            => ['Yes'=> 'Yes', 'No'=>'No'],
            'flag_special_instr'            => ['Yes'=> 'Yes', 'No'=>'No'],
            'catering'            => ['Yes'=> 'Yes', 'No'=>'No'],
            'chain_list'            => ['Yes'=> 'Yes', 'No'=>'No'],
            'published'            => ['Select Status','No Sale'=> 'No Sale', 'Unpublished'=>'Unpublished'],
            'ESign'               => ['Yes'=> 'Yes', 'No'=>'No'],
            'kitchen_view'               => ['basic'=> 'basic', 'advanced'=>'advanced'],
            'type'                => ['American' => 'American','Beer and Wine' => 'Beer and Wine','Burgers' => 'Burgers','Chinese' => 'Chinese','Coffee' => 'Coffee','Indian' => 'Indian','Italian' => 'Italian','Juices' => 'Juices',],
            'category_name'                => ['Restaurant' => 'Restaurant', 'Bakery' => 'Bakery', 'Food Truck' => 'Food Truck', 'Farmers Market' => 'Farmers Market' , 'Catering' => 'Catering'],
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'name'                       => 'Please enter name',
        'slug'                       => 'Please enter slug',
        'description'                => 'Please enter description',
        'address'                    => 'Please enter address',
        'phone'                      => 'Please enter phone',
        'email'                      => 'Please enter email',
        'price_range_min'            => 'Please enter minimum price range',
        'price_range_max'            => 'Please enter maximum price range',
        'keywords'                   => 'Please enter search keywords',
        'zipcode'                    => 'Please enter zipcode',
        'latitude'                   => 'Please enter latitude',
        'longitude'                  => 'Please enter longitude',
        'rating'                     => 'Please enter rating',
        'logo'                       => 'Please enter logo',
        'offer'                       => 'Please enter offer',
        'gallery'                    => 'Please enter images',
        'delivery'                   => 'Please choose home delivery',
        'type'                       => 'Please enter restaurant type',
        'created_at'                 => 'Please select created at',
        'deleted_at'                 => 'Please select deleted at',
        'updated_at'                 => 'Please select updated at',
        'working_hours'              => 'Please enter working Hours',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'name'                       => 'Name',
        'slug'                       => 'Slug',
        'description'                => 'Description',
        'address'                    => 'Address',
        'phone'                      => 'Phone',
        'email'                      => 'Email',
        'price_range_min'            => 'Minimum Price Range',
        'price_range_max'            => 'Maximum Price Range',
        'keywords'                   => 'Search Keywords',
        'zipcode'                    => 'Zipcode',
        'latitude'                   => 'Latitude',
        'longitude'                  => 'Longitude',
        'rating'                     => 'Rating',
        'logo'                       => 'Logo',
        'offer'                      => 'Offer',
        'gallery'                    => 'Gallery',
        'delivery'                   => 'Home Delivery',
        'type'                       => 'Restaurant Type',
        'created_at'                 => 'Created at',
        'deleted_at'                 => 'Deleted at',
        'updated_at'                 => 'Updated at',
        'working_hours'              => 'Working Hours',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'name'                       => ['name' => 'Name', 'data-column' => 1, 'checked'],
        'phone'                      => ['name' => 'Phone', 'data-column' => 2, 'checked'],
        'email'                      => ['name' => 'Email', 'data-column' => 3, 'checked'],
        'price_range'                => ['name' => 'Price range', 'data-column' => 4, 'checked'],
        'rating'                     => ['name' => 'Rating', 'data-column' => 5, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Restaurants',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
