<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for review in restaurant package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  review module in restaurant package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Review',
    'names'         => 'Reviews',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Reviews',
        'sub'   => 'Reviews',
        'list'  => 'List of reviews',
        'edit'  => 'Edit review',
        'create'    => 'Create new review'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'restaurant_id'              => 'Please enter restaurant',
        'customer_id'                => 'Please enter customer',
        'order_id'                   => 'Please enter order id',
        'rating'                     => 'Please enter rating',
        'review'                     => 'Please enter review',
        'created_at'                 => 'Please select created at',
        'deleted_at'                 => 'Please select deleted at',
        'updated_at'                 => 'Please select updated at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant',
        'customer_id'                => 'Customer id',
        'order_id'                   => 'Order',
        'rating'                     => 'Rating',
        'review'                     => 'Review',
        'created_at'                 => 'Created at',
        'deleted_at'                 => 'Deleted at',
        'updated_at'                 => 'Updated at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'customer_id'                => ['name' => 'Customer id', 'data-column' => 1, 'checked'],
        'order_id'                   => ['name' => 'Order id', 'data-column' => 2, 'checked'],
        'rating'                     => ['name' => 'Rating', 'data-column' => 3, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Reviews',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
