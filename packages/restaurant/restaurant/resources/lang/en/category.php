<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for category in restaurant package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  category module in restaurant package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Menu Category',
    'names'         => 'Categories',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Categories',
        'sub'   => 'Categories',
        'list'  => 'List of categories',
        'edit'  => 'Edit category',
        'create'    => 'Create new category'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'restaurant_id'              => 'Please choose restaurant',
        'parent_id'                  => 'Please enter parent',
        'name'                       => 'Please enter category name',
        'preparation_time'           => 'Please enter preparation time',
        'slug'                       => 'Please enter slug',
        'created_at'                 => 'Please select created at',
        'deleted_at'                 => 'Please select deleted at',
        'updated_at'                 => 'Please select updated at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant ',
        'parent_id'                  => 'Parent Category',
        'name'                       => 'Category Name',
        'preparation_time'           => 'Preparation time',
        'slug'                       => 'Slug',
        'created_at'                 => 'Created at',
        'deleted_at'                 => 'Deleted at',
        'updated_at'                 => 'Updated at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'parent_id'                  => ['name' => 'Parent id', 'data-column' => 1, 'checked'],
        'name'                       => ['name' => 'Name', 'data-column' => 2, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Categories',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
