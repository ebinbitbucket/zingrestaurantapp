<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Restaurant package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for restaurant module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Restaurant',
    'names'         => 'Restaurants',
];
