                                                            <div class="row align-items-center">
                                                                <div class="col-5">
                                                                    <select name="offer[schedule][{{$count}}][day]" onchange="dayChange('{{$count}}')" class="form-control">
                                                                                <option selected disabled>Select Date</option>
                                                                                <option value="Mon">Monday</option>
                                                                                <option value="Tue">Tuesday</option>
                                                                                <option value="Wed">Wednesday</option>
                                                                                <option value="Thu">Thursday</option>
                                                                                <option value="Fri">Friday</option>
                                                                                <option value="Sat">Saturday</option>
                                                                                <option value="Sun">Sunday</option>
                                                                            </select>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control" name="offer[schedule][{{$count}}][from]" onchange="checktime('{{$count}}')" id="offer[schedule][{{$count}}][from]">
                                                                                    <option value="">From</option>
                                                                                   <option value="00:00" >12:00 AM</option>
                                                                                    <option value="01:00" >01:00 AM</option>
                                                                                    <option value="02:00">02:00 AM</option>
                                                                                    <option value="03:00" >03:00 AM</option>
                                                                                    <option value="04:00" >04:00 AM</option>
                                                                                    <option value="05:00" >05:00 AM</option>
                                                                                    <option value="06:00" >06:00 AM</option>
                                                                                    <option value="07:00" >07:00 AM</option>
                                                                                    <option value="08:00" >08:00 AM</option>
                                                                                    <option value="09:00" >09:00 AM</option>
                                                                                    <option value="10:00" >10:00 AM</option>
                                                                                    <option value="11:00" >11:00 AM</option>
                                                                                    <option value="12:00" >12:00 PM</option>
                                                                                    <option value="13:00" >01:00 PM</option>
                                                                                    <option value="14:00" >02:00 PM</option>
                                                                                    <option value="15:00" >03:00 PM</option>
                                                                                    <option value="16:00" >04:00 PM</option>
                                                                                    <option value="17:00" >05:00 PM</option>
                                                                                    <option value="18:00" >06:00 PM</option>
                                                                                    <option value="19:00" >07:00 PM</option>
                                                                                    <option value="20:00" >08:00 PM</option>
                                                                                    <option value="21:00" >09:00 PM</option>
                                                                                    <option value="22:00" >10:00 PM</option>
                                                                                    <option value="23:00" >11:00 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control" name="offer[schedule][{{$count}}][end]" onchange="checktime('{{$count}}')" id="offer[schedule][{{$count}}][end]">
                                                                                    <option value="">To</option>
                                                                                    <option value="00:00" >12:00 AM</option>
                                                                                    <option value="01:00" >01:00 AM</option>
                                                                                    <option value="02:00">02:00 AM</option>
                                                                                    <option value="03:00" >03:00 AM</option>
                                                                                    <option value="04:00" >04:00 AM</option>
                                                                                    <option value="05:00" >05:00 AM</option>
                                                                                    <option value="06:00" >06:00 AM</option>
                                                                                    <option value="07:00" >07:00 AM</option>
                                                                                    <option value="08:00" >08:00 AM</option>
                                                                                    <option value="09:00" >09:00 AM</option>
                                                                                    <option value="10:00" >10:00 AM</option>
                                                                                    <option value="11:00" >11:00 AM</option>
                                                                                    <option value="12:00" >12:00 PM</option>
                                                                                    <option value="13:00" >01:00 PM</option>
                                                                                    <option value="14:00" >02:00 PM</option>
                                                                                    <option value="15:00" >03:00 PM</option>
                                                                                    <option value="16:00" >04:00 PM</option>
                                                                                    <option value="17:00" >05:00 PM</option>
                                                                                    <option value="18:00" >06:00 PM</option>
                                                                                    <option value="19:00" >07:00 PM</option>
                                                                                    <option value="20:00" >08:00 PM</option>
                                                                                    <option value="21:00" >09:00 PM</option>
                                                                                    <option value="22:00" >10:00 PM</option>
                                                                                    <option value="23:00" >11:00 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
<script type="text/javascript">
    function checktime(count){
        if(document.getElementById('offer[schedule]['+count+'][from]').value !='' && document.getElementById('offer[schedule]['+count+'][end]').value!= ''){
             if(document.getElementById('offer[schedule]['+count+'][from]').value > document.getElementById('offer[schedule]['+count+'][end]').value){
            toastr.error('starting time shoulb be less than Ending time', 'Error');
            document.getElementById('offer[schedule]['+count+'][from]').selectedIndex = 0;
                        document.getElementById('offer[schedule]['+count+'][end]').selectedIndex = 0;

        }
        }
       
    }
</script>