            <table class="table" id="main-table" data-url="{!!guard_url('restaurant/menu?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="restaurant_id">{!! trans('restaurant::menu.label.restaurant_id')!!}</th>
                    <th data-field="category_id">{!! trans('restaurant::menu.label.category_id')!!}</th>
                    <th data-field="name">{!! trans('restaurant::menu.label.name')!!}</th>
                    <th data-field="price">{!! trans('restaurant::menu.label.price')!!}</th>
                    <th data-field="addons">{!! trans('restaurant::menu.label.addons')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>