            <div class="content">
                <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('restaurant::menu.label.id') !!}
                </label><br />
                    {!! $menu['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="restaurant_id">
                    {!! trans('restaurant::menu.label.restaurant_id') !!}
                </label><br />
                    {!! $menu['restaurant_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="category_id">
                    {!! trans('restaurant::menu.label.category_id') !!}
                </label><br />
                    {!! $menu['category_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('restaurant::menu.label.name') !!}
                </label><br />
                    {!! $menu['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="description">
                    {!! trans('restaurant::menu.label.description') !!}
                </label><br />
                    {!! $menu['description'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="price">
                    {!! trans('restaurant::menu.label.price') !!}
                </label><br />
                    {!! $menu['price'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="image">
                    {!! trans('restaurant::menu.label.image') !!}
                </label><br />
                    {!! $menu['image'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="addons">
                    {!! trans('restaurant::menu.label.addons') !!}
                </label><br />
                    {!! $menu['addons'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="slug">
                    {!! trans('restaurant::menu.label.slug') !!}
                </label><br />
                    {!! $menu['slug'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="status">
                    {!! trans('restaurant::menu.label.status') !!}
                </label><br />
                    {!! $menu['status'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('restaurant::menu.label.created_at') !!}
                </label><br />
                    {!! $menu['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('restaurant::menu.label.deleted_at') !!}
                </label><br />
                    {!! $menu['deleted_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('restaurant::menu.label.updated_at') !!}
                </label><br />
                    {!! $menu['updated_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('restaurant::menu.label.restaurant_id'))
                       -> placeholder(trans('restaurant::menu.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('category_id')
                       -> label(trans('restaurant::menu.label.category_id'))
                       -> placeholder(trans('restaurant::menu.placeholder.category_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('name')
                       -> label(trans('restaurant::menu.label.name'))
                       -> placeholder(trans('restaurant::menu.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('description')
                       -> label(trans('restaurant::menu.label.description'))
                       -> placeholder(trans('restaurant::menu.placeholder.description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('price')
                       -> label(trans('restaurant::menu.label.price'))
                       -> placeholder(trans('restaurant::menu.placeholder.price'))!!}
                </div>

                <div class='col-md-12 col-sm-12'>
                    <div class="form-group">
                        <label for="image" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::menu.label.image') }}
                        </label>
                        <div class='col-lg-3 col-sm-12'>
                            {!! $menu->files('image')
                            ->url($menu->getUploadUrl('image'))
                            ->mime(config('filer.image_extensions'))
                            ->dropzone()!!}
                        </div>
                        <div class='col-lg-7 col-sm-12'>
                        {!! $menu->files('image')
                        ->editor()!!}
                        </div>
                    </div>
                </div>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('addons')
                       -> label(trans('restaurant::menu.label.addons'))
                       -> placeholder(trans('restaurant::menu.placeholder.addons'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('status')
                    -> options(trans('restaurant::menu.options.status'))
                    -> label(trans('restaurant::menu.label.status'))
                    -> placeholder(trans('restaurant::menu.placeholder.status'))!!}
               </div>
            </div>