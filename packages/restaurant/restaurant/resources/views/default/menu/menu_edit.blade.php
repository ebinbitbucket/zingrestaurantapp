 <section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                         <li class="nav-head"><span class="head">Navigation</span></li>
                                        <li>
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashborad</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Billing Details</span></a>
                                        </li>
                                        <li  class="active">
                                            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu Items</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Account Details</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-md-9">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    <form action="">
                                        <div class="element-info">
                                            <div class="element-info-with-icon">
                                                <div class="element-info-icon"><div class="ion-android-restaurant"></div></div>
                                                <div class="element-info-text">
                                                    <h5 class="element-inner-header">Edit Grilled Cheese Sandwich</h5>
                                                    <div class="element-inner-desc">Validation of the form is made possible using powerful validator plugin for bootstrap.</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for=""> Menu Title</label>
                                                    <input class="form-control" placeholder="Menu Title" required="required" type="text" value="Grilled Cheese Sandwich">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Category</label>
                                                    <select class="form-control">
                                                        <option  disabled>Select Category</option>
                                                        <option selected value="Sides">Sides</option>
                                                        <option value="Wings">Wings</option>
                                                        <option value="Salads">Salads</option>
                                                        <option value="Wraps">Wraps</option>
                                                        <option value="Sandwiches">Sandwiches</option>
                                                        <option value="Pizza">Pizza</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Quantity</label>
                                                    <input class="form-control" placeholder="Menu Item Quantity" required="required" type="number" value="50">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">Price</label>
                                                <div class="input-group mb-3">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">$</span>
                                                    </div>
                                                    <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)" value="3"> 
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Order</label>
                                                    <input class="form-control" placeholder="Menu Item Order" required="required" type="text" value="1">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Description</label>
                                                    <textarea class="form-control" placeholder="Description" rows="6" value="">Grilled Cheese Sandwich</textarea>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="">Image</label>
                                                    <div class="dropzone">
                                                        <div class="dz-message needsclick">Drop files here or click to upload.</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-buttons-w">
                                            <button class="btn btn-theme" type="submit"> Update</button>
                                            <button class="btn btn-danger" type="button"> Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>