@extends('resource.index')
@php
$links['create'] = guard_url('restaurant/menu/create');
$links['search'] = guard_url('restaurant/menu');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('restaurant::menu.title.main') !!}
@stop

@section('sub.title') 
{!! __('restaurant::menu.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('restaurant/menu')!!}">{!! __('restaurant::menu.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('restaurant::menu.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$menus->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
