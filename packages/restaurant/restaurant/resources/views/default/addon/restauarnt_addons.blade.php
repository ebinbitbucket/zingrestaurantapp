@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
    
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
                <i class="flaticon-drink app-sec-title-icon"></i>
                <h1>Add Ons</h1>
                <div class="actions">
                    <button type="button" class="btn btn-with-icon btn-dark" data-toggle="modal" data-target="#addAddonModal"><i class="fas fa-plus-circle mr-5"></i>Add</button>
                </div>
                <a href="menu.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="menu-accordion-wrap menu-addon-wrap">
                <div class="accordion">
                    <div class="card" data-id="1">
                        <div class="card-header">
                            <h4>Drinks</h4>
                            <button type="button" class="btn action-res" type="button" id="catAction_01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-ellipsis"></i></button>
                            <div class="actions" id="catAction_01">
                                <a href="#" class="btn edit-btn"><i class="fas fa-pencil-alt"></i></a>
                                <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="card" data-id="2">
                        <div class="card-header">
                            <h4>chicken popcorn</h4>
                            <button type="button" class="btn action-res" type="button" id="catAction_02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-ellipsis"></i></button>
                            <div class="actions" id="catAction_02">
                                <a href="#" class="btn edit-btn"><i class="fas fa-pencil-alt"></i></a>
                                <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="addAddonModal" tabindex="-1" aria-labelledby="addAddonModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="addAddonModalLabel">Add Addons</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="">Addon Name <sup>*</sup></label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
            <div class="menu-modal-addon-varient-wrap addon-add-wrap p-0">
                <div class="addon-varient-list-wrap addon-varient-list-wrap-add">
                    <div class="modal-sec-title modal-sec-title-with-action mt-5">
                        <h4>Addon Item</h4>
                        <div class="actions">
                            <a href="#" class="btn btn-dark add-btn" data-related="addVariationForm"><i class="fas fa-plus-circle"></i> Add</a>
                        </div>
                    </div>
                    <div class="addon-items-wrap">
                        
                    </div>
                </div>
                <div class="menu-modal-addon-varient-form-wrap" id="addVariationForm" style="display: none;">
                    <div class="modal-sec-title modal-sec-title-with-action mt-5">
                        <h4>Add Addon Item</h4>
                        <div class="actions">
                            <a href="#" class="btn btn-secondary close-btn" data-close="addVariationForm"><i class="fas fa-times"></i> Close</a>
                        </div>
                    </div>
                    <div class="menu-modal-addon-varient-form-wrap-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Name <sup>*</sup></label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Price <sup>*</sup></label>
                                    <input type="number" class="form-control">
                                    <p class="m-0" style="font-size: 12px; font-family: 'Lato', sans-serif; line-height: 16px;">Variation upon selection will ignore the existing price of the menu item and will replace it with the selected variation price.</p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer pb-0 pl-0 pr-0">
                        <button type="button" class="btn btn-dark">Add Item</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-dark">Add Addon</button>
        </div>
    </div>
</div>
</div>



<div class="modal fade entry-modal" id="editAddonModal" tabindex="-1" aria-labelledby="editAddonModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="editAddonModalLabel">Edit Drinks</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Addon Name <sup>*</sup></label>
                        <input type="text" class="form-control" value="Drinks">
                    </div>
                </div>
            </div>
            <div class="menu-modal-addon-varient-wrap addon-add-wrap p-0">
                <div class="addon-varient-list-wrap addon-varient-list-wrap-edit" style="">
                    <div class="modal-sec-title modal-sec-title-with-action mt-5">
                        <h4>Addon Item</h4>
                        <div class="actions">
                            <a href="#" class="btn btn-dark preview-btn" data-preview="viewVariations"><i class="fas fa-eye"></i> <span>View Linked Menu Items</span></a>
                            <a href="#" class="btn btn-dark add-btn" data-related="addVariationFormEdit"><i class="fas fa-plus-circle"></i> Add</a>
                        </div>
                    </div>
                    <div class="addon-items-wrap">
                        <div class="addon-item">
                            <h4>Pepsi</h4>
                            <div class="actions">
                                <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </div>
                        <div class="addon-item">
                            <h4>Iced Tea</h4>
                            <div class="actions">
                                <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="menu-modal-addon-varient-form-wrap" id="addVariationFormEdit" style="display: none;">
                    <div class="modal-sec-title modal-sec-title-with-action mt-5">
                        <h4>Add Addon Items</h4>
                        <div class="actions">
                            <a href="#" class="btn btn-secondary close-btn" data-close="addVariationFormEdit"><i class="fas fa-times"></i> Close</a>
                        </div>
                    </div>
                    <div class="menu-modal-addon-varient-form-wrap-inner">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Name <sup>*</sup></label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer pb-0 pl-0 pr-0">
                        <button type="button" class="btn btn-dark">Save Addon</button>
                    </div>
                </div>
                <div class="menu-modal-addon-varient-form-wrap" id="viewVariations" style="display: none;">
                    <div class="modal-sec-title modal-sec-title-with-action mt-5">
                        <h4>View Linked Menu Items</h4>
                        <div class="actions">
                            <a href="#" class="btn btn-secondary close-preview" data-close="viewVariations"><i class="fas fa-times"></i> Close</a>
                        </div>
                    </div>
                    <div class="linked-addons-wrap mb-15" style="height: auto;">
                        <p>See the Menu items that use this Addon</p>
                        <div class="menu-preview-wrap">
                            <div class="menu-item">
                                <h1>Fish Curry</h1>
                                <div class="variation-wrap mt-10">
                                    <div class="variation-item">
                                        <div class="variation-item-inner collapsed" data-toggle="collapse" data-target="#collapseHalf" aria-expanded="false" aria-controls="collapseHalf">
                                            <i class="fas fa-chevron-up"></i>
                                            <h3>Half<span class="price">$1.00</span></h3>
                                            <p>Blended fruit drink with tapioca pearls or  popping pearls</p>
                                        </div>
                                        <div class="addon-wrap mt-10 collapse" id="collapseHalf">
                                            <div class="addon-item">
                                                <div class="addon-item-inner">
                                                    <h3>Drinks</h3>
                                                    <div class="row mt-10 gutter-10">
                                                        <div class="col">
                                                            <div class="row gutter-10">
                                                                <div class="col-6">
                                                                    <div class="form-group m-0">
                                                                        <label for="">Min <sup>*</sup></label>
                                                                        <input type="number" name="price" class="form-control" value="1" required="required">
                                                                        <small style="font-size: 12px; font-family: 'Lato', sans-serif;">Min amount a user can choose</small>
                                                                    </div>
                                                                </div>
                                                                <div class="col-6">
                                                                    <div class="form-group m-0">
                                                                        <label for="">Max <sup>*</sup></label>
                                                                        <input type="number" name="price" class="form-control" value="2" required="required">
                                                                        <small style="font-size: 12px; font-family: 'Lato', sans-serif;">Max amount a user can choose</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col" style="max-width: 70px;">
                                                            <label for="">Required</label>
                                                            <div class="form-group mb-0">
                                                                <div class="custom-control custom-switch custom-switch-lg">
                                                                    <input type="checkbox" class="custom-control-input" id="addonPepsi_01">
                                                                    <label class="custom-control-label" for="addonPepsi_01">&nbsp;</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="addon-variation-wrap mt-10">
                                                    <div class="addon-variation-item">
                                                        <h3>Pepsi</h3>
                                                        <div class="row mt-3 gutter-10">
                                                            <div class="col">
                                                                <div class="form-group m-0">
                                                                    <label for="">Price <sup>*</sup></label>
                                                                    <input type="number" name="price" class="form-control" value="1.99" required="required">
                                                                </div>
                                                            </div>
                                                            <div class="col" style="max-width: 70px;">
                                                                <label for="">Active</label>
                                                                <div class="form-group mb-0">
                                                                    <div class="custom-control custom-switch custom-switch-lg">
                                                                        <input type="checkbox" class="custom-control-input" id="addonPepsi_active_01">
                                                                        <label class="custom-control-label" for="addonPepsi_active_01">&nbsp;</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <p class="m-0" style="font-size: 12px; font-family: 'Lato', sans-serif; line-height: 16px;">Upon selecting this Addon item the total menu price will increase by the entered Addon price.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="addon-variation-item">
                                                        <h3>Hot Tea</h3>
                                                        <div class="row mt-3 gutter-10">
                                                            <div class="col">
                                                                <div class="form-group m-0">
                                                                    <label for="">Price <sup>*</sup></label>
                                                                    <input type="number" name="price" class="form-control" value="1.99" required="required">

                                                                </div>
                                                            </div>
                                                            <div class="col" style="max-width: 70px;">
                                                                <label for="">Active</label>
                                                                <div class="form-group mb-0">
                                                                    <div class="custom-control custom-switch custom-switch-lg">
                                                                        <input type="checkbox" class="custom-control-input" id="addonHottea_active_01">
                                                                        <label class="custom-control-label" for="addonHottea_active_01">&nbsp;</label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <p class="m-0" style="font-size: 12px; font-family: 'Lato', sans-serif; line-height: 16px;">Upon selecting this Addon item the total menu price will increase by the entered Addon price.</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="variation-item">
                                        <div class="variation-item-inner collapsed">
                                            <i class="fas fa-chevron-up"></i>
                                            <h3>Full<span class="price">$5.00</span></h3>
                                            <p>Blended fruit drink with tapioca pearls or  popping pearls</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-dark">Update Addon</button>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $(".edit-btn").on('click', function(){
            $("#editAddonModal").modal('show');
        });
        $(".edit-menu").on('click', function(){
            $("#menuModal").modal('show');
        });
        $(".schedule-menu").on('click', function(){
            $("#scheduleMenuModal").modal('show');
        });

        $(".schedule-menu-offer").on('click', function(){
            $("#scheduleOfferMenuModal").modal('show');
        });

        $('.add-btn').on( "click", function(e) {
            e.preventDefault();
            var id = $(this).data('related'); 
            // $(".menu-modal-addon-varient-form-wrap").hide();
            $(this).closest(".addon-varient-list-wrap").hide();
            $("#"+id).show();
        });

        $('.close-btn').on( "click", function(e) {
            e.preventDefault();
            var id = $(this).data('close'); 
            $("#"+id).hide();
            $(".addon-varient-list-wrap").show();
        });

        $('.preview-btn').on( "click", function(e) {
            e.preventDefault();
            var id = $(this).data('preview'); 
            // $(".menu-modal-addon-varient-form-wrap").hide();
            $(".addon-varient-list-wrap").hide();
            $("#"+id).show();
        });

        $('.close-preview').on( "click", function(e) {
            e.preventDefault();
            var id = $(this).data('close'); 
            $("#"+id).hide();
            $(".addon-varient-list-wrap").show();
        });

        // $(".add-variation").on('click', function(){
        //     $("#addVariationForm").show();
        //     $(".addon-varient-list-wrap-add").hide();
        // });
        // $(".add-variation-edit").on('click', function(){
        //     $("#addVariationFormEdit").show();
        //     $(".addon-varient-list-wrap-edit").hide();
        // });
        // $(".close-variation-add").on('click', function(){
        //     $("#addVariationForm").hide();
        //     $(".addon-varient-list-wrap-add").show();
        // });
        // $(".close-variation-edit").on('click', function(){
        //     $("#addVariationFormEdit").hide();
        //     $(".addon-varient-list-wrap-edit").show();
        // });
        // $(".add-addon").on('click', function(){
        //     $("#addAddonForm").show();
        //     $(".addon-varient-list-wrap").hide();
        // });
        // $(".close-addon").on('click', function(){
        //     $("#addAddonForm").hide();
        //     $(".addon-varient-list-wrap").show();
        // });

        // $(".view-variations").on('click', function(){
        //     $("#viewVariations").show();
        //     $(".addon-varient-list-wrap").hide();
        // });
        // $(".close-variation-view").on('click', function(){
        //     $("#viewVariations").hide();
        //     $(".addon-varient-list-wrap").show();
        // });

        if (Modernizr.mq('(max-width: 767px)')) {
            $(".menu-accordion-wrap .card .card-header .actions").addClass('dropdown-menu dropdown-menu-right');
            $(".card .card-body .menu-items-wrap .menu-item .actions").addClass('dropdown-menu dropdown-menu-right');
        } else {

        }
    });
</script>