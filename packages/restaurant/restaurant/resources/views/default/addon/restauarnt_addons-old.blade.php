<aside class="main-nav">
    <div class="nav-inner">
    @include('restaurant::default.restaurant.partial.mobile_menu')

    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                            <aside class="dashboard-sidemenu">
                            @include('restaurant::default.restaurant.partial.left_menu')

                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="element-wrapper addon-element-wrapper">
                                        <div class="element-box p-0">
                                            <div class="element-info">
                                                <div class="element-info-with-icon">
                                                    <div class="element-info-text">
                                                        <h5 class="element-inner-header">Addons</h5>
                                                    </div>
                                                    <div class="element-info-buttons element-add-buttons">
                                                        <button type="button" id="add_addon_mainbtn" class="btn btn-theme ion-android-add add-addon" data-related="add_Addon"></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="addon-item-wrap" id="addon_list">

                                                @foreach($addons as $addon)
                                                <div class="addon-item">
                                                    <p>{{$addon->name}}</p>
                                                    <div class="addon-actions">
                                                        <a href="#" class="btn edit-btn flaticon-edit text-primary" onclick="updatecount('{{count($addon->variation)}}','{{$addon->id}}')" ></a>
                                                        <form id="delete-form" style="display: inline-block;" method="POST" action="{!! guard_url('restaurant/addon') !!}/{!! $addon->getRouteKey() !!}">
                                              {{csrf_field()}}{{method_field('DELETE')}}
                                             <button type="submit" class="btn edit-btn flaticon-garbage text-danger" style="border: none; background: none; padding: 0;"></button></form>
                                                       
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="element-wrapper menu-element-edit-wrapper">
                                        <div class="element-box p-0">  
                                            <div class="empty-msg" id="emptymsg">
                                                <p>Please click on edit button to edit</p>
                                            </div>  
                                            <div class="menu-element-item-edit-block_1" id="Edit_addon">
                                            @include('notifications')
                                       </div>
                                            <div class="menu-element-item-edit-block" id="add_Addon">
                                                <h3>Add Addon</h3>
                                                {!!Form::vertical_open()
                                                ->id('restaurant-addon-create')
                                                ->method('POST')
                                                ->files('true')
                                                ->addClass('compact-edit')
                                                ->action(guard_url('restaurant/addon'))!!}
                                                    <div class="form-group">
                                                        <label for="title">Name*</label>
                                                         {!! Form::text('name')
                                                             ->required()
                                                             -> label('')
                                                             ->addClass('form-control')
                                                             -> placeholder('')!!}
                                                        
                                                    </div>
                                                    <div class="repeater mt-20">
                                                        <div class="item-sectitle mb-20">
                                                            <h3> Add Variations</h3>
                                                            <button data-repeater-create type="button" class="btn btn-theme btn-icon ion-android-add" onclick="addVariation()"></button>
                                                        </div>
                                                        <div id="variation_div"></div>
                                                    </div>
                                                    <center><button  id="btn_add_addon" style="width:50%;" type="button" class="btn btn-theme">Add</button></center>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

<script type="text/javascript">
 
   <?php if (!empty(@$addon_variations)):  $s = count($addon_variations);
    $c = $s+1 ; ?>
    var count = {!!$s!!} + 1; 
    <?php else: $c = 1; ?>  var count = 1;
  <?php endif ?>

function updatecount(count_varat,addon_id){ 
    count = parseInt(count_varat) + 1;
    document.getElementById("emptymsg").style.display = "none"; 
    $(".menu-element-item-edit-block").hide();
    $("#Edit_addon").css('display','block');
    $.get("{{guard_url('restaurant/restaurant_addons/edit')}}/"+addon_id, function(data) { $('#Edit_addon').html(data); });  
       

}
  function pricetest(event) {
     var selectElement = event.target;
    var value = selectElement.value;
    if( value < 0)
       {
       toastr.error('Price should not be a negative number', 'Error');
       event.target.value = ''
       }
      
 }

  function addVariation(){  
   var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);
                
                

  newTextBoxDiv.after().html(
        '<div data-repeater-list="group-a"><div data-repeater-item class="repeat-item"><div class="row"><div class="col-sm-8"><div class="form-group"><label for="">Name*</label><input type="text" name="variations['+count+'][name]" required="true" id="'+count+' value="" class="form-control" placeholder=""></div></div><div class="col-sm-4"><div class="form-group"><label for="">Price*</label><input type="number" required="true" name="variations['+count+'][price]" id="'+count+' value="" class="form-control price" placeholder="" onchange="pricetest(event)"></div></div></div><div class="form-group"><label for="">Description</label><textarea name="variations['+count+'][description]" value="" class="form-control"></textarea></div> <button data-repeater-delete type="button" onclick="removeVar('+count+')" class="btn btn-danger btn-icon ion-trash-a" ></button></div></div>');
            
  newTextBoxDiv.appendTo("#variation_div");
    count ++;

  }
function editVariation(id){ 
   var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);
                
               

  newTextBoxDiv.after().html(
        '<div data-repeater-list="group-a"><div data-repeater-item class="repeat-item"><div class="row"><div class="col-sm-8"><div class="form-group"><label for="">Name*</label><input type="text" name="variations['+count+'][name]" id="'+count+' value="" class="form-control" placeholder="" required="required"></div></div><div class="col-sm-4"><div class="form-group"><label for="">Price*</label><input type="number" name="variations['+count+'][price]" id="'+count+' value="" class="form-control" placeholder="" required="true" onchange="pricetest(event)"></div></div></div><div class="form-group"><label for="">Description</label><textarea name="variations['+count+'][description]" value="" class="form-control"></textarea></div> <button data-repeater-delete type="button" onclick="removeVar('+count+')" class="btn btn-danger btn-icon ion-trash-a" ></button></div></div>');
            
  newTextBoxDiv.appendTo("#edit_variation_div_"+id);
    count ++;

  }
  function removeVar(key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=variations]").length/3; 
 
            document.getElementById('TextBoxDiv'+key).remove();  


         
}

function removeVars(key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=variations]").length/3; 
 if(countvaraitions > 1){ 
          if($("#TextBoxDiv"+key).length == 0){
                        document.getElementById(key).remove();  
           document.getElementById(key).remove();  

           document.getElementById(key).remove();  
          }
          else{

            document.getElementById('TextBoxDiv'+key).remove();  
          }
        document.getElementById(key+'button').style.display = "none";   
  }
  if(countvaraitions == 1){    

        if($("#TextBoxDiv"+key).length == 0){
           document.getElementById(key).remove();  
           document.getElementById(key+'button').remove();  

           document.getElementById(key).remove();  

          }
          else{
            document.getElementById('TextBoxDiv'+key).remove();  
          }
           
     
    document.getElementById(key).remove();  
     document.getElementById('btn_remove').style.display = "none";
     document.getElementById('variation_name').style.display = "none";
     unset(variation_list);
  }
}

</script>
<script type="text/javascript">
     $(document).on('click','#btn_add_addon',function(){   
            var $f = $('#restaurant-addon-create');
            if(! $f.valid()) return false;
            $.getJSON({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                   $('#addon_list').load('{{guard_url('restaurant/restaurant_addons/list')}}');
                   // document.getElementById("add_Addon").empty();
                    $("#add_Addon").load(" #add_Addon > *");
                   // document.getElementsByClassName("empty-msg").style.display = "block";
                   // document.getElementsByClassName("empty-msg").style.display = "none";
                   // document.getElementById("add_Addon").style.display = "block";
                   
                   // $('.empty-msg').style ='display:block';
                             
                  },
                  error:  function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 
          $(document).on('click','#btn_update_addon',function(){  
            var $f = $('#restaurant-addon-edit');
            var addon_id = $('#btn_update_addon').attr('data-addon');
            if(! $f.valid()) return false;
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    console.log($f);
                    $('#addon_list').load('{{guard_url('restaurant/restaurant_addons/list')}}');
                    $.get("{{guard_url('restaurant/restaurant_addons/edit')}}/"+addon_id, function(data) { $('#Edit_addon').html(data); }); 
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        });  
          $('#add_addon_mainbtn').on( "click", function() { 
            $('#Edit_addon').hide();
    });

    $('.add-addon').on( "click", function(e) { 
        e.preventDefault();
        var id = $(this).attr('data-related'); 
        $(".menu-element-item-edit-block").each(function(){
            $(this).hide();
            if($(this).attr('id') == id) {
                $(this).show();
            }
        });
    });
</script>
