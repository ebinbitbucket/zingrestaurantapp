@extends('resource.show')

@php
$links['back'] = guard_url('restaurant/addon');
$links['edit'] = guard_url('restaurant/addon') . '/' . $addon->getRouteKey() . '/edit';
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('restaurant::addon.title.main') !!}
@stop

@section('sub.title') 
{!! __('restaurant::addon.title.show') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('$restaurant/addon')!!}">{!! __('restaurant::addon.name') !!}</a></li>
  <li>{{ __('app.show') }}</li>
@stop

@section('tabs') 
@stop

@section('tools') 
    <a href="{!!guard_url('$restaurant/addon')!!}" rel="tooltip" class="btn btn-white btn-round btn-simple btn-icon pull-right add-new" data-original-title="" title="">
            <i class="fa fa-chevron-left"></i>
    </a>
@stop

@section('content') 
    @include('restaurant::addon.partial.show', ['mode' => 'show'])
@stop
