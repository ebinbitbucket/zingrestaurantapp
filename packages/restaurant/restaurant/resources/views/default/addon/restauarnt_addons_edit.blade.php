<h3>Edit Addon {{$edit_addon->name}}</h3>
                                                 {!!Form::vertical_open()
                                                    ->id('restaurant-addon-edit')
                                                    ->method('PUT')
                                                    ->enctype('multipart/form-data')
                                                    ->addClass('compact-edit')
                                                    ->action(guard_url('restaurant/addon/'. $edit_addon->getRouteKey()))!!}
                                                    <div class="form-group">
                                                        <label for="title">Name*</label>
                                                        <input type="text" name="name" class="form-control" value="{{$edit_addon->name}}">
                                                    </div>
                                                    @if($edit_addon->variation != '[]')<div class="item-sectitle mb-20">
                                                            <h3>Variations</h3> 
                                                        </div>
                                                        @endif
                                                        @if(!empty($edit_addon->variation))
                                                        @foreach($edit_addon->variation as 
                                                        $key => $edit_varlist)
                                                        <div> 
                                                    <div class="row">

                                                        <div class="col">
                                                            <div class="form-group" id="{{$key}}">
                                                                <label for="">Name*</label>
                                                                <input type="text" name="variations[{{$key}}][name]" id="{{$key}}" class="form-control" value="{{$edit_varlist->name}}">
                                                            </div>

                                                        </div>

                                                        <div class="col">
                                                            <div class="form-group" id="{{$key}}">
                                                                <label for="">Price*</label>
                                                                <input type="number" id="{{$key}}" class="form-control" name="variations[{{$key}}][price]" value="{{$edit_varlist->price}}" onchange="pricetest(event)">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" id="{{$key}}">
                                                    <div class="form-group col-md-8" >
                                                        <label for="title">Description</label>
                                                        <input type="text" id="{{$key}}" class="form-control" name="variations[{{$key}}][description]" value="{{$edit_varlist->description}}">
                                                    </div>
                                                     <div class="col-md-4">
                                                        <button data-repeater-delete="" id="{{$key}}button" type="button" onclick="removeVars({!!$key!!})" class="btn btn-danger btn-icon ion-trash-a"></button>
                                                    </div>
                                                </div>
                                                </div>
                                                @endforeach
                                                    @endif
                                                    <div class="repeater mt-20">
                                                        <div class="item-sectitle mb-20">
                                                            <h3>Add Variations</h3>
                                                            <button data-repeater-create type="button" class="btn btn-theme btn-icon ion-android-add" onclick="editVariation('{{$edit_addon->id}}')"></button>
                                                        </div>
                                                        <div id="edit_variation_div_{{$edit_addon->id}}"></div>
                                                    </div>
                                                    <center><button id="btn_update_addon" style="width:50%;" data-addon="{{$edit_addon->id}}" type="button" class="btn btn-theme">Update</button></center>
                                                {!!Form::close()!!}