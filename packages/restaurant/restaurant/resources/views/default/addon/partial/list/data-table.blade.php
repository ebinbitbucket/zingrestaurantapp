            <table class="table" id="main-table" data-url="{!!guard_url('restaurant/addon?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="restaurant_id">{!! trans('restaurant::addon.label.restaurant_id')!!}</th>
                    <th data-field="name">{!! trans('restaurant::addon.label.name')!!}</th>
                    <th data-field="price">{!! trans('restaurant::addon.label.price')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>