            <div class="content">
                <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('restaurant::addon.label.id') !!}
                </label><br />
                    {!! $addon['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="restaurant_id">
                    {!! trans('restaurant::addon.label.restaurant_id') !!}
                </label><br />
                    {!! $addon['restaurant_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('restaurant::addon.label.name') !!}
                </label><br />
                    {!! $addon['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="price">
                    {!! trans('restaurant::addon.label.price') !!}
                </label><br />
                    {!! $addon['price'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="slug">
                    {!! trans('restaurant::addon.label.slug') !!}
                </label><br />
                    {!! $addon['slug'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('restaurant::addon.label.created_at') !!}
                </label><br />
                    {!! $addon['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('restaurant::addon.label.deleted_at') !!}
                </label><br />
                    {!! $addon['deleted_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('restaurant::addon.label.updated_at') !!}
                </label><br />
                    {!! $addon['updated_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('restaurant::addon.label.restaurant_id'))
                       -> placeholder(trans('restaurant::addon.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('restaurant::addon.label.name'))
                       -> placeholder(trans('restaurant::addon.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('price')
                       -> label(trans('restaurant::addon.label.price'))
                       -> placeholder(trans('restaurant::addon.placeholder.price'))!!}
                </div>
            </div>