            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('restaurant::addon.label.restaurant_id'))
                       -> placeholder(trans('restaurant::addon.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('restaurant::addon.label.name'))
                       -> placeholder(trans('restaurant::addon.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('price')
                       -> label(trans('restaurant::addon.label.price'))
                       -> placeholder(trans('restaurant::addon.placeholder.price'))!!}
                </div>
            </div>