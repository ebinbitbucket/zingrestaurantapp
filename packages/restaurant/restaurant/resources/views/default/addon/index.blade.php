@extends('resource.index')
@php
$links['create'] = guard_url('restaurant/addon/create');
$links['search'] = guard_url('restaurant/addon');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('restaurant::addon.title.main') !!}
@stop

@section('sub.title') 
{!! __('restaurant::addon.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('restaurant/addon')!!}">{!! __('restaurant::addon.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('restaurant::addon.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$addons->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
