@foreach($addons as $addon)
                                                <div class="addon-item">
                                                    <p>{{$addon->name}}</p>
                                                    <div class="addon-actions">
                                                        <a href="#" class="btn edit-btn flaticon-edit text-primary" onclick="updatecount('{{count($addon->variation)}}','{{$addon->id}}')"></a>
                                                        <form id="delete-form" style="display: inline-block;" method="POST" action="{!! guard_url('restaurant/addon') !!}/{!! $addon->getRouteKey() !!}">
                                              {{csrf_field()}}{{method_field('DELETE')}}
                                             <button type="submit" class="btn edit-btn flaticon-garbage text-danger" style="border: none; background: none; padding: 0;"></button></form>
                                                       
                                                    </div>
                                                </div>
                                                @endforeach
<script type="text/javascript">
    function updatecount(count_varat,addon_id){
    count = parseInt(count_varat) + 1;
       document.getElementById("add_Addon").style.display = "none"; 
       document.getElementById("emptymsg").style.display = "none"; 
    $("#Edit_addon").css('display','block');
    $.get("{{guard_url('restaurant/restaurant_addons/edit')}}/"+addon_id, function(data) { $('#Edit_addon').html(data); }); 
            
    
}

</script>                                                