            <table class="table" id="main-table" data-url="{!!guard_url('restaurant/review?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="customer_id">{!! trans('restaurant::review.label.customer_id')!!}</th>
                    <th data-field="order_id">{!! trans('restaurant::review.label.order_id')!!}</th>
                    <th data-field="rating">{!! trans('restaurant::review.label.rating')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>