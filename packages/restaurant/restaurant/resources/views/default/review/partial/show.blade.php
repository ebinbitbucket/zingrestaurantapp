            <div class="content">
                <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('restaurant::review.label.id') !!}
                </label><br />
                    {!! $review['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="customer_id">
                    {!! trans('restaurant::review.label.customer_id') !!}
                </label><br />
                    {!! $review['customer_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_id">
                    {!! trans('restaurant::review.label.order_id') !!}
                </label><br />
                    {!! $review['order_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="rating">
                    {!! trans('restaurant::review.label.rating') !!}
                </label><br />
                    {!! $review['rating'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="review">
                    {!! trans('restaurant::review.label.review') !!}
                </label><br />
                    {!! $review['review'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('restaurant::review.label.created_at') !!}
                </label><br />
                    {!! $review['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('restaurant::review.label.deleted_at') !!}
                </label><br />
                    {!! $review['deleted_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('restaurant::review.label.updated_at') !!}
                </label><br />
                    {!! $review['updated_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('customer_id')
                       -> label(trans('restaurant::review.label.customer_id'))
                       -> placeholder(trans('restaurant::review.placeholder.customer_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('restaurant::review.label.order_id'))
                       -> placeholder(trans('restaurant::review.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('rating')
                       -> label(trans('restaurant::review.label.rating'))
                       -> placeholder(trans('restaurant::review.placeholder.rating'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('review')
                       -> label(trans('restaurant::review.label.review'))
                       -> placeholder(trans('restaurant::review.placeholder.review'))!!}
                </div>
            </div>