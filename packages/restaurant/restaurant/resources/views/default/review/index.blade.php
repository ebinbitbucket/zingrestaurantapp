@extends('resource.index')
@php
$links['create'] = guard_url('restaurant/review/create');
$links['search'] = guard_url('restaurant/review');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('restaurant::review.title.main') !!}
@stop

@section('sub.title') 
{!! __('restaurant::review.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('restaurant/review')!!}">{!! __('restaurant::review.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('restaurant::review.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$reviews->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
