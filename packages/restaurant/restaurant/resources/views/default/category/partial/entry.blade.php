            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('parent_id')
                       -> label(trans('restaurant::category.label.parent_id'))
                       -> placeholder(trans('restaurant::category.placeholder.parent_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('restaurant::category.label.name'))
                       -> placeholder(trans('restaurant::category.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('preparation_time')
                       -> label(trans('restaurant::category.label.preparation_time'))
                       -> placeholder(trans('restaurant::category.placeholder.preparation_time'))!!}
                </div>
            </div>