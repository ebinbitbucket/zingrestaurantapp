@extends('resource.index')
@php
$links['create'] = guard_url('restaurant/category/create');
$links['search'] = guard_url('restaurant/category');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('restaurant::category.title.main') !!}
@stop

@section('sub.title') 
{!! __('restaurant::category.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('restaurant/category')!!}">{!! __('restaurant::category.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('restaurant::category.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$categories->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
