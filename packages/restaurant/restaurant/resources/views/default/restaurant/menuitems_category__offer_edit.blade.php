                                                        <h3 style=" margin: 0;
    font-size: 16px;
    font-weight: 700;
    padding: 15px 30px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.1);">Schedule {{$edit_menu->name}}</h3>
                                                         {!!Form::vertical_open()
        ->id('restaurant-menu-edit-offer')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->addClass('compact-edit')
        ->action(guard_url('restaurant/category/'. $edit_menu->getRouteKey()))!!}
        {{csrf_field()}}
                                                            <div class="form-group">
                                                        <label for="title">Title</label>
                                                        @if(!empty($edit_menu->offer))
                                                        <input type="text" name="offer[name]" class="form-control" value="{{$edit_menu->offer['name']}}">
                                                        @else
                                                        <input type="text" name="offer[name]" class="form-control">
                                                        @endif
                                                    </div>
                                                     <div class="form-group">
                                                        <label for="title">Price</label>
                                                        @if(!empty($edit_menu->offer))
                                                        <input type="text" name="offer[price]" class="form-control" value="{{$edit_menu->offer['price']}}">
                                                        @else
                                                        <input type="text" name="offer[price]" class="form-control">
                                                        @endif
                                                    </div>
                                                    @if(!empty($edit_menu->offer))
                                                        @foreach($edit_menu->offer['schedule'] as $key => $schedule)
                                                         <div class="row align-items-center">
                                                        <div class="col-12">
                                                        <select name="offer[schedule][{{$key}}][day]" onchange="dayChange('{{$key}}')" class="form-control">
                                                                                <option selected disabled>Select Date</option>
                                                                                <option {{$schedule['day'] == 'Mon' ? 'selected':''}} value="Mon">Monday</option>
                                                                                <option {{$schedule['day'] == 'Tue' ? 'selected':''}} value="Tue">Tuesday</option>
                                                                                <option {{$schedule['day'] == 'Wed' ? 'selected':''}} value="Wed">Wednesday</option>
                                                                                <option {{$schedule['day'] == 'Thu' ? 'selected':''}} value="Thu">Thursday</option>
                                                                                <option {{$schedule['day'] == 'Fri' ? 'selected':''}} value="Fri">Friday</option>
                                                                                <option {{$schedule['day'] == 'Sat' ? 'selected':''}} value="Sat">Saturday</option>
                                                                                <option {{$schedule['day'] == 'Sun' ? 'selected':''}} value="Sun">Sunday</option>
                                                                            </select>
                                                                        </div>
                                                                        <div class="col-12">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control" name="offer[schedule][{{$key}}][from]">
                                                                                    <option value="">From</option>
                                                                                   <option {{$schedule['from'] == '00:00' ? 'selected':''}} value="00:00" >12:00 AM</option>
                                                                                    <option {{$schedule['from'] == '01:00' ? 'selected':''}} value="01:00" >01:00 AM</option>
                                                                                    <option {{$schedule['from'] == '02:00' ? 'selected':''}} value="02:00">02:00 AM</option>
                                                                                    <option {{$schedule['from'] == '03:00' ? 'selected':''}} value="03:00" >03:00 AM</option>
                                                                                    <option {{$schedule['from'] == '04:00' ? 'selected':''}} value="04:00" >04:00 AM</option>
                                                                                    <option {{$schedule['from'] == '05:00' ? 'selected':''}} value="05:00" >05:00 AM</option>
                                                                                    <option {{$schedule['from'] == '06:00' ? 'selected':''}} value="06:00" >06:00 AM</option>
                                                                                    <option {{$schedule['from'] == '07:00' ? 'selected':''}} value="07:00" >07:00 AM</option>
                                                                                    <option {{$schedule['from'] == '08:00' ? 'selected':''}} value="08:00" >08:00 AM</option>
                                                                                    <option {{$schedule['from'] == '09:00' ? 'selected':''}} value="09:00" >09:00 AM</option>
                                                                                    <option {{$schedule['from'] == '10:00' ? 'selected':''}} value="10:00" >10:00 AM</option>
                                                                                    <option {{$schedule['from'] == '11:00' ? 'selected':''}} value="11:00" >11:00 AM</option>
                                                                                    <option {{$schedule['from'] == '12:00' ? 'selected':''}} value="12:00" >12:00 PM</option>
                                                                                    <option {{$schedule['from'] == '13:00' ? 'selected':''}} value="13:00" >01:00 PM</option>
                                                                                    <option {{$schedule['from'] == '14:00' ? 'selected':''}} value="14:00" >02:00 PM</option>
                                                                                    <option {{$schedule['from'] == '15:00' ? 'selected':''}} value="15:00" >03:00 PM</option>
                                                                                    <option {{$schedule['from'] == '16:00' ? 'selected':''}} value="16:00" >04:00 PM</option>
                                                                                    <option {{$schedule['from'] == '17:00' ? 'selected':''}} value="17:00" >05:00 PM</option>
                                                                                    <option {{$schedule['from'] == '18:00' ? 'selected':''}} value="18:00" >06:00 PM</option>
                                                                                    <option {{$schedule['from'] == '19:00' ? 'selected':''}} value="19:00" >07:00 PM</option>
                                                                                    <option {{$schedule['from'] == '20:00' ? 'selected':''}} value="20:00" >08:00 PM</option>
                                                                                    <option {{$schedule['from'] == '21:00' ? 'selected':''}} value="21:00" >09:00 PM</option>
                                                                                    <option {{$schedule['from'] == '22:00' ? 'selected':''}} value="22:00" >10:00 PM</option>
                                                                                    <option {{$schedule['from'] == '23:00' ? 'selected':''}} value="23:00" >11:00 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control" name="offer[schedule][{{$key}}][end]">
                                                                                    <option value="">To</option>
                                                                                    <option {{$schedule['end'] == '00:00' ? 'selected':''}} value="00:00" >12:00 AM</option>
                                                                                    <option {{$schedule['end'] == '01:00' ? 'selected':''}} value="01:00" >01:00 AM</option>
                                                                                    <option {{$schedule['end'] == '02:00' ? 'selected':''}} value="02:00">02:00 AM</option>
                                                                                    <option {{$schedule['end'] == '03:00' ? 'selected':''}} value="03:00" >03:00 AM</option>
                                                                                    <option {{$schedule['end'] == '04:00' ? 'selected':''}} value="04:00" >04:00 AM</option>
                                                                                    <option {{$schedule['end'] == '05:00' ? 'selected':''}} value="05:00" >05:00 AM</option>
                                                                                    <option {{$schedule['end'] == '06:00' ? 'selected':''}} value="06:00" >06:00 AM</option>
                                                                                    <option {{$schedule['end'] == '07:00' ? 'selected':''}} value="07:00" >07:00 AM</option>
                                                                                    <option {{$schedule['end'] == '08:00' ? 'selected':''}} value="08:00" >08:00 AM</option>
                                                                                    <option {{$schedule['end'] == '09:00' ? 'selected':''}} value="09:00" >09:00 AM</option>
                                                                                    <option {{$schedule['end'] == '10:00' ? 'selected':''}} value="10:00" >10:00 AM</option>
                                                                                    <option {{$schedule['end'] == '11:00' ? 'selected':''}} value="11:00" >11:00 AM</option>
                                                                                    <option {{$schedule['end'] == '12:00' ? 'selected':''}} value="12:00" >12:00 PM</option>
                                                                                    <option {{$schedule['end'] == '13:00' ? 'selected':''}} value="13:00" >01:00 PM</option>
                                                                                    <option {{$schedule['end'] == '14:00' ? 'selected':''}} value="14:00" >02:00 PM</option>
                                                                                    <option {{$schedule['end'] == '15:00' ? 'selected':''}} value="15:00" >03:00 PM</option>
                                                                                    <option {{$schedule['end'] == '16:00' ? 'selected':''}} value="16:00" >04:00 PM</option>
                                                                                    <option {{$schedule['end'] == '17:00' ? 'selected':''}} value="17:00" >05:00 PM</option>
                                                                                    <option {{$schedule['end'] == '18:00' ? 'selected':''}} value="18:00" >06:00 PM</option>
                                                                                    <option {{$schedule['end'] == '19:00' ? 'selected':''}} value="19:00" >07:00 PM</option>
                                                                                    <option {{$schedule['end'] == '20:00' ? 'selected':''}} value="20:00" >08:00 PM</option>
                                                                                    <option {{$schedule['end'] == '21:00' ? 'selected':''}} value="21:00" >09:00 PM</option>
                                                                                    <option {{$schedule['end'] == '22:00' ? 'selected':''}} value="22:00" >10:00 PM</option>
                                                                                    <option {{$schedule['end'] == '23:00' ? 'selected':''}} value="23:00" >11:00 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
            
                                                        @endforeach
                                                        
                                                    </div>
                                                    @endif
                                                    <div class="repeater time-form">
                                                        
                                                        <div align="right"><button data-repeater-create class="add-btn btn btn-theme ion-android-add" type="button" onclick="addScheduleTime()"></button></div>
                                                        <div id="variation_div"></div>
                                                    </div>
                                                            
                                                            <div class="form-group border-0"><button type="button" class="btn btn-theme" id="btn_edit_menuoffer" data-menu="{{$edit_menu->id}}">Schedule Item</button></div>
{!!Form::close()!!}

<script type="text/javascript">
      <?php if (!empty(@$edit_menu->offer)):  $s = count($edit_menu->offer['schedule']);
    $c = $s+1 ; ?>
    var count = {!!$s!!} + 1;  
    <?php else: $c = 1; ?>  var count = 1;
  <?php endif ?>
       function addScheduleTime(){  
   var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);
                
  newTextBoxDiv.after().load('{{guard_url("restaurant/menu/scheduleForm")}}'+'/'+count);  

            
  newTextBoxDiv.appendTo("#variation_div");
    count ++;

  }
</script>