@include('restaurant::default.restaurant.partial.header')
<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
                <i class="flaticon-price-cut app-sec-title-icon"></i>
                <h1>Promotions</h1>
                <div class="actions">
                    <button type="button" class="btn btn-with-icon btn-dark"><i class="fas fa-save mr-5"></i>Save</button>
                </div>
                <a href="website.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title mb-15">
                    <h2>Upload Promotions</h2>
                    <p>Upload an image with promotional information to market to your website visitors.</p>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</div>