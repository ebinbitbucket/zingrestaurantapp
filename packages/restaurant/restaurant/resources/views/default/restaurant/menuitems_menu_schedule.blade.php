<div class="menu-element-item-edit-block" id="Schedule_grilled_cheese_sandwich">
                                                        <h3>Schedule Grilled Cheese Sandwich</h3>
                                                        <form action="#" class="compact-edit">
                                                            <div class="row align-items-center">
                                                                <div class="col-5">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="monday">
                                                                        <label class="custom-control-label" for="monday">Monday</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>From</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>To</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row align-items-center">
                                                                <div class="col-5">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="tuesday">
                                                                        <label class="custom-control-label" for="tuesday">Tuesday</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>From</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>To</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row align-items-center">
                                                                <div class="col-5">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="wednesday">
                                                                        <label class="custom-control-label" for="wednesday">Wednesday</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>From</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>To</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row align-items-center">
                                                                <div class="col-5">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="thursday">
                                                                        <label class="custom-control-label" for="thursday">Thursday</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>From</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>To</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row align-items-center">
                                                                <div class="col-5">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="friday">
                                                                        <label class="custom-control-label" for="friday">Friday</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>From</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>To</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row align-items-center">
                                                                <div class="col-5">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="saturday">
                                                                        <label class="custom-control-label" for="saturday">Saturday</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>From</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>To</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row align-items-center mb-20">
                                                                <div class="col-5">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="sunday">
                                                                        <label class="custom-control-label" for="sunday">Sunday</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>From</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control">
                                                                                    <option>To</option>
                                                                                    <option>11 AM</option>
                                                                                    <option>12 PM</option>
                                                                                    <option>1 PM</option>
                                                                                    <option>2 PM</option>
                                                                                    <option>3 PM</option>
                                                                                    <option>4 PM</option>
                                                                                    <option>5 PM</option>
                                                                                    <option>6 PM</option>
                                                                                    <option>7 PM</option>
                                                                                    <option>8 PM</option>
                                                                                    <option>9 PM</option>
                                                                                    <option>10 PM</option>
                                                                                    <option>11 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            
                                                            <div class="form-group border-0"><button type="submit" class="btn btn-theme">Schedule Item</button></div>
                                                        </form>
                                                    </div>