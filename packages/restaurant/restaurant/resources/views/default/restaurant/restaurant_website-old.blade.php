<link
    href="https://fonts.googleapis.com/css?family=Lato:400,700|Merriweather:400,700|Oswald:400,700|Overlock:400,700|Poppins:400,500,700&display=swap"
    rel="stylesheet">
<aside class="main-nav">
    <div class="nav-inner">
    @include('restaurant::default.restaurant.partial.mobile_menu')

    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-3 d-none d-lg-block">
                <aside class="dashboard-sidemenu">
                @include('restaurant::default.restaurant.partial.left_menu')

                </aside>
            </div>
            <div class="col-md-12 col-lg-9">
                <div class="element-wrapper">
                    <div class="element-box">
                        {!!Form::vertical_open()
                        ->id('restaurant-restaurant-edit')
                        ->method('POST')
                        ->enctype('multipart/form-data')
                        ->action(guard_url('restaurant/update/'. user()->getRouteKey()))!!}
                        <div class="element-info">
                            <div class="element-info-with-icon">
                                <div class="element-info-icon">
                                    <div class="icon ion-social-buffer"></div>
                                </div>
                                <div class="element-info-text">
                                    <h5 class="element-inner-header">Restaurant Details</h5>
                                    <div class="element-inner-desc"></div>
                                </div>
                                @if(user()->website_status=='Published')
                                <div class="element-info-text"><a href="{{url('domain/'.user()->id)}}"
                                        class="btn btn-theme">View website</a>
                                        <button type="submit"
                                        name="website_status" value="Unpublished"
                                        class="btn btn-theme ml-20">Unpublish</button>
                                    
                                        {{-- <a id="website_refresh" href="{{@user()->website_domain}}/?refresh=1"
                                            class="btn btn-success" onclick="window.open('{{@user()->website_domain}}/menu.php?refresh=1');
                                               window.open('{{@user()->website_domain}}/gallery.php?refresh=1');
                                               window.open('{{@user()->website_domain}}/details.php?refresh=1'); window.open('{{@user()->website_domain}}/locations.php?refresh=1')"
                                              >Refresh website</a> --}}

                                              <div class="btn-group">
                                                {{-- <button type="button" class="btn btn-success">Action</button> --}}
                                                <button type="button" class="btn btn-theme dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    Refresh Pages
                                                  <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <div class="dropdown-menu">
                                                  <a class="dropdown-item" target="_blank" href="{{@user()->website_domain}}/?refresh=1">Home</a>
                                                  <a class="dropdown-item" target="_blank" href="{{@user()->website_domain}}/about.php?refresh=1">About</a>
                                                  <a class="dropdown-item" target="_blank" href="{{@user()->website_domain}}/menu.php?refresh=1">Menu</a>
                                                  <div class="dropdown-divider"></div>
                                                  <a class="dropdown-item" target="_blank" href="{{@user()->website_domain}}/gallery.php?refresh=1">Gallery</a>
                                                  <a class="dropdown-item" target="_blank" href="{{@user()->website_domain}}/details.php?refresh=1">Details</a>
                                                  <div class="dropdown-divider"></div>
                                                  <a class="dropdown-item" target="_blank" href="{{@user()->website_domain}}/locations.php?refresh=1">Locations</a>
                                                  <a class="dropdown-item" target="_blank" href="{{@user()->website_domain}}/contact.php?refresh=1">Contact</a>
                                                </div>
                                              </div>
                                        </div>                                       
                                @endif
                                @if(user()->website_status=='Unpublished')
                                <div class="element-info-text "><button type="submit" name="website_status"
                                        value="Published" class="btn btn-theme">Publish Website</button></div>
                                @endif

                            </div>
                        </div>
                        @include('notifications')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Title*</label>
                                    <input class="form-control" placeholder="Title" required="required" name="title"
                                        type="text" value="{{user()->title}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Subtitle</label>
                                    <input type="text" class="form-control" name="subtitle" placeholder="Subtitle"
                                        value="{{user()->subtitle}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Meta Tags</label>
                                    <input type="text" class="form-control" name="meta_tags" placeholder="Meta tags"
                                        value="{{user()->meta_tags}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Meta Keywords</label>
                                    <input type="text" class="form-control" name="meta_keywords"
                                        placeholder="Meta Keywords" value="{{user()->meta_keywords}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Meta Description</label>
                                    <textarea class="form-control" name="meta_description"
                                        placeholder="Meta Description" rows="6">{{user()->meta_description}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">About Us</label>
                                    <textarea class="form-control" name="description" placeholder="specialities"
                                        rows="6">{{user()->description}}</textarea>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="text" required class="form-control" name="customer_email"
                                        placeholder="Contact email" value="{{user()->customer_email}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Alternate Phone</label>
                                    <input type="text" class="form-control" name="customer_phone"
                                        placeholder="Contact number" value="{{@user()->customer_phone}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Domain</label>
                                    <input type="text" class="form-control" name="website_domain"
                                        placeholder="Domain" value="{{@user()->website_domain}}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="">Button</label>
                                    <input type="text" class="form-control" name="link[text]"
                                        placeholder="Button Name" value="{{@user()->link[text]}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""></label>
                                    <input type="text" class="form-control" name="link[url]" placeholder="Link"
                                        value="{{@user()->link[url]}}">
                                </div>
                            </div>
                           
                       
                    
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="" class="d-block">Locations </label>
                                    {{-- <span class="fa fa-plus-square  btn-success" onclick="addLoc()"></span> --}}
                                        <button id="addLoc" class="btn btn-theme" type="button">
                                            Add</button>
                                </div>
                             
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="repeater">
                                <div class="position-relative" data-repeater-item>
                                    {{-- <button data-repeater-delete class="delete-btn ion-trash-a" type="button"></button> --}}
                                    </br>
                                    @if(is_array(user()->locations))
                                    @foreach(user()->locations as $loc_key => $loc_value) 
                                    <br>
                                    <div class="row border" id="loc_div_{{$loc_key}}">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                {{-- @if($loc_key == 0) --}}
                                                <label> Address <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Address e.g. Fort Worth, TX"></i></label>
                                                {{-- @endif --}}
                                                <input type="text" id="fee_address_{{$loc_key}}"
                                                    name="locations[{{$loc_key}}][address]" class="form-control"
                                                    value="{{@$loc_value['address']}}" placeholder="Address">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label> Location <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Location Name E.g. Tiger Xpress on Beach St, Fort Worth"></i>
                                                </label>
                                                <input type="text" id="fee_title_{{$loc_key}}"
                                                    name="locations[{{$loc_key}}][text]" class="form-control"
                                                    value="{{@$loc_value['text']}}" placeholder="Location Name">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                {{-- @if($loc_key == 0) --}}
                                                <label> Email <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Email where inquiries from Customers will be sent"></i>
                                                </label>
                                                {{-- @endif --}}
                                                <input type="email" id="fee_email_{{$loc_key}}"
                                                    name="locations[{{$loc_key}}][email]" class="form-control"
                                                    value="{{@$loc_value['email']}}" placeholder="Email Address">

                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label> Phone <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Phone Number of the Location"></i>
                                                </label>
                                                <input type="text" id="fee_phone_{{$loc_key}}"
                                                    name="locations[{{$loc_key}}][phone]" class="form-control"
                                                    value="{{@$loc_value['phone']}}" placeholder="Phone">

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Order Link
                                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Zing Link to order from this location"></i>
                                                </label>
                                                <input type="url" id="fee_amt_{{$loc_key}}"
                                                    name="locations[{{$loc_key}}][url]" class="form-control"
                                                    value="{{@$loc_value['url']}}" placeholder="URL">

                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label for="" class="d-block"> </label>
                                                <button class="btn btn-theme form-control" type="button"
                                                    id="fee_rmvbtn_{{$loc_key}}"
                                                    onclick="removeLoc({{$loc_key}})" ><i
                                                        class="fa fa-trash"></i></button>

                                            </div>
                                        </div>
                                    </div>

                                    @endforeach
                                    @endif
                                    <div id="loc_div"></div>

                                </div>
                            </div>
                        </div>
                       <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Contact Information</label>
                                <textarea class="form-control" name="contact_info" placeholder="Contact information text"
                                    rows="2">{{user()->contact_info}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="">Add menu name</label>
                                <input type="text" name="menu_button[name]" placeholder="Menu name" value="{{@user()->menu_button[name]}}" class="form-control">
                            </div>
                        </div>
                         <div class="col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="">Add content</label>
                        {!! Form::textarea('menu_button[content]')
                        -> label(trans('page::page.label.content'))
                        -> value((@user()->menu_button['content']))
                        -> dataUpload(url(user()->getUploadURL('menu_button')))
                        -> addClass('html-editor')
                        -> placeholder(trans('page::page.placeholder.content'))
                        !!}
                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group border-0">
                                <label for="">Banner Image</label>
                                <div class="dropzone dropzone-previews mt-10">
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('banner_image')
                                            ->url(user()->getUploadUrl('banner_image'))
                                            ->mime(config('filer.image_extensions'))
                                            ->dropzone()!!}
                                        </div>
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('banner_image')
                                            ->editor()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-group border-0">
                                <label for="">About Us Image</label>
                                <div class="dropzone dropzone-previews mt-10">
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('spcialities_image')
                                            ->url(user()->getUploadUrl('spcialities_image'))
                                            ->mime(config('filer.image_extensions'))
                                            ->dropzone()!!}
                                        </div>
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('spcialities_image')
                                            ->editor()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group border-0">
                                <label for="">Featured Food</label>
                                <div class="dropzone dropzone-previews mt-10">
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('featured_food')
                                            ->url(user()->getUploadUrl('featured_food'))
                                            ->mime(config('filer.image_extensions'))
                                            ->dropzone()!!}
                                        </div>
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('featured_food')
                                            ->editor()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group border-0">
                                <label for="">Gallery</label>
                                <div class="dropzone dropzone-previews mt-10">
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('website_gallery')
                                            ->url(user()->getUploadUrl('website_gallery'))
                                            ->mime(config('filer.image_extensions'))
                                            ->dropzone()!!}
                                        </div>
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('website_gallery')
                                            ->editor()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group border-0">
                                <label for="">Business Hours</label>
                                <div class="dropzone dropzone-previews mt-10">
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('businesshours_image')
                                            ->url(user()->getUploadUrl('businesshours_image'))
                                            ->mime(config('filer.image_extensions'))
                                            ->dropzone()!!}
                                        </div>
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('businesshours_image')
                                            ->editor()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group border-0">
                                <label for="">Header Image</label>
                                <div class="dropzone dropzone-previews mt-10">
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('header_image')
                                            ->url(user()->getUploadUrl('header_image'))
                                            ->mime(config('filer.image_extensions'))
                                            ->dropzone()!!}
                                        </div>
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('header_image')
                                            ->editor()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group border-0">
                                <label for="">Review Image</label>
                                <div class="dropzone dropzone-previews mt-10">
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('review_image')
                                            ->url(user()->getUploadUrl('review_image'))
                                            ->mime(config('filer.image_extensions'))
                                            ->dropzone()!!}
                                        </div>
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('review_image')
                                            ->editor()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-12">
                            <div class="customizer">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group bg-color">
                                            <label>Theme Color</label><br />
                                            <input type="radio" name="theme_color" id="orange" value="orange"
                                                {{user()->theme_color == "orange" ? 'checked' : ''}}><span
                                                class="orange" style="background-color: #ff7200"></span>
                                            <input type="radio" name="theme_color" id="purple" value="purple"
                                                {{user()->theme_color == "purple" ? 'checked' : ''}}><span
                                                class="purple" style="background-color: #BF55EC"></span>
                                            <input type="radio" name="theme_color" id="blue" value="blue"
                                                {{user()->theme_color == "blue" ? 'checked' : ''}}><span class="blue"
                                                style="background-color: #007bff"></span>
                                            <input type="radio" name="theme_color" id="red" value="red"
                                                {{user()->theme_color == "red" ? 'checked' : ''}}><span class="red"
                                                style="background-color: #f5365c"></span>
                                            <input type="radio" name="theme_color" id="green" value="green"
                                                {{user()->theme_color == "green" ? 'checked' : ''}}><span class="green"
                                                style="background-color: #2dce89"></span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group bg-color">
                                            <label>Background Color</label><br />
                                            <input type="radio" name="bg_color" id="orange" value="orange"
                                                {{user()->bg_color == "orange" ? 'checked' : ''}}><span class="orange"
                                                style="background-color: #ff7200"></span>
                                            <input type="radio" name="bg_color" id="purple" value="purple"
                                                {{user()->bg_color == "purple" ? 'checked' : ''}}><span class="purple"
                                                style="background-color: #BF55EC"></span>
                                            <input type="radio" name="bg_color" id="blue" value="blue"
                                                {{user()->bg_color == "blue" ? 'checked' : ''}}><span class="blue"
                                                style="background-color: #007bff"></span>
                                            <input type="radio" name="bg_color" id="red" value="red"
                                                {{user()->bg_color == "red" ? 'checked' : ''}}><span class="red"
                                                style="background-color: #f5365c"></span>
                                            <input type="radio" name="bg_color" id="green" value="green"
                                                {{user()->bg_color == "green" ? 'checked' : ''}}><span class="green"
                                                style="background-color: #2dce89"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Theme Font</label><br />
                                            <select name="theme_font" class="form-control font">
                                                <option value="poppins"
                                                    {{user()->theme_font == "poppins" ? 'selected' : ''}}><span
                                                        class="poppins"
                                                        style="font-family: 'Poppins', sans-serif;">Poppins</span>
                                                </option>
                                                <option value="lato" {{user()->theme_font == "lato" ? 'selected' : ''}}>
                                                    <span class="lato"
                                                        style="font-family: 'Lato', sans-serif;">Lato</span>
                                                </option>
                                                <option value="oswald"
                                                    {{user()->theme_font == "oswald" ? 'selected' : ''}}><span
                                                        class="oswald"
                                                        style="font-family: 'Oswald', sans-serif;">Oswald</span>
                                                </option>
                                                <option value="merriweather"
                                                    {{user()->theme_font == "merriweather" ? 'selected' : ''}}><span
                                                        class="merriweather"
                                                        style="font-family: 'Merriweather', serif;">Merriweather</span>
                                                </option>
                                                <option value="overlock"
                                                    {{user()->theme_font == "overlock" ? 'selected' : ''}}><span
                                                        class="overlock"
                                                        style="font-family: 'Overlock', cursive;">Overlock</span>
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Design</label><br />
                                            <select name="theme_design" class="form-control font">
                                                <option value="Default"
                                                    {{user()->theme_design == "Default" ? 'selected' : ''}}><span>Default</span>
                                                </option>
                                                <option value="Design1" {{user()->theme_design == "Design1" ? 'selected' : ''}}>
                                                    <span>Design1</span>
                                                </option>
                                                <option value="Design2" {{user()->theme_design == "Design2" ? 'selected' : ''}}>
                                                    <span>Design2</span>
                                                </option>
                                                <option value="Design3" {{user()->theme_design == "Design3" ? 'selected' : ''}}>
                                                    <span>Design3</span>
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                          <div class="col-md-6">
                            <div class="form-group border-0">
                                <label for="">Promotions</label>
                                <div class="dropzone dropzone-previews mt-10">
                                    <div class="row">
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('offer_image')
                                            ->url(user()->getUploadUrl('offer_image'))
                                            ->mime(config('filer.image_extensions'))
                                            ->dropzone()!!}
                                        </div>
                                        <div class='col-lg-12 col-sm-12'>
                                            {!! user()->files('offer_image')
                                            ->editor()!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-buttons-w text-center">
                        <button class="btn btn-theme" type="submit" style="height: 30px; width: 150px;">
                            Update</button>
                        <!--  <button class="btn btn-danger" type="button"> Cancel</button> -->
                    </div>
                    {!!Form::close()!!}
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<style>
.customizer {

    font-family: 'Poppins', sans-serif;
}

.customizer .customizer-toggle {
    position: absolute;
    top: 35%;
    width: 54px;
    height: 50px;
    left: -54px;
    text-align: center;
    line-height: 46px;
    cursor: pointer;
    border-radius: 15px 0 0 15px;
    color: #fff !important;
    font-size: 26px;
    display: block;
    -webkit-box-shadow: -3px 0 8px rgba(0, 0, 0, 0.1);
    box-shadow: -3px 0 8px rgba(0, 0, 0, 0.1);
}

.customizer .bg-color {
    padding: 10px;
}

.customizer .bg-color span {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    display: inline-block;
    margin: 5px;
    position: relative;
    text-align: center;
    line-height: 20px;
    cursor: pointer;
}

.customizer .bg-color span::before {
    content: "\f00c";
    font-family: "Font Awesome 5 Free";
    color: #fff;
    font-size: 12px;
    font-weight: 900;
    opacity: 0;
    -o-transition: 0.3s ease-in-out;
    -webkit-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}

.customizer .bg-color span.selected::before {
    transform: scale(1);
    opacity: 1;
}

.customizer p {
    font-size: 14px;
    margin: 0;
}

.customizer .font {
    padding: 10px;
}

.customizer .font span {
    display: block;
    padding: 3px 0;
    cursor: pointer;
}

.customizer .font span::before {
    content: "\f00c";
    font-family: "Font Awesome 5 Free";
    color: #333;
    font-size: 12px;
    font-weight: 900;
    opacity: 0;
    margin-right: 5px;
    -o-transition: 0.3s ease-in-out;
    -webkit-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}

.customizer .font span.selected::before {
    transform: scale(1);
    opacity: 1;
}
.note-editor.note-frame .note-editing-area .note-editable {
    padding: 10px;
    overflow: auto;
    color: #000;
    word-wrap: break-word;
    background-color: #fff;
}
textarea.form-control {
    height: auto;
}

</style>
<script>    
  $(function () {
  $('[data-toggle="tooltip"]').tooltip();
})
$("#addLoc").click(function(event){
  event.preventDefault();
  addLoc();
//   var elmnt = document.getElementById("LocEdit");
//   elmnt.scrollIntoView();
});  
    var count=0;
    <?php if (!empty(@user()->locations) && is_array(user()->locations)):  $f = last(array_keys(user()->locations));?>
    var fee_count = {!!$f!!} + 1; 
    <?php else: ?>  var fee_count = 1;
    <?php endif ?>
    // function addLoc() {
       
    //     var newTextBoxDiv = $(document.createElement('div'))
    //         .attr("id", 'loc_div_' + fee_count);
    //     newTextBoxDiv.after().html(
    //         '<div data-repeater-list="group-a"><div class="position-relative" data-repeater-item><div class="row"><div class="col-md-3"><div class="form-group"><input type="text" name="locations[' +
    //         fee_count +
    //         '][text]" class="form-control" placeholder="Location"></div></div><div class="col-md-3"><div class="form-group"><input type="text" name="locations[' +
    //         fee_count +
    //         '][phone]" class="form-control" placeholder="Phone"></div></div><div class="col-md-5"><div class="form-group"><input type="url" name="locations[' +
    //         fee_count +
    //         '][url]" class="form-control" placeholder="URL"></div></div><div class="col-md-1"><button  data-repeater-delete class="btn btn-success form-control" type="button" onclick="removeLoc(' +
    //         fee_count + ')" ><i class="fa fa-trash"></i></button></div></div></div></div> ');


    //     newTextBoxDiv.appendTo("#loc_div ");
    //     fee_count++;
    // }

    function addLoc() {
       
       var newTextBoxDiv = $(document.createElement('div'))
           .attr("id", 'loc_div_' + fee_count);
       newTextBoxDiv.after().html(
           '<div data-repeater-list="group-a" id="LocEdit"><div class="position-relative" data-repeater-item><br><div class="row border"><div class="col-md-6"><div class="form-group"><label> Address </label><input type="text" name="locations[' +
           fee_count +'][address]" class="form-control" placeholder="Address"></div></div><div class="col-md-6"><div class="form-group"><label> Location </label><input type="text" name="locations[' +
           fee_count +'][text]" class="form-control" placeholder="Location Name"></div></div><div class="col-md-4"><div class="form-group"><label> Email </label><input type="email" name="locations[' +fee_count +'][email]" class="form-control" placeholder="Email Address"></div></div><div class="col-md-3"><div class="form-group"><label> Phone </label><input type="text" name="locations[' +fee_count +'][phone]" class="form-control" placeholder="Phone" pattern="^([0-9\s\-\+\(\)]*)$"></div></div><div class="col-md-4"><div class="form-group"><label> Order Link </label><input type="url" name="locations[' +
           fee_count +'][url]" class="form-control" placeholder="Order Link"></div></div><div class="col-md-1"><button  data-repeater-delete class="btn btn-theme form-control" type="button" onclick="removeLoc(' +
           fee_count + ')" ><i class="fa fa-trash"></i></button></div></div></div></div> ');


       newTextBoxDiv.appendTo("#loc_div ");
       fee_count++;
   }
    function removeLoc(key) {
    document.getElementById('loc_div_'+key).remove();  
  }
 
</script>
