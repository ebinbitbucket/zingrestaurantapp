<div class="menu-item-details">
                                                            <div class="actions">
                                                                <a href="#" class="btn edit-btn edit-menu flaticon-edit" data-title="{{$menuItem->name}}" onclick="updatemenu('{{$menuItem->id}}')"></a>
                                                                <a href="#" data-toggle="tooltip" title="Schedule Menu Timing!" class="btn edit-btn flaticon-timer" onclick="updatemenuschedule('{{$menuItem->id}}')"></a>
                                                                <a href="#" data-toggle="tooltip" title="Schedule Offer!" class="btn edit-btn fa fa-star-o" onclick="updatemenuoffer('{{$menuItem->id}}')"></a>
                                                                <form id="delete-menu_{!! $menuItem->getRouteKey() !!}" style="display: inline-block;" method="POST" action="{!! guard_url('restaurant/menu') !!}/{!! $menuItem->getRouteKey() !!}">
                                              {{csrf_field()}}{{method_field('DELETE')}}
                                             <button type="button" onclick="deletemenu('{!! $menuItem->getRouteKey() !!}','{{$menuItem->category_id}}')"  class="btn flaticon-garbage"></button></form>
                                                                <!-- <a href="#" class="btn flaticon-garbage bg-danger"></a> -->
                                                            </div>
                                                            <div class="img-wrap">
                                                                <img src="{{url($menuItem->defaultImage('image'))}}" class="img-fluid" alt="">
                                                            </div>
                                                            <h3>{{$menuItem->name}}</h3>
                                                            <div class="price">$ {{$menuItem->price}}</div>
                                                             <p>Master: <h6>{{@$menuItem->master->name}}</h6></p>
                                                            <p>{{$menuItem->description}}</p>
                                                        </div>
                                                        <h2 class="item-detail-sectitle d-flex align-items-center justify-content-between">Addons <button type="button" onclick="add_menuAddon('{{$menuItem->id}}')" class="btn btn-theme ion-android-add add-addon"></button></h2>
                                                        <div class="menu-item-details">
                                                            <div class="addon-item-wrap">
                                                                @foreach($menuItem->addon as $addons)
                                                                <div class="addon-item">
                                                                    <p>{{$addons->name}}</p>
                                                                    <div class="addon-actions">
                                                                        <a href="#" class="btn edit-btn flaticon-edit text-primary" onclick="updatemenuaddon('{{$menuItem->id}}','{{$addons->id}}')" ></a>

                                                                        <form id="delete-addon_{!! $menuItem->getRouteKey() !!}_{{ $addons->id }}" style="display: inline-block;" action="{!! guard_url('restaurant/menu/remove_addon') !!}/{!! $menuItem->getRouteKey() !!}/{{ $addons->id }}">
                                                                            
                                             <button type="button" onclick="deleteaddon('{!! $menuItem->getRouteKey() !!}','{{ $addons->id }}','{{ $menuItem->id }}')" class="btn edit-btn flaticon-garbage text-danger" onclick="" style="border: none; background: none; padding: 0;"></button></form>

                                                                        
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <h2 class="item-detail-sectitle d-flex align-items-center justify-content-between">Variations <button type="button" class="btn btn-theme ion-android-add add-variation" onclick="add_menuVariation('{{$menuItem->id}}')"></button></h2>
                                                        <div class="menu-item-details">
                                                            <div class="addon-item-wrap">
                                                              @if(!empty($menuItem->menu_variations))
                                                               @foreach($menuItem->menu_variations  as $key => $variation)
                                                                <div class="addon-item">
                                                                    <p>{{$variation['name']}}<span>- ${{$variation['price']}}</span></p>
                                                                    <div class="addon-actions">
                                                                        <a href="#" class="btn edit-btn flaticon-view text-primary" onclick="updatemenuvariation('{{$menuItem->id}}','{{$key}}')"></a>
                                                                        <form id="delete-variation_{!! $menuItem->getRouteKey() !!}_{{ $key }}" style="display: inline-block;" action="{!! guard_url('restaurant/menu/remove_var') !!}/{!! $menuItem->getRouteKey() !!}/{{ $key }}">
                                                                            @foreach($menuItem->menu_variations  as $keys => $sample)
                                                                    <input type="hidden" name="variations[{{$keys}}][name]" value="{{$sample['name']}}">
                                                                    <input type="hidden" name="variations[{{$keys}}][price]" value="{{$sample['price']}}">
                                                                    <input type="hidden" name="variations[{{$keys}}][description]" value="{{$sample['description']}}">
                                                                    @endforeach
                                             <button type="button" onclick="deletevariation('{!! $menuItem->getRouteKey() !!}','{{ $key }}','{{ $menuItem->id }}')" class="btn edit-btn flaticon-garbage text-danger" style="border: none; background: none; padding: 0;"></button></form>

                                                                       
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                                @endif
                                                            </div>
                                                        </div>
                                                        