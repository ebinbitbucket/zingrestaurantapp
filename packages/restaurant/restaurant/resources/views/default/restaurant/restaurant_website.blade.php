
@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
    <div class="app-content-inner">
        {!!Form::vertical_open()
            ->id('restaurant-restaurant-edit')
            ->method('POST')
            ->enctype('multipart/form-data')
            ->action(guard_url('restaurant/update/'. user()->getRouteKey()))!!}
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action actions-mobile-bottom">
                <i class="flaticon-design app-sec-title-icon"></i>
                <h1>Website Set Up</h1>
                <div class="actions">
                    @if(user()->website_status=='Published')
                    <a href="{{url('domain/'.user()->id)}}">
                    <button type="button" class="btn btn-with-icon btn-secondary"><i class="fas fa-eye mr-5"></i>View</button></a>
                    <button type="submit" name="website_status" value="Unpublished" class="btn btn-with-icon btn-secondary">
                        <i class="fas fa-eye-slash mr-5"></i>Unpublish</button>
                    @else
                    <button type="submit" name="website_status" value="Published" class="btn btn-with-icon btn-secondary">
                        <i class="fas fa-eye mr-5"></i>Publish Website</button>
                    @endif
                    <button type="submit" class="btn btn-with-icon btn-dark"><i class="fas fa-save mr-5"></i>Save</button>
                </div>
                <a href="{{guard_url('restaurant/view-website-setup')}}" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div> 
            <div class="app-entry-form-section">
                <div class="entry-form-title" data-toggle="collapse" data-target="#designTemplate" aria-expanded="true" aria-controls="designTemplate">
                    <h2>Design Template</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse show" id="designTemplate">
                    <div class="row mt-15">
                        <div class="col-md-6">
                            <div class="web-teplates-wrap mb-15">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="teplate-item">
                                            <input type="radio" value="Default" {{user()->theme_design == "Default" ? 'checked' : ''}} id="template_00" name="theme_design">
                                            <label for="template_00">
                                                <span class="teplate-img" style="background-image: url('{{theme_asset('img/website-design/default.png')}}')"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="teplate-item">
                                            <input type="radio" value="Design1" {{user()->theme_design == "Design1" ? 'checked' : ''}} id="template_01" name="theme_design">
                                            <label for="template_01">
                                                <span class="teplate-img" style="background-image: url('{{theme_asset('img/website-design/theme-1.png')}}')"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="teplate-item">
                                            <input type="radio" value="Design2" {{user()->theme_design == "Design2" ? 'checked' : ''}} id="template_02" name="theme_design">
                                            <label for="template_02">
                                                <span class="teplate-img" style="background-image: url('{{theme_asset('img/website-design/theme-2.png')}}')"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="teplate-item">
                                            <input type="radio" value="Design3" {{user()->theme_design == "Design3" ? 'checked' : ''}} id="template_03" name="theme_design">
                                            <label for="template_03">
                                                <span class="teplate-img" style="background-image: url('{{theme_asset('img/website-design/theme-3.png')}}')"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6"> 
                            <div class="web-font-wrap mt-0">
                                <div class="section-title-sm">Theme Font</div>
                                <div class="font-item">
                                    <input type="radio" id="font_poppins" value="poppins" name="theme_font" {{user()->theme_font == "poppins" ? 'checked' : ''}}>
                                    <label for="font_poppins" style="font-family: 'Poppins', sans-serif;">Poppins</label>
                                </div>
                                <div class="font-item">
                                    <input type="radio" id="font_lato" value="lato" name="theme_font" {{user()->theme_font == "lato" ? 'checked' : ''}}>
                                    <label for="font_lato" style="font-family: 'Lato', sans-serif;">Lato</label>
                                </div>
                                <div class="font-item">
                                    <input type="radio" id="font_osawald" value="oswald" name="theme_font" {{user()->theme_font == "oswald" ? 'checked' : ''}}>
                                    <label for="font_osawald" style="font-family: 'Oswald', sans-serif;">Oswald</label>
                                </div>
                                <div class="font-item">
                                    <input type="radio" id="font_merriweather" value="merriweather" name="theme_font" {{user()->theme_font == "merriweather" ? 'checked' : ''}}>
                                    <label for="font_merriweather" style="font-family: 'Merriweather', serif;">Merriweather</label>
                                </div>
                                <div class="font-item">
                                    <input type="radio" id="font_overlock"  value="overlock" name="theme_font" {{user()->theme_font == "overlock" ? 'checked' : ''}}>
                                    <label for="font_overlock" style="font-family: 'Overlock', cursive;">Overlock</label>
                                </div>
                            </div>
                            <div class="web-color-wrap">
                                <div class="section-title-sm">Theme Color</div>
                                <div class="color-item">
                                    <input type="radio" name="theme_color" id="orange" value="orange" {{user()->theme_color == "orange" ? 'checked' : ''}}>
                                    <label for="orange" style="background-color: #ff7200;"></label>
                                </div>
                                <div class="color-item">
                                    <input type="radio" name="theme_color" id="purple" value="purple" {{user()->theme_color == "purple" ? 'checked' : ''}}>
                                    <label for="purple" style="background-color: #BF55EC;"></label>
                                </div>
                                <div class="color-item">
                                    <input type="radio" name="theme_color" id="blue" value="blue" {{user()->theme_color == "blue" ? 'checked' : ''}}>
                                    <label for="blue" style="background-color: #007bff;"></label>
                                </div>
                                <div class="color-item">
                                    <input type="radio" name="theme_color" id="red" value="red" {{user()->theme_color == "red" ? 'checked' : ''}}>
                                    <label for="red" style="background-color: #f5365c;"></label>
                                </div>
                                <div class="color-item">
                                    <input type="radio" name="theme_color" id="green" value="green" {{user()->theme_color == "green" ? 'checked' : ''}}>
                                    <label for="green" style="background-color: #2dce89;"></label>
                                </div>
                            </div>
                            <div class="web-color-wrap">
                                <div class="section-title-sm">Background Color</div>
                                <div class="color-item">
                                    <input type="radio" name="bg_color" id="bgc_orange" value="orange" {{user()->bg_color == "orange" ? 'checked' : ''}}>
                                    <label for="bgc_orange" style="background-color: #ff7200;"></label>
                                </div>
                                <div class="color-item">
                                    <input type="radio" name="bg_color" id="bgc_purple" value="purple" {{user()->bg_color == "purple" ? 'checked' : ''}}>
                                    <label for="bgc_purple" style="background-color: #BF55EC;"></label>
                                </div>
                                <div class="color-item">
                                    <input type="radio" name="bg_color" id="bgc_blue" value="blue" {{user()->bg_color == "blue" ? 'checked' : ''}}>
                                    <label for="bgc_blue" style="background-color: #007bff;"></label>
                                </div>
                                <div class="color-item">
                                    <input type="radio" name="bg_color" id="bgc_red" value="red" {{user()->bg_color == "red" ? 'checked' : ''}}>
                                    <label for="bgc_red" style="background-color: #f5365c;"></label>
                                </div>
                                <div class="color-item">
                                    <input type="radio" name="bg_color" id="bgc_green" value="green" {{user()->bg_color == "green" ? 'checked' : ''}}>
                                    <label for="bgc_green" style="background-color: #2dce89;"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#homePage" aria-expanded="false" aria-controls="homePage">
                    <h2>Home Page</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="homePage">
                    <div class="row mt-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Title <sup>*</sup></label>
                                <input class="form-control" placeholder="Title" required="required" name="title" type="text" value="{{user()->title}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Subtitle</label>
                                <input type="text" class="form-control" name="subtitle" placeholder="Subtitle" value="{{user()->subtitle}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">About Us</label>
                                <textarea class="form-control" name="description" placeholder="specialities" rows="4">{{user()->description}}</textarea>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Meta Tags</label>
                                <input type="text" class="form-control" name="meta_tags" placeholder="Meta tags" value="{{user()->meta_tags}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Meta Keywords</label>
                                <input type="text" class="form-control" name="meta_keywords" placeholder="Meta Keywords" value="{{user()->meta_keywords}}">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Meta Description</label>
                                <textarea class="form-control" name="meta_description" placeholder="Meta Description" rows="4">{{user()->meta_description}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#customPage" aria-expanded="false" aria-controls="customPage">
                    <h2>Custom Page</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="customPage">
                    <div class="row mt-15">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Custom Page Name</label>
                                <input type="text" name="menu_button[name]" placeholder="Menu name" value="{{@user()->menu_button[name]}}" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Custom Page Content</label>
                                <textarea class="form-control html-editor">gf</textarea>
                                {{-- {!! Form::textarea('menu_button[content]')
                                -> label(trans('page::page.label.content'))
                                -> value((@user()->menu_button['content']))
                                -> dataUpload(url(user()->getUploadURL('menu_button')))
                                -> addClass('html-editor')
                                -> placeholder(trans('page::page.placeholder.content'))
                                !!} --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#locations" aria-expanded="false" aria-controls="locations">
                    <h2>Locations</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="locations">
                    <button id="addLoc" class="btn btn-secondary mt-15 mb-10">Add Location</button>

                    @if(is_array(user()->locations))
                    @foreach(user()->locations as $loc_key => $loc_value)
                    <div class="locations-form-item">
                        <div class="row"  id="loc_div_{{$loc_key}}">
                            <div class="col-md-6">
                                <div class="form-group mb-10">
                                    <label> Address <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Address e.g. Fort Worth, TX"></i></label>
                                    <input type="text" id="fee_address_{{$loc_key}}" name="locations[{{$loc_key}}][address]"
                                        class="form-control" value="{{@$loc_value['address']}}" placeholder="Address">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-10">
                                    <label> Location <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Location Name E.g. Tiger Xpress on Beach St, Fort Worth"></i>
                                    </label>
                                    <input type="text" id="fee_title_{{$loc_key}}" name="locations[{{$loc_key}}][text]" value="{{@$loc_value['text']}}" class="form-control" placeholder="Location Name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">
                                    <label> Email <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Email where inquiries from Customers will be sent"></i>
                                    </label>

                                    <input type="email" id="fee_email_{{$loc_key}}" name="locations[{{$loc_key}}][email]" class="form-control"
                                        value="{{@$loc_value['email']}}" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">
                                    <label> Phone <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Phone Number of the Location"></i>
                                    </label>

                                    <input type="text" id="fee_phone_{{$loc_key}}" name="locations[{{$loc_key}}][phone]" class="form-control"
                                        value="{{@$loc_value['phone']}}" placeholder="Phone" pattern="^([0-9\s\-\+\(\)]*)$">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">
                                    <label>Order Link
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                                            title="" data-original-title="Zing Link to order from this location"></i>
                                    </label>
                                    <input type="url" id="fee_amt_{{$loc_key}}" name="locations[{{$loc_key}}][url]" class="form-control" value="{{@$loc_value['url']}}" placeholder="Website URL">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-dark btn-icon location-remove-btn" type="button" id="fee_rmvbtn_{{$loc_key}}" onclick="removeLoc({{$loc_key}})"><i
                                class="fa fa-trash"></i><span>Remove</span></button>
                    </div>
                    @endforeach
                    @endif
                    <div id="loc_div"></div>


                    {{-- <div class="locations-form-item">
                        <div class="row" id="loc_div_0">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" id="fee_title_0" name="locations[0][text]" class="form-control" value="forthworth" placeholder="Location">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <input type="text" id="fee_phone_0" name="locations[0][phone]" class="form-control" value="" placeholder="Phone">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="url" id="fee_amt_0" name="locations[0][url]" class="form-control" value="https://www.indiankitchenhaltomcity.com/" placeholder="URL">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-dark btn-icon location-remove-btn" type="button" id="fee_rmvbtn_0"><i class="fa fa-trash"></i><span>Remove</span></button>
                    </div> --}}
                  
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#gallery" aria-expanded="false" aria-controls="gallery">
                    <h2>Gallery</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="gallery">
                    <div class="row mt-15">
                        <div class="col-md-12">
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                        <div class="image-editor">
                                            <div class="img-box">
                                                <div class="img-container">
                                                    <a href="#" target="_blank">
                                                        <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="btn-container">
                                                        <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                        <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#contact" aria-expanded="false" aria-controls="contact">
                    <h2>Contact Page</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="contact">
                    <div class="row mt-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" required="" class="form-control" name="customer_email" placeholder="Contact email" value="{{user()->customer_email}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Alternate Phone</label>
                                <input type="text" class="form-control" name="customer_phone" placeholder="Contact number" value="{{user()->customer_phone}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#websiteImages" aria-expanded="false" aria-controls="websiteImages">
                    <h2>Website Images</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="websiteImages">
                    <div class="row mt-15">
                        <div class="col-md-6">
                            <label for="">Banner Image</label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                        <div class="image-editor">
                                            <div class="img-box">
                                                <div class="img-container">
                                                    <a href="#" target="_blank">
                                                        <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="btn-container">
                                                        <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                        <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">About Us Image</label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                        <div class="image-editor">
                                            <div class="img-box">
                                                <div class="img-container">
                                                    <a href="#" target="_blank">
                                                        <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="btn-container">
                                                        <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                        <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Featured Food</label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                        <div class="image-editor">
                                            <div class="img-box">
                                                <div class="img-container">
                                                    <a href="#" target="_blank">
                                                        <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="btn-container">
                                                        <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                        <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Business Hours</label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                        <div class="image-editor">
                                            <div class="img-box">
                                                <div class="img-container">
                                                    <a href="#" target="_blank">
                                                        <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="btn-container">
                                                        <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                        <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Gallery</label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                        <div class="image-editor">
                                            <div class="img-box">
                                                <div class="img-container">
                                                    <a href="#" target="_blank">
                                                        <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="btn-container">
                                                        <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                        <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Header Image</label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                        <div class="image-editor">
                                            <div class="img-box">
                                                <div class="img-container">
                                                    <a href="#" target="_blank">
                                                        <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="btn-container">
                                                        <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                        <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Review Image</label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="upload-wrap">
                                    <div class="upload-wraper">
                                        <div class="dropzone dropzone-previews dz-clickable">
                                            <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                        </div>
                                        <div class="image-editor">
                                            <div class="img-box">
                                                <div class="img-container">
                                                    <a href="#" target="_blank">
                                                        <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                    </a>
                                                    <div class="btn-container">
                                                        <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                        <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!!Form::close()!!}

    </div>
</div>
<script>      
    var count=0;
    <?php if (!empty(@user()->locations) && is_array(user()->locations)):  $f = last(array_keys(user()->locations));?>
    var fee_count = {!!$f!!} + 1; 
    <?php else: ?>  var fee_count = 1;
    <?php endif ?>
   
    function addLoc() {
       
        var newTextBoxDiv = $(document.createElement('div'))
            .attr("id", 'loc_div_' + fee_count);
            newTextBoxDiv.after().html(
            '<div class="locations-form-item" id="LocEdit"><div class="row"  id="loc_div_"><div class="col-md-6"><div class="form-group mb-10"><label> Address <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Address e.g. Fort Worth, TX"></i></label><input type="text" name="locations['+
            fee_count +'][address]" class="form-control" placeholder="Address"></div></div><div class="col-md-6"><div class="form-group mb-10"><label> Location <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Location Name E.g. Tiger Xpress on Beach St, Fort Worth"></i></label><input type="text" name="locations[' +fee_count +'][text]" class="form-control" placeholder="Location Name"></div></div><div class="col-md-4"><div class="form-group mb-10"><label> Email <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Email where inquiries from Customers will be sent"></i></label><input type="email" name="locations[' +fee_count +'][email]" class="form-control" placeholder="Email Address"></div></div><div class="col-md-4"><div class="form-group mb-10"><label> Phone <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title=""data-original-title="Phone Number of the Location"></i></label><input type="text" name="locations[' +fee_count +'][phone]" class="form-control" placeholder="Phone" pattern="^([0-9\s\-\+\(\)]*)$"></div></div><div class="col-md-4"><div class="form-group mb-10"><label>Order Link<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Zing Link to order from this location"></i></label><input type="url" name="locations[' +fee_count +'][url]" class="form-control" value="{{@$loc_value['url']}}" placeholder="Website URL"></div></div></div><button class="btn btn-dark btn-icon location-remove-btn" type="button" id="fee_rmvbtn_'+fee_count+'" onclick="removeLoc('+fee_count+')"><i class="fa fa-trash"></i><span>Remove</span></button></div>');

        newTextBoxDiv.appendTo("#loc_div ");
        fee_count++;
    }
   
    function removeLoc(key) {
    document.getElementById('loc_div_'+key).remove();  
    document.getElementById('fee_rmvbtn_'+key).remove();  

  }
  $("#addLoc").click(function(event){
  event.preventDefault();
  addLoc();
  var elmnt = document.getElementById("LocEdit");
  elmnt.scrollIntoView();
});

  </script>