                                                        <h3 class="d-none d-md-block">Schedule {{$edit_menu->name}}</h3>
                                                         {!!Form::vertical_open()
        ->id('restaurant-menu-edit-schedule')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->addClass('compact-edit')
        ->action(guard_url('restaurant/menu/'. $edit_menu->getRouteKey()))!!}
        {{csrf_field()}}
        
                                                            @foreach(trans('restaurant::menu.options.timing') as $key => $value) 
                                                            <div class="row align-items-center mb-3 mb-md-0">
                                                                <div class="col-5">
                                                                    <div class="custom-control custom-checkbox">
                                                                        <input type="checkbox" class="custom-control-input" id="{{$key}}" {{@$edit_menu->timing[$key]['start'] != '' && @$edit_menu->timing[$key]['end'] != '' ? 'checked':''}}>
                                                                        <label class="custom-control-label" for="{{$key}}">{{$value}}</label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-7">
                                                                    <div class="row no-gutters">
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control" name="timing[{{$key}}][start]">
                                                                                    <option value="">From</option>
                                                                                    <option value="00:00" {{@$edit_menu->timing[$key]['start'] == '00:00' ? 'selected':''}}>12:00 AM</option>
                                                                                    <option value="01:00" {{@$edit_menu->timing[$key]['start'] == '01:00' ? 'selected':''}}>01:00 AM</option>
                                                                                    <option value="02:00" {{@$edit_menu->timing[$key]['start'] == '02:00' ? 'selected':''}}>02:00 AM</option>
                                                                                    <option value="03:00" {{@$edit_menu->timing[$key]['start'] == '03:00' ? 'selected':''}}>03:00 AM</option>
                                                                                    <option value="04:00" {{@$edit_menu->timing[$key]['start'] == '04:00' ? 'selected':''}}>04:00 AM</option>
                                                                                    <option value="05:00" {{@$edit_menu->timing[$key]['start'] == '05:00' ? 'selected':''}}>05:00 AM</option>
                                                                                    <option value="06:00" {{@$edit_menu->timing[$key]['start'] == '06:00' ? 'selected':''}}>06:00 AM</option>
                                                                                    <option value="07:00" {{@$edit_menu->timing[$key]['start'] == '07:00' ? 'selected':''}}>07:00 AM</option>
                                                                                    <option value="08:00" {{@$edit_menu->timing[$key]['start'] == '08:00' ? 'selected':''}}>08:00 AM</option>
                                                                                    <option value="09:00" {{@$edit_menu->timing[$key]['start'] == '09:00' ? 'selected':''}}>09:00 AM</option>
                                                                                    <option value="10:00" {{@$edit_menu->timing[$key]['start'] == '10:00' ? 'selected':''}}>10:00 AM</option>
                                                                                    <option value="11:00" {{@$edit_menu->timing[$key]['start'] == '11:00' ? 'selected':''}}>11:00 AM</option>
                                                                                    <option value="12:00" {{@$edit_menu->timing[$key]['start'] == '12:00' ? 'selected':''}}>12:00 PM</option>
                                                                                    <option value="13:00" {{@$edit_menu->timing[$key]['start'] == '13:00' ? 'selected':''}}>01:00 PM</option>
                                                                                    <option value="14:00" {{@$edit_menu->timing[$key]['start'] == '14:00' ? 'selected':''}}>02:00 PM</option>
                                                                                    <option value="15:00" {{@$edit_menu->timing[$key]['start'] == '15:00' ? 'selected':''}}>03:00 PM</option>
                                                                                    <option value="16:00" {{@$edit_menu->timing[$key]['start'] == '16:00' ? 'selected':''}}>04:00 PM</option>
                                                                                    <option value="17:00" {{@$edit_menu->timing[$key]['start'] == '17:00' ? 'selected':''}}>05:00 PM</option>
                                                                                    <option value="18:00" {{@$edit_menu->timing[$key]['start'] == '18:00' ? 'selected':''}}>06:00 PM</option>
                                                                                    <option value="19:00" {{@$edit_menu->timing[$key]['start'] == '19:00' ? 'selected':''}}>07:00 PM</option>
                                                                                    <option value="20:00" {{@$edit_menu->timing[$key]['start'] == '20:00' ? 'selected':''}}>08:00 PM</option>
                                                                                    <option value="21:00" {{@$edit_menu->timing[$key]['start'] == '21:00' ? 'selected':''}}>09:00 PM</option>
                                                                                    <option value="22:00" {{@$edit_menu->timing[$key]['start'] == '22:00' ? 'selected':''}}>10:00 PM</option>
                                                                                    <option value="23:00" {{@$edit_menu->timing[$key]['start'] == '23:00' ? 'selected':''}}>11:00 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-6">
                                                                            <div class="form-group m-0">
                                                                                <select class="form-control" name="timing[{{$key}}][end]">
                                                                                    <option value="">To</option>
                                                                                    <option value="00:00" {{@$edit_menu->timing[$key]['end'] == '00:00' ? 'selected':''}}>12:00 AM</option>
                                                                                    <option value="01:00" {{@$edit_menu->timing[$key]['end'] == '01:00' ? 'selected':''}}>01:00 AM</option>
                                                                                    <option value="02:00" {{@$edit_menu->timing[$key]['end'] == '02:00' ? 'selected':''}}>02:00 AM</option>
                                                                                    <option value="03:00" {{@$edit_menu->timing[$key]['end'] == '03:00' ? 'selected':''}}>03:00 AM</option>
                                                                                    <option value="04:00" {{@$edit_menu->timing[$key]['end'] == '04:00' ? 'selected':''}}>04:00 AM</option>
                                                                                    <option value="05:00" {{@$edit_menu->timing[$key]['end'] == '05:00' ? 'selected':''}}>05:00 AM</option>
                                                                                    <option value="06:00" {{@$edit_menu->timing[$key]['end'] == '06:00' ? 'selected':''}}>06:00 AM</option>
                                                                                    <option value="07:00" {{@$edit_menu->timing[$key]['end'] == '07:00' ? 'selected':''}}>07:00 AM</option>
                                                                                    <option value="08:00" {{@$edit_menu->timing[$key]['end'] == '08:00' ? 'selected':''}}>08:00 AM</option>
                                                                                    <option value="09:00" {{@$edit_menu->timing[$key]['end'] == '09:00' ? 'selected':''}}>09:00 AM</option>
                                                                                    <option value="10:00" {{@$edit_menu->timing[$key]['end'] == '10:00' ? 'selected':''}}>10:00 AM</option>
                                                                                    <option value="11:00" {{@$edit_menu->timing[$key]['end'] == '11:00' ? 'selected':''}}>11:00 AM</option>
                                                                                    <option value="12:00" {{@$edit_menu->timing[$key]['end'] == '12:00' ? 'selected':''}}>12:00 PM</option>
                                                                                    <option value="13:00" {{@$edit_menu->timing[$key]['end'] == '13:00' ? 'selected':''}}>01:00 PM</option>
                                                                                    <option value="14:00" {{@$edit_menu->timing[$key]['end'] == '14:00' ? 'selected':''}}>02:00 PM</option>
                                                                                    <option value="15:00" {{@$edit_menu->timing[$key]['end'] == '15:00' ? 'selected':''}}>03:00 PM</option>
                                                                                    <option value="16:00" {{@$edit_menu->timing[$key]['end'] == '16:00' ? 'selected':''}}>04:00 PM</option>
                                                                                    <option value="17:00" {{@$edit_menu->timing[$key]['end'] == '17:00' ? 'selected':''}}>05:00 PM</option>
                                                                                    <option value="18:00" {{@$edit_menu->timing[$key]['end'] == '18:00' ? 'selected':''}}>06:00 PM</option>
                                                                                    <option value="19:00" {{@$edit_menu->timing[$key]['end'] == '19:00' ? 'selected':''}}>07:00 PM</option>
                                                                                    <option value="20:00" {{@$edit_menu->timing[$key]['end'] == '20:00' ? 'selected':''}}>08:00 PM</option>
                                                                                    <option value="21:00" {{@$edit_menu->timing[$key]['end'] == '21:00' ? 'selected':''}}>09:00 PM</option>
                                                                                    <option value="22:00" {{@$edit_menu->timing[$key]['end'] == '22:00' ? 'selected':''}}>10:00 PM</option>
                                                                                    <option value="23:00" {{@$edit_menu->timing[$key]['end'] == '23:00' ? 'selected':''}}>11:00 PM</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                            
                                                            <div class="form-group border-0"><button type="button" class="btn btn-theme" id="btn_edit_menuschedule" data-menu="{{@$edit_menu->id}}">Schedule Item</button></div>
{!!Form::close()!!}