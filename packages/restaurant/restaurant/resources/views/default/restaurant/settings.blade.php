@include('restaurant::default.restaurant.partial.header')
            <div class="app-content-wrap">
                 @include('restaurant::default.restaurant.partial.left_menu_new')
                <div class="app-content-inner">
                    <div class="app-entry-form-wrap">
                        {!!Form::vertical_open()
                        ->id('restaurant-restaurant-edit')
                        ->method('Get')
                        ->enctype('multipart/form-data')
                        ->addClass('compact-edit p-0')
                        ->action(guard_url('restaurant/settings-save/'. user()->getRouteKey()))!!}
                        <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
                            <i class="flaticon-settings app-sec-title-icon"></i>
                            <h1>Settings</h1>
                            <div class="actions">
                                <button type="submit" class="btn btn-with-icon btn-dark"><i class="fas fa-save mr-5"></i>Update</button>
                            </div>
                            <a href="index.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
                        </div>
                        <div class="app-entry-form-section">
                            @include('notifications')
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Start taking online orders</label>
                                        <div class="custom-control custom-switch custom-switch-lg">
                                            <input type="checkbox" name="onoffswitch" class="custom-control-input" id="myonoffswitch" @if($restaurant_stop==false)checked @endif>
                                            <label class="custom-control-label" for="myonoffswitch">&nbsp;</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">In-house Delivery</label>
                                        <div class="custom-control custom-switch custom-switch-lg">
                                            <input type="checkbox" class="custom-control-input" id="inhouse_delivery" name="delivery_status" {{($restaurant->delivery=='Yes'?'checked':'')}}>
                                            <label class="custom-control-label" for="inhouse_delivery">&nbsp;</label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="{{(@$restaurant->delivery=='Yes'?'Yes':'No')}}" id="delivery" name="delivery">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">No-Contact Delivery Option</label>
                                        <div class="custom-control custom-switch custom-switch-lg">
                                            <input type="checkbox" class="custom-control-input" id="nocontact_delivery" name="contact_less" {{($restaurant->non_contact=='1'?'checked':'')}}>
                                            <label class="custom-control-label" for="nocontact_delivery">&nbsp;</label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="{{(@$restaurant->non_contact=='1'?'1':'0')}}" id="non_contact" name="non_contact">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="">Car Pickup Option</label>
                                        <div class="custom-control custom-switch custom-switch-lg">
                                            <input type="checkbox" class="custom-control-input" id="pickup_car" name="pickup_car" {{($restaurant->car_pickup=='1'?'checked':'')}}>
                                            <label class="custom-control-label" for="pickup_car">&nbsp;</label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="{{(@$restaurant->car_pickup=='1'?'1':'0')}}" id="car_pickup" name="car_pickup">
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>
                </div>
            </div>

<div class="modal fade cusines-modal" id="stopKitchenModal" tabindex="-1" role="dialog" aria-labelledby="cuisine_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <form action="{{guard_url('restaurant/settings-save/'. user()->getRouteKey())}}" method="GET" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cuisine_modalLabel">How long are you not taking orders ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <b>For the rest of  today(you can resume taking orders by clicking start at any time)</b>&nbsp;&nbsp; <input type="checkbox" name="stop_date" value="{{date("Y-m-d", strtotime("+ 1 day"))}}" id="rest_of_the_day" class="pt-2">
                </div>
                <div class="form-group">
                    <!-- <label><b>OR until the date selected below:</b></label> -->
                    <h6><i>Pick the date you want to start taking orders again.</i></h6>
                   <input type="text" id="datepicker1" class="form-control" name="stop_date" placeholder="">
                        <a href="#" class="text-secondary" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<p class='m-0 text-left'>This will stop taking orders for the day.</p>"><i class="ion-android-alert"></i></a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
</div>


<script type="text/javascript">
    $(document ).ready(function() {
        $('#myonoffswitch').change(function() { 
        if(this.checked) {
           // var returnVal = confirm("Are you sure?");
            //$(this).prop("checked", returnVal);
        }else { 
            $('#stopKitchenModal').modal('show');
        }
        //$('#textbox1').val(this.checked);
    });
    });
</script>
<script type="text/javascript">
 $("#rest_of_the_day").click(function () {
      $('#datepicker1').attr("disabled", $(this).is(":checked"));
   });
    $('[data-toggle="tooltip"]').tooltip();
    $( function() {
        $("#datepicker1" ).datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 1,
        });
        $("#datepicker1").datepicker('setDate', new Date());
    });
    
    $( document ).ready(function() {
       
        $('#myonoffswitch').change(function() { 
        if(this.checked) {
           // var returnVal = confirm("Are you sure?");
            //$(this).prop("checked", returnVal);
        }else {
            $('#stopKitchenModal').modal('show');
        }
        //$('#textbox1').val(this.checked);
    });
    $('#inhouse_delivery').change(function() {
        if(this.checked) {
            $('#delivery').val('Yes');

        }else {
             $('#delivery').val('No');

        }
    });
    $('#nocontact_delivery').change(function() {
        if(this.checked) {
            $('#non_contact').val('1');

        }else {
             $('#non_contact').val('0');

        }
    });
    $('#pickup_car').change(function() {
        if(this.checked) {
            $('#car_pickup').val('1');

        }else {
             $('#car_pickup').val('0');

        }
    });
    })
</script>