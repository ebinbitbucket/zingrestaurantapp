<aside class="main-nav">
    <div class="nav-inner">
        @include('restaurant::default.restaurant.partial.mobile_menu')


    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{ theme_asset('img/logo.png') }}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-3 d-none d-lg-block">
                <aside class="dashboard-sidemenu">
                    @include('restaurant::default.restaurant.partial.left_menu')


                </aside>
            </div>
            <div class="col-md-12 col-lg-9">
                <div class="element-wrapper">
                    <div class="element-box">
                        {!! Form::vertical_open()
                        ->id('restaurant-restaurant-edit')
                        ->method('PUT')
                        ->enctype('multipart/form-data')
                        ->addClass('compact-edit p-0')
                        ->action(guard_url('restaurant/restaurant/' . user()->getRouteKey())) !!}
                        <div class="element-info">
                            <div class="element-info-with-icon">
                                <div class="element-info-icon">
                                    <div class="icon icon ion-card"></div>
                                </div>
                                <div class="element-info-text">
                                    <h5 class="element-inner-header">Account Details</h5>
                                    <div class="element-inner-desc">Enter the Banking details for where your money
                                        should be deposited.</div>
                                </div>
                            </div>
                        </div>
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <b> Success </b>
                                <div>{{ Session::get('success') }}</div>
                            </div>
                            {{ Session::forget('success') }}
                        @endif
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label for=""> Account Number*</label>
                                    <input onfocus="this.value=''" class="form-control" id="ac_no" name="ac_no"
                                        placeholder="" required="required" type="text" pattern="[0-9a-zA-Z]*">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="">Routing Number*</label>
                                    <input onfocus="this.value=''" class="form-control" id="IFSC" name="IFSC"
                                        placeholder="" required="required" type="text" pattern="[0-9a-zA-Z]*">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Bank*</label>
                                    <input class="form-control" name="bank" placeholder="" required="required"
                                        type="text" onfocus="this.value=''" value="{{ user()->bank }}">
                                </div>
                            </div>
                            <!--   <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">GST Number</label>
                                        <input class="form-control" name="GST_no" placeholder="GST Number" required="required" type="text" value="{{ user()->GST_no }}">
                                    </div>
                                </div> -->
                        </div>
                        <div class="form-buttons-w text-center">
                            <button class="btn btn-theme" type="submit" style="width: 150px;"> Update</button>
                            <!--                            <button class="btn btn-danger" type="button"> Cancel</button>
-->
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        var account = '<?php echo user()->ac_no; ?>';
        document.getElementById('ac_no').value = new Array(account.length - 3).join('x') + '-' + account.substr(
            account.length - 4, 4);
        var ifsc = '<?php echo user()->IFSC; ?>';
        document.getElementById('IFSC').value = new Array(ifsc.length - 3).join('x') + '-' + ifsc.substr(ifsc
            .length - 4, 4);
    })

</script>