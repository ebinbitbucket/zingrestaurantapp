
<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a class="active" href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside><section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">a
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                        <li >
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                        <li class="active">
                                            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                                        </li>
                                         <li>
                                            <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-android-calendar"></i><span>Eatery Schedule</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-android-home"></i><span>Kitchen</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    {!!Form::vertical_open()
        ->id('restaurant-restaurant-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('restaurant/restaurant/'. user()->getRouteKey()))!!}
                                        <div class="element-info">
                                            <div class="element-info-with-icon">
                                                <div class="element-info-icon">
                                                    <div class="icon ion-social-buffer"></div>
                                                </div>
                                                <div class="element-info-text">
                                                    <h5 class="element-inner-header">Restaurant Details</h5>
                                                    <div class="element-inner-desc"></div>
                                                </div>
                                            </div>
                                        </div>
                                        @include('notifications')
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Restaurant Name*</label>
                                                    <input class="form-control" placeholder="Restaurant Name" required="required" name="name" type="text" value="{{user()->name}}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Email*</label>
                                                    <input type="email" class="form-control" name="email" placeholder="Email" readonly required="" value="{{user()->email}}">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Phone No*</label>
                                                    <input type="tel" class="form-control" name="phone" placeholder="Phone No" required="" value="{{user()->phone}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Type of Restaurant*</label>
                                                     {!! Form::text('type')
                                                      ->id('type')
                                                       -> label('')
                                                       ->value(user()->type)
                                                       -> placeholder(trans('restaurant::restaurant.placeholder.type'))!!}
                                                </div>
                                            </div>
                                             <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Delivery Charge</label>
                                                     {!! Form::decimal('delivery_charge')
                                                      -> label('')
                                                      ->value(user()->delivery_charge)
                                                      -> placeholder('Delivery Charge')!!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Minimum Order Amount</label>
                                                     {!! Form::decimal('min_order_count')
                                                      -> label('')
                                                      ->value(user()->min_order_count)
                                                      -> placeholder('Minimum Order Amount')!!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Minimum Price range*</label>
                                                     {!! Form::text('price_range_min')
                                                      -> label('')
                                                      -> required()
                                                      ->value(user()->price_range_min)
                                                      -> placeholder(trans('restaurant::restaurant.placeholder.price_range_min'))!!}
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="">Maximum Price range*</label>
                                                     {!! Form::text('price_range_max')
                                                      -> label('')
                                                      -> required()
                                                      ->value(user()->price_range_max)
                                                      -> placeholder(trans('restaurant::restaurant.placeholder.price_range_max'))!!}
                                                </div>
                                            </div>
                                           <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Delivery*</label>
                                                      {!! Form::select('delivery')
                                                         -> options(trans('restaurant::restaurant.options.delivery'))
                                                         -> label('')
                                                         -> required()
                                                         ->value(user()->delivery)
                                                         -> placeholder(trans('restaurant::restaurant.placeholder.delivery'))!!}
                                                </div>
                                            </div>
                                                        <div class="col-md-2">
                                                <div class="form-group">
                                                    <label for="">Delivery Limit</label>
                                                    {!! Form::text ('delivery_limit')
                                                            -> label('')
                                                            ->value(user()->delivery_limit)
                                                            -> placeholder('in miles')!!}
                                                      
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                </br>
                                                      <input data-toggle="tooltip" title="Enable/Disable Sound for Order Notifications" type="checkbox" name="audio_alert" readonly {{user()->audio_alert == 'on' ? 'checked':''}} >Audio Alert
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Zipcode</label>
                                                      {!! Form::numeric('zipcode')
                                                         -> label('')
                                                         ->value(user()->zipcode)
                                                         -> placeholder(trans('restaurant::restaurant.placeholder.zipcode'))!!}
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Keywords</label>
                                                    {!! Form::text('keywords')
                                                      -> label('')
                                                      ->value(user()->keywords)
                                                      -> placeholder(trans('restaurant::restaurant.placeholder.keywords'))!!}
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Category*</label>
                                                    {!! Form::select('category_name')
                                                    -> options(Master::getMasterCategories())
                                                      -> label('')
                                                      -> required()
                                                      ->value(user()->category_name)
                                                      -> placeholder('Category')!!}
                                                </div>
                                            </div>
                                                <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Credit Card % Rate</label>
                                                   {!! Form::text('CCR')
                                                      -> label('')
                                                      ->readonly()
                                                      ->value(user()->CCR)
                                                      -> placeholder('Credit Card % Rate')!!}
                                                </div>
                                            </div>
                                               <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Credit Card Transaction Fee</label>
                                                   {!! Form::text('CCF')
                                                      -> label('')
                                                      ->readonly()
                                                      ->value(user()->CCF)
                                                      -> placeholder('Credit Card Transaction Fee')!!}
                                                </div>
                                            </div>
                                               <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Eatery % Rate Zing Fee</label>
                                                   {!! Form::text('ZR')
                                                      -> label('')
                                                      ->readonly()
                                                      ->value(user()->ZR)
                                                      -> placeholder('Eatery % Rate Zing Fee')!!}
                                                </div>
                                            </div>
                                               <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Eatery Zing Flat Fee</label>
                                                   {!! Form::text('ZF')
                                                      -> label('')
                                                      ->readonly()
                                                      ->value(user()->ZF)
                                                      -> placeholder('Eatery Zing Flat Fee')!!}
                                                </div>
                                            </div>
                                               <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Customer Zing Fee</label>
                                                   {!! Form::text('ZC')
                                                      -> label('')
                                                      ->readonly()
                                                      ->value(user()->ZC)
                                                      -> placeholder('Customer Zing Fee')!!}
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Tax Rate*</label>
                                                   {!! Form::text('tax_rate')
                                                      -> label('')
                                                      ->value(user()->tax_rate)
                                                      -> placeholder('Please enter tax rate')!!}
                                                </div>
                                            </div>
                                              <div class="col-md-4">
                                                <label for="">Facebook Link</label>
                                                {!! Form::text('social_media_links[facebook]')
                                                      -> label('')
                                                      ->value(@user()->social_media_links['facebook'])
                                                      -> placeholder('Facebook Link')!!}
                                              </div>
                                               <div class="col-md-4">
                                                <label for="">Twitter Link</label>
                                                {!! Form::text('social_media_links[twitter]')
                                                      -> label('')
                                                      ->value(@user()->social_media_links['twitter'])
                                                      -> placeholder('Twitter Link')!!}
                                              </div>
                                                 <div class="col-md-4">
                                                <label for="">Youtube Link</label>
                                                {!! Form::text('social_media_links[youtube]')
                                                      -> label('')
                                                      ->value(@user()->social_media_links['youtube'])
                                                      -> placeholder('Youtube Link')!!}
                                              </div>
                                                 <div class="col-md-4">
                                                <label for="">Linkedln Link</label>
                                                {!! Form::text('social_media_links[linkedln]')
                                                      -> label('')
                                                      ->value(@user()->social_media_links['linkedln'])
                                                      -> placeholder('Linkedln Link')!!}
                                              </div>
                                                 <div class="col-md-4">
                                                <label for="">Instagram Link</label>
                                                {!! Form::text('social_media_links[instagram]')
                                                      -> label('')
                                                      ->value(@user()->social_media_links['instagram'])
                                                      -> placeholder('Instagram Link')!!}
                                              </div>
                                                 <div class="col-md-4">
                                                <label for="">Pinterest Link</label>
                                                {!! Form::text('social_media_links[pintrest]')
                                                      -> label('')
                                                      ->value(@user()->social_media_links['pintrest'])
                                                      -> placeholder('Pinterest Link')!!}
                                              </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Restaurant Address*</label>
                                                    {!! Form::text ('address')
            ->id('textAddres')
            -> label('')
            -> required()
            ->value(user()->address)
            -> placeholder(trans('restaurant::restaurant.placeholder.address'))!!}
                                                    <!-- <textarea name="address" id="textAddres" class="form-control" value="{{user()->address}}" rows="2">{{user()->address}}</textarea> -->
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Lattitude*</label>
                                                    {!! Form::text('latitude')
                                                     -> label('')
                                                     -> required()
                                                     ->value(user()->latitude)
                                                     -> placeholder(trans('restaurant::restaurant.placeholder.latitude'))!!}
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="">Longitude*</label>
                                                    {!! Form::text('longitude')
                                                       -> label('')
                                                       -> required()
                                                       ->value(user()->longitude)
                                                       -> placeholder(trans('restaurant::restaurant.placeholder.longitude'))!!}
                                                </div>
                                            </div>
                                            <input type="hidden" id="latitude_address" >
                                                    <input type="hidden" id="longitude_address" >
                                            <div class="col-md-12">
                                                <div class="form-group border-0">
                                                    <label for="">Map View</label>
                                                    <div class="map-wrap mt-10">
                                                        <div id="map_canvas" style="height: 280px;width: 100%;"></div>
                                                   <!--      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15716.750801923079!2d76.29128701977541!3d10.00134909999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1554718143114!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                                                    </div>       
                                                </div>
                                            </div>
                                             
                                            <div class="col-md-6">
                                                <div class="form-group border-0">
                                                    <label for="">Logo*</label>
                                                    <div class="dropzone dropzone-previews mt-10">
                                                      <div class="row">
                                                        <div class='col-lg-12 col-sm-12'>
                                                            {!! user()->files('logo')
                                                            ->url(user()->getUploadUrl('logo'))
                                                            ->mime(config('filer.image_extensions'))
                                                            ->dropzone()!!}
                                                        </div>
                                                        <div class='col-lg-12 col-sm-12'>
                                                        {!! user()->files('logo')
                                                        ->editor()!!}
                                                        </div>
                                                        </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                                 <div class="col-md-6">
                                                <div class="form-group border-0">
                                                    <label for="">Offer</label>
                                                    <div class="dropzone dropzone-previews mt-10">
                                                      <div class="row">
                                                        <div class='col-lg-12 col-sm-12'>
                                                              {!! user()->files('offer')
                                                              ->url(user()->getUploadUrl('offer'))
                                                              ->mime(config('filer.image_extensions'))
                                                              ->dropzone()!!}
                                                          </div>
                                                          <div class='col-lg-12 col-sm-12'>
                                                          {!! user()->files('offer')
                                                          ->editor()!!}
                                                          </div>
                                                          </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                              
                                             <div class="col-md-6">
                                                <div class="form-group border-0">
                                                    <label for="">Gallery</label>
                                                    <div class="dropzone dropzone-previews mt-10">
                                                      <div class="row">
                                                        <div class='col-lg-12 col-sm-12'>
                        {!! user()->files('gallery')
                        ->url(user()->getUploadUrl('gallery'))
                        ->mime(config('filer.image_extensions'))
                        ->dropzone()!!}
                    </div>
                    <div class='col-lg-12 col-sm-12'>
                    {!! user()->files('gallery')
                    ->editor()!!}
                    </div>
                    </div>
                                                        </div>
                                                    </div>  
                                                </div>
                                                  <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Description</label>
                                                      {!! Form::textarea ('description')
                -> dataUpload(trans_url(user()->getUploadURL('description')))
               -> label('')
               ->value(user()->description)
               -> placeholder(trans('restaurant::restaurant.placeholder.description'))
               ->rows(6)!!}
                                                </div>
                                            </div>
        
             

                                        </div>
                                        <div class="form-buttons-w text-center">
                                          <button class="btn btn-theme" type="submit" style="height: 30px; width: 150px;"> Update</button>
                                           <!--  <button class="btn btn-danger" type="button"> Cancel</button> -->
                                        </div>
                                    {!!Form::close()!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
            <script type="text/javascript">
   var count=2;
  function addTiming(day){

     var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);

     // $("#TextBoxDiv"+count).load('{{guard_url("restaurant/timing")}}'+'/'+count+'/'+day);
     newTextBoxDiv.after().load('{{guard_url("restaurant/timing")}}'+'/'+count+'/'+day);
  // newTextBoxDiv.after().html(
  //       '<div class="row"><div class="col-md-3 col-sm-12"></div><div class="col-md-3" style="display: block";><input class="timepicker form-control" id="start" type="text" placeholder="Start Time" name='+day+'['+count+'][start]"></div><div class="col-md-3" style="display: block";><input class="timepicker form-control" id="start" type="text" placeholder="End Time" name='+day+'['+count+'][end]"></div><div class="col-md-3" style="display: block";></div></div>');

  newTextBoxDiv.appendTo("#variation_div_"+day);
    count ++;
  }

  function removeVar(day,key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=timings]").length/3;
  if(countvaraitions > 1){
       
              document.getElementById('TextBoxDiv'+key).remove();  
           
        }
       
        document.getElementById(key+'button').style.display = "none";   

  if(countvaraitions == 1){  
    document.getElementById(key).remove();  
     document.getElementById('btn_remove').style.display = "none";
     document.getElementById('variation_name').style.display = "none";
     unset(variation_list);
  }
}

    $('.timepicker').datetimepicker({

        format: 'HH:mm'

    });
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('textAddres'));
        google.maps.event.addListener(places, 'place_changed', function () {
            geocodeAddress(geocoder);
        });
    });
 function geocodeAddress(geocoder) {
        var address = document.getElementById('textAddres').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng()
            document.getElementById('latitude').value=results[0].geometry.location.lat();
            document.getElementById('longitude').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    $(function(){ 
     var map,myLatlng;
     var lat = '<?php echo user()->latitude; ?>';
     var lon = '<?php echo user()->longitude; ?>';
    if((lat !='') && (lon != '')){
         myLatlng = new google.maps.LatLng(lat,lon);
     }
     else{
         myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
     }
      
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#textAddres').val(results[0].formatted_address);
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
    });
       })
function initialize(){
    var map,myLatlng;
    var lat = '<?php echo user()->latitude; ?>';
     var lon = '<?php echo user()->longitude; ?>';
      if((document.getElementById('latitude_address').value != '') && (document.getElementById('longitude_address').value != '')){
         myLatlng = new google.maps.LatLng(document.getElementById('latitude_address').value,document.getElementById('longitude_address').value);
     }
     else if((lat!='') && (lon != '')){
         myLatlng = new google.maps.LatLng(lat,lon);
     }
     else{
         myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
     }
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#textAddres').val(results[0].formatted_address);
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
    });
}

</script>

<script type="text/javascript">
    $( document ).ready(function() {
    $('#type').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'types',
        labelField: 'types',
        searchField: 'types',
        options: [
@forelse(Master::getCuisines() as $key => $types)

    {types: "{{$types}}" },
    @empty
    @endif
],
        create: function(input) {
            return {
                types: input
            }
        }
    });
});
</script>
<style>
input[type="checkbox"][readonly] {
  pointer-events: none;
}
</style>
