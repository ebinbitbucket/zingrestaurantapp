<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<h3 class="d-none d-md-block">Edit {{$edit_menu->name}}</h3>
                                                         {!!Form::vertical_open()
        ->id('restaurant-menu-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->addClass('compact-edit')
        ->action(guard_url('restaurant/menu/'. $edit_menu->getRouteKey()))!!}
        {{csrf_field()}}
                                                            <div class="form-group">
                                                                <label for="title">Menu Name*</label>
                                                                <input type="text" name="name" class="form-control" value="{{$edit_menu->name}}" required="required">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="title">Status*</label>
                                                                <label class="switch">
                                                                
  <input type="checkbox" <?php if($edit_menu->stock_status){ echo 'checked'; }?> name="stock_status">
  <span class="slider round"></span>
</label>                                                            </div>
                                                            

<input name="is_check" value=1 type="hidden" >

                                                            <div class="form-group">
                                                                <label for="">Category*</label>
                                                                {!! Form::select('category_id')
                          ->options(Restaurant::getRestaurantCategory(user_id()))
                          ->required()
                          ->value($edit_menu->category_id)
                          -> label('')
                           -> placeholder(trans('restaurant::menu.placeholder.category_id'))!!}

                                                                <!-- <select class="form-control">
                                                                    <option disabled>Select Category</option>
                                                                    <option selected value="Sides">Sides</option>
                                                                    <option value="Wings">Wings</option>
                                                                    <option value="Salads">Salads</option>
                                                                    <option value="Wraps">Wraps</option>
                                                                    <option value="Sandwiches">Sandwiches</option>
                                                                    <option value="Pizza">Pizza</option>
                                                                </select> -->
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Master</label>
                                                                {!! Form::select('master_category')
                                                                -> options([null => 'Select a master'] + Master::getAllMasters()->toArray())
                                                                -> label('')
                                                               
                                                                !!}
                                                               </div>
                                                            <div class="form-group">
                                                                <label for="">Price*</label>
                                                                <input type="number" name="price" class="form-control" value="{{$edit_menu->price}}" required="required">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Serves</label>
                                                                <input type="text" name="serves" class="form-control" value="{{$edit_menu->serves}}" >
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Popular Dish No</label>
                                                                <input type="number" name="popular_dish_no" class="form-control" value="{{$edit_menu->popular_dish_no}}" >
                                                            </div>
                                                             <div class="form-group">
                                                                <label for="">Youtube Link</label>
                                                                <input type="text" name="youtube_link" class="form-control" value="{{$edit_menu->youtube_link}}">
                                                            </div>
                                                        <!--    <div class="form-group">-->
                                                        <!--    <label for="">Rating</label>-->
                                                        <!--    <select class="form-control" name="review" id="review">-->
                                                        <!--        <option value="">Please select rating</option>-->
                                                        <!--            <option {{$edit_menu->reveiw == 1 ? 'selected':''}} value="1">1</option>-->
                                                        <!--            <option {{$edit_menu->reveiw == 2 ? 'selected':''}} value="2">2</option>-->
                                                        <!--            <option {{$edit_menu->reveiw == 3 ? 'selected':''}} value="3">3</option>-->
                                                        <!--            <option {{$edit_menu->reveiw == 4 ? 'selected':''}} value="4">4</option>-->
                                                        <!--            <option {{$edit_menu->reveiw == 5 ? 'selected':''}} value="5">5</option>-->
                                                        <!--    </select>-->
                                                        <!--</div>-->
                                                         <div class="form-group">
                                                          <div class="custom-control custom-switch">
                                                            <input type="checkbox" class="custom-control-input" name="catering" id="catering" {{$edit_menu->catering == 'Yes' ? 'checked':''}}>
                                                            <label class="custom-control-label" for="catering">Catering</label>
                                                          </div>
                                                          </div>
                                                            <div class="form-group">
                                                                <label for="">Maxium Order Amount</label>
                                                                <input type="text" name="max_order_count" class="form-control" value="{{$edit_menu->max_order_count}}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Description</label>
                                                                <textarea name="description" class="form-control" placeholder="Description" rows="3">{{$edit_menu->description}}</textarea>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="image">Image</label>
                        {!! $edit_menu->files('image')
                        ->url($edit_menu->getUploadUrl('image'))
                        ->mime(config('filer.image_extensions'))
                        ->dropzone()!!}
                        {!! $edit_menu->files('image')
                        ->editor()!!}
                                                    </div>
                                                            <div class="form-group border-0"><button type="button" id="btn_edit_menu" class="btn btn-theme" data-menu="{{$edit_menu->id}}">Update Item</button></div>
                                                         {!!Form::close()!!}