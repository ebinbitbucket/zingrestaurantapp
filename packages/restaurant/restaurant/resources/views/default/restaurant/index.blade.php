@extends('resource.index')
@php
$links['create'] = guard_url('restaurant/restaurant/create');
$links['search'] = guard_url('restaurant/restaurant');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('restaurant::restaurant.title.main') !!}
@stop

@section('sub.title') 
{!! __('restaurant::restaurant.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('restaurant/restaurant')!!}">{!! __('restaurant::restaurant.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('restaurant::restaurant.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$restaurants->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
