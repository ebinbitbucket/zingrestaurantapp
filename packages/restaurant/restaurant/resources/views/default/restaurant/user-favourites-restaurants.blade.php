<section class="user-wrap-inner">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-3 user-aside">
                {!! Theme::partial('aside') !!}
            </div>
            <div class="col-md-9">
                <div class="user-content-wrap">
                    <div class="heading-block">
                        <h3>Orders</h3>
                    </div>
                    <div class="inner-content">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="{{guard_url('restaurant/favourites/restaurants')}}">Eateries</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="{{guard_url('restaurant/favourites/orders')}}">Orders</a>
                            </li>
                        </ul>

                        <div class="orders-wrap">
                            @forelse($restaurants as $restaurant)
                            <div class="order-item">
                                <div class="restaurant-detail">
                                    <div class="img-holder">
                                        <figure>
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">
                                                <span class="img-wrap" style="background-image: url('{{url($restaurant->defaultImage('logo'))}}')"></span>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="text-holder">
                                        <span class="raty" data-score="{{$restaurant->rating}}"></span>
                                        <h3 class="title"><a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">{{$restaurant->name}}</a></h3>
                                        @if(!empty($restaurant->type))
                                        <p class="type">{{$restaurant->type}}</p>
                                        @endif
                                        <div class="location"><i class="flaticon-pin"></i>{{$restaurant->address}}</div>
                                        <div class="meta-infos">
                                          @if($restaurant->delivery == 'Yes')
                                        @if($restaurant->delivery_charge == 0)
                                             <div class="info">    <i class="flaticon-fast-delivery"></i>Free Delivery</div>
                                        @elseif($restaurant->delivery_charge != '')
                                             <div class="info">    <i class="flaticon-fast-delivery"></i>${{number_format($restaurant->delivery_charge,2)}} / Delivery</div>
                                        @endif
                                @endif
                                        </div>
                                </div>
                            </div>
                        </div>
                            @empty
                            <div class="empty-content-wrap">
                                <img src="{{theme_asset('img/order-empty.svg')}}" alt="">
                                <h2>You do not have any Eatery set as favorite!</h2>
                                <p><a href="{{url('/')}}">Find something tasty to order</a></p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


            