<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a class="active" href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-3 d-none d-lg-block">
                <aside class="dashboard-sidemenu">
                    <nav class="sidebar-nav">
                        <ul>
                            <li class="nav-head"><span class="head">Navigation</span></li>
                            <li>
                                <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                            </li>
                            <li class="active">
                                <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                            </li>
                             <li>
                                <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-android-calendar"></i><span>Eatery Schedule</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-android-home"></i><span>Kitchen</span></a>
                            </li>
                        </ul>
                    </nav>
                </aside>
            </div>
            <div class="col-md-12 col-lg-9">
                <div class="element-wrapper">
                    <div class="element-box">
                             {!!Form::vertical_open()
->id('restaurant-restaurant-edit')
->method('PUT')
->enctype('multipart/form-data')
->addClass('compact-edit p-0')
->action(guard_url('restaurant/restaurant/'. user()->getRouteKey()))!!}
                            <div class="element-info">
                                <div class="element-info-with-icon">
                                    <div class="element-info-icon"><div class="icon icon ion-card"></div></div>
                                    <div class="element-info-text">
                                        <h5 class="element-inner-header">Account Details</h5>
                                        <div class="element-inner-desc">Enter the Banking details for where your money should be deposited.</div>
                                    </div>
                                </div>
                            </div>
                            @include('notifications')
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label for=""> Account Number*</label>
                                        <input class="form-control" id="ac_no" name="ac_no" placeholder="" required="required" type="text">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Routing Number*</label>
                                        <input class="form-control" id="IFSC" name="IFSC" placeholder="" required="required" type="text" >
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Bank*</label>
                                        <input class="form-control" name="bank" placeholder="" required="required" type="text" value="{{user()->bank}}">
                                    </div>
                                </div>
                              <!--   <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">GST Number</label>
                                        <input class="form-control" name="GST_no" placeholder="GST Number" required="required" type="text" value="{{user()->GST_no}}">
                                    </div>
                                </div> -->
                            </div>
                            <div class="form-buttons-w text-center">
                                <button class="btn btn-theme" type="submit" style="width: 150px;"> Update</button>
<!--                            <button class="btn btn-danger" type="button"> Cancel</button>
-->                         </div>
                       {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
 $( document ).ready(function() {  
    var account = '<?php echo user()->ac_no; ?>'  ;
        document.getElementById('ac_no').value = new Array(account.length-3).join('x') +'-'+ account.substr(account.length-4, 4);
        var ifsc = '<?php echo user()->IFSC; ?>'  ;
        document.getElementById('IFSC').value = new Array(ifsc.length-3).join('x') +'-'+ ifsc.substr(ifsc.length-4, 4);
 })
</script>