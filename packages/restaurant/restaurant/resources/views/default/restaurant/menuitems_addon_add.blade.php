<h3 class="d-none d-md-block">Add Addon</h3>
                                                        {!!Form::vertical_open()
                                                        ->id('restaurant-menu-edits')
                                                        ->method('PUT')
                                                        ->enctype('multipart/form-data')
                                                        ->addClass('compact-edit')
                                                        ->action(guard_url('restaurant/menu/'. $edit_addonmenu->getRouteKey()))!!}
                                                        
                                                            <div class="form-group">
                                                                <label for="">Select one Addon*</label>
                                                                 {!! Form::select('addon_ids[]')
                                                                 ->id($edit_addonmenu->id)
                                                                ->options(Restaurant::getAddonsList(user_id()))
                                                                -> label('')
                                                                ->required()
                                                                ->onchange("addon_change('$edit_addonmenu->id')")
                                                                 -> placeholder('Please select addon')!!}
                                                            </div>

                                                            <div id="addon_det_{{$edit_addonmenu->id}}" class="addon_det"></div>
                                                            <div class="form-group border-0"><button type="button" id="btn_add_menuaddon" class="btn btn-theme" data-menu="{{$edit_addonmenu->id}}">Add Addon</button></div>
                                                       {!!Form::close()!!}