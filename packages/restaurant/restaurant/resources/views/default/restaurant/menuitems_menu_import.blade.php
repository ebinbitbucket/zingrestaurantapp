<div class="menu-element-add-wrapper-header">
                                                <h5>Import Menu</h5>
                                                <button type="button" class="btn ion-android-close close-menu-element-add-wrapper" ></button>
                                            </div>
                                             {!!Form::vertical_open()
            ->id('restaurant-menu-create')
            ->method('POST')
            ->files('true')
            ->addClass('compact-edit')
            ->action(guard_url('restaurant/menu/import'))!!}
             <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group row border-0">
                                                            <div class='col-lg-8 col-sm-12'>
                            {!! $menuss->files('csv_file')
                            ->mime(config('filer.allowed_extensions'))
                            ->url($menuss->getUploadUrl('csv_file'))
                            ->dropzone()!!}
                        </div>
                        <div class='col-lg-5 col-sm-12'>
                            {!! $menuss
                            ->files('csv_file')
                            ->editor()!!}
                        </div>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-12">
                                                        <div class="form-footer mb-10">
                                                            <button type="button" id="btn_import_menu" class="btn btn-theme">Import Menu</button>
                                                        </div>
                                                    </div>
                                                </div>
                                        
                                            {!! Form::close() !!}

<script type="text/javascript">
     $('.close-menu-element-add-wrapper').on( "click", function(e) { 
        $(".menu-element-add-wrapper, .menu-category-edit-wrapper_1").hide();
        $(".menu-element-item-wrapper, .menu-element-edit-wrapper").show();
        $(".empty-msg").show();
    });
</script>