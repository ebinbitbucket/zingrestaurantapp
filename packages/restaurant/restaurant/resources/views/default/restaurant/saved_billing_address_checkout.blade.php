@if(!empty($address))
<div class="address-item">
	<input type="radio" id="address_{{$address->id}}" name="billing" onclick="address_radio('{{$address->id}}')" checked="checked" value="{{$address->id}}">
	<label for="address_{{$address->id}}">
		<h3>{{@$address->title}}</h3>
		<p>{{@$address->address}}</p>
	</label>
	<input type="hidden" name="billing_address_detail" value="{{$address->title}},{{strtok($address->address,',')}}">  
</div>
@endif