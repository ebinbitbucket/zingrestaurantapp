<link href="https://fonts.googleapis.com/css?family=Lato:400,700|Merriweather:400,700|Oswald:400,700|Overlock:400,700|Poppins:400,500,700&display=swap"
    rel="stylesheet">
<aside class="main-nav">
    <div class="nav-inner">
    @include('restaurant::default.restaurant.partial.mobile_menu')

    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>

<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-3 d-none d-lg-block">
                <aside class="dashboard-sidemenu">
                @include('restaurant::default.restaurant.partial.left_menu')
                </aside>
            </div>
            <div class="col-md-12 col-lg-9" id="appEdit">
                <div class="element-wrapper">
                    <div class="element-box">
                        <div class="element-info">
                            <div class="element-info-with-icon">
                                <div class="element-info-icon">
                                    <div class="icon ion-social-buffer"></div>
                                </div>
                                <div class="element-info-text">
                                    <h5 class="element-inner-header">Restaurant Details</h5>
                                    <div class="element-inner-desc"></div>
                                </div>
                                @if(user()->website_status=='Published')
                                <div class="element-info-text"><a href="{{url('app-view/'.user()->id)}}"
                                        class="btn btn-theme">View App</a>
                                        {{-- <button type="submit"
                                        name="website_status" value="Unpublished"
                                        class="btn btn-success ml-20">Unpublish</button> --}}
{{--                                     
                                        <a href="{{url('refresh/'.user()->id)}}"
                                            class="btn btn-success">Refresh website</a> --}}
                                        </div>

                                       
                                @endif
                                @if(user()->website_status=='Unpublished')
                                <div class="element-info-text "><button type="submit" name="website_status"
                                        value="Published" class="btn btn-theme">Publish Website</button></div>
                                @endif

                            </div>
                        </div>
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true" style="color:#40b559;"><b>Push Notifications</b></a>
                              <a style="color: #29b050;" class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false"><b>Setup</b></a>
                              {{-- <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false"></a> --}}
                            </div>
                          </nav>
                          <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                               <br>
                               <ul class="nav nav-tabs">
                                <li><a class="btn btn-outline-primary btn-sm r-5 active" data-toggle="tab" href="#menu2">Create</a>&nbsp;&nbsp;&nbsp;</li>
                                <li><a class="btn btn-outline-primary btn-sm r-5" data-toggle="tab" href="#home">Scheduled&nbsp;&nbsp;&nbsp;</a></li>
                                <li class="active"><a class="btn btn-outline-primary btn-sm r-5" data-toggle="tab" href="#menu1">Past&nbsp;&nbsp;&nbsp;</a></li>
                                
                            </ul>
                              <div class="tab-content">
                                <div id="home" class="tab-pane">
                                    <div id="editPushDiv"> </div>
                                    <br>
                                    @if(count($new_notifications)!=0)
                                    <div class="table-responsive element-table">
                                        <table id="example" class="display table table-responsive-xl table-striped table-bordered table-sm">
                                            <thead>
                                               
                                                <tr class="text-center">
                                                    <th scope="col">#</th>
                                                    <th scope="col">Push Notification Name</th>
                                                    <th scope="col">Push Notification Message</th>
                                                    <th scope="col">Schedule Time</th>
                                                    <th scope="col">Created At</th>
                                                    <th scope="col">Updated At</th>
                                                    <th scope="col">Edit</th>
                                                    <th scope="col">Delete</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach(@$new_notifications as $loc_key => $loc_value)
                                                <tr>
                                                    @php
                                                      $loc_key =$loc_key+1; 
                                                    @endphp
                                                    <td>{{ @$loc_key }}</td>
                                                    <td>{{ @$loc_value->title }}</td>
                                                    <td class="text-wrap">{{ @$loc_value->content }}</td>
                                                    <td>{{ date('Y-m-d H:i:A', strtotime($loc_value->schedule_date)) }}</td>
                                                    <td>{{ date('Y-m-d H:i:A', strtotime($loc_value->created_at)) }}</td>
                                                    <td>{{ date('Y-m-d H:i:A', strtotime($loc_value->updated_at)) }}</td>

                                                    {{-- <td><span class="badge badge-xs badge-dark">Cencelled</span></td> --}}
                                                    {{-- {{guard_url('restaurant/edit-push/'.@$loc_value->id)}} --}}
                                                    {{-- <td><span><a id="editButton"><i class="fa fa-edit"></i> </a></span> </td> --}}
                                                    <td><span><button onclick="editPush(this.value)" value="{{ @$loc_value->id }}" class="btn"><i class="fa fa-edit"></i></button></span> </td>
                                                    <td><span><button onclick="deletePush(this.value)" value="{{ @$loc_value->id }}" class="btn"><i class="fa fa-trash"></i></button></span> </td>
                                                </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    @else
                                    <h6>No Results</h6>
                                    @endif
                                </div>
                                <div id="menu1" class="tab-pane fade">
                                    <br>
                                    @if(count($past_notifications)!=0)
                                    <div class="table-responsive element-table">
                                    <br>
                                        <table id="example" class="display table table-responsive-xl table-striped table-bordered table-sm">
                                            <thead>
                                                <tr class="text-center">
                                                    <th scope="col">#</th>
                                                    <th scope="col">Push Notification Name</th>
                                                    <th scope="col">Push Notification Message</th>
                                                    <th scope="col">Schedule Time</th>
                                                    <th scope="col">Created At</th>
                                                    <th scope="col">Updated At</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach(@$past_notifications as $loc_key => $loc_value)
                                                <tr>
                                                    @php
                                                    $loc_key =$loc_key+1; 
                                                  @endphp
                                                    <td>{{ @$loc_key }}</td>
                                                    <td>{{ @$loc_value->title }}</td>
                                                    <td class="text-wrap">{{ @$loc_value->content }}</td>
                                                    <td>{{ date('Y-m-d H:i:A', strtotime($loc_value->schedule_date)) }}</td>
                                                    <td>{{ date('Y-m-d H:i:A', strtotime($loc_value->created_at)) }}</td>
                                                    <td>{{ date('Y-m-d H:i:A', strtotime($loc_value->updated_at)) }}</td>

                                                    {{-- <td><span class="badge badge-xs badge-dark">Cencelled</span></td> --}}
                                                </tr>
                                                @endforeach
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                    @else
                                    <h6>No Results</h6>
                                    @endif
                                </div>
                                <div id="menu2" class="tab-pane container active">
                                    <br>
                                    {!!Form::vertical_open()
                                        ->id('restaurant-restaurant-edit')
                                        ->method('POST')
                                        ->enctype('multipart/form-data')
                                        ->action(guard_url('restaurant/addpush/'. user()->getRouteKey()))!!}
                                        @include('notifications')
                                        <div class="row">
                                        <div class="col-md-12">
                                            <div class="repeater">
                                                <div class="position-relative" data-repeater-item>
                                                <input type="text" class="d-none" name="restaurant_id" class="form-control" value="{{ user()->getRouteKey() }}" placeholder="id">
                                                <input type="text" class="d-none" name="push_not_id" class="form-control" value="" placeholder="push_not_id">   
                                                <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Name <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Name of the message for your records"></i></label>
                                                                <input type="text" id="push_not_name" class="form-control" name="push_not_name" placeholder="Push Notification Name">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Schedule Date</label>
                                                                <input type="datetime-local" id="push_not_date" name="schedule_date" class="form-control" value="{{ date('Y-m-d\TH:i') }}" placeholder="Schedule Date">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Message</label>
                                                                <textarea class="form-control" name="push_not_content" rows="3" placeholder="Push Notification Message"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                        <div class="form-group text-left">
                                                            <button class="btn btn-theme" type="submit">
                                                                Save</button>
                                                        </div>
                                                    </div>

                                                        {!!Form::close()!!}
                                                       <hr>
                                                    </div>
                                                    <div id="not_div"></div>
                                                </div>
                                            </div>
                                        </div>
                
                                    </div>
                                   
                                </div>
                              </div>
                        

                            </div>
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                <br>
                                <br>
                                <div class="menu-category-item-wrap">
                                    <div class="accordion">
                                        {!!Form::vertical_open()
                                            ->id('restaurant-restaurant-edit')
                                            ->method('POST')
                                            ->enctype('multipart/form-data')
                                            ->action(guard_url('restaurant/updateapp/'. user()->getRouteKey()))!!}
                                            
                                            @include('notifications')
                                            
                                        <div class="card">
                                            <div class="card-header" id="cat_Sides" style="cursor:pointer;" data-toggle="collapse" data-target="#Sides" aria-expanded="true" aria-controls="Sides">
                                                {{-- <button class="btn" type="button" >Basic</button> --}}
                                                {{-- <h5 class="font-700"></h5> --}}
                                                <h6 class="element-inner-header">Basic</h6>
                                                <i class="fa fa-angle-down float-right"></i>
                                            </div>
                                            <div id="Sides" class="collapse show" aria-labelledby="cat_Sides">
                                                <div class="card-body">
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Meta Title* - <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Helps Search Engines like Google undertand your content. e.g. Black Cats Pizza place in Irving,TX"></i></label>
                                                            <input class="form-control" placeholder="Title" required="required" name="title"
                                                                type="text" value="{{@$app->title}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Meta Subtitle  
                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Helps Search Engines like Google undertand your content"></i>
                                                            </label>
                                                            <input type="text" class="form-control" name="subtitle" placeholder="Subtitle"
                                                                value="{{@$app->subtitle}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Restaurant Email  
                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Email where inquiries from Customers will be sent"></i>
                                                            </label>
                                                            <input type="text" required class="form-control" name="customer_email"
                                                                placeholder="Contact email" value="{{@$app->email}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Restaurant Phone  
                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Phone number where customers calls will be directed to. e.g. 817-857-8482"></i>
                                                            </label>
                                                            <input type="text" class="form-control" name="customer_phone" placeholder="Contact number" value="{{@$app->alternate_phone}}" pattern="^([0-9\s\-\+\(\)]*)$">
                                                        </div>
                                                    </div>
                                                </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header" id="cat_Wings" style="cursor: pointer;" data-toggle="collapse" data-target="#Wings" aria-expanded="false" aria-controls="Wings">
                                                <h6 class="element-inner-header">Contact & Locations</h6>
                                                <i class="fa fa-angle-down float-right"></i>
                                            </div>
                                            <div id="Wings" class="collapse" aria-labelledby="cat_Wings">
                                                <div class="card-body">
                                                    <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <label for="" class="d-block font-700"><b>Locations</b>
                                                                    </label>
                                                                    <button id="addLoc" class="btn btn-theme" type="button">
                                                                        Add</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="repeater">
                                                            <div class="position-relative" data-repeater-item>
                                                                @if(is_array(@$app->locations))
                                                                @foreach(@$app->locations as $loc_key => $loc_value) 
                                                                <br>
                                                                <div class="row border" id="loc_div_{{$loc_key}}">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            {{-- @if($loc_key == 0) --}}
                                                                            <label> Address <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Address e.g. Fort Worth, TX"></i></label>
                                                                            {{-- @endif --}}
                                                                            <input type="text" id="fee_address_{{$loc_key}}"
                                                                                name="locations[{{$loc_key}}][address]" class="form-control"
                                                                                value="{{@$loc_value['address']}}" placeholder="Address">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            {{-- @if($loc_key == 0) --}}
                                                                            <label> Location <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Location Name E.g. Tiger Xpress on Beach St, Fort Worth"></i>
                                                                            </label>
                                                                            {{-- @endif --}}
                                                                            <input type="text" id="fee_title_{{$loc_key}}"
                                                                                name="locations[{{$loc_key}}][text]" class="form-control"
                                                                                value="{{@$loc_value['text']}}" placeholder="Location Name">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            {{-- @if($loc_key == 0) --}}
                                                                            <label> Email <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Email where inquiries from Customers will be sent"></i>
                                                                            </label>
                                                                            {{-- @endif --}}
                                                                            <input type="email" id="fee_email_{{$loc_key}}"
                                                                                name="locations[{{$loc_key}}][email]" class="form-control"
                                                                                value="{{@$loc_value['email']}}" placeholder="Email Address">
                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            {{-- @if($loc_key == 0) --}}
                                                                            <label> Phone <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Phone Number of the Location"></i>
                                                                            </label>
                                                                            {{-- @endif --}}
                                                                            <input type="text" id="fee_phone_{{$loc_key}}"
                                                                                name="locations[{{$loc_key}}][phone]" class="form-control"
                                                                                value="{{@$loc_value['phone']}}" placeholder="Phone" pattern="^([0-9\s\-\+\(\)]*)$">
                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            {{-- @if($loc_key == 0) --}}
                                                                            <label>Order Link
                                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Zing Link to order from this location"></i>
                                                                            </label>
                                                                            {{-- @endif --}}
                                                                            <input type="url" id="fee_amt_{{$loc_key}}"
                                                                                name="locations[{{$loc_key}}][url]" class="form-control"
                                                                                value="{{@$loc_value['url']}}" placeholder="Website URL">
                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <div class="form-group">
                                                                            {{-- <label for="" class="d-block"> </label> --}}
                                                                            <button class="btn btn-theme form-control" type="button" id="fee_rmvbtn_{{$loc_key}}" onclick="removeLoc({{$loc_key}})" ><i class="fa fa-trash"></i></button>
                            
                                                                        </div>
                                                                    </div>
                                                                </div>
                            
                                                                @endforeach
                                                                @endif
                                                                <div id="loc_div"></div>
                            
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                   <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Contact Information
                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Text to display on Contact Form Section"></i>
                                                            </label>
                                                            <textarea class="form-control" name="contact_info" placeholder="Contact information text"
                                                                rows="2">{{@$app->contact_info}}</textarea>
                                                        </div>
                                                    </div>

                                                </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header"  id="cat_Salads" style="cursor: pointer;" data-toggle="collapse" data-target="#Salads" aria-expanded="false" aria-controls="Salads">
                                                <h6 class="element-inner-header">Images</h6>
                                                <i class="fa fa-angle-down float-right"></i>
                                            </div>
                                            <div id="Salads" class="collapse" aria-labelledby="cat_Salads">
                                                <div class="card-body">
                                                    <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group border-0">
                                                            <label for="">Featured Items
                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Images for App Featured Items Section; (Size - 508 px x 320 px)"></i>
                                                            </label>
                                                            <div class="dropzone dropzone-previews mt-10">
                                                                <div class="row">
                                                                    <div class='col-lg-12 col-sm-12'>
                                                                        {!! $app->files('featured_food')
                                                                        ->url($app->getUploadUrl('featured_food'))
                                                                        ->mime(config('filer.image_extensions'))
                                                                        ->dropzone()!!}
                                                                    </div>
                                                                    <div class='col-lg-12 col-sm-12'>
                                                                          {!! $app->files('featured_food')
                                                                        ->editor()!!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                            
                                                      <div class="col-md-6">
                                                        <div class="form-group border-0">
                                                            <label for="">Slider Images
                                                                <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Images for App Slider Section; (Size - 1620 px x 1080 px)"></i>
                                                            </label>
                                                            <div class="dropzone dropzone-previews mt-10">
                                                                <div class="row">
                                                                    <div class='col-lg-12 col-sm-12'>
                                                                        {!! $app->files('slider_images')
                                                                        ->url($app->getUploadUrl('gallery'))
                                                                        ->mime(config('filer.image_extensions'))
                                                                        ->dropzone()!!}
                                                                    </div>
                                                                    <div class='col-lg-12 col-sm-12'>
                                                                        {!! $app->files('slider_images')
                                                                        ->editor()!!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                            
                                                  
                                                  
                                                    <div class="col-md-12">
                                                        <div class="form-group border-0">
                                                            <label for="">Gallery
                                                             <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Images for App Gallery Section; (Size - 320 px x 320 px)"></i>
                                                            </label>
                                                            <div class="dropzone dropzone-previews mt-10">
                                                                <div class="row">
                                                                    <div class='col-lg-12 col-sm-12'>
                                                                        {!! $app->files('gallery')
                                                                        ->url($app->getUploadUrl('gallery'))
                                                                        ->mime(config('filer.image_extensions'))
                                                                        ->dropzone()!!}
                                                                    </div>
                                                                    <div class='col-lg-12 col-sm-12'>
                                                                        {!! $app->files('gallery')
                                                                        ->editor()!!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                


                                                </div>
                                            </div>
                                        </div>


                                        <div class="card">
                                            <div class="card-header" id="cat_Wings2" style="cursor: pointer;" data-toggle="collapse" data-target="#Wings2" aria-expanded="false" aria-controls="Wings">
                                                <h6 class="element-inner-header">Loyalty Points</h6>
                                                <i class="fa fa-angle-down float-right"></i>
                                            </div>
                                            <div id="Wings2" class="collapse" aria-labelledby="cat_Wings2">
                                                <div class="card-body">
                                                    <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label><b>Point Conversion ( % ) </b> <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Restaurant inputs % of Customer spent amount for Cashback. E.g 5% => 100 points;For Every $1 customer spends they get 100 points.
                                                                        "></i></label>
                                                                        <label id="points">
                                                                            @if($app->points != null)
                                                                            <h6 style="color:#40b659;">For Every $100 customer spends they get <b> {{ $app->points }}</b> points or <b>${{ $app->percentage }}</b> which can be used towards a future purchase.</h6>
                                                                            {{-- <h6 style="color:#0c0c0c;">For Every $1 customer spends they get <b>{{ $app->points }}</b> points</h6> --}}
                                                                            @endif
                                                                        </label>
                                                                    <input id="point_percentage" type="number" step="0.1" min="0" max="100" name="point_percentage" class="form-control" value="{{ $app->point_percentage }}" placeholder="Point Conversion to Cash in %">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                </div>
                                            </div>
                                        </div>


                                    {{-- </div> --}}
                                    <div class="form-buttons-w text-center position-sticky">
                                        <button class="btn btn-theme" type="submit" style="height: 30px; width: 150px;">
                                            Update</button>
                                    </div>
                                    {!!Form::close()!!}
                                        
                                    </div>
                                </div>


                            </div>


                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">...</div>
                          </div>
                       
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

<style>
.customizer {

    font-family: 'Poppins', sans-serif;
}

.customizer .customizer-toggle {
    position: absolute;
    top: 35%;
    width: 54px;
    height: 50px;
    left: -54px;
    text-align: center;
    line-height: 46px;
    cursor: pointer;
    border-radius: 15px 0 0 15px;
    color: #fff !important;
    font-size: 26px;
    display: block;
    -webkit-box-shadow: -3px 0 8px rgba(0, 0, 0, 0.1);
    box-shadow: -3px 0 8px rgba(0, 0, 0, 0.1);
}

.customizer .bg-color {
    padding: 10px;
}

.customizer .bg-color span {
    width: 20px;
    height: 20px;
    border-radius: 50%;
    display: inline-block;
    margin: 5px;
    position: relative;
    text-align: center;
    line-height: 20px;
    cursor: pointer;
}

.customizer .bg-color span::before {
    content: "\f00c";
    font-family: "Font Awesome 5 Free";
    color: #fff;
    font-size: 12px;
    font-weight: 900;
    opacity: 0;
    -o-transition: 0.3s ease-in-out;
    -webkit-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}

.customizer .bg-color span.selected::before {
    transform: scale(1);
    opacity: 1;
}

.customizer p {
    font-size: 14px;
    margin: 0;
}

.customizer .font {
    padding: 10px;
}

.customizer .font span {
    display: block;
    padding: 3px 0;
    cursor: pointer;
}

.customizer .font span::before {
    content: "\f00c";
    font-family: "Font Awesome 5 Free";
    color: #333;
    font-size: 12px;
    font-weight: 900;
    opacity: 0;
    margin-right: 5px;
    -o-transition: 0.3s ease-in-out;
    -webkit-transition: 0.3s ease-in-out;
    transition: 0.3s ease-in-out;
}

.customizer .font span.selected::before {
    transform: scale(1);
    opacity: 1;
}
.note-editor.note-frame .note-editing-area .note-editable {
    padding: 10px;
    overflow: auto;
    color: #000;
    word-wrap: break-word;
    background-color: #fff;
}
textarea.form-control {
    height: auto;
}


.btn-outline-primary:not(:disabled):not(.disabled):active, .btn-outline-primary:not(:disabled):not(.disabled).active, .show > .btn-outline-primary.dropdown-toggle {
    color: #fff;
    background-color: #29b050;
    border-color: #29b050;
}
.btn-outline-primary {
    color: #29b050;
    border-color: #29b050;
}
.btn-outline-primary:hover {
  background-color: #29b050;
}
.btn-outline-primary:not(:disabled):not(.disabled):active, .btn-outline-primary:not(:disabled):not(.disabled).active, .show > .btn-outline-primary.dropdown-toggle {
    color: #fff;
    background-color: #29b050;
    border-color: #29b050;
}

.btn-outline-primary:not(:disabled):not(.disabled):active:focus, .btn-outline-primary:not(:disabled):not(.disabled).active:focus, .show > .btn-outline-primary.dropdown-toggle:focus {
    -webkit-box-shadow: 0 0 0 0.2rem #29b050;
    box-shadow: 0 0 0 0.2rem #29b050;
}

</style>
<script>      
    var count=0;
    <?php if (!empty(@user()->locations) && is_array(user()->locations)):  $f = last(array_keys(user()->locations));?>
    var fee_count = {!!$f!!} + 1; 
    <?php else: ?>  var fee_count = 1;
    <?php endif ?>
   
    function addLoc() {
       
        var newTextBoxDiv = $(document.createElement('div'))
            .attr("id", 'loc_div_' + fee_count);
        newTextBoxDiv.after().html(
            '<div data-repeater-list="group-a" id="LocEdit"><div class="position-relative" data-repeater-item><br><div class="row border"><div class="col-md-6"><div class="form-group"><label> Address </label><input type="text" name="locations[' +
            fee_count +'][address]" class="form-control" placeholder="Address"></div></div><div class="col-md-6"><div class="form-group"><label> Location </label><input type="text" name="locations[' +
            fee_count +'][text]" class="form-control" placeholder="Location Name"></div></div><div class="col-md-4"><div class="form-group"><label> Email </label><input type="email" name="locations[' +fee_count +'][email]" class="form-control" placeholder="Email Address"></div></div><div class="col-md-3"><div class="form-group"><label> Phone </label><input type="text" name="locations[' +fee_count +'][phone]" class="form-control" placeholder="Phone" pattern="^([0-9\s\-\+\(\)]*)$"></div></div><div class="col-md-4"><div class="form-group"><label> Order Link </label><input type="url" name="locations[' +
            fee_count +'][url]" class="form-control" placeholder="Order Link"></div></div><div class="col-md-1"><button  data-repeater-delete class="btn btn-theme form-control" type="button" onclick="removeLoc(' +
            fee_count + ')" ><i class="fa fa-trash"></i></button></div></div></div></div> ');


        newTextBoxDiv.appendTo("#loc_div ");
        fee_count++;
    }
   
    function removeLoc(key) {
    document.getElementById('loc_div_'+key).remove();  
  }

  $(function () {
  $('[data-toggle="tooltip"]').tooltip();
})

$("#addLoc").click(function(event){
  event.preventDefault();
  addLoc();
  var elmnt = document.getElementById("LocEdit");
  elmnt.scrollIntoView();
});



function editPush(push_not_id){
    alert(push_not_id);
    $.ajax({
          type: 'GET',
          url : 'edit-push/'+push_not_id,
          success: function(data) { 
            $('#editPushDiv').empty();
            $('#editPushDiv').html(data);
            $('[data-toggle="tooltip"]').tooltip();

            // window.location.hash = '#appEdit';
            var elmnt = document.getElementById("appEdit");
            elmnt.scrollIntoView();
          },
          error: function(msg) { 
          
          }
    });
}
function removeEditPush(){
    $('#editPushDiv').empty();
}

function deletePush(push_not_id){
    swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => { 
                if (willDelete) {
                    $.ajax({
                        url : "delete-push" + '/' + push_not_id,
                        type: 'GET',
                        success: function(data){ 
                          toastr.info('This feature will be enabled soon.', 'Coming soon');

                            swal({
                                title: 'Success',
                                text : 'Deleted Successfully!',
                                type : 'success',
                                timer : '1000'
                            })
                            window.location = "{{guard_url('restaurant/appdetails')}}";
                        },
                        error : function(msg){
                            swal({
                                title: 'Opps...',
                                text : msg,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                return;
                }
            });

}
$(document).ready(function(){
  $("#point_percentage").keyup(function(){
    percentage = this.value;
    points = (percentage/0.5)*10;
    $("#points").empty();
    $("#points").append(`<h6 style="color:#40b659;">For Every $100 customer spends they get <b> ${points}</b> points or <b>$${percentage}</b> which can be used towards a future purchase.</h6>`);
  });
});
</script>
