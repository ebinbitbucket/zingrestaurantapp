<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<div class="menu-category-edit-wraper-header d-none d-md-flex">
                                                <h5>Edit {{$edit_cats->name}}</h5>
                                                <button type="button" class="btn ion-android-close" id="catedit"></button>
                                            </div>
                                            {!!Form::vertical_open()
        ->id('restaurant-category-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->addClass('compact-edit')
        ->action(guard_url('restaurant/category/'. $edit_cats->getRouteKey()))!!}
                                           
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Category Name*</label>
                                                            <input class="form-control" name="name" placeholder="Category Name" required="required" type="text" value="{{$edit_cats->name}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                    <div class="form-group">
                                                            <label for="">Preparation Time*</label>
                                                            <input class="form-control" name="preparation_time" placeholder="Preparation Time" required="required" type="text" value="{{$edit_cats->preparation_time}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                    <div class="form-group">
                                                                <label for="title">Status*</label>
                                                                <label class="switch">

  <input type="checkbox" <?php if($edit_cats->stock_status){ echo 'checked'; }?> name="stock_status">
  <span class="slider round"></span>
</label>                                                            </div>
                                                            
                                                    </div>
                                                    <input name="is_check" value=1 type="hidden" >

                                                     <div class="col-md-12">
                                                    <div class="form-group">
                                                            <label for="">Order No</label>
                                                            <input class="form-control" name="order_no" placeholder="Order No"  type="text" value="{{$edit_cats->order_no}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-footer mb-10">
                                                            <button type="button" id="btn_edit_category" data-category="{{$edit_cats->id}}" class="btn btn-theme">Update Category</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
<script type="text/javascript">
      $('#catedit').on( "click", function(e) { 
       $(".menu-category-edit-wrapper_1").hide();
       $(".menu-element-item-wrapper, .menu-element-edit-wrapper").show();
    $('.menu-element-item-wrapper .element-box .empty-msg').show();
    }); 

</script>                                            