<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a class="active" href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
            <!--<a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i>Monthly Statement</a>-->
            
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                        <li>
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                                        </li>
                                        <li class="active">
                                            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                                        </li>
                                         <li>
                                            <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-android-calendar"></i><span>Eatery Schedule</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-android-home"></i><span>Kitchen</span></a>
                                        </li>
                                        <!--<li>-->
                                        <!--    <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i><span>Monthly Statement</span></a>-->
                                        <!--</li>-->
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="element-wrapper menu-element-wrapper">
                                        <div class="element-box p-0">
                                            <div class="element-info">
                                                <div class="element-info-with-icon">
                                                    <div class="element-info-text">
                                                        <h5 class="element-inner-header">Menus</h5>
                                                    </div>
                                                    <div class="element-info-buttons element-add-buttons">
                                                        <div class="btn-group">
                                                            <button type="button" class="btn btn-theme ion-android-add dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                            <div class="dropdown-menu dropdown-menu-right">
                                                                <a class="dropdown-item" href="#" data-related="add_Category" onclick="addcategory()">Add Category</a>
                                                                <a class="dropdown-item" href="#" id="btn_main_add_menu" onclick="addmenu()">Add Menu</a>
                                                                 <a class="dropdown-item" href="#" id="btn_main_import_menu" onclick="importmenu()">Import Menus</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="menu-category-item-wrap">
                                                <div class="accordion" id="Menu_Accordion">
                                                    @foreach($categories as $category)
                                                    <div class="card">
                                                        <div class="card-header" id="cat_{{$category->id}}">
                                                            <button class="btn" type="button" data-toggle="collapse" data-target="#cat{{$category->id}}" aria-expanded="true" aria-controls="{{$category->id}}">{{$category->name}}</button>
                                                            <div class="actions">
                                                                <a href="#" data-toggle="tooltip" title="Schedule Offer!" class="edit-btn" onclick="updatecategoryoffer('{{$category->id}}')"><i class="flaticon-timer"></i></a>
                                                                <a href="#" class="edit-btn" onclick="updatecount('{{$category->id}}')"><i class="flaticon-edit"></i></a>
                                                                <form id="delete-form_{!! $category->getRouteKey() !!}" style="display: inline-block;" method="POST" action="{!! guard_url('restaurant/category') !!}/{!! $category->getRouteKey() !!}">
                                              {{csrf_field()}}{{method_field('DELETE')}}
                                             <button type="button" class="btn_delete_category" data-key="{!! $category->getRouteKey() !!}" onclick="deletecategory('{!! $category->getRouteKey() !!}')" style="border: none; background: none; padding: 0;"><i class="flaticon-garbage"></i></button></form>
                                             <!-- 
                                                                <a data-action="DELETE" href="{{ guard_url('restaurant/category') }}/{{$category->getRouteKey()}}"><i class="flaticon-garbage"></i></a> -->
                                                            </div>
                                                        </div>
                                                        <div id="cat{{$category->id}}" class="collapse" aria-labelledby="cat_{{$category->id}}" data-parent="#Menu_Accordion">
                                                            <div class="card-body">
                                                                @forelse($category->menu as $menu_item)
                                                                <div class="menu-item">
                                                                    <a href="#" class="menu-item-block" onclick="show_detailmenu('{{$menu_item->id}}')">
                                                                        <div class="cell-img" style="background-image: url({{url($menu_item->defaultImage('image'))}})"></div>{{$menu_item->name}}
                                                                    </a>
                                                                </div>
                                                                @empty
                                                                 No Menus added.
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                   @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8 add-edit-wrap">
                                    <div class="row position-relative">
                                        <div class="col-md-12 menu-element-add-wrapper" id="add_Category">
                                            <div class="menu-element-add-wrapper-header">
                                                <h5>Add Category</h5>
                                                <button type="button" class="btn ion-android-close close-menu-element-add-wrapper"></button>
                                            </div>
                                            {!!Form::vertical_open()
            ->id('restaurant-category-create')
            ->method('POST')
            ->files('true')
            ->addClass('compact-edit')
            ->action(guard_url('restaurant/category'))!!}
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Category Name*</label>
                                                            <input class="form-control" name="name" placeholder="" required="required" type="text">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Preparation Time*</label>
                                                            <input class="form-control" name="preparation_time" placeholder="" required="required" type="text" value="15">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-footer mb-10">
                                                            <button type="button" id="btn_add_category" class="btn btn-theme">Add Category</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="col-md-12 menu-element-add-wrapper" id="add_MenuItem">
                                            
                                        </div>
                                          <div class="col-md-12 menu-element-add-wrapper" id="import_MenuItem">
                                            
                                        </div>
                                        <div class="col-md-12 menu-category-edit-wrapper_1" id="edit_category">
                                            
                                        </div>
                                        <div class="col-md-6">
                                            <div class="element-wrapper menu-element-item-wrapper">
                                                <div class="element-box">  
                                                    <div class="empty-msg" id="emptymsg">
                                                        <p>Please click on menu to view it's details</p>
                                                    </div>  
                                                   
                                                    <div class="menu-element-item-detail-block" id="menuItem">
                                                        
                                                    </div>
                                                                     
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="element-wrapper menu-element-edit-wrapper">
                                                <div class="element-box p-0">  
                                                    <div class="empty-msg" id="emptymsg_edit">
                                                        <p>Please click on edit button to edit</p>
                                                    </div>  
                                                   
                                                    <div class="menu-element-item-edit-block" id="Edit_menu">
                                                    </div>
                                                    <div class="menu-element-item-edit-block" id="Edit_menu_schedule">
                                                    </div>
                                                    <div class="menu-element-item-edit-block" id="Edit_menu_offer">
                                                    </div>
                                                    <div class="menu-element-item-edit-block" id="add_Addon">
                                                    </div>

                                                    <div class="menu-element-item-edit-block" id="Edit_addon">
                                                    </div>
                                                   
                                                    
                                                   
                                                    <div class="menu-element-item-edit-block" id="Edit_variation">
                                                        
                                                    </div>
                                                  

                                                  
                                                    
                                                    <div class="menu-element-item-edit-block" id="add_Variation">
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<script type="text/javascript">
      <?php if (!empty(@$addon_variations)):  $s = count($addon_variations);
    $c = $s+1 ; ?>
    var count = {!!$s!!} + 1; 
    <?php else: $c = 1; ?>  var count = 1;
  <?php endif ?>

function updatecount(count_varat){
    count = parseInt(count_varat) + 1;
    
}

  function addVariation(){  
   var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);
                
                

  newTextBoxDiv.after().html(
        '<div data-repeater-list="group-a"><div data-repeater-item class="repeat-item"><div class="row"><div class="col-sm-8"><div class="form-group"><label for="">Name*</label><input type="text" name="menu_variations['+count+'][name]" id="'+count+' value="" class="form-control" placeholder=""></div></div><div class="col-sm-4"><div class="form-group"><label for="">Price*</label><input type="number" name="menu_variations['+count+'][price]" id="'+count+' value="" class="form-control" placeholder="" ></div></div></div><div class="form-group"><label for="">Description</label><textarea name="menu_variations['+count+'][description]" value="" class="form-control"></textarea></div> <button data-repeater-delete type="button" onclick="removeVar('+count+')" class="btn btn-danger btn-icon ion-trash-a" ></button></div></div>');
            
  newTextBoxDiv.appendTo("#variation_div");
    count ++;

  }

  function addAddons(){  
   var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);
                
                

  newTextBoxDiv.after().html(
        '<div class="form-group"><select class="form-control" name="addon_ids[]" id="addon_id" onchange="addon_add()"><option value="">Please select addon*</option>@foreach(Restaurant::getAddonsList(user_id()) as $key => $value)<option value="{{$key}}">{{$value}}</option>@endforeach</select><div id="addon_det" class="addon_det"></div> </div>');
            
  newTextBoxDiv.appendTo("#addon_div");
    count ++;
document.getElementById('addon_icon').style.display="none";
  }

  function addon_add(){
        var addon_id = $("#addon_id option:selected").val();
         $("#addon_det").load('{{guard_url("restaurant/addons/rest_variations")}}'+'/'+addon_id);
         document.getElementById('addon_icon').style.display="none";

     }

     function addon_change(id){
        var addon_id = $("#"+id+" option:selected").val();
         $("#addon_det_"+id).load('{{guard_url("restaurant/addons/rest_variations")}}'+'/'+addon_id);

     }

     function enableVariation(element,addon,id,menu){ 
    if(element.checked == true){
      document.getElementById(menu+'_'+addon+'_'+id+'_price').disabled = false;
      document.getElementById(menu+'_'+addon+'_'+id+'_desc').disabled = false;
      document.getElementById(menu+'_'+addon+'_'+id+'_id').disabled = false;
    }
    else
    {
      document.getElementById(menu+'_'+addon+'_'+id+'_price').disabled = true;
      document.getElementById(menu+'_'+addon+'_'+id+'_desc').disabled = true;
      document.getElementById(menu+'_'+addon+'_'+id+'_id').disabled = true;
    }
  }
  function removeVar(key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=variations]").length/3; 
 
            document.getElementById('TextBoxDiv'+key).remove();  
         
}
</script>
<script type="text/javascript">
    $(document).on('click','#btn_add_category',function(){   
            var $f = $('#restaurant-category-create');
            if(! $f.valid()) return false;

            $.getJSON({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Category added successfully', 'sucess');
                   $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                   // document.getElementById("add_Addon").empty();
                    $("#add_Category").load(" #add_Category > *");
                   // document.getElementsByClassName("empty-msg").style.display = "block";
                   // document.getElementsByClassName("empty-msg").style.display = "none";
                   // document.getElementById("add_Addon").style.display = "block";
                   
                   // $('.empty-msg').style ='display:block';
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
           
        }); 
    function addcategory(){
        $(" .menu-category-edit-wrapper_1").hide();
      $(".menu-element-item-wrapper, .menu-element-edit-wrapper").hide();
    }
    function addmenu(){
        $(".menu-element-add-wrapper, .menu-category-edit-wrapper_1").hide();
      $(".menu-element-item-wrapper, .menu-element-edit-wrapper").hide();
    }
    function importmenu(){
        $(".menu-element-add-wrapper, .menu-category-edit-wrapper_1").hide();
      $(".menu-element-item-wrapper, .menu-element-edit-wrapper").hide();
    }
    $(document).on('click','#btn_add_menu',function(){   
            var $f = $('#restaurant-menu-create');
            if(! $f.valid()) return false;
            var price = document.getElementById("restaurant-menu-create").elements.namedItem("price").value;
           
            if(! $f.valid()) return false;
             if(price < 0){
                toastr.error('Price should be greater than 0', 'Error');
                return false;
            }
            $.getJSON({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu added successfully', 'sucess');
                   $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                   // document.getElementById("add_Addon").empty();
                    $("#add_MenuItem").load(" #add_MenuItem > *");
                   $(".menu-element-item-wrapper, .menu-element-edit-wrapper").show();
    $('.menu-element-item-wrapper .element-box .empty-msg').show();
                   // document.getElementsByClassName("empty-msg").style.display = "none";
                   // document.getElementById("add_Addon").style.display = "block";
                   
                   // $('.empty-msg').style ='display:block';
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 
     $(document).on('click','#btn_import_menu',function(){   
            var $f = $('#restaurant-menu-create');
            if(! $f.valid()) return false;
             document.getElementById('btn_import_menu').disabled = true;
            $.getJSON({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu added successfully', 'sucess');
                   $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                   // document.getElementById("add_Addon").empty();
                   $("#import_MenuItem").hide();
                   $(".menu-element-item-wrapper, .menu-element-edit-wrapper").show();
                    $('.menu-element-item-wrapper .element-box .empty-msg').show();
                   // document.getElementsByClassName("empty-msg").style.display = "none";
                   // document.getElementById("add_Addon").style.display = "block";
                   
                   // $('.empty-msg').style ='display:block';
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        });
    
    function updatecount(category_id){ 
     $(".menu-element-add-wrapper, .menu-category-edit-wrapper_1").hide();
      $(".menu-element-item-wrapper, .menu-element-edit-wrapper").hide();
    document.getElementById("emptymsg").style.display = "none"; 
    $("#edit_category").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/category/edit')}}/"+category_id, function(data) { $('#edit_category').html(data); });  
       

}

function show_detailmenu(menu_id){ 
    $(".menu-element-add-wrapper, .menu-category-edit-wrapper_1").hide();
       $(".menu-element-item-wrapper, .menu-element-edit-wrapper").show();
    $('.menu-element-item-wrapper .element-box .empty-msg').show();

    document.getElementById("emptymsg").style.display = "none"; 
    document.getElementById("Edit_menu").style.display = "none";
    document.getElementById("add_Addon").style.display = "none";
        document.getElementById("add_Variation").style.display = "none";
        document.getElementById("emptymsg_edit").style.display = "none";  
        document.getElementById("Edit_addon").style.display = "none";
        document.getElementById("Edit_variation").style.display = "none";
        document.getElementById("Edit_menu_schedule").style.display = "none";
        document.getElementById("Edit_menu_offer").style.display = "none";
    $('.empty-msg').show();
    $("#menuItem").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/menu/detailform')}}/"+menu_id, function(data) { $('#menuItem').html(data); });  
       

}

 function updatemenu(menu_id){ 
        document.getElementById("add_Addon").style.display = "none";
        document.getElementById("add_Variation").style.display = "none";
        document.getElementById("emptymsg_edit").style.display = "none";  
        document.getElementById("Edit_addon").style.display = "none";
        document.getElementById("Edit_variation").style.display = "none";
        document.getElementById("Edit_menu_schedule").style.display = "none";
        document.getElementById("Edit_menu_offer").style.display = "none";
    $("#Edit_menu").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/menu/editform')}}/"+menu_id, function(data) { $('#Edit_menu').html(data); });  
       

}
function updatemenuschedule(menu_id){ 
    document.getElementById("Edit_menu").style.display = "none";
        document.getElementById("add_Addon").style.display = "none";
        document.getElementById("add_Variation").style.display = "none";
        document.getElementById("emptymsg_edit").style.display = "none";  
        document.getElementById("Edit_addon").style.display = "none";
        document.getElementById("Edit_variation").style.display = "none";
        document.getElementById("Edit_menu_offer").style.display = "none";
    $("#Edit_menu_schedule").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/menu/editscheduleform')}}/"+menu_id, function(data) { $('#Edit_menu_schedule').html(data); });  
       

}
function updatemenuoffer(menu_id){ 
    document.getElementById("Edit_menu").style.display = "none";
        document.getElementById("add_Addon").style.display = "none";
        document.getElementById("add_Variation").style.display = "none";
        document.getElementById("emptymsg_edit").style.display = "none";  
        document.getElementById("Edit_addon").style.display = "none";
        document.getElementById("Edit_variation").style.display = "none";
        document.getElementById("Edit_menu_schedule").style.display = "none";
    $("#Edit_menu_offer").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/menu/editofferform')}}/"+menu_id, function(data) { $('#Edit_menu_offer').html(data); });  
       

}
function updatecategoryoffer(menu_id){ 
   $(".menu-element-add-wrapper, .menu-category-edit-wrapper_1").hide();
      $(".menu-element-item-wrapper, .menu-element-edit-wrapper").hide();
    document.getElementById("emptymsg").style.display = "none"; 
    $("#edit_category").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/category/editofferform')}}/"+menu_id, function(data) { $('#edit_category').html(data); });  
       

}
function add_menuAddon(menu_id){
     document.getElementById("Edit_menu").style.display = "none";
        document.getElementById("add_Variation").style.display = "none";
        document.getElementById("Edit_addon").style.display = "none";
        document.getElementById("Edit_variation").style.display = "none";
    document.getElementById("emptymsg_edit").style.display = "none";
    document.getElementById("Edit_menu_schedule").style.display = "none";
        document.getElementById("Edit_menu_offer").style.display = "none"; 
    $("#add_Addon").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/addon/addform')}}/"+menu_id, function(data) { $('#add_Addon').html(data); }); 
}
function updatemenuaddon(menu_id,addon_id){
    document.getElementById("add_Addon").style.display = "none";
        document.getElementById("add_Variation").style.display = "none";
        document.getElementById("Edit_menu").style.display = "none";
        document.getElementById("Edit_variation").style.display = "none";
    document.getElementById("emptymsg_edit").style.display = "none"; 
    document.getElementById("Edit_menu_schedule").style.display = "none";
        document.getElementById("Edit_menu_offer").style.display = "none";
    $("#Edit_addon").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/addon/editform')}}/"+menu_id+'/'+addon_id, function(data) { $('#Edit_addon').html(data); }); 
}

function add_menuVariation(menu_id){
    document.getElementById("add_Addon").style.display = "none";
        document.getElementById("Edit_menu").style.display = "none";
        document.getElementById("Edit_addon").style.display = "none";
        document.getElementById("Edit_variation").style.display = "none";
    document.getElementById("emptymsg_edit").style.display = "none"; 
    document.getElementById("Edit_menu_schedule").style.display = "none";
        document.getElementById("Edit_menu_offer").style.display = "none";
    $("#add_Variation").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/variation/addform')}}/"+menu_id, function(data) { $('#add_Variation').html(data); }); 
}
function updatemenuvariation(menu_id,key){
    document.getElementById("add_Addon").style.display = "none";
        document.getElementById("add_Variation").style.display = "none";
        document.getElementById("emptymsg_edit").style.display = "none";  
        document.getElementById("Edit_addon").style.display = "none";
        document.getElementById("Edit_menu").style.display = "none";
        document.getElementById("Edit_menu_schedule").style.display = "none";
        document.getElementById("Edit_menu_offer").style.display = "none";
    $("#Edit_variation").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/variation/editform')}}/"+menu_id+'/'+key, function(data) { $('#Edit_variation').html(data); }); 
}
 $(document).on('click','#btn_main_add_menu',function(){  
       document.getElementById("emptymsg").style.display = "none"; 
    $("#add_MenuItem").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/menu/addform')}}", function(data) { $('#add_MenuItem').html(data); });  

 }); 
  $(document).on('click','#btn_main_import_menu',function(){  
       document.getElementById("emptymsg").style.display = "none"; 
    $("#import_MenuItem").css('display','block');
    $.get("{{guard_url('restaurant/menu_items/menu/importform')}}", function(data) { $('#import_MenuItem').html(data); });  

 }); 
 
$(document).on('click','#btn_edit_category',function(){  
            var $f = $('#restaurant-category-edit');
            var category_id = $('#btn_edit_category').attr('data-category');
            if(! $f.valid()) return false;
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Category updated successfully', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                    $.get("{{guard_url('restaurant/menu_items/category/edit')}}/"+category_id, function(data) { $('#edit_category').html(data); });
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 

$(document).on('click','#btn_edit_menu',function(){ 
            var $f = $('#restaurant-menu-edit');
            var menu_id = $('#btn_edit_menu').attr('data-menu');
            var price = document.getElementById("restaurant-menu-edit").elements.namedItem("price").value;
           
            if(! $f.valid()) return false;
             if(price < 0){
                toastr.error('Price should be greater than 0', 'Error');
                return false;
            }
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu updated successfully', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                    $.get("{{guard_url('restaurant/menu_items/menu/detailform')}}/"+menu_id, function(data) { $('#menuItem').html(data); });  
                    
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 
$(document).on('click','#btn_edit_menuschedule',function(){ 
            var $f = $('#restaurant-menu-edit-schedule');
            var menu_id = $('#btn_edit_menuschedule').attr('data-menu');
           
            if(! $f.valid()) return false;
             
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu schedule updated successfully', 'sucess');
                     $.get("{{guard_url('restaurant/menu_items/menu/editscheduleform')}}/"+menu_id, function(data) { $('#Edit_menu_schedule').html(data); });  
                    
                    
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 
$(document).on('click','#btn_edit_menuoffer',function(){ 
            var $f = $('#restaurant-menu-edit-offer');
            var menu_id = $('#btn_edit_menuoffer').attr('data-menu');
           
            if(! $f.valid()) return false;
             
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu schedule updated successfully', 'sucess');
                    $.get("{{guard_url('restaurant/menu_items/menu/editofferform')}}/"+menu_id, function(data) { $('#Edit_menu_offer').html(data); });   
                    
                    
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        });
$(document).on('click','#btn_add_menuaddon',function(){ 
            var $f = $('#restaurant-menu-edits');
            var menu_id = $('#btn_add_menuaddon').attr('data-menu');
            if(! $f.valid()) return false;
            var addon_id = $("#"+menu_id+" option:selected").val();
            var min = document.getElementById("restaurant-menu-edits").elements.namedItem("av_"+addon_id+"[min]").value;
            var max = document.getElementById("restaurant-menu-edits").elements.namedItem("av_"+addon_id+"[max]").value;
            var varcount = document.querySelectorAll("[id^=variation_id_]").length; 
            if(min > max){
                toastr.error('Min value should be less than max value', 'Error');
                return false;
            }

            if(max > varcount){
                toastr.error('Max value should be less than total number of variations', 'Error');
                return false;
            }
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu Addon added successfully', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                    $.get("{{guard_url('restaurant/menu_items/menu/detailform')}}/"+menu_id, function(data) { $('#menuItem').html(data); });  
                    $.get("{{guard_url('restaurant/menu_items/menu/editform')}}/"+menu_id, function(data) { $('#Edit_menu').html(data); });  
                    $.get("{{guard_url('restaurant/menu_items/addon/addform')}}/"+menu_id, function(data) { $('#add_Addon').html(data); }); 
       
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 
$(document).on('click','#btn_menuaddon_edit',function(){ 
            var $f = $('#restaurant-menu-edit');
            var menu_id = $('#btn_menuaddon_edit').attr('data-menu');
            var addon_id = $('#btn_menuaddon_edit').attr('data-addon');
            if(! $f.valid()) return false;
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu Addon updated successfully', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                    $.get("{{guard_url('restaurant/menu_items/menu/detailform')}}/"+menu_id, function(data) { $('#menuItem').html(data); });  
                    $.get("{{guard_url('restaurant/menu_items/addon/editform')}}/"+menu_id+'/'+addon_id, function(data) { $('#Edit_addon').html(data); }); 
       
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 
 $(document).on('click','#btn_add_menuvariation',function(){ 
            var $f = $('#restaurant-menu-edit-variation');
            var menu_id = $('#btn_add_menuvariation').attr('data-menu');
            var price = document.getElementById("price").value;
            if(! $f.valid()) return false;
            if(price < 0){
                toastr.error('Price should be greater than 0', 'Error');
                return false;
            }
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu Variation added successfully', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                    $.get("{{guard_url('restaurant/menu_items/menu/detailform')}}/"+menu_id, function(data) { $('#menuItem').html(data); });  
                    $.get("{{guard_url('restaurant/menu_items/variation/addform')}}/"+menu_id, function(data) { $('#add_Variation').html(data); });
       
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 
 $(document).on('click','#btn_edit_menuvariation',function(){ 
            var $f = $('#restaurant-menu-edit');
            var menu_id = $('#btn_edit_menuvariation').attr('data-menu');
            var key = $('#btn_edit_menuvariation').attr('data-key');
            var price = document.getElementById("price").value;
            if(! $f.valid()) return false;
            if(isNaN(price)){
                toastr.error('Price should be a number', 'Error');
                return false;
            }
            if(price < 0){
                toastr.error('Price should be greater than 0', 'Error');
                return false;
            }
            $.getJSON({
                  type: 'PUT',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu Variation updated successfully', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                    $.get("{{guard_url('restaurant/menu_items/menu/detailform')}}/"+menu_id, function(data) { $('#menuItem').html(data); });  
                    $.get("{{guard_url('restaurant/menu_items/variation/editform')}}/"+menu_id+'/'+key, function(data) { $('#Edit_variation').html(data); }); 
       
                             
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }); 
 function deletecategory(key) {
    var $f = $('#delete-form_'+key);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
                  type: 'delete',
                  url: $f.attr('action'),
                  success: function(data) { 
                    toastr.success('Category Deleted', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                          
                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }
        function deletemenu(key,cat) {
    var $f = $('#delete-menu_'+key);
    document.getElementById("cat"+cat).className = "show";

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
                  type: 'delete',
                  url: $f.attr('action'),
                  success: function(data) { 
                    toastr.success('Menu Deleted', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                              document.getElementById("menuItem").style.display = "none"; 
                               $('.menu-element-item-wrapper .element-box .empty-msg').show();

document.getElementById("emptymsg").style.display = "block"; 

                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }
         function deleteaddon(menu_id,addon_id,menu) {
    var $f = $('#delete-addon_'+menu_id+'_'+addon_id);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
                  type: 'GET',
                  url: $f.attr('action'),
                  success: function(data) { 
                    toastr.success('Menu Addon Deleted', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                    document.getElementById("menuItem").style.display = "block"; 
$.get("{{guard_url('restaurant/menu_items/menu/detailform')}}/"+menu, function(data) { $('#menuItem').html(data); });  


                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }

          function deletevariation(menu_id,key,menu) {
    var $f = $('#delete-variation_'+menu_id+'_'+key);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
                  type: 'GET',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    toastr.success('Menu Variation Deleted', 'sucess');
                    $('#Menu_Accordion').load('{{guard_url('restaurant/menu_items/category/list')}}');
                    document.getElementById("menuItem").style.display = "block"; 
$.get("{{guard_url('restaurant/menu_items/menu/detailform')}}/"+menu, function(data) { $('#menuItem').html(data); });  


                  },
                  error: function(xhr, status, error) {
                    toastr.error(xhr.responseJSON.message, 'Error');
                    return false;
}
              });
           
        }

</script>

                                                        
                                                            
                                                           
                                                       