<div class="menu-element-add-wrapper-header">
                                                <h5>Add Menu</h5>
                                                <button type="button" class="btn ion-android-close close-menu-element-add-wrapper" id="ss"></button>
                                            </div>
                                             {!!Form::vertical_open()
            ->id('restaurant-menu-create')
            ->method('POST')
            ->files('true')
            ->addClass('compact-edit')
            ->action(guard_url('restaurant/menu'))!!}
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <label for="">Menu Name*</label>
                                                            <input class="form-control" name="name" placeholder="" required="required" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Category*</label>
                                                            <select class="form-control" name="category_id" id="category_id">
                                                                <option>Please select category</option>
                                                                @forelse(Restaurant::getRestaurantCategory(user_id()) as $key => $category)
                                                                    <option value="{{$key}}">{{$category}}</option>
                                                                @empty
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Price*</label>
                                                            <input type="number" required="required" name="price" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Serves</label>
                                                            <input type="text" name="serves" class="form-control" placeholder="">
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Maxium Order Amount</label>
                                                            <input class="form-control" name="max_order_count" placeholder="" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Youtube Link</label>
                                                            <input class="form-control" name="youtube_link" placeholder="" type="text">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Rating</label>
                                                            <select class="form-control" name="review" id="review">
                                                                <option value="">Please select rating</option>
                                                                    <option value="1">1</option>
                                                                    <option value="2">2</option>
                                                                    <option value="3">3</option>
                                                                    <option value="4">4</option>
                                                                    <option value="5">5</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                <div class="form-group">
                                                      <input data-toggle="tooltip" title="Enable/Disable Catering" type="checkbox" name="catering" >Catering
                                                </div>
                                            </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Description</label>
                                                            <textarea name="description" class="form-control" placeholder="" rows="4"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group border-0">
                                                            <label for="">Image</label>
                        {!! $menuss->files('image')
                        ->url($menuss->getUploadUrl('image'))
                        ->mime(config('filer.image_extensions'))
                        ->dropzone()!!}
                    {!! $menuss->files('image')
                    ->editor()!!}

                                                        </div>
                                                    </div>
                                                     

                                                <div class="repeater mt-20">
                                                    <div class="item-sectitle">
                                                        <h3>Variations</h3>
                                                        <button data-repeater-create type="button" class="btn btn-theme btn-icon ion-android-add" onclick="addVariation()"></button>
                                                    </div>
                                                    <div id="variation_div"></div>
                                                </div>
                                                <div class="repeater" style="margin-top: -1px;">
                                                    <div class="item-sectitle">
                                                        <h3>Addons</h3>
                                                        <button id="addon_icon" data-repeater-create type="button" class="btn btn-theme btn-icon ion-android-add" onclick="addAddons()"></button>
                                                    </div>
                                                    <div id="addon_div"></div>
                                                </div>
                                                    <div class="col-md-12">
                                                        <div class="form-footer mb-10 mt-20 text-center">
                                                            <button type="button" id="btn_add_menu" style="width: 150px;" class="btn btn-theme">Add Menu</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
<script type="text/javascript">
      $('#ss').on( "click", function(e) { 
       $(".menu-element-add-wrapper").hide();
       $(".menu-element-item-wrapper, .menu-element-edit-wrapper").show();
    $('.menu-element-item-wrapper .element-box .empty-msg').show();
    }); 

</script>                                                