@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')

    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
                <i class="flaticon-burger app-sec-title-icon"></i>
                <h1>Menu Set Up</h1>
                <div class="actions">
                    <div class="dropdown">
                        <button type="button" class="btn btn-with-icon btn-dark dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-plus-circle mr-5"></i>Add</button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#addCatModal" data-toggle="modal" data-target="#addCatModal">Add Category</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addMenuModal">Add Menu</a>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#importMenuModal">Import Menu</a>
                        </div>
                    </div>
                </div>
                <a href="menu.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="menu-accordion-wrap">
                <div class="accordion" id="menuCategories">
                    <div class="card" data-id="1">
                        <div class="card-header" data-toggle="collapse" data-target="#dhabba_special" aria-expanded="false" aria-controls="dhabba_special">
                            <div class="handle"><i class="fas fa-arrows-alt"></i></div>
                            <h4>dhaba specials</h4>
                            <button type="button" class="btn action-res" type="button" id="catAction_01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-ellipsis"></i></button>
                            <div class="actions" id="catAction_01">
                                <a href="#" class="btn schedule-btn"><i class="fas fa-tag"></i></a>
                                <a href="#" class="btn edit-btn"><i class="fas fa-pencil-alt"></i></a>
                                <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </div>
                        <div id="dhabba_special" class="collapse" aria-labelledby="headingOne" data-parent="#menuCategories">
                            <div class="card-body">
                                <div class="menu-items-wrap">
                                    <div class="menu-item" >
                                        <a class="menu-item-block" href="#menuModal" data-toggle="modal" data-target="#menuModal">
                                            <div class="menu-img" style="background-image: url('https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg')"></div>
                                            <h4>fish curry</h4>
                                        </a>
                                        <button type="button" class="btn action-res" type="button" id="menuAction_01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-ellipsis"></i></button>
                                        <div class="actions" id="menuAction_01">
                                            <a href="#" class="btn edit-menu"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="btn schedule-menu"><i class="fas fa-tag"></i></a>
                                            <a href="#" class="btn schedule-menu-offer"><i class="fas fa-clock"></i></a>
                                            <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="card" data-id="2">
                        <div class="card-header" data-toggle="collapse" data-target="#biriyani" aria-expanded="false" aria-controls="biriyani">
                            <div class="handle"><i class="fas fa-arrows-alt"></i></div>
                            <h4>Biriyani</h4>
                            <button type="button" class="btn action-res" type="button" id="catAction_02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-ellipsis"></i></button>
                            <div class="actions" id="catAction_02">
                                <a href="#" class="btn"><i class="fas fa-tag"></i></a>
                                <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                            </div>
                        </div>
                        <div id="biriyani" class="collapse" aria-labelledby="headingOne" data-parent="#menuCategories">
                            <div class="card-body">
                                <div class="menu-items-wrap">
                                    <div class="menu-item">
                                        <div class="menu-item-block">
                                            <div class="menu-img" style="background-image: url('https://www.cookwithmanali.com/wp-content/uploads/2019/09/Vegetable-Biryani-Restaurant-Style-500x500.jpg')"></div>
                                            <h4>Vegetable Biryani</h4>
                                        </div>
                                        <button type="button" class="btn action-res" type="button" id="menuAction_02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-ellipsis"></i></button>
                                        <div class="actions" id="menuAction_02">
                                            <a href="#" class="btn edit-menu"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="btn schedule-menu"><i class="fas fa-tag"></i></a>
                                            <a href="#" class="btn schedule-menu-offer"><i class="fas fa-clock"></i></a>
                                            <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                    <div class="menu-item">
                                        <div class="menu-item-block">
                                            <div class="menu-img" style="background-image: url('https://recipesofhome.com/wp-content/uploads/2020/06/chicken-biryani-recipe-720x540.jpg')"></div>
                                            <h4>Chicken Biryani</h4>
                                        </div>
                                        <button type="button" class="btn action-res" type="button" id="menuAction_03" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="flaticon-ellipsis"></i></button>
                                        <div class="actions" id="menuAction_03">
                                            <a href="#" class="btn edit-menu"><i class="fas fa-pencil-alt"></i></a>
                                            <a href="#" class="btn schedule-menu"><i class="fas fa-tag"></i></a>
                                            <a href="#" class="btn schedule-menu-offer"><i class="fas fa-clock"></i></a>
                                            <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="addCatModal" tabindex="-1" aria-labelledby="addCatModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="addCatModalLabel">Add Category</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label for="">Category Name <sup>*</sup></label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-12">
                    <label for="">Preparation Time <sup>*</sup></label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Minutes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-dark">Save Category</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="scheduleModal" tabindex="-1" aria-labelledby="scheduleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="scheduleModalLabel">Schedule dhaba specials</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="">Title</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Price</label>
                <input type="number" class="form-control">
            </div>
            <div class="modal-sec-title modal-sec-title-with-action">
                <h4>Dates</h4>
                <div class="actions">
                    <a href="#" class="btn btn-dark"><i class="fas fa-plus-circle"></i> Add</a>
                </div>
            </div>
            <div class="schedule-form">
                <div class="schedule-form-item">
                    <div class="row gutter-20">
                        <div class="col-md-5">
                            <div class="form-group">
                                <select class="form-control">
                                    <option selected="" disabled="">Select Date</option>
                                    <option value="Mon">Monday</option>
                                    <option value="Tue">Tuesday</option>
                                    <option value="Wed">Wednesday</option>
                                    <option value="Thu">Thursday</option>
                                    <option value="Fri">Friday</option>
                                    <option value="Sat">Saturday</option>
                                    <option value="Sun">Sunday</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row gutter-20">
                                <div class="col-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option value="">From</option>
                                            <option value="00:00">12:00 AM</option>
                                            <option value="01:00">01:00 AM</option>
                                            <option value="02:00">02:00 AM</option>
                                            <option value="03:00">03:00 AM</option>
                                            <option value="04:00">04:00 AM</option>
                                            <option value="05:00">05:00 AM</option>
                                            <option value="06:00">06:00 AM</option>
                                            <option value="07:00">07:00 AM</option>
                                            <option value="08:00">08:00 AM</option>
                                            <option value="09:00">09:00 AM</option>
                                            <option value="10:00">10:00 AM</option>
                                            <option value="11:00">11:00 AM</option>
                                            <option value="12:00">12:00 PM</option>
                                            <option value="13:00">01:00 PM</option>
                                            <option value="14:00">02:00 PM</option>
                                            <option value="15:00">03:00 PM</option>
                                            <option value="16:00">04:00 PM</option>
                                            <option value="17:00">05:00 PM</option>
                                            <option value="18:00">06:00 PM</option>
                                            <option value="19:00">07:00 PM</option>
                                            <option value="20:00">08:00 PM</option>
                                            <option value="21:00">09:00 PM</option>
                                            <option value="22:00">10:00 PM</option>
                                            <option value="23:00">11:00 PM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option value="">To</option>
                                            <option value="00:00">12:00 AM</option>
                                            <option value="01:00">01:00 AM</option>
                                            <option value="02:00">02:00 AM</option>
                                            <option value="03:00">03:00 AM</option>
                                            <option value="04:00">04:00 AM</option>
                                            <option value="05:00">05:00 AM</option>
                                            <option value="06:00">06:00 AM</option>
                                            <option value="07:00">07:00 AM</option>
                                            <option value="08:00">08:00 AM</option>
                                            <option value="09:00">09:00 AM</option>
                                            <option value="10:00">10:00 AM</option>
                                            <option value="11:00">11:00 AM</option>
                                            <option value="12:00">12:00 PM</option>
                                            <option value="13:00">01:00 PM</option>
                                            <option value="14:00">02:00 PM</option>
                                            <option value="15:00">03:00 PM</option>
                                            <option value="16:00">04:00 PM</option>
                                            <option value="17:00">05:00 PM</option>
                                            <option value="18:00">06:00 PM</option>
                                            <option value="19:00">07:00 PM</option>
                                            <option value="20:00">08:00 PM</option>
                                            <option value="21:00">09:00 PM</option>
                                            <option value="22:00">10:00 PM</option>
                                            <option value="23:00">11:00 PM</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-dark">Schedule Item</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="scheduleMenuModal" tabindex="-1" aria-labelledby="scheduleMenuModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="scheduleMenuModalLabel">Schedule Fish Curry</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        </div>
        <div class="modal-body">
            <div class="row align-items-center mb-3">
                <div class="col-5">
                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                        <input type="checkbox" class="custom-control-input" id="mon">
                        <label class="custom-control-label m-0" for="mon">Monday</label>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row gutter-20">
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[mon][start]">
                                    <option value="">From</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[mon][end]">
                                    <option value="">To</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mb-3">
                <div class="col-5">
                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                        <input type="checkbox" class="custom-control-input" id="tue">
                        <label class="custom-control-label m-0" for="tue">Tuesday</label>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row gutter-20">
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[tue][start]">
                                    <option value="">From</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[tue][end]">
                                    <option value="">To</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mb-3">
                <div class="col-5">
                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                        <input type="checkbox" class="custom-control-input" id="wed">
                        <label class="custom-control-label m-0" for="wed">Wednesday</label>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row gutter-20">
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[wed][start]">
                                    <option value="">From</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[wed][end]">
                                    <option value="">To</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mb-3">
                <div class="col-5">
                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                        <input type="checkbox" class="custom-control-input" id="thu">
                        <label class="custom-control-label m-0" for="thu">Thursday</label>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row gutter-20">
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[thu][start]">
                                    <option value="">From</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[thu][end]">
                                    <option value="">To</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mb-3">
                <div class="col-5">
                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                        <input type="checkbox" class="custom-control-input" id="fri">
                        <label class="custom-control-label m-0" for="fri">Friday</label>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row gutter-20">
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[fri][start]">
                                    <option value="">From</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[fri][end]">
                                    <option value="">To</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center mb-3">
                <div class="col-5">
                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                        <input type="checkbox" class="custom-control-input" id="sat">
                        <label class="custom-control-label m-0" for="sat">Saturday</label>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row gutter-20">
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[sat][start]">
                                    <option value="">From</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[sat][end]">
                                    <option value="">To</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-5">
                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                        <input type="checkbox" class="custom-control-input" id="sun">
                        <label class="custom-control-label m-0" for="sun">Sunday</label>
                    </div>
                </div>
                <div class="col-7">
                    <div class="row gutter-20">
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[sun][start]">
                                    <option value="">From</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group m-0">
                                <select class="form-control" name="timing[sun][end]">
                                    <option value="">To</option>
                                    <option value="00:00">12:00 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-dark">Schedule Item</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="categoryModal" tabindex="-1" aria-labelledby="categoryModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="categoryModalLabel">Edit dhaba specials</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Category Name <sup>*</sup></label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="">Preparation Time <sup>*</sup></label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Minutes</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Status <sup>*</sup></label>
                        <div class="custom-control custom-switch custom-switch-lg">
                            <input type="checkbox" class="custom-control-input" id="customSwitch1">
                            <label class="custom-control-label" for="customSwitch1">&nbsp;</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Order No</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-dark">Update Category</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal add-menu-modal" id="menuModal" tabindex="-1" aria-labelledby="categoryModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable">
    <div class="modal-content">
        <div class="add-menu-modal-header">
            <h1>Edit Menu Fish Curry</h1>
        </div>
        <a href="#previewFishCurry" class="preview-btn" data-preview="previewFishCurry"><i class="fas fa-server"></i><span>Preview</span></a>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        <div class="modal-body p-0">
            <div class="row no-gutters">
                <div class="col-md-7">
                    <form action="" class="p-3">
                        <div class="modal-sec-title mt-5">
                            <h4>Edit Menu</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Menu Name <sup>*</sup></label>
                                            <input type="text" class="form-control" value="Fish Curry">
                                        </div>
                                    </div>
                                    <div class="col" style="max-width: 85px;">
                                        <div class="form-group">
                                            <label for="">Status <sup>*</sup></label>
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch2">
                                                <label class="custom-control-label" for="customSwitch2">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" class="form-control" placeholder="Description" rows="3">spicy</textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <label for="">Menu Image</label>
                                <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                    <div class="upload-wrap">
                                        <div class="upload-wraper">
                                            <div class="dropzone dropzone-previews dz-clickable">
                                                <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                            </div>
                                            <div class="image-editor">
                                                <div class="img-box">
                                                    <div class="img-container">
                                                        <a href="#" target="_blank">
                                                            <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                        </a>
                                                        <div class="btn-container">
                                                            <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                            <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Category <sup>*</sup></label>
                                    <select class="form-control" required="" id="category_id" name="category_id">
                                        <option value="" disabled="disabled">Please select category</option>
                                        <option value="1048">Appetizers</option>
                                        <option value="1049" selected="selected">Biryani</option>
                                        <option value="46757">Cakes</option>
                                        <option value="1050">Chicken Specials</option>
                                        <option value="50772">Create your Own</option>
                                        <option value="1051">Desserts</option>
                                        <option value="50774">Dinner @2</option>
                                        <option value="48966">preparation time</option>
                                        <option value="1058">Rice Dish</option>
                                        <option value="1059">Salads</option>
                                        <option value="1060">Seafood Specials</option>
                                        <option value="1061">Soups</option>
                                        <option value="1062">Tandoori Specials</option>
                                        <option value="1063">Vegetarian Specials</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Closest Dish for Search Results</label>
                                    <select class="form-control" required="" id="category_id" name="category_id">
                                        <option value="" disabled="disabled">Please select master</option>
                                        <option value="10527">Acai Bowl</option>
                                        <option value="11326">Adana Kebab</option>
                                        <option value="11550">Agua De Jamaica</option>
                                        <option value="10607">Alfredo Sauce</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Price <sup>*</sup></label>
                                            <input type="number" name="price" class="form-control" value="5.00" required="required">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Serves</label>
                                            <input type="text" name="serves" class="form-control" value="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Popular Dish No</label>
                                                    <input type="number" name="popular_dish_no" class="form-control" value="0">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Maxium Order Amount</label>
                                                    <input type="text" name="max_order_count" class="form-control" value="5">
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col" style="max-width: 90px;">
                                        <div class="form-group">
                                            <label for="">Catering</label>
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch3">
                                                <label class="custom-control-label" for="customSwitch3">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer pb-0 pl-0 pr-0">
                            <button type="button" class="btn btn-dark">Update Menu</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="menu-modal-addon-varient-wrap">
                        <div class="addon-varient-list-wrap">
                            <div class="modal-sec-title modal-sec-title-with-action mt-5">
                                <h4>Addons</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-dark add-btn" data-related="edit_addAddonForm"><i class="fas fa-plus-circle"></i> Add</a>
                                </div>
                            </div>
                            <div class="addon-items-wrap">
                                <div class="info-text-wrap">
                                    <small>Only created Add ons can be selected for this menu. </small><small id="edit_addon_infos" style="display: none;">To create add-ons please go back to <a href="menu-addons.html">Menu > Add ons</a>. Please note prices for Addons are in addition to the menu price. Eg. Cheese Pizza (menu price) - $10, Add On Topping - $1, then the customer pays - $11</small><a href="#edit_addon_infos" class="view-more-text-btn">Read More</a>
                                </div>

                                <div class="addon-item">
                                    <h4>Drinks</h4>
                                    <div class="actions">
                                        <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                        <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                                <div class="addon-item">
                                    <h4>Coca Cola</h4>
                                    <div class="actions">
                                        <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                        <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-sec-title modal-sec-title-with-action">
                                <h4>Addon Items</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-dark add-btn" data-related="edit_addVariationForm"><i class="fas fa-plus-circle"></i> Add</a>
                                </div>
                            </div>
                            <div class="addon-items-wrap">
                                <div class="info-text-wrap">
                                    <small>Variation of the menu can be added here. Eg. Small $10, Large $12, X-Large $14. </small><small id="edit_variation_infos" style="display: none;">The price entered for variation is the new price of the menu. Addons will be in addition to the variation price. Eg. Cheese Pizza (menu price) - $10, Variation Large - $12, Add-on Topping - $1,  then the customer pays -  $13</small><a href="#edit_variation_infos" class="view-more-text-btn">Read More</a>
                                </div>
                                <div class="addon-item">
                                    <h4>fried noodles sm</h4>
                                    <div class="actions">
                                        <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                        <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                                <div class="addon-item">
                                    <h4>fried noodles full</h4>
                                    <div class="actions">
                                        <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                        <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="menu-modal-addon-varient-form-wrap" id="edit_addAddonForm" style="display: none;">
                            <div class="modal-sec-title modal-sec-title-with-action mt-5">
                                <h4>Add Addon</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-secondary close-btn" data-close="edit_addAddonForm"><i class="fas fa-times"></i> Close</a>
                                </div>
                            </div>
                            <div class="menu-modal-addon-varient-form-wrap-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Choose Addons <sup>*</sup></label>
                                            <select class="form-control" id="330764" required="">
                                                <option value="" disabled="disabled" selected="selected">Please choose addon</option>
                                                <option value="152">Drinks</option>
                                                <option value="196">Choose Upto Three Entrees</option>
                                                <option value="197">Choose Entree 1</option>
                                                <option value="198">Choose Entree 2</option>
                                                <option value="199">Choose Free Entree</option>
                                                <option value="277">chicken popcorn</option>
                                                <option value="279">fries</option>
                                                <option value="293">egg rice</option>
                                                <option value="973">spicy</option>
                                                <option value="974">naan</option>
                                                <option value="975">chicken</option>
                                                <option value="1163">beef samosa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Min</label>
                                                            <input type="number" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Max</label>
                                                            <input type="number" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col" style="max-width: 90px;">
                                                <div class="form-group">
                                                    <label for="">Required</label>
                                                    <div class="custom-control custom-switch custom-switch-lg">
                                                        <input type="checkbox" class="custom-control-input" id="customSwitch5">
                                                        <label class="custom-control-label" for="customSwitch5">&nbsp;</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="modal-sec-title mt-5">
                                            <h4>Variations</h4>
                                        </div>
                                        <div class="row align-items-center mb-15">
                                            <div class="col">
                                                <div class="form-group m-0">
                                                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                                                        <input type="checkbox" class="custom-control-input" id="variation_id_0" name="av_152[variation_list][0][name]" value="Pepsi"> 
                                                        <label class="custom-control-label" for="variation_id_0">Pepsi</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                 <div class="form-group m-0">
                                                    <input class="form-control" id="152_0price" placeholder="Price" required="" type="text" name="av_152[variation_list][0][price]">
                                                </div>
                                            </div>
                                        </div>

                                    </div>  
                                </div>
                            </div>
                            
                            <div class="modal-footer pb-0 pl-0 pr-0">
                                <button type="button" class="btn btn-dark">Save Addon</button>
                            </div>
                        </div>
                        <div class="menu-modal-addon-varient-form-wrap" id="edit_addVariationForm" style="display: none;">
                            <div class="modal-sec-title modal-sec-title-with-action mt-5">
                                <h4>Add Addon Items</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-secondary close-btn" data-close="edit_addVariationForm"><i class="fas fa-times"></i> Close</a>
                                </div>
                            </div>
                            <div class="menu-modal-addon-varient-form-wrap-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Name <sup>*</sup></label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Description</label>
                                            <textarea name="" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="modal-footer pb-0 pl-0 pr-0">
                                <button type="button" class="btn btn-dark">Save Addon Item</button>
                            </div>
                        </div>

                        
                        <div class="menu-modal-addon-varient-form-wrap" id="previewFishCurry" style="display: none;">
                            <div class="modal-sec-title modal-sec-title-with-action mt-5">
                                <h4>Menu Preview</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-secondary close-preview" data-close="previewFishCurry"><i class="fas fa-times"></i> Close Preview</a>
                                </div>
                            </div>
                            <div class="preview-menu-wrap">
                                <p>As seen by customer on the online ordering page</p>
                                <div class="menu-preview-head">
                                    <h1>Fish Curry</h1>
                                    <div class="menu-metas">
                                        <span>dhaba specials</span>
                                        <span><i class="fas fa-utensils mr-5"></i>1</span>
                                        <span>$5.00</span>
                                    </div>
                                </div>
                                <div class="menu-preview-body">
                                    <div class="menu-offers">
                                        <h4 class="text-success">Happy Monday Price- $ 12</h4>
                                        <p>Regular Price- $ 14.95</p>
                                        <p><b>Mon at 12:00 AM to 11:00 PM</b></p>
                                    </div>
                                    <div class="linked-addons-wrap">
                                        <div class="addons-wrap">
                                            <div class="addon-item">
                                                <div class="addon-item-inner" data-toggle="collapse" data-target="#fishCurryPreviewVariations" aria-expanded="false" aria-controls="fishCurryPreviewVariations">
                                                    <h3 class="m-0">Variation</h3>
                                                </div>
                                                <div class="variations-wrap collapse" class="collapse" id="fishCurryPreviewVariations">
                                                    <div class="variation-item mt-10">
                                                        <div class="variation-item-inner">
                                                            <h3>Large <span class="price">$5.00</span></h3>
                                                        </div>
                                                    </div>
                                                    <div class="variation-item">
                                                        <div class="variation-item-inner">
                                                            <h3>Quarter <span class="price">$1.99</span></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="addon-item">
                                                <div class="addon-item-inner" data-toggle="collapse" data-target="#fishCurryPreviewDrinks" aria-expanded="false" aria-controls="fishCurryPreviewDrinks">
                                                    <div class="d-flex justify-content-between">
                                                        <h3 class="mb-0">Drinks</h3>
                                                        <span>(Required)</span>
                                                    </div>
                                                </div>
                                                <div class="variations-wrap collapse" class="collapse" id="fishCurryPreviewDrinks">
                                                    <div class="variation-item mt-10">
                                                        <div class="variation-item-inner">
                                                            <h3>Pepsi <span class="price">$1.99</span></h3>
                                                        </div>
                                                    </div>
                                                    <div class="variation-item">
                                                        <div class="variation-item-inner">
                                                            <h3>Iced Tea <span class="price">$2.49</span></h3>
                                                        </div>
                                                    </div>
                                                    <div class="variation-item">
                                                        <div class="variation-item-inner">
                                                            <h3>Hot Tea <span class="price">$1.99</span></h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="scheduleOfferMenuModal" tabindex="-1" aria-labelledby="scheduleOfferMenuModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="scheduleOfferMenuModalLabel">Schedule Offer for Fish Curry</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="">Title</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Price</label>
                <input type="number" class="form-control">
            </div>
            <div class="modal-sec-title modal-sec-title-with-action">
                <h4>Schedule Offer</h4>
                <div class="actions">
                    <a href="#" class="btn btn-dark"><i class="fas fa-plus-circle"></i> Add</a>
                </div>
            </div>
            <div class="schedule-form">
                <div class="schedule-form-item">
                    <div class="row gutter-20">
                        <div class="col-md-5">
                            <div class="form-group">
                                <select class="form-control">
                                    <option selected="" disabled="">Select Date</option>
                                    <option value="Mon">Monday</option>
                                    <option value="Tue">Tuesday</option>
                                    <option value="Wed">Wednesday</option>
                                    <option value="Thu">Thursday</option>
                                    <option value="Fri">Friday</option>
                                    <option value="Sat">Saturday</option>
                                    <option value="Sun">Sunday</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row gutter-20">
                                <div class="col-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option value="">From</option>
                                            <option value="00:00">12:00 AM</option>
                                            <option value="01:00">01:00 AM</option>
                                            <option value="02:00">02:00 AM</option>
                                            <option value="03:00">03:00 AM</option>
                                            <option value="04:00">04:00 AM</option>
                                            <option value="05:00">05:00 AM</option>
                                            <option value="06:00">06:00 AM</option>
                                            <option value="07:00">07:00 AM</option>
                                            <option value="08:00">08:00 AM</option>
                                            <option value="09:00">09:00 AM</option>
                                            <option value="10:00">10:00 AM</option>
                                            <option value="11:00">11:00 AM</option>
                                            <option value="12:00">12:00 PM</option>
                                            <option value="13:00">01:00 PM</option>
                                            <option value="14:00">02:00 PM</option>
                                            <option value="15:00">03:00 PM</option>
                                            <option value="16:00">04:00 PM</option>
                                            <option value="17:00">05:00 PM</option>
                                            <option value="18:00">06:00 PM</option>
                                            <option value="19:00">07:00 PM</option>
                                            <option value="20:00">08:00 PM</option>
                                            <option value="21:00">09:00 PM</option>
                                            <option value="22:00">10:00 PM</option>
                                            <option value="23:00">11:00 PM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option value="">To</option>
                                            <option value="00:00">12:00 AM</option>
                                            <option value="01:00">01:00 AM</option>
                                            <option value="02:00">02:00 AM</option>
                                            <option value="03:00">03:00 AM</option>
                                            <option value="04:00">04:00 AM</option>
                                            <option value="05:00">05:00 AM</option>
                                            <option value="06:00">06:00 AM</option>
                                            <option value="07:00">07:00 AM</option>
                                            <option value="08:00">08:00 AM</option>
                                            <option value="09:00">09:00 AM</option>
                                            <option value="10:00">10:00 AM</option>
                                            <option value="11:00">11:00 AM</option>
                                            <option value="12:00">12:00 PM</option>
                                            <option value="13:00">01:00 PM</option>
                                            <option value="14:00">02:00 PM</option>
                                            <option value="15:00">03:00 PM</option>
                                            <option value="16:00">04:00 PM</option>
                                            <option value="17:00">05:00 PM</option>
                                            <option value="18:00">06:00 PM</option>
                                            <option value="19:00">07:00 PM</option>
                                            <option value="20:00">08:00 PM</option>
                                            <option value="21:00">09:00 PM</option>
                                            <option value="22:00">10:00 PM</option>
                                            <option value="23:00">11:00 PM</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-dark">Schedule Item</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal add-menu-modal" id="addMenuModal" tabindex="-1" aria-labelledby="addMenuModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable">
    
    <div class="modal-content">
        <div class="add-menu-modal-header">
            <h1>Menu, Varients and Addons</h1>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        <div class="modal-body p-0">
            <div class="row no-gutters">
                <div class="col-md-7">
                    <form action="" class="p-3">
                        <div class="modal-sec-title mt-5">
                            <h4>Add Menu</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Menu Name <sup>*</sup></label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col" style="max-width: 85px;">
                                        <div class="form-group">
                                            <label for="">Status <sup>*</sup></label>
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch7">
                                                <label class="custom-control-label" for="customSwitch7">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" class="form-control" placeholder="Description" rows="3"></textarea>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <label for="">Menu Image</label>
                                <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                    <div class="upload-wrap">
                                        <div class="upload-wraper">
                                            <div class="dropzone dropzone-previews dz-clickable">
                                                <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Category <sup>*</sup></label>
                                    <select class="form-control" required="" id="category_id" name="category_id">
                                        <option value="" disabled="disabled" selected="selected">Please select category</option>
                                        <option value="1048">Appetizers</option>
                                        <option value="1049">Biryani</option>
                                        <option value="46757">Cakes</option>
                                        <option value="1050">Chicken Specials</option>
                                        <option value="50772">Create your Own</option>
                                        <option value="1051">Desserts</option>
                                        <option value="50774">Dinner @2</option>
                                        <option value="48966">preparation time</option>
                                        <option value="1058">Rice Dish</option>
                                        <option value="1059">Salads</option>
                                        <option value="1060">Seafood Specials</option>
                                        <option value="1061">Soups</option>
                                        <option value="1062">Tandoori Specials</option>
                                        <option value="1063">Vegetarian Specials</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Closest Dish for Search Results</label>
                                    <select class="form-control" required="" id="category_id" name="category_id">
                                        <option value="" disabled="disabled" selected="">Please select master</option>
                                        <option value="10527">Acai Bowl</option>
                                        <option value="11326">Adana Kebab</option>
                                        <option value="11550">Agua De Jamaica</option>
                                        <option value="10607">Alfredo Sauce</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Price <sup>*</sup></label>
                                            <input type="number" name="price" class="form-control" required="required">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Serves</label>
                                            <input type="text" name="serves" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Popular Dish No</label>
                                                    <input type="number" name="popular_dish_no" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="">Maxium Order Amount</label>
                                                    <input type="text" name="max_order_count" class="form-control">
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                    <div class="col" style="max-width: 90px;">
                                        <div class="form-group">
                                            <label for="">Catering</label>
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch6">
                                                <label class="custom-control-label" for="customSwitch6">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="modal-footer pb-0 pl-0 pr-0">
                            <button type="button" class="btn btn-dark">Add Menu</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="menu-modal-addon-varient-wrap">
                        <div class="addon-varient-list-wrap">
                            <div class="modal-sec-title modal-sec-title-with-action mt-5">
                                <h4>Addons</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-dark add-btn" data-related="addAddonForm"><i class="fas fa-plus-circle"></i> Add</a>
                                </div>
                            </div>
                            <div class="addon-items-wrap">
                                <div class="info-text-wrap">
                                    <small>Only created Add ons can be selected for this menu. </small><small id="addon_infos" style="display: none;">To create add-ons please go back to <a href="menu-addons.html">Menu > Add ons</a>. Please note prices for Addons are in addition to the menu price. Eg. Cheese Pizza (menu price) - $10, Add On Topping - $1, then the customer pays - $11</small><a href="#addon_infos" class="view-more-text-btn">Read More</a>
                                </div>
                            </div>
                            <div class="modal-sec-title modal-sec-title-with-action">
                                <h4>Addon Items</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-dark add-btn" data-related="addVariationForm"><i class="fas fa-plus-circle"></i> Add</a>
                                </div>
                            </div>
                            <div class="addon-items-wrap">
                                <div class="info-text-wrap">
                                    <small>Variation of the menu can be added here. Eg. Small $10, Large $12, X-Large $14. </small><small id="variation_infos" style="display: none;">The price entered for variation is the new price of the menu. Addons will be in addition to the variation price. Eg. Cheese Pizza (menu price) - $10, Variation Large - $12, Add-on Topping - $1,  then the customer pays -  $13</small><a href="#variation_infos" class="view-more-text-btn">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="menu-modal-addon-varient-form-wrap" id="addAddonForm" style="display: none;">
                            <div class="modal-sec-title modal-sec-title-with-action mt-5">
                                <h4>Add Addon</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-secondary close-btn" data-close="addAddonForm"><i class="fas fa-times"></i> Close</a>
                                </div>
                            </div>
                            <div class="menu-modal-addon-varient-form-wrap-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Choose Addons <sup>*</sup></label>
                                            <select class="form-control" id="330764" required="">
                                                <option value="" disabled="disabled" selected="selected">Please choose addon</option>
                                                <option value="152">Drinks</option>
                                                <option value="196">Choose Upto Three Entrees</option>
                                                <option value="197">Choose Entree 1</option>
                                                <option value="198">Choose Entree 2</option>
                                                <option value="199">Choose Free Entree</option>
                                                <option value="277">chicken popcorn</option>
                                                <option value="279">fries</option>
                                                <option value="293">egg rice</option>
                                                <option value="973">spicy</option>
                                                <option value="974">naan</option>
                                                <option value="975">chicken</option>
                                                <option value="1163">beef samosa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Min</label>
                                                            <input type="number" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label for="">Max</label>
                                                            <input type="number" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col" style="max-width: 90px;">
                                                <div class="form-group">
                                                    <label for="">Required</label>
                                                    <div class="custom-control custom-switch custom-switch-lg">
                                                        <input type="checkbox" class="custom-control-input" id="customSwitch5">
                                                        <label class="custom-control-label" for="customSwitch5">&nbsp;</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="modal-sec-title mt-5">
                                            <h4>Addon Items</h4>
                                        </div>
                                        <div class="row align-items-center mb-15">
                                            <div class="col">
                                                <div class="form-group m-0">
                                                    <div class="custom-control custom-checkbox custom-checkbox-lg">
                                                        <input type="checkbox" class="custom-control-input" id="variation_id_0" name="av_152[variation_list][0][name]" value="Pepsi"> 
                                                        <label class="custom-control-label" for="variation_id_0">Pepsi</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                 <div class="form-group m-0">
                                                    <input class="form-control" id="152_0price" placeholder="Price" required="" type="text" name="av_152[variation_list][0][price]">
                                                </div>
                                            </div>
                                        </div>

                                    </div>  
                                </div>
                            </div>
                            
                            <div class="modal-footer pb-0 pl-0 pr-0">
                                <button type="button" class="btn btn-dark">Save Addon</button>
                            </div>
                        </div>
                        <div class="menu-modal-addon-varient-form-wrap" id="addVariationForm" style="display: none;">
                            <div class="modal-sec-title modal-sec-title-with-action mt-5">
                                <h4>Add Addon Item</h4>
                                <div class="actions">
                                    <a href="#" class="btn btn-secondary close-btn" data-close="addVariationForm"><i class="fas fa-times"></i> Close</a>
                                </div>
                            </div>
                            <div class="menu-modal-addon-varient-form-wrap-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Name <sup>*</sup></label>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="">Description</label>
                                            <textarea name="" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="modal-footer pb-0 pl-0 pr-0">
                                <button type="button" class="btn btn-dark">Save Addon Item</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="importMenuModal" tabindex="-1" aria-labelledby="importMenuModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="importMenuModalLabel">Import Menu</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <div class="dropzone dropzone-previews" style="min-height: 187px;">
                    <div class="upload-wrap">
                        <div class="upload-wraper">
                            <div class="dropzone dropzone-previews dz-clickable">
                                <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-dark">Import Menu</button>
        </div>
    </div>
</div>