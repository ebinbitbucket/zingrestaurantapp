@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
    <div class="app-content-inner">
        {!!Form::vertical_open()
            ->id('restaurant-restaurant-edit')
            ->method('POST')
            ->enctype('multipart/form-data')
            ->action(guard_url('restaurant/updateapp/'. user()->getRouteKey()))!!}
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action actions-mobile-bottom">
                <i class="flaticon-app app-sec-title-icon"></i>
                <h1>App Set Up</h1>
                <div class="actions">
                    <button type="button" class="btn btn-with-icon btn-secondary"><i
                            class="fas fa-eye mr-5"></i>View</button>
                    <button type="submit" class="btn btn-with-icon btn-dark"><i
                            class="fas fa-save mr-5"></i>Save</button>
                </div>
                <a href="app.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title" data-toggle="collapse" data-target="#basicInfo" aria-expanded="true"
                    aria-controls="basicInfo">
                    <h2>Basic</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse show" id="basicInfo">
                    <div class="row mt-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Meta Title <i class="fa fa-question-circle" data-toggle="tooltip"
                                        data-placement="right" title=""
                                        data-original-title="Helps Search Engines like Google undertand your content. e.g. Black Cats Pizza place in Irving,TX"></i>
                                    <sup>*</sup></label>
                                <input class="form-control" placeholder="Title" required="required" name="title"
                                    type="text" value="{{@$app->title}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Meta Subtitle
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                                        title=""
                                        data-original-title="Helps Search Engines like Google undertand your content"></i>
                                </label>
                                <input type="text" class="form-control" name="subtitle" placeholder="Subtitle"
                                    value="{{@$app->subtitle}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Restaurant Email
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                                        title=""
                                        data-original-title="Email where inquiries from Customers will be sent"></i>
                                </label>
                                <input type="text" required="" class="form-control" name="customer_email"
                                    placeholder="Contact email" value="{{@$app->email}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Restaurant Phone
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                                        title=""
                                        data-original-title="Phone number where customers calls will be directed to. e.g. 817-857-8482"></i>
                                </label>
                                <input type="text" class="form-control" name="customer_phone"
                                    placeholder="Contact number" value="{{@$app->alternate_phone}}" pattern="^([0-9\s\-\+\(\)]*)$">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#contact"
                    aria-expanded="false" aria-controls="contact">
                    <h2>Contact & Locations</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="contact">
                    <button id="addLoc" class="btn btn-secondary mt-15 mb-10">Add Location</button>
                    @if(is_array(@$app->locations))
                    @foreach(@$app->locations as $loc_key => $loc_value)
                    <div class="locations-form-item">
                        <div class="row"  id="loc_div_{{$loc_key}}">
                            <div class="col-md-6">
                                <div class="form-group mb-10">
                                    <label> Address <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Address e.g. Fort Worth, TX"></i></label>
                                    <input type="text" id="fee_address_{{$loc_key}}" name="locations[{{$loc_key}}][address]"
                                        class="form-control" value="{{@$loc_value['address']}}" placeholder="Address">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-10">
                                    <label> Location <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Location Name E.g. Tiger Xpress on Beach St, Fort Worth"></i>
                                    </label>
                                    <input type="text" id="fee_title_{{$loc_key}}" name="locations[{{$loc_key}}][text]" value="{{@$loc_value['text']}}" class="form-control" placeholder="Location Name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">
                                    <label> Email <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Email where inquiries from Customers will be sent"></i>
                                    </label>

                                    <input type="email" id="fee_email_{{$loc_key}}" name="locations[{{$loc_key}}][email]" class="form-control"
                                        value="{{@$loc_value['email']}}" placeholder="Email Address">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">
                                    <label> Phone <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Phone Number of the Location"></i>
                                    </label>

                                    <input type="text" id="fee_phone_{{$loc_key}}" name="locations[{{$loc_key}}][phone]" class="form-control"
                                        value="{{@$loc_value['phone']}}" placeholder="Phone" pattern="^([0-9\s\-\+\(\)]*)$">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">
                                    <label>Order Link
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                                            title="" data-original-title="Zing Link to order from this location"></i>
                                    </label>
                                    <input type="url" id="fee_amt_{{$loc_key}}" name="locations[{{$loc_key}}][url]" class="form-control" value="{{@$loc_value['url']}}" placeholder="Website URL">
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-dark btn-icon location-remove-btn" type="button" id="fee_rmvbtn_{{$loc_key}}" onclick="removeLoc({{$loc_key}})"><i
                                class="fa fa-trash"></i><span>Remove</span></button>
                    </div>
                    @endforeach
                    @endif
                    <div id="loc_div"></div>

                    {{-- <div class="locations-form-item">
                        <div class="row" id="loc_div_1">
                            <div class="col-md-6">
                                <div class="form-group mb-10">

                                    <label> Address <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Address e.g. Fort Worth, TX"></i></label>

                                    <input type="text" id="fee_address_1" name="locations[1][address]"
                                        class="form-control" value="Fort Worth, TX" placeholder="Address">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-10">

                                    <label> Location <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Location Name E.g. Tiger Xpress on Beach St, Fort Worth"></i>
                                    </label>

                                    <input type="text" id="fee_title_1" name="locations[1][text]" class="form-control"
                                        value="Fort Worth 2" placeholder="Location Name">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">

                                    <label> Email <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Email where inquiries from Customers will be sent"></i>
                                    </label>

                                    <input type="email" id="fee_email_1" name="locations[1][email]" class="form-control"
                                        value="restaurant@zing.com" placeholder="Email Address">

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">

                                    <label> Phone <i class="fa fa-question-circle" data-toggle="tooltip"
                                            data-placement="right" title=""
                                            data-original-title="Phone Number of the Location"></i>
                                    </label>

                                    <input type="text" id="fee_phone_1" name="locations[1][phone]" class="form-control"
                                        value="45665555" placeholder="Phone" pattern="^([0-9\s\-\+\(\)]*)$">

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group mb-10">

                                    <label>Order Link
                                        <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                                            title="" data-original-title="Zing Link to order from this location"></i>
                                    </label>

                                    <input type="url" id="fee_amt_1" name="locations[1][url]" class="form-control"
                                        value="https://www.zomato.com/bangalore/indian-kitchen-malleshwaram"
                                        placeholder="Website URL">

                                </div>
                            </div>
                        </div>
                        <button class="btn btn-dark btn-icon location-remove-btn" type="button" id="fee_rmvbtn_0"><i
                                class="fa fa-trash"></i><span>Remove</span></button>
                    </div> --}}

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="">Contact Information
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right"
                                        title="" data-original-title="Text to display on Contact Form Section"></i>
                                </label>
                                <textarea class="form-control" name="contact_info"
                                    placeholder="Contact information text" rows="2">{{@$app->contact_info}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#images"
                    aria-expanded="false" aria-controls="images">
                    <h2>Images</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="images">
                    <div class="row mt-15">
                        <div class="col-md-6">
                            <label for="">Featured Items <i class="fa fa-question-circle" data-toggle="tooltip"
                                    data-placement="right" title=""
                                    data-original-title="Images for App Featured Items Section; (Size - 508 px x 320 px)"></i></label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="row">
                                    <div class='col-lg-12 col-sm-12'>
                                       {!! $app->files('featured_food')
                                       ->url($app->getUploadUrl('featured_food'))
                                       ->mime(config('filer.image_extensions'))
                                       ->dropzone()!!}
                                    </div>
                                    <div class='col-lg-12 col-sm-12'>
                                       {!! $app->files('featured_food')
                                       ->editor()!!}
                                    </div>
                                 </div>

                          


                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Slider Images <i class="fa fa-question-circle" data-toggle="tooltip"
                                    data-placement="right" title=""
                                    data-original-title="Images for App Slider Section; (Size - 1620 px x 1080 px)"></i></label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="row">
                                    <div class='col-lg-12 col-sm-12'>
                                       {!! $app->files('slider_images')
                                       ->url($app->getUploadUrl('slider_images'))
                                       ->mime(config('filer.image_extensions'))
                                       ->dropzone()!!}
                                    </div>
                                    <div class='col-lg-12 col-sm-12'>
                                       {!! $app->files('slider_images')
                                       ->editor()!!}
                                    </div>
                                 </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Gallery <i class="fa fa-question-circle" data-toggle="tooltip"
                                    data-placement="right" title=""
                                    data-original-title="Images for App Gallery Section; (Size - 320 px x 320 px)"></i></label>
                            <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                <div class="row">
                                    <div class='col-lg-12 col-sm-12'>
                                       {!! $app->files('gallery')
                                       ->url($app->getUploadUrl('gallery'))
                                       ->mime(config('filer.image_extensions'))
                                       ->dropzone()!!}
                                    </div>
                                    <div class='col-lg-12 col-sm-12'>
                                       {!! $app->files('gallery')
                                       ->editor()!!}
                                    </div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="app-entry-form-section">
                <div class="entry-form-title collapsed" data-toggle="collapse" data-target="#points-view"
                    aria-expanded="false" aria-controls="images">
                    <h2>Loyalty Points</h2>
                    <p>Information regarding your Eatery or Restaurant as seen in the Zing Platform.</p>
                    <div class="icon"><i class="fas fa-chevron-up"></i></div>
                </div>
                <div class="collapse" id="points-view">
                    <div class="row mt-15">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for=""><b>Point Conversion ( % ) </b>
                                    <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Email where inquiries from Customers will be sent"></i>
                                </label>
                                <label id="points">
                                    @if($app->points != null)
                                     <p>For Every $100 customer spends they get <b> {{ $app->points }}</b> points or <b>${{$app->point_percentage }}</b> which can be used towards a future purchase.</p>
                                    @endif
                                </label>
                                <input type="number" step="0.1" min="0" max="100" class="form-control" id="point_percentage" name="point_percentage" placeholder="Point Conversion to Cash in %" value="{{@$app->point_percentage}}">
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>

        </div>
        {!!Form::close()!!}

    </div>
</div>
<script>      
    var count=0;
    <?php if (!empty(@user()->locations) && is_array(user()->locations)):  $f = last(array_keys(user()->locations));?>
    var fee_count = {!!$f!!} + 1; 
    <?php else: ?>  var fee_count = 1;
    <?php endif ?>
   
    function addLoc() {
       
        var newTextBoxDiv = $(document.createElement('div'))
            .attr("id", 'loc_div_' + fee_count);
            newTextBoxDiv.after().html(
            '<div class="locations-form-item" id="LocEdit"><div class="row"  id="loc_div_"><div class="col-md-6"><div class="form-group mb-10"><label> Address <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Address e.g. Fort Worth, TX"></i></label><input type="text" name="locations['+
            fee_count +'][address]" class="form-control" placeholder="Address"></div></div><div class="col-md-6"><div class="form-group mb-10"><label> Location <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Location Name E.g. Tiger Xpress on Beach St, Fort Worth"></i></label><input type="text" name="locations[' +fee_count +'][text]" class="form-control" placeholder="Location Name"></div></div><div class="col-md-4"><div class="form-group mb-10"><label> Email <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Email where inquiries from Customers will be sent"></i></label><input type="email" name="locations[' +fee_count +'][email]" class="form-control" placeholder="Email Address"></div></div><div class="col-md-4"><div class="form-group mb-10"><label> Phone <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title=""data-original-title="Phone Number of the Location"></i></label><input type="text" name="locations[' +fee_count +'][phone]" class="form-control" placeholder="Phone" pattern="^([0-9\s\-\+\(\)]*)$"></div></div><div class="col-md-4"><div class="form-group mb-10"><label>Order Link<i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="" data-original-title="Zing Link to order from this location"></i></label><input type="url" name="locations[' +fee_count +'][url]" class="form-control" value="{{@$loc_value['url']}}" placeholder="Website URL"></div></div></div><button class="btn btn-dark btn-icon location-remove-btn" type="button" id="fee_rmvbtn_'+fee_count+'" onclick="removeLoc('+fee_count+')"><i class="fa fa-trash"></i><span>Remove</span></button></div>');

        newTextBoxDiv.appendTo("#loc_div ");
        fee_count++;
    }
   
    function removeLoc(key) {
    document.getElementById('loc_div_'+key).remove();  
    document.getElementById('fee_rmvbtn_'+key).remove();  

  }

  $(function () {
  $('[data-toggle="tooltip"]').tooltip();
})

$("#addLoc").click(function(event){
  event.preventDefault();
  addLoc();
  var elmnt = document.getElementById("LocEdit");
  elmnt.scrollIntoView();
});
$(document).ready(function(){
  $("#point_percentage").keyup(function(){
    percentage = this.value;
    points = (percentage/0.5)*10;
    $("#points").empty();
    
    $("#points").append(`<p>For Every $100 customer spends they get <b> ${points}</b> points or <b>$${percentage}</b> which can be used towards a future purchase.</p>`);
  });
});
</script>

