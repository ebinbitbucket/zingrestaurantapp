{!! Form::vertical_open()
->id('restaurant-restaurant-edit')
->method('POST')
->enctype('multipart/form-data')
->action(guard_url('restaurant/addpush/'.user()->getRouteKey())) !!}
@include('notifications')
<div class="row">
    @foreach ($notifications as $data)

    <div class="app-entry-form-section p-0">
        <h5 class="font-weight-bold">Edit Push Notification</h5>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Name <i class="fa fa-question-circle" style="cursor: pointer;" data-toggle="tooltip" data-placement="right" title="" data-original-title="Name of the message for your records"></i></label>
                    <input type="text" id="push_not_name" class="form-control" value="{{ $data->title }}" name="push_not_name" placeholder="Push Notification Name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Schedule Date</label>
                    <input type="datetime-local" id="push_not_date" value="{{ date('Y-m-d\TH:i', strtotime($data->schedule_date)) }}" name="schedule_date" class="form-control" value="2020-09-24T06:23" placeholder="Schedule Date">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Message</label>
                    <textarea class="form-control" name="push_not_content" rows="3" placeholder="Push Notification Message">{{ $data->content }}</textarea>
                </div>
            </div>
            <input type="text" class="d-none" name="restaurant_id" class="form-control" value="{{ user()->getRouteKey() }}" placeholder="id">
            <input type="text" class="d-none" name="push_not_id" class="form-control" value="{{ $data->id }}" placeholder="push_not_id">
        
            <div class="form-group text-left">
                <button class="btn btn-dark" type="submit">Update</button>
            </div>
            &nbsp;&nbsp;&nbsp;
            <div class="form-group text-left">
                <button class="btn btn-secondary" type="button" onclick="removeEditPush()" id="cancelEdit">
                    Cancel
                </button>
            </div>
        </div>
    </div>





















        {{-- <div class="col-md-12">
            <div class="repeater">
                <div class="position-relative" data-repeater-item>
                    <br>
                    <h5 class="font-weight-bold">Edit Push Notification</h5>
                    <input type="text" class="d-none" name="restaurant_id" class="form-control"
                        value="{{ user()->getRouteKey() }}" placeholder="id">
                        <input type="text" class="d-none" name="push_not_id" class="form-control"
                        value="{{ $data->id }}" placeholder="push_not_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Push Notification Name - <i class="fa fa-question-circle" data-toggle="tooltip"
                                        data-placement="top"
                                        title="Helps Search Engines like Google undertand your content. e.g. Black Cats Pizza place in Irving,TX"></i></label>
                                <input type="text" id="push_not_name" class="form-control" name="push_not_name"
                                    value="{{ $data->title }}" placeholder="Push Notification Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Schedule Date - <i class="fa fa-question-circle" data-toggle="tooltip"
                                        data-placement="top" title="The time to send push notification."></i></label>
                                <input type="datetime-local" id="push_not_date" name="schedule_date"
                                    class="form-control"
                                    value="{{ date('Y-m-d\TH:i', strtotime($data->schedule_date)) }}"
                                    placeholder="Schedule Date">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>Push Notification Message - <i class="fa fa-question-circle"
                                        data-toggle="tooltip" data-placement="top"
                                        title="This message will be send as Push Notification Content."></i></label>
                                <textarea class="form-control" name="push_not_content" rows="3"
                                    placeholder="Push Notification Message">{{ $data->content }}</textarea>
                            </div>
                        </div>
                        <div class="form-group text-left">
                            <button class="btn btn-theme" type="submit">Update</button>
                        </div>
                        &nbsp;&nbsp;&nbsp;
                        <div class="form-group text-left">
                            <button class="btn btn-secondary" type="button" onclick="removeEditPush()" id="cancelEdit">
                                Cancel
                            </button>
                        </div> --}}


    @endforeach
</div>
{!! Form::close() !!}
