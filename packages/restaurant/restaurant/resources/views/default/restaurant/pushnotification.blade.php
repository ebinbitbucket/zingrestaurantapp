@include('restaurant::default.restaurant.partial.header')
<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
    <div class="app-content-inner">
        {!! Form::vertical_open()
            ->id('restaurant-restaurant-edit')
            ->method('POST')
            ->enctype('multipart/form-data')
            ->action(guard_url('restaurant/addpush/' . user()->getRouteKey())) !!}
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
                <i class="flaticon-notification app-sec-title-icon"></i>
                <h1>Push Notifications</h1>
                <div class="actions">
                    <button type="submit" class="btn btn-with-icon btn-dark"><i
                            class="fas fa-save mr-5"></i>Save</button>
                </div>
                <a href="app.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="app-conent-tabs">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="create-tab" data-toggle="tab" href="#create" role="tab"
                            aria-controls="create" aria-selected="true">Create</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="scheduled-tab" data-toggle="tab" href="#scheduled" role="tab"
                            aria-controls="scheduled" aria-selected="false">Scheduled</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="past-tab" data-toggle="tab" href="#past" role="tab" aria-controls="past"
                            aria-selected="false">Past</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade active show" id="create" role="tabpanel" aria-labelledby="create-tab">
                       <div class="app-entry-form-section p-0">
                            <div class="row">
                                <input type="text" class="d-none" name="restaurant_id" class="form-control" value="{{ user()->getRouteKey() }}" placeholder="id">
                                <input type="text" class="d-none" name="push_not_id" class="form-control" value="" placeholder="push_not_id">   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name <i class="fa fa-question-circle" style="cursor: pointer;"data-toggle="tooltip" data-placement="right" title="" data-original-title="Name of the message for your records"></i></label>
                                        <input type="text" id="push_not_name" class="form-control" name="push_not_name"
                                            placeholder="Push Notification Name">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Schedule Date</label>
                                        <input type="datetime-local" id="push_not_date" name="schedule_date"
                                            class="form-control" value="2020-09-24T06:23" placeholder="Schedule Date">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea class="form-control" name="push_not_content" rows="3"
                                            placeholder="Push Notification Message"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="scheduled" role="tabpanel" aria-labelledby="scheduled-tab">
                        <div id="editPushDiv"> </div>
                        <div class="statements-list-wrap">
                            <div class="table-responsive">
                                @if (count($new_notifications) != 0)
                                    <table class="table m-0 table-align-center">
                                        <thead>
                                            <tr>
                                                <th scope="col">Notification Name</th>
                                                <th scope="col">Notification Message</th>
                                                <th scope="col">Schedule Time</th>
                                                <th scope="col">Created At</th>
                                                <th scope="col">Updated At</th>
                                                <th scope="col" class="text-right">Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach (@$new_notifications as $loc_key => $loc_value)

                                                <tr>
                                                    @php
                                                    $loc_key =$loc_key+1;
                                                    @endphp
                                                    <td data-label="Notification Name">{{ @$loc_value->title }}</td>
                                                    <td data-label="Notification Name">{{ @$loc_value->content }}</td>
                                                    <td data-label="Schedule Time">
                                                        {{ date('Y-m-d H:i:A', strtotime($loc_value->schedule_date)) }}
                                                    </td>
                                                    <td data-label="Created At">
                                                        {{ date('Y-m-d H:i:A', strtotime($loc_value->created_at)) }}
                                                    </td>
                                                    <td data-label="Updated At">
                                                        {{ date('Y-m-d H:i:A', strtotime($loc_value->updated_at)) }}
                                                    </td>
                                                    <td data-label="Actions" class="text-right">
                                                        <div class="table-actions">
                                                            <button type="button" onclick="editPush(this.value)"
                                                                value="{{ @$loc_value->id }}"
                                                                class="btn btn-icon btn-secondary"><i
                                                                    class="fas fa-pencil-alt"></i></button>
                                                            <button type="button" onclick="deletePush(this.value)"
                                                                value="{{ @$loc_value->id }}"
                                                                class="btn btn-icon btn-secondary"><i
                                                                    class="fas fa-trash"></i></button>
                                                        </div>
                                                    </td>
                                                </tr>

                                            @endforeach



                                        </tbody>
                                    </table>
                                @else
                                    <h6>No Results</h6>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="past" role="tabpanel" aria-labelledby="past-tab">
                        <div class="statements-list-wrap">
                            <div class="table-responsive">
                                @if (count($past_notifications) != 0)
                                    <table class="table m-0 table-align-center">
                                        <thead>
                                            <tr>
                                                <th scope="col">Notification Name</th>
                                                <th scope="col">Notification Message</th>
                                                <th scope="col">Schedule Time</th>
                                                <th scope="col">Created At</th>
                                                <th scope="col">Updated At</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach (@$past_notifications as $loc_key => $loc_value)
                                                <tr>
                                                    @php
                                                    $loc_key =$loc_key+1;
                                                    @endphp
                                                    <td data-label="Notification Name">{{ @$loc_value->title }}</td>
                                                    {{-- <td class="text-wrap">
                                                        {{ @$loc_value->content }}</td> --}}
                                                    <td data-label="Notification Name">{{ @$loc_value->content }}</td>
                                                    <td data-label="Schedule Time">
                                                        {{ date('Y-m-d H:i:A', strtotime($loc_value->schedule_date)) }}
                                                    </td>
                                                    <td data-label="Created At">
                                                        {{ date('Y-m-d H:i:A', strtotime($loc_value->created_at)) }}
                                                    </td>
                                                    <td data-label="Updated At">
                                                        {{ date('Y-m-d H:i:A', strtotime($loc_value->updated_at)) }}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <h6>No Results</h6>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}

    </div>
</div>
<script>
    function editPush(push_not_id) {
        $.ajax({
            type: 'GET',
            url: 'edit-push/' + push_not_id,
            success: function(data) {
                $('#editPushDiv').empty();
                $('#editPushDiv').html(data);
                $('[data-toggle="tooltip"]').tooltip();

                // window.location.hash = '#appEdit';
                // var elmnt = document.getElementById("appEdit");
                // elmnt.scrollIntoView();
            },
            error: function(msg) {

            }
        });
    }

    function removeEditPush() {
        $('#editPushDiv').empty();
    }

    function deletePush(push_not_id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    url: "delete-push" + '/' + push_not_id,
                    type: 'GET',
                    success: function(data) {
                        toastr.info('This feature will be enabled soon.', 'Coming soon');

                        swal({
                            title: 'Success',
                            text: 'Deleted Successfully!',
                            type: 'success',
                            timer: '1000'
                        })
                        window.location = "{{ guard_url('restaurant/push-notifications') }}";
                    },
                    error: function(msg) {
                        swal({
                            title: 'Opps...',
                            text: msg,
                            type: 'error',
                            timer: '1500'
                        })
                    }
                })
            } else {
                return;
            }
        });

    }

</script>
