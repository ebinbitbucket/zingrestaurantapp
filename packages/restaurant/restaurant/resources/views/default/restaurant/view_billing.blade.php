@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
   
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon mb-20">
                <i class="flaticon-wallet app-sec-title-icon"></i>
                <h1>Billing</h1>
                <a href="index.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a class="link-widget mb-20" href="{{ guard_url('restaurant/billing') }}">
                        <i class="flaticon-card"></i>
                        <h4>Account Details</h4>
                        <p>Enter your Banking information here to get paid.</p>
                    </a>
                </div>
                <div class="col-md-6">
                    <a class="link-widget mb-20" href="{{ guard_url('cart/orders/monthstmnt') }}">
                        <i class="flaticon-invoice"></i>
                        <h4>Statements & Reports</h4>
                        <p>See Monthly statements and orders by date range</p>
                    </a>
                </div>
            </div>  
        </div>
    </div>
</div>