@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon">
                <i class="flaticon-megaphone app-sec-title-icon"></i>
                <h1>Marketing Hub</h1>
                <a href="index.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
        </div>
    </div>
</div>
