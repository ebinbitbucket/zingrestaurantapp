<style>
.onoffswitch {
    position: relative; width: 90px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
    display: none;
}
.onoffswitch-label {
    display: block; overflow: hidden; cursor: pointer;
    border: 2px solid #999999; border-radius: 20px;
}
.onoffswitch-inner {
    display: block; width: 200%; margin-left: -100%;
    transition: margin 0.3s ease-in 0s;
}
.onoffswitch-inner:before, .onoffswitch-inner:after {
    display: block; float: left; width: 50%; height: 30px; padding: 0; line-height: 30px;
    font-size: 14px; color: white; font-family: Trebuchet, Arial, sans-serif; font-weight: bold;
    box-sizing: border-box;
}
.onoffswitch-inner:before {
    content: "ON";
    padding-left: 10px;
    background-color: #34A7C1; color: #FFFFFF;
}
.onoffswitch-inner:after {
    content: "OFF";
    padding-right: 10px;
    background-color: #EEEEEE; color: #999999;
    text-align: right;
}
.onoffswitch-switch {
    display: block; width: 18px; margin: 6px;
    background: #FFFFFF;
    position: absolute; top: 0; bottom: 0;
    right: 56px;
    border: 2px solid #999999; border-radius: 20px;
    transition: all 0.3s ease-in 0s;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-switch {
    right: 0px;

}
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #333;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #40b659;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<aside class="main-nav">
    <div class="nav-inner">
    @include('restaurant::default.restaurant.partial.mobile_menu')


    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-3 d-none d-lg-block">
                <aside class="dashboard-sidemenu">
                @include('restaurant::default.restaurant.partial.left_menu')


                </aside>
            </div>
            <div class="col-md-12 col-lg-9">
                <div class="element-wrapper">
                    <div class="element-box">
                             {!!Form::vertical_open()
->id('restaurant-restaurant-edit')
->method('Get')
->enctype('multipart/form-data')
->addClass('compact-edit p-0')
->action(guard_url('restaurant/settings-save/'. user()->getRouteKey()))!!}
                            <div class="element-info">
                                <div class="element-info-with-icon">
                                    <div class="element-info-icon"><div class="icon icon ion-settings"></div></div>
                                    <div class="element-info-text">
                                        <h5 class="element-inner-header">Account Settings</h5>
                                    </div>
                                </div>
                            </div>
                            @include('notifications')
                            <div class="row">
                                <div class="col-md-6">


                                <div class="form-group">
                                <label style="padding:5px" for=""> Start taking online orders</label>

                        <label class="switch">
                            <input type="checkbox" id="myonoffswitch" name="onoffswitch" @if($restaurant_stop==false)
 checked @endif>
                            <span class="slider round"></span>
                        </label>
                        

                    </div>


                               
                               
                               
                               
                               
                                </div>




                                <div class="col-md-6">
                                <div class="form-group">
                                <label style="padding:5px"  for=""> In-house Delivery</label>

                        <label class="switch">
                            <input type="checkbox" id="delivery_status" name="delivery_status" {{($restaurant->delivery=='Yes'?'checked':'')}}>
                            <span class="slider round"></span>
                        </label>
                        <input type="hidden" value="{{(@$restaurant->delivery=='Yes'?'Yes':'No')}}" id="delivery" name="delivery">

                    </div>
                    </div>

                    <div class="col-md-6">
                                <div class="form-group">
                                <label style="padding:5px"  for=""> No-Contact Delivery Option</label>

                        <label class="switch">
                            <input type="checkbox" id="contact_less" name="contact_less" {{($restaurant->non_contact=='1'?'checked':'')}}>
                            <span class="slider round"></span>
                        </label>
                        <input type="hidden" value="{{(@$restaurant->non_contact=='1'?'1':'0')}}" id="non_contact" name="non_contact">

                    </div>
                    </div>

                    <div class="col-md-6">
                                <div class="form-group">
                                <label style="padding:5px"  for=""> Car Pickup Option</label>

                        <label class="switch">
                            <input type="checkbox" id="pickup_car" name="pickup_car" {{($restaurant->car_pickup=='1'?'checked':'')}}>
                            <span class="slider round"></span>
                        </label>
                        <input type="hidden" value="{{(@$restaurant->car_pickup=='1'?'1':'0')}}" id="car_pickup" name="car_pickup">

                    </div>
                    </div>

                            </div>
                            <div class="form-buttons-w text-center">
                                <button class="btn btn-theme" type="submit" style="width: 150px;"> Update</button>
<!--                            <button class="btn btn-danger" type="button"> Cancel</button>
-->                         </div>
                       {!!Form::close()!!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade cusines-modal" id="stopKitchenModal" tabindex="-1" role="dialog" aria-labelledby="cuisine_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
        <form action="{{guard_url('restaurant/settings-save/'. user()->getRouteKey())}}" method="GET" class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cuisine_modalLabel">How long are you not taking orders ?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    <b>For the rest of  today(you can resume taking orders by clicking start at any time)</b>&nbsp;&nbsp; <input type="checkbox" name="stop_date" value="{{date("Y-m-d", strtotime("+ 1 day"))}}" id="rest_of_the_day" class="pt-2">
                </div>
                <div class="form-group">
                    <!-- <label><b>OR until the date selected below:</b></label> -->
                    <h6><i>Pick the date you want to start taking orders again.</i></h6>
                   <input type="text" id="datepicker1" class="form-control" name="stop_date" placeholder="">
                        <a href="#" class="text-secondary" data-toggle="tooltip" data-placement="top" data-html="true" title="" data-original-title="<p class='m-0 text-left'>This will stop taking orders for the day.</p>"><i class="ion-android-alert"></i></a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
    </div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">
 $("#rest_of_the_day").click(function () {
      $('#datepicker1').attr("disabled", $(this).is(":checked"));
   });
    $('[data-toggle="tooltip"]').tooltip();
    $( function() {
        $("#datepicker1" ).datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 1,
        });
        $("#datepicker1").datepicker('setDate', new Date());
    });
    $( document ).ready(function() {
        $('#myonoffswitch').change(function() {
        if(this.checked) {
           // var returnVal = confirm("Are you sure?");
            //$(this).prop("checked", returnVal);
        }else {
            $('#stopKitchenModal').modal('show');
        }
        //$('#textbox1').val(this.checked);
    });
    $('#delivery_status').change(function() {
        if(this.checked) {
            $('#delivery').val('Yes');

        }else {
             $('#delivery').val('No');

        }
    });
    $('#contact_less').change(function() {
        if(this.checked) {
            $('#non_contact').val('1');

        }else {
             $('#non_contact').val('0');

        }
    });
    $('#pickup_car').change(function() {
        if(this.checked) {
            $('#car_pickup').val('1');

        }else {
             $('#car_pickup').val('0');

        }
    });
    })
</script>
<style type="text/css">
    .form-control {
    display: inline;
    width: 90%;
</style>
