 @if(!empty($edit_varmenu->menu_variations))
                                                     <?php $s = last(array_keys($edit_varmenu->menu_variations))+1;
                                                     ?>
                                                    @else
                                                    <?php $s = 0;
                                                    ?>
                                                    @endif
                                                    <h3 class="d-none d-md-block">Add Variation</h3>
                                                        {!!Form::vertical_open()
        ->id('restaurant-menu-edit-variation')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->addClass('compact-edit')
        ->action(guard_url('restaurant/menu/'. $edit_varmenu->getRouteKey()))!!}

        @if(!empty($edit_varmenu->menu_variations))
        @foreach($edit_varmenu->menu_variations as $key => $menu_varat) 
            <input type='hidden' name="menu_variations[{{$key}}][name]" value="{{$menu_varat['name']}}">
            <input type='hidden' name="menu_variations[{{$key}}][price]" value="{{$menu_varat['price']}}">
            <input type='hidden' name="menu_variations[{{$key}}][description]" value="{{$menu_varat['description']}}">
        @endforeach
        @endif
                                                            <div class="row">
                                                                <div class="col-sm-8">
                                                                    <div class="form-group">
                                                                        <label for="">Name*</label>
                                                                        <input name="menu_variations[{{$s}}][name]" type="text" required="required" class="form-control" placeholder="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label for="">Price*</label>
                                                                        <input id="price" name="menu_variations[{{$s}}][price]" type="number" class="form-control" placeholder="" required="required" >
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Description</label>
                                                                <textarea name="menu_variations[{{$s}}][description]" class="form-control" rows="2"></textarea>
                                                            </div>
                                                            <div class="form-group border-0"><button type="button" id="btn_add_menuvariation" data-menu="{{$edit_varmenu->id}}"  class="btn btn-theme">Add Variation</button></div>
                                                         {!!Form::close()!!}