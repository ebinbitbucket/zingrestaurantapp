@include('restaurant::default.restaurant.partial.header')
            <div class="app-content-wrap">
                @include('restaurant::default.restaurant.partial.left_menu_new')
                <div class="app-content-inner">
                    <div class="app-entry-form-wrap">
                        {!! Form::vertical_open()
                        ->id('restaurant-restaurant-edit')
                        ->method('PUT')
                        ->enctype('multipart/form-data')
                        ->addClass('compact-edit p-0')
                        ->action(guard_url('restaurant/restaurant/' . user()->getRouteKey())) !!}
                        <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
                            <i class="flaticon-card app-sec-title-icon"></i>
                            <h1>Account Details</h1>
                            <div class="actions">
                                <button type="submit" class="btn btn-with-icon btn-dark"><i class="fas fa-save mr-5"></i>Save</button>
                            </div>
                            <a href="{{guard_url('restaurant/billing')}}" class="back-nav"><i class="fas fa-chevron-left"></i></a>
                        </div>
                        <div class="app-entry-form-section">
                        @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <b> Success </b>
                                <div>{{ Session::get('success') }}</div>
                            </div>
                            {{ Session::forget('success') }}
                        @endif
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Account Number <sup>*</sup></label>
                                       <input onfocus="this.value=''" class="form-control" id="ac_no" name="ac_no"
                                        placeholder="" required="required" type="text" pattern="[0-9a-zA-Z]*">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Routing Number <sup>*</sup></label>
                                        <input onfocus="this.value=''" class="form-control" id="IFSC" name="IFSC"
                                        placeholder="" required="required" type="text" pattern="[0-9a-zA-Z]*">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="">Bank <sup>*</sup></label>
                                        <input class="form-control" name="bank" placeholder="" required="required"
                                        type="text" onfocus="this.value=''" value="{{ user()->bank }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
<script type="text/javascript">
    $(document).ready(function() {
        var account = '<?php echo user()->ac_no; ?>';
        document.getElementById('ac_no').value = new Array(account.length - 3).join('x') + '-' + account.substr(
            account.length - 4, 4);
        var ifsc = '<?php echo user()->IFSC; ?>';
        document.getElementById('IFSC').value = new Array(ifsc.length - 3).join('x') + '-' + ifsc.substr(ifsc
            .length - 4, 4);
    })

</script>