@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
   
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon mb-20">
                <i class="flaticon-responsive app-sec-title-icon"></i>
                <h1>Website</h1>
                <a href="index.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a class="link-widget mb-20" href="{{ guard_url('restaurant/promotions') }}">
                        <i class="flaticon-price-cut"></i>
                        <h4>Promotions</h4>
                        <p>Upload an image with promotional information to market to your website visitors.</p>
                    </a>
                </div>
                <div class="col-md-6">
                    <a class="link-widget mb-20" href="{{ guard_url('restaurant/website') }}">
                        <i class="flaticon-design"></i>
                        <h4>Website Set Up</h4>
                        <p>Discharge best employed your phase each the of shine.</p>
                    </a>
                </div>
            </div>  
        </div>
    </div>
</div>