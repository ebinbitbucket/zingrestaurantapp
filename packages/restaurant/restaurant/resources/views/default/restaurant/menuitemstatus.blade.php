@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon">
                <i class="flaticon-restaurant-menu app-sec-title-icon"></i>
                <h1>Menu Status</h1>
                <a href="menu.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="menu-accordion-wrap menu-status-wrap">
                <div class="accordion" id="menuCategories">
                    <div class="card">
                        <div class="card-header" data-toggle="collapse" data-target="#dhabba_special" aria-expanded="false" aria-controls="dhabba_special">
                            <h4>dhaba specials</h4>
                            <div class="actions">
                                <div class="custom-control custom-switch custom-switch-lg">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch1">
                                    <label class="custom-control-label" for="customSwitch1">&nbsp;</label>
                                </div>
                            </div>
                            
                        </div>
                        <div id="dhabba_special" class="collapse" aria-labelledby="headingOne" data-parent="#menuCategories">
                            <div class="card-body">
                                <div class="menu-items-wrap">
                                    <div class="menu-item">
                                        <div class="menu-item-block">
                                            <div class="menu-img" style="background-image: url('https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg')"></div>
                                            <h4>fish curry</h4>
                                        </div>
                                        <div class="actions">
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch2">
                                                <label class="custom-control-label" for="customSwitch2">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" data-toggle="collapse" data-target="#biriyani" aria-expanded="false" aria-controls="biriyani">
                            <h4>Biriyani</h4>
                            <div class="actions">
                                <div class="custom-control custom-switch custom-switch-lg">
                                    <input type="checkbox" class="custom-control-input" id="customSwitch3">
                                    <label class="custom-control-label" for="customSwitch3">&nbsp;</label>
                                </div>
                            </div>
                        </div>
                        <div id="biriyani" class="collapse" aria-labelledby="headingOne" data-parent="#menuCategories">
                            <div class="card-body">
                                <div class="menu-items-wrap">
                                    <div class="menu-item">
                                        <div class="menu-item-block">
                                            <div class="menu-img" style="background-image: url('https://www.cookwithmanali.com/wp-content/uploads/2019/09/Vegetable-Biryani-Restaurant-Style-500x500.jpg')"></div>
                                            <h4>Vegetable Biryani</h4>
                                        </div>
                                        <div class="actions">
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch4">
                                                <label class="custom-control-label" for="customSwitch4">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="menu-item">
                                        <div class="menu-item-block">
                                            <div class="menu-img" style="background-image: url('https://recipesofhome.com/wp-content/uploads/2020/06/chicken-biryani-recipe-720x540.jpg')"></div>
                                            <h4>Chicken Biryani</h4>
                                        </div>
                                        <div class="actions">
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch5">
                                                <label class="custom-control-label" for="customSwitch5">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="scheduleModal" tabindex="-1" aria-labelledby="scheduleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="scheduleModalLabel">Schedule dhaba specials</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="">Title</label>
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <label for="">Price</label>
                <input type="number" class="form-control">
            </div>
            <div class="modal-sec-title modal-sec-title-with-action">
                <h4>Dates</h4>
                <div class="actions">
                    <a href="#" class="btn btn-success"><i class="fas fa-plus-circle"></i> Add</a>
                </div>
            </div>
            <div class="schedule-form">
                <div class="schedule-form-item">
                    <div class="row gutter-20">
                        <div class="col-md-5">
                            <div class="form-group">
                                <select class="form-control">
                                    <option selected="" disabled="">Select Date</option>
                                    <option value="Mon">Monday</option>
                                    <option value="Tue">Tuesday</option>
                                    <option value="Wed">Wednesday</option>
                                    <option value="Thu">Thursday</option>
                                    <option value="Fri">Friday</option>
                                    <option value="Sat">Saturday</option>
                                    <option value="Sun">Sunday</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row gutter-20">
                                <div class="col-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option value="">From</option>
                                            <option value="00:00">12:00 AM</option>
                                            <option value="01:00">01:00 AM</option>
                                            <option value="02:00">02:00 AM</option>
                                            <option value="03:00">03:00 AM</option>
                                            <option value="04:00">04:00 AM</option>
                                            <option value="05:00">05:00 AM</option>
                                            <option value="06:00">06:00 AM</option>
                                            <option value="07:00">07:00 AM</option>
                                            <option value="08:00">08:00 AM</option>
                                            <option value="09:00">09:00 AM</option>
                                            <option value="10:00">10:00 AM</option>
                                            <option value="11:00">11:00 AM</option>
                                            <option value="12:00">12:00 PM</option>
                                            <option value="13:00">01:00 PM</option>
                                            <option value="14:00">02:00 PM</option>
                                            <option value="15:00">03:00 PM</option>
                                            <option value="16:00">04:00 PM</option>
                                            <option value="17:00">05:00 PM</option>
                                            <option value="18:00">06:00 PM</option>
                                            <option value="19:00">07:00 PM</option>
                                            <option value="20:00">08:00 PM</option>
                                            <option value="21:00">09:00 PM</option>
                                            <option value="22:00">10:00 PM</option>
                                            <option value="23:00">11:00 PM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option value="">To</option>
                                            <option value="00:00">12:00 AM</option>
                                            <option value="01:00">01:00 AM</option>
                                            <option value="02:00">02:00 AM</option>
                                            <option value="03:00">03:00 AM</option>
                                            <option value="04:00">04:00 AM</option>
                                            <option value="05:00">05:00 AM</option>
                                            <option value="06:00">06:00 AM</option>
                                            <option value="07:00">07:00 AM</option>
                                            <option value="08:00">08:00 AM</option>
                                            <option value="09:00">09:00 AM</option>
                                            <option value="10:00">10:00 AM</option>
                                            <option value="11:00">11:00 AM</option>
                                            <option value="12:00">12:00 PM</option>
                                            <option value="13:00">01:00 PM</option>
                                            <option value="14:00">02:00 PM</option>
                                            <option value="15:00">03:00 PM</option>
                                            <option value="16:00">04:00 PM</option>
                                            <option value="17:00">05:00 PM</option>
                                            <option value="18:00">06:00 PM</option>
                                            <option value="19:00">07:00 PM</option>
                                            <option value="20:00">08:00 PM</option>
                                            <option value="21:00">09:00 PM</option>
                                            <option value="22:00">10:00 PM</option>
                                            <option value="23:00">11:00 PM</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success">Schedule Item</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="categoryModal" tabindex="-1" aria-labelledby="categoryModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="categoryModalLabel">Edit dhaba specials</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Category Name <sup>*</sup></label>
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">
                    <label for="">Preparation Time <sup>*</sup></label>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2">Minutes</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Status <sup>*</sup></label>
                        <div class="custom-control custom-switch custom-switch-lg">
                            <input type="checkbox" class="custom-control-input" id="customSwitch1">
                            <label class="custom-control-label" for="customSwitch1">&nbsp;</label>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="">Order No</label>
                        <input type="text" class="form-control">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-success">Update Category</button>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="menuModal" tabindex="-1" aria-labelledby="categoryModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-capitalize" id="categoryModalLabel">Edit Fish Curry</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fas fa-times"></i></button>
        </div>
        <div class="modal-body p-0">
            <div class="row no-gutters">
                <div class="col-md-7">
                    <form action="" class="p-3">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Menu Name <sup>*</sup></label>
                                            <input type="text" class="form-control" value="Fish Curry">
                                        </div>
                                    </div>
                                    <div class="col" style="max-width: 85px;">
                                        <div class="form-group">
                                            <label for="">Status <sup>*</sup></label>
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch2">
                                                <label class="custom-control-label" for="customSwitch2">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            
                            <div class="col-md-6">
                                <label for="">Menu Image</label>
                                <div class="dropzone dropzone-previews mb-15" style="min-height: 187px;">
                                    <div class="upload-wrap">
                                        <div class="upload-wraper">
                                            <div class="dropzone dropzone-previews dz-clickable">
                                                <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                                            </div>
                                            <div class="image-editor">
                                                <div class="img-box">
                                                    <div class="img-container">
                                                        <a href="#" target="_blank">
                                                            <img src="https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg" class="img-responsive" alt="">
                                                        </a>
                                                        <div class="btn-container">
                                                            <a href="#" class="edit-image text-primary"><i class="fas fa-pencil-alt"></i></a>
                                                            <a href="#" class="remove-image text-danger"><i class="fas fa-times"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Category <sup>*</sup></label>
                                    <select class="form-control" required="" id="category_id" name="category_id">
                                        <option value="" disabled="disabled">Please select category</option>
                                        <option value="1048">Appetizers</option>
                                        <option value="1049" selected="selected">Biryani</option>
                                        <option value="46757">Cakes</option>
                                        <option value="1050">Chicken Specials</option>
                                        <option value="50772">Create your Own</option>
                                        <option value="1051">Desserts</option>
                                        <option value="50774">Dinner @2</option>
                                        <option value="48966">preparation time</option>
                                        <option value="1058">Rice Dish</option>
                                        <option value="1059">Salads</option>
                                        <option value="1060">Seafood Specials</option>
                                        <option value="1061">Soups</option>
                                        <option value="1062">Tandoori Specials</option>
                                        <option value="1063">Vegetarian Specials</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Master</label>
                                    <select class="form-control" required="" id="category_id" name="category_id">
                                        <option value="" disabled="disabled">Please select master</option>
                                        <option value="10527">Acai Bowl</option>
                                        <option value="11326">Adana Kebab</option>
                                        <option value="11550">Agua De Jamaica</option>
                                        <option value="10607">Alfredo Sauce</option>
                                    </select>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Price <sup>*</sup></label>
                                            <input type="number" name="price" class="form-control" value="5.00" required="required">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="">Serves</label>
                                            <input type="text" name="serves" class="form-control" value="1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Popular Dish No</label>
                                    <input type="number" name="popular_dish_no" class="form-control" value="0">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Maxium Order Amount</label>
                                    <input type="text" name="max_order_count" class="form-control" value="5">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Youtube Link</label>
                                            <input type="text" name="youtube_link" class="form-control" value="http://gheerice">
                                        </div>
                                    </div>
                                    <div class="col" style="max-width: 90px;">
                                        <div class="form-group">
                                            <label for="">Catering</label>
                                            <div class="custom-control custom-switch custom-switch-lg">
                                                <input type="checkbox" class="custom-control-input" id="customSwitch3">
                                                <label class="custom-control-label" for="customSwitch3">&nbsp;</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" class="form-control" placeholder="Description" rows="3">spicy</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer pb-0 pl-0 pr-0">
                            <button type="button" class="btn btn-success">Update Menu</button>
                        </div>
                    </form>
                </div>
                <div class="col-md-5">
                    <div class="menu-modal-addon-varient-wrap">
                        <div class="modal-sec-title modal-sec-title-with-action mt-0">
                            <h4>Addons</h4>
                            <div class="actions">
                                <a href="#" class="btn btn-success"><i class="fas fa-plus-circle"></i> Add</a>
                            </div>
                        </div>
                        <div class="addon-items-wrap">
                            <div class="addon-item">
                                <h4>Drinks</h4>
                                <div class="actions">
                                    <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                </div>
                            </div>
                            <div class="addon-item">
                                <h4>Coca Cola</h4>
                                <div class="actions">
                                    <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="modal-sec-title modal-sec-title-with-action">
                            <h4>Variations</h4>
                            <div class="actions">
                                <a href="#" class="btn btn-success"><i class="fas fa-plus-circle"></i> Add</a>
                            </div>
                        </div>
                        <div class="addon-items-wrap">
                            <div class="addon-item">
                                <h4>fried noodles sm</h4>
                                <div class="actions">
                                    <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                </div>
                            </div>
                            <div class="addon-item">
                                <h4>fried noodles full</h4>
                                <div class="actions">
                                    <a href="#" class="btn"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="#" class="btn"><i class="fas fa-trash-alt"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
    </div>
</div>