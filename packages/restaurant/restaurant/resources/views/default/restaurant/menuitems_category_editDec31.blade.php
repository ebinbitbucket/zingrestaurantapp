<div class="menu-category-edit-wraper-header">
                                                <h5>Edit {{$edit_cats->name}}</h5>
                                                <button type="button" class="btn ion-android-close" id="catedit"></button>
                                            </div>
                                            {!!Form::vertical_open()
        ->id('restaurant-category-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->addClass('compact-edit')
        ->action(guard_url('restaurant/category/'. $edit_cats->getRouteKey()))!!}
                                           
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Category Name*</label>
                                                            <input class="form-control" name="name" placeholder="Category Name" required="required" type="text" value="{{$edit_cats->name}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                    <div class="form-group">
                                                            <label for="">Preparation Time*</label>
                                                            <input class="form-control" name="preparation_time" placeholder="Preparation Time" required="required" type="text" value="{{$edit_cats->preparation_time}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-footer mb-10">
                                                            <button type="button" id="btn_edit_category" data-category="{{$edit_cats->id}}" class="btn btn-theme">Update Category</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
<script type="text/javascript">
      $('#catedit').on( "click", function(e) { 
       $(".menu-category-edit-wrapper_1").hide();
       $(".menu-element-item-wrapper, .menu-element-edit-wrapper").show();
    $('.menu-element-item-wrapper .element-box .empty-msg').show();
    }); 

</script>                                            