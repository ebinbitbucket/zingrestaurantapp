<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a class="active" href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
            <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i>Monthly Statement</a>
            <a href="{{guard_url('expense/expense')}}"><i class="fa fa-sticky-note-o"></i>Invoice</a>
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                        <li >
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                                        </li>
                                         <li>
                                            <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-android-calendar"></i><span>Eatery Schedule</span></a>
                                        </li>
                                        <li class="active">
                                            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-android-home"></i><span>Kitchen</span></a>
                                        </li>
                                        <li>
                                             <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i>Monthly Statement</a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('expense/expense')}}"><i class="fa fa-sticky-note-o"></i><span>Invoice</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    {!!Form::vertical_open()
            ->id('kitchen-kitchen-create')
            ->method('POST')
            ->files('true')
            ->action(guard_url('kitchen/kitchen'))!!}
                                        <div class="element-info">
                                            <div class="element-info-with-icon">
                                                <div class="element-info-icon">
                                                    <div class="icon ion-social-buffer"></div>
                                                </div>
                                                <div class="element-info-text">
                                                    <h5 class="element-inner-header">Add Kitchen</h5>
                                                    <div class="element-inner-desc"></div>
                                                </div>
                                            </div>
                                        </div>
                                        @include('notifications')
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::text('name')
                       -> label(trans('kitchen::kitchen.label.name'))
                       -> placeholder(trans('kitchen::kitchen.placeholder.name'))!!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::text('username')
                       -> label('Username')
                       -> placeholder('Enter Username')!!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::email('email')
                       -> label(trans('kitchen::kitchen.label.email'))
                       -> required()
                       -> placeholder(trans('kitchen::kitchen.placeholder.email'))!!}
                                            </div>
                                             <div class="col-md-6">
                                                {!! Form::password('password')
                       -> label(trans('kitchen::kitchen.label.password'))
                       -> required()
                       -> placeholder(trans('kitchen::kitchen.placeholder.password'))!!}
                                            </div>
                                            <div class="col-md-12">
                                               {!! Form::textarea ('description')
                    -> label(trans('kitchen::kitchen.label.description'))
                    -> placeholder(trans('kitchen::kitchen.placeholder.description'))!!}
                                            </div>
                                           </div>
                                           <input type="hidden" name="restaurant_id" value="{{user_id()}}">
                                        <div class="form-buttons-w text-center">
                                          <button class="btn btn-theme" type="submit" style="height: 30px; width: 150px;"> Save</button>
                                           <!--  <button class="btn btn-danger" type="button"> Cancel</button> -->
                                        </div>
                                   {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
            <script type="text/javascript">
   var count=2;
  function addTiming(day){

     var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);

     // $("#TextBoxDiv"+count).load('{{guard_url("restaurant/timing")}}'+'/'+count+'/'+day);
     newTextBoxDiv.after().load('{{guard_url("restaurant/timing")}}'+'/'+count+'/'+day);
  // newTextBoxDiv.after().html(
  //       '<div class="row"><div class="col-md-3 col-sm-12"></div><div class="col-md-3" style="display: block";><input class="timepicker form-control" id="start" type="text" placeholder="Start Time" name='+day+'['+count+'][start]"></div><div class="col-md-3" style="display: block";><input class="timepicker form-control" id="start" type="text" placeholder="End Time" name='+day+'['+count+'][end]"></div><div class="col-md-3" style="display: block";></div></div>');

  newTextBoxDiv.appendTo("#variation_div_"+day);
    count ++;
  }

  function removeVar(day,key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=timings]").length/3;
  if(countvaraitions > 1){
       
              document.getElementById('TextBoxDiv'+key).remove();  
           
        }
       
        document.getElementById(key+'button').style.display = "none";   

  if(countvaraitions == 1){  
    document.getElementById(key).remove();  
     document.getElementById('btn_remove').style.display = "none";
     document.getElementById('variation_name').style.display = "none";
     unset(variation_list);
  }
}

    $('.timepicker').datetimepicker({

        format: 'HH:mm'

    });
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('textAddres'));
        google.maps.event.addListener(places, 'place_changed', function () {
            geocodeAddress(geocoder);
        });
    });
 function geocodeAddress(geocoder) {
        var address = document.getElementById('textAddres').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng()
            document.getElementById('latitude').value=results[0].geometry.location.lat();
            document.getElementById('longitude').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
    $(function(){ 
     var map,myLatlng;
    if(({{user()->latitude}}!='') && ({{user()->longitude}} != '')){
         myLatlng = new google.maps.LatLng({!! user()->latitude !!},{!!user()->longitude !!});
     }
     else{
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
     }
      
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#textAddres').val(results[0].formatted_address);
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
    });
       })
function initialize(){
    var map,myLatlng;
      if((document.getElementById('latitude_address').value != '') && (document.getElementById('longitude_address').value != '')){
         myLatlng = new google.maps.LatLng(document.getElementById('latitude_address').value,document.getElementById('longitude_address').value);
     }
     else if(({{user()->latitude}}!='') && ({{user()->longitude}} != '')){
         myLatlng = new google.maps.LatLng({!! user()->latitude !!},{!!user()->longitude !!});
     }
     else{
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
     }
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
        geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#textAddres').val(results[0].formatted_address);
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
    });
}

</script>

<script type="text/javascript">
    $( document ).ready(function() {
    $('#type').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'types',
        labelField: 'types',
        searchField: 'types',
        options: [
@forelse(trans('restaurant::restaurant.options.type') as $key => $types)

    {types: "{{$types}}" },
    @empty
    @endif
],
        create: function(input) {
            return {
                types: input
            }
        }
    });
});
</script>
