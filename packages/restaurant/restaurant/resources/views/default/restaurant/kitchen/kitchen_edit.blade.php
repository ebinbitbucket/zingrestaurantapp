<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a class="active" href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
            <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i>Monthly Statement</a>
            <a href="{{guard_url('expense/expense')}}"><i class="fa fa-sticky-note-o"></i>Invoice</a>
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>

<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                        <li >
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                                        </li>
                                         <li>
                                            <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-social-buffer"></i><span>Eatery Schedule</span></a>
                                        </li>
                                        <li class="active">
                                            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-social-buffer"></i><span>Kitchen</span></a>
                                        </li>
                                        <li>
                                             <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i>Monthly Statement</a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('expense/expense')}}"><i class="fa fa-sticky-note-o"></i><span>Invoice</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    {!!Form::vertical_open()
        ->id('kitchen-kitchen-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('kitchen/kitchen/'. $kitchen->getRouteKey()))!!}
                                        <div class="element-info">
                                            <div class="element-info-with-icon">
                                                <div class="element-info-icon">
                                                    <div class="icon ion-social-buffer"></div>
                                                </div>
                                                <div class="element-info-text">
                                                    <h5 class="element-inner-header">Edit Kitchen</h5>
                                                    <div class="element-inner-desc"></div>
                                                </div>
                                            </div>
                                        </div>
                                        @include('notifications')
                                        <div class="row">
                                            <div class="col-md-6">
                                                {!! Form::text('name')
                       -> label(trans('kitchen::kitchen.label.name'))
                       ->value($kitchen->name)
                       -> placeholder(trans('kitchen::kitchen.placeholder.name'))!!}
                                            </div>
                                              <div class="col-md-6">
                                                {!! Form::text('username')
                       -> label('Username')
                       ->value($kitchen->username)
                       -> placeholder('Enter Username')!!}
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::email('email')
                       -> label(trans('kitchen::kitchen.label.email'))
                       -> required()
                       ->value($kitchen->email)
                       -> placeholder(trans('kitchen::kitchen.placeholder.email'))!!}
                                            </div>
                                             <div class="col-md-6">
                                                {!! Form::password('password')
                       -> label(trans('kitchen::kitchen.label.password'))
                       -> required()
                       ->value($kitchen->password)
                       -> placeholder(trans('kitchen::kitchen.placeholder.password'))!!}
                                            </div>
                                            <div class="col-md-12">
                                               {!! Form::textarea ('description')
                    -> label(trans('kitchen::kitchen.label.description'))
                    ->value($kitchen->description)
                    -> placeholder(trans('kitchen::kitchen.placeholder.description'))!!}
                                            </div>
                                           </div>
                                           <input type="hidden" name="restaurant_id" value="{{user_id()}}">
                                        <div class="form-buttons-w text-center">
                                            <button class="btn btn-theme" type="submit" style="height: 30px; width: 150px;"> Update</button>
                                           <!--  <button class="btn btn-danger" type="button"> Cancel</button> -->
                                        </div>
                                   {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
 
