<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a class="active" href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                        <li >
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                                        </li>
                                         <li>
                                            <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-android-calendar"></i><span>Eatery Schedule</span></a>
                                        </li>
                                        <li class="active">
                                            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-android-home"></i><span>Kitchen</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="element-wrapper order-detail-wrap">
                                <div class="element-box">
                                    <div class="element-info">
                                        <div class="element-info-with-icon">
                                            <div class="element-info-icon"><div class="icon ion-person-stalker"></div></div>
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header">Kitchens</h5>
                                                <a href="{{guard_url('kitchen/restaurant/create')}}"><i class="ion-plus-circled"></i> Create Kitchen Login</a>
                                                <!-- <div class="element-inner-desc">Validation of the form is made possible using powerful validator plugin for bootstrap.</div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive element-table border-top-0">
                                        <table id="example" class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Email</th>
                                                    <th scope="col">Actions</th>    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users as $key => $val)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{$val->name}}</td>
                                                    <td>{{$val->email}}</td>
                                                    <td><a href="{{guard_url('kitchen/edit')}}/{{$val->slug}}" ><i class="fa fa-pencil"></i></a></td>
                                                     <?php
                                                        $parameter= Crypt::encrypt($val->id);
                                                    ?>
                                                    <td><a href="{{guard_url('kitchen/kitchen_dashboard')}}/{{$parameter}}" >Dashboard</a></td>
                                                </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
