<aside class="main-nav">
    <div class="nav-inner">
    @include('restaurant::default.restaurant.partial.mobile_menu')

    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                            <aside class="dashboard-sidemenu">
                            @include('restaurant::default.restaurant.partial.left_menu')

                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="element-wrapper order-detail-wrap">
                                <div class="element-box">
                                    <div class="element-info">
                                        <div class="element-info-with-icon">
                                            <div class="element-info-icon"><div class="icon ion-person-stalker"></div></div>
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header">Kitchens</h5>
                                                <a href="{{guard_url('kitchen/restaurant/create')}}"><i class="ion-plus-circled"></i> Create Kitchen Login</a>
                                                <!-- <div class="element-inner-desc">Validation of the form is made possible using powerful validator plugin for bootstrap.</div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-responsive element-table border-top-0">
                                        <table id="example" class="table">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Email</th>
                                                    <th scope="col">Actions</th>    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users as $key => $val)
                                                <tr>
                                                    <td>{{$key+1}}</td>
                                                    <td>{{$val->name}}</td>
                                                    <td>{{$val->email}}</td>
                                                    <td><a href="{{guard_url('kitchen/edit')}}/{{$val->slug}}" ><i class="fa fa-pencil"></i></a></td>
                                                     <?php
                                                        $parameter= Crypt::encrypt($val->id);
                                                    ?>
                                                    <td><a href="{{guard_url('kitchen/kitchen_dashboard')}}/{{$parameter}}" target="_blank">Dashboard</a></td>
                                                </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
