<div class="menu-item-details">
                                                            <div class="img-wrap">
                                                                <img src="{{url($menuItem->defaultImage('image'))}}" class="img-fluid" alt="">
                                                            </div>
                                                            <h3>{{$menuItem->name}}</h3>
                                                            <div class="price">$ {{$menuItem->price}}</div>
                                                            <p>{{$menuItem->description}}</p>
                                                        </div>
                                                        <h2 class="item-detail-sectitle d-flex align-items-center justify-content-between">Addons</h2>
                                                        <div class="menu-item-details">
                                                            <div class="addon-item-wrap">
                                                                @foreach($menuItem->addon as $addons)
                                                                <div class="addon-item">
                                                                    <p>{{$addons->name}}</p>
                                                                    <div class="addon-actions">
                                                                        <div class="custom-control custom-switch">
                                                                            <input onclick="addon_status('{{$menuItem->id}}','{{$addons->id}}')" type="checkbox" class="custom-control-input addon_status" id="addon_{{$addons->id}}"  {{Restaurant::getStockStatus($menuItem->id,$addons->id) == 1 ? 'checked':''}}>
                                                                            <label class="custom-control-label" for="addon_{{$addons->id}}">&nbsp;</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                        <h2 class="item-detail-sectitle d-flex align-items-center justify-content-between">Variations</h2>
                                                        <div class="menu-item-details">
                                                            <div class="addon-item-wrap">
                                                                @if(!empty($menuItem->menu_variations))
                                                               @foreach($menuItem->menu_variations  as $key => $variation)
                                                                <div class="addon-item">
                                                                    <p>{{$variation['name']}}<span>- ${{$variation['price']}}</span></p>
                                                                    <div class="addon-actions">
                                                                        <div class="custom-control custom-switch">
                                                                            <input onclick="variation_status('{{$menuItem->id}}','{{$key}}')" type="checkbox" class="custom-control-input variation_status" id="var_{{$key}}" @if(!empty($variation['stock_status'])) {{$variation['stock_status'] == 1 ? 'checked':''}} @endif>
                                                                            <label class="custom-control-label" for="var_{{$key}}">&nbsp;</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                    @endforeach
                                                                @endif
                                                                </div>
                                                            </div>
<script type="text/javascript">
    function addon_status(menu_id,addon_id){
        var fav_status = $('#addon_'+addon_id). prop("checked");
        if(fav_status == true)
            fav_status = 'on';
        else
            fav_status = 'off';
            $.getJSON({
                  type: 'GET',
                  url : "{{guard_url('restaurant/addon/status_update')}}"+'/'+menu_id+'/'+addon_id+'/'+fav_status,
                  success: function(data) { 
        //             if(fav_status == on)
        //      $('..category_status').addClass('active');
        // else
        //      $('..category_status').removeClass('active');
                   
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
     }
     function variation_status(menu_id,key){
        var fav_status = $('#var_'+key). prop("checked");
        if(fav_status == true)
            fav_status = 'on';
        else
            fav_status = 'off';
            $.getJSON({
                  type: 'GET',
                  url : "{{guard_url('restaurant/variation/status_update')}}"+'/'+menu_id+'/'+key+'/'+fav_status,
                  success: function(data) { 
        //             if(fav_status == on)
        //      $('..category_status').addClass('active');
        // else
        //      $('..category_status').removeClass('active');
                   
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
     }
</script>                                                            