<aside class="main-nav">
    <div class="nav-inner">
    @include('restaurant::default.restaurant.partial.mobile_menu')

   
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-xl-3 d-none">
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                      <!--   <li>
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li> -->
                                         <li class="active">
                                            <a href="{{guard_url('restaurant/kitchen_menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('cart/orders/kitchen_orders/new')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="element-wrapper menu-element-wrapper">
                                        <div class="element-box p-0">
                                            <div class="element-info">
                                                <div class="element-info-with-icon">
                                                    <div class="element-info-text">
                                                        <h5 class="element-inner-header">Menus</h5>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="menu-category-item-wrap">
                                                <div class="accordion" id="Menu_Accordion">
                                                    @foreach($categories as $category)
                                                    <div class="card">
                                                        <div class="card-header" id="cat_Sides">
                                                            <button class="btn" type="button" data-toggle="collapse" data-target="#cat{{$category->id}}" aria-expanded="true" aria-controls="{{$category->id}}">{{$category->name}}</button>
                                                            <div class="actions">
                                                                <div class="custom-control custom-switch">
                                                                    <input onclick="category_status('{{$category->id}}')" type="checkbox" class="custom-control-input category_status" id="cat_{{$category->id}}" {{$category->stock_status == 1 ? 'checked':''}}>
                                                                    <label class="custom-control-label" for="cat_{{$category->id}}">&nbsp;</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div id="cat{{$category->id}}" class="collapse" aria-labelledby="cat{{$category->id}}" data-parent="#Menu_Accordion">
                                                            <div class="card-body">
                                                                @forelse($category->menu as $menu_item)
                                                                                <div class="menu-item row">
                                                                                <div class="col-md-10">
                                                                                    <a href="#" class="menu-item-block" onclick="show_detailmenu('{{$menu_item->id}}')">
                                                                                        <div class="cell-img" style="background-image: url({{url($menu_item->defaultImage('image'))}})"></div>{{$menu_item->name}}
                                                                                    </a>
                                                                                    </div>
                                                                                    <div class="actions col-md-2">
                                                                                        <div class="custom-control custom-switch">
                                                                                            <input onclick="menu_status('{{$menu_item->id}}')" type="checkbox" class="custom-control-input menu_status" id="menu_{{$menu_item->id}}"  {{$menu_item->stock_status == 1 ? 'checked':''}}>
                                                                                            <label class="custom-control-label" for="menu_{{$menu_item->id}}">&nbsp;</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                    @empty
                                                                 No Menus added.
                                                                @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                   @endforeach <!--end of category --> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 add-edit-wrap">
                                    <div class="row position-relative">
                                        <div class="col-md-12">
                                            <div class="element-wrapper menu-element-item-wrapper">
                                                <div class="element-box">  
                                                    <div class="empty-msg">
                                                        <p>Please click one menu to view it's details</p>
                                                    </div>  
                                                    <div class="menu-element-item-detail-block" id="menuItem">
                                                        
                                                        </div>
                                                    </div>                                       
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
<script type="text/javascript">
    function show_detailmenu(menu_id){ 
       $(".menu-element-item-wrapper, .menu-element-edit-wrapper").show();

    $("#menuItem").css('display','block');
    $.get("{{guard_url('restaurant/kitchen_menu_items/menu/detailform')}}/"+menu_id, function(data) { $('#menuItem').html(data); });  
       

}
  function category_status(category_id){
        var fav_status = $('#cat_'+category_id). prop("checked");
        if(fav_status == true)
            fav_status = 'on';
        else
            fav_status = 'off';

            $.getJSON({
                  type: 'GET',
                  url : "{{guard_url('restaurant/category/status_update')}}"+'/'+category_id+'/'+fav_status,
                  success: function(data) { 
        //             if(fav_status == on)
        //      $('..category_status').addClass('active');
        // else
        //      $('..category_status').removeClass('active');
                   
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
     }
      function menu_status(menu_id){
        var fav_status = $('#menu_'+menu_id). prop("checked");
        if(fav_status == true)
            fav_status = 'on';
        else
            fav_status = 'off';
            $.getJSON({
                  type: 'GET',
                  url : "{{guard_url('restaurant/menu/status_update')}}"+'/'+menu_id+'/'+fav_status,
                  success: function(data) { 
        //             if(fav_status == on)
        //      $('..category_status').addClass('active');
        // else
        //      $('..category_status').removeClass('active');
                   
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
     }
</script>