
                                                    @foreach(Restaurant::getMenuAddons($edit_menu_addon->id,$edit_addon->id) as $addon_dets)

                                                    <h3 class="d-none d-md-block">Addon {{$edit_addon->name}}</h3>
                                                        {!!Form::vertical_open()
                                                        ->id('restaurant-menu-addon-edit')
                                                        ->method('PUT')
                                                        ->enctype('multipart/form-data')
                                                        ->addClass('compact-edit')
                                                        ->action(guard_url('restaurant/menu/'. $edit_menu_addon->getRouteKey()))!!}
                                                            <input type="hidden" name="addon_ids[]" id="addons_{{$edit_menu_addon->id}}" value="{{$edit_addon->id}}">
                                                            <div class="form-group">
                                                                <label for="title">Name</label>
                                                                {!! Form::text('addon_name')
                                  ->id('addon_id')
                                  ->value($edit_addon->name)
                                  ->disabled()
                                  -> label('')!!}
                                                                
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group">
                                                                        <label for="">Min</label>
                                                                        {!! Form::text('av_'.$addon_dets->addon_id.'[min]')
                              -> label('')
                              ->value($addon_dets->min)
                              -> required()
                              !!}
                                                                       
                                                                    </div>
                                                                </div>
                                                                <div class="col">
                                                                    <div class="form-group">
                                                                        <label for="">Max</label>
                                                                        {!! Form::text('av_'.$addon_dets->addon_id.'[max]')
                              -> label('')
                              ->value($addon_dets->max)
                              -> required()
                              !!}
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group border-0">
                                                                        <div class="custom-control custom-checkbox">
                                                                          

                                                                            <input type="checkbox" class="custom-control-input" name="av_{{$addon_dets->addon_id}}[required]" id="customCheck{{$addon_dets->id}}" value="Yes" {{$addon_dets->required == 'Yes' ? 'checked':'' }}>
                                                                            <label class="custom-control-label" for="customCheck{{$addon_dets->id}}">Required</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <b>Addon Variations</b></br> 
                                                            @foreach(json_decode($addon_dets->variation_list) as $key => $variation_edit)
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group border-0">
                                                                        <div class="custom-control custom-checkbox">

                                                                            <input type="checkbox" class="custom-control-input" name="av_{{$addon_dets->addon_id}}[variation_list][{{$key}}][name]" value="{!! $variation_edit->name !!}" onclick="enableVariation(this,'{{$addon_dets->addon_id}}','{{$key}}','{{$edit_menu_addon->id}}')" id="customCheck_{{$addon_dets->id}}_{{$key}}" checked="true">
                                                                            <label class="custom-control-label" for="customCheck_{{$addon_dets->id}}_{{$key}}">{!!$variation_edit->name !!}</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col">
                                                                     <div class="form-group">
                                                                        {!! Form::number('av_'.$addon_dets->addon_id.'[variation_list]['.$key.'][price]')
                              ->id($edit_menu_addon->id.'_'.$addon_dets->addon_id.'_'.$key.'_price')
                              -> label('')
                              ->value($variation_edit->price)
                              -> placeholder('Price')
                              -> required()
                              !!}

                                 {!! Form::hidden('av_'.$addon_dets->addon_id.'[variation_list]['.$key.'][id]')
                              ->id($edit_menu_addon->id.'_'.$addon_dets->addon_id.'_'.$key.'_id')
                              -> label('ID')
                              ->value($variation_edit->id)
                              -> placeholder('Please enter ID')
                              -> required()
                              !!}

                              {!! Form::hidden('av_'.$addon_dets->addon_id.'[variation_list]['.$key.'][description]')
                              ->id($edit_menu_addon->id.'_'.$addon_dets->addon_id.'_'.$key.'_desc')
                              -> label('Description')
                              ->value($variation_edit->description)
                              -> placeholder('Please enter Description')
                              -> required()
                              !!}
                                                                       
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @endforeach
                                                             
                                                            <div class="form-group border-0"><button type="button" id="btn_menuaddon_edit" data-menu="{{$edit_menu_addon->id}}" data-addon="{{$edit_addon->id}}" class="btn btn-theme">Update Addon</button></div>
                                                        {!!Form::close()!!}
                                                        @endforeach