 @if(!empty($editmenuvar->menu_variations))
 @foreach($editmenuvar->menu_variations as $keyd => $edit_varat)
 @if($key == $keyd)
 <h3>Edit Variation {{$edit_varat['name']}}</h3>
                                                        {!!Form::vertical_open()
        ->id('restaurant-menu-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->addClass('compact-edit')
        ->action(guard_url('restaurant/menu/'. $editmenuvar->getRouteKey()))!!}

        @foreach($editmenuvar->menu_variations as $keys => $oldmenu_varat) 
            <input type='hidden' name="menu_variations[{{$keys}}][name]" value="{{$oldmenu_varat['name']}}">
            <input type='hidden' name="menu_variations[{{$keys}}][price]" value="{{$oldmenu_varat['price']}}">
            <input type='hidden' name="menu_variations[{{$keys}}][description]" value="{{$oldmenu_varat['description']}}">
        @endforeach
                                                            <div class="row">
                                                                <div class="col-sm-8">
                                                                    <div class="form-group">
                                                                        <label for="">Name</label>
                                                                        <input type="text" name="menu_variations[{{$keyd}}][name]" class="form-control" placeholder="" value="{{$edit_varat['name']}}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group">
                                                                        <label for="">Price</label>
                                                                       <input type="text" name="menu_variations[{{$keyd}}][price]" class="form-control" id="price"  placeholder="" value=" {{($edit_varat['price'])}}" required="required">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="">Description</label>
                                                                <textarea class="form-control" name="menu_variations[{{$keyd}}][description]" rows="2">{{$edit_varat['description']}}</textarea>
                                                            </div>
                                                            <div class="form-group border-0"><button type="button" class="btn btn-theme" id="btn_edit_menuvariation" data-menu="{{$editmenuvar->id}}" data-key="{{$keyd}}">Update Variation</button></div>
                                                       {!!Form::close()!!}
                                                       @endif
 @endforeach
                                                  @endif                                                       