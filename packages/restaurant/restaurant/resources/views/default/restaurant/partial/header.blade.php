<header class="app-header">
    <div class="eatery-logo">
        <a href="{{guard_url('/')}}">
        @php
            $restaurant = user();
        @endphp
        <img src="{{ url(@$restaurant->defaultImage('logo')) }}">
    </a>
    </div>
    <div class="zing-logo">
        <a href="{{url('/')}}"><img src="{{theme_asset('img/logo-round.png')}}" alt=""></a>
    </div>
</header>