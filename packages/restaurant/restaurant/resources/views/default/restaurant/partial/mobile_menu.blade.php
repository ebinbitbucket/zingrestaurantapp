
    @if (Session::has('login_from_kitchen'))
    <div class="links-wrap">
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/settings')}}"><i class="icon ion-settings"></i>On-Off Kitchen</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu Status</a>
      
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
       
        </div>
        @else
   <div class="links-wrap">
            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/settings')}}"><i class="icon ion-settings"></i>Settings</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/menu-item-status')}}"><i class="icon ion-android-restaurant"></i>Menu Status</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
            <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i>Statement</a>
       
        </div>
        @endif
        <script>
$(document).ready(function () {
    var current = window.location.pathname.split("/").pop()
    $('.links-wrap a').each(function(){
        var $this = $(this);
        if($this.attr('href').split("/").pop()==current){
        var $parent = $(this);
        $parent.addClass('active');        }
    })

    $('.links-wrap  a').click(function() {

        $('.links-wrap a.active').removeClass('active');

        var $parent = $(this);
        $parent.addClass('active');
      //  e.preventDefault();
    });

});

</script>