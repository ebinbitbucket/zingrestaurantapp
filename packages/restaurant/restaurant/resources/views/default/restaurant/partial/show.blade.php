            <div class="content">
                <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('restaurant::restaurant.label.id') !!}
                </label><br />
                    {!! $restaurant['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('restaurant::restaurant.label.name') !!}
                </label><br />
                    {!! $restaurant['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="description">
                    {!! trans('restaurant::restaurant.label.description') !!}
                </label><br />
                    {!! $restaurant['description'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="address">
                    {!! trans('restaurant::restaurant.label.address') !!}
                </label><br />
                    {!! $restaurant['address'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="phone">
                    {!! trans('restaurant::restaurant.label.phone') !!}
                </label><br />
                    {!! $restaurant['phone'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="email">
                    {!! trans('restaurant::restaurant.label.email') !!}
                </label><br />
                    {!! $restaurant['email'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="price_range">
                    {!! trans('restaurant::restaurant.label.price_range') !!}
                </label><br />
                    {!! $restaurant['price_range'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="country">
                    {!! trans('restaurant::restaurant.label.country') !!}
                </label><br />
                    {!! $restaurant['country'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="city">
                    {!! trans('restaurant::restaurant.label.city') !!}
                </label><br />
                    {!! $restaurant['city'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="state">
                    {!! trans('restaurant::restaurant.label.state') !!}
                </label><br />
                    {!! $restaurant['state'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="location">
                    {!! trans('restaurant::restaurant.label.location') !!}
                </label><br />
                    {!! $restaurant['location'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="zipcode">
                    {!! trans('restaurant::restaurant.label.zipcode') !!}
                </label><br />
                    {!! $restaurant['zipcode'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="latitude">
                    {!! trans('restaurant::restaurant.label.latitude') !!}
                </label><br />
                    {!! $restaurant['latitude'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="longitude">
                    {!! trans('restaurant::restaurant.label.longitude') !!}
                </label><br />
                    {!! $restaurant['longitude'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="rating">
                    {!! trans('restaurant::restaurant.label.rating') !!}
                </label><br />
                    {!! $restaurant['rating'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="logo">
                    {!! trans('restaurant::restaurant.label.logo') !!}
                </label><br />
                    {!! $restaurant['logo'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="gallery">
                    {!! trans('restaurant::restaurant.label.gallery') !!}
                </label><br />
                    {!! $restaurant['gallery'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="delivery">
                    {!! trans('restaurant::restaurant.label.delivery') !!}
                </label><br />
                    {!! $restaurant['delivery'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="type">
                    {!! trans('restaurant::restaurant.label.type') !!}
                </label><br />
                    {!! $restaurant['type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('restaurant::restaurant.label.created_at') !!}
                </label><br />
                    {!! $restaurant['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('restaurant::restaurant.label.deleted_at') !!}
                </label><br />
                    {!! $restaurant['deleted_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('restaurant::restaurant.label.updated_at') !!}
                </label><br />
                    {!! $restaurant['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="working_hours">
                    {!! trans('restaurant::restaurant.label.working_hours') !!}
                </label><br />
                    {!! $restaurant['working_hours'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('restaurant::restaurant.label.name'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('description')
                    -> label(trans('restaurant::restaurant.label.description'))
                    -> placeholder(trans('restaurant::restaurant.placeholder.description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('address')
                    -> label(trans('restaurant::restaurant.label.address'))
                    -> placeholder(trans('restaurant::restaurant.placeholder.address'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('phone')
                       -> label(trans('restaurant::restaurant.label.phone'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.phone'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::email('email')
                       -> label(trans('restaurant::restaurant.label.email'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('price_range')
                    -> options(trans('restaurant::restaurant.options.price_range'))
                    -> label(trans('restaurant::restaurant.label.price_range'))
                    -> placeholder(trans('restaurant::restaurant.placeholder.price_range'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('country')
                       -> label(trans('restaurant::restaurant.label.country'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.country'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('city')
                       -> label(trans('restaurant::restaurant.label.city'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.city'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('state')
                       -> label(trans('restaurant::restaurant.label.state'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.state'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('location')
                       -> label(trans('restaurant::restaurant.label.location'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.location'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('zipcode')
                       -> label(trans('restaurant::restaurant.label.zipcode'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.zipcode'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('latitude')
                       -> label(trans('restaurant::restaurant.label.latitude'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.latitude'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('longitude')
                       -> label(trans('restaurant::restaurant.label.longitude'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.longitude'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('rating')
                       -> label(trans('restaurant::restaurant.label.rating'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.rating'))!!}
                </div>
            </div>