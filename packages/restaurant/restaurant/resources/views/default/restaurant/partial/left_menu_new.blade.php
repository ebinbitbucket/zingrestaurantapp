
<div class="app-side-nav">
    <ul>
        <li>
            <a class="nav-item {{ Request::is('restaurant/restaurant/accounts') ? 'active' : '' }}" href="{{guard_url('restaurant/accounts')}}">
                <div>
                    <i class="icon flaticon-shop"></i>
                    <span>Account Info</span>
                </div>
            </a>
        </li>
        <li>
            <a class="nav-item {{ Request::is('restaurant/restaurant/menu') || Request::is('restaurant/restaurant/menu-setup') || Request::is('restaurant/restaurant/menu-item-status') || Request::is('restaurant/restaurant/restaurant_addons') ? 'active' : '' }}" href="{{guard_url('restaurant/menu')}}">
                <div>
                    <i class="icon flaticon-cutlery"></i>
                    <span>Menu</span>
                </div>
            </a>
        </li>
        <li>
            <a class="nav-item {{ Request::is('restaurant/cart/restaurant/orders') ? 'active' : '' }}" href="{{guard_url('cart/restaurant/orders')}}">
                <div>
                    <i class="icon flaticon-shopping-cart"></i>
                    <span>Orders</span>
                </div>
            </a>
        </li>
        <li>
            <a class="nav-item {{ Request::is('restaurant/restaurant/view-settings') ? 'active' : '' }}" href="{{guard_url('restaurant/view-settings')}}">
                <div>
                    <i class="icon flaticon-settings"></i>
                    <span>Settings</span>
                </div>
            </a>
        </li>
        <li>
            <a class="nav-item {{ Request::is('restaurant/restaurant/view-billing') || Request::is('restaurant/restaurant/billing') ? 'active' : '' }}" href="{{guard_url('restaurant/view-billing')}}">
                <div>
                    <i class="icon flaticon-wallet"></i>
                    <span>Billing</span>
                </div>
            </a>
        </li>
        <li>
            <a class="nav-item {{ Request::is('restaurant/restaurant/view-website-setup') || Request::is('restaurant/restaurant/promotions')|| Request::is('restaurant/restaurant/website') ? 'active' : '' }}" href="{{guard_url('restaurant/view-website-setup')}}">
                <div>
                    <i class="icon flaticon-responsive"></i>
                    <span>Website</span>
                </div>
            </a>
        </li>
        <li>
            <a class="nav-item {{ Request::is('restaurant/restaurant/appsettings') || Request::is('restaurant/restaurant/push-notifications') || Request::is('restaurant/restaurant/app-setup') ? 'active' : '' }}" href="{{guard_url('restaurant/appsettings')}}">
                <div>
                    <i class="icon flaticon-smartphone"></i>
                    <span>App</span>
                </div>
            </a>
        </li>
        <li>
            <a class="nav-item {{ Request::is('restaurant/restaurant/marketing-hub') ? 'active' : '' }}" href="{{guard_url('restaurant/marketing-hub')}}">
                <div>
                    <i class="icon flaticon-megaphone"></i>
                    <span>Marketing Hub</span>
                </div>
            </a>
        </li>
    </ul>    
</div> 