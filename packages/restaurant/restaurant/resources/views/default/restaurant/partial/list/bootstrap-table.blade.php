            <table class="table" id="main-table" data-url="{!!guard_url('restaurant/restaurant?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="name">{!! trans('restaurant::restaurant.label.name')!!}</th>
                    <th data-field="phone">{!! trans('restaurant::restaurant.label.phone')!!}</th>
                    <th data-field="email">{!! trans('restaurant::restaurant.label.email')!!}</th>
                    <th data-field="price_range">{!! trans('restaurant::restaurant.label.price_range')!!}</th>
                    <th data-field="rating">{!! trans('restaurant::restaurant.label.rating')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>