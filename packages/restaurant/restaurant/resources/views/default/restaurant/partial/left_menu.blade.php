@if (Session::has('login_from_kitchen'))
<nav class="sidebar-nav">
                        <ul>
                            <li class="nav-head"><span class="head">Navigation</span></li>
                            
                            <li>
                                <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                            </li> 
                            <li >
                                <a href="{{guard_url('restaurant/settings')}}"><i class="icon ion-settings"></i><span> On-Off Kitchen</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu Status</span></a>
                            </li>
                            
                            <li>
                                <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-android-calendar"></i><span>Eatery Schedule</span></a>
                            </li>
                            
                        </ul>
                    </nav>
                    @else

                    <nav class="sidebar-nav">
                        <ul>
                            <li class="nav-head"><span class="head">Navigation</span></li>
                            <li>
                                <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                            </li>
                            <li >
                                <a href="{{guard_url('restaurant/settings')}}"><i class="icon ion-settings"></i><span> Settings</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/menu-item-status')}}"><i class="icon ion-android-restaurant"></i><span>Menu Status</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                            </li>
                             <li>
                                <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-android-calendar"></i><span>Eatery Schedule</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-android-home"></i><span>Kitchen</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('cart/orders/monthstmnt')}}"><i class="ion-android-clipboard"></i><span>Statement</span></a>
                            </li>
                            <li>
                                <a href="{{guard_url('restaurant/website')}}"><i class="ion-android-clipboard"></i><span>Website</span></a>
                            </li>
                           <li>
                                <a href="{{guard_url('restaurant/appdetails')}}"><i class="ion-android-clipboard"></i><span>App</span></a>
                            </li>
                        </ul>
                    </nav>
                    @endif

<script>
$(document).ready(function () {
    var current = window.location.pathname.split("/").pop()
    $('.sidebar-nav li a').each(function(){
        var $this = $(this);
        if($this.attr('href').split("/").pop()==current){
        var $parent = $(this).parent();
        $parent.addClass('active');        }
    })

    $('.sidebar-nav li a').click(function() {

        $('.sidebar-nav li.active').removeClass('active');

        var $parent = $(this).parent();
        $parent.addClass('active');
      //  e.preventDefault();
    });

});

</script>
