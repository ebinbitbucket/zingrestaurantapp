            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('restaurant::restaurant.label.name'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('description')
                    -> label(trans('restaurant::restaurant.label.description'))
                    -> placeholder(trans('restaurant::restaurant.placeholder.description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('address')
                    -> label(trans('restaurant::restaurant.label.address'))
                    -> placeholder(trans('restaurant::restaurant.placeholder.address'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('phone')
                       -> label(trans('restaurant::restaurant.label.phone'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.phone'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::email('email')
                       -> label(trans('restaurant::restaurant.label.email'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('price_range')
                    -> options(trans('restaurant::restaurant.options.price_range'))
                    -> label(trans('restaurant::restaurant.label.price_range'))
                    -> placeholder(trans('restaurant::restaurant.placeholder.price_range'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('country')
                       -> label(trans('restaurant::restaurant.label.country'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.country'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('city')
                       -> label(trans('restaurant::restaurant.label.city'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.city'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('state')
                       -> label(trans('restaurant::restaurant.label.state'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.state'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('location')
                       -> label(trans('restaurant::restaurant.label.location'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.location'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('zipcode')
                       -> label(trans('restaurant::restaurant.label.zipcode'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.zipcode'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('latitude')
                       -> label(trans('restaurant::restaurant.label.latitude'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.latitude'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('longitude')
                       -> label(trans('restaurant::restaurant.label.longitude'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.longitude'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('rating')
                       -> label(trans('restaurant::restaurant.label.rating'))
                       -> placeholder(trans('restaurant::restaurant.placeholder.rating'))!!}
                </div>
            </div>