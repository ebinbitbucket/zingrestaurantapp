@foreach($categories as $category)
                                                    <div class="card">
                                                        <div class="card-header" id="cat_{{$category->id}}">
                                                            <button class="btn" type="button" data-toggle="collapse" data-target="#cat{{$category->id}}" aria-expanded="true" aria-controls="{{$category->id}}">{{$category->name}}</button>
                                                            <div class="actions">
                                                                <a href="#" class="edit-btn" onclick="updatecount('{{$category->id}}')"><i class="flaticon-edit"></i></a>
                                                                <form id="delete-form_{!! $category->getRouteKey() !!}" style="display: inline-block;" method="POST" action="{!! guard_url('restaurant/category') !!}/{!! $category->getRouteKey() !!}">
                                              {{csrf_field()}}{{method_field('DELETE')}}
                                             <button type="button" class="btn_delete_category" data-key="{!! $category->getRouteKey() !!}" onclick="deletecategory('{!! $category->getRouteKey() !!}')" style="border: none; background: none; padding: 0;"><i class="flaticon-garbage"></i></button></form>
                                             <!-- 
                                                                <a data-action="DELETE" href="{{ guard_url('restaurant/category') }}/{{$category->getRouteKey()}}"><i class="flaticon-garbage"></i></a> -->
                                                            </div>
                                                        </div>
                                                        <div id="cat{{$category->id}}" class="collapse" aria-labelledby="cat_{{$category->id}}" data-parent="#Menu_Accordion">
                                                            <div class="card-body">
                                                                @forelse($category->menu as $menu_item)
                                                                <div class="menu-item">
                                                                    <a href="#" class="menu-item-block" onclick="show_detailmenu('{{$menu_item->id}}')">
                                                                        <div class="cell-img" style="background-image: url({{url($menu_item->defaultImage('image'))}})"></div>{{$menu_item->name}}
                                                                    </a>
                                                                </div>
                                                                @empty
                                                                 No Menus added.
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                   @endforeach