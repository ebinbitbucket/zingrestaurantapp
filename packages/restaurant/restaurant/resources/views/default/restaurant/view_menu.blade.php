@include('restaurant::default.restaurant.partial.header')

<div class="app-content-wrap">
    @include('restaurant::default.restaurant.partial.left_menu_new')
   
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon mb-20">
                <i class="flaticon-cutlery app-sec-title-icon"></i>
                <h1>Menu</h1>
                <a href="index.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <a class="link-widget mb-20" href="{{ guard_url('restaurant/menu-setup') }}">
                        <i class="flaticon-burger"></i>
                        <h4>Menu Set Up</h4>
                        <p>Change Prices, Pictures, Menu information, Menu Schedule and more here.</p>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="link-widget mb-20" href="{{ guard_url('restaurant/menu-item-status') }}">
                        <i class="flaticon-restaurant-menu"></i>
                        <h4>Menu Status</h4>
                        <p>Easily turn on/off unavailable menu items.</p>
                    </a>
                </div>
                <div class="col-md-4">
                    <a class="link-widget mb-20" href="{{ guard_url('restaurant/restaurant_addons') }}">
                        <i class="flaticon-drink"></i>
                        <h4>Add Ons</h4>
                        <p>Create additional choices for the menu item eg. spice level, tortilla type, salsa choice, People also ordered etc.</p>
                    </a>
                </div>
            </div>  
        </div>
    </div>
</div>