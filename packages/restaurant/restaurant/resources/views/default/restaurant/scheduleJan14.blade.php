
<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i>Account Details</a>
            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i>Addons</a>
            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i>Orders</a>
            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i>Eatery Details</a>
            <a class="active" href="{{guard_url('restaurant/schedule')}}"><i class="ion-android-calendar"></i>Eatery Schedule</a>
            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="ion-android-home"></i>Kitchen</a>
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                        <li >
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                                        </li>
                                         <li class="active">
                                            <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-android-calendar"></i><span>Eatery Schedule</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-android-home"></i><span>Kitchen</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="element-wrapper">
                                <div class="element-box">
                                    {!!Form::vertical_open()
        ->id('restaurant-restaurant-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->addClass('compact-edit p-0')
        ->action(guard_url('restaurant/restaurant/'. user()->getRouteKey()))!!}
                                        <div class="element-info">
                                            <div class="element-info-with-icon">
                                                <div class="element-info-icon">
                                                    <div class="icon ion-social-buffer"></div>
                                                </div>
                                                <div class="element-info-text">
                                                    <h5 class="element-inner-header">Eatery Hours</h5>
                                                    <div class="element-inner-desc"></div>
                                                </div>
                                            </div>
                                        </div>
                                        @include('notifications')
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="d-flex">
                                                    <p class="m-0 mr-10">Hours*</p>
                                                    <div class="custom-control custom-checkbox">
                                                      <input type="checkbox" class="custom-control-input" id="apply_all">
                                                      <label class="custom-control-label" for="apply_all">Apply all</label>
                                                    </div>
                                                </div>
                                            
                                            <div class="mb-10"> 
                                            <div class="row">
        <div class='col-md-3'>
          <label>Day</label>
        </div>
        <div class='col-md-3'>
          <label>Opening</label>
        </div>
        <div class='col-md-3'>
          <label>Ending</label>
        </div>
    </div>
</div>
              @forelse($restaurant_timings as $key=>$value)
              <div class="mb-10">

          <div class='row'>
                  <div class='col-md-3 col-sm-12'>
                          
                           <input class="form-control" type="text" name="day" value="{{ucfirst($value->day)}}" readonly>
                  </div>
                  <div class="col-md-3 col-sm-12">
                          
                          <select class="form-control" id="start_{{$value->day}}" name="timings[{{$value->day}}][{{$value->id}}][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00" {{ $value->opening =='00:00' ? ' selected' : '' }}>12:00 AM</option>
                                    <option value="00:30"  {{ $value->opening =='00:30' ? ' selected' : '' }}>12:30 AM</option>
                                    <option value="01:00"  {{ $value->opening =='01:00' ? ' selected' : '' }}>01:00 AM</option>
                                    <option value="01:30"  {{ $value->opening =='01:30' ? ' selected' : '' }}>01:30 AM</option>
                                    <option value="02:00"  {{ $value->opening =='02:00' ? ' selected' : '' }}>02:00 AM</option>
                                    <option value="02:30"  {{ $value->opening =='02:30' ? ' selected' : '' }}>02:30 AM</option>
                                    <option value="03:00"  {{ $value->opening =='03:00' ? ' selected' : '' }}>03:00 AM</option>
                                    <option value="03:30"  {{ $value->opening =='03:30' ? ' selected' : '' }}>03:30 AM</option>
                                    <option value="04:00"  {{ $value->opening =='04:00' ? ' selected' : '' }}>04:00 AM</option>
                                    <option value="04:30"  {{ $value->opening =='04:30' ? ' selected' : '' }}>04:30 AM</option>
                                    <option value="05:00"  {{ $value->opening =='05:00' ? ' selected' : '' }}>05:00 AM</option>
                                    <option value="05:30"  {{ $value->opening =='05:30' ? ' selected' : '' }}>05:30 AM</option>
                                    <option value="06:00"  {{ $value->opening =='06:00' ? ' selected' : '' }}>06:00 AM</option>
                                    <option value="06:30"  {{ $value->opening =='06:30' ? ' selected' : '' }}>06:30 AM</option>
                                    <option value="07:00"  {{ $value->opening =='07:00' ? ' selected' : '' }}>07:00 AM</option>
                                    <option value="07:30"  {{ $value->opening =='07:30' ? ' selected' : '' }}>07:30 AM</option>
                                    <option value="08:00"  {{ $value->opening =='08:00' ? ' selected' : '' }}>08:00 AM</option>
                                    <option value="08:30"  {{ $value->opening =='08:30' ? ' selected' : '' }}>08:30 AM</option>
                                    <option value="09:00"  {{ $value->opening =='09:00' ? ' selected' : '' }}>09:00 AM</option>
                                    <option value="09:30"  {{ $value->opening =='09:30' ? ' selected' : '' }}>09:30 AM</option>
                                    <option value="10:00"  {{ $value->opening =='10:00' ? ' selected' : '' }}>10:00 AM</option>
                                    <option value="10:30"  {{ $value->opening =='10:30' ? ' selected' : '' }}>10:30 AM</option>
                                    <option value="11:00"  {{ $value->opening =='11:00' ? ' selected' : '' }}>11:00 AM</option>
                                    <option value="11:30"  {{ $value->opening =='11:30' ? ' selected' : '' }}>11:30 AM</option>
                                    <option value="12:00"  {{ $value->opening =='12:00' ? ' selected' : '' }}>12:00 PM</option>
                                    <option value="12:30"  {{ $value->opening =='12:30' ? ' selected' : '' }}>12:30 PM</option>
                                    <option value="13:00"  {{ $value->opening =='13:00' ? ' selected' : '' }}>01:00 PM</option>
                                    <option value="13:30"  {{ $value->opening =='13:30' ? ' selected' : '' }}>01:30 PM</option>
                                    <option value="14:00"  {{ $value->opening =='14:00' ? ' selected' : '' }}>02:00 PM</option>
                                    <option value="14:30"  {{ $value->opening =='14:30' ? ' selected' : '' }}>02:30 PM</option>
                                    <option value="15:00"  {{ $value->opening =='15:00' ? ' selected' : '' }}>03:00 PM</option>
                                    <option value="15:30"  {{ $value->opening =='15:30' ? ' selected' : '' }}>03:30 PM</option>
                                    <option value="16:00"  {{ $value->opening =='16:00' ? ' selected' : '' }}>04:00 PM</option>
                                    <option value="16:30"  {{ $value->opening =='16:30' ? ' selected' : '' }}>04:30 PM</option>
                                    <option value="17:00"  {{ $value->opening =='17:00' ? ' selected' : '' }}>05:00 PM</option>
                                    <option value="17:30"  {{ $value->opening =='17:30' ? ' selected' : '' }}>05:30 PM</option>
                                    <option value="18:00"  {{ $value->opening =='18:00' ? ' selected' : '' }}>06:00 PM</option>
                                    <option value="18:30"  {{ $value->opening =='18:30' ? ' selected' : '' }}>06:30 PM</option>
                                    <option value="19:00"  {{ $value->opening =='19:00' ? ' selected' : '' }}>07:00 PM</option>
                                    <option value="19:30"  {{ $value->opening =='19:30' ? ' selected' : '' }}>07:30 PM</option>
                                    <option value="20:00"  {{ $value->opening =='20:00' ? ' selected' : '' }}>08:00 PM</option>
                                    <option value="20:30"  {{ $value->opening =='20:30' ? ' selected' : '' }}>08:30 PM</option>
                                    <option value="21:00"  {{ $value->opening =='21:00' ? ' selected' : '' }}>09:00 PM</option>
                                    <option value="21:30"  {{ $value->opening =='21:30' ? ' selected' : '' }}>09:30 PM</option>
                                    <option value="22:00"  {{ $value->opening =='22:00' ? ' selected' : '' }}>10:00 PM</option>
                                    <option value="22:30"  {{ $value->opening =='22:30' ? ' selected' : '' }}>10:30 PM</option>
                                    <option value="23:00"  {{ $value->opening =='23:00' ? ' selected' : '' }}>11:00 PM</option>
                                    <option value="23:30"  {{ $value->opening =='23:30' ? ' selected' : '' }}>11:30 PM</option>
                                    <option value="23:59"  {{ $value->opening =='23:59' ? ' selected' : '' }}>11:59 PM</option>


                                </select>
                                
                  </div>
                  <div class='col-md-3 col-sm-12'>
                          

                          <select class="form-control time_add" id="end_{{$value->day}}" name="timings[{{$value->day}}][{{$value->id}}][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00" {{ $value->closing =='00:00' ? ' selected' : '' }}>12:00 AM</option>
                                    <option value="00:30"  {{ $value->closing =='00:30' ? ' selected' : '' }}>12:30 AM</option>
                                    <option value="01:00"  {{ $value->closing =='01:00' ? ' selected' : '' }}>01:00 AM</option>
                                    <option value="01:30"  {{ $value->closing =='01:30' ? ' selected' : '' }}>01:30 AM</option>
                                    <option value="02:00"  {{ $value->closing =='02:00' ? ' selected' : '' }}>02:00 AM</option>
                                    <option value="02:30"  {{ $value->closing =='02:30' ? ' selected' : '' }}>02:30 AM</option>
                                    <option value="03:00"  {{ $value->closing =='03:00' ? ' selected' : '' }}>03:00 AM</option>
                                    <option value="03:30"  {{ $value->closing =='03:30' ? ' selected' : '' }}>03:30 AM</option>
                                    <option value="04:00"  {{ $value->closing =='04:00' ? ' selected' : '' }}>04:00 AM</option>
                                    <option value="04:30"  {{ $value->closing =='04:30' ? ' selected' : '' }}>04:30 AM</option>
                                    <option value="05:00"  {{ $value->closing =='05:00' ? ' selected' : '' }}>05:00 AM</option>
                                    <option value="05:30"  {{ $value->closing =='05:30' ? ' selected' : '' }}>05:30 AM</option>
                                    <option value="06:00"  {{ $value->closing =='06:00' ? ' selected' : '' }}>06:00 AM</option>
                                    <option value="06:30"  {{ $value->closing =='06:30' ? ' selected' : '' }}>06:30 AM</option>
                                    <option value="07:00"  {{ $value->closing =='07:00' ? ' selected' : '' }}>07:00 AM</option>
                                    <option value="07:30"  {{ $value->closing =='07:30' ? ' selected' : '' }}>07:30 AM</option>
                                    <option value="08:00"  {{ $value->closing =='08:00' ? ' selected' : '' }}>08:00 AM</option>
                                    <option value="08:30"  {{ $value->closing =='08:30' ? ' selected' : '' }}>08:30 AM</option>
                                    <option value="09:00"  {{ $value->closing =='09:00' ? ' selected' : '' }}>09:00 AM</option>
                                    <option value="09:30"  {{ $value->closing =='09:30' ? ' selected' : '' }}>09:30 AM</option>
                                    <option value="10:00"  {{ $value->closing =='10:00' ? ' selected' : '' }}>10:00 AM</option>
                                    <option value="10:30"  {{ $value->closing =='10:30' ? ' selected' : '' }}>10:30 AM</option>
                                    <option value="11:00"  {{ $value->closing =='11:00' ? ' selected' : '' }}>11:00 AM</option>
                                    <option value="11:30"  {{ $value->closing =='11:30' ? ' selected' : '' }}>11:30 AM</option>
                                    <option value="12:00"  {{ $value->closing =='12:00' ? ' selected' : '' }}>12:00 PM</option>
                                    <option value="12:30"  {{ $value->closing =='12:30' ? ' selected' : '' }}>12:30 PM</option>
                                    <option value="13:00"  {{ $value->closing =='13:00' ? ' selected' : '' }}>01:00 PM</option>
                                    <option value="13:30"  {{ $value->closing =='13:30' ? ' selected' : '' }}>01:30 PM</option>
                                    <option value="14:00"  {{ $value->closing =='14:00' ? ' selected' : '' }}>02:00 PM</option>
                                    <option value="14:30"  {{ $value->closing =='14:30' ? ' selected' : '' }}>02:30 PM</option>
                                    <option value="15:00"  {{ $value->closing =='15:00' ? ' selected' : '' }}>03:00 PM</option>
                                    <option value="15:30"  {{ $value->closing =='15:30' ? ' selected' : '' }}>03:30 PM</option>
                                    <option value="16:00"  {{ $value->closing =='16:00' ? ' selected' : '' }}>04:00 PM</option>
                                    <option value="16:30"  {{ $value->closing =='16:30' ? ' selected' : '' }}>04:30 PM</option>
                                    <option value="17:00"  {{ $value->closing =='17:00' ? ' selected' : '' }}>05:00 PM</option>
                                    <option value="17:30"  {{ $value->closing =='17:30' ? ' selected' : '' }}>05:30 PM</option>
                                    <option value="18:00"  {{ $value->closing =='18:00' ? ' selected' : '' }}>06:00 PM</option>
                                    <option value="18:30"  {{ $value->closing =='18:30' ? ' selected' : '' }}>06:30 PM</option>
                                    <option value="19:00"  {{ $value->closing =='19:00' ? ' selected' : '' }}>07:00 PM</option>
                                    <option value="19:30"  {{ $value->closing =='19:30' ? ' selected' : '' }}>07:30 PM</option>
                                    <option value="20:00"  {{ $value->closing =='20:00' ? ' selected' : '' }}>08:00 PM</option>
                                    <option value="20:30"  {{ $value->closing =='20:30' ? ' selected' : '' }}>08:30 PM</option>
                                    <option value="21:00"  {{ $value->closing =='21:00' ? ' selected' : '' }}>09:00 PM</option>
                                    <option value="21:30"  {{ $value->closing =='21:30' ? ' selected' : '' }}>09:30 PM</option>
                                    <option value="22:00"  {{ $value->closing =='22:00' ? ' selected' : '' }}>10:00 PM</option>
                                    <option value="22:30"  {{ $value->closing =='22:30' ? ' selected' : '' }}>10:30 PM</option>
                                    <option value="23:00"  {{ $value->closing =='23:00' ? ' selected' : '' }}>11:00 PM</option>
                                    <option value="23:30"  {{ $value->closing =='23:30' ? ' selected' : '' }}>11:30 PM</option>
                                    <option value="23:59"  {{ $value->closing =='23:59' ? ' selected' : '' }}>11:59 PM</option>
    

                                </select>
                  </div>
                  <div class='col-md-3 col-sm-12'>
                    <div class="row">
                        <div class="col-md-6">
                          <a class="btn btn-primary btn-icon" id="'+count+'button" onclick="addTiming('{{$value->day}}')"><i class="fa fa-plus"></i></a>
                      </div>
                            
                      </div>

                  </div>
          </div>
                 <div id = "variation_div_{{$value->day}}"></div>

      </div>
      @empty
         <div>
          <div class='row'>
                  <div class='col-md-3 col-sm-12'>
                         
                           <input class="form-control" type="text" name="day" value="Sunday" readonly>
                  </div>
                  <div class="col-md-3 col-sm-12">
                          
                          <select class="form-control" id="start_sun" name="timings[sun][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                  <div class='col-md-3 col-sm-12'>
                          <select class="form-control" id="end_sun" name="timings[sun][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                  <div class='col-md-3 col-sm-12'>
                          <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('sun')"><i class="fa fa-plus"></i></a>
                  </div>
          </div>
                 <div id = "variation_div_sun"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Monday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control time_start" id="start_mon" name="timings[mon][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end_mon" name="timings[mon][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                              <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('mon')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

                   <div id = "variation_div_mon"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Tuesday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control time_start" id="start_tue" name="timings[tue][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end_tue" name="timings[tue][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                              <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('tue')"><i class="fa fa-plus"></i></a>
                          </div>
          </div>

                   <div id = "variation_div_tue"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Wednesday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control time_start" id="start_wed" name="timings[wed][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>
                            <select class="form-control" id="end_wed" name="timings[wed][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                              <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('wed')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_wed"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Thursday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">
                            <select class="form-control time_start" id="start_thu" name="timings[thu][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>
                            <select class="form-control" id="end_thu" name="timings[thu][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                              <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('thu')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_thu"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Friday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control time_start" id="start_fri" name="timings[fri][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end_fri" name="timings[fri][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                              <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('fri')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_fri"></div>
      </div>
      <div>
            <div class='row'>
                      <div class='col-md-3 col-sm-12'>
                               <input class="form-control" type="text" name="day" value="Saturday" readonly>
                      </div>
                          <div class="col-md-3 col-sm-12">
                              <select class="form-control time_start" id="start_sat" name="timings[sat][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                            </div>
                              <div class='col-md-3 col-sm-12'>

                              <select class="form-control" id="end_sat" name="timings[sat][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                            </div>
                            <div class='col-md-3 col-sm-12'>
                                  <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('sat')"><i class="fa fa-plus"></i></a>
                            </div>
                     </div>

             <div id = "variation_div_sat"></div>
      </div>
      @endif
  </div>
<!-- 
  <div class="col-md-4" style="padding-left: 0px;"></br></br>
      <input type="checkbox" id="apply_all" >Apply all
  </div> -->

                                        </div>
                                        <div class="form-buttons-w">
                                            <center><button class="btn btn-theme" type="submit" style="height: 30px; width: 50%;"> Update</button></center>
                                           <!--  <button class="btn btn-danger" type="button"> Cancel</button> -->
                                        </div>
                                    {!!Form::close()!!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
            <script type="text/javascript">
   var count=2;
  function addTiming(day){

     var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);

     // $("#TextBoxDiv"+count).load('{{guard_url("restaurant/timing")}}'+'/'+count+'/'+day);
     newTextBoxDiv.after().load('{{guard_url("restaurant/timing")}}'+'/'+count+'/'+day);
  // newTextBoxDiv.after().html(
  //       '<div class="row"><div class="col-md-3 col-sm-12"></div><div class="col-md-3" style="display: block";><input class="timepicker form-control" id="start" type="text" placeholder="Start Time" name='+day+'['+count+'][start]"></div><div class="col-md-3" style="display: block";><input class="timepicker form-control" id="start" type="text" placeholder="End Time" name='+day+'['+count+'][end]"></div><div class="col-md-3" style="display: block";></div></div>');

  newTextBoxDiv.appendTo("#variation_div_"+day);
    count ++;
  }

  function removeVar(day,key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=timings]").length/3;
  if(countvaraitions > 1){
       
              document.getElementById('TextBoxDiv'+key).remove();  
           
        }
       
        document.getElementById(key+'button').style.display = "none";   

  if(countvaraitions == 1){  
    document.getElementById(key).remove();  
     document.getElementById('btn_remove').style.display = "none";
     document.getElementById('variation_name').style.display = "none";
     unset(variation_list);
  }
}

    $('.timepicker').datetimepicker({

        format: 'HH:mm'

    });

    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('textAddres'));
        google.maps.event.addListener(places, 'place_changed', function () {
        });
    });

    $(function(){ 
     var map,myLatlng;
      @if(!empty(user()->latitude) && !empty(user()->longitude))
         myLatlng = new google.maps.LatLng({!! user()->latitude !!},{!!user()->longitude !!});
      @else
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
      @endif
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
    });
       })


</script>

<script type="text/javascript">
    $( document ).ready(function() {
    $('#type').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'types',
        labelField: 'types',
        searchField: 'types',
        options: [
@forelse(trans('restaurant::restaurant.options.type') as $key => $types)

    {types: "{{$types}}" },
    @empty
    @endif
],
        create: function(input) {
            return {
                types: input
            }
        }
    });
});
    $('#apply_all').click(function(){ 
        var start = document.getElementById('start_mon').value;
        var end = document.getElementById('end_mon').value;
        var week = new Array(
    "mon",
    "tue",
    "wed",
    "thu",
    "fri",
    "sat",
    "sun"
  );

  for (i = 0; i < week.length; i++) {
    document.getElementById('start_'+week[i]).value = start;
    document.getElementById('end_'+week[i]).value = end;
  }

        
      
    }) 
</script>
