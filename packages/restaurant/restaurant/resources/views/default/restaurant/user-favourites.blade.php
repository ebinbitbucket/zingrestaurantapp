<section class="user-wrap-inner">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-3 user-aside">
                {!! Theme::partial('aside') !!}
            </div>
            <div class="col-md-9">
                <div class="user-content-wrap">
                    <div class="heading-block">
                        <h3>Orders</h3>
                    </div>
                    <div class="inner-content">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link" href="{{guard_url('restaurant/favourites/restaurants')}}">Eateries</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link  active" href="{{guard_url('restaurant/favourites/orders')}}">Orders</a>
                            </li>
                        </ul>

                        <div class="orders-wrap">
                            @forelse($orders as $order)
                            <div class="order-item">
                                <div class="restaurant-detail">
                                    <div class="img-holder">
                                        <figure>
                                            <a href="{{guard_url('cart/order/details/'.$order->getRoutekey())}}">
                                                <span class="img-wrap" style="background-image: url('{{url($order->restaurant->defaultImage('logo'))}}')"></span>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="text-holder">
                                        <h3 class="title"><a href="{{guard_url('cart/order/details/'.$order->getRoutekey())}}">{{$order->restaurant->name}}</a></h3>
                                        <h6><b>{{$order->order_type}} Order</b></h6>
                                        <div class="location">{{$order->restaurant->address}}</div>
                                        <div class="odrer-date">{{date('D, M d, h:i A',strtotime($order->created_at))}}</div>
                                        <!-- <a href="#" class="view-detail">View Details</a> -->
                                        <div class="status delivered">{{$order->order_status}} on {{date('D, M d, h:i A',strtotime($order->delivery_time))}} <i class="fa fa-check-circle text-success"></i></div>
                                        <div class="menu-items status delivered" style="font-size: 15px;">
                                            <div class="total">Total Paid: ${{$order->total}}</br>Loyalty Points: @if($order->loyalty_points == '') 0 @else {{number_format($order->loyalty_points,2)}} @endif</div>
                                        </div>
                                        @if($order->order_status != 'New Orders' && $order->order_status != 'Completed' && $order->order_status != 'Scheduled' && $order->order_status != 'Preparing')
                                        <div class="button-wrap mt-20">
                                            <a href="{{guard_url('cart/reorder/'.$order->getRoutekey())}}" class="btn btn-theme">Reorder</a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="menu-items meta-infos">
<!--                                     <div class="total">Total Paid: ${{$order->total}}</br>Loyalty Points: @if($order->loyalty_points == '') 0 @else {{$order->loyalty_points}} @endif</div>
 -->                                    @foreach($order->detail as $order_det)
                                    <div class="info pl-0">
                                        @if($order_det->menu_addon == 'menu' && (!empty($order_det->menu)))
                                            {{$order_det->menu->name}} 
                                        @elseif(!empty($order_det->addon))
                                            {{$order_det->addon->name}}  
                                        @endif 
                                      - {{$order_det->quantity}} x ${{$order_det->unit_price}} 
                                    </div>
                                    @endforeach
                                </div>
                            <!--     <div class="button-wrap mt-20">
                                    <a href="{{guard_url('cart/reorder/'.$order->getRoutekey())}}" class="btn btn-theme">Reorder</a>
                                </div> -->
                            </div>
                            @empty
                            <div class="empty-content-wrap">
                                <img src="{{theme_asset('img/order-empty.svg')}}" alt="">
                                <h2>You do not have any Order set as favorite!</h2>
                                <p><a href="{{url('/')}}">Find something tasty to order</a></p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


            