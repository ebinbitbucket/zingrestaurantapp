            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('customer_id')
                       -> label(trans('restaurant::review.label.customer_id'))
                       -> placeholder(trans('restaurant::review.placeholder.customer_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('restaurant::review.label.order_id'))
                       -> placeholder(trans('restaurant::review.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('rating')
                       -> label(trans('restaurant::review.label.rating'))
                       -> placeholder(trans('restaurant::review.placeholder.rating'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('review')
                       -> label(trans('restaurant::review.label.review'))
                       -> placeholder(trans('restaurant::review.placeholder.review'))!!}
                </div>
            </div>