<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="menu">
    <div class="row">
      <div class="col-md-6">
        <div class="row">
          <div class='col-md-6 col-sm-6'>
                         {!! Form::select('restaurant_id')
                         -> options(Restaurant::getRestaurant())
                         ->required()
                         -> label(trans('restaurant::menu.label.restaurant_id'))
                         -> placeholder(trans('restaurant::menu.placeholder.restaurant_id'))!!}
          </div>
          <div class='col-md-6 col-sm-6'>
                          {!! Form::select('category_id')
                          ->options(array())
                          ->required()
                          -> label(trans('restaurant::menu.label.category_id'))
                           -> placeholder(trans('restaurant::menu.placeholder.category_id'))!!}
          </div>
        </div>
        <div class="row">
          <div class='col-md-6 col-sm-6'>
                         {!! Form::text('name')
                         ->required()
                         -> label(trans('restaurant::menu.label.name'))
                         -> placeholder(trans('restaurant::menu.placeholder.name'))!!}
          </div>

          <div class='col-md-6 col-sm-6'>
                         {!! Form::text('description')
                         -> label(trans('restaurant::menu.label.description'))
                         -> placeholder(trans('restaurant::menu.placeholder.description'))!!}
          </div>
        </div>
        <div class="row">
          <div class='col-md-6 col-sm-6'>
                         {!! Form::number('price')
                         ->required()
                         -> label(trans('restaurant::menu.label.price'))
                         -> placeholder(trans('restaurant::menu.placeholder.price'))!!}
          </div>
          <div class='col-md-6 col-sm-6'>
                      {!! Form::select('status')
                      ->required()
                      -> options(trans('restaurant::menu.options.status'))
                      -> label(trans('restaurant::menu.label.status'))
                      -> placeholder(trans('restaurant::menu.placeholder.status'))!!}
          </div>
      </div>
      <div class="row">
       <div class='col-md-6 col-sm-6'>
                       {!! Form::text('youtube_link')
                         -> label('Youtube Link')
                         -> placeholder('Youtube Link')!!}
          </div>
           <div class='col-md-6 col-sm-6'>
                      {!! Form::select('home_show')
                      -> options(trans('restaurant::menu.options.home_show'))
                      -> label('Show in home page')
                      -> placeholder('Show in home page')!!}
          </div>
        </div>
                <div class="row">
          <div class='col-md-6 col-sm-12'>
              {!! Form::text('search_text')
              ->id('search_text')
               -> label('Search Texts')
               -> placeholder('Search Texts')!!}
        </div>
        <div class='col-md-6 col-sm-12'>
              {!! Form::text('master_search')
              ->id('master_search')
               -> label('Masters')
               -> placeholder('Masters')!!}
        </div>
        </div>
      @if($mode == 'show')
            <div class="row">
          <div class='col-md-12 col-sm-12'>
            <div class="form-group row">
              <label for="image" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::menu.label.image') }}
              </label>
             
              <div class='col-lg-7 col-sm-12'>
                          {!! $menu->files('image')
                          ->size('xs')
                          ->show()!!}
              </div>
            </div>
          </div>
        </div>
      </div>
    @else
        <div class="row">
          <div class='col-md-12 col-sm-12'>
            <div class="form-group row">
              <label for="image" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::menu.label.image') }}
              </label>
              <div class='col-lg-12 col-sm-12'>
                              {!! $menu->files('image')
                              ->url($menu->getUploadUrl('image'))
                              ->mime(config('filer.image_extensions'))
                              ->dropzone()!!}
              </div>
              <div class='col-lg-7 col-sm-12'>
                          {!! $menu->files('image')
                          ->editor()!!}
              </div>
            </div>
          </div>
        </div>
      </div>
      @endif
      <div class="col-sm-6">
        @if($mode == 'show')
        @if(!empty($menu->menu_variations)) 
          <div class="row">
            <label for="" style="display: block";>Variations</label>
          </div>
          @foreach($menu->menu_variations as $key=>$value)
            <div class="col-md-12 col-sm-12" >
              <div class="form-group" id="{{$key}}" style="display: block";>
                <span><input class="form-control"  id="{{$key}}" type="text" name="menu_variations[{{$key}}]['name']" value="{{$value['name']}}" >
                </span>
                <span><input class="form-control"  id="{{$key}}" type="text" name="menu_variations[{{$key}}]['description']" value="{{$value['description']}}" >
                </span>
                <span><input class="form-control"  id="{{$key}}" type="text" name="menu_variations[{{$key}}]['price']" value="{{$value['price']}}" >
                </span>
              </div>
            </div>
          @endforeach
        @endif 
    @endif
        @if(($mode == 'edit') || ($mode == 'create'))
      <div class="row">
        <a class="btn btn-primary btn-sm" onclick="addVariation()"><i class="fa fa-plus-circle"></i> Add Variation</a>
      </div>
      @if(!empty($menu->menu_variations))
        </br><div class="row"> 
          <div class="col-md-12"><label for="" style="display: block";>Variations</label></div>
        </div>
        @foreach($menu->menu_variations as $key=>$value) 
          <div class="row">
            <div id="{{$key}}" style="display: block";>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-6">
                      <input class="form-control"  id="{{$key}}" type="text" name="menu_variations[{{$key}}][name]" value="{{$value['name']}}" required="required">
                  </div>
                  <div class="col-md-6">
                    <input class="form-control"  id="{{$key}}" type="number" name="menu_variations[{{$key}}][price]" value="{{$value['price']}}" required="required" onchange="pricetest(event)">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <input class="form-control"  id="{{$key}}" type="text" name="menu_variations[{{$key}}][description]" value="{{$value['description']}}" >
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                    <a class="btn btn-primary btn-sm" id="{{$key}}button" onclick="removeVar({!!$key!!})"><i class="fa fa-close"></i></a>
              </div>
            </div>
          </div>
          </br>
        @endforeach
      @endif 
      <div class="row">
        </br><label for="" id="variation_name" style="display: none";></label>
      </div>
      <div class="row">
        <div class='col-md-12 col-sm-12'>
          <div id = "variation_div"></div>
        </div>
      </div>
    @endif
      </div>
    </div>
  </div>
  <div role="tabpanel" class="tab-pane " id="addons">
    <div class="row">
        <div class="col-md-6">
          @if($mode == 'create' || $mode == 'edit')
          <div class="row">
            <div class='col-md-10 col-sm-6'>

                        {!! Form::select('addon_ids[]')
                        ->id('addon_id')
                        ->options(Restaurant::getAddonsList($menu->restaurant_id))
                        -> label('Addon')
                         -> placeholder('Please select addon')!!}
            </div>
            <div class="col-md-2">
            </br>
              <a class="btn btn-primary btn-sm" id="button" onclick="showvariations()">Add</a>
            </div>
          </div>
          @endif
                @if($mode == 'create')
                <div class="row">
                  <div class='col-md-12 col-sm-6' id="addon_det"></div>
                </div>
                 @elseif($mode == 'show')
                  
                  @foreach($addon_det as $key => $addon_dets)
                            <div class="row">
                      <div class='col-md-10 col-sm-6'>

                                  {!! Form::text('addon_name')
                                  ->id('addon_id')
                                  ->value($addon_dets->name)
                                  -> label('Addon')
                                   -> placeholder('Please select addon')!!}
                      </div>
                     </div>
                          <div id="addon_det">
                           
                             <div class='col-md-4 col-sm-6'>
                              {!! Form::text('min')
                              -> label('Minimum')
                              ->value($addon_dets->min)
                              -> placeholder('Please enter Minimum')
                              -> required()
                              !!}
                         </div>
                          <div class='col-md-4 col-sm-6'>
                              {!! Form::text('max')
                              -> label('Maximum')
                              ->value($addon_dets->max)
                              -> placeholder('Please enter Maximum')
                              -> required()
                              !!}
                         </div>
                            <div class="row">
                              <div class='col-md-4 col-sm-6'>
                           </br>
                            <input type="checkbox" class="form_control" name="required" value="Yes"
                            {{$addon_dets->required == 'Yes' ? 'checked':'' }}> Required
                         </div>
                        </div>
                     @foreach(json_decode($addon_dets->variation_list) as $key => $variation)
                       <div class="row">
                       </br>
                          <div class='col-md-6 col-sm-6'>
                                <input type="checkbox" id="variation_id" checked="true"> {!!$variation->name !!}
                            </div>
                             <div class='col-md-4 col-sm-6' id="price">
                                {!! Form::text("variation_list[$key]['price']")
                                ->id($key)
                                -> label('Price')
                                ->value($variation->price)
                                ->disabled()
                                -> placeholder('Please enter Price')
                                !!}
                           </div>
                         </div>
                         <div class='col-md-12 col-sm-6' id="price">
                              {!! Form::hidden("variation_list[$key]['id']")
                              ->id($key)
                              -> label('ID')
                              ->value($variation->id)
                              ->disabled()
                              -> placeholder('Please enter ID')
                              -> required()
                              !!}
                         </div>
                         <div class='col-md-12 col-sm-6' id="price">
                              {!! Form::hidden("variation_list[$key]['description']")
                              ->id($key)
                              -> label('Description')
                              ->value($variation->description)
                              ->disabled()
                              -> placeholder('Please enter Description')
                              -> required()
                              !!}
                         </div>
                      @endforeach
                          </div>
                    @endforeach
                 @elseif($mode == 'edit')
                    @foreach($addon_det as $key => $addon_dets)
                         <input type="hidden" name="addon_ids[]" id="addons" value="{{$addon_dets->addon_id}}">
                         <div class="row">
                      <div class='col-md-10 col-sm-6'>

                                  {!! Form::text('addon_name')
                                  ->id('addon_id')
                                  ->value($addon_dets->name)
                                  ->disabled()
                                  -> label('Addon')
                                   -> placeholder('Please select addon')!!}
                      </div>
                     </div>
                          <div class="row">
                           
                             <div class='col-md-4 col-sm-6'>
                              {!! Form::text('av_'.$addon_dets->addon_id.'[min]')
                              -> label('Minimum')
                              ->value($addon_dets->min)
                              -> placeholder('Please enter Minimum')
                              -> required()
                              !!}
                         </div>
                          <div class='col-md-4 col-sm-6'>
                              {!! Form::text('av_'.$addon_dets->addon_id.'[max]')
                              -> label('Maximum')
                              ->value($addon_dets->max)
                              -> placeholder('Please enter Maximum')
                              -> required()
                              !!}
                         </div>
                           <div class='col-md-4 col-sm-6'>
                           </br>
                            <input type="checkbox" class="form_control" name="av_{{$addon_dets->addon_id}}[required]" value="Yes"
                            {{$addon_dets->required == 'Yes' ? 'checked':'' }}> Required

                             
                         </div>
                       </div>
                     @foreach(json_decode($addon_dets->variation_list) as $key => $variation)
                     <div class="row">
                        <div class='col-md-6 col-sm-6'>
                              <input type="checkbox" id="variation_id" checked="true" name="av_{{$addon_dets->addon_id}}[variation_list][{{$key}}][name]" value="{!! $variation->name !!}" onclick="enableVariation(this,{{$addon_dets->addon_id}},{{$key}})"> {!!$variation->name !!}
                          </div>
                           <div class='col-md-4 col-sm-6' id="price">
                              {!! Form::number('av_'.$addon_dets->addon_id.'[variation_list]['.$key.'][price]')
                              ->id($addon_dets->addon_id.'_'.$key.'_price')
                              -> label('Price')
                              ->value($variation->price)
                              -> placeholder('Please enter Price')
                              -> required()
                              !!}
                         </div>
                       </div>
                       <div class='col-md-12 col-sm-6' id="variations_id">
                              {!! Form::hidden('av_'.$addon_dets->addon_id.'[variation_list]['.$key.'][id]')
                              ->id($addon_dets->addon_id.'_'.$key.'_id')
                              -> label('ID')
                              ->value($variation->id)
                              -> placeholder('Please enter ID')
                              -> required()
                              !!}
                         </div>
                         <div class='col-md-12 col-sm-6' id="description">
                              {!! Form::hidden('av_'.$addon_dets->addon_id.'[variation_list]['.$key.'][description]')
                              ->id($addon_dets->addon_id.'_'.$key.'_desc')
                              -> label('Description')
                              ->value($variation->description)
                              -> placeholder('Please enter Description')
                              -> required()
                              !!}
                         </div>
                      @endforeach

                    @endforeach
                    <div id="addon_det"> </div>
                @endif
                <!-- <div class='col-md-4 col-sm-6'>
                       {!! Form::text('addons')
                       -> label(trans('restaurant::menu.label.addons'))
                       -> placeholder(trans('restaurant::menu.placeholder.addons'))!!}
                </div> -->

    </div>
            </div>
          </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
             $("#restaurant_id").on('change', function(){
              var restaurant_id = $("#restaurant_id option:selected").val();
              $("#addon_id").load('{{guard_url("restaurant/addons")}}'+'/'+restaurant_id);
              $("#category_id").load('{{trans_url("categories/Category")}}'+'/'+restaurant_id);
            })
             
  $('#search_text').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'search_text',
        labelField: 'search_text',
        create: function(input) {
            return {
                search_text: input
            }
        }
    });
    $('#master_search').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'master_search',
        labelField: 'master_search',
        create: function(input) {
            return {
                master_search: input
            }
        }
    });
   })
    $("#price").change(function(){
       var price=$("#price").val();
      if( price < 0)
       {
       toastr.error('Price should not be a negative number.', 'Error');
       $("#price").val('');
       }
   });

     function pricetest(event) {
     var selectElement = event.target;
    var value = selectElement.value;
    if( value < 0)
       {
       toastr.error('Variation Price should not be a negative number', 'Error');
      event.target.value='';
       }
      
 }

    function showvariations(){ 
      var addon_id = $("#addon_id option:selected").val();

              $("#addon_det").load('{{guard_url("restaurant/addons/variations")}}'+'/'+addon_id);

    }
    function enableVariation(element,addon,id){ 
    if(element.checked == true){
      document.getElementById(addon+'_'+id+'_price').disabled = false;
      document.getElementById(addon+'_'+id+'_desc').disabled = false;
      document.getElementById(addon+'_'+id+'_id').disabled = false;
    }
    else
    {
      document.getElementById(addon+'_'+id+'_price').disabled = true;
      document.getElementById(addon+'_'+id+'_desc').disabled = true;
      document.getElementById(addon+'_'+id+'_id').disabled = true;
    }
  }
</script>
<script type="text/javascript">

  <?php if (!empty(@$menu->menu_variations)):  $s = last(array_keys($menu->menu_variations));?>
    var count = {!!$s!!} + 1; 
    <?php else: ?>  var count = 1;
  <?php endif ?>
 var variation_list;
function addVariation()
{
  document.getElementById('variation_name').style.display = "block";
   var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);
                
  newTextBoxDiv.after().html(
        '<div class="row"><div class="col-md-10"><div class="row"><div class="col-md-6" style="display: block";><input type="text" class="form-control" name="menu_variations['+count+'][name]" id="'+count+' value="" placeholder="Variation Name" required="required"></div><div class="col-md-6" style="display: block";><input type="number" class="form-control" name="menu_variations['+count+'][price]" id="'+count+' value="" placeholder="Price" required="required" onchange="pricetest(event)"></div></div><div class="row"><div class="col-md-12" style="display: block";><input type="text" class="form-control" name="menu_variations['+count+'][description]" id="'+count+' value="" placeholder="Description"></div></div></div><div class="col-md-2"><a class="btn btn-primary btn-sm" id="'+count+'button" onclick="removeVar('+count+')"><i class="fa fa-close"></i></a></div></div></br>');
            
  newTextBoxDiv.appendTo("#variation_div");
    count ++;
    document.getElementById('btn_remove').style.display = "block";
    variation_list.push(document.getElementById("menu_variations["+count+"]"));
    // alert(variation_list);
    //alert("helloworld");
}

function removeVariation()
{
    //alert("nothing happened " );
    if(count > 1)
    {
        var lastCount = count - 1;
        var variation_id = "variation"+lastCount;
        document.getElementById(variation_id).remove();
    }


    if(count == 1)
    {
        document.getElementById('btn_remove').style.display = "none";
        document.getElementById('variation_name').style.display = "none";
    }
    else
    {
        count--;
    }

}


function removeVar(key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=menu_variations]").length/3;
   var mode = '<?php echo $mode; ?>'; 
   if(countvaraitions > 1){
  
        if(mode == 'edit'){
           if($("#TextBoxDiv"+key).length == 0){
                          document.getElementById(key).remove();  

            }
            else{

              document.getElementById('TextBoxDiv'+key).remove();  
            }
        }
        else if(mode == 'create'){
          document.getElementById('TextBoxDiv'+key).remove();   
        }
        document.getElementById(key+'button').style.display = "none";   
  }
  if(countvaraitions == 1){  
    if(mode == 'create' || mode=='edit'){
        if($("#TextBoxDiv"+key).length == 0){
           document.getElementById(key).remove();  

          }
          else{
            document.getElementById('TextBoxDiv'+key).remove();  
          }
           
        }
    document.getElementById(key).remove();  
     document.getElementById('btn_remove').style.display = "none";
     document.getElementById('variation_name').style.display = "none";
     unset(variation_list);
  }
}
        

</script>

