    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
             <li role="presentation" class="active"><a href="#menu" aria-controls="menu" role="tab" data-toggle="tab">Menu</a></li>
             <li role="presentation"><a href="#addons" aria-controls="addons" role="tab" data-toggle="tab">Addons</a></li>
            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#restaurant-menu-entry' data-href='{{guard_url('restaurant/menu/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($menu->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#restaurant-menu-entry' data-href='{{ guard_url('restaurant/menu') }}/{{$menu->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#restaurant-menu-entry' data-datatable='#restaurant-menu-list' data-href='{{ guard_url('restaurant/menu') }}/{{$menu->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('restaurant-menu-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('restaurant/menu'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('restaurant::menu.name') !!}  [{!! $menu->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('restaurant::admin.menu.partial.entry', ['mode' => 'show','addon_det' => $addon_det])
                </div>
            </div>
        {!! Form::close() !!}
    </div>
    <script type="text/javascript">
        $("#category_id").load('{{URL::to("/admin/restaurant/category/restaurantcategory"). '/' .@$menu['restaurant_id'].'/'. @$menu['category_id']}}');
    </script>