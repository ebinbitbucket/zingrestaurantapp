    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
             <li role="presentation" class="active"><a href="#menu" aria-controls="menu" role="tab" data-toggle="tab">Menu</a></li>
             <li role="presentation"><a href="#addons" aria-controls="addons" role="tab" data-toggle="tab">Addons</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#restaurant-menu-edit'  data-load-to='#restaurant-menu-entry' data-datatable='#restaurant-menu-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#restaurant-menu-entry' data-href='{{guard_url('restaurant/menu')}}/{{$menu->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('restaurant-menu-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('restaurant/menu/'. $menu->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="details">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('restaurant::menu.name') !!} [{!!$menu->name!!}] </div>
                @include('restaurant::admin.menu.partial.entry', ['mode' => 'edit','addon_det' => $addon_det])
            </div>
        </div>
        {!!Form::close()!!}
    </div>
    <script type="text/javascript">
        $("#category_id").load('{{URL::to("/admin/restaurant/category/restaurantcategory"). '/' .@$menu['restaurant_id'].'/'. @$menu['category_id']}}');
    </script>