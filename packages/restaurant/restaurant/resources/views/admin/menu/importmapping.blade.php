
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li role="presentation" class="active"><a href="#menu" aria-controls="menu" role="tab" data-toggle="tab">Menu</a></li>
             <li role="presentation"><a href="#addons" aria-controls="addons" role="tab" data-toggle="tab">Addons</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#restaurant-menu-create'  data-load-to='#restaurant-menu-entry' data-datatable='#restaurant-menu-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CLOSE' data-load-to='#restaurant-menu-entry' data-href='{{guard_url('restaurant/menu/0')}}'><i class="fa fa-times-circle"></i> {{ trans('app.close') }}</button>
            </div>
        </ul>
        <div class="tab-content clearfix">
            {!!Form::vertical_open()
            ->id('restaurant-menu-create')
            ->method('POST')
            ->files('true')
            ->action(guard_url('restaurant/menu/importmapping'))!!}
            <div class="tab-pane active" id="details">
                <div class="tab-pan-title">  {{ trans('app.new') }}  [{!! trans('restaurant::menu.name') !!}] </div>
                <div class="row">
          <div class='col-md-12 col-sm-12'>
            <div class="form-group row">
              <label for="mapingfile" class="control-label col-lg-12 col-sm-12 text-left"> Upload file
              </label>
              <div class='col-lg-12 col-sm-12'>
                              {!! $menu->files('csv_file')
                            ->mime(config('filer.allowed_extensions'))
                            ->url($menu->getUploadUrl('csv_file'))
                            ->dropzone()!!}
              </div>
              <div class='col-lg-7 col-sm-12'>
                          {!! $menu
                            ->files('csv_file')
                            ->editor()!!}
              </div>
            </div>
          </div>
        </div>
      </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>