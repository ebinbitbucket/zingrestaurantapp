<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('restaurant::menu.name') !!} <small> {!! trans('app.manage') !!} {!! trans('restaurant::menu.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('restaurant::menu.names') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div id='restaurant-menu-entry'>
    </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                    <li class="{!!(request('status') == '')?'active':'';!!}"><a href="{!!guard_url('restaurant/menu')!!}">{!! trans('restaurant::menu.names') !!}</a></li>
                    
                    <li class="pull-right">
                    <span class="actions">
                    <!--   
                    <a  class="btn btn-xs btn-purple"  href="{!!guard_url('restaurant/menu/reports')!!}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Reports</span></a>
                    @include('restaurant::admin.menu.partial.actions')
                    -->
                    <button type="button" class="btn btn-success btn-sm"  data-action='NEW' data-load-to='#restaurant-menu-entry' data-href='{{guard_url("restaurant/menu/updatesearchtext")}}'><i class="fa fa-refresh"></i> Update search Tags </button>
                    <button type="button" class="btn btn-primary btn-sm"  data-action='NEW' data-load-to='#restaurant-menu-entry' data-href='{{guard_url("restaurant/menu/importmaping")}}'><i class="fa fa-upload"></i> Import Menu Mapping </button>
                     <a type="button" class="btn btn-primary btn-sm" href='#' id="download_csv"><i class="fa fa-file-excel-o"></i> Download CSV </a>
                    @include('restaurant::admin.menu.partial.filter')
                    @include('restaurant::admin.menu.partial.column')
                    </span> 
                </li>
            </ul>
            <div class="tab-content">
                <table id="restaurant-menu-list" class="table table-striped data-table">
                    <thead class="list_head">
                        <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset" style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input type="checkbox" id="restaurant-menu-check-all"></th>
                        <th data-field="restaurant_id">{!! trans('restaurant::menu.label.restaurant_id')!!}</th>
                    <th data-field="category_id">{!! trans('restaurant::menu.label.category_id')!!}</th>
                    <th data-field="name">{!! trans('restaurant::menu.label.name')!!}</th>
                    <th data-field="master">Master</th>
                    <th data-field="price">{!! trans('restaurant::menu.label.price')!!}</th>
                    <th data-field="status">Status</th>
                    <th data-field="stock_status">Menu Schedule</th>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

var oTable;
var oSearch;
$(document).ready(function(){
    app.load('#restaurant-menu-entry', '{!!guard_url('restaurant/menu/0')!!}');
    oTable = $('#restaurant-menu-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }], 
        
        "responsive" : true,
        "order": [[3, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('restaurant/menu') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'restaurant_id'},
            {data :'category_id'},
            {data :'name'},
             {data :'master'},
            {data :'price'},
            {data :'status'},
            {data :'stock_status'},
        ],
        "pageLength": 25
    });

    $('#restaurant-menu-list tbody').on( 'click', 'tr td:not(:first-child)', function (e) {
        e.preventDefault();

        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#restaurant-menu-list').DataTable().row( this ).data();
        $('#restaurant-menu-entry').load('{!!guard_url('restaurant/menu')!!}' + '/' + d.id);
    });

    $('#restaurant-menu-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent'); 
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#restaurant-menu-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }
        
    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#restaurant-menu-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#restaurant-menu-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});

 $('#download_csv').click(function(){
      var filter = $("#form-search").serialize();
      $('#download_csv').attr('href',"{{guard_url('restaurant/menu/download/csv')}}"+ "/" + filter);
      $('#download_csv').attr("target", "_blank");
    });
</script>