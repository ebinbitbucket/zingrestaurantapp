 @if (Session::has('messageRestaurant'))
                                    <strong><h3> {{Session::get('messageRestaurant')}}</h3></strong>
                                    @endif
                                    <div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="restaurant">
    <div class='row'> 
      @if($mode != 'edit')
        <div class='col-md-3 col-sm-12'>
          {!! Form::text('name')
          -> label(trans('restaurant::restaurant.label.name'))
          -> required()
          -> placeholder(trans('restaurant::restaurant.placeholder.name'))!!}
        </div>
        <div class='col-md-3 col-sm-12'>
            {!! Form::numeric('phone')
            -> label(trans('restaurant::restaurant.label.phone'))
            -> required()
            -> placeholder(trans('restaurant::restaurant.placeholder.phone'))!!}
        </div>
        <div class='col-md-3 col-sm-12'>
             {!! Form::email('email')
             ->autocomplete('off')
              -> label(trans('restaurant::restaurant.label.email'))
              -> required()
              -> placeholder(trans('restaurant::restaurant.placeholder.email'))!!}
        </div>
        
        <div class='col-md-3 col-sm-12'>
            {!! Form::password('password')
            ->required()
            ->autocomplete('false')
            -> label(trans('user::user.label.password')) 
            -> placeholder(trans('user::user.placeholder.password')) !!}
        </div>
      @else
          <div class='col-md-3 col-sm-12'>
          {!! Form::text('name')
          -> label(trans('restaurant::restaurant.label.name'))
          -> required()
          -> placeholder(trans('restaurant::restaurant.placeholder.name'))!!}
        </div>
        <div class='col-md-2 col-sm-12'>
            {!! Form::numeric('phone')
            -> label(trans('restaurant::restaurant.label.phone'))
            -> required()
            -> placeholder(trans('restaurant::restaurant.placeholder.phone'))!!}
        </div>
        <div class='col-md-2 col-sm-12'>
             {!! Form::email('email')
             ->autocomplete('off')
              -> label(trans('restaurant::restaurant.label.email'))
              -> required()
              -> placeholder(trans('restaurant::restaurant.placeholder.email'))!!}
        </div>
           <div class='col-md-2 col-sm-6'>
             {!! Form::select('ESign')
             -> options(trans('restaurant::restaurant.options.ESign'))
             -> label('ESign')
             -> placeholder('ESign')!!}
        </div>
        <div class='col-md-3 col-sm-12'>
            {!! Form::password('password')
            -> label(trans('user::user.label.password').' <a href="javascript:void(0)" class="pwdedit"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>')
            -> disabled(true)
            -> placeholder(trans('user::user.placeholder.password')) !!}
        </div>
        @endif
      </div>
      <div class="row">
        <div class='col-md-3 col-sm-12'>
              {!! Form::text('type')
              ->id('type')
              ->required()
               -> label(trans('restaurant::restaurant.label.type'))
               -> placeholder(trans('restaurant::restaurant.placeholder.type'))!!}
        </div>
        <div class='col-md-3 col-sm-12'>
              {!! Form::text('slug')
              ->id('slug')
              ->required()
               -> label(trans('restaurant::restaurant.label.slug'))
               -> placeholder(trans('restaurant::restaurant.placeholder.slug'))!!}
        </div>
        <div class='col-md-2 col-sm-12'>
              {!! Form::text('price_range_min')
              ->required()
              -> label(trans('restaurant::restaurant.label.price_range_min'))
              -> placeholder(trans('restaurant::restaurant.placeholder.price_range_min'))!!}
        </div>
        <div class='col-md-2 col-sm-12'>
              {!! Form::text('price_range_max')
              ->required()
              -> label(trans('restaurant::restaurant.label.price_range_max'))
              -> placeholder(trans('restaurant::restaurant.placeholder.price_range_max'))!!}
        </div>
         <div class='col-md-2 col-sm-12'>
              {!! Form::select('category_name')
              -> options(Master::getMasterCategories())
              -> label('Category')
              -> required()
              -> placeholder('Category')!!}
        </div>
        
      </div>
      <div class="row">
        @if($mode == 'edit')
          @if($restaurant->published != 'Published')
            <div class='col-md-2 col-sm-6'>
                 {!! Form::select('published')
                 -> options(trans('restaurant::restaurant.options.published'))
                 -> label('Status')
                 -> placeholder('Status')!!}
            </div>
             <div class='col-md-2 col-sm-6'>
             {!! Form::text('min_order_count')
             -> label('Minimum Order Amount-Pickup')
             -> placeholder('Minimum Order Amount-Pickup')!!}
        </div>
        <div class='col-md-2 col-sm-6'>
             {!! Form::text('min_order_amount_delivery')
             -> label('Minimum Order Amount-Delivery')
             -> placeholder('Minimum Order Amount-Delivery')!!}
        </div>
        <div class='col-md-2 col-sm-6'>
             {!! Form::select('delivery')
             ->required()
             -> options(trans('restaurant::restaurant.options.delivery'))
             -> label(trans('restaurant::restaurant.label.delivery'))
             -> placeholder(trans('restaurant::restaurant.placeholder.delivery'))!!}
        </div>
         <div class='col-md-2 col-sm-12'>
              {!! Form::text('keywords')
              -> label(trans('restaurant::restaurant.label.keywords'))
              -> placeholder(trans('restaurant::restaurant.placeholder.keywords'))!!}
        </div>
        <div class='col-md-2 col-sm-12'>
            {!! Form::text ('delivery_charge')
            -> label('Delivery charge')
            -> placeholder('Please enter delivery charge')!!}
          </div>
            <div class='col-md-2 col-sm-12'>
              {!! Form::select('flag_special_instr')
              -> options(trans('restaurant::restaurant.options.flag_special_instr'))
              -> label('Show Special Instructions')
              -> placeholder('Show Special Instructions')!!}
        </div>
        <div class='col-md-3 col-sm-12'>
             {!! Form::numeric('zipcode')
             -> label(trans('restaurant::restaurant.label.zipcode'))
             -> placeholder(trans('restaurant::restaurant.placeholder.zipcode'))!!}
          </div>
          
        @else
         <div class='col-md-2 col-sm-6'>
             {!! Form::text('min_order_count')
             -> label('Minimum Order Amount-Pickup')
             -> placeholder('Minimum Order Amount-Pickup')!!}
        </div>
        <div class='col-md-2 col-sm-6'>
             {!! Form::text('min_order_amount_delivery')
             -> label('Minimum Order Amount-Delivery')
             -> placeholder('Minimum Order Amount-Delivery')!!}
        </div>
        <div class='col-md-2 col-sm-6'>
             {!! Form::select('delivery')
             ->required()
             -> options(trans('restaurant::restaurant.options.delivery'))
             -> label(trans('restaurant::restaurant.label.delivery'))
             -> placeholder(trans('restaurant::restaurant.placeholder.delivery'))!!}
        </div>
          <div class='col-md-3 col-sm-12'>
              {!! Form::text('keywords')
              -> label(trans('restaurant::restaurant.label.keywords'))
              -> placeholder(trans('restaurant::restaurant.placeholder.keywords'))!!}
        </div>
        <div class='col-md-2 col-sm-12'>
            {!! Form::text ('delivery_charge')
            -> label('Delivery charge')
            -> placeholder('Please enter delivery charge')!!}
          </div>
          <div class='col-md-2 col-sm-12'>
              {!! Form::select('flag_special_instr')
              -> options(trans('restaurant::restaurant.options.flag_special_instr'))
              -> label('Show Special Instructions')
              -> placeholder('Show Special Instructions')!!}
        </div>
        <div class='col-md-3 col-sm-12'>
             {!! Form::numeric('zipcode')
             -> label(trans('restaurant::restaurant.label.zipcode'))
             -> placeholder(trans('restaurant::restaurant.placeholder.zipcode'))!!}
          </div>
         
          @endif

        @endif
        
        <div class='col-md-2 col-sm-12'>
          {!! Form::select('kitchen_view')
             -> options(trans('restaurant::restaurant.options.kitchen_view'))
             -> label('Kitchen view')
             -> placeholder('Kitchen view')!!}
             </div> 
             <div class='col-md-2 col-sm-12'>
              {!! Form::select('catering')
              -> options(trans('restaurant::restaurant.options.catering'))
              -> label('Catering')
              -> placeholder('Catering')!!}
        </div>
         <div class='col-md-3 col-sm-12'>
             {!! Form::text('memobirdID')
                -> label('Printer ID')
                -> placeholder('Printer ID')!!}
        </div>
        <div class='col-md-3 col-sm-12'>
              {!! Form::select('chain_list')
              -> options(trans('restaurant::restaurant.options.chain_list'))
              -> label('Chain list')
              -> placeholder('Chain list')!!}
        </div>
         <div class='col-md-3 col-sm-12'>
             {!! Form::email('confirmation_mail')
             ->autocomplete('off')
              -> label('Confirmation Mail')
              -> required()
              -> placeholder('Confirmation Mail')!!}
        </div>
        <div class='col-md-3 col-sm-12'>
             {!! Form::email('confirmation_text_mail')
             ->autocomplete('off')
              -> label('Confirmation Text Mail')
              -> placeholder('Confirmation Text Mail')!!}
        </div>
        <div class='col-md-3 col-sm-12'>
             {!! Form::email('confirmation_fax_mail')
             ->autocomplete('off')
              -> label('Confirmation Fax Mail')
              -> placeholder('Confirmation Fax Mail')!!}
        </div>
        <div class='col-md-3 col-sm-12'>
          {!! Form::text('refund_email')
           -> label('Refund Email')
           -> placeholder('Refund Email')!!}
     </div>
        
     <div class='col-md-3 col-sm-12'>
      {!! Form::text('reminder')
       -> label('Reminder')
       -> placeholder('Reminder')!!}
 </div>
 <div class='col-md-3 col-sm-12'>
  {!! Form::text('delay_call_number')
   -> label('Delay Call Number')
   -> placeholder('Delay Call Number')!!}
</div>
<div class='col-md-3 col-sm-12'>
  {!! Form::text('second_call_delay')
   -> label('Second Call Delay')
   -> placeholder('Second Call Delay')!!}
</div>
<div class='col-md-2 col-sm-6'>
  {!! Form::select('postmate_delivery')
  ->required()
  -> options(trans('restaurant::restaurant.options.delivery'))
  -> label('Postmate Delivery')
  -> placeholder('Postmate Delivery')!!}
</div>
      </div>

         
         
       <div class="row">
          <div class='col-md-3 col-sm-12'>
            {!! Form::text ('delivery_limit')
            -> label('Delivery Limit')
            -> placeholder('Please enter delivery limit in miles')!!}
          </div>
          <div class='col-md-3 col-sm-12'>
            {!! Form::text ('address')
            ->id('txtPlaces')
            ->required()
            -> label(trans('restaurant::restaurant.label.address'))
            -> placeholder(trans('restaurant::restaurant.placeholder.address'))!!}
          </div>
          

          <div class='col-md-3 col-sm-12'>
             {!! Form::text('latitude')
             ->required()
             -> label(trans('restaurant::restaurant.label.latitude'))
             -> placeholder(trans('restaurant::restaurant.placeholder.latitude'))!!}
         </div>
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('longitude')
           ->required()
           -> label(trans('restaurant::restaurant.label.longitude'))
           -> placeholder(trans('restaurant::restaurant.placeholder.longitude'))!!}

         </div>
       </div>
       <div class="row">
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('CCR')
           -> label('Credit Card % Rate')
           -> placeholder('Credit Card % Rate')!!}

         </div>
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('CCF')
           -> label('Credit Card Transaction Fee')
           -> placeholder('Credit Card Transaction Fee')!!}

         </div>
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('ZR')
           -> label('Eatery % Rate Zing Fee')
           -> placeholder('Eatery % Rate Zing Fee')!!}

         </div>
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('ZF')
           -> label('Eatery Zing Flat Fee')
           -> placeholder('Eatery Zing Flat Fee')!!}

         </div>
       </div>
          <div class="row">
                 <div class='col-md-3 col-sm-12'>

           {!! Form::text('ZC')
           -> label('Customer Zing Fee')
           -> placeholder('Customer Zing Fee')!!}

         </div>
            <div class='col-md-3 col-sm-12'>

           {!! Form::text('tax_rate')
           -> label('Tax Rate')
           -> placeholder('Tax Rate')!!}

         </div>
         <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[facebook]')
             -> label('Facebook Link')
             -> placeholder('Facebook Link')!!}
        </div>
        <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[twitter]')
             -> label('Twitter Link')
             -> placeholder('Twitter Link')!!}
        </div>
        
      </div>
      <div class="row"> 
        <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[youtube]')
             -> label('Youtube Link')
             -> placeholder('Youtube Link')!!}
        </div>
        <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[linkedln]')
             -> label('Linkedln Link')
             -> placeholder('Linkedln Link')!!}
        </div>
         <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[instagram]')
             -> label('Instagram Link')
             -> placeholder('Instagram Link')!!}
        </div>
        <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[pintrest]')
             -> label('Pinterest Link')
             -> placeholder('Pinterest Link')!!}
        </div>
        
      </div>
          <div id="map_canvas" style="height: 280px;width: 100%; margin-bottom: 30px"></div>
        
          <div class="row">
            <div class='col-md-6 col-sm-12'>
              <div class="row">
              <div class="col-md-12">
                <div class="form-group row">
                    <label for="logo" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::restaurant.label.logo') }}
                    </label>
                    <div class='col-lg-12 col-sm-12'>
                        {!! $restaurant->files('logo')
                        
                        ->url($restaurant->getUploadUrl('logo'))
                        ->mime(config('filer.image_extensions'))
                        ->dropzone()!!}
                    </div>
                    <div class='col-lg-12 col-sm-12'>
                    {!! $restaurant->files('logo')
                    ->editor()!!}
                    </div>
                </div>
              </div>
              </div>
              <div class="row">
               <div class="col-md-12">
                <div class="form-group row">
                    <label for="logo" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::restaurant.label.offer') }}
                    </label>
                    <div class='col-lg-12 col-sm-12'>
                        {!! $restaurant->files('offer')
                        ->url($restaurant->getUploadUrl('offer'))
                        ->mime(config('filer.image_extensions'))
                        ->dropzone()!!}
                    </div>
                    <div class='col-lg-12 col-sm-12'>
                    {!! $restaurant->files('offer')
                    ->editor()!!}
                    </div>
                </div>
              </div>
            </div>
          </div>
            <div class='col-md-6 col-sm-12'>
              <div class="row">
              <div class="col-md-12">
               {!! Form::textarea ('description')
                -> dataUpload(trans_url($restaurant->getUploadURL('description')))
               -> label(trans('restaurant::restaurant.label.description'))
               -> placeholder(trans('restaurant::restaurant.placeholder.description'))
               ->rows(10)!!}
            </div>
          </div>
            <div class="row">
              <div class='col-md-12 col-sm-12'>
                <div class="form-group row">
                    <label for="gallery" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::restaurant.label.gallery') }}
                    </label>
                    <div class='col-lg-12 col-sm-12'>
                        {!! $restaurant->files('gallery')
                        ->url($restaurant->getUploadUrl('gallery'))
                        ->mime(config('filer.image_extensions'))
                        ->dropzone()!!}
                    </div>
                    <div class='col-lg-12 col-sm-12'>
                    {!! $restaurant->files('gallery')
                    ->editor()!!}
                    </div>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
    <div role="tabpanel" class="tab-pane " id="restaurant_timings">
      @if($mode == 'edit')
      <div class='row'>
        <div class='col-md-3 col-sm-12'>
          <label>Day</label>
        </div>
        <div class='col-md-3 col-sm-12'>
          <label>Opening</label>
        </div>
        <div class='col-md-3 col-sm-12'>
          <label>Ending</label>
        </div>
      </div>
              @forelse($restaurant_timings as $key=>$value)
              <div>
          <div class='row'>
                  <div class='col-md-3 col-sm-12'>
                          
                           <input class="form-control" type="text" name="day" value="{{ucfirst($value->day)}}" readonly>
                  </div>
                  <div class="col-md-3 col-sm-12">
                          
                          <select class="form-control" id="start" name="timings[{{$value->day}}][{{$value->id}}][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00" {{ $value->opening =='00:00' ? ' selected' : '' }}>12:00 AM</option>
                                    <option value="00:30"  {{ $value->opening =='00:30' ? ' selected' : '' }}>12:30 AM</option>
                                    <option value="01:00"  {{ $value->opening =='01:00' ? ' selected' : '' }}>01:00 AM</option>
                                    <option value="01:30"  {{ $value->opening =='01:30' ? ' selected' : '' }}>01:30 AM</option>
                                    <option value="02:00"  {{ $value->opening =='02:00' ? ' selected' : '' }}>02:00 AM</option>
                                    <option value="02:30"  {{ $value->opening =='02:30' ? ' selected' : '' }}>02:30 AM</option>
                                    <option value="03:00"  {{ $value->opening =='03:00' ? ' selected' : '' }}>03:00 AM</option>
                                    <option value="03:30"  {{ $value->opening =='03:30' ? ' selected' : '' }}>03:30 AM</option>
                                    <option value="04:00"  {{ $value->opening =='04:00' ? ' selected' : '' }}>04:00 AM</option>
                                    <option value="04:30"  {{ $value->opening =='04:30' ? ' selected' : '' }}>04:30 AM</option>
                                    <option value="05:00"  {{ $value->opening =='05:00' ? ' selected' : '' }}>05:00 AM</option>
                                    <option value="05:30"  {{ $value->opening =='05:30' ? ' selected' : '' }}>05:30 AM</option>
                                    <option value="06:00"  {{ $value->opening =='06:00' ? ' selected' : '' }}>06:00 AM</option>
                                    <option value="06:30"  {{ $value->opening =='06:30' ? ' selected' : '' }}>06:30 AM</option>
                                    <option value="07:00"  {{ $value->opening =='07:00' ? ' selected' : '' }}>07:00 AM</option>
                                    <option value="07:30"  {{ $value->opening =='07:30' ? ' selected' : '' }}>07:30 AM</option>
                                    <option value="08:00"  {{ $value->opening =='08:00' ? ' selected' : '' }}>08:00 AM</option>
                                    <option value="08:30"  {{ $value->opening =='08:30' ? ' selected' : '' }}>08:30 AM</option>
                                    <option value="09:00"  {{ $value->opening =='09:00' ? ' selected' : '' }}>09:00 AM</option>
                                    <option value="09:30"  {{ $value->opening =='09:30' ? ' selected' : '' }}>09:30 AM</option>
                                    <option value="10:00"  {{ $value->opening =='10:00' ? ' selected' : '' }}>10:00 AM</option>
                                    <option value="10:30"  {{ $value->opening =='10:30' ? ' selected' : '' }}>10:30 AM</option>
                                    <option value="11:00"  {{ $value->opening =='11:00' ? ' selected' : '' }}>11:00 AM</option>
                                    <option value="11:30"  {{ $value->opening =='11:30' ? ' selected' : '' }}>11:30 AM</option>
                                    <option value="12:00"  {{ $value->opening =='12:00' ? ' selected' : '' }}>12:00 PM</option>
                                    <option value="12:30"  {{ $value->opening =='12:30' ? ' selected' : '' }}>12:30 PM</option>
                                    <option value="13:00"  {{ $value->opening =='13:00' ? ' selected' : '' }}>01:00 PM</option>
                                    <option value="13:30"  {{ $value->opening =='13:30' ? ' selected' : '' }}>01:30 PM</option>
                                    <option value="14:00"  {{ $value->opening =='14:00' ? ' selected' : '' }}>02:00 PM</option>
                                    <option value="14:30"  {{ $value->opening =='14:30' ? ' selected' : '' }}>02:30 PM</option>
                                    <option value="15:00"  {{ $value->opening =='15:00' ? ' selected' : '' }}>03:00 PM</option>
                                    <option value="15:30"  {{ $value->opening =='15:30' ? ' selected' : '' }}>03:30 PM</option>
                                    <option value="16:00"  {{ $value->opening =='16:00' ? ' selected' : '' }}>04:00 PM</option>
                                    <option value="16:30"  {{ $value->opening =='16:30' ? ' selected' : '' }}>04:30 PM</option>
                                    <option value="17:00"  {{ $value->opening =='17:00' ? ' selected' : '' }}>05:00 PM</option>
                                    <option value="17:30"  {{ $value->opening =='17:30' ? ' selected' : '' }}>05:30 PM</option>
                                    <option value="18:00"  {{ $value->opening =='18:00' ? ' selected' : '' }}>06:00 PM</option>
                                    <option value="18:30"  {{ $value->opening =='18:30' ? ' selected' : '' }}>06:30 PM</option>
                                    <option value="19:00"  {{ $value->opening =='19:00' ? ' selected' : '' }}>07:00 PM</option>
                                    <option value="19:30"  {{ $value->opening =='19:30' ? ' selected' : '' }}>07:30 PM</option>
                                    <option value="20:00"  {{ $value->opening =='20:00' ? ' selected' : '' }}>08:00 PM</option>
                                    <option value="20:30"  {{ $value->opening =='20:30' ? ' selected' : '' }}>08:30 PM</option>
                                    <option value="21:00"  {{ $value->opening =='21:00' ? ' selected' : '' }}>09:00 PM</option>
                                    <option value="21:30"  {{ $value->opening =='21:30' ? ' selected' : '' }}>09:30 PM</option>
                                    <option value="22:00"  {{ $value->opening =='22:00' ? ' selected' : '' }}>10:00 PM</option>
                                    <option value="22:30"  {{ $value->opening =='22:30' ? ' selected' : '' }}>10:30 PM</option>
                                    <option value="23:00"  {{ $value->opening =='23:00' ? ' selected' : '' }}>11:00 PM</option>
                                    <option value="23:30"  {{ $value->opening =='23:30' ? ' selected' : '' }}>11:30 PM</option>
                                    <option value="23:59"  {{ $value->opening =='23:59' ? ' selected' : '' }}>11:59 PM</option>


                                </select>
                                
                  </div>
                  <div class='col-md-3 col-sm-12'>
                          

                          <select class="form-control" id="end" name="timings[{{$value->day}}][{{$value->id}}][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00" {{ $value->closing =='00:00' ? ' selected' : '' }}>12:00 AM</option>
                                    <option value="00:30"  {{ $value->closing =='00:30' ? ' selected' : '' }}>12:30 AM</option>
                                    <option value="01:00"  {{ $value->closing =='01:00' ? ' selected' : '' }}>01:00 AM</option>
                                    <option value="01:30"  {{ $value->closing =='01:30' ? ' selected' : '' }}>01:30 AM</option>
                                    <option value="02:00"  {{ $value->closing =='02:00' ? ' selected' : '' }}>02:00 AM</option>
                                    <option value="02:30"  {{ $value->closing =='02:30' ? ' selected' : '' }}>02:30 AM</option>
                                    <option value="03:00"  {{ $value->closing =='03:00' ? ' selected' : '' }}>03:00 AM</option>
                                    <option value="03:30"  {{ $value->closing =='03:30' ? ' selected' : '' }}>03:30 AM</option>
                                    <option value="04:00"  {{ $value->closing =='04:00' ? ' selected' : '' }}>04:00 AM</option>
                                    <option value="04:30"  {{ $value->closing =='04:30' ? ' selected' : '' }}>04:30 AM</option>
                                    <option value="05:00"  {{ $value->closing =='05:00' ? ' selected' : '' }}>05:00 AM</option>
                                    <option value="05:30"  {{ $value->closing =='05:30' ? ' selected' : '' }}>05:30 AM</option>
                                    <option value="06:00"  {{ $value->closing =='06:00' ? ' selected' : '' }}>06:00 AM</option>
                                    <option value="06:30"  {{ $value->closing =='06:30' ? ' selected' : '' }}>06:30 AM</option>
                                    <option value="07:00"  {{ $value->closing =='07:00' ? ' selected' : '' }}>07:00 AM</option>
                                    <option value="07:30"  {{ $value->closing =='07:30' ? ' selected' : '' }}>07:30 AM</option>
                                    <option value="08:00"  {{ $value->closing =='08:00' ? ' selected' : '' }}>08:00 AM</option>
                                    <option value="08:30"  {{ $value->closing =='08:30' ? ' selected' : '' }}>08:30 AM</option>
                                    <option value="09:00"  {{ $value->closing =='09:00' ? ' selected' : '' }}>09:00 AM</option>
                                    <option value="09:30"  {{ $value->closing =='09:30' ? ' selected' : '' }}>09:30 AM</option>
                                    <option value="10:00"  {{ $value->closing =='10:00' ? ' selected' : '' }}>10:00 AM</option>
                                    <option value="10:30"  {{ $value->closing =='10:30' ? ' selected' : '' }}>10:30 AM</option>
                                    <option value="11:00"  {{ $value->closing =='11:00' ? ' selected' : '' }}>11:00 AM</option>
                                    <option value="11:30"  {{ $value->closing =='11:30' ? ' selected' : '' }}>11:30 AM</option>
                                    <option value="12:00"  {{ $value->closing =='12:00' ? ' selected' : '' }}>12:00 PM</option>
                                    <option value="12:30"  {{ $value->closing =='12:30' ? ' selected' : '' }}>12:30 PM</option>
                                    <option value="13:00"  {{ $value->closing =='13:00' ? ' selected' : '' }}>01:00 PM</option>
                                    <option value="13:30"  {{ $value->closing =='13:30' ? ' selected' : '' }}>01:30 PM</option>
                                    <option value="14:00"  {{ $value->closing =='14:00' ? ' selected' : '' }}>02:00 PM</option>
                                    <option value="14:30"  {{ $value->closing =='14:30' ? ' selected' : '' }}>02:30 PM</option>
                                    <option value="15:00"  {{ $value->closing =='15:00' ? ' selected' : '' }}>03:00 PM</option>
                                    <option value="15:30"  {{ $value->closing =='15:30' ? ' selected' : '' }}>03:30 PM</option>
                                    <option value="16:00"  {{ $value->closing =='16:00' ? ' selected' : '' }}>04:00 PM</option>
                                    <option value="16:30"  {{ $value->closing =='16:30' ? ' selected' : '' }}>04:30 PM</option>
                                    <option value="17:00"  {{ $value->closing =='17:00' ? ' selected' : '' }}>05:00 PM</option>
                                    <option value="17:30"  {{ $value->closing =='17:30' ? ' selected' : '' }}>05:30 PM</option>
                                    <option value="18:00"  {{ $value->closing =='18:00' ? ' selected' : '' }}>06:00 PM</option>
                                    <option value="18:30"  {{ $value->closing =='18:30' ? ' selected' : '' }}>06:30 PM</option>
                                    <option value="19:00"  {{ $value->closing =='19:00' ? ' selected' : '' }}>07:00 PM</option>
                                    <option value="19:30"  {{ $value->closing =='19:30' ? ' selected' : '' }}>07:30 PM</option>
                                    <option value="20:00"  {{ $value->closing =='20:00' ? ' selected' : '' }}>08:00 PM</option>
                                    <option value="20:30"  {{ $value->closing =='20:30' ? ' selected' : '' }}>08:30 PM</option>
                                    <option value="21:00"  {{ $value->closing =='21:00' ? ' selected' : '' }}>09:00 PM</option>
                                    <option value="21:30"  {{ $value->closing =='21:30' ? ' selected' : '' }}>09:30 PM</option>
                                    <option value="22:00"  {{ $value->closing =='22:00' ? ' selected' : '' }}>10:00 PM</option>
                                    <option value="22:30"  {{ $value->closing =='22:30' ? ' selected' : '' }}>10:30 PM</option>
                                    <option value="23:00"  {{ $value->closing =='23:00' ? ' selected' : '' }}>11:00 PM</option>
                                    <option value="23:30"  {{ $value->closing =='23:30' ? ' selected' : '' }}>11:30 PM</option>
                                    <option value="23:59"  {{ $value->closing =='23:59' ? ' selected' : '' }}>11:59 PM</option>


                                </select>
                  </div>
                  <div class='col-md-3 col-sm-12'>
                        </br>
                          <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('{{$value->day}}')"><i class="fa fa-plus"></i></a>
                  </div>
          </div>
                 <div id = "variation_div_{{$value->day}}"></div>

      </div>
      @empty
         <div>
          <div class='row'>
                  <div class='col-md-3 col-sm-12'>
                         
                           <input class="form-control" type="text" name="day" value="Sunday" readonly>
                  </div>
                  <div class="col-md-3 col-sm-12">
                          
                          <select class="form-control" id="start" name="timings[sun][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                  <div class='col-md-3 col-sm-12'>
                          <select class="form-control" id="end" name="timings[sun][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                  <div class='col-md-3 col-sm-12'>
                        </br>
                          <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('sun')"><i class="fa fa-plus"></i></a>
                  </div>
          </div>
                 <div id = "variation_div_sun"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Monday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[mon][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end" name="timings[mon][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('mon')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

                   <div id = "variation_div_mon"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Tuesday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[tue][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end" name="timings[tue][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('tue')"><i class="fa fa-plus"></i></a>
                          </div>
          </div>

                   <div id = "variation_div_tue"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Wednesday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[wed][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>
                            <select class="form-control" id="end" name="timings[wed][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('wed')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_wed"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Thursday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">
                            <select class="form-control" id="start" name="timings[thu][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>
                            <select class="form-control" id="end" name="timings[thu][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('thu')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_thu"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Friday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[fri][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end" name="timings[fri][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('fri')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_fri"></div>
      </div>
      <div>
            <div class='row'>
                      <div class='col-md-3 col-sm-12'>
                               <input class="form-control" type="text" name="day" value="Saturday" readonly>
                      </div>
                          <div class="col-md-3 col-sm-12">
                              <select class="form-control" id="start" name="timings[sat][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                            </div>
                              <div class='col-md-3 col-sm-12'>

                              <select class="form-control" id="end" name="timings[sat][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                            </div>
                            <div class='col-md-3 col-sm-12'>
                            </br>
                              <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('sat')"><i class="fa fa-plus"></i></a>
                            </div>
                     </div>

             <div id = "variation_div_sat"></div>
      </div>
      @endif

      @elseif($mode == 'create')
      <div class='row'>
        <div class='col-md-3 col-sm-12'>
          <label>Day</label>
        </div>
        <div class='col-md-3 col-sm-12'>
          <label>Opening</label>
        </div>
        <div class='col-md-3 col-sm-12'>
          <label>Ending</label>
        </div>
      </div>
      <div>
          <div class='row'>
                  <div class='col-md-3 col-sm-12'>
                           <input class="form-control" type="text" name="day" value="Sunday" readonly>
                  </div>
                  <div class="col-md-3 col-sm-12">
                          <select class="form-control" id="start" name="timings[sun][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                  <div class='col-md-3 col-sm-12'>
                          <select class="form-control" id="end" name="timings[sun][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                  <div class='col-md-3 col-sm-12'>
                        </br>
                          <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('sun')"><i class="fa fa-plus"></i></a>
                  </div>
          </div>
                 <div id = "variation_div_sun"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Monday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[mon][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end" name="timings[mon][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('mon')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

                   <div id = "variation_div_mon"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Tuesday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[tue][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:PM AM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                  </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end" name="timings[tue][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('tue')"><i class="fa fa-plus"></i></a>
                          </div>
          </div>

                   <div id = "variation_div_tue"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Wednesday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[wed][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end" name="timings[wed][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('wed')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_wed"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Thursday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[thu][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end" name="timings[thu][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('thu')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_thu"></div>
      </div>
      <div>
          <div class='row'>
                    <div class='col-md-3 col-sm-12'>
                             <input class="form-control" type="text" name="day" value="Friday" readonly>
                    </div>
                        <div class="col-md-3 col-sm-12">

                            <select class="form-control" id="start" name="timings[fri][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                            <div class='col-md-3 col-sm-12'>

                            <select class="form-control" id="end" name="timings[fri][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                          </div>
                          <div class='col-md-3 col-sm-12'>
                          </br>
                            <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('fri')"><i class="fa fa-plus"></i></a>
                          </div>
                   </div>

             <div id = "variation_div_fri"></div>
      </div>
      <div>
            <div class='row'>
                      <div class='col-md-3 col-sm-12'>
                               <input class="form-control" type="text" name="day" value="Saturday" readonly>
                      </div>
                          <div class="col-md-3 col-sm-12">
                              <select class="form-control" id="start" name="timings[sat][1][start]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                            </div>
                              <div class='col-md-3 col-sm-12'>

                              <select class="form-control" id="end" name="timings[sat][1][end]">
                                    <option value="">Pick a time</option>

                                    <option value="00:00">12:00 AM</option>
                                    <option value="00:30">12:30 AM</option>
                                    <option value="01:00">01:00 AM</option>
                                    <option value="01:30">01:30 AM</option>
                                    <option value="02:00">02:00 AM</option>
                                    <option value="02:30">02:30 AM</option>
                                    <option value="03:00">03:00 AM</option>
                                    <option value="03:30">03:30 AM</option>
                                    <option value="04:00">04:00 AM</option>
                                    <option value="04:30">04:30 AM</option>
                                    <option value="05:00">05:00 AM</option>
                                    <option value="05:30">05:30 AM</option>
                                    <option value="06:00">06:00 AM</option>
                                    <option value="06:30">06:30 AM</option>
                                    <option value="07:00">07:00 AM</option>
                                    <option value="07:30">07:30 AM</option>
                                    <option value="08:00">08:00 AM</option>
                                    <option value="08:30">08:30 AM</option>
                                    <option value="09:00">09:00 AM</option>
                                    <option value="09:30">09:30 AM</option>
                                    <option value="10:00">10:00 AM</option>
                                    <option value="10:30">10:30 AM</option>
                                    <option value="11:00">11:00 AM</option>
                                    <option value="11:30">11:30 AM</option>
                                    <option value="12:00">12:00 PM</option>
                                    <option value="12:30">12:30 PM</option>
                                    <option value="13:00">01:00 PM</option>
                                    <option value="13:30">01:30 PM</option>
                                    <option value="14:00">02:00 PM</option>
                                    <option value="14:30">02:30 PM</option>
                                    <option value="15:00">03:00 PM</option>
                                    <option value="15:30">03:30 PM</option>
                                    <option value="16:00">04:00 PM</option>
                                    <option value="16:30">04:30 PM</option>
                                    <option value="17:00">05:00 PM</option>
                                    <option value="17:30">05:30 PM</option>
                                    <option value="18:00">06:00 PM</option>
                                    <option value="18:30">06:30 PM</option>
                                    <option value="19:00">07:00 PM</option>
                                    <option value="19:30">07:30 PM</option>
                                    <option value="20:00">08:00 PM</option>
                                    <option value="20:30">08:30 PM</option>
                                    <option value="21:00">09:00 PM</option>
                                    <option value="21:30">09:30 PM</option>
                                    <option value="22:00">10:00 PM</option>
                                    <option value="22:30">10:30 PM</option>
                                    <option value="23:00">11:00 PM</option>
                                    <option value="23:30">11:30 PM</option>
                                    <option value="23:59">11:59 PM</option>


                                </select>
                            </div>
                            <div class='col-md-3 col-sm-12'>
                            </br>
                              <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('sat')"><i class="fa fa-plus"></i></a>
                            </div>
                     </div>

             <div id = "variation_div_sat"></div>
      </div>
      @endif
</div>

<script type="text/javascript">

    $(document).ready(function(){
    $(".pwdedit").click(function(){
      console.log('dsfdsf');
      $("#password").removeAttr('disabled');
    });
    $('#email').attr('autocomplete','nope');

       $("#country").on('change', function(){

                 var country = $("#country option:selected").val();

                 $("#state").load('{{url("location/options")}}'+'/'+country);
               })
               $("#state").on('change', function(){

                 var state = $("#state option:selected").val();

                 $("#city").load('{{url("location/options")}}'+'/'+state);
               })



               $("#city").on('change', function(){

                 var city = $("#city option:selected").val();

                 $("#location").load('{{url("location/options")}}'+'/'+city);
               })

             })
</script>

<script type="text/javascript">
   var count=2;
  function addTiming(day){

     var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);

     // $("#TextBoxDiv"+count).load('{{guard_url("restaurant/timing")}}'+'/'+count+'/'+day);
     newTextBoxDiv.after().load('{{guard_url("restaurant/timing")}}'+'/'+count+'/'+day);
  // newTextBoxDiv.after().html(
  //       '<div class="row"><div class="col-md-3 col-sm-12"></div><div class="col-md-3" style="display: block";><input class="timepicker form-control" id="start" type="text" placeholder="Start Time" name='+day+'['+count+'][start]"></div><div class="col-md-3" style="display: block";><input class="timepicker form-control" id="start" type="text" placeholder="End Time" name='+day+'['+count+'][end]"></div><div class="col-md-3" style="display: block";></div></div>');

  newTextBoxDiv.appendTo("#variation_div_"+day);
    count ++;
  }
    $('.timepicker').datetimepicker({

        format: 'HH:mm'

    });

    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
        });
    });

    $(function(){
     var map,myLatlng;
      @if(!empty($restaurant->latitude) && !empty($restaurant->longitude))
         myLatlng = new google.maps.LatLng({!! @$restaurant->latitude !!},{!! @$restaurant->longitude !!});
      @else
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
      @endif
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
    });
       })


</script>
            <script>
$( document ).ready(function() {
    $('#type').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'types',
        labelField: 'types',
        searchField: 'types',
        options: [
@forelse(Master::getCuisines() as $key => $types)

    {types: "{{$types}}" },
    @empty
    @endif
],
        create: function(input) {
            return {
                types: input
            }
        }
    });
});

$('#confirmation_mail').selectize({
    delimiter: ',',
    persist: false,
    create: function(input) {
        return {
            value: input,
            text: input
        }
    }
});
function removeVar(day,key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=timings]").length/3;
  if(countvaraitions > 1){
    var mode = '<?php echo $mode; ?>'; 
        if(mode == 'edit'){
          if($("#TextBoxDiv"+key).length == 0){
                          document.getElementById(key).remove();  

            }
            else{

              document.getElementById('TextBoxDiv'+key).remove();  
            }
        }
        else if(mode == 'create'){
          document.getElementById('TextBoxDiv'+key).remove();   
        }
        document.getElementById(key+'button').style.display = "none";   
  }
  if(countvaraitions == 1){  
    document.getElementById(key).remove();  
     document.getElementById('btn_remove').style.display = "none";
     document.getElementById('variation_name').style.display = "none";
     unset(variation_list);
  }
}
</script>
