<div class="content-wrapper">
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('restaurant::restaurant.title.transaction-log') !!} <small> {!!
                trans('app.manage') !!} {!! trans('restaurant::restaurant.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('restaurant::restaurant.title.transaction-log') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div id='restaurant-restaurant-entry'>
        </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="{!!(request('status') == '')?'active':'';!!}"><a
                        href="{!!guard_url('restaurant/restaurant')!!}">{!!
                        trans('restaurant::restaurant.title.transaction-log') !!}</a></li>

                <li class="pull-right">
                    <span class="actions">
                      <span class="label label-success mr-10">Filter Date </span> 
                    <input type="text" id="datepicker1">
                    <input type="text" id="datepicker2">
                    
                    </span> 
                    
                    </span>
                </li>
            </ul>

            <div class="tab-content" >
                <div id="filter_table">
                <table id="restaurant-restaurant-list" class="table table-striped data-table">
                    <thead class="list_head">
                        <!-- <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset"
                                style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input
                                type="checkbox" id="restaurant-restaurant-check-all"></th> -->
                        <th>Restaurant</th>
                        <th data-field="email">Unique Visits</th>
                        <th data-field="email">Repeat Visits</th>
                        <th data-field="email">Direct Link Visits</th>
                        <th data-field="email">Direct Link Orders</th>
                        <th data-field="email">Referral Visits</th>
                        <th data-field="email">Referral Orders</th>
                        <th data-field="type">orders</th>
                        <th data-field="phone">Total Sales</th>
                        <th data-field="email">Zing Total Fees</th>
                        <th data-field="type">UV:order</th>
                        <th data-field="phone">DL:order</th>
                        <th data-field="email">Refferal:order</th>

                    </thead>
                    <tbody>
                    @foreach($logs as $log)
                        <?php 
                        $unique_visits = Restaurant::transactionLog($log->id, 'unique_visits');
                        $repeat_visits = Restaurant::transactionLog($log->id, 'repeat_visits');                       
                        $direct_link_visits = Restaurant::transactionLog($log->id, 'direct_link_visits');
                        $direct_link_orders =  Restaurant::transactionLog($log->id, 'direct_link_orders');
                        $google_referral_visits = Restaurant::transactionLog($log->id, 'referral_visits');
                        $google_referral_orders =  Restaurant::transactionLog($log->id, 'referral_orders');                   
                        ?>
                        <tr>
                            <td>{{@$log->name}}</td>
                            <td>{{@$unique_visits}}</td>
                            <td>{{@$repeat_visits}}</td>
                            <td>{{@$direct_link_visits}}</td>
                            <td>{{@$direct_link_orders}}</td>
                            <td>{{@$google_referral_visits}}</td>
                            <td>{{@$google_referral_orders}}</td>
                            <td>{{@$log->orders}}</td>
                            <td>{{@$log->total_sales}}</td>
                            <td >{{@$log->ZTF}}</td>
                            <td>{{@$unique_visits}}:{{@$log->orders}}</td>
                            <td>{{@$direct_link_visits}}:{{@$log->orders}}</td>
                            <td >{{@$google_referral_visits}}:{{@$log->orders}}</td>
                        </tr>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>
    </section>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $( function() {
    $( "#datepicker1" ).datepicker();
    $( "#datepicker2" ).datepicker();
  } );
    $('#datepicker2').change(function(e){ 
        e.preventDefault();
        var filter_date1 = $('#datepicker1').val();
        var filter_date2 = $('#datepicker2').val();
   
        filter_ajax(filter_date1,filter_date2);
    
    })
    $('#datepicker1').change(function(e){ 
        e.preventDefault();
        var filter_date1 = $('#datepicker1').val();
        var filter_date2 = $('#datepicker2').val();
     
        filter_ajax(filter_date1,filter_date2);
    })
           
    function filter_ajax(filter_date1,filter_date2){
        $.ajax({
            url: "{{ URL::to('admin/restaurant/transaction-log') }}/",
            data: {filter_date1:filter_date1,filter_date2:filter_date2},
            success: function(response){

                    //console.log("New Order",response);
                    $('#filter_table').html(response);
            },
            error: function(msg) { 
            }
        });
    }
</script>