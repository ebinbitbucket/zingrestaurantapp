    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li role="presentation" class="active"><a href="#restaurant" aria-controls="restaurant" role="tab" data-toggle="tab">Restaurants</a></li>
             <li role="presentation"><a href="#restaurant_timings" aria-controls="restaurant_timings" role="tab" data-toggle="tab">Hours</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#restaurant-restaurant-edit'  data-load-to='#restaurant-restaurant-entry' data-datatable='#restaurant-restaurant-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#restaurant-restaurant-entry' data-href='{{guard_url('restaurant/restaurant')}}/{{$restaurant->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('restaurant-restaurant-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('restaurant/restaurant/'. $restaurant->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="details">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('restaurant::restaurant.name') !!} [{!!$restaurant->name!!}] </div>
                @include('restaurant::admin.restaurant.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>