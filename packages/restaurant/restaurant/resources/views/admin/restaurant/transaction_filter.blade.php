 <table id="restaurant-restaurant-list" class="table table-striped data-table">
        <thead class="list_head">
            <!-- <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset"
                    style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input
                    type="checkbox" id="restaurant-restaurant-check-all"></th> -->
            <th data-field="name">Restaurant</th>
            <th data-field="email">No of Unique Visits</th>
            <th data-field="email">No of Repeat Visits</th>
            <th data-field="email">Direct Link Visits</th>
            <th data-field="email">Direct Link Orders</th>
            <th data-field="email">Referral Visits</th>
            <th data-field="email">Referral Orders</th>
            <th data-field="type">Number of orders</th>
            <th data-field="phone">Total Sales</th>
            <th data-field="email">Zing Total Fees</th>
            <th data-field="type">UV:order</th>
            <th data-field="phone">DL:order</th>
            <th data-field="email">Refferal:order</th>

        </thead>
        <tbody>
            @foreach($logs as $log)
            <?php 
            $unique_visits = Restaurant::transactionLog($log->id, 'unique_visits', $filter_date1, $filter_date2);
            $repeat_visits = Restaurant::transactionLog($log->id, 'repeat_visits', $filter_date1, $filter_date2);
            $direct_link_visits = Restaurant::transactionLog($log->id, 'direct_link_visits', $filter_date1, $filter_date2);
            $direct_link_orders =  Restaurant::transactionLog($log->id, 'direct_link_orders', $filter_date1, $filter_date2);
            $google_referral_visits = Restaurant::transactionLog($log->id, 'referral_visits', $filter_date1, $filter_date2);
            $google_referral_orders =  Restaurant::transactionLog($log->id, 'referral_orders', $filter_date1, $filter_date2);
                 ?>
            <tr>
                <td data-field="name">{{@$log->name}}</td>
                <td>{{@$unique_visits}}</td>
                <td>{{@$repeat_visits}}</td>
                <td>{{@$direct_link_visits}}</td>
                <td>{{@$direct_link_orders}}</td>
                <td>{{@$google_referral_visits}}</td>
                <td>{{@$google_referral_orders}}</td>
                <td data-field="name">{{@$log->orders}}</td>
                <td data-field="email">{{@$log->total_sales}}</td>
                <td data-field="phone">{{@$log->ZTF}}</td>
                <td>{{@$unique_visits}}:{{@$log->orders}}</td>
                <td>{{@$direct_link_visits}}:{{@$log->orders}}</td>
                <td >{{@$google_referral_visits}}:{{@$log->orders}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>