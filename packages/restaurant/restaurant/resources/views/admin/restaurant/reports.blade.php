
<!DOCTYPE html>
<html>
    <head>
        <title>Reports</title>
        <link rel="stylesheet" href="{{asset('assets/vendor/libs/bootstrap.min.css')}}">

        <!-- external libs from cdnjs -->
        <script type="text/javascript" src="{{asset('assets/vendor/libs/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/vendor/libs/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/vendor/libs/d3.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/vendor/libs/jquery.ui.touch-punch.min.js')}}"></script>
        <script src="{{asset('assets/vendor/libs/plotly-basic-latest.min.js')}}"></script>

        <!-- PivotTable.js libs from ../dist -->
        <link rel="stylesheet" type="text/css" href="{{asset('assets/vendor/dist/pivot.css')}}">
        <script type="text/javascript" src="{{asset('assets/vendor/dist/pivot.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/vendor/dist/export_renderers.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/vendor/dist/d3_renderers.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/vendor/dist/plotly_renderers.js')}}"></script>

        <style>
            body {font-family: Verdana;}
            .node {
              border: solid 1px white;
              font: 10px sans-serif;
              line-height: 12px;
              overflow: hidden;
              position: absolute;
              text-indent: 2px;
            }
            .content{
                margin-top:20px;
            }
        </style>

   
    </head>
    <body>
        <script type="text/javascript">
    // This example has all the renderers loaded,


    $(function(){
        pivot();
        $("#date-frm").submit(function(e){
            e.preventDefault();
            var filter = $(this).serialize();
            pivot(filter);

       });

       $("#last_day").click(function(e){
            e.preventDefault();
             var filter='last_day';
             pivot(filter);

       });
       $("#last_7_day").click(function(e){
            e.preventDefault();
             var filter='last_7_day';
             pivot(filter);

       });
       $("#month_to_date").click(function(e){
            e.preventDefault();
             var filter='month_to_date';
             pivot(filter);

       });
       $("#last_month").click(function(e){
            e.preventDefault();
             var filter='last_month';
             pivot(filter);

       });
       $("#last_6_avg").click(function(e){
            e.preventDefault();
             var filter='last_6_avg';
             pivot(filter);

       });
       $("#last_dow").click(function(e){
            e.preventDefault();
             var filter='last_dow';
             pivot(filter);

       });
       $("#published").click(function(e){
            e.preventDefault();
             var filter='published';
             pivot(filter);

       });
       

     });
     function pivot(filter=0){
        var derivers = $.pivotUtilities.derivers;
        var renderers = $.extend(
            $.pivotUtilities.renderers,
            $.pivotUtilities.plotly_renderers,
            $.pivotUtilities.d3_renderers,
            $.pivotUtilities.export_renderers
            );

        $.getJSON("{{guard_url('restaurant/restaurant/report')}}"+ '?' + filter, function(mps) {
            $("#output").pivotUI(mps, {
                renderers: renderers,
                // derivedAttributes: {
                //     "Age Bin": derivers.bin("Age", 10),
                //     "Gender Imbalance": function(mp) {
                //         return mp["Gender"] == "Male" ? 1 : -1;
                //     }
                // },
                 cols: ["Published"], rows: ["Name","Total","Completed Count","Cancelled Count","Count","Average"],
                rendererName: "Heatmap"
            });
        });
     }
        </script>
<div class="container-fluid">

<div class="content">
    <button type="button" id="last_day">Last day</button>
    <button type="button" id="last_7_day">Last 7 day</button>
    <button type="button" id="month_to_date">Month To Date</button>
    <button type="button" id="last_month">Last Month</button>
    <button type="button" id="last_6_avg">Last 6 d.o.w  avg</button>
    <button type="button" id="last_dow"> Last d.o.w differential</button>
    <button type="button" id="published"> Published</button>

   
{{-- <form class="form-inline" id="date-frm" action="">
  <div class="form-group">
    <label for="frm_date">Date From:</label>
    <input type="date" value="{{date('yy-m-d', strtotime('-2 month'))}}" class="form-control" name="frm_date" id="frm_date">
  </div>
  <div class="form-group">
    <label for="to_date">Date To:</label>
    <input type="date" value="{{date('yy-m-d')}}" class="form-control" name="to_date" id="to_date">
  </div>
  
  <button type="submit" class="btn btn-default">Submit</button>
</form> --}}
</div>

        <div id="output" style="margin: 30px;"></div>
        </div>

    </body>
</html>
