<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('restaurant::restaurant.name') !!} <small> {!! trans('app.manage') !!} {!! trans('restaurant::restaurant.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('restaurant::restaurant.names') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div id='restaurant-restaurant-entry'>
    </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                    <li class="{!!(request('status') == '')?'active':'';!!}"><a href="{!!guard_url('restaurant/restaurant')!!}">{!! trans('restaurant::restaurant.names') !!}</a></li>
                    
                    <li class="pull-right">
                    <span class="actions">
                    <!--   
                    <a  class="btn btn-xs btn-purple"  href="{!!guard_url('restaurant/restaurant/reports')!!}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Reports</span></a>
                    @include('restaurant::admin.restaurant.partial.actions')
                    -->
                    <a type="button" class="btn btn-success btn-sm" href='{{guard_url("restaurant/updatemaster")}}' ><i class="fa fa-refresh"></i> Update Restaurant Masters </a>
                    @include('restaurant::admin.restaurant.partial.filter')
                    @include('restaurant::admin.restaurant.partial.column')
                    </span> 
                </li>
            </ul>

            <div class="tab-content">
                <table id="restaurant-restaurant-list" class="table table-striped data-table">
                    <thead class="list_head">
                        <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset" style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input type="checkbox" id="restaurant-restaurant-check-all"></th>
                        <th data-field="name">{!! trans('restaurant::restaurant.label.name')!!}</th>
                        <th data-field="type">{!! trans('restaurant::restaurant.label.type')!!}</th>
                    <th data-field="phone">{!! trans('restaurant::restaurant.label.phone')!!}</th>
                    <th data-field="email">{!! trans('restaurant::restaurant.label.email')!!}</th>
                    <th data-field="address">{!! trans('restaurant::restaurant.label.address')!!}</th>
                    
                    
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

var oTable;
var oSearch;
$(document).ready(function(){
 
    app.load('#restaurant-restaurant-entry', '{!!guard_url('restaurant/restaurant/0')!!}');
    oTable = $('#restaurant-restaurant-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }], 
        
        "responsive" : true,
        "order": [[1, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('restaurant/restaurant') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'name'},
            {data :'type'},
            {data :'phone'},
            {data :'email'},
            {data :'address'},
            
            
        ],
        "pageLength": 10
    });

    $('#restaurant-restaurant-list tbody').on( 'click', 'tr td:not(:first-child)', function (e) {
        e.preventDefault();

        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#restaurant-restaurant-list').DataTable().row( this ).data();
        $('#restaurant-restaurant-entry').load('{!!guard_url('restaurant/restaurant')!!}' + '/' + d.id);
    });

    $('#restaurant-restaurant-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent'); 
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#restaurant-restaurant-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }
        
    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#restaurant-restaurant-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#restaurant-restaurant-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});
</script>