<div class="tab-content">
  
  <div role="tabpanel" class="tab-pane active" id="restaurant">  
    <div class='row'>
        <div class='col-md-3 col-sm-12'>
          {!! Form::text('name')
          -> label(trans('restaurant::restaurant.label.name'))
          -> required()
          -> placeholder(trans('restaurant::restaurant.placeholder.name'))!!}
        </div>
        <div class='col-md-3 col-sm-12'>
            {!! Form::numeric('phone')
            -> label(trans('restaurant::restaurant.label.phone'))
            -> required()
            -> placeholder(trans('restaurant::restaurant.placeholder.phone'))!!}
        </div>
        <div class='col-md-3 col-sm-12'>
             {!! Form::email('email')
              -> label(trans('restaurant::restaurant.label.email'))
              -> required()
              -> placeholder(trans('restaurant::restaurant.placeholder.email'))!!}
        </div>
        <div class='col-md-3 col-sm-12'>
              {!! Form::text('type')
              ->id('type')
               -> label(trans('restaurant::restaurant.label.type'))
               -> placeholder(trans('restaurant::restaurant.placeholder.type'))!!}
        </div>
      </div>
      <div class='row'>
        <div class='col-md-3 col-sm-12'>
              {!! Form::text('price_range_min')
              -> label(trans('restaurant::restaurant.label.price_range_min'))
              -> placeholder(trans('restaurant::restaurant.placeholder.price_range_min'))!!}
        </div>
        <div class='col-md-2 col-sm-12'>
              {!! Form::text('price_range_max')
              -> label(trans('restaurant::restaurant.label.price_range_max'))
              -> placeholder(trans('restaurant::restaurant.placeholder.price_range_max'))!!}
        </div>
        <div class='col-md-5 col-sm-12'>
              {!! Form::text('slug')
              -> label(trans('restaurant::restaurant.label.slug'))
              -> placeholder(trans('restaurant::restaurant.placeholder.slug'))!!}
        </div>
        <div class='col-md-3 col-sm-12'>
             {!! Form::select('delivery')
             -> options(trans('restaurant::restaurant.options.delivery'))
             -> label(trans('restaurant::restaurant.label.delivery'))
             -> placeholder(trans('restaurant::restaurant.placeholder.delivery'))!!}
        </div>
        <div class='col-md-3 col-sm-12'>
          {!! Form::select('postmate_delivery')
          -> options(trans('restaurant::restaurant.options.delivery'))
          -> label('Postmate Delivery')
          -> placeholder('Postmate Delivery')!!}
     </div>
         <div class='col-md-3 col-sm-12'>
            {!! Form::text ('delivery_charge')
            -> label('Delivery charge')
            -> placeholder('Please enter delivery charge')!!}
          </div>
          
        
           </div>
           <div class="row">
            <div class='col-md-3 col-sm-12'>
              {!! Form::select('category_name')
              -> options(trans('restaurant::restaurant.options.category_name'))
              -> label('Category')
              -> required()
              -> placeholder('Category')!!}
          </div>
            <div class='col-md-3 col-sm-12'>
              {!! Form::text('keywords')
              -> label(trans('restaurant::restaurant.label.keywords'))
              -> placeholder(trans('restaurant::restaurant.placeholder.keywords'))!!}
          </div>
            <div class='col-md-3 col-sm-6'>
             {!! Form::text('min_order_count')
             -> label('Minimum Order Amount')
             -> placeholder('Minimum Order Amount')!!}
        </div>
             <div class='col-md-3 col-sm-12'>
             {!! Form::numeric('zipcode')
             -> label(trans('restaurant::restaurant.label.zipcode'))
             -> placeholder(trans('restaurant::restaurant.placeholder.zipcode'))!!}
          </div>
        </div>
         <div class="row">
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('CCR')
           -> label('Credit Card % Rate')
           -> placeholder('Credit Card % Rate')!!}

         </div>
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('CCF')
           -> label('Credit Card Transaction Fee')
           -> placeholder('Credit Card Transaction Fee')!!}

         </div>
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('ZR')
           -> label('Eatery % Rate Zing Fee')
           -> placeholder('Eatery % Rate Zing Fee')!!}

         </div>
         <div class='col-md-3 col-sm-12'>

           {!! Form::text('ZF')
           -> label('Eatery Zing Flat Fee')
           -> placeholder('Eatery Zing Flat Fee')!!}

         </div>
       </div>
                 <div class="row">
                   <div class='col-md-3 col-sm-12'>

           {!! Form::text('ZC')
           -> label('Customer Zing Fee')
           -> placeholder('Customer Zing Fee')!!}

         </div>
            <div class='col-md-3 col-sm-12'>

           {!! Form::text('tax_rate')
           -> label('Tax Rate')
           -> placeholder('Tax Rate')!!}

         </div>
         <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[facebook]')
             -> label('Facebook Link')
             -> placeholder('Facebook Link')!!}
        </div>
        <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[twitter]')
             -> label('Twitter Link')
             -> placeholder('Twitter Link')!!}
        </div>
        
      </div>
      <div class="row">
         <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[youtube]')
             -> label('Youtube Link')
             -> placeholder('Youtube Link')!!}
        </div>
        <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[linkedln]')
             -> label('Linkedln Link')
             -> placeholder('Linkedln Link')!!}
        </div>
         <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[instagram]')
             -> label('Instagram Link')
             -> placeholder('Instagram Link')!!}
        </div>
        <div class='col-md-3 col-sm-6'>
             {!! Form::text('social_media_links[pintrest]')
             -> label('Pinterest Link')
             -> placeholder('Pinterest Link')!!}
        </div>
        
      </div>
        <div class="row">
              <div class='col-md-4 col-sm-12'>
            {!! Form::text ('address')
            ->id('txtPlaces')
            -> label(trans('restaurant::restaurant.label.address'))
            -> placeholder(trans('restaurant::restaurant.placeholder.address'))!!}
          </div>
         
          <div class='col-md-4 col-sm-12'>
             {!! Form::text('latitude')
             -> label(trans('restaurant::restaurant.label.latitude'))
             -> placeholder(trans('restaurant::restaurant.placeholder.latitude'))!!}
         </div>
         <div class='col-md-4 col-sm-12'>

           {!! Form::text('longitude')
           -> label(trans('restaurant::restaurant.label.longitude'))
           -> placeholder(trans('restaurant::restaurant.placeholder.longitude'))!!}

         </div>
         </div>
          
          <div id="map_canvas" style="height: 280px;width: 100%; margin-bottom: 30px"></div>
         
          <div class="row">
            <div class='col-md-6 col-sm-12'>
              <div class="row">
              <div class="col-md-12">
                <div class="form-group row">
                    <label for="logo" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::restaurant.label.logo') }}
                    </label>
                 
                    <div class='col-lg-12 col-sm-12' style="width: 250px; height: 200px;">
                    {!! $restaurant->files('logo')
                    ->size('xs')
                    ->show()!!}
                    </div>
                </div>
              </div>
            </div>
             <div class="row">
              <div class="col-md-12">
                <div class="form-group row">
                    <label for="logo" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::restaurant.label.offer') }}
                    </label>
                   
                    <div class='col-lg-12 col-sm-12' style="width: 250px; height: 200px;">
                    {!! $restaurant->files('offer')
                    ->size('xs')
                    ->show()!!}
                    </div>
                </div>
              </div>
            </div>
          </div>
            <div class='col-md-6 col-sm-12'>
              <div class="row">
              <div class="col-md-12">
               {!! Form::textarea ('description')
                -> dataUpload(trans_url($restaurant->getUploadURL('description')))
               -> label(trans('restaurant::restaurant.label.description'))
               -> placeholder(trans('restaurant::restaurant.placeholder.description'))
               ->rows(10)!!}
            </div>
          </div>
           <div class="row">
            <div class='col-md-12 col-sm-12'>
                <div class="form-group row">
                    <label for="gallery" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('restaurant::restaurant.label.gallery') }}
                    </label>
                   
                    <div class='col-lg-12 col-sm-12' >
                    {!! $restaurant->files('gallery')
                    ->size('xs')
                    ->show()!!}
                    </div>
                </div>
              </div>
            </div>
    </div>
  </div>
</div>
     <div role="tabpanel" class="tab-pane" id="timings"> 
      <div class='row'>
        <div class='col-md-3 col-sm-12'>
          <label>Day</label>
        </div>
        <div class='col-md-3 col-sm-12'>
          <label>Opening</label>
        </div>
        <div class='col-md-3 col-sm-12'>
          <label>Ending</label>
        </div>
      </div>
       @foreach($restaurant_timings as $key=>$value)
              <div>  
          <div class='row'>
                  <div class='col-md-3 col-sm-12'>
                           <input class="form-control" type="text" name="day" value="{{ucfirst($value->day)}}" readonly>
                  </div>
                  <div class="col-md-3 col-sm-12">
                          <input class="timepicker form-control" id="start" type="text" placeholder="Start Time" name="timings[{{$value->day}}][{{$value->id}}][start]" value="{{$value->opening}}">
                  </div>
                  <div class='col-md-3 col-sm-12'>
                          <input class="timepicker form-control" id="end" type="text" placeholder="End Time" name="timings[{{$value->day}}][{{$value->id}}][end]" value="{{$value->closing}}">
                  </div>
                  <div class='col-md-3 col-sm-12'>
                        </br>
                          <a class="btn btn-primary btn-sm" id="'+count+'button" onclick="addTiming('{{$value->day}}')"><i class="fa fa-plus"></i></a>
                  </div>
          </div>
                 <div id = "variation_div_sun"></div>
      </div>
      @endforeach
  </div>
    <div role="tabpanel" class="tab-pane" id="category"> 
      <table class="table table-striped data-table">
        <thead class="list_head">
        <th>{!!trans('restaurant::category.label.name')!!}</th>
        <th>{!!trans('restaurant::category.label.preparation_time')!!}</th>
    </thead>
    @foreach(Restaurant::showCategory($restaurant->id) as $category) 
      <tr>
        <td>{!!($category->name)!!}</td>
        <td>{!!($category->preparation_time)!!}</td>
      </tr>
    @endforeach  
    </table>
  </div>

  <div role="tabpanel" class="tab-pane" id="addon"> 
      <table class="table table-striped data-table">
        <thead class="list_head">
        <th>{!!trans('restaurant::addon.label.name')!!}</th>
    </thead>
    @foreach(Restaurant::showAddon($restaurant->id) as $addon)
      <tr>
        <td>{!!($addon->name)!!}</td>
      </tr>
    @endforeach  
    </table>
  </div>

  <div role="tabpanel" class="tab-pane" id="menus"> 
      <table class="table table-striped data-table">
        <thead class="list_head">
        <th>{!!trans('restaurant::menu.label.category_id')!!}</th>
        <th>{!!trans('restaurant::menu.label.name')!!}</th>
        <th>{!!trans('restaurant::menu.label.price')!!}</th>
    </thead>
     @foreach(Restaurant::showMenus($restaurant->id) as $menu)  
        @foreach(Restaurant::showMenuCategory($menu->category_id) as $cat)
        <td>{!!($cat->name)!!}</td>
        <td>{!!($menu->name)!!}</td>
        <td>{!!($menu->price)!!}</td>
      </tr>
    @endforeach 
    @endforeach  
    </table>
  </div>






            <script type="text/javascript">
             $(document).ready(function(){
              
               $("#country").on('change', function(){
                 
                 var country = $("#country option:selected").val();
                 
                 $("#state").load('{{url("location/options")}}'+'/'+country);
               })
               $("#state").on('change', function(){
                 
                 var state = $("#state option:selected").val();
                
                 $("#city").load('{{url("location/options")}}'+'/'+state);
               })
              
               
               
               $("#city").on('change', function(){
                 
                 var city = $("#city option:selected").val();
                
                 $("#location").load('{{url("location/options")}}'+'/'+city);
               })
                
             })
       </script>

       <script type="text/javascript">

    $('.timepicker').datetimepicker({

        format: 'hh:mm A'

    }); 


    $(function(){
     var map,myLatlng;   
      @if(!empty($restaurant->latitude) && !empty($restaurant->longitude))
         myLatlng = new google.maps.LatLng({!! @$restaurant->latitude !!},{!! @$restaurant->longitude !!});
      @else
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
      @endif      
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
    });
       })

</script>
