
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li role="presentation" class="active"><a href="#restaurant" aria-controls="restaurant" role="tab" data-toggle="tab">Restaurants</a></li>
             <li role="presentation"><a href="#restaurant_timings" aria-controls="restaurant_timings" role="tab" data-toggle="tab">Hours</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#restaurant-restaurant-create'  data-load-to='#restaurant-restaurant-entry' data-datatable='#restaurant-restaurant-list' onclick="test()"><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CLOSE' data-load-to='#restaurant-restaurant-entry' data-href='{{guard_url('restaurant/restaurant/0')}}'><i class="fa fa-times-circle"></i> {{ trans('app.close') }}</button>
            </div>
        </ul>
        <div class="tab-content clearfix">
            {!!Form::vertical_open()
            ->id('restaurant-restaurant-create')
            ->method('POST')
            ->files('true')
            ->action(guard_url('restaurant/restaurant'))!!}
            <div class="tab-pane active" id="details">
                <div class="tab-pan-title">  {{ trans('app.new') }}  [{!! trans('restaurant::restaurant.name') !!}] </div>
                @include('restaurant::admin.restaurant.partial.entry', ['mode' => 'create'])
            </div>
            {!! Form::close() !!}
        </div>
    </div>
<script type="text/javascript">
    function test() {
        
    }
</script>