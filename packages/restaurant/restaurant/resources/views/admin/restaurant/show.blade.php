    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            

            <li role="presentation" class="active"><a href="#restaurant" aria-controls="restaurant" role="tab" data-toggle="tab">Restaurant</a></li>
            <li role="presentation"><a href="#timings" aria-controls="timings" role="tab" data-toggle="tab">Hours</a></li>
             <li role="presentation"><a href="#category" aria-controls="category" role="tab" data-toggle="tab">Categories</a></li>
             <li role="presentation"><a href="#menus" aria-controls="menus" role="tab" data-toggle="tab">Menus</a></li>
             <li role="presentation"><a href="#addon" aria-controls="addon" role="tab" data-toggle="tab">Add-ons</a></li>
            <div class="box-tools pull-right">
            <a type="button" target='_blank' class="btn btn-primary btn-sm" href='{{ guard_url('restaurant/menu/download-csv')}}/{{$restaurant->getRouteKey()}}' id="download_csv"><i class="fa fa-file-excel-o"></i> Download Menus </a>
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#restaurant-restaurant-entry' data-href='{{guard_url('restaurant/restaurant/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($restaurant->id )
                 <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#restaurant-restaurant-entry' data-href='{{ guard_url('restaurant/restaurant/publish') }}/{{$restaurant->getRouteKey()}}'><i class="fa fa-upload"></i> @if($restaurant->published == 'Published') Unpublish @else Publish @endif</button>
                 <a  href='{{ guard_url('restaurant/restaurant/loginas') }}/{{$restaurant->getRouteKey()}}' target="_blank"><button type="button" class="btn btn-primary btn-sm">Login As</button></a>
<!-- 
                  <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#restaurant-restaurant-entry' data-href='{{ guard_url('restaurant/restaurant/loginas') }}/{{$restaurant->getRouteKey()}}'>Login As</button> -->
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#restaurant-restaurant-entry' data-href='{{ guard_url('restaurant/restaurant') }}/{{$restaurant->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#restaurant-restaurant-entry' data-datatable='#restaurant-restaurant-list' data-href='{{ guard_url('restaurant/restaurant') }}/{{$restaurant->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('restaurant-restaurant-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('restaurant/restaurant'))!!} 

            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('restaurant::restaurant.name') !!}  [{!! $restaurant->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('restaurant::admin.restaurant.showall', ['mode' => 'show'])
                </div>
                
            </div>
        {!! Form::close() !!}
    </div>
     <script>
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
      alert(msg);
    }
    
    // $('#download_csv').click(function(){
    //   var filter = $("#form-search1").serialize();
    // //  $('#download_csv').attr('href',"http://zingmyorder.webnapps.in/admin/restaurant/menu/download/csv"+ "/" + filter);
    //   $('#download_csv').attr('href','{{ guard_url('restaurant/menu/download/csv') }}'+ "/" + filter);

    //   $('#download_csv').attr("target", "_blank");
    // });
  </script>