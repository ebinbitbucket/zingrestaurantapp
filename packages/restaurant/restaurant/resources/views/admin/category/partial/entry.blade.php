            <div class='row'>
              <div class='col-md-4 col-sm-6'>
                       {!! Form::select('restaurant_id')
                       -> options(Restaurant::getRestaurant())
                       ->required()
                       -> label(trans('restaurant::category.label.restaurant_id'))
                       -> placeholder(trans('restaurant::category.placeholder.restaurant_id'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       ->required()
                       -> label(trans('restaurant::category.label.name'))
                       -> placeholder(trans('restaurant::category.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('preparation_time')
                       ->required()
                       -> label(trans('restaurant::category.label.preparation_time'))
                       -> placeholder(trans('restaurant::category.placeholder.preparation_time'))!!}
                </div>
            </div>

            