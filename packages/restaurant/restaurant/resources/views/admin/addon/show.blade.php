    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('restaurant::addon.name') !!}</a></li>
            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#restaurant-addon-entry' data-href='{{guard_url('restaurant/addon/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($addon->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#restaurant-addon-entry' data-href='{{ guard_url('restaurant/addon') }}/{{$addon->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#restaurant-addon-entry' data-datatable='#restaurant-addon-list' data-href='{{ guard_url('restaurant/addon') }}/{{$addon->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('restaurant-addon-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('restaurant/addon'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('restaurant::addon.name') !!}  [{!! $addon->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('restaurant::admin.addon.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>