<div class="row">
  <div class="col-md-6">
    <div class="row">
      <div class='col-md-12 col-sm-12'>
           {!! Form::select('restaurant_id')
           -> options(Restaurant::getRestaurant())
           ->required()
           -> label(trans('restaurant::addon.label.restaurant_id'))
           -> placeholder(trans('restaurant::addon.placeholder.restaurant_id'))!!}
      </div>
    </div>
    <div class="row">
      <div class='col-md-12 col-sm-12'>
             {!! Form::text('name')
             ->required()
             -> label(trans('restaurant::addon.label.name'))
             -> placeholder(trans('restaurant::addon.placeholder.name'))!!}
      </div>
    </div>
  </div>
  <div class="col-md-6">
    @if($mode == 'show')
        @if(!empty($addon_variations)) 
          <div class="row">
            <label for="" style="display: block";>Variations</label>
          </div>
          @foreach($addon_variations as $key=>$value)
            <div class="col-md-12 col-sm-12" >
              <div class="form-group" id="{{$key}}" style="display: block";>
                <span><input class="form-control"  id="{{$key}}" type="text" name="variations[{{$key}}]['name']" value="{{$value->name}}" >
                </span>
                <span><input class="form-control"  id="{{$key}}" type="text" name="variations[{{$key}}]['description']" value="{{$value->description}}" >
                </span>
                <span><input class="form-control"  id="{{$key}}" type="text" name="variations[{{$key}}]['price']" value="{{$value->price}}" >
                </span>
              </div>
            </div>
          @endforeach
        @endif 
    @endif
    @if(($mode == 'edit') || ($mode == 'create'))
      <div class="row">
        <a class="btn btn-primary btn-sm" onclick="addVariation()" ><i class="fa fa-plus-circle"></i> Add Variation</a>
      </div>
      @if(!empty($addon_variations))
        </br><div class="row" class="form-control"> 
          <div class="col-md-12">
          <label for="" style="display: block";>Variations</label></div>
        </div>
        @foreach($addon_variations as $key=>$value)
          <div class="row">
            <div id="{{$key}}" style="display: block";>
              <div class="col-md-10">
                <div class="row">
                  <div class="col-md-6">
                      <input class="form-control"  id="{{$key}}" type="text" name="variations[{{$key}}][name]" value="{{$value->name}}" >
                  </div>
                  <div class="col-md-6">
                    <input class="form-control"  id="{{$key}}" type="number" name="variations[{{$key}}][price]" value="{{$value->price}}" onchange="pricetest(event)">
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <input class="form-control"  id="{{$key}}" type="text" name="variations[{{$key}}][description]" value="{{$value->description}}" >
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                    <a class="btn btn-primary btn-sm" id="{{$key}}button" onclick="removeVar({!!$key!!})"><i class="fa fa-close"></i></a>
              </div>
            </div>
          </div>
          </br>
        @endforeach
      @endif 
      <div class="row">
        </br><label for="" id="variation_name" style="display: none";></label>
      </div>
      <div class="row">
        <div class='col-md-12 col-sm-12'>
          <div id = "variation_div"></div>
        </div>
      </div>
    @endif
  </div>
</div>
 <script type="text/javascript">
  $(document).ready(function(){
    document.getElementById("variation_name").style.display = "none";

  });
 function pricetest(event) {
     var selectElement = event.target;
    var value = selectElement.value;
    if( value < 0)
       {
       toastr.error('Price should not be a negative number', 'Error');
      event.target.value='';
       }
      
 }
  <?php if (!empty(@$addon_variations)):  $s = count($addon_variations);?>
    var count = {!!$s!!} + 1; 
    <?php else: ?>  var count = 1;
  <?php endif ?>
 var variation_list;
function addVariation()
{
  document.getElementById('variation_name').style.display = "block";
   var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);
                
  newTextBoxDiv.after().html(
        '<div class="row"><div class="col-md-10"><div class="row"><div class="col-md-6" style="display: block";><input type="text" class="form-control" name="variations['+count+'][name]" id="'+count+' value="" placeholder="Variation Name*" required="required"></div><div class="col-md-6" style="display: block";><input type="number" class="form-control" name="variations['+count+'][price]" id="'+count+' value="" placeholder="Price*" required="required" onchange="pricetest(event)"></div></div><div class="row"><div class="col-md-12" style="display: block";><input type="text" class="form-control" name="variations['+count+'][description]" id="'+count+' value="" placeholder="Description"></div></div></div><div class="col-md-2"><a class="btn btn-primary btn-sm" id="'+count+'button" onclick="removeVar('+count+')"><i class="fa fa-close"></i></a></div></div></br>');
            
  newTextBoxDiv.appendTo("#variation_div");
    count ++;
    document.getElementById('btn_remove').style.display = "block";
    variation_list.push(document.getElementById("variations["+count+"]"));
    alert(variation_list);
    //alert("helloworld");
}

function removeVariation()
{ 
    //alert("nothing happened " );
    if(count > 1)
    {
        var lastCount = count - 1;
        var variation_id = "variation"+lastCount;
        document.getElementById(variation_id).remove();
    }


    if(count == 1)
    {
        document.getElementById('btn_remove').style.display = "none";
        document.getElementById('variation_name').style.display = "none";
    }
    else
    {
        count--;
    }

}


function removeVar(key) 
{ 
  var countvaraitions = document.querySelectorAll("[name^=variations]").length/3; 
 var mode = '<?php echo $mode; ?>'; 
 if(countvaraitions > 1){
        if(mode == 'edit'){
          if($("#TextBoxDiv"+key).length == 0){
                        document.getElementById(key).remove();  

          }
          else{

            document.getElementById('TextBoxDiv'+key).remove();  
          }
          
        }
        else if(mode == 'create'){
          document.getElementById('TextBoxDiv'+key).remove();   
        }
        document.getElementById(key+'button').style.display = "none";   
  }
  if(countvaraitions == 1){  
    if(mode == 'create' || mode=='edit'){
        if($("#TextBoxDiv"+key).length == 0){
           document.getElementById(key).remove();  

          }
          else{
            document.getElementById('TextBoxDiv'+key).remove();  
          }
           
        }
    document.getElementById(key).remove();  
     document.getElementById('btn_remove').style.display = "none";
     document.getElementById('variation_name').style.display = "none";
     unset(variation_list);
  }
}
        


</script>
