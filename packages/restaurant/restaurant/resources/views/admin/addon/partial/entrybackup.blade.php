  <div class="col-md-6">
    <div class='col-md-10 col-sm-12'>
           {!! Form::select('restaurant_id')
           -> options(Restaurant::getRestaurant())
           ->required()
           -> label(trans('restaurant::addon.label.restaurant_id'))
           -> placeholder(trans('restaurant::addon.placeholder.restaurant_id'))!!}
    </div>

    <div class='col-md-10 col-sm-12'>
           {!! Form::text('name')
           ->required()
           -> label(trans('restaurant::addon.label.name'))
           -> placeholder(trans('restaurant::addon.placeholder.name'))!!}
    </div>
  </div>
  <div class="col-md-6">
  @if($mode == 'show')
  
       @if(!empty($addon->variations)) 
       <label for="" style="display: block";>Variations</label>
        @foreach($addon->variations as $key=>$value)
               

         <div class="col-md-12 col-sm-12" >
           <div class="form-group" id="{{$key}}" style="display: block";>
            <span><input class="form-control"  id="{{$key}}" type="text" name="variations[{{$key}}]" value="{{$value}}" >
             </span>
          </div>
         </div>

        @endforeach
      @endif 

      
     @endif
  @if(($mode == 'edit') || ($mode == 'create'))
  <div class="row">
  <a class="btn btn-primary btn-sm" onclick="addVariation()"><i class="fa fa-plus-circle"></i> Add Variation</a>
</div>

       @if(!empty($addon->variations)) 
       <label for="" style="display: block";>Variations</label>
        @foreach($addon->variations as $key=>$value)
               <div class="row">
        
         <div class="col-md-12 col-sm-12" >
           <div class="form-group" id="{{$key}}" style="display: block";>
            <div class="col-md-10">
            <input class="form-control"  id="{{$key}}" type="text" name="variations[{{$key}}]" value="{{$value}}" >
          </div>
          <div class="col-md-2">
              <a class="btn btn-primary btn-sm" id="{{$key}}button" onclick="removeVar({!!$key!!})"><i class="fa fa-close"></i></a>
            </div>
          </div>
         </div>
</div>
        @endforeach
      @endif 
       
      </br><label for="" id="variation_name" style="display: none";>Variation Name</label>
        <div class="row">
         <div class='col-md-12 col-sm-12'>
            <div id = "variation_div"></div>
          </div>
        </div>
     @endif
</div>

 <script type="text/javascript">
  $(document).ready(function(){
    document.getElementById("variation_name").style.display = "none";
  });

  <?php if (!empty(@$addon->variations)):  $s = last(array_keys($addon->variations));?>
    var count = {!!$s!!} + 1;
    <?php else: ?>  var count = 1;
  <?php endif ?>
 var variation_list;
function addVariation()
{
  document.getElementById('variation_name').style.display = "block";
   var newTextBoxDiv = $(document.createElement('div'))
       .attr("id", 'TextBoxDiv' + count);
                
  newTextBoxDiv.after().html(
        '<div class="col-md-10" style="display: block";><input type="text" class="form-control" name="variations['+count+']" id="'+count+' value="" ></div><div class="col-md-2"><a class="btn btn-primary btn-sm" id="'+count+'button" onclick="removeVar('+count+')"><i class="fa fa-close"></i></a></div>');
            
  newTextBoxDiv.appendTo("#variation_div");
    count ++;
    document.getElementById('btn_remove').style.display = "block";
    variation_list.push(document.getElementById("variations["+count+"]"));
    alert(variation_list);
    //alert("helloworld");
}

function removeVariation()
{
    //alert("nothing happened " );
    if(count > 1)
    {
        var lastCount = count - 1;
        var variation_id = "variation"+lastCount;
        document.getElementById(variation_id).remove();
    }


    if(count == 1)
    {
        document.getElementById('btn_remove').style.display = "none";
        document.getElementById('variation_name').style.display = "none";
    }
    else
    {
        count--;
    }

}


function removeVar(key) 
{
  var countvaraitions = document.querySelectorAll("[name^=variations]").length;
  if(countvaraitions > 1){
    var mode = '<?php echo $mode; ?>'; 
        if(mode == 'edit'){
          document.getElementById(key).remove();   
        }
        else if(mode == 'create'){
          document.getElementById('TextBoxDiv'+key).remove();   
        }
        document.getElementById(key+'button').style.display = "none";   
  }
  if(countvaraitions == 1){ 
     document.getElementById('btn_remove').style.display = "none";
     document.getElementById('variation_name').style.display = "none";
  }
}
        


</script>
