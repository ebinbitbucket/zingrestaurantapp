             <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="">Min</label>
                            {!! Form::text('av_'.$id.'[min]')
                                -> label('')
                                -> placeholder('Minimum')
                                ->addClass('form-control')
                                -> required()
                            !!}
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="">Max</label>
                            {!! Form::text('av_'.$id.'[max]')
                    -> label('')
                    ->addClass('form-control')
                    -> placeholder('Maximum')
                    -> required()
                    !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group border-0">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" name="av_{{$id}}[required]" value="Yes" id="customCheck1" checked="">
                                                                            <label class="custom-control-label" for="customCheck1">Required</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <b>Addon Variations</b></br>
                                                            @foreach($addon as $key => $variation)
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="form-group border-0">
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox" class="custom-control-input" id="variation_id_{{$key}}" name="av_{{$id}}[variation_list][{{$key}}][name]" value="{!! $variation->name !!}" onclick="enableVariation(this,'{{$id}}','{{$key}}')"> 
                                                                            <label class="custom-control-label" for="variation_id_{{$key}}">{!!$variation->name !!}</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col">
                                                                     <div class="form-group">
                                                                        <label for="">Price</label>
                                                                        {!! Form::text('av_'.$id.'[variation_list]['.$key.'][price]')
                      ->id($id.'_'.$key.'price')
                      -> label('')
                      ->forceValue($variation->price)
                      ->disabled()
                      -> placeholder('Price')
                      -> required()
                      !!}
                                                                    </div>
                                                                </div>
                                                                {!! Form::hidden('av_'.$id.'[variation_list]['.$key.'][description]')
                    ->id($id.'_'.$key.'desc')
                    -> label('Description')
                    ->forceValue($variation->description)
                    ->disabled()
                    -> placeholder('Please enter Description')
                    -> required()
                    !!}

                    {!! Form::hidden('av_'.$id.'[variation_list]['.$key.'][id]')
                    ->id($id.'_'.$key.'id')
                    -> label('Id')
                    ->disabled()
                    ->forceValue($variation->id)
                    -> placeholder('Please enter ID')
                    -> required()
                    !!}
                                                            </div>
                                                            @endforeach


                                                             
           
<script type="text/javascript">
function enableVariation(element,addon,id){
    if(element.checked == true){
      document.getElementById(addon+'_'+id+'price').disabled = false;
      document.getElementById(addon+'_'+id+'desc').disabled = false;
      document.getElementById(addon+'_'+id+'id').disabled = false;
    }
    else
    {
      document.getElementById(addon+'_'+id+'price').disabled = true;
      document.getElementById(addon+'_'+id+'desc').disabled = true;
      document.getElementById(addon+'_'+id+'id').disabled = true;
    }
  }

</script>
