    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#addon" data-toggle="tab">{!! trans('restaurant::addon.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#restaurant-addon-edit'  data-load-to='#restaurant-addon-entry' data-datatable='#restaurant-addon-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#restaurant-addon-entry' data-href='{{guard_url('restaurant/addon')}}/{{$addon->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('restaurant-addon-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('restaurant/addon/'. $addon->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="addon">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('restaurant::addon.name') !!} [{!!$addon->name!!}] </div>
                @include('restaurant::admin.addon.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>