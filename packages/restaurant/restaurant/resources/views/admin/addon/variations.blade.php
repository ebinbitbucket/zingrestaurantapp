              <div class="row">
                <div class='col-md-4 col-sm-6'>
                    {!! Form::text('av_'.$id.'[min]')
                    -> label('Minimum')
                    -> placeholder('Please enter Minimum')
                    -> required()
                    !!}
                </div>
                <div class='col-md-4 col-sm-6'>
                    {!! Form::text('av_'.$id.'[max]')
                    -> label('Maximum')
                    -> placeholder('Please enter Maximum')
                    -> required()
                    !!}
               </div>
                <div class='col-md-4 col-sm-6'>
                </br>
                  <input type="checkbox" class="form_control" name="av_{{$id}}[required]" value="Yes" > Required
                </div>
          </div> 
           @foreach($addon as $key => $variation)
              <div class="row">
                <div class='col-md-6 col-sm-6'>
                </br>
                      <input type="checkbox" id="variation_id" name="av_{{$id}}[variation_list][{{$key}}][name]" value="{!! $variation->name !!}" onclick="enableVariation(this,{{$id}},{{$key}})"> {!!$variation->name !!}
                  </div>
                   <div class='col-md-4 col-sm-6' id="price">
                      {!! Form::number('av_'.$id.'[variation_list]['.$key.'][price]')
                      ->id($id.'_'.$key.'price')
                      -> label('Price')
                      ->forceValue($variation->price)
                      ->disabled()
                      -> placeholder('Please enter Price')
                      -> required()
                      !!}
                 </div>
              </div>
               <div class='col-md-12 col-sm-6' id="price">
                    {!! Form::hidden('av_'.$id.'[variation_list]['.$key.'][description]')
                    ->id($id.'_'.$key.'desc')
                    -> label('Description')
                    ->forceValue($variation->description)
                    ->disabled()
                    -> placeholder('Please enter Description')
                    -> required()
                    !!}
               </div>
                <div class='col-md-12 col-sm-6' id="price">
                    {!! Form::hidden('av_'.$id.'[variation_list]['.$key.'][id]')
                    ->id($id.'_'.$key.'id')
                    -> label('Id')
                    ->disabled()
                    ->forceValue($variation->id)
                    -> placeholder('Please enter ID')
                    -> required()
                    !!}
               </div>
            @endforeach
<script type="text/javascript">
function enableVariation(element,addon,id){ 
    if(element.checked == true){
      document.getElementById(addon+'_'+id+'price').disabled = false;
      document.getElementById(addon+'_'+id+'desc').disabled = false;
      document.getElementById(addon+'_'+id+'id').disabled = false;
    }
    else
    {
      document.getElementById(addon+'_'+id+'price').disabled = true;
      document.getElementById(addon+'_'+id+'desc').disabled = true;
      document.getElementById(addon+'_'+id+'id').disabled = true;
    }
  }
  
</script>
                