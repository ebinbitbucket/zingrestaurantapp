<section class="eateries-list-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="eateries-letters">
                    <a href="{{url('food-more')}}">#</a>
                    @foreach (range('A', 'Z') as $char) 
                    <a href="{{url('food-'.$char)}}">{{$char}}</a>
                    @endforeach
                </div>
			</div>
		</div>
		<ul class="row companies-overview">
			@forelse($menus->chunk(2) as $menu) 
		    @foreach($menu as $data)
						<li class="col-md-6"><a href="{{!empty(@$data->restaurant->slug) ? url('restaurants/'.@$data->restaurant->slug) : '#'}}">{{$data->name}}<span class="fa fa-cutlery">{{@$data->restaurant->name}}</span><span class="fa fa-map-marker" style="padding-top:3px;">{{@$data->restaurant->address}}</span></a></li>

			@endforeach
	    	@empty
	    	@endif
		</ul>
		<ul class="pagination" role="navigation">
    	{{$menus->links()}}
    	</ul>
	</div>
</section>