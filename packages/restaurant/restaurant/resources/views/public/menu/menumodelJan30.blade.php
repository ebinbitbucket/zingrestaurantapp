    <div class="modal-header">
        <h5 class="modal-title" id="productModalTitle">{{$menu->name}}</h5>
        <p>{{$menu->description}}</p>
        <div class="dish-metas">
            <span>{{@$menu->categories->name}}</span>
            <span class="ml-15"><i class="fa fa-star" style="color: #f6af00;"></i>{{@$menu->review}}</span>
            <span class="ml-15"><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
            <span class="ml-15">$ {{$menu->price}}</span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
    </div>
    <div class="modal-body">
        <div class="menu-item-content">
            @if($menu['description'] >0 )
            <p>{{$menu->description}}</p>
            @endif
             @if(!empty($menu->offer))
                @if(empty(Session::get('search_time'))) 
                                <?php Session::put('search_time',date('Y-m-d H:i:s')); ?>
                @endif
                <?php $offer_time_flag = 0; ?>
                @if(!empty($menu->offer['schedule']))
                <?php $offer_time_flag = 0; ?>
                    @foreach($menu->offer['schedule'] as $schedule)
                        @if(date('H:i',strtotime($schedule['from']))<=date('H:i',strtotime(Session::get('search_time'))) && date('H:i',strtotime($schedule['end']))>=date('H:i',strtotime(Session::get('search_time'))))
                            <?php $offer_time_flag = 1;
                                break; ?>
                        @endif
                    @endforeach
                @endif
                @if($offer_time_flag==1)
                    <div class="menu-offer">
                        <h4 style="padding-bottom:10px;"><i>{{$menu->offer['name']}} Price- $ {{$menu->offer['price']}}</i></h4>
                        <p style="padding-bottom:10px;">Regular Price- $ {{$menu->price}}</p>
                        @if(!empty($menu->offer['schedule']))
                            @foreach($menu->offer['schedule'] as $schedule)
                              <p><b>{{$schedule['day']}} at {{date('h:i A',strtotime($schedule['from']))}} to {{date('h:i A',strtotime($schedule['end']))}}</b></p>
                            @endforeach
                        @endif
                    </div>
                 @endif
            @endif

             @if(!empty($menu->image))
             <div class="menu-image" style="background-image: url('{{url($menu->defaultImage('image','master'))}}')"></div>
            @else
                
               @if(!empty(@$menu->master))
               <?php $master_image = Master::getMasterImage($menu->name); ?>
                <img src="{{url($menu->master->defaultImage('image','master'))}}" class="img-fluid w-100" alt="">
                    @else
                    <img src="{{theme_asset('img/menu/default_logo.jpg')}}" class="img-fluid" alt="">
                    @endif
            @endif
            <p class="mt-10" style="color:black;"><i>*The image is either uploaded by the eatery/restaurant or a generic one to give an idea of what it may look like.</i></p>
            
           

            <div class="accordion" id="menuOptions" style="padding-bottom:10px;">
                 <div class="alert required-menu-alert" role="alert" style="display: none;border: none;background-color: none;" id="main_error"><i class="fa fa-exclamation-circle"></i><span id="msg"></span></div>
                @if(!empty($menu->menu_variations))
                    <div class="card">
                        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#menu_variation" aria-expanded="false" aria-controls="menu_variation">
                            <h4>Variation</h4>
                            <p id="variation_value">{{@$menu->menu_variations[0]['name']}}</p>
                        </div>
                         
                            <div id="menu_variation" class="collapse" aria-labelledby="headingOne" data-parent="#menuOptions">
                                <div class="card-body">
                                    @foreach($menu->menu_variations as $key => $menu_variation)
                                        <div class="option-item">
                                            <input type="radio" id="{{$menu_variation['name']}}" name="price" value="{{$menu_variation['name']}},{{$menu_variation['price']}}" {{$key == 0 ? 'checked' : ''}}>
                                            <label for="{{$menu_variation['name']}}">
                                                <h5>{{$menu_variation['name']}} <span>${{number_format($menu_variation['price'],2)}}</span></h5>
                                                <p>{{$menu_variation['description']}}</p>
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                    </div>
                @endif

                @if(count($menu_addons)>0)
                @foreach($menu_addons as $menus) 
                    <div class="card">  
                             <div class="required-menu-alert" role="alert" style="display: none;" id="sub_error_{{$menus->addon_id}}"><i class="fa fa-exclamation-circle"></i><span id="msg_{{$menus->addon_id}}"></span></div>
                             <div id="addon_men_minimum_error_{{$menus->addon_id}}" class="required-menu-alert" style="display: none;"><i class="fa fa-exclamation-circle"></i> Please select atleast {{$menus->min}} addon(s)</div>
                             <div id="addon_men_maximum_error_{{$menus->addon_id}}" class="required-menu-alert" style="display: none;"><i class="fa fa-exclamation-circle"></i> Maximum {{$menus->max}} addons allowed.</div>
                        @foreach(Restaurant::getAddon($menus->addon_id) as $addon) 
                        <div class="card-header @if($menus->required == 'No') optional @else required @endif" id="headingTwo" data-toggle="collapse" data-target="#menu_addon_{{$menus->addon_id}}" aria-expanded="false" aria-controls="menu_addon">
                            <h4>{{$addon}} <span>
                                @if($menus->required == 'No')
                                        (Optional)
                                            @else
                                        (Required)
                                        @endif
                                        <!-- <b>Minimum:{{$menus->min}} Maximum:{{$menus->max}}</b> -->
                                    </span></h4>
                                    <p id="addon_value_{{$menus->addon_id}}"></p>
                        </div>
                        @endforeach

                        <div id="menu_addon_{{$menus->addon_id}}" class="collapse" aria-labelledby="headingTwo" data-parent="#menuOptions">
                            <div class="card-body">
                                @foreach(json_decode($menus->variation_list) as $variation) 
                                <div class="option-item">
                                    <input type="checkbox" id="{{$variation->name}}" name="variations[{{$menus->addon_id}}][{{$variation->id}}]" value="{{$variation->name}},{{$variation->price}}" data-id="{{$menus->addon_id}}" data-min="{{$menus->min}}" data-max="{{$menus->max}}">
                                    <label for="{{$variation->name}}">
                                        <h5>{{$variation->name}} <span>{{($variation->price > 0) ? '$'.$variation->price : ''}}</span></h5>
                                        <p>{{$variation->description}}</p>
                                    </label>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>                                
                @endforeach
                @endif
</div>
<input type="hidden" name="menu_id" value="{{$menu->id}}">
@if($menu->restaurant->flag_special_instr=='Yes')
            <h4>Preferences</h4>
            <div class="form-group">
                <label for="instructions">Special instructions <span>List any special requests</span></label>
                <textarea class="form-control" placeholder="Add note (extra sauce,extra spicy etc.)" rows="4" name="special_instr"></textarea>
            </div>
            <!--<div class="form-group">-->
            <!--                        <label for="soldout">If sold out</span></label>-->
            <!--                        <select name="soldout" id="soldout" class="form-control">-->
            <!--                            <option value="substitute">Go with merchant recommendation</option>-->
            <!--                            <option value="refund">Refund this item</option>-->
            <!--                            <option value="contact">Contact me</option>-->
            <!--                            <option value="cancel">Cancel the entire order</option>-->
            <!--                        </select>-->
            <!--                    </div>-->
                                @endif
            
        </div>
         @if($menu->stock_status != 1 || $menu->categories->stock_status!=1)
            <p style="color:red;"><b>The Eatery has made this dish temporarily unavailable</b></p>
        @endif

    </div>
    <div class="modal-footer">
         @if($menu->restaurant->published!='No Sale')
        <div class="input-counter">
            <span class="minus-btn" data-quantity="minus" data-field="quantity"></span>
            <input type="number" value="1" min="1" max="999" name="quantity" id="qty">
            <span class="plus-btn" data-quantity="plus" data-field="quantity"></span>
        </div> 
        @endif

        @if($menu->restaurant->published == 'Published')
        @if($menu->stock_status == 1 && $menu->categories->stock_status==1)
             @if(!empty($menu->timing)) 
                @foreach($menu->timing as $key => $time) 
                @if(empty(Session::get('search_time'))) 
                    <?php Session::put('search_time',date('Y-m-d H:i:s')); ?>
                @endif
                 @if(strtolower(substr(date('l',strtotime(Session::get('search_time'))),0,3)) == $key)
                    @if(!empty($time['start']) && !empty($time['end']))
                        @if(date('H:i',strtotime($time['start']))<=date('H:i',strtotime(Session::get('search_time'))) && date('H:i',strtotime($time['end']))>=date('H:i',strtotime(Session::get('search_time'))))
                            <button type="button" class="btn btn-theme btn_add_cart" onclick="checkcount()">Add to Cart <span></span></button>
                        @else
                        <button type="button" disabled="true"  class="btn btn-theme btn_add_cart">Add to Cart <span></span></button>
                        @endif
                    @else
                        <button type="button" class="btn btn-theme btn_add_cart" onclick="checkcount()">Add to Cart <span></span></button>
                    @endif
                @endif
                @endforeach
               @else
               <button type="button" class="btn btn-theme btn_add_cart" onclick="checkcount()">Add to Cart <span></span></button>
                @endif
        @else
        <button type="button" disabled="true"  class="btn btn-theme btn_add_cart">Add to Cart <span></span></button>
        @endif
        @else
        <button type="button" style="display: none;"  class="btn btn-theme btn_add_cart">Add to Cart <span></span></button>
        @endif
    </div>
<script>
    function checkcount(){
        var currentVal = $('#qty').val();
        var count = '<?php echo Cart::MaxOrderCount($menu->id); ?>'; 
        var max = '<?php echo $menu->max_order_count; ?>'; 
        if(max!='' && max < (parseInt(count)+parseInt(currentVal))){
            alert(" Today's Maxiumum order quantity of this menu item reached");
             event.preventDefault();
        }
        $(".cart-toggle-btn .cart_count_header").addClass("pulse");
    }

    setTimeout(function() {
        $(".cart-toggle-btn .cart_count_header").removeClass("pulse");
    }, 800);
    
    // $(document).ready(function(){
    //     var radio = document.getElementsByName('price'); 
    //     $('#'+radio[0].id).attr('checked',true);
    // });
    $('[data-quantity="plus"]').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal)) { 
            var count = '<?php echo Cart::MaxOrderCount($menu->id); ?>'; 
            var max = '<?php echo $menu->max_order_count; ?>'; 
            if(max!='' && max < (parseInt(count)+parseInt(currentVal)+1)){
                alert(" Today's Maxiumum order quantity of this menu item reached");
            }
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            $('input[name='+fieldName+']').val(0);
        }
    });
    $('[data-quantity="minus"]').click(function(e) {
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal) && currentVal > 1) {
            var count = '<?php echo Cart::MaxOrderCount($menu->id); ?>'; 
            var max = '<?php echo $menu->max_order_count; ?>'; 

            if(max!='' && max< (parseInt(count)+parseInt(currentVal)-1)){
                alert('Maxiumum quantity for this menu item exceeded for today');
            }
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {

            $('input[name='+fieldName+']').val(1);
        }
    });

    $(document).ready(function() {
        $("#menuOptions .card-body").on('click', function(e){
            e.stopPropagation();
        });
        $('input[name="price"]').bind('change', function() {
        var alsoInterested = '';
            $('input[name="price"]').each(function(index, value) {
                 if (this.checked) {
                     alsoInterested += ($(this).attr('id'));
                 }
             });

             $('#variation_value').html(alsoInterested);
        });

        $('input[name^="variations"]').bind('change', function() {
             var alsoInterested_1 = '';
            var name = "variations["+$(this).attr('data-id')+"]";
             $('input[name^="'+name+'"]').each(function(index, value) {           
                 if (this.checked) {
                    if(alsoInterested_1!=''){
                        alsoInterested_1 += ', ';
                    }
                     alsoInterested_1 += ($(this).attr('id') );
                    $(this).closest('.card').removeClass('required-item');
                    $(".required-menu-alert").hide();
                 }
         });
            $('input[name^="'+name+'"]').on('change', function () {
                if ($('input[name^="'+name+'"]:checked').length == 0) {
                    $('#addon_men_error_'+$(this).attr('data-id')).show();
                } else {
                    $('#addon_men_error_'+$(this).attr('data-id')).hide();
                }

                if($('input[name^="'+name+'"]:checked').size() > $(this).data('max')){
                    $('#addon_men_maximum_error_'+$(this).attr('data-id')).show();
                } else if ($('input[name^="'+name+'"]:checked').size() < $(this).data('min')) {
                    $('#addon_men_minimum_error_'+$(this).attr('data-id')).show();
                }
                else{
                    $('#addon_men_minimum_error_'+$(this).attr('data-id')).hide();
                    $('#addon_men_maximum_error_'+$(this).attr('data-id')).hide();
                }
            });
                $('#addon_value_'+$(this).attr('data-id')).html(alsoInterested_1);
        });
        
        
        // $("input[name='variation']").change(function(){
        //     var favorite = [];
        //     $.each($("input[name='variation']:checked"), function(){
        //         favorite.push($(this).val());
        //     });
        //     $("#variation_value").append(favorite.join(", "));
        // });
    })

</script>