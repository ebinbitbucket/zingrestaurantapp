<div class="row no-gutters">
    <div class="col-md-5">
        <div class="product-gallery">
            <div class="gallery-with-thumbs" id="newImages">
                <div class="product-image-container">
                    <div class="product-full-image main-slider swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($menu->getImages('image','lg') as $key=> $image)
                            <figure class="swiper-slide">
                                <img src="{!!url($menu->defaultImage('image', 'sm' ,$key))!!}" alt="Product Image">
                            </figure>
                            @endforeach
                        </div>
                        <div class="swiper-arrow next"><i class="ft-chevron-right"></i></div>
                        <div class="swiper-arrow prev"><i class="ft-chevron-left"></i></div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
    </div>
<div class="col-md-7">
        <div class="popup-content">
            {!!Form::vertical_open()
            ->class('form-vertical')
            ->action('/carts/save/'.$menu['id'])
            ->method('GET')!!}
            <div class="product-info-wrapper product-details">
                <div class="row">
                    <div class="col-4">
                        <div class="product-price">
                            <p class="d-flex align-items-center m-0">
                                <span class="price-new">£ {{$menu->price}}</span>
                            </p>
                        </div>
                    </div>
                    
                    <div class="col-8">
                        
                        <div class="d-flex">
                            <div class="product-quantity mt-0">
                                <h4 class="d-inline-block">Qty : </h4>
                                <div class="enumerator">
                                    <span class="enumerator_btn js-minus_btn">-</span>
                                    <input type="number" name="quantity" value="1" class="enumerator_input">
                                    <span class="enumerator_btn js-plus_btn">+</span>
                                </div>
                            </div>
                            <div class="product-actions ml-20 mt-0">
                                <button class="btn btn-dark" type="submit" title="Choose one size"><i class="ft-shopping-cart"></i>Add to Bag</button>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="product-description">
                   {!!str_limit($menu->description, 200)!!}
                </div>
                
               
            </div>
            {!!Form::close()!!}
        </div>
    </div>