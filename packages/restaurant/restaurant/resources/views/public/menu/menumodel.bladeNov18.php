    <div class="modal-header">
        <h5 class="modal-title" id="productModalTitle">{{$menu->name}}</h5>
        <p>{{$menu->description}}</p>
        <div class="dish-metas">
            <span><i class="fa fa-star" style="color: #f6af00;"></i>{{@$menu->review}}</span>
            <span class="ml-15"><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
    </div>
    <div class="modal-body">
        <div class="menu-item-content">
            @if($menu['description'] >0 )
            <p>{{$menu->description}}</p>
            @endif
             @if(!empty($menu->offer))
                @if(empty(Session::get('search_time'))) 
                                <?php Session::put('search_time',date('Y-m-d H:i:s')); ?>
                @endif
                @if(!empty($menu->offer['schedule']))
                <?php $offer_time_flag = 0; ?>
                    @foreach($menu->offer['schedule'] as $schedule)
                        @if(date('H:i',strtotime($schedule['from']))<=date('H:i',strtotime(Session::get('search_time'))) && date('H:i',strtotime($schedule['end']))>=date('H:i',strtotime(Session::get('search_time'))))
                            <?php $offer_time_flag = 1;
                                break; ?>
                        @endif
                    @endforeach
                @endif
                @if($offer_time_flag==1)
                    <div class="menu-offer">
                        <h4 style="padding-bottom:10px;"><i>{{$menu->offer['name']}} Price- $ {{$menu->offer['price']}}</i></h4>
                        <p style="padding-bottom:10px;">Regular Price- $ {{$menu->price}}</p>
                        @if(!empty($menu->offer['schedule']))
                            @foreach($menu->offer['schedule'] as $schedule)
                              <p><b>{{$schedule['day']}} at {{date('h:i A',strtotime($schedule['from']))}} to {{date('h:i A',strtotime($schedule['end']))}}</b></p>
                            @endforeach
                        @endif
                    </div>
                 @endif
            @endif

             @if(!empty($menu->image))
             <div class="menu-image" style="background-image: url('{{url($menu->defaultImage('image','original'))}}')"></div>
                                                            @else
                                                                <?php $master_image = Master::getMasterImage($menu->name); ?>
                                                                @if(!empty($master_image))
                                                                <img src="{{url($master_image->defaultImage('image','original'))}}" class="img-fluid" alt="">
                                                                    @else
                                                                    <img src="{{theme_asset('img/menu/default_logo.jpg')}}" class="img-fluid" alt="">
                                                                    @endif
                                                            @endif
            <p class="mt-10" style="color:black;"><i>*Actual dish may look different than the above image.</i></p>
                <div class="item-option-wrap mb-20"> 

                    @if(!empty($menu->menu_variations))
                @foreach($menu->menu_variations as $key => $menu_variation) 
                 <div class="option-item">
                       <div class="custom-control custom-checkbox">
                            <input type="radio" class="custom-control-input" name="price" id="{{$menu_variation['name']}}" value="{{$menu_variation['name']}},{{$menu_variation['price']}}" {{$key == 0 ? 'checked' : ''}}>
                            <label class="custom-control-label" for="{{$menu_variation['name']}}">
                                <h4>{{$menu_variation['name']}} <span>${{$menu_variation['price']}}</span></h4>
                                <p>{{$menu_variation['description']}}</p>
                            </label>
                        </div>
                    </div>
                @endforeach
                @endif
                    
            </div>
            @foreach($menu_addons as $menus) 
            @foreach(Restaurant::getAddon($menus->addon_id) as $addon) 
                <h4>{{$addon}}
                    <span>@if($menus->required == 'No')
                            (Optional)
                          @else
                            (Required)
                           @endif
                </span></h4>
            @endforeach
                <div class="item-option-wrap mb-20"> 
                @foreach(json_decode($menus->variation_list) as $variation) 
                    <div class="option-item">
                       <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="variations[{{$menus->addon_id}}][{{$variation->id}}]"  id="{{$variation->name}}" value="{{$variation->name}},{{$variation->price}}">
                            <label class="custom-control-label" for="{{$variation->name}}">
                                <h4>{{$variation->name}} <span>{{($variation->price > 0) ? '$'.$variation->price : ''}}</span></h4>
                                <p>{{$variation->description}} </p>
                            </label>
                        </div>
                    </div>
                @endforeach
                
            </div>
           @endforeach
<input type="hidden" name="menu_id" value="{{$menu->id}}">
@if($menu->restaurant->flag_special_instr=='Yes')
            <h4>Preferences</h4>
            <div class="form-group">
                <label for="instructions">Special instructions <span>List any special requests</span></label>
                <textarea class="form-control" placeholder="Add note (extra sauce,extra spicy etc.)" rows="4" name="special_instr"></textarea>
            </div>
            <!--<div class="form-group">-->
            <!--                        <label for="soldout">If sold out</span></label>-->
            <!--                        <select name="soldout" id="soldout" class="form-control">-->
            <!--                            <option value="substitute">Go with merchant recommendation</option>-->
            <!--                            <option value="refund">Refund this item</option>-->
            <!--                            <option value="contact">Contact me</option>-->
            <!--                            <option value="cancel">Cancel the entire order</option>-->
            <!--                        </select>-->
            <!--                    </div>-->
                                @endif
            
        </div>
         @if($menu->stock_status != 1 || $menu->categories->stock_status!=1)
            <p style="color:red;"><b>The Eatery has made this dish temporarily unavailable</b></p>
        @endif
    </div>
    <div class="modal-footer">
         @if($menu->restaurant->published!='No Sale')
        <div class="input-counter">
            <span class="minus-btn" data-quantity="minus" data-field="quantity"></span>
            <input type="number" value="1" min="1" max="999" name="quantity" id="qty">
            <span class="plus-btn" data-quantity="plus" data-field="quantity"></span>
        </div> 
        @endif
        @if($menu->restaurant->published == 'Published')
        @if($menu->stock_status == 1 && $menu->categories->stock_status==1)
             @if(!empty($menu->timing)) 
                @foreach($menu->timing as $key => $time) 
                @if(empty(Session::get('search_time'))) 
                    <?php Session::put('search_time',date('Y-m-d H:i:s')); ?>
                @endif
                 @if(strtolower(substr(date('l',strtotime(Session::get('search_time'))),0,3)) == $key)
                    @if(!empty($time['start']) && !empty($time['end']))
                        @if(date('H:i',strtotime($time['start']))<=date('H:i',strtotime(Session::get('search_time'))) && date('H:i',strtotime($time['end']))>=date('H:i',strtotime(Session::get('search_time'))))
                            <button type="button" class="btn btn-theme btn_add_cart" onclick="checkcount()">Add to Cart <span></span></button>
                        @else
                        <button type="button" disabled="true"  class="btn btn-theme btn_add_cart">Add to Cart <span></span></button>
                        @endif
                    @else
                        <button type="button" class="btn btn-theme btn_add_cart" onclick="checkcount()">Add to Cart <span></span></button>
                    @endif
                @endif
                @endforeach
               @else
               <button type="button" class="btn btn-theme btn_add_cart" onclick="checkcount()">Add to Cart <span></span></button>
                @endif
        @else
        <button type="button" disabled="true"  class="btn btn-theme btn_add_cart">Add to Cart <span></span></button>
        @endif
        @else
        <button type="button" style="display: none;"  class="btn btn-theme btn_add_cart">Add to Cart <span></span></button>
        @endif
    </div>
<script>
    function checkcount(){
        var currentVal = $('#qty').val();
        var count = '<?php echo Cart::MaxOrderCount($menu->id); ?>'; 
            var max = '<?php echo $menu->max_order_count; ?>'; 
            if(max!='' && max < (parseInt(count)+parseInt(currentVal))){
                alert(" Today's Maxiumum order quantity of this menu item reached");
                 event.preventDefault();
            }
            
    }
    // $(document).ready(function(){
    //     var radio = document.getElementsByName('price'); 
    //     $('#'+radio[0].id).attr('checked',true);
    // });
    $('[data-quantity="plus"]').click(function(e){
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal)) { 
            var count = '<?php echo Cart::MaxOrderCount($menu->id); ?>'; 
            var max = '<?php echo $menu->max_order_count; ?>'; 
            if(max!='' && max < (parseInt(count)+parseInt(currentVal)+1)){
                alert(" Today's Maxiumum order quantity of this menu item reached");
            }
            $('input[name='+fieldName+']').val(currentVal + 1);
        } else {
            $('input[name='+fieldName+']').val(0);
        }
    });
    $('[data-quantity="minus"]').click(function(e) {
        e.preventDefault();
        fieldName = $(this).attr('data-field');
        var currentVal = parseInt($('input[name='+fieldName+']').val());
        if (!isNaN(currentVal) && currentVal > 1) {
            var count = '<?php echo Cart::MaxOrderCount($menu->id); ?>'; 
            var max = '<?php echo $menu->max_order_count; ?>'; 

            if(max!='' && max< (parseInt(count)+parseInt(currentVal)-1)){
                alert('Maxiumum quantity for this menu item exceeded for today');
            }
            $('input[name='+fieldName+']').val(currentVal - 1);
        } else {

            $('input[name='+fieldName+']').val(1);
        }
    });

</script>