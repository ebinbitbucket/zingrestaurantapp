<div class="modal-body menu-video-modal">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	<div class="embed-responsive embed-responsive-16by9">
  		<iframe class="embed-responsive-item" id="menuVideo"  src="https://www.youtube.com/embed/{{$menu->youtube_link}}?rel=0" allowfullscreen ng-show="showvideo"></iframe>
	</div>
</div>