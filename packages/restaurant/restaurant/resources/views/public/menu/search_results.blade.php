
                                                       <div class="menu-items-section content-section">

													   </div>
														<div class="card menu-cat-card">              
{{-- 

                                    <div class="card menu-cat-card" id="menu_{{$men->$category->id}}">
                                        <div class="card-header">
                                            <h4 class="mb-0 menu-link" data-target="menu_{{$men->$category->id}}">{{$men->$category->name}} <span class="item-count">
                                                @if(isset($menus))
                                                {{count($menus->where('catering', '<>', 'Yes'))}}
                                                @endif
                                                Items</span></h4>
                                        </div> --}}


                                        <div class="menu-cat-card-content" style="display: block" >
                                            <div class="card-body">
                                                @if(isset($menus))
                                                @foreach($menus as $menu)
                                                
                                                {{-- {{dd($restaurant)}} --}}
                                                @if($menu->restaurant)
                                                @if($menu->catering != 'Yes')
                                                <div class="dish-item" data-slug="{{$menu->id}}" onclick="modalopen('{{$menu->id}}')">
                                                    <div class="dish-description">
                                                        <h3 class="dish-title"><a href="#" class="someclass">{{$menu->name}}</a></h3>
                                                        
                                                        <p>{{str_limit($menu->description, 30)}}</p>
                                                        <div class="dish-metas">
                                                            <span class="mr-15">${{$menu->price}}</span>
                                                            <!--<span class="mr-15"><i class="fa fa-star" style="color: #f6af00;"></i>{{$menu->review}}</span>-->
                                                            <span><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
                                                        </div>
                                                        
                                                    </div>

                                                    <div class="dish-image">
                                                        @if(!empty($menu->image))
                                                        <img data-src="{{url($menu->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                        @else
                                                            @if(!empty(@$menu->master->image))
                                                            <img src="{{url(@$menu->master->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                             @elseif(!empty(url($menu->restaurant->mainlogo)))
                                                             <img src="{{url($menu->restaurant->mainlogo)}}" class="img-fluid" alt="">
                                                            @else
                                                            <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>


                                                @endif

                                                @endif
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>


									</div>
									

                                    
