 <div class="modal-header">
                            <h5 class="modal-title" id="productModalTitle">{{$menu->name}}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
                        </div>
                        <div class="modal-body">
                            <div class="menu-item-content">
                                <p>{{$menu->description}}</p>
                                <img src="img/menu/01.jpg" class="img-fluid mb-30" alt="">
                                <h4>People also added: <span>(Optional)</span></h4>
                                <div class="item-option-wrap mb-20">
                                    @foreach($menu_addons as $value)
                                    <div class="option-item">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">
                                                <h4>{{$value->name}} + Chips <span>+$5.49</span></h4>
                                                <p><span>Acompañamientos (Sides)</span> · Fresh avocado, red onion, chopped cilantro, red bell pepper, lime juice, salt and black pepper. Served with green plantain chips. Gluten Friendly & Vegetarian</p>
                                            </label>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                                <h4>Preferences</h4>
                                <div class="form-group">
                                    <label for="instructions">Extra instructions <span>List any special requests</span></label>
                                    <textarea class="form-control" placeholder="e.g. allergies, extra spicy, etc." rows="4"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="soldout">If sold out</span></label>
                                    <select name="soldout" id="soldout" class="form-control">
                                        <option value="substitute">Go with merchant recommendation</option>
                                        <option value="refund">Refund this item</option>
                                        <option value="contact">Contact me</option>
                                        <option value="cancel">Cancel the entire order</option>
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="input-counter">
                                <span class="minus-btn" data-quantity="minus" data-field="quantity"></span>
                                <input type="number" value="1" min="0" max="999" name="quantity">
                                <span class="plus-btn" data-quantity="plus" data-field="quantity"></span>
                            </div>
                            <button type="button" class="btn btn-theme">Add to Cart - <span>$10.99</span></button>
                        </div>
                    </div>