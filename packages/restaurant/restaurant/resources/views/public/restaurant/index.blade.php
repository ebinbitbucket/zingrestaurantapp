 @include('restaurant::public.restaurant.search')
            <section class="result-header-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p>We have found </p>
                            <h3>{{$restaurants->count()}} restaurants near you</h3>
                        </div>
                        <div class="col-md-6 text-right">
                          <a class="map-view-btn" href="{{trans_url('restaurants/mapview')}}">Map View</a>
                            <!-- <button class="map-view-btn collapsed" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"></button> -->
                        </div>
                    </div>
                </div>
            </section>

            <div class="collapse map-view-wrap" id="collapseExample">
                <div id="search-map"></div>
            </div>
      <section class="listing-wrap" style="min-height: 397px;"> 
                <div class="container">
                    <div class="listing-wrap-inner">
                        <div class="row">
                            @foreach($restaurants as $restaurant)
                            <div class="col-md-6">
                                <div class="listing-item">
                                    <div class="img-holder">
                                        <figure>
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">
                                                <img src="{{url($restaurant->defaultImage('logo'))}}" alt="" style='height: 100%; width: 100%; object-fit: contain'>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="text-holder">
                                        
                                        <!-- <div class="status closed">Preorder</div> -->
                                        <h3 class="title"><a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">{{$restaurant->name}}</a></h3>
                                        <span class="raty" data-score="{{$restaurant->rating}}"></span>

                                        @if(!empty($restaurant->type))
                                        <p class="type">{{$restaurant->type}}</p>
                                        @endif
                                        <div class="location"><i class="ion ion-android-pin"></i>{{$restaurant->address}}</div>
                                        <div class="meta-infos">
                                          @if($restaurant->delivery == 'Yes')
                                        @if($restaurant->delivery_charge == 0)
                                             <div class="info">    <i class="flaticon-fast-delivery"></i>Free Delivery</div>
                                        @elseif($restaurant->delivery_charge != '')
                                             <div class="info">    <i class="flaticon-fast-delivery"></i>${{number_format($restaurant->delivery_charge,2)}} / Delivery</div>
                                        @endif
                                @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                         @endforeach 
                        </div>
                    </div>
                </div>
            </section>
            <div class="modal fade login-modal location-modal" id="locationModel" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">

                    <div class="modal-content"> 
                      {!!Form::vertical_open()
                      ->method('GET')
                      ->action('restaurants/')
                      ->addclass('search-form-compact')
                      !!}  
                        <div class="modal-body">
                            <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a>
                            <div class="login-wrap">
                                <div class="sign-in-wrap">
                                    <div class="wrap-inner">
                                            <div class="login-header text-center">
                                              <h2>Please select location</h2>
                                            </div>
                                            <div class="form-group">
                                              <i class="flaticon-pin"></i>
                                              <input type="text" name="search[address]" id="googlelocation" class="form-control" placeholder="Enter delivery address">
                                              <input type="hidden" name="search[latitude]" id="lat">
                                              <input type="hidden" name="search[longitude]" id="long">
                                            </div>
                                            <div class="text-center login-footer">
                                               <button type="submit" class="btn btn-theme search-btn">Search</button>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>
                    
                </div>
            </div>
<style>
    .pac-container {
        z-index: 10000 !important;
    }
</style>
<script type="text/javascript">
       $( document ).ready(function() { 
      var someSessionVariable = '<?php echo Session::get('location'); ?>';

        if(someSessionVariable == '')
         {
           $('#locationModel').modal({show:true, backdrop: 'static',
            keyboard: false}); 

         }
         $("#del_type_schedule").on('click', function (e) {
            $("#schedule_time").show();
        });
        $("#del_type_asap").on('click', function (e) {
            $("#schedule_time").hide();
        });
    })

     var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('googlelocation'));
        google.maps.event.addListener(places, 'place_changed', function () {        

             geocodeAddress(geocoder);
        });
    });
    function geocodeAddress(geocoder) {
        var address = document.getElementById('googlelocation').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('lat').value=results[0].geometry.location.lat();
            document.getElementById('long').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
</script>

<script type="text/javascript">
       $(function(){
     var map,myLatlng;   
      @if(!empty($restaurant->latitude) && !empty($restaurant->longitude))
         myLatlng = new google.maps.LatLng({!! @$restaurant->latitude !!},{!! @$restaurant->longitude !!});
      @else
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
      @endif      
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP
         }
      map = new google.maps.Map(document.getElementById("search-map"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
    });
       })
</script>
