
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body> 

   
      
   
   <div bgcolor="#EFF2F7" style="background-color:#eff2f7;padding:30px 0;margin:0;width:100%">
      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;min-width:600px;border-radius:6px 6px 6px 6px;margin:0 auto;margin-top:30px" width="600">
         <tbody>
            <tr>
               <td bgcolor="#40b659" height="40" style="border-radius:6px 6px 0px 0px"> </td>
            </tr>
            <tr>
               <td align="center" bgcolor="#40b659" style="padding:0px 0px">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                     <tbody>
                        <tr>
                           <td style="text-align:center"><img src="https://zingmyorder.com/img/logo-round-white.png" style="display:inline-block;width:80px"></td>
                        </tr>
                        <tr>
                           <td height="10" style="line-height:10px;height:10px"> </td>
                        </tr>
                        <tr>
                           <td align="center" style="font-size:28px;line-height:28px;text-align:center;font-weight:600;text-decoration:none"> <span style="color:#fff">Thank you for joining Zing!</span> </td>
                        </tr>
                        <tr>
                           <td height="40" style="line-height:40px;height:40px"> </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            
            <tr>
               <td align="center" bgcolor="#ffffff" style="padding:0px 40px">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                     <tbody>
                        <tr>
                           <td height="30" style="line-height:30px;height:30px"> </td>
                        </tr>
                        <tr>
                           <td align="center" style="font-size:20px;line-height:28px;color:#3c4858;text-align:left;font-weight:700;font-style:normal;text-decoration:none"> <span style="color:#3c4858"> Hi {{@$data['contact_name']}}, </span> </td>
                        </tr>
                        <tr>
                           <td height="10" style="line-height:10px;height:10px"> </td>
                        </tr>
                        <tr>
                           <td align="center" style="color:#3c4858;font-size:14px;line-height:24px;font-weight:300;text-align:left">
                              <div>
                                 <p style="margin-top:0px;margin-bottom:10px">We are excited to see your Restaurant <b>{{@$user->name}}</b> thrive. A Zing specialist will contact you shortly to complete your set up to get you started.</p>
                                 <h4 style="font-size:16px;line-height:28px;color:#3c4858;text-align:left;font-weight:700;font-style:normal;margin:0px;margin-bottom:10px;text-decoration:none"> <span style="color:#3c4858">Account Info</span></h4>

                                 <div style="display:block;border-radius:4px;padding:10px 5px;background-color:#f4f5fa;margin-bottom:15px">
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                       <tbody>
                                          <tr>
                                             <td style="color:#000;padding:5px 0px;padding-left:10px">Username</td>
                                             <td style="color:#000;padding:5px 0px">:</td>
                                             <td style="color:#000;padding:5px 0px;padding-left:10px"><a href="mailto:{{@$user->email}}" target="_blank">{{@$user->email}}</a></td>
                                          </tr>
                                          <tr>
                                             <td style="color:#000;padding:5px 0px;padding-left:10px">Restaurant Name</td>
                                             <td style="color:#000;padding:5px 0px">:</td>
                                             <td style="color:#000;padding:5px 0px;padding-left:10px">{{@$user->name}}</td>
                                          </tr>
                                          <tr>
                                             <td style="color:#000;padding:5px 0px;padding-left:10px">Phone Number</td>
                                             <td style="color:#000;padding:5px 0px">:</td>
                                             <td style="color:#000;padding:5px 0px;padding-left:10px">{{@$user->phone}}</td>
                                          </tr>
                                          
                                          <tr>
                                             <td style="color:#000;padding:5px 0px;padding-left:10px">Restaurant Address</td>
                                             <td style="color:#000;padding:5px 0px">:</td>
                                             <td style="color:#000;padding:5px 0px;padding-left:10px">{{@$user->address}}</td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>

                                 

                                 <h5 style="margin-top:0px;margin-bottom:10px;font-size:20px;line-height:28px;color:#3c4858">Here’s what&#39;s next:</h5>
                                 
                                 <ul style="list-style:circle;margin-left:0px;padding-left:20px;margin-bottom:20px">
                                    <li>
                                       <p style="margin-top:0px;margin-bottom:10px">Our friendly Zing Agent will confirm how you would like to set up the following services provided to you at no additional charge:</p>
                                       <ol style="font-style:italic;line-height:34px;margin-bottom:10px">
                                          <li><b>Personalized website</b> powered by Zing*</li>
                                          <li><b>Automated Phone System</b></li>
                                          <li><b>Zing SEO Services**</b></li>
                                          <li><b>In-House Delivery option</b></li>
                                       </ol>
                                    </li>
                                    <li>
                                       <p style="margin-top:0px;margin-bottom:10px">Zing team will help set up your initial menu and the selected service</p>
                                    </li>
                                    <li>
                                       <p style="margin-top:0px;margin-bottom:10px">Set Up a time for an agent to come on site (only in Dallas Fort Worth Area) or schedule a Zoom Meeting </p>
                                    </li>
                                 </ul>
                                 <p style="margin-top:0px;margin-bottom:10px">Zing Specialist will assist you in the easy on-boarding process to get you going LIVE. We can have you start taking orders as early as 24 hours of sign up.</p>
                                 <p style="margin-top:0px;margin-bottom:10px">We are excited to see your site up and running in Zing soon.</p>
                              </div>
                           </td>
                        </tr>
                        <tr>
                           <td height="10" style="line-height:10px;height:10px"> </td>
                        </tr>
                        <tr>
                           <td align="center" style="font-size:16px;line-height:28px;color:#3c4858;text-align:left;font-weight:normal;font-style:normal;text-decoration:none"> <span style="color:#3c4858">Cheers,<br>The Zingmyorder Team</span> </td>
                        </tr>
                     </tbody>
                  </table>
               </td>
            </tr>
            <tr>
               <td bgcolor="#ffffff" height="40" style="border-radius:0px 0px 6px 6px"> </td>
            </tr>
            
         </tr></tbody>
      </table>
      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;min-width:600px;margin-top:30px" width="600">
         <tbody>
            <tr>
               <td align="left" style="line-height:24px;font-size:12px;color:#8492a6">
                  <p style="margin-top:0px;margin-bottom:10px">* Personalized website is set up initially by the zing team. The restaurant can make changes on the fly front of the dashboard. This is not a custom web development service.</p>
                  <p style="margin-top:0px;margin-bottom:10px">**Zing SEO services - This is dependent on using websites powered by Zing. Non Zing powered website may have limitations to the SEO services we offer.</p>
               </td>
            </tr>
         </tbody>
      </table>
      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:600px;min-width:600px;margin-top:30px" width="600">
         <tbody>
            <tr>
               <td align="center" style="line-height:24px;font-size:14px;color:#8492a6">
                  <p style="margin-top:0px;margin-bottom:10px"> Copyrignts © 2020 Zing My Order. All Rights Reserved </p>
               </td>
            </tr>
         </tbody>
      </table>
   </div>
</body></html>