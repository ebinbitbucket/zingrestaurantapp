<section class="cusine-slider-wrap map-cusine-slider-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cusine-slider-wrap-inner">
                    <div class="cusine-slider swiper-container">
                        <div class="swiper-wrapper">
                            @foreach(Master::getDetailCuisines() as $cuisine)
                            <div class="swiper-slide cuisine-item">
                                <input type="checkbox" name="cuisine_types[]" id="{{$cuisine->id}}" class="search_cuisine" value="{{$cuisine->id}}" data-name="{{$cuisine->name}}">
                                <label for="{{$cuisine->id}}">
                                    <span class="cuisene-img" style="background-image: url({{url($cuisine->defaultImage('image','sm'))}})"></span>
                                    <h4>{{$cuisine->name}}</h4>
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="cusine-slider-prev ion-android-arrow-back"></div>
                    <div class="cusine-slider-next ion-android-arrow-forward"></div>
                </div>
            </div>
        </div>
        
    </div>
</section>
<div id="search_bar">
<form class="restaurant_filter">
    <div class="map-header-filter-wrap">
        <div class="map-filter-wrap-inner">
            <div class="filter-item filter-item-sche">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-clock"></i> <span><span id="del_date_v">All Time</span> 
<span id="del_time_v"></span></span></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="del-time-wrap">
                            <div class="time-item">
                                <input type="radio" name="del_time_type" id="all_time" value="all" checked="checked">
                                <label for="all_time"><i class="ik ik-clock"></i>All</label>
                            </div>
                            <div class="time-item">
                                <input type="radio" name="del_time_type" id="asap_time" value="asap">
                                <label for="asap_time"><i class="ik ik-bell"></i>Open Now</label>
                            </div>
                            <div class="time-item">
                                <input type="radio" id="schedule_time" name="del_time_type" value="scheduled">
                                <label for="schedule_time"><i class="ik ik-calendar"></i>Schedule</label>
                            </div>
                        </div>
                        <div class="schedule-inner-block" id="scedule-block" style="display: none;">
                            <div class="form-group">
                                <p>Select a Delivery Date</p>
                                <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                    @for($i=0;$i<5;$i++)
                                        <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('d M', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                            <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                            <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <p>Desired Delivery Time</p>
                                <select class="form-control" id="sch_time">
                                    <option value="">Select a Time</option>
                                    <option>12:00 AM</option>
                                    <option>12:15 AM</option>
                                    <option>12:30 AM</option>
                                    <option>12:45 AM</option>
                                    <option>1:00 AM</option>
                                    <option>1:15 AM</option>
                                    <option>1:30 AM</option>
                                    <option>1:45 AM</option>
                                    <option>2:00 AM</option>
                                    <option>2:15 AM</option>
                                    <option>2:30 AM</option>
                                    <option>2:45 AM</option>
                                    <option>3:00 AM</option>
                                    <option>3:15 AM</option>
                                    <option>3:30 AM</option>
                                    <option>3:45 AM</option>
                                    <option>4:00 AM</option>
                                    <option>4:15 AM</option>
                                    <option>4:30 AM</option>
                                    <option>4:45 AM</option>
                                    <option>5:00 AM</option>
                                    <option>5:15 AM</option>
                                    <option>5:30 AM</option>
                                    <option>5:45 AM</option>
                                    <option>6:00 AM</option>
                                    <option>6:15 AM</option>
                                    <option>6:30 AM</option>
                                    <option>6:45 AM</option>
                                    <option>7:00 AM</option>
                                    <option>7:15 AM</option>
                                    <option>7:30 AM</option>
                                    <option>7:45 AM</option>
                                    <option>8:00 AM</option>
                                    <option>8:15 AM</option>
                                    <option>8:30 AM</option>
                                    <option>8:45 AM</option>
                                    <option>9:00 AM</option>
                                    <option>9:15 AM</option>
                                    <option>9:30 AM</option>
                                    <option>9:45 AM</option>
                                    <option>10:00 AM</option>
                                    <option>10:15 AM</option>
                                    <option>10:30 AM</option>
                                    <option>10:45 AM</option>
                                    <option>11:00 AM</option>
                                    <option>11:15 AM</option>
                                    <option>11:30 AM</option>
                                    <option>11:45 AM</option>
                                    <option>12:00 PM</option>
                                    <option>12:15 PM</option>
                                    <option>12:30 PM</option>
                                    <option>12:45 PM</option>
                                    <option>1:00 PM</option>
                                    <option>1:15 PM</option>
                                    <option>1:30 PM</option>
                                    <option>1:45 PM</option>
                                    <option>2:00 PM</option>
                                    <option>2:15 PM</option>
                                    <option>2:30 PM</option>
                                    <option>2:45 PM</option>
                                    <option>3:00 PM</option>
                                    <option>3:15 PM</option>
                                    <option>3:30 PM</option>
                                    <option>3:45 PM</option>
                                    <option>4:00 PM</option>
                                    <option>4:15 PM</option>
                                    <option>4:30 PM</option>
                                    <option>4:45 PM</option>
                                    <option>5:00 PM</option>
                                    <option>5:15 PM</option>
                                    <option>5:30 PM</option>
                                    <option>5:45 PM</option>
                                    <option>6:00 PM</option>
                                    <option>6:15 PM</option>
                                    <option>6:30 PM</option>
                                    <option>6:45 PM</option>
                                    <option>7:00 PM</option>
                                    <option>7:15 PM</option>
                                    <option>7:30 PM</option>
                                    <option>7:45 PM</option>
                                    <option>8:00 PM</option>
                                    <option>8:15 PM</option>
                                    <option>8:30 PM</option>
                                    <option>8:45 PM</option>
                                    <option>9:00 PM</option>
                                    <option>9:15 PM</option>
                                    <option>9:30 PM</option>
                                    <option>9:45 PM</option>
                                    <option>10:00 PM</option>
                                    <option>10:15 PM</option>
                                    <option>10:30 PM</option>
                                    <option>10:45 PM</option>
                                    <option>11:00 PM</option>
                                    <option>11:15 PM</option>
                                    <option>11:30 PM</option>
                                    <option>11:45 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="working_date" id="datecheck" value="{{date('d M', strtotime(date('d-M-Y')))}}">
                <input type="hidden" name="working_hours" id="timecheck" value="{{date('g:i A')}}">
                <input type="hidden" name="time_type" id="time_type">
                <input type="hidden" name="sort_filter" id="sort_filter1">
            </div>
            <div class="filter-item filter-item-type">
                <select name="delivery" id="order_type" class="selectize type-select">
                       <option value="pickup" >Pickup</option>
                <option value="orderpickup" >Order Pick Up</option>
                <option value="orderdelivery" >Order Delivery</option>
                </select>
            </div>
            <div class="filter-item filter-item-rating">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-star"></i> <input type="text" id="mapcurRating"  value="2"  readonly></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                       <p>Select Rating</p> 
                       
                         <select id="rating_pill_Header" name="rating" autocomplete="off">
                            <option value="1">1</option>
                            <option value="2" selected>2</option>
                            <option value="3">3</option>
                            <option value="4" >4</option>
                            <option value="5">5</option>
                        </select>
                       
                    </div>
                </div>
            </div>
            <div class="filter-item filter-item-price">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="price_change">$</span></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <p>Select Price</p> 
                        <div class="pricing-block">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="1" name="price" id="inlineRadio1" checked data-value="$">
                                <label class="form-check-label" for="inlineRadio1">$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="5" name="price" id="inlineRadio2" data-value="$$">
                                <label class="form-check-label" for="inlineRadio2">$$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="12" name="price" id="inlineRadio3" data-value="$$$">
                                <label class="form-check-label" for="inlineRadio3">$$$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="20" name="price" id="inlineRadio4" data-value="$$$$">
                                <label class="form-check-label" for="inlineRadio4">$$$$</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-item filter-item-category">
                <select name="main_category" id="main_category" class="selectize">
                    <option value="all" selected>All Eateries</option>
                     @forelse(Master::getMasterAllCategories() as $key => $categories)
                        <option value="{{$categories->id}}" >{{ucfirst($categories->name)}}</option>
                    @empty
                    @endif
                </select>
            </div>
            <div class="filter-item">
                 <select class="selectize" name="chain_list" id="chain_list">
                    <option value='all'>All</option>
                        <option value="No" selected="selected">Local</option>
                        <option value="Yes">Chain</option>
                </select>
            </div>
            <div class="filter-item">
                <button type="button" class="btn btn-secondary" id="reset_all">Reset</button>
            </div>
        </div>
    </div>
</form>
 <form class="restaurant_filter_Mob">
   <div class="map-header-filter-wrap-mobile">
        <div class="map-filter-wrap-inner">
            <div class="filter-item filter-item-sche">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-clock"></i> <span id="del_date_vMob">{{date('d M', strtotime(date('d-M-Y')))}}</span> 
        <span id="del_time_vMob">{{date('g:i A')}}</span></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="del-time-wrap">
                            <div class="time-item">
                                <input type="radio" name="del_time_typeMob" id="all_timeMob" value="all" checked="checked">
                                <label for="all_timeMob"><i class="ik ik-clock"></i>All</label>
                            </div>
                            <div class="time-item">
                                <input type="radio" name="del_time_typeMob" id="asap_timeMob" value="asap">
                                <label for="asap_timeMob"><i class="ik ik-bell"></i>Open Now</label>
                            </div>
                            <div class="time-item">
                                <input type="radio" id="schedule_timeMob" name="del_time_typeMob" value="scheduled">
                                <label for="schedule_timeMob"><i class="ik ik-calendar"></i>Schedule</label>
                            </div>
                        </div>
                        <div class="schedule-inner-block" id="scedule-blockMob">
                            <div class="form-group">
                                <p>Select a Delivery Date</p>
                                <div class="sch-date-wrap" id="sch_DateWrapMob" name="datesearch">
                                   @for($i=0;$i<5;$i++)
                                        <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('d M', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                            <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                            <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <p>Desired Delivery Time</p>
                                <select class="form-control" id="sch_timeMob">
                                    <option value="">Select a Time</option>
                                    <option>12:00 AM</option>
                                    <option>12:15 AM</option>
                                    <option>12:30 AM</option>
                                    <option>12:45 AM</option>
                                    <option>1:00 AM</option>
                                    <option>1:15 AM</option>
                                    <option>1:30 AM</option>
                                    <option>1:45 AM</option>
                                    <option>2:00 AM</option>
                                    <option>2:15 AM</option>
                                    <option>2:30 AM</option>
                                    <option>2:45 AM</option>
                                    <option>3:00 AM</option>
                                    <option>3:15 AM</option>
                                    <option>3:30 AM</option>
                                    <option>3:45 AM</option>
                                    <option>4:00 AM</option>
                                    <option>4:15 AM</option>
                                    <option>4:30 AM</option>
                                    <option>4:45 AM</option>
                                    <option>5:00 AM</option>
                                    <option>5:15 AM</option>
                                    <option>5:30 AM</option>
                                    <option>5:45 AM</option>
                                    <option>6:00 AM</option>
                                    <option>6:15 AM</option>
                                    <option>6:30 AM</option>
                                    <option>6:45 AM</option>
                                    <option>7:00 AM</option>
                                    <option>7:15 AM</option>
                                    <option>7:30 AM</option>
                                    <option>7:45 AM</option>
                                    <option>8:00 AM</option>
                                    <option>8:15 AM</option>
                                    <option>8:30 AM</option>
                                    <option>8:45 AM</option>
                                    <option>9:00 AM</option>
                                    <option>9:15 AM</option>
                                    <option>9:30 AM</option>
                                    <option>9:45 AM</option>
                                    <option>10:00 AM</option>
                                    <option>10:15 AM</option>
                                    <option>10:30 AM</option>
                                    <option>10:45 AM</option>
                                    <option>11:00 AM</option>
                                    <option>11:15 AM</option>
                                    <option>11:30 AM</option>
                                    <option>11:45 AM</option>
                                    <option>12:00 PM</option>
                                    <option>12:15 PM</option>
                                    <option>12:30 PM</option>
                                    <option>12:45 PM</option>
                                    <option>1:00 PM</option>
                                    <option>1:15 PM</option>
                                    <option>1:30 PM</option>
                                    <option>1:45 PM</option>
                                    <option>2:00 PM</option>
                                    <option>2:15 PM</option>
                                    <option>2:30 PM</option>
                                    <option>2:45 PM</option>
                                    <option>3:00 PM</option>
                                    <option>3:15 PM</option>
                                    <option>3:30 PM</option>
                                    <option>3:45 PM</option>
                                    <option>4:00 PM</option>
                                    <option>4:15 PM</option>
                                    <option>4:30 PM</option>
                                    <option>4:45 PM</option>
                                    <option>5:00 PM</option>
                                    <option>5:15 PM</option>
                                    <option>5:30 PM</option>
                                    <option>5:45 PM</option>
                                    <option>6:00 PM</option>
                                    <option>6:15 PM</option>
                                    <option>6:30 PM</option>
                                    <option>6:45 PM</option>
                                    <option>7:00 PM</option>
                                    <option>7:15 PM</option>
                                    <option>7:30 PM</option>
                                    <option>7:45 PM</option>
                                    <option>8:00 PM</option>
                                    <option>8:15 PM</option>
                                    <option>8:30 PM</option>
                                    <option>8:45 PM</option>
                                    <option>9:00 PM</option>
                                    <option>9:15 PM</option>
                                    <option>9:30 PM</option>
                                    <option>9:45 PM</option>
                                    <option>10:00 PM</option>
                                    <option>10:15 PM</option>
                                    <option>10:30 PM</option>
                                    <option>10:45 PM</option>
                                    <option>11:00 PM</option>
                                    <option>11:15 PM</option>
                                    <option>11:30 PM</option>
                                    <option>11:45 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="working_date" id="datecheckMob" value="{{date('d M', strtotime(date('d-M-Y')))}}">
                 <input type="hidden" name="working_hours" id="timecheckMob" value="{{date('g:i A')}}">
                <input type="hidden" name="time_type" id="time_typeMob">
                <input type="hidden" name="sort_filter" id="sort_filter2">
            </div>
            <div class="filter-item filter-item-type">
                <select name="delivery" id="order_typeMob" class="selectize type-select">
                        <option value="pickup" >Pickup</option>
                <option value="orderpickup" >Order Pick Up</option>
                <option value="orderdelivery" >Order Delivery</option>
                </select>
            </div>
            
        </div>
        <button class="adv-filter-toggle ion-android-more-vertical" type="button" data-toggle="collapse" data-target="#mobile_filter_advance" aria-expanded="false" aria-controls="mobile_filter_advance"></button>
        </div>
        <div class="collapse adv-filter-mob-wrap" id="mobile_filter_advance">
        <div class="adv-filter-mob-wrap-inner">
            <div class="filter-item filter-item-rating">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-star"></i> <input type="text" id="curRatingMob"   value="2" readonly></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                       <p>Select Rating</p> 
                       
                             <select id="rating_pill_mobile_Header" name="rating" autocomplete="off">
                                <option value="1">1</option>
                                <option value="2" selected>2</option>
                                <option value="3">3</option>
                                <option value="4" >4</option>
                                <option value="5">5</option>
                            </select>
                    </div>
                </div>
            </div>
            <div class="filter-item filter-item-price">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="priceMob_change">$</span></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <p>Select Price</p> 
                        <div class="pricing-block">
                             <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="1" name="priceMob" id="inlineRadio11" checked data-value="$">
                                <label class="form-check-label" for="inlineRadio11">$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="5" name="priceMob" id="inlineRadio21" data-value="$$">
                                <label class="form-check-label" for="inlineRadio21">$$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="12" name="priceMob" id="inlineRadio31" data-value="$$$">
                                <label class="form-check-label" for="inlineRadio31">$$$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="20" name="priceMob" id="inlineRadio41" data-value="$$$$">
                                <label class="form-check-label" for="inlineRadio41">$$$$</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-item">
                 <select name="main_category" id="main_categoryMob" class="form-control">
                    <option value="all" selected>All Eateries</option>
                     @forelse(Master::getMasterAllCategories() as $key => $categories)
                        <option value="{{$categories->id}}" >{{ucfirst($categories->name)}}</option>
                    @empty
                    @endif
                </select>
            </div>
            <div class="filter-item">
                <select class="form-control" name="chain_list" id="chain_listMob">
                    <option value='all'>All</option>
                        <option value="No" selected="selected">Local</option>
                        <option value="Yes">Chain</option>
                </select>
            </div>

        </div>
    </div>
</form>
</div>
 <section class="search-map-main-wrap" id="mapview_section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="search-map-wrapper split-map">
                            <div id="search-map"></div>
                        </div>
                        <div id="map_restauarnt" class="search-filter-result-wrapper">
                            
                            <div class="search-meta-wrapper">
                                <p class="search-number">{{$restaurants->total()}} Results found within 10 miles of the entered address.</p>
                                <select name="sort_filter" placeholder="Sort by" id="sort_filter" class="form-control">
                                    <option>Sort By</option>
                                    <option value="distance">Distance</option>
                                    <option value="pricelh">Price (Low to High)</option>
                                    <option value="pricehl">Price (High to Low)</option>
                                    <option value="rating">Rating (High to Low)</option>
                                </select>
                            </div>
                            <div class="search-meta-wrapper-mob">

                                <p class="search-number">{{$restaurants->total()}} Results found within 10 miles of the entered address.</p>
                                <select name="sort_filter" placeholder="Sort by" id="sort_filterMob" class="form-control">
                                    <option>Sort By</option>
                                    <option value="distance">Distance</option>
                                    <option value="pricelh">Price (Low to High)</option>
                                    <option value="pricehl">Price (High to Low)</option>
                                    <option value="rating">Rating (High to Low)</option>
                                </select>

                            </div>
                            <div class="search-result-wrapper restaurant-list-wraper" id="">
                                @foreach($restaurants as $key => $restaurant)
                                <?php $flag=0;
                                $flag=Restaurant::getOpenStatus($restaurant->id); ?> 

                                <div class="listing-item {{$flag!=1 ? 'closed' : ''}}" data-id="{!!$key+1!!}">
                                    <div class="left-image-block">
                                        <div class="img-holder">
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}"><figure style="background-image: url('{{@$restaurant->mainlogo}}')"></figure></a>
                                        </div>
                                        <div class="action-holder">
                                            <div class="status">
                                                @if($flag == 1)
                                                <span class="open"><i class="ion-android-time"></i>Open</span>
                                                @else
                                                <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                                @endif
                                            </div>
                                           <!--  <a href="#" class="btn btn-theme zing-order-btn">Order Now</a> -->
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}" class="btn view-menu-btn">See Menu</a>
                                        </div>
                                    </div>
                                    <div class="right-content-block">
                                        <a href="#" class="add-fav-btn ion-ios-heart"></a>
                                        <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">
                                            <span class="raty" data-score="{{$restaurant->rating}}"></span>
                                            <h3>{{$restaurant->name}}</h3>
                                            @if($restaurant->published == 'Published')
                                            <span class="zing-partner"><i></i>Partner</span>
                                            @endif
                                            <p class="type">{{$restaurant->type}}</p>
                                            <div class="location">{{number_format($restaurant->distance,2)}} mi <i class="ion ion-android-pin"></i> {{$restaurant->address}}</div>
                                            <div class="right-metas">
                                                <div class="status">
                                                    @if($flag == 1)
                                                    <span class="open"><i class="ion-android-time"></i>Open</span>
                                                    @else
                                                    <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                                    @endif
                                                </div>
                                                <div class="meta-infos">
                                                    <div class="info">
                                                        @if($restaurant->delivery =='Yes')
                                                        <i class="flaticon-fast-delivery"></i>
                                                        @if($restaurant->delivery_charge == 0)
                                                        Free Delivery
                                                        @elseif($restaurant->delivery_charge != '')
                                                        Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                                        @endif {{!empty($restaurant->delivery_limit) ? '(within '.$restaurant->delivery_limit.' mi)' : ''}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="dish-items">
                                                <?php $res_menu = Restaurant::getGoodMenu($restaurant->id); ?>
                                        @forelse($res_menu as $res_menu_det)
                                            <div class="item">
                                                <div class="item-info name">{{$res_menu_det->name}}</div>
                                                <div class="item-info name">{{@$res_menu_det->categories->name}}</div>
                                                <div class="item-info price">${{$res_menu_det->price}}</div>
                                                <div class="item-info rating"><span class="raty" data-score="{{$res_menu_det->review}}"></span></div>
                                            </div>
                                        @empty
                                         <div class="item"><div class="item-info name">
                                        No dishes !!</div></div>
                                        @endif
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                 @endforeach  
                                 <div class="row ">
                                <div class="col-md-12" >
                                    {{$restaurants->links()}}
                                </div>
                            </div>
                             </div>
                             
                        </div>
                    </div>
                </div>
            </section>

<script>
    

$(document).ready(function(){

    $('input[name="del_time_type"]').click(function() {
       if($(this).attr('id') == 'schedule_time') {
            $('#scedule-block').show();           
       } else {
            $('#scedule-block').hide();   
       }
   });

    $('input[name="del_time_typeMob"]').click(function() {
           if($(this).attr('id') == 'schedule_timeMob') {
                $('#scedule-blockMob').show();           
           } else {
                $('#scedule-blockMob').hide();   
           }
       });

    $('[data-toggle="popover"]').popover();
    
    $('#main_category').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
    $('#chain_list').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
        $(".raty").raty({
            readOnly: true,
            starOff: 'fa fa-star-o',
            starOn: 'fa fa-star',
            score: function() {
                return $(this).attr('data-score');
            },
        });
        $("#rating_pill_Header").barrating({
            theme: 'fontawesome-stars',
            showSelectedRating: false,
            onSelect: function(value, text, event) {
              $("#mapcurRating").val(value);
            }
        });
        $('.map-header-filter-wrap .dropdown-menu').click(function (e) {
            e.stopPropagation();
        });





    var someSessionVariable = '<?php echo Session::get('location'); ?>';
    if(someSessionVariable == ''){
        $('#locationModel').modal({show:true, backdrop: 'static', keyboard: false});
    }

    $(".listing-item").on('mouseenter', function(){ 
        var r = $(this).data("id");
        $('.marker-icon').each(function() {
            var m = $(this);
            if(m.data('id') == r){
                m.addClass('map-active');
            }else{
                m.removeClass('map-active');
            }
        });
    }).on('mouseleave', function(){
        $('.marker-icon').removeClass('map-active');
    });
});
var locations=[@foreach($restaurants as $key=>$value) [{!!$value['latitude']!!},{!!$value['longitude']!!},'{{url($value->defaultImage('logo','original'))}}',"{!!addslashes($value['name'])!!}",'{!!addslashes($value['address'])!!}','Preorder','rest-icon',{!!$key+1!!},"{{trans_url('restaurants/')}}/{!!addslashes($value['slug'])!!}"],@endforeach
    ];

                // var map_data1= '';
                // @foreach($restaurants as $key=>$value) 
                // { 
                //   map_data1 = map_data1 +  "[{!!$value['latitude']!!},{!!$value['longitude']!!},'restaurants/thumb/restaurant-01.jpg','{!!$value['name']!!}','{!!$value['address']!!}','Preorder','rest-icon']";
                //   if('{!!$key!!}' < {!!json_encode(count($restaurants))-1 !!}){
                //     map_data1 = map_data1 + ',';
                //   }
                 
                //   }
                // @endforeach
                // var locations=[map_data1];
                // console.log(map_data);

     $('input[name="del_time_type"]').click(function() {
           if($(this).attr('id') == 'schedule_time') {
                $('#scedule-block').show();           
           } else {
                $('#scedule-block').hide();   
           }
       });

         $('input[name="del_time_typeMob"]').click(function() {
           if($(this).attr('id') == 'schedule_timeMob') {
                $('#scedule-blockMob').show();           
           } else {
                $('#scedule-blockMob').hide();   
           }
       });
$('input[name="deli_type"]').click(function() {
        if($(this).attr('id') == 'del_schedule') {
            $("#deltypet_wrap").show();
            $("#deltypet_txt").hide();
        }
        else {
            $("#deltypet_wrap").hide();
            $("#deltypet_txt").show();
        }
    });
    $(document).ready(function() { 
        document.getElementById('timecheck').value = '<?php echo date('h:i:m A'); ?>';
        //new Date().toLocaleTimeString();
        document.getElementById('datecheck').value = '<?php echo date('m/d/Y'); ?>';
        //new Date().toLocaleDateString();
        document.getElementById('time_type').value = 'all';

 document.getElementById('timecheckMob').value = '<?php echo date('h:i:m A'); ?>';
        //new Date().toLocaleTimeString();
        document.getElementById('datecheckMob').value = '<?php echo date('m/d/Y'); ?>';
        //new Date().toLocaleDateString();
        document.getElementById('time_typeMob').value = 'all';
         $('.header-search-form-wrap .search-item .dropdown-menu').click(function(e) {
                    e.stopPropagation();
                 });

         // $("#asap").on('click', function(){ 
         //     document.getElementById('timecheck').value = '<?php echo date('h:i:m A'); ?>';
         //     //new Date().toLocaleTimeString();
         //     document.getElementById('datecheck').value = '<?php echo date('m/d/Y'); ?>';
         //     //new Date().toLocaleDateString();
         //     document.getElementById('time_type').value = 'asap';
         //    })
      
   });     
                
                $(".deltype-dropdown .dropdown-menu").click(function(e){
                    e.stopPropagation();
                });

                $('input[name="del_type"]').click(function() { 
                    var radioValue = $(this).val();
                    $("#del_type_v").html(radioValue)
                });
                var schDateitem = $("#sch_DateWrap .date-item");
                $(schDateitem).click(function() { 
                    var c_Date = $(this).attr("data-date");
                    document.getElementById('datecheck').value = c_Date;
                    $("#sch_DateWrap .date-item").removeClass("active");
                    $(this).addClass("active");
                    $("#del_date_v").html(c_Date)
                });

                var schDateitem1 = $("#sch_DateWrapMob .date-item");
                $(schDateitem1).click(function() { 
                    var c_Date = $(this).attr("data-date"); 
                    document.getElementById('datecheckMob').value = c_Date;
                    $("#sch_DateWrapMob .date-item").removeClass("active");
                    $(this).addClass("active");
                    $("#del_date_vMob").html(c_Date);
                });

                $('#sch_time').on('change', function() { 
                    var c_Time = $(this).find(":checked").val(); 
                    document.getElementById('timecheck').value = c_Time;
                     document.getElementById('time_type').value = 'scheduled';
                    $("#del_time_v").html(c_Time);
                            $(".restaurant_filter").submit();

                });

    $('#sch_timeMob').on('change', function() { 
        var c_Time = $(this).find(":checked").val();
        document.getElementById('timecheckMob').value = c_Time;
         document.getElementById('time_typeMob').value = 'scheduled';
        $("#del_time_vMob").html(c_Time);

       $(".restaurant_filter_Mob").submit();
    });

    $('#all_time').on('click', function() { 
     
         document.getElementById('time_type').value = 'all';
         $("#del_time_v").html('All Time');
         $("#del_date_v").html('');

         $(".restaurant_filter").submit();
    });
    $('#all_timeMob').on('click', function() { 
       
         document.getElementById('time_typeMob').value = 'all';
         $("#del_time_vMob").html('All Time');
         $("#del_date_vMob").html('');

        $(".restaurant_filter_Mob").submit();
    });

    $('#asap_time').on('click', function() { 
       
         document.getElementById('time_type').value = 'asap';
         $("#del_time_v").html('Open Now');
         $("#del_date_v").html('');
          $(".restaurant_filter").submit();
    });
     $('#asap_timeMob').on('click', function() { 
       
         document.getElementById('time_typeMob').value = 'asap';
         $("#del_time_vMob").html('Open Now');
         $("#del_date_vMob").html('');

        $(".restaurant_filter_Mob").submit();
    });

      $('#rating_pill_mobile_Header').change(function(e){

   $(".restaurant_filter_Mob").submit();
})

      $('#main_categoryMob').change(function(e){

    $(".restaurant_filter_Mob").submit();
})

      $('#chain_listMob').change(function(e){ 

   $(".restaurant_filter_Mob").submit();
})
$("input[name='priceMob']").change(function(){
    var value = $("input[name=priceMob]:checked").attr("data-value");
     $("#priceMob_change").html(value);
    var price = document.querySelector('input[name="priceMob"]:checked').value; 
    $(".restaurant_filter_Mob").submit();
});


                
$('#order_type').change(function(e){ 
var type = $("#order_type option:selected").val();  
   $(".restaurant_filter").submit();
});

$('#order_typeMob').change(function(e){ 
var type = $("#order_typeMob option:selected").val();  
   $(".restaurant_filter_Mob").submit();
});

$('#rating_pill_Header').change(function(e){ 
   $(".restaurant_filter").submit();
})   
 $('#main_category').change(function(e){
$(".restaurant_filter").submit();
})
  $('#chain_list').change(function(e){
$(".restaurant_filter").submit();
})

    $("input[name='price']").click(function(){
         var value = $("input[name=price]:checked").attr("data-value");
    $("#price_change").html(value);
    var price = document.querySelector('input[name="price"]:checked').value; 
    $(".restaurant_filter").submit();
});
 $('#sort_filter').change(function(e){
    var val = $("#sort_filter option:selected").val();
    document.getElementById('sort_filter1').value = val;

    $(".restaurant_filter").submit();
   
})

$('#sort_filterMob').change(function(e){
    var val = $("#sort_filterMob option:selected").val();
    document.getElementById('sort_filter2').value = val;

    $(".restaurant_filter_Mob").submit();
   
})
$(".restaurant_filter").submit(function(e) { 

        e.preventDefault();
             formData = $(this).serializeArray(); 
              console.log(formData);
       $.ajax({
                    url: "{{ URL::to('restaurant/restaurantmaplisting') }}/",
                    data: formData,
                    dataType:'JSON',
                    success: function(response){

                            console.log("New Order",response);
                            $('#mapview_section').html(response.new_menus);
                    },
                    error: function(msg) { 
                           
                          }
                });
   });

$(".restaurant_filter_Mob").submit(function(e) { 

        e.preventDefault();
             formData = $(this).serializeArray(); 
             console.log(formData);
       $.ajax({
                    url: "{{ URL::to('restaurant/restaurantmaplisting') }}/",
                    data: formData,
                    dataType:'JSON',
                    success: function(response){

                            console.log("New Order",response);
                            $('#mapview_section').html(response.new_menus);
                    },
                    error: function(msg) { 
                           
                          }
                });
   });
 $('.search_cuisine').click(function(e){
    var favorite = [];
            $.each($("input[name='cuisine_types[]']:checked"), function(){            
                favorite.push($(this).attr('data-name'));
            });
    $.ajax({
            url: "{{ URL::to('restaurant/restaurantmaplisting') }}/",
            data: {type:favorite},
            dataType:'JSON',
            success: function(response){

                    console.log("New Order",response);
                    $('#mapview_section').html(response.new_menus);
            },
            error: function(msg) { 
                   
                  }
        });
})

 // $('#reset_all').click(function(e){ 
    
 // // $( ".restaurant_filter" ).load(window.location.href + " .restaurant_filter" );     
 //    // $("#all_time").attr('checked', 'checked');
 //    //  $('#order_type').val('pickup');
 //    //  $('#rating_pill_Header').val(2);
 //    //  $('#inlineRadio1').attr('checked', 'checked');
 //    //  $('#main_category').val('all');
 //    //  $('#chain_list').val('No');
 //     $('#search_bar').load('{{url("restaurant/map/searchbar")}}');
 //     $(".restaurant_filter").submit();
 // });
</script>


<script>eval(info_bubble);</script>
<script>eval(map_data);</script>
<script>eval(map_init);</script>
<style type="text/css">
    .page-item.active .page-link {
    background-color: #40b659;
    border-color: #40b659;
    }
    .page-link {
        color: #212529;
    }
      .reset-btn {
    font-size: 14px;
    font-weight: 700;
    color: #4BCC88;
    display: inline-block;
    float: right;
    margin-left: 15px;

}
</style>