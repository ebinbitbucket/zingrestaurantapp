<section class="cusine-slider-wrap map-cusine-slider-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cusine-slider-wrap-inner">
                    <div class="cusine-slider swiper-container">
                        <div class="swiper-wrapper">
                            @foreach(Master::getDetailCuisines() as $cuisine)
                            <div class="swiper-slide cuisine-item">
                                <input type="checkbox" name="cuisine_types[]" id="{{$cuisine->id}}" class="search_cuisine" value="{{$cuisine->id}}" data-name="{{$cuisine->name}}">
                                <label for="{{$cuisine->id}}">
                                    <span class="cuisene-img" style="background-image: url({{url($cuisine->defaultImage('image','sm'))}})"></span>
                                    <h4>{{$cuisine->name}}</h4>
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="cusine-slider-prev ion-android-arrow-back"></div>
                    <div class="cusine-slider-next ion-android-arrow-forward"></div>
                </div>
                <div class="cusine-slider-mob-wrap-inner">
                    <div class="cusine-slider-mob">
                        <div class="cuisine-item">
                            <input type="checkbox" name="cuisine_types[]" id="cu_37" class="search_cuisine_mob" value="37" data-name="American">
                            <label for="cu_37">
                                <span class="cuisene-img" style="background-image: url('https://zingmyorder.com/image/sm/master/cuisine/2019/08/23/234103341/image/american.png')"></span>
                                <h4>American</h4>
                            </label>
                        </div>
                        <div class="cuisine-item">
                            <input type="checkbox" name="cuisine_types[]" id="cu_26" class="search_cuisine_mob" value="26" data-name="Barbeque">
                            <label for="cu_26">
                                <span class="cuisene-img" style="background-image: url('https://zingmyorder.com/image/sm/master/cuisine/2019/08/17/122205994/image/barbeque.png')"></span>
                                <h4>Barbeque</h4>
                            </label>
                        </div>
                        <div class="cuisine-item">
                            <input type="checkbox" name="cuisine_types[]" id="cu_28" class="search_cuisine_mob" value="28" data-name="Brazilian">
                            <label for="cu_28">
                                <span class="cuisene-img" style="background-image: url('https://zingmyorder.com/image/sm/master/cuisine/2019/08/17/151747564/image/brazilian.png')"></span>
                                <h4>Brazilian</h4>
                            </label>
                        </div>
                    </div>
                    <button type="button" class="cuisine-popup-mob-btn ion ion-android-more-vertical" data-toggle="modal" data-target="#cuisine_modal"></button>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="search_bar">
<form class="restaurant_filter">
    <div class="map-header-filter-wrap">
        <div class="map-filter-wrap-inner">
            <div class="filter-item filter-item-sche">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-clock"></i> <span><span id="del_date_v">Any Hours</span>
<span id="del_time_v"></span></span></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="del-time-wrap">
                            <div class="time-item">
                                <input type="radio" name="del_time_type" id="all_time" value="all" checked="checked">
                                <label for="all_time"><i class="ik ik-clock"></i>Any Hours</label>
                            </div>
                            <div class="time-item">
                                <input type="radio" name="del_time_type" id="asap_time" value="asap">
                                <label for="asap_time"><i class="ik ik-bell"></i>Open Now</label>
                            </div>
                            <div class="time-item">
                                <input type="radio" id="schedule_time" name="del_time_type" value="scheduled">
                                <label for="schedule_time"><i class="ik ik-calendar"></i>Change Time</label>
                            </div>
                        </div>
                        <div class="schedule-inner-block" id="scedule-block" style="display: none;">
                            <div class="form-group">
                                <p>Change Time to see eateries that are open at the selected time.</p>
                                <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                    @for($i=0;$i<5;$i++)
                                        <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('d M', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                            <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                            <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="form-group mb-0">
                                <select class="form-control" id="sch_time">
                                    <option value="">Select a Time</option>
                                    <option>12:00 AM</option>
                                    <option>12:15 AM</option>
                                    <option>12:30 AM</option>
                                    <option>12:45 AM</option>
                                    <option>1:00 AM</option>
                                    <option>1:15 AM</option>
                                    <option>1:30 AM</option>
                                    <option>1:45 AM</option>
                                    <option>2:00 AM</option>
                                    <option>2:15 AM</option>
                                    <option>2:30 AM</option>
                                    <option>2:45 AM</option>
                                    <option>3:00 AM</option>
                                    <option>3:15 AM</option>
                                    <option>3:30 AM</option>
                                    <option>3:45 AM</option>
                                    <option>4:00 AM</option>
                                    <option>4:15 AM</option>
                                    <option>4:30 AM</option>
                                    <option>4:45 AM</option>
                                    <option>5:00 AM</option>
                                    <option>5:15 AM</option>
                                    <option>5:30 AM</option>
                                    <option>5:45 AM</option>
                                    <option>6:00 AM</option>
                                    <option>6:15 AM</option>
                                    <option>6:30 AM</option>
                                    <option>6:45 AM</option>
                                    <option>7:00 AM</option>
                                    <option>7:15 AM</option>
                                    <option>7:30 AM</option>
                                    <option>7:45 AM</option>
                                    <option>8:00 AM</option>
                                    <option>8:15 AM</option>
                                    <option>8:30 AM</option>
                                    <option>8:45 AM</option>
                                    <option>9:00 AM</option>
                                    <option>9:15 AM</option>
                                    <option>9:30 AM</option>
                                    <option>9:45 AM</option>
                                    <option>10:00 AM</option>
                                    <option>10:15 AM</option>
                                    <option>10:30 AM</option>
                                    <option>10:45 AM</option>
                                    <option>11:00 AM</option>
                                    <option>11:15 AM</option>
                                    <option>11:30 AM</option>
                                    <option>11:45 AM</option>
                                    <option>12:00 PM</option>
                                    <option>12:15 PM</option>
                                    <option>12:30 PM</option>
                                    <option>12:45 PM</option>
                                    <option>1:00 PM</option>
                                    <option>1:15 PM</option>
                                    <option>1:30 PM</option>
                                    <option>1:45 PM</option>
                                    <option>2:00 PM</option>
                                    <option>2:15 PM</option>
                                    <option>2:30 PM</option>
                                    <option>2:45 PM</option>
                                    <option>3:00 PM</option>
                                    <option>3:15 PM</option>
                                    <option>3:30 PM</option>
                                    <option>3:45 PM</option>
                                    <option>4:00 PM</option>
                                    <option>4:15 PM</option>
                                    <option>4:30 PM</option>
                                    <option>4:45 PM</option>
                                    <option>5:00 PM</option>
                                    <option>5:15 PM</option>
                                    <option>5:30 PM</option>
                                    <option>5:45 PM</option>
                                    <option>6:00 PM</option>
                                    <option>6:15 PM</option>
                                    <option>6:30 PM</option>
                                    <option>6:45 PM</option>
                                    <option>7:00 PM</option>
                                    <option>7:15 PM</option>
                                    <option>7:30 PM</option>
                                    <option>7:45 PM</option>
                                    <option>8:00 PM</option>
                                    <option>8:15 PM</option>
                                    <option>8:30 PM</option>
                                    <option>8:45 PM</option>
                                    <option>9:00 PM</option>
                                    <option>9:15 PM</option>
                                    <option>9:30 PM</option>
                                    <option>9:45 PM</option>
                                    <option>10:00 PM</option>
                                    <option>10:15 PM</option>
                                    <option>10:30 PM</option>
                                    <option>10:45 PM</option>
                                    <option>11:00 PM</option>
                                    <option>11:15 PM</option>
                                    <option>11:30 PM</option>
                                    <option>11:45 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="schedule-inner-block" id="all-block" >
                            <div class="form-group m-0">
                                <p>See Eateries Open or Closed any time today.</p>
                            </div>
                        </div>
                        <div class="schedule-inner-block" id="open-block" style="display: none;">
                            <div class="form-group m-0">
                                <p>See eateries that are open now.</p>
                            </div>
                        </div>

                    </div>
                </div>
                <input type="hidden" name="working_date" id="datecheck" value="{{date('d M', strtotime(date('d-M-Y')))}}">
                <input type="hidden" name="working_hours" id="timecheck" value="{{date('g:i A')}}">
                <input type="hidden" name="time_type" id="time_type">
                <input type="hidden" name="sort_filter" id="sort_filter1">
            </div>
            <div class="filter-item filter-item-type">
                <select name="delivery" id="order_type" class="selectize type-select">
                    <option value="pickup" >All Take Out</option>
                <option value="orderpickup" >Order Take Out</option>
                <option value="orderdelivery" >Order Delivery</option>
                </select>
            </div>
            <div class="filter-item filter-item-rating">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-star"></i> <input type="text" id="mapcurRating"  value="2"  readonly></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                       <p>Select Rating</p>

                         <select id="rating_pill_Header" name="rating" autocomplete="off">
                            <option value="1">1</option>
                            <option value="2" selected>2</option>
                            <option value="3">3</option>
                            <option value="4" >4</option>
                            <option value="5">5</option>
                        </select>

                    </div>
                </div>
            </div>
            <div class="filter-item filter-item-price">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="price_change">$</span></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <p>Select Price</p>
                        <div class="pricing-block">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="1" name="price" id="inlineRadio1" checked data-value="$">
                                <label class="form-check-label" for="inlineRadio1">$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="5" name="price" id="inlineRadio2" data-value="$$">
                                <label class="form-check-label" for="inlineRadio2">$$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="12" name="price" id="inlineRadio3" data-value="$$$">
                                <label class="form-check-label" for="inlineRadio3">$$$</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" value="20" name="price" id="inlineRadio4" data-value="$$$$">
                                <label class="form-check-label" for="inlineRadio4">$$$$</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-item filter-item-category">
                <select name="main_category" id="main_category" class="selectize">
                    <option value="all" selected>All Eateries</option>
                     @forelse(Master::getMasterAllCategories() as $key => $categories)
                        <option value="{{$categories->id}}" >{{ucfirst($categories->name)}}</option>
                    @empty
                    @endif
                </select>
            </div>
            <div class="filter-item">
                 <select class="selectize" name="chain_list" id="chain_list">
                    <option value='all'>All</option>
                        <option value="No" selected="selected">Local</option>
                        <option value="Yes">Chain</option>
                </select>
            </div>
            <div class="filter-item filter-item-range">
              <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="distance">10mi</span></button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <p class="mb-10">Select Distance</p>
                    <div class="range-slider">
                        <input id="distanceRange" data-type="single" data-min="3" data-max="25" data-postfix="mi" type="text" name="maxDistance" value="" />
                        <input type="hidden" name="maxDistance" id="distanceRange_val" value="10">
                    </div>
                </div>
            </div>
            </div>
            <div class="filter-item">
                <button type="button" class="btn btn-secondary" id="reset_all">Reset</button>
            </div>
        </div>
    </div>
</form>
 <form class="restaurant_filter_Mob">
   <div class="map-header-filter-wrap-mobile">
        <div class="map-filter-wrap-inner">
            <div class="filter-item filter-item-sche">
                <div class="dropdown">
                    <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-clock"></i> <span id="del_date_vMob"></span>
        <span id="del_time_vMob">Any Hours</span></button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <div class="del-time-wrap">
                            <div class="time-item">
                                <input type="radio" name="del_time_typeMob" id="all_timeMob" value="all" checked="checked">
                                <label for="all_timeMob"><i class="ik ik-clock"></i>Any Hours</label>
                            </div>
                            <div class="time-item">
                                <input type="radio" name="del_time_typeMob" id="asap_timeMob" value="asap">
                                <label for="asap_timeMob"><i class="ik ik-bell"></i>Open Now</label>
                            </div>
                            <div class="time-item">
                                <input type="radio" id="schedule_timeMob" name="del_time_typeMob" value="scheduled">
                                <label for="schedule_timeMob"><i class="ik ik-calendar"></i>Change Time</label>
                            </div>
                        </div>
                        <div class="schedule-inner-block" id="scedule-blockMob" style="display: none;">
                            <div class="form-group">
                                <p>Change Time to see eateries that are open at the selected time.</p>
                                <div class="sch-date-wrap" id="sch_DateWrapMob" name="datesearch">
                                   @for($i=0;$i<5;$i++)
                                        <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('d M', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                            <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                            <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="form-group mb-0">
                               <!--  <p>Desired Delivery Time</p> -->
                                <select class="form-control" id="sch_timeMob">
                                    <option value="">Select a Time</option>
                                    <option>12:00 AM</option>
                                    <option>12:15 AM</option>
                                    <option>12:30 AM</option>
                                    <option>12:45 AM</option>
                                    <option>1:00 AM</option>
                                    <option>1:15 AM</option>
                                    <option>1:30 AM</option>
                                    <option>1:45 AM</option>
                                    <option>2:00 AM</option>
                                    <option>2:15 AM</option>
                                    <option>2:30 AM</option>
                                    <option>2:45 AM</option>
                                    <option>3:00 AM</option>
                                    <option>3:15 AM</option>
                                    <option>3:30 AM</option>
                                    <option>3:45 AM</option>
                                    <option>4:00 AM</option>
                                    <option>4:15 AM</option>
                                    <option>4:30 AM</option>
                                    <option>4:45 AM</option>
                                    <option>5:00 AM</option>
                                    <option>5:15 AM</option>
                                    <option>5:30 AM</option>
                                    <option>5:45 AM</option>
                                    <option>6:00 AM</option>
                                    <option>6:15 AM</option>
                                    <option>6:30 AM</option>
                                    <option>6:45 AM</option>
                                    <option>7:00 AM</option>
                                    <option>7:15 AM</option>
                                    <option>7:30 AM</option>
                                    <option>7:45 AM</option>
                                    <option>8:00 AM</option>
                                    <option>8:15 AM</option>
                                    <option>8:30 AM</option>
                                    <option>8:45 AM</option>
                                    <option>9:00 AM</option>
                                    <option>9:15 AM</option>
                                    <option>9:30 AM</option>
                                    <option>9:45 AM</option>
                                    <option>10:00 AM</option>
                                    <option>10:15 AM</option>
                                    <option>10:30 AM</option>
                                    <option>10:45 AM</option>
                                    <option>11:00 AM</option>
                                    <option>11:15 AM</option>
                                    <option>11:30 AM</option>
                                    <option>11:45 AM</option>
                                    <option>12:00 PM</option>
                                    <option>12:15 PM</option>
                                    <option>12:30 PM</option>
                                    <option>12:45 PM</option>
                                    <option>1:00 PM</option>
                                    <option>1:15 PM</option>
                                    <option>1:30 PM</option>
                                    <option>1:45 PM</option>
                                    <option>2:00 PM</option>
                                    <option>2:15 PM</option>
                                    <option>2:30 PM</option>
                                    <option>2:45 PM</option>
                                    <option>3:00 PM</option>
                                    <option>3:15 PM</option>
                                    <option>3:30 PM</option>
                                    <option>3:45 PM</option>
                                    <option>4:00 PM</option>
                                    <option>4:15 PM</option>
                                    <option>4:30 PM</option>
                                    <option>4:45 PM</option>
                                    <option>5:00 PM</option>
                                    <option>5:15 PM</option>
                                    <option>5:30 PM</option>
                                    <option>5:45 PM</option>
                                    <option>6:00 PM</option>
                                    <option>6:15 PM</option>
                                    <option>6:30 PM</option>
                                    <option>6:45 PM</option>
                                    <option>7:00 PM</option>
                                    <option>7:15 PM</option>
                                    <option>7:30 PM</option>
                                    <option>7:45 PM</option>
                                    <option>8:00 PM</option>
                                    <option>8:15 PM</option>
                                    <option>8:30 PM</option>
                                    <option>8:45 PM</option>
                                    <option>9:00 PM</option>
                                    <option>9:15 PM</option>
                                    <option>9:30 PM</option>
                                    <option>9:45 PM</option>
                                    <option>10:00 PM</option>
                                    <option>10:15 PM</option>
                                    <option>10:30 PM</option>
                                    <option>10:45 PM</option>
                                    <option>11:00 PM</option>
                                    <option>11:15 PM</option>
                                    <option>11:30 PM</option>
                                    <option>11:45 PM</option>
                                </select>
                            </div>
                        </div>
                        <div class="schedule-inner-block" id="all-blockMob" >
                            <div class="form-group">
                                <p>See Eateries Open or Closed any time today.</p>
                            </div>
                        </div>
                        <div class="schedule-inner-block" id="open-blockMob" style="display: none;">
                            <div class="form-group">
                                <p>See eateries that are open now.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="working_date" id="datecheckMob" value="{{date('d M', strtotime(date('d-M-Y')))}}">
                 <input type="hidden" name="working_hours" id="timecheckMob" value="{{date('g:i A')}}">
                <input type="hidden" name="time_type" id="time_typeMob">
                <input type="hidden" name="sort_filter" id="sort_filter2">
            </div>
            <div class="filter-item filter-item-type">
                <select name="delivery" id="order_typeMob" class="selectize type-select">
                     <option value="pickup" >All Take Out</option>
                <option value="orderpickup" >Order Take Out</option>
                <option value="orderdelivery" >Order Delivery</option>
                </select>
            </div>

        </div>
        <button class="adv-filter-toggle ion-android-more-vertical" type="button" data-toggle="collapse" data-target="#mobile_filter_advance" aria-expanded="true" aria-controls="mobile_filter_advance"></button>
        </div>
        <div class="adv-filter-mob-wrap">
            <div class="collapse show adv-filter-mob-wrap-out" id="mobile_filter_advance">
                <div class="adv-filter-mob-wrap-inner">
                    <div class="filter-item filter-item-rating">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-star"></i> <input type="text" id="curRatingMob"   value="2" readonly></button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                               <p>Select Rating</p>

                                     <select id="rating_pill_mobile_Header" name="rating" autocomplete="off">
                                        <option value="1">1</option>
                                        <option value="2" selected>2</option>
                                        <option value="3">3</option>
                                        <option value="4" >4</option>
                                        <option value="5">5</option>
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="filter-item filter-item-price">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="priceMob_change">$</span></button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <p>Select Price</p>
                                <div class="pricing-block">
                                     <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" value="1" name="priceMob" id="inlineRadio11" checked data-value="$">
                                        <label class="form-check-label" for="inlineRadio11">$</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" value="5" name="priceMob" id="inlineRadio21" data-value="$$">
                                        <label class="form-check-label" for="inlineRadio21">$$</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" value="12" name="priceMob" id="inlineRadio31" data-value="$$$">
                                        <label class="form-check-label" for="inlineRadio31">$$$</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" value="20" name="priceMob" id="inlineRadio41" data-value="$$$$">
                                        <label class="form-check-label" for="inlineRadio41">$$$$</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filter-item">
                         <select name="main_category" id="main_categoryMob" class="form-control">
                            <option value="all" selected>All Eateries</option>
                             @forelse(Master::getMasterAllCategories() as $key => $categories)
                                <option value="{{$categories->id}}" >{{ucfirst($categories->name)}}</option>
                            @empty
                            @endif
                        </select>
                    </div>
                    <div class="filter-item">
                        <select class="form-control" name="chain_list" id="chain_listMob">
                            <option value='all'>All</option>
                                <option value="No" selected="selected">Local</option>
                                <option value="Yes">Chain</option>
                        </select>
                    </div>
                    <div class="filter-item filter-item-range">
                            <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="distanceMob">10mi</span></button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <p class="mb-10">Select Distance</p>
                                <div class="range-slider">
                                    <input id="distanceRangeMob" data-type="single" data-min="3" data-max="25" data-postfix="mi" type="text" name="maxDistance" value="" />
                                    <input type="hidden" name="maxDistance" id="distanceRange_valMob" value="10">
                                </div>
                            </div>
                        </div>
                        </div>
                </div>
            </div>
    </div>
</form>
</div>
 <section class="search-map-main-wrap" id="mapview_section">
                <div class="container-fluid">
                    <div class="row">
                        <div class="search-map-wrapper split-map">
                            <div id="search-map"></div>
                        </div>
                        <div id="map_restauarnt" class="search-filter-result-wrapper">
                            <div class="alert-wrapper">
                                <div class="alert alert-success" role="alert" id="sectionHead_time" >Your Cart contains <span id="cart_count">{{Cart::count()}}</span> item(s). <a href="{{url('cart/checkout')}}">Click here to Checkout.</a></div>
                            </div>

                            <div class="search-meta-wrapper">
                                <p class="search-number">{{$restaurants->total()}} Results found within 10 miles of the entered address.</p>
                                <select name="sort_filter" placeholder="Sort by" id="sort_filter" class="form-control">
                                    <option>Sort By</option>
                                    <option value="distance">Distance</option>
                                    <option value="pricelh">Price (Low to High)</option>
                                    <option value="pricehl">Price (High to Low)</option>
                                    <option value="rating">Rating (High to Low)</option>
                                </select>
                            </div>
                            <div class="search-meta-wrapper-mob">

                                <p class="search-number">{{$restaurants->total()}} Results found within 10 miles of the entered address.</p>
                                <select name="sort_filter" placeholder="Sort by" id="sort_filterMob" class="form-control">
                                    <option>Sort By</option>
                                    <option value="distance">Distance</option>
                                    <option value="pricelh">Price (Low to High)</option>
                                    <option value="pricehl">Price (High to Low)</option>
                                    <option value="rating">Rating (High to Low)</option>
                                </select>

                            </div>
                            <div class="search-result-wrapper restaurant-list-wraper" id="">
                                @foreach($restaurants as $key => $restaurant)
                                <?php $flag = 0;
                                $flag                                       = Restaurant::getOpenStatus($restaurant->id);?>

                                <div class="listing-item {{$flag!=1 ? 'closed' : ''}}" data-id="{!!$key+1!!}">
                                    <div class="left-image-block">
                                        <div class="img-holder">
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}"><figure style="background-image: url('{{@$restaurant->mainlogo}}')"></figure></a>
                                        </div>
                                        <div class="action-holder">
                                            <div class="status">
                                                @if($flag == 1)
                                                <span class="open"><i class="ion-android-time"></i>Open</span>
                                                @else
                                                <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                                @endif
                                            </div>
                                           <!--  <a href="#" class="btn btn-theme zing-order-btn">Order Now</a> -->
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}" class="btn view-menu-btn">See Menu</a>
                                        </div>
                                    </div>
                                    <div class="right-content-block">
                                        <a href="#" class="add-fav-btn ion-ios-heart"></a>



                                            <h3><a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">{{$restaurant->name}}</a></h3>
                                            @if($restaurant->published == 'Published')
                                            <span class="zing-partner"><i></i>Partner</span>
                                            @endif
                                            <div class="rating-pricerang-wrap">
                                                <span class="rating"><i class="fa fa-star"></i>{{$restaurant->rating}}</span>
                                                <span class="price-range">${{number_format(@$restaurant->price_range_min,0)}}-{{number_format(@$restaurant->price_range_max,0)}}</span>
                                            </div>
                                            <p class="type">{{$restaurant->type}}</p>
                                            <div class="location">{{number_format($restaurant->distance,2)}} mi <i class="ion ion-android-pin"></i> {{$restaurant->address}}</div>
                                            <div class="right-metas">
                                                <div class="status">
                                                    @if($flag == 1)
                                                    <span class="open"><i class="ion-android-time"></i>Open</span>
                                                    @else
                                                    <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                                    @endif
                                                </div>
                                                <div class="meta-infos">
                                                    <div class="info">
                                                        @if($restaurant->delivery =='Yes')
                                                        <i class="flaticon-fast-delivery"></i>
                                                        @if($restaurant->delivery_charge == 0)
                                                        Free Delivery
                                                        @elseif($restaurant->delivery_charge != '')
                                                        Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                                        @endif {{!empty($restaurant->delivery_limit) ? '(within '.$restaurant->delivery_limit.' mi)' : ''}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="dish-items">
                                                <?php $res_menu = Restaurant::getGoodMenu($restaurant->id);?>
                                                    @forelse($res_menu as $res_menu_det)
                                                    <div class="dish-item-block-mob">
                                                        <div class="item-cat-name">{{$res_menu_det->name}}</div>
                                                        <div class="item-name-price-rat">
                                                            <p>
                                                                {{@$res_menu_det->categories->name}} 
                                                                @if($restaurant->published == 'Published')
                                                                <a href="#{{$res_menu_det->slug}}" class="add-cart-btn" onclick="addonModel('{{$res_menu_det->id}}')" data-toggle="modal" data-target="#{{$res_menu_det->slug}}"><i class="ik ik-shopping-bag"></i> Order</a>
                                                                @endif
                                                            </p>
                                                            <div class="item-rate-price">
                                                                <span class="rating"><i class="fa fa-star"></i>{{$res_menu_det->review}}</span>
                                                                <span>${{$res_menu_det->price}}</span>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                    @empty
                                                     <div class="item"><div class="item-info name">
                                                    Menu Unavailable</div>
                                                </div>
                                                    @endif
                                            </div>
                                    </div>
                                </div>
                                 @endforeach
                                 <div class="row">
                                <div class="col-md-12" >
                                    {{$restaurants->links()}}
                                </div>
                            </div>
                             </div>

                        </div>
                    </div>
                </div>
            </section>
<div class="modal fade menu-item-modal" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalTitle" aria-hidden="true">
     {!!Form::vertical_open()
    ->id('menu-cart-add')
    ->addClass('modal-dialog modal-dialog-scrollable modal-dialog-centered')
    ->action('/carts/save/')
    ->method('GET')!!}
        <div class="modal-content"></div>
    {!!Form::close()!!}
</div>

<script>

var baseUrl ="{{url('/') }}";
var transUrl ="{{trans_url('/') }}";
var someSessionVariable = '<?php echo Session::get('location'); ?>';
document.getElementById('timecheck').value = '<?php echo date('h:i:m A'); ?>';
        //new Date().toLocaleTimeString();
document.getElementById('datecheck').value = '<?php echo date('m/d/Y'); ?>';
        //new Date().toLocaleDateString();
document.getElementById('time_type').value = 'all';

document.getElementById('timecheckMob').value = '<?php echo date('h:i:m A'); ?>';
        //new Date().toLocaleTimeString();
document.getElementById('datecheckMob').value = '<?php echo date('m/d/Y'); ?>';
        //new Date().toLocaleDateString();
document.getElementById('time_typeMob').value = 'all';
$('.header-search-form-wrap .search-item .dropdown-menu').click(function(e) {
    e.stopPropagation();
});
var geocoder = new google.maps.Geocoder();
google.maps.event.addDomListener(window, 'load', function () {
    var places = new google.maps.places.Autocomplete(document.getElementById('googlelocation'));
    google.maps.event.addListener(places, 'place_changed', function () {
    geocodeAddress(geocoder);
    });
});

var locations=[@foreach($restaurants as $key=>$value) [{!!$value['latitude']!!},{!!$value['longitude']!!},'{{url($value->defaultImage('logo','original'))}}',"{!!addslashes($value['name'])!!}",'{!!addslashes($value['address'])!!}','Preorder','rest-icon',{!!$key+1!!},"{{trans_url('restaurants/')}}/{!!addslashes($value['slug'])!!}"],@endforeach
    ];
</script>


<script>eval(info_bubble);</script>
<script>eval(map_data);</script>
<script>eval(map_init);</script>
<style type="text/css">
    .page-item.active .page-link {
    background-color: #40b659;
    border-color: #40b659;
    }
    .page-link {
        color: #212529;
    }
      .reset-btn {
    font-size: 14px;
    font-weight: 700;
    color: #4BCC88;
    display: inline-block;
    float: right;
    margin-left: 15px;

}
</style>