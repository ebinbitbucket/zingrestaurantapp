     {!!Form::vertical_open()
                ->method('GET')
                ->action('restaurants/mapview')
                ->addclass('search-form')
                !!}
                                    <div class="search-wrap-inner">
                                        <div class="inner-item location">
                                            <i class="flaticon-pin"></i>
                                            <input type="text" class="form-control" name="search[address]" id="txtPlacessearch" value="{{Session::get('location')}}" placeholder="Delivery Address">
                                        </div>
                                        <div class="inner-item category">
                                            <select name="search[type]" id="category" class="selectize category-select">
                                                <option value="" selected>All</option>
                                            </select>
                                        </div>
                                        <div class="inner-item type">
                                            <select name="search[delivery]" class="selectize type-select">
                                                <option value="Yes" selected>Delivery</option>
                                                <option value="" selected>Pickup</option>
                                            </select>
                                        </div>
                                        <button type="submit" class="btn btn-theme">Search</button>
                                        <a href="#" class="more-filter-btn">
                                            <span></span>
                                            <span></span>
                                            <span></span>
                                        </a>
                                    </div>
                                    <div class="advanced-search-wrapper">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" placeholder="Keywords">
                                            </div>
                                            <div class="col-md-3">
                                                <div class="radio-btn">
                                                    <input type="radio" id="del_type_asap" name="delivery_type" value="asap" class="custom-control-input">
                                                    <label for="del_type_asap">Asap</label>
                                                </div>
                                                <div class="radio-btn">
                                                    <input type="radio" id="del_type_schedule" name="delivery_type" value="schedule_time" class="custom-control-input">
                                                    <label for="del_type_schedule">Schedule Time</label>
                                                </div>
                                            </div>
                                            <input type="hidden" name="search[working_hours]" id="timecheck">
                                            <div class="col-md-5 range-wrapper">
                                                <span class="filter-inp-title">Price Range</span>
                                                <div class="range-slider">
                                        <div id="priceRangeHeader" data-min="0" data-max="30" data-unit="$"></div>
                                    </div>
                                            <!--     <div class="range-slider"><div id="priceRangeHeader" data-min="0" data-max="30" data-unit="$"></div></div> -->
                                            </div>
                                        </div>
                                    </div>
                                    {!!Form::close()!!}

                                    

<script type="text/javascript">
    $("#del_type_asap").on('click', function(){
             document.getElementById('timecheck').value = new Date().toLocaleString();
            })
    $("#schedule_time").on('change', function(){ 
            document.getElementById('timecheck').value = document.getElementById('schedule_time').value;
     })
    
</script>

  <script>
$( document ).ready(function() {
    $('#category').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'types',
        labelField: 'types',
        searchField: 'types',
        options: [
@forelse(trans('restaurant::restaurant.options.type') as $key => $types)

    {types: "{{$types}}" },
    @empty
    @endif
],
        create: function(input) {
            return {
                types: input
            }
        }
    });
});
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
<script type="text/javascript">
     var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlacessearch'));
        google.maps.event.addListener(places, 'place_changed', function () {
                geocodeAddress(geocoder);
        });
    });
     function geocodeAddress(geocoder) {
        var address = document.getElementById('txtPlacessearch').value;
        geocoder.geocode({'address': address}, function(results, status) {alert(status);
          if (status === 'OK') {
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
</script>
<script>
  $("#priceRangeHeader").each(function() {
        var dataMin = $(this).attr('data-min');
        var dataMax = $(this).attr('data-max');
        var dataUnit = $(this).attr('data-unit');
        $(this).append("<input type='text' class='first-slider-value' name='search[price_range_min]'/><input type='text' class='second-slider-value' name='search[price_range_max]'/>");
        $(this).slider({
            range: true,
            min: dataMin,
            max: dataMax,
            step: 0.5,
            values: [dataMin, dataMax],
            slide: function(event, ui) {
                event = event;
                $(this).children(".first-slider-value").val(dataUnit + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $(this).children(".second-slider-value").val(dataUnit + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#minPriceHead").val(dataUnit + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#maxPriceHead").val(dataUnit + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            }
        });
        $(this).children(".first-slider-value").val(dataUnit + $(this).slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $(this).children(".second-slider-value").val(dataUnit + $(this).slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $("#minPriceHead").val(dataUnit + $(this).slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $("#maxPriceHead").val(dataUnit + $(this).slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    });
</script>