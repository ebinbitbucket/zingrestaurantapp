<section class="eateries-list-wrap">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="eateries-letters">
                    <a href="{{url('eatery-more')}}">#</a>
                    @foreach (range('A', 'Z') as $char) 
                    <a href="{{url('eatery-'.$char)}}">{{$char}}</a>
                    @endforeach
                </div>
			</div>
		</div>
		<ul class="row companies-overview">
			@forelse($restaurants->chunk(2) as $restaurant) 
		    @foreach($restaurant as $data)
			<li class="col-md-6"><a href="{{!empty($data->slug) ? url('restaurants/'.$data->slug) : '#'}}">{!!$data->name!!}<span class="ion-ios-location-outline">{{$data->address}}</span></a></li>
			@endforeach
	    	@empty
	    	@endif
		</ul>
		<ul class="pagination" role="navigation">
    	{{$restaurants->links()}}
    	</ul>
	</div>
</section>