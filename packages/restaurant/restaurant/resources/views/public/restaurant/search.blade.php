<section class="results-wrap">
        {!!Form::vertical_open()
                ->method('GET')
                ->action('restaurants/mapview')
                ->addclass('search-form')
                !!}
                <div class="container">
                    <div class="header-search-wrap">
                        <div class="search-wrap-inner">
                            <div class="inner-item location">
                                <i class="flaticon-pin"></i>
                                <input type="text" name="search[address]" id="txtPlacess" class="form-control"  value="{{Session::get('location')}}" placeholder="Delivery Address">
                                <input type="hidden" name="search[latitude]" id="latitudes" value="{{Session::get('latitude')}}">
                                <input type="hidden" name="search[longitude]" id="longitudes" value="{{Session::get('longitude')}}">
                            </div>
                            <div class="inner-item category">
                                <input type="text" name="search[type]" class="selectize" id="input-tags3" value="{{Session::get('types')}}" placeholder="All">
                           <!--      <select name="search[type][]" id="type" class="selectize category-select">
                                    <option value="" selected>All</option>
                                </select> -->
                            </div>
                             <div class="inner-item main-cat">
                                                        <select class="selectize" name="search[category_name]" id="category-select" placeholder="{{!empty(Session::get('category_name')) ? Session::get('category_name') : 'All Eateries'}}"></select>
                                                    </div>
                            <div class="inner-item delivery">
                                <select name="search[delivery]" class="selectize type-select">
                                    @if(!empty(Session::get('delivery')))
                                        @if(Session::get('delivery') == 'delivery')
                                            <option value="Yes" selected>Delivery</option>
                                        @else
                                            <option value="" selected>Pickup</option>
                                        @endif
                                    @else
                                         <option value="" selected>Pickup</option>
                                    @endif
                                    
                                </select>
                            </div>
                            <div class="inner-item schedule">
                                <div class="dropdown deltype-dropdown">
                                                        <button class="btn dropdown-toggle btn-block text-left" type="button" id="deltypeMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="del_type_v">@if(Session::get('search_time') && Session::get('search_type')== 'scheduled') {{Session::get('search_time')}} @else ASAP @endif</span>
                                                            <span id="deltypet_wrap" style="display: none;">
                                                                <span id="del_date_v"></span> - 
                                                                <span id="del_time_v"></span>
                                                            </span>
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="deltypeMenu">
                                                           <div class="delivery-type-item">
                                                                <input type="radio" name="del_type" value="ASAP" id="asap" checked="">
                                                                <label for="asap"><i class="ion ion-android-time"></i>ASAP</label>
                                                            </div>
                                                            <div class="delivery-type-item">
                                                                <input type="radio" name="del_type" id="schedule_order" value="Schedule Order">
                                                                <label for="schedule_order"><i class="ion ion-android-calendar"></i>Schedule Order</label>
                                                                <div class="schedule-inner-block" id="scedule-block">
                                                                    <div class="form-group">
                                                                        <p>Select a Delivery Date</p>
                                                                        <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                                                            @for($i=0;$i<5;$i++)
                                                                            <div class="date-item active" data-date="{{date('d-M-Y', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                                                                <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                                                                <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                                                            </div>
                                                                            @endfor
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group mb-0">
                                                                        <p>Desired Delivery Time</p>
                                                                        <select class="selectize" id="sch_time">
                                                                            <option value="">Select a Time</option>
                                                                            <option>12:00 AM</option>
                                                                            <option>12:15 AM</option>
                                                                            <option>12:30 AM</option>
                                                                            <option>12:45 AM</option>
                                                                            <option>01:00 AM</option>
                                                                            <option>01:15 AM</option>
                                                                            <option>01:30 AM</option>
                                                                            <option>01:45 AM</option>
                                                                            <option>02:00 AM</option>
                                                                            <option>02:15 AM</option>
                                                                            <option>02:30 AM</option>
                                                                            <option>02:45 AM</option>
                                                                            <option>03:00 AM</option>
                                                                            <option>03:15 AM</option>
                                                                            <option>03:30 AM</option>
                                                                            <option>03:45 AM</option>
                                                                            <option>04:00 AM</option>
                                                                            <option>04:15 AM</option>
                                                                            <option>04:30 AM</option>
                                                                            <option>04:45 AM</option>
                                                                            <option>05:00 AM</option>
                                                                            <option>05:15 AM</option>
                                                                            <option>05:30 AM</option>
                                                                            <option>05:45 AM</option>
                                                                            <option>06:00 AM</option>
                                                                            <option>06:15 AM</option>
                                                                            <option>06:30 AM</option>
                                                                            <option>06:45 AM</option>
                                                                            <option>07:00 AM</option>
                                                                            <option>07:15 AM</option>
                                                                            <option>07:30 AM</option>
                                                                            <option>07:45 AM</option>
                                                                            <option>08:00 AM</option>
                                                                            <option>08:15 AM</option>
                                                                            <option>08:30 AM</option>
                                                                            <option>08:45 AM</option>
                                                                            <option>09:00 AM</option>
                                                                            <option>09:15 AM</option>
                                                                            <option>09:30 AM</option>
                                                                            <option>09:45 AM</option>
                                                                            <option>10:00 AM</option>
                                                                            <option>10:15 AM</option>
                                                                            <option>10:30 AM</option>
                                                                            <option>10:45 AM</option>
                                                                            <option>11:00 AM</option>
                                                                            <option>11:15 AM</option>
                                                                            <option>11:30 AM</option>
                                                                            <option>11:45 AM</option>
                                                                            <option>12:00 PM</option>
                                                                            <option>12:15 PM</option>
                                                                            <option>12:30 PM</option>
                                                                            <option>12:45 PM</option>
                                                                            <option>01:00 PM</option>
                                                                            <option>01:15 PM</option>
                                                                            <option>01:30 PM</option>
                                                                            <option>01:45 PM</option>
                                                                            <option>02:00 PM</option>
                                                                            <option>02:15 PM</option>
                                                                            <option>02:30 PM</option>
                                                                            <option>02:45 PM</option>
                                                                            <option>03:00 PM</option>
                                                                            <option>03:15 PM</option>
                                                                            <option>03:30 PM</option>
                                                                            <option>03:45 PM</option>
                                                                            <option>04:00 PM</option>
                                                                            <option>04:15 PM</option>
                                                                            <option>04:30 PM</option>
                                                                            <option>04:45 PM</option>
                                                                            <option>05:00 PM</option>
                                                                            <option>05:15 PM</option>
                                                                            <option>05:30 PM</option>
                                                                            <option>05:45 PM</option>
                                                                            <option>06:00 PM</option>
                                                                            <option>06:15 PM</option>
                                                                            <option>06:30 PM</option>
                                                                            <option>06:45 PM</option>
                                                                            <option>07:00 PM</option>
                                                                            <option>07:15 PM</option>
                                                                            <option>07:30 PM</option>
                                                                            <option>07:45 PM</option>
                                                                            <option>08:00 PM</option>
                                                                            <option>08:15 PM</option>
                                                                            <option>08:30 PM</option>
                                                                            <option>08:45 PM</option>
                                                                            <option>09:00 PM</option>
                                                                            <option>09:15 PM</option>
                                                                            <option>09:30 PM</option>
                                                                            <option>09:45 PM</option>
                                                                            <option>10:00 PM</option>
                                                                            <option>10:15 PM</option>
                                                                            <option>10:30 PM</option>
                                                                            <option>10:45 PM</option>
                                                                            <option>11:00 PM</option>
                                                                            <option>11:15 PM</option>
                                                                            <option>11:30 PM</option>
                                                                            <option>11:45 PM</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <input type="hidden" name="search[working_date]" id="datecheck">
                                                    <input type="hidden" name="search[working_hours]" id="timecheck">
                                                    <input type="hidden" name="search[time_type]" id="time_type">
                                    </div>
                            <div class="inner-item price">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <input type="text" id="minPriceHead" name="search[price_range_min]" > -&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" id="maxPriceHead" name="search[price_range_max]"></button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <label>Select Price</label>
                                        <div class="range-slider"><div id="priceRangeHeader" data-min="0" data-max="50" data-unit="$"></div></div>
                                    </div>
                                </div>
                            </div>

                            <div class="inner-item rating">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <input type="text" id="ss" value="{{Session::get('rating')}}" disabled name="search[rating]"><!-- <input type="text" id="curRating" value="4" disabled> --><b class="fa fa-star"></b>
                                        
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <label>Select Rating</label>
                                        <select id="rating-pill" name="search[rating]" autocomplete="off">
                                            <option value="1" {{Session::get('rating') == 1 ? 'selected' : ''}}>1</option>
                                            <option value="2" {{Session::get('rating') == 2 ? 'selected' : ''}}>2</option>
                                            <option value="3" {{Session::get('rating') == 3 ? 'selected' : ''}}>3</option>
                                            <option value="4" {{Session::get('rating') == 4 ? 'selected' : ''}}>4</option>
                                            <option value="5" {{Session::get('rating') == 5 ? 'selected' : ''}}>5</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-theme"><i class="flaticon-search"></i></button>
                    </div>
                </div>
    {!!Form::close()!!}
</section>
<script>
 var CATEGORIES = [
    {
        id:'all',
        name: "All Eateries",
        code: "all",
        icon: "icon flaticon-fast-food"
    },
    @forelse(Master::getMasterAllCategories() as $key => $categories)

    {id: "{{$categories->id}}" ,name: "{{ucfirst($categories->name)}}",code: "{{$categories->name}}",icon: "icon {{$categories->icon}}" },
    @empty
    @endif

 ];

    $("#category-select").selectize({
        maxItems: 1,
        labelField: 'name',
        valueField: 'id',
        searchField: ['name', 'code'],
        options: CATEGORIES,
        preload: true,
        persist: false,
        render: {
            item: function(item, escape) {
                return "<div class='cat-item'><i class='flaticon-" + escape(item.icon) + "'></i>" + escape(item.name) + "</div>";
            },
            option: function(item, escape) {
                return "<div class='cat-item'><i class='flaticon-" + escape(item.icon) + "'></i>" + escape(item.name) + "</div>";
            }
        },
    });

$('#input-tags3').selectize({
    plugins: ['remove_button'],
    persist: false,
    preload: true,
    labelField: 'types',
    valueField: 'types',
    searchField: ['types'],
    options : [

@forelse(Master::getCuisines() as $key => $types)

    {types: "{{$types}}" },
    @empty
    @endif
],
});
            hide = true;
            $('body').on("click", function () {
                if (hide) $(".search-form").removeClass('adv-open');
                hide = true;
             });
            
            $('body').on('click', '.search-form', function () {
                $(this).addClass('adv-open');
                hide = false;
            });

            $(document).ready(function() { 
                document.getElementById('timecheck').value = '<?php echo date('h:i:m A'); ?>';
                //new Date().toLocaleTimeString();
                document.getElementById('datecheck').value = '<?php echo date('d-M-Y'); ?>';
                //new Date().toLocaleDateString();
                document.getElementById('time_type').value = 'asap';

         $("#asap").on('click', function(){ 
             document.getElementById('timecheck').value = '<?php echo date('h:i:m A'); ?>';
             //new Date().toLocaleTimeString();
             document.getElementById('datecheck').value = '<?php echo date('d-M-Y'); ?>';
             //new Date().toLocaleDateString();
             document.getElementById('time_type').value = 'asap';
            })
      $("#sch_time").on('change', function(){ 
        var c_Time = $(this).find(":checked").val();  
            document.getElementById('timecheck').value = c_Time;
            document.getElementById('time_type').value = 'scheduled';
     })
        $('#sch_time').selectize({
        maxItems: 1,
    });

                $('input[name="del_type"]').click(function() {
                    if($(this).attr('id') == 'schedule_order') {
                        $('#scedule-block').show();
                        $("#del_type_v").hide();
                        $("#deltypet_wrap").show();
                    }
                    else {
                        $('#scedule-block').hide();  
                        $("#del_type_v").show(); 
                        $("#deltypet_wrap").hide();
                    }
                });
                $(".deltype-dropdown .dropdown-menu").click(function(e){
                    e.stopPropagation();
                });

                $('input[name="del_type"]').click(function() { 
                    if($(this).attr('id') == 'asap'){
                        $("#del_type_v").html('ASAP')
                    }
                    else{
                        var radioValue = $(this).val();
                    $("#del_type_v").html(radioValue)
                    }
                    
                });
                var schDateitem = $("#sch_DateWrap .date-item");
                $(schDateitem).click(function() { 
                    var c_Date = $(this).attr("data-date");
                    document.getElementById('datecheck').value = c_Date;
                    $("#sch_DateWrap .date-item").removeClass("active");
                    $(this).addClass("active");
                    $("#del_date_v").html(c_Date)
                });
                $('#sch_time').on('change', function() { 
                    var c_Time = $(this).find(":checked").val(); 
                    $("#del_time_v").html(c_Time)
                });

            });
</script>      
<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
<script type="text/javascript">
     var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlacess'));
        google.maps.event.addListener(places, 'place_changed', function () {
                test(geocoder);
        });
    });
     function test(geocoder) {
        var address = document.getElementById('txtPlacess').value; 
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitudes').value=results[0].geometry.location.lat();
            document.getElementById('longitudes').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
</script>
<script>
    $("#del_type_asap").on('click', function(){
             document.getElementById('timecheck').value = '<?php echo date('d-M-Y h:i:m A'); ?>';
             //new Date().toLocaleString();
            })
    $("#schedule_time").on('focusout', function(){ 
            document.getElementById('timecheck').value = document.getElementById('schedule_time').value;
            var radioValue = $('#timecheck').val();
            $("#sch_value").html(radioValue)
     })

    $("#rating-pill").on('change', function(){  
            var rate = $("#rating-pill option:selected").val();
            document.getElementById('ss').value = rate;
            document.getElementById('curRating').value = rate;
     })

      $('input[name="delivery_type"]').click(function() { 
      var radioValue = $(this).val();
      if($(this).attr('id') == 'del_type_schedule') {
        $('#schedule_time').show();           
      }
      else {
        $('#schedule_time').hide();   
      }
      $("#sch_value").html(radioValue)
    });
</script>

  <script>
    var nowDate = new Date();
      
       var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
var endDates =  new Date(nowDate.getFullYear(), nowDate.getMonth() +2, +0);
   $('.datetimepicker').each(function(){
        $(this).datetimepicker({
            stepping: 5,
            minDate: new Date(),
            maxDate: endDates,
            buttons : {
                showClose: true
            }
        })
    });
$( document ).ready(function() { 
    rating_def = '<?php echo Session::get('rating'); ?>';
    if(rating_def == ''){
        document.getElementById('ss').value = 1;
    }
    if("<?php echo Session::get('price_range_min'); ?>" != ''){
        $("#minPriceHead").val("<?php echo Session::get('price_range_min'); ?>");
    $("#maxPriceHead").val("<?php echo Session::get('price_range_max'); ?>");
    $('#min_value').val("<?php echo Session::get('price_range_min'); ?>");
        $('#max_value').val("<?php echo Session::get('price_range_max'); ?>");
    }
           
    $('#type').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'types',
        labelField: 'types',
        searchField: 'types',
        maxItems : null,
        options: [
@forelse(Master::getCuisines() as $key => $types)

    {types: "{{$types}}" },
    @empty
    @endif
]
    });
    
});
</script>
<script>
  $("#priceRangeHeader").each(function() {
        var dataMin = $(this).attr('data-min');
        var dataMax = $(this).attr('data-max');
        var dataUnit = $(this).attr('data-unit');
        
        $(this).slider({
            range: true,
            min: dataMin,
            max: dataMax,
            step: 0.5,
            values: [dataMin, dataMax],
            slide: function(event, ui) {
                event = event;
                $(this).children(".first-slider-value").val(dataUnit + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $(this).children(".second-slider-value").val(dataUnit + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#minPriceHead").val(dataUnit + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#maxPriceHead").val(dataUnit + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            }
        });
        $(this).children(".first-slider-value").val(dataUnit + $(this).slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $(this).children(".second-slider-value").val(dataUnit + $(this).slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $("#minPriceHead").val(dataUnit + $(this).slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $("#maxPriceHead").val(dataUnit + $(this).slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    });
</script>
