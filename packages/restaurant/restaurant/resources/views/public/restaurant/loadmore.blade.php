@forelse($data['menus'] as $menu) 
<div class="dish-grid__item">
    <div class="dish-grid__image-block">
        <a href="javascript:void(0);" class="dish-link" onclick="addToSession('{{$menu->id}}')"></a>
        <img class="dish-grid__image" src="{{url($menu->defaultImage('image','master'))}}">
        <div class="dish-overlay"></div>
        <a href="javascript:void(0);" class="favorite-btn" id="{{$menu->id}}" onclick="addToSession('{{$menu->id}}')"><i class="ion ion-plus-round"></i></a>
        <a href="javascript:void(0);" class="view-more-btn" role="button" data-placement="left" data-toggle="popover" data-trigger="focus" data-content="{{$menu->description}}"><i class="ion ion-more"></i></a>
    </div>
    <h3 class="dish-title"><a href="#">{{$menu->name}}</a></h3>
</div>        
@empty
<!--<div class="dish-grid__item no-items-found">-->
<!--    <img src="{{theme_asset('img/no-dish-items.svg')}}" alt="No items Found">-->
<!--    <h4>No dishes served in your area match the search keywords used.</h4>-->
<!--</div>-->
@endif
