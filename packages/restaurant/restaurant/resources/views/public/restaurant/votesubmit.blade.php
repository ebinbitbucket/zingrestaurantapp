@if($success == 1)
<div class="progress">
	<div class="progress-bar bg-success" role="progressbar" aria-valuenow="{{count($restaurant->votes) + 3}}" aria-valuemin="0" aria-valuemax="30" style="width:{{(count($restaurant->votes) + 3)*10}}%"><span class="vote-count">@if($restaurant->votes != null) {{count($restaurant->votes) + 3}} @else 3 @endif</span></div>
	
</div>
<div class="alert alert-success m-0" role="alert"><b>Thank you for your vote.</b><br>This helps us in our quest to add quality eateries to our platform that people like to order from. We appreciate it.</div>
@else
<div class="alert alert-danger m-0" role="alert">Sorry. You have already submitted your feedback.</div>
@endif
