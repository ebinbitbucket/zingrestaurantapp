 <div class="container-fluid">
    <div class="row">
        <div class="search-map-wrapper split-map">
            <div id="search-map"></div>
        </div>
        <div id="map_restauarnt" class="search-filter-result-wrapper">
              <div class="alert-wrapper">
                <div class="alert alert-success" role="alert" id="sectionHead_time" >Your Cart contains <span id="cart_count">{{Cart::count()}}</span> item(s). <a href="{{url('cart/checkout')}}">Click here to Checkout.</a></div>
            </div>
            <div class="search-meta-wrapper">
                <p class="search-number">{{$restaurants->total()}} Results found within {{@$maxDistance}} miles of the entered address.</p>
                <select name="sort_filter" placeholder="Sort by" id="sort_filter" class="form-control">
                    <option>Sort By</option>
                    <option value="distance">Distance</option>
                    <option value="pricelh">Price (Low to High)</option>
                    <option value="pricehl">Price (High to Low)</option>
                    <option value="rating">Rating (High to Low)</option>
                </select>
            </div>
            <div class="search-result-wrapper restaurant-list-wraper" id="">
                @foreach($restaurants as $key => $restaurant)
                                <?php $flag=0;
                                $flag=Restaurant::getOpenStatus($restaurant->id); ?> 

                                <div class="listing-item {{$flag!=1 ? 'closed' : ''}}" data-id="{!!$key+1!!}">
                                    <div class="left-image-block">
                                        <div class="img-holder">
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}"><figure style="background-image: url('{{@$restaurant->mainlogo}}')"></figure></a>
                                        </div>
                                        <div class="action-holder">
                                            <div class="status">
                                                @if($flag == 1)
                                                <span class="open"><i class="ion-android-time"></i>Open</span>
                                                @else
                                                <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                                @endif
                                            </div>
                                           <!--  <a href="#" class="btn btn-theme zing-order-btn">Order Now</a> -->
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}" class="btn view-menu-btn">See Menu</a>
                                        </div>
                                    </div>
                                    <div class="right-content-block">
                                        <a href="#" class="add-fav-btn ion-ios-heart"></a>
                                        <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">
                                            <span class="raty" data-score="{{$restaurant->rating}}"></span>
                                            <h3>{{$restaurant->name}}</h3>
                                             @if($restaurant->published == 'Published')
                                            <span class="zing-partner"><i></i>Partner</span>
                                            @endif
                                            <p class="type">{{$restaurant->type}}</p>
                                            <div class="location">{{number_format($restaurant->distance,2)}} mi <i class="ion ion-android-pin"></i> {{$restaurant->address}}</div>
                                            <div class="right-metas">
                                                <div class="status">
                                                    @if($flag == 1)
                                                    <span class="open"><i class="ion-android-time"></i>Open</span>
                                                    @else
                                                    <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                                    @endif
                                                </div>
                                                <div class="meta-infos">
                                                    <div class="info">
                                                        @if($restaurant->delivery =='Yes')
                                                        <i class="flaticon-fast-delivery"></i>
                                                        @if($restaurant->delivery_charge == 0)
                                                        Free Delivery
                                                        @elseif($restaurant->delivery_charge != '')
                                                        Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                                        @endif {{!empty($restaurant->delivery_limit) ? '(within '.$restaurant->delivery_limit.' mi)' : ''}}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                           <div class="dish-items">
                                                <?php $res_menu = Restaurant::getGoodMenu($restaurant->id); ?>
                                        @forelse($res_menu as $res_menu_det)
                                        <div class="dish-item-block-mob">
                                                        <div class="item-cat-name">{{$res_menu_det->name}}</div>
                                                        <div class="item-name-price-rat">
                                                            <p>
                                                                {{@$res_menu_det->categories->name}} 
                                                                @if($restaurant->published == 'Published')
                                                                <a href="#{{$res_menu_det->slug}}" class="add-cart-btn" onclick="addonModel('{{$res_menu_det->id}}')" data-toggle="modal" data-target="#{{$res_menu_det->slug}}"><i class="ik ik-shopping-bag"></i> Order</a>
                                                                @endif
                                                            </p>
                                                            <div class="item-rate-price">
                                                                <span class="rating"><i class="fa fa-star"></i>{{$res_menu_det->review}}</span>
                                                                <span>${{$res_menu_det->price}}</span>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                        @empty
                                         <div class="item"><div class="item-info name">
                                        Menu Unavailable</div></div>
                                        @endif
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                 @endforeach  
                                 <div class="row ">
                                <div class="col-md-12" >
                                    {{$restaurants->links()}}
                                </div>
                            </div>
             </div>
             
        </div>
    </div>
</div>
<script>
//   $(".pagination a.page-link").on('click', function (event) { alert('hi');
//     event.preventDefault();
//     var href = $(this).attr('href');
//     alert(href)
//     $.ajax({
//                     url: href,
//                     dataType:'JSON',
//                     success: function(response){

//                             console.log("New Order",response);
//                             $('#mapview_section').html(response.new_menus);
//                     },
//                     error: function(msg) { 
                           
//                           }
//                 });

//   });
$(".raty").raty({
        readOnly: true,
        starOff: 'fa fa-star-o',
        starOn: 'fa fa-star',
        score: function() {
            return $(this).attr('data-score');
        },
    });
 $('#main_category').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
 $('#order_type').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
 $('#chain_list').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
$("#rating_pill_Header").barrating({
            theme: 'fontawesome-stars',
            showSelectedRating: false,
            onSelect: function(value, text, event) {
              $("#mapcurRating").val(value);
            }
        });
$(document).ready(function(){
  $(".pagination a.page-link").on('click', function (event) { 
    event.preventDefault();
    var href = $(this).attr('href');
    $.ajax({
                    url: href,
                    dataType:'JSON',
                    success: function(response){

                            console.log("New Order",response);
                            $('#mapview_section').html(response.new_menus);
                    },
                    error: function(msg) { 
                           
                          }
                });

  });
   var someSessionVariable = '<?php echo Session::get('location'); ?>';
    if(someSessionVariable == ''){
        $('#locationModel').modal({show:true, backdrop: 'static', keyboard: false});
    }

    $(".listing-item").on('mouseenter', function(){ 
        var r = $(this).data("id");
        $('.marker-icon').each(function() {
            var m = $(this);
            if(m.data('id') == r){
                m.addClass('map-active');
            }else{
                m.removeClass('map-active');
            }
        });
    }).on('mouseleave', function(){
        $('.marker-icon').removeClass('map-active');
    });
});
var locations=[@foreach($restaurants as $key=>$value) [{!!$value['latitude']!!},{!!$value['longitude']!!},'{{url($value->defaultImage('logo','original'))}}',"{!!addslashes($value['name'])!!}",'{!!addslashes($value['address'])!!}','Preorder','rest-icon',{!!$key+1!!},"{{trans_url('restaurants/')}}/{!!addslashes($value['slug'])!!}"],@endforeach
    ];


 $('#sort_filter').change(function(e){
    var val = $("#sort_filter option:selected").val();
    document.getElementById('sort_filter1').value = val;

    $(".restaurant_filter").submit();
   
})

$('#sort_filterMob').change(function(e){
    var val = $("#sort_filterMob option:selected").val();
    document.getElementById('sort_filter2').value = val;

    $(".restaurant_filter_Mob").submit();
   
})
</script>


<script>eval(info_bubble);</script>
<script>eval(map_data);</script>
<script>eval(map_init);</script>
<style type="text/css">
    .page-item.active .page-link {
    background-color: #40b659;
    border-color: #40b659;
    }
    .page-link {
        color: #212529;
    }
    .cuisine_label_css {
background-color: #ededed;border-radius: 50px;
/*padding-top: 6px;*/
padding-bottom: 6px;
padding-left: 15px;
padding-right: 15px;
    }
</style>