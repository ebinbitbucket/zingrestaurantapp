<div class="search-meta-wrapper">
                                <p class="search-number">{{$restaurants->total()}} Results found within 10 miles of the entered address.</p>
<!--                                 <a href="{{trans_url('restaurants')}}" class="reset-btn">List View</a>
 -->                            </div>
 <div class="search-result-wrapper restaurant-list-wraper" >
 @foreach($restaurants as $key => $restaurant)
 <?php $flag=0;
                                $flag=Restaurant::getOpenStatus($restaurant['id']); ?> 
                                <div class="listing-item" data-id="{!!$key+1!!}" style="{{$flag!=1 ? 'background-color:#ededed;' : ''}}">
                                    <div class="img-holder">
                                        <figure>
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">
                                                <img src="{{url($restaurant->defaultImage('logo'))}}" class="img-fluid" alt="" style='height: 100%; width: 100%; object-fit: contain'>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="text-holder">
                                        
                                        <!-- <div class="status closed">Preorder</div> -->
                                        <h3 class="title"><a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">{{$restaurant->name}}</a></h3>
                                        <div class="location" style="{{$flag!=1 ? 'color:red;' : 'color:#40b659;'}}"><i class="fa fa-clock-o" style="{{$flag!=1 ? 'color:red;' : ''}}"></i> {{$flag!=1 ? 'Closed' : 'Open'}}</div>
                                        <span class="raty" data-score="{{$restaurant->rating}}"></span>
                                        <p class="type">{{$restaurant->type}}</p>
                                        <div class="location"><i class="ion ion-android-pin"></i>{{$restaurant->address}}</div>
                                        <div class="meta-infos">
                                            @if($restaurant->delivery =='Yes')
                                                 <div class="info">    <i class="flaticon-fast-delivery"></i>Free Delivery</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                 @endforeach 
                             </div>
                                                           <div class="row pull-right">
                                <div class="col-md-12" >
                                    {{$restaurants->links()}}
                                </div>
                            </div>

<script type="text/javascript">
      $(".raty").raty({
        readOnly: true,
        starOff: 'fa fa-star-o',
        starOn: 'fa fa-star',
        score: function() {
            return $(this).attr('data-score');
        },
    });
</script>