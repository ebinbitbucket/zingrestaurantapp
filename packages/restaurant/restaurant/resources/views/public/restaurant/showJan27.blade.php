         {!!'<!-- ' . date("F Y h:i:s u") . '-->'!!}
<div class="single-listing-header-wrap">
                <div class="container">
                    <div class="row" >
                        <div class="col-3 col-md-3 col-lg-2">
                            <div class="header-left-info" >
                                <a class="logo-block" href="{{url('/')}}">
                                    <img src="{{url('assets/img/logo-round-big.png')}}" alt="logo" title="Zing My Order">
                                </a>
                                <div class="user-infos">
                                    @if(Auth::user())
                                    <h3>{{Auth::user()->name}}<span class="points">{{number_format(Cart::getLoyaltypoints(),0)}}</span></h3>
                                    @else
                                    @endif
                                    <div class="location" data-toggle="tooltip" data-placement="top" title="{{Session::get('location')}}"><i class="ik ik-map-pin"></i>{{!empty(Session::get('location')) ? strtok(Session::get('location'),',') : 'Select Location' }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-9 col-md-9 col-lg-10">
                            <div class="single-listing-info">
                                <div class="single-image" style="background-image: url({{url($restaurant->mainlogo)}})"></div>
                                <div class="single-content">
                                    <div class="title-block">
                                        <h3>{{$restaurant->name}}</h3>
                                        <div class="rating">
                                            <span class="raty" data-score="{{$restaurant->rating}}"></span><span class="count"><a href="#review" class="inner-scroll">{{$restaurant->rating}} ({{$restaurant->review->count()}} ratings)</a></span>
                                        </div>
                                        <button onclick="add_fav()" class="add-fav-btn btn" data-toggle="tooltip" data-placement="top" title="Add to Your Favorites" data-original-title="Add to Favorites"><i class="ion ion-ios-heart"></i></button>
                                        <div class="item" style="padding-left: 10px;">
                                            @if(!empty($restaurant->social_media_links)) 
                                                <div class="social">
                                                @foreach($restaurant->social_media_links as $key_link => $val_link)
                                                    @if($key_link == 'facebook' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #3B5998" target="_blank"><i class="fa fa-facebook-f"></i></a>
                                                    @elseif($key_link == 'twitter' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #55ACEE" target="_blank"><i class="fa fa-twitter"></i></a>
                                                    @elseif($key_link == 'youtube' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #bb0000" target="_blank"><i class="fa fa-youtube"></i></a>
                                                    @elseif($key_link == 'linkedln' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #007bb5" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                    @elseif($key_link == 'instagram' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #405DE6" target="_blank"><i class="fa fa-instagram"></i></a>
                                                    @elseif($key_link == 'pintrest' && !empty($val_link)) 
                                                        <a href="{{$val_link}}" style="background-color: #cb2027" target="_blank"><i class="fa fa-pinterest"></i></a>
                                                    @endif
                                                @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <p class="location" data-toggle="tooltip" data-placement="top" title="{{$restaurant->address}}"><i class="ik ik-map-pin"></i> {{strtok($restaurant->address,',')}}</p>
                                        @if(!empty($restaurant->phone))<p class="ml-15"> <i class="ik ik-phone"></i>&nbsp;&nbsp;{{substr($restaurant->phone, 0,3)}}-{{substr($restaurant->phone, 3,3)}}-{{substr($restaurant->phone, 6,4)}}</p>@endif
                                        <p class="ml-15">Today’s Hours  

                                            @if(isset($restaurant->timings[$weekday]))
                                            @php
                                               $resultstr = [];
                                            @endphp
                                            @foreach($restaurant->timings[$weekday] as $key => $val) 
                                                @if($val->opening != 'off' && $val->opening != 'off') 
                                                    @php
                                                       $resultstr[] = date('g:i a',strtotime($val->opening)).' - '.date('g:i a',strtotime($val->closing))
                                                    @endphp
                                                @endif
                                            @endforeach
                                            @php 
                                                echo implode(" & ",$resultstr);
                                            @endphp
                                            @else
                                            
                                            @endif
                                            <!-- @if(isset($restaurant->timings[$weekday]))
                                            @foreach($restaurant->timings[$weekday] as $key => $val) 
                                                @if($val->opening != 'off' && $val->opening != 'closing') 
                                                   {{date('g:i a',strtotime($val->opening))}} - {!!date('g:i a',strtotime($val->closing))!!} &
                                                @endif
                                            @endforeach
                                            @else
                                            @endif -->
                                        </p>
                                    </div>
                                    <div class="meta-infos">
                                        <div class="item">
                                            <div class="eatery-search-form">
                                                <i class="fa fa-search"></i>
                                                <input type="text"  class="form-control" id="dish_quick_search" placeholder="Search Food...">
                                            </div>
                                        </div>
                                        <div class="item">
                                            <a href="#about" class="btn eatery-inner-scroll">More Info</a>
                                        </div>
                                        <div class="item">
                                            <span>
                                                 @if($restaurant->published == 'Published')
                                                @if($restaurant->delivery == 'Yes')
                                                    @if($restaurant->delivery_charge == 0)
                                                        Free Delivery
                                                    @elseif($restaurant->delivery_charge != '')
                                                       Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                                    @endif
                                                @else
                                                    Take out
                                                @endif
                                                @endif
                                            </span>
                                        </div>
                                    </div>

                                     
                                </div>
                                <div class="single-header-actions">
                                    <a href="{{url('/')}}" class="ion ion-android-arrow-back back-btn" data-toggle="tooltip" data-placement="top" title="Back to Listing" data-original-title="Go Back"></a>
                                   <!--  <a href="#" onClick="history.go(-1)" class="ion ion-android-arrow-back back-btn" data-toggle="tooltip" data-placement="top" title="Back to Listing" data-original-title="Go Back"></a> -->
                                    <button class="cart-toggle-btn push-body menu-right" type="button" data-toggle="collapse" data-target="#cart">
                                        <div class="cart-inner">
                                            @if(Cart::count() != 0)
                                            <div class="icon cart-item-exist">
                                                <span class="count cart_count_header ">{{Cart::count()}}</span>
                                                <i class="ik ik-shopping-bag"></i>
                                            </div>
                                            @else
                                            <div class="icon">
                                                <span class="count cart_count_header">{{Cart::count()}}</span>
                                                <i class="ik ik-shopping-bag"></i>
                                            </div>
                                            @endif
                                        </div>
                                    </button>
                                    @if(Auth::user())
                                    @else
                                    <a href="#signIn_signUp_Modal" class="btn btn-theme btn-signin" data-toggle="modal" data-target="#signIn_signUp_Modal"> Sign In</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-listing-header-mobile-wrap">
                <div class="top-nav-wrap">
                    <div class="container">
                        <div class="top-nav-wrap-inner">
                            <a class="logo-block" href="{{url('/')}}">
                                <img src="{{url('assets/img/logo-round-big.png')}}" alt="logo" title="Zing My Order">
                            </a>
                            <a href="#" onClick="history.go(-1)" class="ion ion-android-arrow-back back-btn ml-0" data-toggle="tooltip" data-placement="top" title="Back to Listing" data-original-title="Go Back"></a>
                            <div class="single-header-infos restaurant-info">
                                <div class="restaurant-detail-drpdown">
                                    <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#mobile-restaurant-detail-collapse" aria-expanded="false" aria-controls="mobile-restaurant-detail-collapse">
                                        <span class="rest-infos">
                                            <h3>{{$restaurant->name}}  <i class="ion-chevron-up dropdown-icon"></i></h3>
                                            <p class="pr-0">
                                                <span>
                                                @if($restaurant->published == 'Published')
                                                    @if($restaurant->delivery == 'Yes')
                                                        Take out or Delivery
                                                    @else
                                                        Takeout
                                                    @endif
                                                @else
                                                    Online Ordering Unavailable
                                                @endif
                                                </span> 
                                                <!-- <span class="ml-10">
                                                    @if($restaurant->delivery == 'Yes')
                                                    @if($restaurant->delivery_charge == 0)
                                                        Free Delivery
                                                    @elseif($restaurant->delivery_charge != '')
                                                        Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                                    @endif
                                                  @endif
                                                </span> -->
                                            </p>
                                        </span>
                                    </button>
                                    @if($restaurant->published != 'Published')
                                    <a href="#cart_items_side" class="btn btn-theme vote-to-add-btn">Vote to Add</a>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="single-header-infos">
                                <!-- <div class="location" data-toggle="tooltip" data-placement="top" title="{{Session::get('location')}}"><i class="ion ion-android-pin"></i>{{!empty(Session::get('location')) ? str_limit(Session::get('location'),18) : 'Select Location' }}</div> -->
                                <!-- <a href="#" onClick="history.go(-1)" class="ion ion-android-arrow-back back-btn" data-toggle="tooltip" data-placement="top" title="Back to Listing" data-original-title="Go Back"></a> -->
                                <button onclick="add_fav()" class="add-fav-btn btn" data-toggle="tooltip" data-placement="top" title="Add to Your Favorites" data-original-title="Add to Favorites"><i class="ion ion-ios-heart"></i></button>
                                
                                <div class="actions">
                                    <div class="user-infos">
                                        @if(Auth::user())
                                        <div class="user-wrap">
                                            <div class="dropdown">
                                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <img src="{{Auth::user()->picture}}" onerror="if (this.src != '{{url('themes/client/assets/img/avatar/male.png')}}') this.src = '{{url('themes/client/assets/img/avatar/male.png')}}';" >
                                                </button>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item short-info" href="#">
                                                        <img class="avatar" src="{{Auth::user()->picture}}" onerror="if (this.src != '{{url('themes/client/assets/img/avatar/male.png')}}') this.src = '{{url('themes/client/assets/img/avatar/male.png')}}';" >
                                                        <span class="user-info">
                                                            <h4 class="user-info-name">{{Auth::user()->name}}</h4>
                                                            <p class="user-info-email">{{Auth::user()->email}}</p>
                                                            <span class="user-info-preferences">Account &amp; Preferences</span>
                                                        </span>
                                                    </a>
                                                    <a class="dropdown-item" href="{{url('client/')}}">Orders</a>
                <!--                                     <a class="dropdown-item" href="#">Invoices</a>
                 -->                                    <div class="menu-footer">
                                                        <a href="{{url('client/password')}}">Reset Password</a>
                                                        <a href="{{url('client/logout')}}">Sign Out</a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @else
                                        <a class="btn btn-theme" href="#signIn_signUp_Modal" data-toggle="modal" data-target="#signIn_signUp_Modal"><i class="flaticon-user"></i><span>Sign In</span></a>
                                        @endif
                                    </div>
                                    <button class="cart-toggle-btn push-body menu-right" type="button" data-toggle="collapse" data-target="#cart">
                                        <div class="cart-inner">
                                            @if(Cart::count() != 0)
                                            <div class="icon cart-item-exist">
                                                <span class="count cart_count_header ">{{Cart::count()}}</span>
                                                <i class="ik ik-shopping-bag"></i>
                                            </div>
                                            @else
                                            <div class="icon">
                                                <span class="count cart_count_header">{{Cart::count()}}</span>
                                                <i class="ik ik-shopping-bag"></i>
                                            </div>
                                            @endif
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bottom-rastaurant-detail-wrap collapse" id="mobile-restaurant-detail-collapse">
                    <div class="container">
                        <div class="single-listing-info">
                            <div class="single-image" style="background-image: url({{url($restaurant->defaultImage('logo'))}})"></div>
                            <div class="single-content">
                                <div class="title-block">
                                    <div class="rating">
                                        <span class="raty" data-score="{{$restaurant->rating}}"></span><span class="count"><a href="#review" class="inner-scroll">{{$restaurant->rating}} ({{$restaurant->review->count()}} ratings)</a></span>
                                    </div>
                                    <button onclick="add_fav()" class="add-fav-btn btn" data-toggle="tooltip" data-placement="top" title="Add to Your Favorites" data-original-title="Add to Favorites"><i class="ion ion-ios-heart"></i></button>
                                </div>
                                
                                <div class="d-sm-flex">
                                    <div data-toggle="tooltip" data-placement="top" title="{{$restaurant->address}}" class="mr-15"> <p class="location"><i class="ion ion-android-pin mr-5"></i>{{$restaurant->address}}</p></div>
                                   
                                </div>
                                <p><i class="fa fa-phone mr-5"></i>{{substr($restaurant->phone, 0,3)}}-{{substr($restaurant->phone, 3,3)}}-{{substr($restaurant->phone, 6,4)}}</p>
                                <div class="meta-infos">
                                    <div class="item">
                                        <p><span>Today’s Hours</span>
                                            @if(isset($restaurant->timings[$weekday]))
                                            @foreach($restaurant->timings[$weekday] as $key => $val) 
                                                @if($val->opening != 'off' && $val->opening != 'closing') 
                                                    <span class="hours">{{date('g:i a',strtotime($val->opening))}} - {!!date('g:i a',strtotime($val->closing))!!}</span></p>
                                                @endif
                                            @endforeach
                                            @else
                                            -
                                            @endif
                                    </div>
                                    <div class="item">
                                        <p>
                                        <span>
                                            @if($restaurant->published == 'Published')
                                           @if($restaurant->delivery == 'Yes')
                                                @if($restaurant->delivery_charge == 0)
                                                    Free Delivery
                                                @elseif($restaurant->delivery_charge != '')
                                                   Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                                @endif
                                            @endif
                                            @endif
                                        </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <nav class="bottom-nav-mobile">
                @if(Cart::count() != 0)
                <div class="checkout-button-wrap">
                    <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme btn-block">Checkout Now</a>
                </div>
                @else
                <div class="checkout-button-wrap" id="mobile_chk_btn" style="display: none;">
                    <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme btn-block">Checkout Now</a>
                </div>
                <div class="nav-link-item logo">
                    <a href="{{url('/')}}">
                        <img src="{!!url('assets/img/logo-round-big.png')!!}" alt="logo" title="Zing My Order">
                    </a>
                </div>
                <div class="nav-link-item">
                    <a href="javascript:void(0);" id="location_change_mob">
                        <i class="ik ik-map-pin"></i>
                        <span>{{!empty(Session::get('location')) ? strtok(Session::get('location'),',') : 'Address' }}</span>
                    </a>
                </div>
                <div class="nav-link-item sign-in">
                     @if(!Auth::user())
                    <a  href="#signIn_signUp_Modal" data-toggle="modal" data-target="#signIn_signUp_Modal"><img src="{{theme_asset('img/user.svg')}}" alt=""></a>
                    @elseif(Auth::user())
                    <a href="{{url('client/')}}">
                        <img src="{{Auth::user()->picture}}" onerror="if (this.src != '{{url('themes/client/assets/img/avatar/male.png')}}') this.src = '{{url('themes/client/assets/img/avatar/male.png')}}';" >
                    </a>
                    @endif
                </div>
                @endif
            </nav>

            <div class="single-listing-wrap" id="singleRestWrap">
                <div class="single-menu-items-wrap listing-wrap p-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-lg-2 col-xl-2 menu-col-wrap">
                                <div class="menu-col-wrap-inner">
                                    <div class="man-menu-items-nav">
                                        <div class="menu-items-nav-inner">
                                            <ul class="nav-menu" id="manuNav">
                                                @foreach($restaurant->category as $category)
                                                <li class="menu-item"><a href="#{{$category->id}}" class="menu-link roll">{{$category->name}}</a></li>
                                                @endforeach
                                                @if(!empty($restaurant->catering_category) && count($restaurant->catering_category) > 0)
                                                <div class="divider"></div>
                                                <a href="#" class="menu-link roll" id="cater_heading" style="color: #333; padding-right: 20px; font-size: 15px; font-weight: 700;">Catering </a>
                                                @foreach($restaurant->catering_category as $key => $category)
                                                <li class="menu-item {{(Session::get('category_name')== 'Catering' && $key==0) ? 'active' : ''}}"><a href="#cater_{{$category->id}}" class="menu-link roll">{{$category->name}}</a></li>
                                                @endforeach
                                                @endif
                                                <div class="divider"></div>
                                                <li class="menu-item"><a href="#about" class="menu-link roll text-success">About Us</a></li>
                                                <li class="menu-item"><a href="#gallery" class="menu-link roll text-success">Gallery</a></li>
                                                <li class="menu-item"><a href="#timing" class="menu-link roll text-success">Hours</a></li>
                                                <li class="menu-item"><a href="#offer" class="menu-link roll text-success">Offers</a></li>
                                                <li class="menu-item"><a href="#review" class="menu-link roll text-success">Reviews</a></li>
                                            </ul>
                                           <!--  <div class="offer-block mt-20">
                                                <img src="{{url($restaurant->defaultImage('offer','original'))}}" class="img-fluid" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-5 col-xl-7">
                                <div class="responsive-search-menu-wrap">
                                    <div class="cat-title-btn-wrap">
                                        <button type="button" data-toggle="collapse" data-target="#resposive_menu_cat" aria-expanded="false" aria-controls="resposive_menu_cat" class="btn cat-title-btn">
                                            <span id="res_cat_title">Popular Dishes</span> <i class="ik ik-more-vertical" style="float: right; margin-top: 5px;"></i>
                                        </button>
                                        <div class="buttons-block">
                                            <button type="button" data-toggle="collapse" data-target="#resposive_search" aria-expanded="false" aria-controls="resposive_search" class="btn ik ik-search mobile-search-btn"></button>
                                        </div>
                                    </div>
                                    <div class="collapse" id="resposive_search">
                                        <div class="mobile-dish-search">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                            <input type="text" class="form-control"  id="dish_quick_search_mob" placeholder="Search Food...">
                                            <button type="button" id="dish_quick_clear" class="clear-btn fa fa-times-circle"></button>
                                        </div>
                                    </div>
                                    <div class="collapse" id="resposive_menu_cat">
                                        <div class="mobile-dish-menu">
                                            <h3>Menu</h3>
                                            <ul class="nav-menu" id="manuNav">
                                                 @foreach($restaurant->category as $category)
                                                <li class="menu-item active"><a href="#{{$category->id}}" class="menu-link res-menu-scroll">{{$category->name}}</a></li>
                                                @endforeach
                                                @if(!empty($restaurant->catering_category) && count($restaurant->catering_category) > 0)
                                                <div class="divider"></div>
                                                <a href="#" class="menu-link res-menu-scroll" id="cater_heading" style="color: #333; font-size: 15px; font-weight: 700;">Catering </a> 
                                                @foreach($restaurant->catering_category as $key => $category)
                                                <li class="menu-item {{(Session::get('category_name')== 'Catering' && $key==0) ? 'active' : ''}}"><a href="#cater_{{$category->id}}" class="menu-link res-menu-scroll">{{$category->name}}</a></li>
                                                @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="menu-items-wrap-inner listing-wrap">
                                    @if(!empty($restaurant->offers))
                                    <div class="menu-items-section content-section" id="selected">
                                        <h3 class="title">Offers</h3>
                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <a class="dish-item offer-item" style="cursor: auto;">
                                                        <div class="dish-description">
                                                            <!-- <h3 class="dish-title">{{$restaurant->offers->name,10}}</h3> -->
                                                            @if($restaurant->offers->type == 'Price')
                                                            <h3 class="dish-title">${{number_format($restaurant->offers->value,0)}} Off on all Orders</h3>
                                                            @else
                                                            <h3 class="dish-title">{{number_format($restaurant->offers->value,0)}}% Off on all Orders</h3>
                                                            @endif
                                                            <p>Apply at Check out</p>
                                                            <div class="dish-metas">
                                                                <span>Valid: {{date('d-m-Y', strtotime($restaurant->offers->start_date))}}</span>
                                                                <span>to {{date('d-m-Y', strtotime($restaurant->offers->end_date))}}</span>
                                                                <!-- @if($restaurant->offers->type == 'Price')
                                                                <span>$ {{$restaurant->offers->value}}</span>
                                                                @else
                                                                <span>{{$restaurant->offers->value}} %</span>
                                                                @endif -->
                                                            </div>
                                                         </div>
                                                         <div class="dish-image">
                                                            <!-- @if(!empty($restaurant->offers->image)) 
                                                            <img data-src="{{url($restaurant->offers->defaultImage('image','md'))}}" class="img-fluid lazy" alt="">
                                                            @else
                                                            <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                            @endif -->
                                                            @if($restaurant->offers->type == 'Price')
                                                            <h3>${{number_format($restaurant->offers->value,0)}}<span>Off</span></h3>
                                                            @else
                                                            <h3>{{number_format($restaurant->offers->value,0)}}%<span>Off</span></h3>
                                                            @endif
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @if (Session::has('carterror'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <span><b> Error - </b> {{ Session::get('carterror') }}</span>
                                    </div>
                                    @endif 
                                    @if(!empty(Session::get('selected_masters[]')) && count(Restaurant::getSessionMenus(Session::get('selected_masters[]'),$restaurant->id))>0)
                                    <div class="menu-items-section content-section" id="selected">
                                        <h3 class="title">Selected Dishes</h3>
                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                @foreach(Restaurant::getSessionMenus(Session::get('selected_masters[]'),$restaurant->id) as $selected_menu)
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <div class="dish-item" data-slug="{{$selected_menu->id}}"  >
                                                        <div class="dish-description">
                                                            <h3 class="dish-title"><a href="#!" class="someclass">{{$selected_menu->name,10}}</a></h3>

                                                            <p>{{str_limit($selected_menu->description, 30)}}</p>
                                                            <div class="dish-metas">
                                                                <span><i class="fa fa-star" style="color: #f6af00;"></i>{{$selected_menu->review}}</span>
                                                                <span class="ml-15"><i class="ion-android-restaurant"></i>@if(!empty($selected_menu->serves)) {{$selected_menu->serves}} @else 1 @endif</span>
                                                            </div>
                                                            @if($restaurant->published == 'Published')
                                                                <a class="dish-add-btn" href="#{{$selected_menu->slug}}" data-toggle="modal" data-target="#{{$selected_menu->slug}}" data-slug="{{$selected_menu->id}}">Add</a>
                                                            @endif
                                                        </div>

                                                        <div class="dish-image">
                                                            @if(!empty($selected_menu->image)) 
                                                            <img data-src="{{url($selected_menu->defaultImage('image','xs'))}}" class="img-fluid lazy" alt="">
                                                            @else
                                                            @if(!empty(@$selected_menu->master))
                                                            <img data-src="{{url(@$selected_menu->master->defaultImage('image','xs'))}}" class="img-fluid lazy" alt="">
                                                            @else
                                                            <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                            @endif
                                                            @endif
                                                        </div>
                                                        <div class="dish-price">${{$selected_menu->price}}</div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endif 
                                  
                                    @if(count($restaurant->popular_category) > 0)
                                    <div class="menu-items-section content-section" id="popular">
                                         
                                        <h3 class="title" data-title="Popular Dishes">Popular Dishes</h3>
                                        
                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                @foreach($restaurant->popular_category as $menu)
                                                @if($menu->popular_dish_no != 0)
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <div class="dish-item" data-slug="{{$menu->id}}" >
                                                    <!-- <a class="dish-item add_to_cart"> -->
                                                        <div class="dish-description">
                                                            <h3 class="dish-title"><a href="#!" class="someclass">{{$menu->name}}</a></h3>
                                                            <p>{{str_limit($menu->description, 30)}}</p>
                                                            <div class="dish-metas">
                                                                <span><i class="fa fa-star" style="color: #f6af00;"></i>{{$menu->review}}</span>
                                                                <span class="ml-15"><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
                                                            </div>
                                                            @if($restaurant->published == 'Published')
                                                                <a class="dish-add-btn" href="#{{$menu->slug}}" data-toggle="modal" data-target="#{{$menu->slug}}" data-slug="{{$menu->id}}"><i class="ik ik-shopping-bag"></i>Add</a>
                                                            @endif
                                                        </div>

                                                        <div class="dish-image">
                                                            @if(!empty($menu->image))
                                                            <img data-src="{{url($menu->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                            @else
                                                            @if(!empty(@$menu->master))
                                                            <img data-src="{{url(@$menu->master->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                            @else
                                                            <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                            @endif
                                                            @endif
                                                        </div>
                                                        <div class="dish-price">${{$menu->price}}</div>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @foreach($restaurant->category as $category)
                                    <div class="menu-items-section content-section" id="{{$category->id}}">
                                         
                                        <h3 class="title" data-title="{{$category->name}}">{{$category->name}}</h3>
                                        
                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                @if(isset($restaurant->menu[$category->id]))
                                                @foreach($restaurant->menu[$category->id] as $menu)
                                                @if($menu->catering != 'Yes')
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <div class="dish-item" data-slug="{{$menu->id}}" >
                                                    <!-- <a class="dish-item add_to_cart"> -->
                                                        <div class="dish-description">
                                                            <h3 class="dish-title"><a href="#!" class="someclass">{{$menu->name}}</a></h3>
                                                            <p>{{str_limit($menu->description, 30)}}</p>
                                                            <div class="dish-metas">
                                                                <span><i class="fa fa-star" style="color: #f6af00;"></i>{{$menu->review}}</span>
                                                                <span class="ml-15"><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
                                                            </div>
                                                            @if($restaurant->published == 'Published')
                                                                <a class="dish-add-btn" href="#{{$menu->slug}}" data-toggle="modal" data-target="#{{$menu->slug}}" data-slug="{{$menu->id}}"><i class="ik ik-shopping-bag"></i>Add</a>
                                                            @endif
                                                        </div>

                                                        <div class="dish-image">
                                                            @if(!empty($menu->image))
                                                            <img data-src="{{url($menu->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                            @else
                                                            @if(!empty(@$menu->master))
                                                            <img data-src="{{url(@$menu->master->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                            @else
                                                            <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                            @endif
                                                            @endif
                                                        </div>
                                                        <div class="dish-price">${{$menu->price}}</div>
                                                    </div>
                                                    
                                                </div>
                                                @endif
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @foreach($restaurant->catering_category as $category)
                                    <div class="menu-items-section content-section" id="cater_{{$category->id}}">
                                      
                                        <h3 class="title">{{$category->name}}</h3>
                                      
                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                @foreach($restaurant->menu[$category->id] as $menu)
                                                @if($menu->catering == 'Yes')
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <div class="dish-item"  data-slug="{{$menu->id}}" >
                                                        <div class="dish-description">
                                                            <h3 class="dish-title"><a href="#!" class="someclass">{{$menu->name,10}}</a></h3>
                                                            <p>{{str_limit($menu->description, 30)}}</p>
                                                            <div class="dish-metas">
                                                                <span><i class="fa fa-star" style="color: #f6af00;"></i>{{$menu->review}}</span>
                                                                <span class="ml-15"><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
                                                               
                                                                <!--<span class="ml-15"><a href="#{{$menu->slug}}" onclick="addonModel('{{$menu->id}}')" data-toggle="modal" data-target="#{{$menu->slug}}"><i class="ik ik-shopping-bag"></i></a></span>-->
                                                            </div>
                                                             @if($restaurant->published == 'Published')
                                                                    <a class="dish-add-btn" href="#{{$menu->slug}}"  data-toggle="modal" data-target="#{{$menu->slug}}" data-slug="{{$menu->id}}"><i class="ik ik-shopping-bag"></i>Add</a>
                                                             @endif
                                                        </div>

                                                        <div class="dish-image">
                                                            @if(!empty($menu->image))
                                                            <img data-src="{{url($menu->defaultImage('image','xs'))}}" class="img-fluid lazy" alt="">
                                                            @else
                                                            @if(!empty(@$menu->master))
                                                            <img data-src="{{url(@$menu->master->defaultImage('image','xs'))}}" class="img-fluid lazy" alt="">
                                                            @else
                                                            <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                            @endif
                                                            @endif
                                                        </div>
                                                        <div class="dish-price">${{$menu->price}}</div> 
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="about-wrap content-section" id="about">
                                        <div class="about-inner-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="title">About {{$restaurant->name}}</h3>
                                                    <p>{{$restaurant->description}}</p>
                                                    <div class="info-wrap">
                                                         <div id="map_canvas" style="height: 300px;width: 100%; margin-bottom: 30px"></div>
                                                        <p><b>{{$restaurant->address}}</b> </p>
                                                        @if(!empty($restaurant->phone))<p class="phone-no">Phone : {{substr($restaurant->phone, 0,3)}}-{{substr($restaurant->phone, 3,3)}}-{{substr($restaurant->phone, 6,4)}} </p>@endif
                                                    </div>
                                                </div> 
                                            </div>

                                        </div>
                                    </div>
<!--
                                    <div class="about-wrap content-section" id="gallery">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3 class="title">Gallery! </h3>
                                                @if(!empty($restaurant->images) || !empty($restaurant->photos))
                                                <div class="popup-gallery" id="lightgallery">
                                                    @if(!empty($restaurant->images))
                                                    @foreach($restaurant->getImages('gallery','lg') as $key=> $image)
                                                    <a href="{!!url($restaurant->defaultImage('gallery' ,'lg',$key))!!}">
                                                        <span class="gallery-img" style="background-image: url('{!!url($restaurant->defaultImage('gallery' ,'xs',$key))!!}')"></span></a>
                                                    @endforeach
                                                    @endif
                                                    @if(!empty($restaurant->photos))
                                                    @foreach($restaurant->photos as  $photo)
                                                    <a href="{!!$photo!!}">
                                                        <span class="gallery-img" style="background-image: url('{!!$photo!!}')"></span></a>
                                                    @endforeach
                                                    @endif
                                                </div>
                                                @else
                                                    There are no Pictures at this time
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                    <div class="about-wrap content-section" id="timing">
                                        <div class="about-inner-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="title">Hours</h3>
                                                    <img src="img/home-salad-banner1.png" class="img-fluid" alt="">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            @foreach($weekMap as $wkey => $wval)
                                                            <div class="row no-gutters">
                                                                <div class="col-md-4"><p>{{$wval}}</p></div>
                                                                <div class="col-md-8">
                                                                    @if(isset($restaurant->timings[$wkey]))
                                                                    @foreach($restaurant->timings[$wkey] as $val)
                                                                            <p class="m-0">
                                                                                @if($val->opening== 'off' || $val->closing == 'off')
                                                                                    Off 
                                                                                @else
                                                                                    {{date('h:i a',strtotime($val->opening))}} - {!!date('h:i a',strtotime($val->closing))!!}
                                                                                @endif
                                                                            </p>
                                                                    @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="about-wrap content-section" id="offer">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3 class="title">Offers </h3>
                                                @if(!empty($restaurant->offer))
                                                <div class="popup-gallery" id="lightgallery">
                                                    @foreach($restaurant->getImages('offer','lg') as $key=> $image)
                                                    <a href="{!!url($restaurant->defaultImage('offer' ,'lg',$key))!!}">
                                                        <span class="gallery-img" style="background-image: url('{!!url($restaurant->defaultImage('offer' ,'xs',$key))!!}')"></span></a>
                                                    @endforeach
                                                </div>
                                                @else
                                                    There are no offers at this time
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="review-wrap content-section" id="review">
                                        <h3 class="title">Reviews for {{$restaurant->name}}</h3>
                                        <div class="widget-wraper">
                                            <div class="review-header">
                                                <h2>{{$restaurant->rating}}<span>/5</span></h2>
                                                <span class="raty" data-score="{{$restaurant->rating}}"></span>
                                                <p>{{$restaurant->review->count()}} Reviews</p>
                                            </div>
                                            @forelse($restaurant->review as $review)
                                            <div class="media">
                                                @if(@$review->yelp_id == '')
                                                <div class="rating-avatar">{{substr(@$review->customer->name, 0,1)}}</div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{{@$review->customer->name}} 
                                                        @if($review->order_id != null)
                                                        <i class="ion ion-checkmark-circled verified"></i>
                                                        @endif
                                                        <span>{{date('M d,Y',strtotime($review->created_at))}} at {{date(' h:m a',strtotime($review->created_at))}}</span></h4>
                                                    <span class="raty" data-score="{{$review->rating}}"></span>
                                                    <p>{{$review->review}}.</p>
                                                </div>
                                                @else
                                                <div class="rating-avatar" style="background-image: url('{{@$review->json_user['image_url']}}'); background-size: contain; background-position: center;"></div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{{@$review->json_user['name']}} 
                                                   <!--  <i class="ion ion-checkmark-circled verified"></i> --> 
                                                        <span>{{date('M d,Y',strtotime($review->created_at))}} at {{date(' h:m a',strtotime($review->created_at))}}</span></h4>
                                                    <span class="raty" data-score="{{$review->rating}}"></span>
                                                    <p>{{$review->review}}.</p>
                                                </div>
                                                @endif
                                            </div>

                                            @empty
                                                No Reviews Added.
                                            @endif
                                            @if(Auth::User())
                                            <div class="post-form-area">
                                                <h4>Post Your Review</h4>
                                                {!!Form::vertical_open()
                                                -> class('form-comment')
                                                -> method('POST') 
                                                -> action('review/post/'.$restaurant->id)!!}
                                                
                                                <div class="form-group">
                                                    {!!Form::textarea('comment')
                                                    ->placeholder('Your comment')
                                                    ->required()
                                                    ->forceValue('')
                                                    ->rows(4)
                                                    ->raw()!!}
                                                </div>
                                                <div class="form-group">
                                                    <label for="rate" class="mr-5">Your Rating </label>  
                                                    <span id="rate" class="rating" data-score="4"></span>
                                                </div>
                                                <input type="hidden" name="restaurant_id" value="{{$restaurant['id']}}">
                                                <button type="submit" id="btn_add_review" class="btn btn-theme" onclick="add_review()">Post Review</button>
                                                {!!Form::close()!!}  
                                            </div>
                                            @else
                                            <div class="alert alert-info mb-5"><span style="color: #444;">Please  <a href="{{url('review/add')}}/{{$restaurant->slug}}">Sign in</a>  to Post Reviews and Ratings</span></div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                         
                            <div class="col-md-4 col-lg-4 col-xl-3 eatery-cart-wrap">
                                <div class="sticky-spacer" id="cart_items_side">
                                    @if($restaurant->published == 'Published')
                                        @if(Cart::count() == 0)
                                        <div class="cart-inner-wrap empty-cart">
                                            <img src="{{theme_asset('img/empty-cart.png')}}" alt="">
                                            <h2>Your Cart is Empty</h2>
                                            <p>Looks like you haven't made your choice yet</p>
                                        </div>
                                        @else
                                        <div class="cart-inner-wrap sticky-div ">
                                            <h2>Cart <span>{{Cart::count()}} Item</span></h2>
                                            <p>( @if($restaurant->delivery == 'Yes')
                                                    @if($restaurant->delivery_charge == 0)
                                                        Take out or Delivery
                                                    @elseif($restaurant->delivery_charge != '')
                                                       Take out or Delivery
                                                    @endif
                                                @else
                                                    Take out
                                                @endif) <a href="#" class="text-success" data-toggle="tooltip" data-placement="top" title="Earn Zing Points with Each Order. Use your points towards a future order to save money.">Earn Points <i class="fa fa-exclamation-circle text-dark"></i></a></p>
                                            <hr>
                                            <div class="cart-item-wrap">
                                                @foreach(Cart::content() as $cart)
                                                <?php $variation = $cart->options->variation;?>
                                                <div class="cart-item" style="{{$cart->options['menu_addon'] == 'addon' ? 'margin-left: 20px' : ''}}">
                                                    <div class="item-name-price">
                                                        <h4>{{str_limit($cart->name,17)}}</h4>
                                                         @php
                                                                $addon_price = 0;
                                                                    if(!empty($cart->options['addons'])){
                                                                foreach($cart->options['addons'] as $cart_addon){
                                            
                                                                        $addon_price = $addon_price + $cart_addon[2];
                                                                }
                                                                               
                                                               }
                                                            @endphp
                                                        <div class="price">${{number_format($cart->price+$addon_price,2)}}</div>
                                                    </div>
                                                    @if(!empty($variation))<p>(Variation-{{$variation}})</p>@endif
                                                    @if(!empty($cart->options['addons']))
                                                        @foreach($cart->options['addons'] as $cart_addon)
                                                         <div class="item-addon"><span data-toggle="tooltip" data-placement="top" title="{{$cart_addon[1]}}">{{str_limit($cart_addon[1],17)}}</span></div>
                                                        @endforeach
                                                    @endif
                                                     @if($cart->options['menu_addon'] != 'addon')
                                                    <div class="item-qty-price">
                                                        <div class="qty-price-inner">
                                                            <span class="enumerator">
                                                                <span class="enumerator_btn js-minus_btn" id="minus_span" onclick="decrement('{{$cart->rowId}}')">-</span>
                                                                <span class="enumerator_input">{{$cart->qty}}</span>
                                                                <input type="hidden" value="{{$cart->qty}}" readonly="readonly">
                                                                <span class="enumerator_btn js-plus_btn" id="plus_span" onclick="increment('{{$cart->rowId}}')">+</span>
                                                            </span>
                                                             
                                                            <span class="remove-btn"><a href="#" class="remove-btn" onclick="removeCartItem('{{$cart->rowId}}')"><i class="ion-android-cancel"></i></a></span>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                                @endforeach
                                            </div>
                                            <div class="cart-summary-wrap mt-20">
                                                <div class="summary-item">
                                                    <div class="summary-text">Subtotal</div>
                                                    <div class="summary-price">${{number_format(Cart::total(),2)}}</div>
                                                </div>
                                                <!-- <div class="summary-extra">Extra charges may apply</div> -->
                                                <a class="btn btn-theme checkout-btn" href="{{url('cart/checkout')}}">Checkout Now</a>
                                                <!-- <a class="btn btn-theme checkout-btn" href="{{url('cart/checkout/guest')}}" id="guest_form">Checkout As Guest</a> -->
                                                
                                            </div>
                                        </div>
                                        @endif
                                    @else
                                        <div class="cart-inner-wrap vote-wrap">
                                            <h2>Take Out or Delivery Unavailable</h2>
                                            <p>Click on Vote to add & we'll convey your request for the restaurant to join Zing's *Free Platform to take orders online.</p>
                                            
                                            <div id="feedback">
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar" @if($restaurant->votes != null) aria-valuenow="{{count($restaurant->votes) + 3}}" style="width:{{(count($restaurant->votes) + 3)*10}}%" @else  aria-valuenow="3" style="width:30%" @endif aria-valuemin="0" aria-valuemax="30" >
                                                        <span class="vote-count">@if($restaurant->votes != null) {{count($restaurant->votes) + 3}} @else 3 @endif</span>
                                                    </div>
                                                    
                                                </div>
                                                <button type="button" id="vote_to_add" class="btn btn-theme">Vote to Add</button>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- <div class="single-tab-wrap">
                    <div class="tab-nav-wrap">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="true">About Restaurant</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="manu-tab" data-toggle="tab" href="#manu" role="tab" aria-controls="manu" aria-selected="false">Menu</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content-wrap">
                        <div class="container">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="about-tab">...</div>
                                <div class="tab-pane fade" id="manu" role="tabpanel" aria-labelledby="manu-tab">...</div>
                            </div>
                        </div>
                    </div>    
                </div> -->
            </div>
<div class="modal fade menu-item-modal" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalTitle" aria-hidden="true">
     {!!Form::vertical_open()
    ->id('menu-cart-add')
    ->addClass('modal-dialog modal-dialog-scrollable modal-dialog-centered')
    ->action('/carts/save/')
    ->method('GET')!!}
        <div class="modal-content"></div>
    {!!Form::close()!!}
</div>

<div class="modal video-modal fade bd-example-modal-lg" id="youtubemodel" tabindex="-1" role="dialog" aria-labelledby="youtubemodelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content"></div>
    </div>
</div>

<div class="modal fade login-modal location-modal" id="favouriteModel" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body">
                <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                                <div class="login-header text-center">
                                  <h5><b>Please Sign in to proceed</b></h5>
                                </div>
                                
                                <div class="text-center" >
                                   <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" id="favourite_submit">Ok</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
<div class="modal fade login-modal" id="signIn_signUp_Modal" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                            @include('notifications')
                            {!!Form::vertical_open()
->id('login')
->method('POST')
->action('client/login')!!}
                                <div class="login-header text-center">
                                    <h2>Sign in</h2>
                                    <p>New to Zing My Order? <a href="{{url('client/register')}}">Sign Up</a></p>
                                </div>
                                <div class="social-btn-wrap">
                                    <a href='/client/login/facebook' class="btn btn-block btn-facebook" ><i class="fa fa-facebook"></i> Sign in with Facebook</a>
                                                <a href='/client/login/google' class="btn btn-block btn-google" ><i class="fa fa-google"></i> Sign in with Google</a>
                                    <h3>OR<br><span class="login">Log in Using</span></h3>
                                </div>
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <i class="flaticon-user"></i>
                                    {!! Form::email('email')
    ->required()
    ->placeholder('Enter Your Email')
    ->raw() !!}
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <a href="#" class="forgot_pw">Forgot your password?</a>
                                    <i class="flaticon-key"></i>
                                    {!! Form::password('password')
                                   ->placeholder('Enter Your Password')
    ->required()->raw()!!}
                                </div>
                                <div class="text-center login-footer">
                                    <button type="submit" class="btn btn-theme">Sign In</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="error_model" tabindex="-1" role="dialog" aria-labelledby="error_modelLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body text-center" style="background-color: #c2c8ce;">
                <h5 id="msg" class="mb-20 text-white"></h5>
                <button type="button" class="btn btn-theme" style="width: 100px;" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade added-to-cart-modal" id="cartMessageModel" tabindex="-1" role="dialog" aria-labelledby="cartMessageModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body">
                <a href="{{url('/')}}" class="close cart_close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></a>
                <div class="inner-content">
                    <!-- <i class="ion ion-ios-checkmark"></i> -->
                    <h4 class="mt-20">Your dish has been added to the cart</h4>
                    <p>Continue to add more or proceed to checkout</p>
                    <button  data-dismiss="modal" class="btn btn-secondary cart_close mr-15">Continue</button>
                    <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme search-btn">Checkout</a>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
<script type="text/javascript">
        var active = '<?php echo $restaurant_fav; ?>';
            var baseUrl ="{{url('/') }}";
            var transUrl ="{{trans_url('/') }}";
            var vote_restaurant_id = "{!!$restaurant->id!!}";
            var vote_restaurant_slug = "{!!$restaurant->slug!!}";
                $(function(){ 
          
     var map,myLatlng;
      @if(!empty($restaurant->latitude) && !empty($restaurant->longitude))
         myLatlng = new google.maps.LatLng({!! @$restaurant->latitude !!},{!! @$restaurant->longitude !!});
      @else
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
      @endif
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
         disableDefaultUI: true
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
    });
       })

</script>
<!-- Hotjar Tracking Code for https://zingmyorder.com/ -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1629504,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<style type="text/css">
    .well {
    background-color: #f6f9fc;
    -webkit-box-shadow: none;
    box-shadow: none;
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
}
.main-wrap .main-header .header-search-short-info-wrap.visible {
    display: none;
}
.main-wrap .main-header .header-search-short-info-wrap::before {
    display: none;
}
.footer-social a {
    width: 30px;
    height: 30px;
    line-height: 28px;
    text-align: center;
    color: #fff;
    background-color: #444;
    display: inline-block;
    border-radius: 50%;
    margin-right: 5px;
}
.checkout-button-wrap {
    padding: 10px;
    background-color: #fff;
    width: 100%;
}
</style>
         {!!'<!-- ' . date("F Y h:i:s u") . '-->'!!}
