<!-- <section class="cusine-slider-wrap map-cusine-slider-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="cusine-slider-wrap-inner">
                    <div class="cusine-slider swiper-container">
                        <div class="swiper-wrapper">
                            @foreach(Master::getDetailCuisines() as $cuisine)
                            <div class="swiper-slide cuisine-item">
                                <input type="checkbox" name="cuisine_types[]" id="{{$cuisine->id}}" class="search_cuisine" value="{{$cuisine->id}}" data-name="{{$cuisine->name}}">
                                <label for="{{$cuisine->id}}">
                                    <span class="cuisene-img" style="background-image: url({{url($cuisine->defaultImage('image','sm'))}})"></span>
                                    <h4>{{$cuisine->name}}</h4>
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="cusine-slider-prev ion-android-arrow-back"></div>
                    <div class="cusine-slider-next ion-android-arrow-forward"></div>
                </div>
                <div class="cusine-slider-mob-wrap-inner">
                    <div class="cusine-slider-mob">
                        <div class="cuisine-item">
                            <input type="checkbox" name="cuisine_types[]" id="cu_37" class="search_cuisine_mob" value="37" data-name="American">
                            <label for="cu_37">
                                <span class="cuisene-img" style="background-image: url('https://zingmyorder.com/image/sm/master/cuisine/2019/08/23/234103341/image/american.png')"></span>
                                <h4>American</h4>
                            </label>
                        </div>
                        <div class="cuisine-item">
                            <input type="checkbox" name="cuisine_types[]" id="cu_26" class="search_cuisine_mob" value="26" data-name="Barbeque">
                            <label for="cu_26">
                                <span class="cuisene-img" style="background-image: url('https://zingmyorder.com/image/sm/master/cuisine/2019/08/17/122205994/image/barbeque.png')"></span>
                                <h4>Barbeque</h4>
                            </label>
                        </div>
                        <div class="cuisine-item">
                            <input type="checkbox" name="cuisine_types[]" id="cu_28" class="search_cuisine_mob" value="28" data-name="Brazilian">
                            <label for="cu_28">
                                <span class="cuisene-img" style="background-image: url('https://zingmyorder.com/image/sm/master/cuisine/2019/08/17/151747564/image/brazilian.png')"></span>
                                <h4>Brazilian</h4>
                            </label>
                        </div>
                    </div>
                    <button type="button" class="cuisine-popup-mob-btn ion ion-android-more-vertical" data-toggle="modal" data-target="#cuisine_modal"></button>
                </div>
            </div>
        </div>
    </div>
</section>
 -->
<section class="location-city-wrap">
    <div class="search-map-main-wrap mt-0">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="sec-title text-center">
                        <h2 class="title">Eateries in {{$selected_location->name}}</h2>
                        <p>{{$restaurants->total()}} Results found within 10 miles of the entered address.</p>
                    </div>
                </div>
            </div>
            <div class="search-filter-result-wrapper w-100 float-none ml-0 d-block">
                <div class="search-result-wrapper restaurant-list-wraper">
                    <div class="row">
                        @foreach($restaurants as $key => $restaurant)
                        <?php $flag = 0;
                        $flag                                       = Restaurant::getOpenStatus($restaurant->id);?>
                        <div class="col-md-6">
                            <div class="listing-item {{$flag!=1 ? 'closed' : ''}}">
                                <div class="left-image-block">
                                    <div class="img-holder">
                                        <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}"><figure style="background-image: url('{{@$restaurant->mainlogo}}')"></figure></a>
                                    </div>
                                    <div class="action-holder">
                                        <div class="status">
                                            @if($flag == 1)
                                            <span class="open"><i class="ion-android-time"></i>Open</span>
                                            @else
                                            <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                            @endif
                                        </div>
                                       <!--  <a href="#" class="btn btn-theme zing-order-btn">Order Now</a> -->
                                        <a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}" class="btn view-menu-btn">See Menu</a>
                                    </div>
                                </div>
                                <div class="right-content-block">
                                    <a href="#" class="add-fav-btn ion-ios-heart"></a>
                                    <h3><a href="{{trans_url('restaurants/')}}/{{@$restaurant['slug']}}">{{$restaurant->name}}</a></h3>
                                    @if($restaurant->published == 'Published')
                                    <span class="zing-partner"><i></i>Partner</span>
                                    @endif
                                    <div class="rating-pricerang-wrap">
                                        <span class="rating"><i class="fa fa-star"></i>{{$restaurant->rating}}</span>
                                        <span class="price-range">${{number_format(@$restaurant->price_range_min,0)}}-{{number_format(@$restaurant->price_range_max,0)}}</span>
                                    </div>
                                    <p class="type">{{$restaurant->type}}</p>
                                    <div class="location">{{number_format($restaurant->distance,2)}} mi <i class="ion ion-android-pin"></i> {{$restaurant->address}}</div>
                                    <div class="right-metas">
                                        <div class="status">
                                            @if($flag == 1)
                                            <span class="open"><i class="ion-android-time"></i>Open</span>
                                            @else
                                            <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                            @endif
                                        </div>
                                        <div class="meta-infos">
                                            <div class="info">
                                                @if($restaurant->delivery =='Yes')
                                                <i class="flaticon-fast-delivery"></i>
                                                @if($restaurant->delivery_charge == 0)
                                                Free Delivery
                                                @elseif($restaurant->delivery_charge != '')
                                                Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                                @endif {{!empty($restaurant->delivery_limit) ? '(within '.$restaurant->delivery_limit.' mi)' : ''}}
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                        
                                    <div class="dish-items">
                                        <?php $res_menu = Restaurant::getMenuNameSEO($restaurant->id,$masters);?>
                                            @forelse($res_menu as $res_menu_det)
                                            <div class="dish-item-block-mob">
                                                <div class="item-cat-name">{{$res_menu_det->name}}</div>
                                                <div class="item-name-price-rat">
                                                    <p>
                                                        {{@$res_menu_det->categories->name}} 
                                                        @if($restaurant->published == 'Published')
                                                        <a href="#{{$res_menu_det->slug}}" class="add-cart-btn" onclick="addonModel('{{$res_menu_det->id}}')" data-toggle="modal" data-target="#{{$res_menu_det->slug}}"><i class="ik ik-shopping-bag"></i> Add</a>
                                                        @endif
                                                    </p>
                                                    <div class="item-rate-price">
                                                        <span class="rating"><i class="fa fa-star"></i>{{$res_menu_det->review}}</span>
                                                        <span>${{$res_menu_det->price}}</span>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            @empty
                                             <div class="item"><div class="item-info name">
                                            Menu Unavailable</div>
                                        </div>
                                            @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                         @endforeach
                     </div>  
                     <div class="pagination-wrap d-flex justify-content-center">
                         {{$restaurants->links()}}
                     </div>
                </div>
            </div>
        </div>
    </div>
    
</section>
<script type="text/javascript">
                        	
var locations=[@foreach($map_restaurants as $key=>$value) [{!!$value['latitude']!!},{!!$value['longitude']!!},'{{url($value->defaultImage('logo','original'))}}',"{!!addslashes($value['name'])!!}",'{!!addslashes($value['address'])!!}','Preorder','rest-icon',{!!$key+1!!},"{{trans_url('restaurants/')}}/{!!addslashes($value['slug'])!!}"],@endforeach
    ];
    console.log('hi',locations);
    $('.search_cuisine').click(function(e){
    var favorite = [];
            $.each($("input[name='cuisine_types[]']:checked"), function(){            
                favorite.push($(this).attr('data-name'));
            });
                                window.location.href = "{{url('location')}}/{{$selected_location->slug}}"+'_'+favorite;

  
  })
</script>
<script>eval(info_bubble);</script>
<script>eval(map_data);</script>
<script>eval(map_init);</script>