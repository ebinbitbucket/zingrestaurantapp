
                                           @forelse($restaurant->review->groupBy('customer_id') as $key => $value)
                                            @if(is_numeric($key))
                                               <?php $reviews = $value->take(1);?>
                                            @else
                                              <?php $reviews = $value;?>
                                            @endif

                                             @foreach($reviews as $review)
                                            <div class="media">



                                                @if(@$review->yelp_id == '')
                                                @if(@$review->rating >= 1)
                                                <div class="avtar-block">
                                                    <div class="rating-avatar">{{substr(@$review->customer->name, 0,1)}}</div>

                                                @if($review->order_id != null)

                                                        <i class="zing-icon verified"></i>

                                                        @endif
                                                    </div>
                                                <div class="media-body">
                                                    <h4 class="media-heading">{{@$review->customer->name}} @if($review->order_id != null) <i class="verified ion-checkmark-circled"></i><label style="font-weight: 400; font-size: 13px ">&nbsp; Verified Customer</label> @endif
                                                        <span>{{date('M d,Y',strtotime($review->created_at))}}
                                                        <!--at {{date(' h:m a',strtotime($review->created_at))}}-->
                                                        </span></h4>
                                                    <span class="raty" data-score="{{$review->rating}}"></span>
                                                    <p>{{$review->review}}</p>
                                                </div>

                                                @if($review->order_id != null)
                                                    @forelse(Restaurant::foodReview(@$review->restaurant_id, @$review->customer_id) as $foodreview)
                                                    @if(@$foodreview->rating >= 1)
                                                    <div class="media-body">
                                                        <h4>{{@$foodreview->menu->name}}<span class="raty float-none" data-score="{{$foodreview->rating}}"></span></h4>
                                                        <h4 class="media-heading">
                                                        <span>{{date('M d,Y',strtotime($foodreview->created_at))}}
                                                        <!--at {{date(' h:m a',strtotime($foodreview->created_at))}}-->
                                                        </span></h4>

                                                        <p>{{@$foodreview->review}}</p>
                                                    </div>
                                                    @endif
                                                    @empty
                                                    @endif
                                                @endif
                                                @endif
                                                @else
                                                <div class="avtar-block">

                                                @if(@$review->yelp_id == 'Google')
                                                <a href="{{@$review->json_user['photo_url']}}" target="_blank">
                                                    <div class="rating-avatar" style="background-image: url('{{@$review->json_user['image_url']}}'); background-size: contain; background-position: center;"></div><i class="fa fa-google verified"></i></a>
                                                @else
                                                    <a href="{{@$review->json_user['profile_url']}}" target="_blank">
                                                        <div class="rating-avatar" style="background-image: url('{{@$review->json_user['image_url']}}'); background-size: contain; background-position: center;"></div><i class="fa fa-yelp verified" style="background-color: #ff3d00;"></i></a>
                                                @endif
                                                </div>
                                                <div class="media-body">
                                                    @if(@$review->yelp_id == 'Google')
                                                         <a href="{{@$review->json_user['photo_url']}}" target="_blank"><h4 class="media-heading">{{@$review->json_user['name']}}
                                                        <span>{{date('M d,Y',strtotime($review->created_at))}}
                                                        <!--at {{date(' h:m a',strtotime($review->created_at))}}-->
                                                        </span></h4></a>
                                                    @else
                                                        <a href="{{@$review->json_user['profile_url']}}" target="_blank"><h4 class="media-heading">{{@$review->json_user['name']}}
                                                        <span>{{date('M d,Y',strtotime($review->created_at))}}
                                                        <!--at {{date(' h:m a',strtotime($review->created_at))}}-->
                                                        </span></h4></a>
                                                    @endif
                                                    <span class="raty" data-score="{{$review->rating}}"></span>
                                                    <p>{{$review->review}}.</p>

                                                </div>
                                                @endif
                                            </div>
                                            @endforeach
                                            @empty
                                            <p style="color:red;">No More Reviews.</p>
                                            @endif
                                            <script type="text/javascript">
$(document).ready(function(){
    $(".raty").raty({
        readOnly: true,
        starOff: 'fa fa-star-o',
        starOn: 'fa fa-star',
        score: function() {
            return $(this).attr('data-score');
        },
    });
   
});

</script>



    