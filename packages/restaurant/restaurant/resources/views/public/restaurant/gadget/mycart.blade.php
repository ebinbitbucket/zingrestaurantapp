<div class="collapse navbar-collapse cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cart">
    <div class="aside-cart-header">
        <h4 class="title">Your Cart</h4>
        <button class="cart-close-btn"><i class="ik ik-x"></i></button>
    </div>
            
            @if(Cart::count() <= 0)
            <div class="aside-empty-content-wrap">
                <img src="{{theme_asset('img/empty-cart.png')}}" alt="">
                <h2>Your Cart is Empty</h2>
                <p>Looks like you haven't made your choice yet</p>
            </div>
            @else
            <div class="aside-cart-rest-detail">
                @foreach(Cart::content() as $cart) 
                <?php $rest_name = $cart->options->restaurantname;
                      $rest_logo = $cart->options->restaurantimage;
                      ?>
                @endforeach 
                @if(!empty($rest_logo)) <span class="rest-img" style="background-image: url('{{trans_url('image/original/'.$rest_logo[0]['path'])}}')"></span>@endif
                <div class="rest-info">
                    <h6>{{$rest_name}}</h6>
                </div>
            </div>
            <table class="cart-table">
                <tbody>
                   <?php $delivery_charge =0; ?>

                    @foreach(Cart::content() as $item) 
                    <?php 
                    $variation = $item->options->variation; 
                    $addon_price = 0;
                        if(!empty($item->options['addons'])){
                    foreach($item->options['addons'] as $cart_addon){

                            $addon_price = $addon_price + $cart_addon[2];
                    }
                                   
                   }
                    ?>
                    <tr>
                        <td style="width: 200px;{{$item->options['menu_addon'] == 'addon' ? 'padding-left: 20px' : ''}}">
                            <h3 class="product-name"><a href="#">{{$item->name}}</a></h3>
                            @if(!empty($variation))<h3 class="product-name"><a href="#">(Variation-{{$variation}})</a></h3>@endif
                            @if($item->options['menu_addon'] != 'addon')<span class="product-price">{{$item->qty}} @if($item->price > 0)- ${{$item->price+$addon_price}}@endif</span>@endif
                                @if(!empty($item->options['addons']))
                                    @foreach($item->options['addons'] as $cart_addon)
                                    <h3 class="product-name"><a href="#">{{$cart_addon[1]}}</a></h3>
                                    @endforeach
                                @endif
                        </td>
                        <td>
                            <a href="#" class="remove-btn" onclick="removeCartItem1('{{$item->rowId}}')"><i class="ion ion-android-close"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><hr></td>
                    </tr> 
                   <?php 
                   if($item->options['delivery_charge'] != '')
                   { 
                    $delivery_charge = $item->options['delivery_charge']; }
                  ?>
                 @endforeach
                </tbody>
            </table>
            <div class="cart-total">
                <div class="total-wrap-inner">
                    <div class="total-item">
                        <span>Item Total</span>
                        <span>${{Cart::total()}}</span>
                    </div>
                  @if($item->options['delivery_type'] == 'Yes')
                   {{-- <div class="total-item">
                        <span>Delivery Charges</span>
                        <span>${{$delivery_charge}}</span>
                    </div> --}}
                @endif
                </div>
                <div class="grand-total">
                    <span>To Pay</span>
                    <span>${{Cart::total()}}</span>
                </div>
                <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme btn-block mt-10">Checkout Now</a>
                
            </div>
            @endif
        </div>
       
<script type="text/javascript">
    function emptyCart(){ 
      $('#EmptyCartModel').modal({show:true, backdrop: 'static',
            keyboard: false}); 
    }
      
      function removeCartItem1(id){
        $.getJSON({
                  type: 'GET',
                  url : '{{url("carts/remove")}}'+'/'+id,
                  success: function(data) { 
                    // $('#cart_items_side').load('{{url("cart/restaurantcart/")}}');
                  
                    $('#cart_items_side').load('{{url("cart/restaurantcart/")}}');
    
                    $('#cart').load('{{url("cart/restaurantcartPage/")}}');
                    if({!!Cart::count()-1!!} <=0 ){ 
                        $('body').removeClass('cbp-spmenu-push cbp-spmenu-push-toleft');
                        $('.cbp-spmenu-right').removeClass('cbp-spmenu-open');
                        $('#mobile_chk_btn').hide();
                        $('#mobile_chk_txt').show();
                    }
                    
                  },
                  error: function(msg) { 
                  
                  }
              });
    }        
$('.cart-close-btn').click(function(){
     $('body').removeClass('cbp-spmenu-push cbp-spmenu-push-toleft');
                        $('.cbp-spmenu-right').removeClass('cbp-spmenu-open');
})

if({!!Cart::count()!!} ==0 ){ 
    $('body').removeClass('cbp-spmenu-push cbp-spmenu-push-toleft');
    $('.cbp-spmenu-right').removeClass('cbp-spmenu-open');
    $('#mobile_chk_btn').hide();
    $('#mobile_chk_txt').show();
}

</script>            