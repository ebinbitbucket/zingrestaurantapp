<div class="hero-search-wrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-7 col-md-12">

                                <div class="hero-search-tab-wrap hidden">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="eateries-tab" data-toggle="tab" href="#eateries" role="tab" aria-controls="eateries" aria-selected="true">Explore Eateries</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="cuisines-tab" data-toggle="tab" href="#cuisines" role="tab" aria-controls="cuisines" aria-selected="false">Explore Cuisines</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="eateries" role="tabpanel" aria-labelledby="eateries-tab">
                                            <div class="item-category-wrap">
                                                <div class="category-item">
                                                    <input type="checkbox" name="eatery_categories" id="eatery_all">
                                                    <label for="eatery_all">
                                                        <i class="icon flaticon-fast-food"></i>
                                                        <h5>All</h5>
                                                    </label>
                                                </div>
                                                <div class="category-item">
                                                    <input type="checkbox" name="eatery_categories" id="eatery_restaurant">
                                                    <label for="eatery_restaurant">
                                                        <i class="icon flaticon-dish"></i>
                                                        <h5>Restaurant</h5>
                                                    </label>
                                                </div>
                                                <div class="category-item">
                                                    <input type="checkbox" name="eatery_categories" id="eatery_foodtruck">
                                                    <label for="eatery_foodtruck">
                                                        <i class="icon flaticon-delivery-truck-1"></i>
                                                        <h5>Food Truck</h5>
                                                    </label>
                                                </div>
                                                <div class="category-item">
                                                    <input type="checkbox" name="eatery_categories" id="eatery_catering">
                                                    <label for="eatery_catering">
                                                        <i class="icon flaticon-soup"></i>
                                                        <h5>Catering</h5>
                                                    </label>
                                                </div>
                                                <div class="category-item">
                                                    <input type="checkbox" name="eatery_categories" id="eatery_farmers">
                                                    <label for="eatery_farmers">
                                                        <i class="icon flaticon-fruit"></i>
                                                        <h5>Farmers Market</h5>
                                                    </label>
                                                </div>
                                                <div class="category-item">
                                                    <input type="checkbox" name="eatery_categories" id="eatery_bakery">
                                                    <label for="eatery_bakery">
                                                        <i class="icon flaticon-coffee-cup"></i>
                                                        <h5>Bakery</h5>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <input type="text" name="search[type]" class="selectize" id="input-tags3" placeholder="All">
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="delivery-type-block">
                                                        <div class="block-item">
                                                            <input type="radio" name="deli_type" id="del_asap">
                                                            <label for="del_asap"><i class="ion ion-android-time"></i>Asap</label>
                                                        </div>
                                                        <div class="block-item">
                                                            <input type="radio" name="deli_type" id="del_schedule">
                                                            <label for="del_schedule">
                                                                <i class="ion ion-android-calendar"></i>
                                                                <span id="deltypet_txt">Schedule</span>
                                                                <span id="deltypet_wrap" style="display: none;">
                                                                    <span id="del_date_v"></span> - 
                                                                    <span id="del_time_v"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="location-wrap-inner">
                                                        <i class="flaticon-pin"></i>
                                                        <input type="text" name="search[address]" id="txtPlaces" class="form-control" placeholder="Enter your Address">
                                                        <input type="hidden" name="search[latitude]" id="latitude">
                                                        <input type="hidden" name="search[longitude]" id="longitude">
                                                        <a href="#" class="my-location ion ion-android-locate"></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-2"><button type="submit" class="btn btn-theme btn-block search-btn">Search</button></div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="cuisines" role="tabpanel" aria-labelledby="cuisines-tab">
                                            <div class="form-group mb-10">
                                                <input type="text" id="keywordsInput" class="form-control ui-autocomplete-input" placeholder="Type Cuisine"  autocomplete="off">
                                            </div>
                                            <div class="row mb-10">
                                                <div class="col-md-4">
                                                    <div class="rating">
                                                        <select id="example-pill" name="rating" autocomplete="off">
                                                            <option value="1"><i class="fas fa-star"></i></option>
                                                            <option value="2"><i class="fas fa-star"></i></option>
                                                            <option value="3"><i class="fas fa-star"></i></option>
                                                            <option value="4"><i class="fas fa-star"></i></option>
                                                            <option value="5"><i class="fas fa-star"></i></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="delivery-type-block">
                                                        <div class="block-item">
                                                            <input type="radio" name="cuisine_deli_type" id="cuisine_del_asap">
                                                            <label for="cuisine_del_asap"><i class="ion ion-android-time"></i>Asap</label>
                                                        </div>
                                                        <div class="block-item">
                                                            <input type="radio" name="cuisine_deli_type" id="cuisine_del_schedule">
                                                            <label for="cuisine_del_schedule">
                                                                <i class="ion ion-android-calendar"></i>Schedule
                                                                <span id="deltypet_wrap" style="display: none;">
                                                                    <span id="del_date_v"></span> - 
                                                                    <span id="del_time_v"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="location-wrap-inner">
                                                        <i class="flaticon-pin"></i>
                                                        <input type="text" name="search[address]" id="txtPlaces" class="form-control" placeholder="Enter your Address">
                                                        <input type="hidden" name="search[latitude]" id="latitude">
                                                        <input type="hidden" name="search[longitude]" id="longitude">
                                                        <a href="#" class="my-location ion ion-android-locate"></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-2"><button type="submit" class="btn btn-theme btn-block search-btn">Search</button></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                                <!-- <div class="item-category-wrap hero-catslider">
                                    <a class="category-item" href="{{url('restaurants')}}?category=all">
                                        <div class="icon flaticon-fast-food" style="color: #333;"></div>
                                        <div class="content"><h5>All</h5></div>
                                    </a>
                                    <a class="category-item" href="{{url('restaurants')}}?category=restaurant">
                                        <div class="icon flaticon-dish" style="color: #333;"></div>
                                        <div class="content"><h5>Restaurant</h5></div>
                                    </a>
                                    <a class="category-item" href="{{url('restaurants')}}?category=bakery">
                                        <div class="icon flaticon-coffee-cup" style="color: #333;"></div>
                                        <div class="content"><h5>Bakery</h5></div>
                                    </a>
                                    <a class="category-item" href="{{url('restaurants')}}?category=food_truck">
                                        <div class="icon flaticon-delivery-truck-1" style="color: #333;"></div>
                                        <div class="content"><h5>Food Truck</h5></div>
                                    </a>
                                    <a class="category-item" href="{{url('restaurants')}}?category=farmers_market">
                                        <div class="icon flaticon-fruit" style="color: #333;"></div>
                                        <div class="content"><h5>Farmers Market</h5></div>
                                    </a>
                                    <a class="category-item" href="{{url('restaurants')}}?category=catering">
                                        <div class="icon flaticon-soup" style="color: #333;"></div>
                                        <div class="content"><h5>Catering</h5></div>
                                    </a>
                                </div> -->



                               {!!Form::vertical_open()
                ->method('GET')
                ->action('restaurants/')
                ->addclass('search-form-compact')
                !!}
                                    <div class="compact-wrap-inner">
                                        <div class="location-wrap">
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <div class="location-wrap-inner">
                                                        <i class="flaticon-pin"></i>
                                                        <input type="text" name="search[address]" id="txtPlaces" class="form-control" placeholder="Enter your Address">
                                                        <input type="hidden" name="search[latitude]" id="latitude">
                                                        <input type="hidden" name="search[longitude]" id="longitude">
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="category-wrap">
                                                        <select class="selectize" name="search[category_name]" id="category-select"></select>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            <button type="submit" class="btn btn-theme search-btn">Search</button>
                                        </div>
                                        <div class="advance-search-wrap">
                                            <div class="row">
                                                <!-- <div class="item col-md-3">
                                                    <input type="text" class="form-control" placeholder="Keywords">
                                                </div> -->
                                                <div class="item col-md-4">
                                                    <input type="text" name="search[type]" class="selectize" id="input-tags3" placeholder="All">
                                <!-- <select name="search[type][]" id="type" class="selectize category-select">
                                    <option value="" selected>All</option>
                                </select> -->
                                                </div>
                                                <div class="item col-md-4">
                                                    <select name="search[delivery]" id="type" class="selectize type-select" placeholder="Pickup">
                                                    </select>
                                                </div>
                                                <div class="item col-md-4">
                                                    <div class="dropdown deltype-dropdown">
                                                        <button class="btn dropdown-toggle btn-block text-left" type="button" id="deltypeMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <span id="del_type_v">ASAP</span>
                                                            <span id="deltypet_wrap" style="display: none;">
                                                                <span id="del_date_v"></span> - 
                                                                <span id="del_time_v"></span>
                                                            </span>
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="deltypeMenu">
                                                           <div class="delivery-type-item">
                                                                <input type="radio" name="del_type" value="ASAP" id="asap" checked="">
                                                                <label for="asap"><i class="ion ion-android-time"></i>ASAP</label>
                                                            </div>
                                                            <div class="delivery-type-item">
                                                                <input type="radio" name="del_type" id="schedule_order" value="Schedule Order">
                                                                <label for="schedule_order"><i class="ion ion-android-calendar"></i>Schedule Order</label>
                                                                <!-- <div class="schedule-inner-block" id="scedule-block">
                                                                    <div class="form-group">
                                                                        <p>Select a Delivery Date</p>
                                                                        <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                                                            @for($i=0;$i<5;$i++)
                                                                            <div class="date-item active" data-date="{{date('d-M-Y', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                                                                <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                                                                <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                                                            </div>
                                                                            @endfor
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group mb-0">
                                                                        <p>Desired Delivery Time</p>
                                                                        <select class="selectize" id="sch_time">
                                                                            <option value="">Select a Time</option>
                                                                            <option>12:00 AM</option>
                                                                            <option>12:15 AM</option>
                                                                            <option>12:30 AM</option>
                                                                            <option>12:45 AM</option>
                                                                            <option>01:00 AM</option>
                                                                            <option>01:15 AM</option>
                                                                            <option>01:30 AM</option>
                                                                            <option>01:45 AM</option>
                                                                            <option>02:00 AM</option>
                                                                            <option>02:15 AM</option>
                                                                            <option>02:30 AM</option>
                                                                            <option>02:45 AM</option>
                                                                            <option>03:00 AM</option>
                                                                            <option>03:15 AM</option>
                                                                            <option>03:30 AM</option>
                                                                            <option>03:45 AM</option>
                                                                            <option>04:00 AM</option>
                                                                            <option>04:15 AM</option>
                                                                            <option>04:30 AM</option>
                                                                            <option>04:45 AM</option>
                                                                            <option>05:00 AM</option>
                                                                            <option>05:15 AM</option>
                                                                            <option>05:30 AM</option>
                                                                            <option>05:45 AM</option>
                                                                            <option>06:00 AM</option>
                                                                            <option>06:15 AM</option>
                                                                            <option>06:30 AM</option>
                                                                            <option>06:45 AM</option>
                                                                            <option>07:00 AM</option>
                                                                            <option>07:15 AM</option>
                                                                            <option>07:30 AM</option>
                                                                            <option>07:45 AM</option>
                                                                            <option>08:00 AM</option>
                                                                            <option>08:15 AM</option>
                                                                            <option>08:30 AM</option>
                                                                            <option>08:45 AM</option>
                                                                            <option>09:00 AM</option>
                                                                            <option>09:15 AM</option>
                                                                            <option>09:30 AM</option>
                                                                            <option>09:45 AM</option>
                                                                            <option>10:00 AM</option>
                                                                            <option>10:15 AM</option>
                                                                            <option>10:30 AM</option>
                                                                            <option>10:45 AM</option>
                                                                            <option>11:00 AM</option>
                                                                            <option>11:15 AM</option>
                                                                            <option>11:30 AM</option>
                                                                            <option>11:45 AM</option>
                                                                            <option>12:00 PM</option>
                                                                            <option>12:15 PM</option>
                                                                            <option>12:30 PM</option>
                                                                            <option>12:45 PM</option>
                                                                            <option>01:00 PM</option>
                                                                            <option>01:15 PM</option>
                                                                            <option>01:30 PM</option>
                                                                            <option>01:45 PM</option>
                                                                            <option>02:00 PM</option>
                                                                            <option>02:15 PM</option>
                                                                            <option>02:30 PM</option>
                                                                            <option>02:45 PM</option>
                                                                            <option>03:00 PM</option>
                                                                            <option>03:15 PM</option>
                                                                            <option>03:30 PM</option>
                                                                            <option>03:45 PM</option>
                                                                            <option>04:00 PM</option>
                                                                            <option>04:15 PM</option>
                                                                            <option>04:30 PM</option>
                                                                            <option>04:45 PM</option>
                                                                            <option>05:00 PM</option>
                                                                            <option>05:15 PM</option>
                                                                            <option>05:30 PM</option>
                                                                            <option>05:45 PM</option>
                                                                            <option>06:00 PM</option>
                                                                            <option>06:15 PM</option>
                                                                            <option>06:30 PM</option>
                                                                            <option>06:45 PM</option>
                                                                            <option>07:00 PM</option>
                                                                            <option>07:15 PM</option>
                                                                            <option>07:30 PM</option>
                                                                            <option>07:45 PM</option>
                                                                            <option>08:00 PM</option>
                                                                            <option>08:15 PM</option>
                                                                            <option>08:30 PM</option>
                                                                            <option>08:45 PM</option>
                                                                            <option>09:00 PM</option>
                                                                            <option>09:15 PM</option>
                                                                            <option>09:30 PM</option>
                                                                            <option>09:45 PM</option>
                                                                            <option>10:00 PM</option>
                                                                            <option>10:15 PM</option>
                                                                            <option>10:30 PM</option>
                                                                            <option>10:45 PM</option>
                                                                            <option>11:00 PM</option>
                                                                            <option>11:15 PM</option>
                                                                            <option>11:30 PM</option>
                                                                            <option>11:45 PM</option>
                                                                        </select>
                                                                    </div>
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <input type="hidden" name="search[working_date]" id="datecheck">
                                                    <input type="hidden" name="search[working_hours]" id="timecheck">
                                                    <input type="hidden" name="search[time_type]" id="time_type">
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                 {!!Form::close()!!}
                                
                                <div class="serach-header mt-30 mb-0">
                                    <h1 class="title">Explore Eateries near you</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade schedule-modal" id="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="scheduleModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="scheduleModalTitle">Schedule Order</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="schedule-inner-block" id="scedule-block">
                                    <div class="form-group">
                                        <p>Select a Delivery Date</p>
                                        <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                            @for($i=0;$i<5;$i++)
                                            <div class="date-item active" data-date="{{date('d-M-Y', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                                <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                                <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                            </div>
                                            @endfor
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <p>Desired Delivery Time</p>
                                        <select class="selectize" id="sch_time">
                                            <option value="">Select a Time</option>
                                            <option>12:00 AM</option>
                                            <option>12:15 AM</option>
                                            <option>12:30 AM</option>
                                            <option>12:45 AM</option>
                                            <option>01:00 AM</option>
                                            <option>01:15 AM</option>
                                            <option>01:30 AM</option>
                                            <option>01:45 AM</option>
                                            <option>02:00 AM</option>
                                            <option>02:15 AM</option>
                                            <option>02:30 AM</option>
                                            <option>02:45 AM</option>
                                            <option>03:00 AM</option>
                                            <option>03:15 AM</option>
                                            <option>03:30 AM</option>
                                            <option>03:45 AM</option>
                                            <option>04:00 AM</option>
                                            <option>04:15 AM</option>
                                            <option>04:30 AM</option>
                                            <option>04:45 AM</option>
                                            <option>05:00 AM</option>
                                            <option>05:15 AM</option>
                                            <option>05:30 AM</option>
                                            <option>05:45 AM</option>
                                            <option>06:00 AM</option>
                                            <option>06:15 AM</option>
                                            <option>06:30 AM</option>
                                            <option>06:45 AM</option>
                                            <option>07:00 AM</option>
                                            <option>07:15 AM</option>
                                            <option>07:30 AM</option>
                                            <option>07:45 AM</option>
                                            <option>08:00 AM</option>
                                            <option>08:15 AM</option>
                                            <option>08:30 AM</option>
                                            <option>08:45 AM</option>
                                            <option>09:00 AM</option>
                                            <option>09:15 AM</option>
                                            <option>09:30 AM</option>
                                            <option>09:45 AM</option>
                                            <option>10:00 AM</option>
                                            <option>10:15 AM</option>
                                            <option>10:30 AM</option>
                                            <option>10:45 AM</option>
                                            <option>11:00 AM</option>
                                            <option>11:15 AM</option>
                                            <option>11:30 AM</option>
                                            <option>11:45 AM</option>
                                            <option>12:00 PM</option>
                                            <option>12:15 PM</option>
                                            <option>12:30 PM</option>
                                            <option>12:45 PM</option>
                                            <option>01:00 PM</option>
                                            <option>01:15 PM</option>
                                            <option>01:30 PM</option>
                                            <option>01:45 PM</option>
                                            <option>02:00 PM</option>
                                            <option>02:15 PM</option>
                                            <option>02:30 PM</option>
                                            <option>02:45 PM</option>
                                            <option>03:00 PM</option>
                                            <option>03:15 PM</option>
                                            <option>03:30 PM</option>
                                            <option>03:45 PM</option>
                                            <option>04:00 PM</option>
                                            <option>04:15 PM</option>
                                            <option>04:30 PM</option>
                                            <option>04:45 PM</option>
                                            <option>05:00 PM</option>
                                            <option>05:15 PM</option>
                                            <option>05:30 PM</option>
                                            <option>05:45 PM</option>
                                            <option>06:00 PM</option>
                                            <option>06:15 PM</option>
                                            <option>06:30 PM</option>
                                            <option>06:45 PM</option>
                                            <option>07:00 PM</option>
                                            <option>07:15 PM</option>
                                            <option>07:30 PM</option>
                                            <option>07:45 PM</option>
                                            <option>08:00 PM</option>
                                            <option>08:15 PM</option>
                                            <option>08:30 PM</option>
                                            <option>08:45 PM</option>
                                            <option>09:00 PM</option>
                                            <option>09:15 PM</option>
                                            <option>09:30 PM</option>
                                            <option>09:45 PM</option>
                                            <option>10:00 PM</option>
                                            <option>10:15 PM</option>
                                            <option>10:30 PM</option>
                                            <option>10:45 PM</option>
                                            <option>11:00 PM</option>
                                            <option>11:15 PM</option>
                                            <option>11:30 PM</option>
                                            <option>11:45 PM</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Schedule</button>
                            </div>
                        </div>
                    </div>
                </div>
<script>

    var availableMenus = [
    "Chicken Biriyani",
    "Beef Biriyani",
    "Veg Biriyani",
    "Mutton Biriyani",
    "Fish Biriyani",
    "Ghee Rice",
    "Thanthoori Chicken",
    "Alfam",
    "Veg Kuruma",
    "Noodles",
    ];
    $( "#keywordsInput" ).autocomplete({
        source: availableMenus,
        autoFocus:true
    });

    $('#example-pill').barrating('show', {
        theme: 'bars-pill',
        initialRating: '1',
        showValues: true,
        showSelectedRating: false,
        allowEmpty: true,
        emptyValue: '-- no rating selected --',
    });

    $('input[id="del_schedule"]').click(function() {
        $('#scheduleModal').modal('show');
    });

    $('input[name="deli_type"]').click(function() {
        if($(this).attr('id') == 'del_schedule') {
            $("#deltypet_wrap").show();
            $("#deltypet_txt").hide();
        }
        else {
            $("#deltypet_wrap").hide();
            $("#deltypet_txt").show();
        }
    });


    var CATEGORIES = [{
        name: "All",
        code: "all",
        icon: "icon flaticon-fast-food"
    }, {
        name: "Restaurant",
        code: "restaurant",
        icon: "icon flaticon-dish"
    }, {
        name: "Bakery",
        code: "bakery",
        icon: "icon flaticon-coffee-cup"
    }, {
        name: "Food Truck",
        code: "food_truck",
        icon: "icon flaticon-delivery-truck-1"
    }, {
        name: "Farmers Market",
        code: "farmers_market",
        icon: "icon flaticon-fruit"
    }, {
        name: "Catering",
        code: "catering",
        icon: "icon flaticon-soup"
    }];

    $("#category-select").selectize({
        maxItems: 1,
        labelField: 'name',
        valueField: 'code',
        searchField: ['name', 'code'],
        placeholder     : "All Categories",
        options: CATEGORIES,
        preload: true,
        persist: false,
        render: {
            item: function(item, escape) {
                return "<div class='cat-item'><i class='flaticon-" + escape(item.icon) + "'></i>" + escape(item.name) + "</div>";
            },
            option: function(item, escape) {
                return "<div class='cat-item'><i class='flaticon-" + escape(item.icon) + "'></i>" + escape(item.name) + "</div>";
            }
        },
    });


$('#input-tags3').selectize({
    plugins: ['remove_button'],
    persist: false,
    preload: true,
    labelField: 'types',
    valueField: 'types',
    searchField: ['types'],
    placeholder     : "All Cuisines",
    options : [
@forelse(trans('restaurant::restaurant.options.type') as $key => $types)

    {types: "{{$types}}" },
    @empty
    @endif
],
});
            hide = true;
            $('body').on("click", function () {
                if (hide) $(".search-form").removeClass('adv-open');
                hide = true;
             });
            
            $('body').on('click', '.search-form', function () {
                $(this).addClass('adv-open');
                hide = false;
            });

            $(document).ready(function() { 
                document.getElementById('timecheck').value = new Date().toLocaleTimeString();
                document.getElementById('datecheck').value = new Date().toLocaleDateString();
                document.getElementById('time_type').value = 'asap';

         $("#asap").on('click', function(){ 
             document.getElementById('timecheck').value = new Date().toLocaleTimeString();
             document.getElementById('datecheck').value = new Date().toLocaleDateString();
             document.getElementById('time_type').value = 'asap';
            })
      $("#sch_time").on('change', function(){ 
        var c_Time = $(this).find(":checked").val();  
            document.getElementById('timecheck').value = c_Time;
            document.getElementById('time_type').value = 'scheduled';
     })
        $('#sch_time').selectize({
        maxItems: 1,
    });

                $('input[name="del_type"]').click(function() {
                    if($(this).attr('id') == 'schedule_order') {
                        $('#scedule-block').show();
                        $("#del_type_v").hide();
                        $("#deltypet_wrap").show();
                    }
                    else {
                        $('#scedule-block').hide();  
                        $("#del_type_v").show(); 
                        $("#deltypet_wrap").hide();
                    }
                });
                $(".deltype-dropdown .dropdown-menu").click(function(e){
                    e.stopPropagation();
                });

                $('input[name="del_type"]').click(function() {
                    var radioValue = $(this).val();
                    $("#del_type_v").html(radioValue)
                });
                var schDateitem = $("#sch_DateWrap .date-item");
                $(schDateitem).click(function() { 
                    var c_Date = $(this).attr("data-date");
                    document.getElementById('datecheck').value = c_Date;
                    $("#sch_DateWrap .date-item").removeClass("active");
                    $(this).addClass("active");
                    $("#del_date_v").html(c_Date)
                });
                $('#sch_time').on('change', function() { 
                    var c_Time = $(this).find(":checked").val(); 
                    $("#del_time_v").html(c_Time)
                });

            });
</script>      
<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
<script type="text/javascript">
     var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {
                geocodeAddress(geocoder);
        });
    });
     function geocodeAddress(geocoder) {
        var address = document.getElementById('txtPlaces').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude').value=results[0].geometry.location.lat();
            document.getElementById('longitude').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
</script>  
<script>
  $("#priceRangeHeader").each(function() {
        var dataMin = $(this).attr('data-min');
        var dataMax = $(this).attr('data-max');
        var dataUnit = $(this).attr('data-unit');
        $(this).append("<input type='text' class='first-slider-value' name='search[price_range_min]'/><input type='text' class='second-slider-value' name='search[price_range_max]'/>");
        $(this).slider({
            range: true,
            min: dataMin,
            max: dataMax,
            step: 0.5,
            values: [dataMin, dataMax],
            slide: function(event, ui) {
                event = event;
                $(this).children(".first-slider-value").val(dataUnit + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $(this).children(".second-slider-value").val(dataUnit + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#minPriceHead").val(dataUnit + ui.values[0].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
                $("#maxPriceHead").val(dataUnit + ui.values[1].toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            }
        });
        $(this).children(".first-slider-value").val(dataUnit + $(this).slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $(this).children(".second-slider-value").val(dataUnit + $(this).slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $("#minPriceHead").val(dataUnit + $(this).slider("values", 0).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        $("#maxPriceHead").val(dataUnit + $(this).slider("values", 1).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
    });
</script>        