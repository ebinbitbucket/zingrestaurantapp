 
 <ul class="list-unstyled result-bucket">
    @foreach($restaurants as $restaurant)
    <li class="result-entry" data-suggestion="Target 1" data-position="1" data-type="type" data-analytics-type="merchant">
        <a href="{{url('restaurants/'.$restaurant->slug)}}" class="result-link">
            <div class="media">
<!--                 <div class="media-body" onclick='master_name("{{$restaurant->name}}")'>
 -->           <div class="media-body" >
                    <h4 class="media-heading master_name"> {{$restaurant->name}}</h4>
                </div>
            </div>
        </a>
    </li>
    @endforeach
</ul>
<script type="text/javascript">
    

</script>
<style type="text/css">
    .result-bucket li {
    padding: 7px 15px;
}
.result-link {
    color: #4f7593;
}
.result-link .media-body {
    font-size: 13px;
    color: gray;
}
.result-link .media-heading {
    font-size: 15px;
    font-weight: 400;
    color: #4f7593;
}
.result-link:hover,
.result-link:hover .media-heading,
.result-link:hover .media-body {
    text-decoration: none;
    color: #4f7593
}
.result-link .media-object {
    width: 50px;
    padding: 3px;
    border: 1px solid #c1c1c1;
    border-radius: 3px;
}
.result-entry + .result-entry {
    border-top:1px solid #ddd;
}
.top-keyword {
    margin: 3px 0 0;
    font-size: 12px;
    font-family: Arial;
}
.top-keyword a {
    font-size: 12px;
    font-family: Arial;
}
.top-keyword a:hover {
    color: rgba(0, 0, 0, 0.7);
}
</style>