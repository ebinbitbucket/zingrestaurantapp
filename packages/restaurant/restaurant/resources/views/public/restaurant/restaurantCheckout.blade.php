<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">This Restauarnt can't take orders at this time.</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ion ion-ios-close-outline"></span></button>
</div> 
<div class="modal-body">
    <p>Please look at the hours of this Eatery</p>
    <table class="table table-bordered m-0">
        <tr>
            <td><b>Sunday</b></td>
            <td>
                @foreach($restaurant_timings as $key => $value)
                @if($value->day == 'sun')<p class="m-0">@if($value->opening== 'off' || $value->closing == 'off')
                Off @else{{date('h:i a',strtotime($value->opening))}} - {!!date('h:i a',strtotime($value->closing))!!}@endif</p>
                @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td><b>Monday</b></td>
            <td>
                @foreach($restaurant_timings as $key => $value)
                @if($value->day == 'mon')<p class="m-0">@if($value->opening== 'off' || $value->closing == 'off')
                Off @else{{date('h:i a',strtotime($value->opening))}} - {!!date('h:i a',strtotime($value->closing))!!}@endif</p>
                @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td><b>Tuesday</b></td>
            <td>
                @foreach($restaurant_timings as $key => $value)
                @if($value->day == 'tue')<p class="m-0">@if($value->opening== 'off' || $value->closing == 'off')
                Off @else{{date('h:i a',strtotime($value->opening))}} - {!!date('h:i a',strtotime($value->closing))!!}@endif</p>
                @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td><b>Wednesday</b></td>
            <td>
                @foreach($restaurant_timings as $key => $value)
                @if($value->day == 'wed')<p class="m-0">@if($value->opening== 'off' || $value->closing == 'off')
                Off @else{{date('h:i a',strtotime($value->opening))}} - {!!date('h:i a',strtotime($value->closing))!!}@endif</p>
                @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td><b>Thursday</b></td>
            <td>
                @foreach($restaurant_timings as $key => $value)
                @if($value->day == 'thu')<p class="m-0">@if($value->opening== 'off' || $value->closing == 'off')
                Off @else{{date('h:i a',strtotime($value->opening))}} - {!!date('h:i a',strtotime($value->closing))!!}@endif</p>
                @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td><b>Friday</b></td>
            <td>
                @foreach($restaurant_timings as $key => $value)
                @if($value->day == 'fri')<p class="m-0">@if($value->opening== 'off' || $value->closing == 'off')
                Off @else{{date('h:i a',strtotime($value->opening))}} - {!!date('h:i a',strtotime($value->closing))!!}@endif</p>
                @endif
                @endforeach
            </td>
        </tr>
        <tr>
            <td><b>Saturday</b></td>
            <td>
                @foreach($restaurant_timings as $key => $value)
                @if($value->day == 'sat')<p class="m-0">@if($value->opening== 'off' || $value->closing == 'off')
                Off @else{{date('h:i a',strtotime($value->opening))}} - {!!date('h:i a',strtotime($value->closing))!!}@endif</p>
                @endif
                @endforeach
            </td>
        </tr>
    </table>
</div>
<div class="modal-footer justify-content-start">
    <button type="button" class="btn btn-theme" style="width: 150px;" data-dismiss="modal" id="ErrorModel_submit">Ok</button>
</div>
<style>
    .table p {
        font-size: 14px;
        color: #444;
    }
</style>