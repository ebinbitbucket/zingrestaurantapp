{!!'<!-- ' . date("F Y h:i:s u") . '-->'!!}

@php
Session::put('search_time',date('Y-m-d H:i:s'));
@endphp

            <div class="single-listing-header-wrap">
                <div class="container">
                    <div class="row" >
                        <div class="col-3 col-md-3 col-lg-2">
                            <div class="header-left-info" >
                                <a class="logo-block" href="{{url('/')}}">
                                    <img src="{{url('assets/img/logo-round-big.png')}}" alt="logo" title="Zing My Order">
                                </a>
                                <div class="user-infos">
                                    @if(Auth::user())
                                    <h3>{{Auth::user()->name}}
                                    @if(Auth::user()->guest == 'false')<span class="points">{{number_format(Cart::getLoyaltypoints(),0)}}</span>@endif
                                    </h3>
                                    @else
                                    @endif
                                    <div class="location" data-toggle="tooltip" data-placement="top" title="{{Session::get('location')}}"><i class="ik ik-map-pin"></i>{{!empty(Session::get('location')) ? strtok(Session::get('location'),',') : 'Select Location' }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-9 col-md-9 col-lg-10">
                            <div class="single-listing-info">
                                <div class="single-image" style="background-image: url({{url($restaurant->mainlogo)}})"></div>
                                <div class="single-content">
                                    <div class="title-block">
                                        <h3>{{$restaurant->name}}</h3>
                                        <div class="rating">
                                            <span class="raty" data-score="{{$restaurant->rating}}"></span><span class="count"><a href="#review" class="inner-scroll">{{$restaurant->rating}} ({{$restaurant->review->count()}} ratings)</a></span>
                                        </div>
                                        <button onclick="add_fav()" class="add-fav-btn btn" data-toggle="tooltip" data-placement="top" title="Add to Your Favorites" data-original-title="Add to Favorites"><i class="ion ion-ios-heart"></i></button>
                                        <div class="item" style="padding-left: 10px;">
                                            @if(!empty($restaurant->social_media_links))
                                                <div class="social">
                                                @foreach($restaurant->social_media_links as $key_link => $val_link)
                                                    @if($key_link == 'facebook' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #3B5998" target="_blank"><i class="fa fa-facebook-f"></i></a>
                                                    @elseif($key_link == 'twitter' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #55ACEE" target="_blank"><i class="fa fa-twitter"></i></a>
                                                    @elseif($key_link == 'youtube' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #bb0000" target="_blank"><i class="fa fa-youtube"></i></a>
                                                    @elseif($key_link == 'linkedln' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #007bb5" target="_blank"><i class="fa fa-linkedin"></i></a>
                                                    @elseif($key_link == 'instagram' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #405DE6" target="_blank"><i class="fa fa-instagram"></i></a>
                                                    @elseif($key_link == 'pintrest' && !empty($val_link))
                                                        <a href="{{$val_link}}" style="background-color: #cb2027" target="_blank"><i class="fa fa-pinterest"></i></a>
                                                    @endif
                                                @endforeach
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <p class="location" data-toggle="tooltip" data-placement="top" title="{{$restaurant->address}}"><i class="ik ik-map-pin"></i> {{strtok($restaurant->address,',')}}</p>
                                        @if(!empty($restaurant->phone))<p class="ml-15"> <i class="ik ik-phone"></i>&nbsp;&nbsp;{{substr($restaurant->phone, 0,3)}}-{{substr($restaurant->phone, 3,3)}}-{{substr($restaurant->phone, 6,4)}}</p>@endif
                                        <p class="ml-15">Today’s Hours

                                            @if(isset($restaurant->timings[$weekday]))
                                            @php
                                               $resultstr = [];
                                            @endphp
                                            @foreach($restaurant->timings[$weekday] as $key => $val)
                                                @if($val->opening != 'off' && $val->opening != 'off')
                                                    @php
                                                       $resultstr[] = date('g:i a',strtotime($val->opening)).' - '.date('g:i a',strtotime($val->closing))
                                                    @endphp
                                                @endif
                                            @endforeach
                                            @php
                                                echo implode(" & ",$resultstr);
                                            @endphp
                                            @else

                                            @endif
                                            <!-- @if(isset($restaurant->timings[$weekday]))
                                            @foreach($restaurant->timings[$weekday] as $key => $val)
                                                @if($val->opening != 'off' && $val->opening != 'closing')
                                                   {{date('g:i a',strtotime($val->opening))}} - {!!date('g:i a',strtotime($val->closing))!!} &
                                                @endif
                                            @endforeach
                                            @else
                                            @endif -->
                                        </p>
                                    </div>
                                    <div class="meta-infos">
                                        <div class="item">
                                            <div class="eatery-search-form">
                                                <i class="fa fa-search"></i>
                                                <input type="text"  class="form-control" id="dish_quick_search" placeholder="Search Food...">
                                            </div>
                                        </div>
                                        <div class="item">
                                            <a href="#about" class="btn eatery-inner-scroll">More Info</a>
                                        </div>
                                        <div class="item">
                                            <span>
                                                 @if($restaurant->published == 'Published')
                                                @if($restaurant->delivery == 'Yes')
                                                    @if($restaurant->delivery_charge == 0)
                                                        Free Delivery
                                                    @elseif($restaurant->delivery_charge != '')
                                                       Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                                    @endif
                                                @else
                                                    Take out
                                                @endif
                                                @endif
                                            </span>
                                        </div>
                                    </div>


                                </div>
                                <div class="single-header-actions">
                                    <a href="{{url('/')}}" class="ion ion-android-arrow-back back-btn" data-toggle="tooltip" data-placement="top" title="Back to Listing" data-original-title="Go Back"></a>
                                   <!--  <a href="#" onClick="history.go(-1)" class="ion ion-android-arrow-back back-btn" data-toggle="tooltip" data-placement="top" title="Back to Listing" data-original-title="Go Back"></a> -->
                                    <button class="cart-toggle-btn push-body menu-right" type="button" data-toggle="collapse" data-target="#cart">
                                        <div class="cart-inner">
                                            @if(Cart::count() != 0)
                                            <div class="icon cart-item-exist">
                                                <span class="count cart_count_header ">{{Cart::count()}}</span>
                                                <i class="ik ik-shopping-bag"></i>
                                            </div>
                                            @else
                                            <div class="icon">
                                                <span class="count cart_count_header">{{Cart::count()}}</span>
                                                <i class="ik ik-shopping-bag"></i>
                                            </div>
                                            @endif
                                        </div>
                                    </button>
                                    @if(Auth::user())
                                    @else
                                    <a href="#signIn_signUp_Modal" class="btn btn-theme btn-signin" data-toggle="modal" data-target="#signIn_signUp_Modal"> Sign In</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="single-listing-header-mobile-wrap shadow">
                <div class="top-nav-wrap">
                    <div class="container">
                        <div class="top-nav-wrap-inner">
                            <div class="single-header-infos mobile-menu-search-wrap">
                                <div class="mobile-menu-search-inner">
                                    <!-- <input type="search"  class="form-control" id="search_menu_mob">
                                    <button id="sub">Submit</button> -->

                                    <input type="search" class="form-control" placeholder="Search..." id="search_menu_mob">
                                    <button class="clear-btn" type="button" id="clear_btn" style="display: none;"><i class="ik ik-x"></i></button>
                                    <button class="btn" type="button" id="sub"><i class="ik ik-search"></i> 
                                    </button>
                                </div>
                            </div>

                            <div class="single-header-infos">
                                <button onclick="add_fav()" style="display:none" class="add-fav-btn btn" data-toggle="tooltip" data-placement="top" title="Add to Your Favorites" data-original-title="Add to Favorites"><i class="ion ion-ios-heart"></i></button>

                                <div class="actions">
                                    <button class="cart-toggle-btn push-body menu-right" type="button" data-toggle="collapse" data-target="#cart">
                                        <div class="cart-inner">
                                            @if(Cart::count() != 0)
                                            <div class="icon cart-item-exist">
                                                <span class="count cart_count_header ">{{Cart::count()}}</span>
                                                <i class="ik ik-shopping-bag"></i>
                                            </div>
                                            @else
                                            <div class="icon">
                                                <span class="count cart_count_header">{{Cart::count()}}</span>
                                                <i class="ik ik-shopping-bag"></i>
                                            </div>
                                            @endif
                                        </div>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="top-restaurant-info-wrap-mobile">

                @if($restaurant->mobile_banner)
                <div class="top-restaurant-banner"  style="background-image: url({{url(@$restaurant->defaultImage('mobile_banner','xs'))}})"></div>
                @else 
                <div class="top-restaurant-banner"  style="background-image: url({{url($restaurant->mainlogo)}})"></div>

                @endif
                <a href="{{url('restaurants/mapview')}}"   class="ion ion-android-arrow-back back-btn" data-toggle="tooltip" data-placement="top" title="Back to Listing" data-original-title="Go Back"></a>
                <div class="top-restaurant-info">
                    <div class="container">
                        <h3>{{$restaurant->name}}</h3>
                        <div class="working-hours-block dropdown">
                            <button class="btn dropdown-toggle" type="button" id="wHours" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                @if(isset($restaurant->timings[$weekday]))
                                @php  $i =0 @endphp
                                @foreach($restaurant->timings[$weekday] as $key => $val)
                                @php  ($i++) @endphp
                                @if($i>1)
                                <span class="hours">...</span>
                                @break
                                @endif
                                    @if($val->opening != 'off' && $val->opening != 'closing')

                                        <span class="hours"> Hours:{{date('g:i a',strtotime($val->opening))}} - {!!date('g:i a',strtotime($val->closing))!!}</span>
                                    @endif

                                @endforeach
                                @else
                                -
                                @endif
                                
                                <i class="ik ik-chevron-down"></i></button>
                            <div class="dropdown-menu" aria-labelledby="wHours">

                                @foreach($weekMap as $wkey => $wval)
	                    	 @if(isset($restaurant->timings[$wkey]))
                             <p> {{$wval}} :
                                @foreach($restaurant->timings[$wkey] as $val)

	                                @if($val->opening== 'off' || $val->closing == 'off')
	                                    Closed <br>
	                                    @else
	                                     {{date('h:i a',strtotime($val->opening))}} - {!!date('h:i
	                                    a',strtotime($val->closing))!!}</p>
	                                @endif
	                            @endforeach
                            @endif
                            @endforeach

                               
                            </div>
                        </div>
                        <!-- <div class="pickup-delivery-wrap">
                            <div class="pickup-delivery-item">
                                <div class="radio-block">
                                    <div class="radio-item">
                                        <input type="radio" id="pickup" name="del_type" checked="">
                                        <label for="pickup">Pickup</label>
                                    </div>
                                    <div class="radio-item">
                                        <input type="radio" id="delivery" name="del_type">
                                        <label for="delivery">Delivery</label>
                                    </div>
                                </div>
                            </div>
                            <div class="pickup-delivery-item-asap">
                                <button class="btn" type="button" data-toggle="modal" data-target="#scheduleModal"><i class="ik ik-clock"></i> ASAP</button>
                            </div>
                        </div> -->
                        <div class="pickup-delivery-unavailable-wrap">
                            <div id="search_phrase">

                            @if($restaurant->published == 'Published')
                                                    @if($restaurant->delivery == 'Yes')
                                                    <h4> Take out or Delivery</h4>

                                                    @else
                                                    <h4>Takeout</h4>

                                                    @endif
                                                @else
                                                <h4>Online Ordering Unavailable</h4>




                                                @endif


                            </div>

                            @if($restaurant->published != 'Published')
                            <a href="#" class="btn btn-theme">Vote to Add</a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal schedule-modal fade" id="scheduleModal" tabindex="-1" role="dialog" aria-labelledby="scheduleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="scheduleModalLabel">Select Time</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ik ik-x"></i></button>
                        </div>
                        <div class="modal-body">
                            <p>Today’s Hours
                                @if(isset($restaurant->timings[$weekday]))
                                @php
                                   $resultstr = [];
                                @endphp
                                @foreach($restaurant->timings[$weekday] as $key => $val)
                                    @if($val->opening != 'off' && $val->opening != 'off')
                                        @php
                                           $resultstr[] = date('g:i a',strtotime($val->opening)).' - '.date('g:i a',strtotime($val->closing))
                                        @endphp
                                    @endif
                                @endforeach
                                @php
                                    echo implode(" & ",$resultstr);
                                @endphp
                                @else

                                @endif
                            </p>
                            <div class="schedule-modal-radio-buttons">
                                <div class="radio-item">
                                    <input type="radio" id="asap" name="delivery_time" checked="">
                                    <label for="asap">ASAP</label>
                                </div>
                                <div class="radio-item">
                                    <input type="radio" id="schedule_order" name="delivery_time">
                                    <label for="schedule_order">Schedule for later</label>
                                </div>
                            </div>

                            <div class="schedule-inner-block" id="schedule_order_block" style="display: none;">
                                <div class="form-group">
                                    <p>Select Date</p>
                                    <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                        <div class="date-item active" data-date="Thu 05-Dec" data-val="2019-12-05">
                                            <span class="day">Thu</span>
                                            <span class="date">05</span>
                                        </div>
                                        <div class="date-item " data-date="Fri 06-Dec" data-val="2019-12-06">
                                            <span class="day">Fri</span>
                                            <span class="date">06</span>
                                        </div>
                                        <div class="date-item " data-date="Sat 07-Dec" data-val="2019-12-07">
                                            <span class="day">Sat</span>
                                            <span class="date">07</span>
                                        </div>
                                        <div class="date-item " data-date="Sun 08-Dec" data-val="2019-12-08">
                                            <span class="day">Sun</span>
                                            <span class="date">08</span>
                                        </div>
                                        <div class="date-item " data-date="Mon 09-Dec" data-val="2019-12-09">
                                            <span class="day">Mon</span>
                                            <span class="date">09</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Select Time</p>
                                    <select class="form-control" id="sch_time">
                                        <option value="">Select a Time</option>
                                        <option>12:00 AM</option>
                                        <option>12:15 AM</option>
                                        <option>12:30 AM</option>
                                        <option>12:45 AM</option>
                                        <option>01:00 AM</option>
                                        <option>01:15 AM</option>
                                        <option>01:30 AM</option>
                                        <option>01:45 AM</option>
                                        <option>02:00 AM</option>
                                        <option>02:15 AM</option>
                                        <option>02:30 AM</option>
                                        <option>02:45 AM</option>
                                        <option>03:00 AM</option>
                                        <option>03:15 AM</option>
                                        <option>03:30 AM</option>
                                        <option>03:45 AM</option>
                                        <option>04:00 AM</option>
                                        <option>04:15 AM</option>
                                        <option>04:30 AM</option>
                                        <option>04:45 AM</option>
                                        <option>05:00 AM</option>
                                        <option>05:15 AM</option>
                                        <option>05:30 AM</option>
                                        <option>05:45 AM</option>
                                        <option>06:00 AM</option>
                                        <option>06:15 AM</option>
                                        <option>06:30 AM</option>
                                        <option>06:45 AM</option>
                                        <option>07:00 AM</option>
                                        <option>07:15 AM</option>
                                        <option>07:30 AM</option>
                                        <option>07:45 AM</option>
                                        <option>08:00 AM</option>
                                        <option>08:15 AM</option>
                                        <option>08:30 AM</option>
                                        <option>08:45 AM</option>
                                        <option>09:00 AM</option>
                                        <option>09:15 AM</option>
                                        <option>09:30 AM</option>
                                        <option>09:45 AM</option>
                                        <option>10:00 AM</option>
                                        <option>10:15 AM</option>
                                        <option>10:30 AM</option>
                                        <option>10:45 AM</option>
                                        <option>11:00 AM</option>
                                        <option>11:15 AM</option>
                                        <option>11:30 AM</option>
                                        <option>11:45 AM</option>
                                        <option>12:00 PM</option>
                                        <option>12:15 PM</option>
                                        <option>12:30 PM</option>
                                        <option>12:45 PM</option>
                                        <option>01:00 PM</option>
                                        <option>01:15 PM</option>
                                        <option>01:30 PM</option>
                                        <option>01:45 PM</option>
                                        <option>02:00 PM</option>
                                        <option>02:15 PM</option>
                                        <option>02:30 PM</option>
                                        <option>02:45 PM</option>
                                        <option>03:00 PM</option>
                                        <option>05:00 PM</option>
                                        <option>05:15 PM</option>
                                        <option>05:30 PM</option>
                                        <option>05:45 PM</option>
                                        <option>06:00 PM</option>
                                        <option>06:15 PM</option>
                                        <option>06:30 PM</option>
                                        <option>06:45 PM</option>
                                        <option>07:00 PM</option>
                                        <option>07:15 PM</option>
                                        <option>07:30 PM</option>
                                        <option>07:45 PM</option>
                                        <option>08:00 PM</option>
                                        <option>08:15 PM</option>
                                        <option>08:30 PM</option>
                                        <option>08:45 PM</option>
                                        <option>09:00 PM</option>
                                        <option>09:15 PM</option>
                                        <option>09:30 PM</option>
                                        <option>09:45 PM</option>
                                        <option>10:00 PM</option>
                                    </select>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $(document).ready(function(){
                    $('input[name="delivery_time"]').click(function() { 
                        if($(this).attr('id') == 'schedule_order') {
                            $('#schedule_order_block').show();
                        } else {
                            $('#schedule_order_block').hide();
                        }
                    });
                    var schDateitem = $("#sch_DateWrap .date-item");
                    $(schDateitem).on('click', function() { 
                        var c_Date = $(this).attr("data-date");
                        var c_Date1 = $(this).attr("data-val");
                        $("#sch_DateWrap .date-item").removeClass("active");
                        $(this).addClass("active");
                        if(c_Date == "Thu 05-Dec") {
                            $("#del_date_v").html('Today')
                        }
                        else{
                            $("#del_date_v").html(c_Date);
                            $("#del_type_v").hide();
                            $("#deltypet_wrap").show();
                            $('#scedule_edit').show();
                        }
                    });
                    $('#sch_time').on('change', function() { 
                        $("#deltypet_wrap").show();
                        $("#del_type_v").hide();
                        $("#del_date_v").show();
                        var c_Time = $(this).find(":checked").val(); 
                        var c_Date = $(this).attr("data-date");
                        $("#del_date_v").html(c_Date)
                        $("#del_time_v").html(c_Time);
                    });
                });
            </script>
             @php
             $user = json_decode(json_encode(user()), true);
             if($user == null){
                 $user=[];
             }
             @endphp

            <nav class="bottom-nav-mobile">
                @if(Cart::count() != 0)
                <div class="checkout-button-wrap">
                    {{-- <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme btn-block">Checkout Now</a> --}}
                    @if(array_key_exists('is_app',$user))
                    <div class="row">
                        <a style="width: 70%;" href="{{trans_url('cart/checkout')}}" class="btn btn-theme">Checkout Now</a>
                         <a style="width: 30%;color:#ffff;background-color: #ed3833;" href="{{ env('REST_APP_URL').'/client/app/home/'.$user['restaurant_id'] }}" class="btn">Back</a>
                        </div>
                    @else
                     <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme btn-block">Checkout Now</a>
                    @endif
                </div>
                @else
                <div class="checkout-button-wrap" id="mobile_chk_btn" style="display: none;">
                    {{-- <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme btn-block">Checkout Now</a> --}}
                    @if(array_key_exists('is_app',$user))
                    <div class="row">
                        <a style="width: 70%;" href="{{trans_url('cart/checkout')}}" class="btn btn-theme">Checkout Now</a>
                         <a style="width: 30%;color:#ffff;background-color: #ed3833;" href="{{ env('REST_APP_URL').'/client/app/home/'.$user['restaurant_id'] }}" class="btn">Back</a>
                        </div>
                    @else
                     <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme btn-block">Checkout Now</a>
                    @endif
                </div>
                <div class="checkout-button-wrap" id="mobile_chk_txt" >
                    <div class="alert  alert-dismissible m-0" style="border: 2px solid #40b659;">
                      <a href="#" class="close" data-dismiss="alert" aria-label="close" style="color: black;">&times;</a>
                      <center><strong style="color: black; font-size: 15px;" > Order Now to have your food ready when you arrive.</strong>
                        
                    </center>
                    </div>
                    @if(array_key_exists('is_app',$user))
                        <div class="row" class="text-center">
                             <a class="text-center" style="width: 100%;color:#ffff;background-color: #ed3833;" href="{{ env('REST_APP_URL').'/client/app/home/'.$user['restaurant_id'] }}" class="btn">Back</a>
                        </div>
                    @endif
            </div>
                <div class="nav-link-item logo">
                    <a href="{{url('/')}}">
                        <img src="{!!url('assets/img/logo-round-big.png')!!}" alt="logo" title="Zing My Order">
                    </a>
                </div>
                <div class="nav-link-item">
                    <!--<a href="javascript:void(0);" id="location_change_mob">-->
                    <!--    <i class="ik ik-map-pin"></i>-->
                    <!--    <span>{{!empty(Session::get('location')) ? strtok(Session::get('location'),',') : 'Address' }}</span>-->
                    <!--</a>-->
                </div>
                <div class="nav-link-item sign-in">
                     @if(!Auth::user())
                    <a  href="#signIn_signUp_Modal" data-toggle="modal" data-target="#signIn_signUp_Modal"><img src="{{theme_asset('img/user.svg')}}" alt=""></a>
                    @elseif(Auth::user())
                    <a href="{{url('client/')}}">
                        <img src="{{Auth::user()->picture}}" onerror="if (this.src != '{{url('themes/client/assets/img/avatar/male.png')}}') this.src = '{{url('themes/client/assets/img/avatar/male.png')}}';" >
                    </a>
                    @endif
                </div>
                @endif
            </nav>

            <div class="single-listing-wrap" id="singleRestWrap">
                <div class="single-menu-items-wrap listing-wrap p-0">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-lg-2 col-xl-2 menu-col-wrap">
                                <div class="menu-col-wrap-inner">
                                    <div class="man-menu-items-nav">
                                        <div class="menu-items-nav-inner">
                                            <ul class="nav-menu" id="manuNav">
                                                @foreach($restaurant->category as $category)
                                                <li class="menu-item"><a href="#{{$category->id}}" class="menu-link roll">{{$category->name}}</a></li>
                                                @endforeach
                                                @if(!empty($restaurant->catering_category) && count($restaurant->catering_category) > 0)
                                                <div class="divider"></div>
                                                <a href="#" class="menu-link roll" id="cater_heading" style="color: #333; padding-right: 20px; font-size: 15px; font-weight: 700;">Catering </a>
                                                @foreach($restaurant->catering_category as $key => $category)
                                                <li class="menu-item {{(Session::get('category_name')== 'Catering' && $key==0) ? 'active' : ''}}"><a href="#cater_{{$category->id}}" class="menu-link roll">{{$category->name}}</a></li>
                                                @endforeach
                                                @endif
                                                <div class="divider"></div>
                                                <li class="menu-item"><a href="#about" class="menu-link roll text-success">About Us</a></li>
                                                <li class="menu-item"><a href="#gallery" class="menu-link roll text-success">Gallery</a></li>
                                                <li class="menu-item"><a href="#timing" class="menu-link roll text-success">Hours</a></li>
                                                <li class="menu-item"><a href="#offer" class="menu-link roll text-success">Offers</a></li>
                                                <li class="menu-item"><a href="#review" class="menu-link roll text-success">Reviews</a></li>
                                            </ul>
                                           <!--  <div class="offer-block mt-20">
                                                <img src="{{url($restaurant->defaultImage('offer','original'))}}" class="img-fluid" alt="">
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-5 col-xl-7">
                                <div class="responsive-search-menu-wrap"  style="display: none">
                                    <div class="cat-title-btn-wrap">
                                        <!--<button type="button" data-toggle="collapse" data-target="#resposive_menu_cat" aria-expanded="false" aria-controls="resposive_menu_cat" class="btn cat-title-btn">-->
                                        <!--    <span id="res_cat_title">Popular Dishes</span> <i class="ik ik-more-vertical" style="float: right; margin-top: 5px;"></i>-->
                                        <!--</button>-->
                                        <!--<div class="buttons-block">-->
                                        <!--    <button type="button" data-toggle="collapse" data-target="#resposive_search" aria-expanded="false" aria-controls="resposive_search" class="btn ik ik-search mobile-search-btn"></button>-->
                                        <!--</div>-->
                                        <div class="mobile-dish-search mt-0">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                            <input type="text" class="form-control"   placeholder="Search Food...">
                                            <button type="button" id="dish_quick_clear" class="clear-btn fa fa-times-circle"></button>
                                        </div>
                                    </div>
                                    
                                    <div class="collapse" id="resposive_menu_cat">
                                        <div class="mobile-dish-menu">
                                            <h3>Menu</h3>
                                            <ul class="nav-menu" id="manuNav">
                                                 @foreach($restaurant->category as $category)
                                                <li class="menu-item active"><a href="#{{$category->id}}" class="menu-link res-menu-scroll">{{$category->name}}</a></li>
                                                @endforeach
                                                @if(!empty($restaurant->catering_category) && count($restaurant->catering_category) > 0)
                                                <div class="divider"></div>
                                                <a href="#" class="menu-link res-menu-scroll" id="cater_heading" style="color: #333; font-size: 15px; font-weight: 700;">Catering </a>
                                                @foreach($restaurant->catering_category as $key => $category)
                                                <li class="menu-item {{(Session::get('category_name')== 'Catering' && $key==0) ? 'active' : ''}}"><a href="#cater_{{$category->id}}" class="menu-link res-menu-scroll">{{$category->name}}</a></li>
                                                @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                                <div class="menu-items-wrap-inner listing-wrap" id="accordionMenu">
                                    @if(!empty($restaurant->offers))
                                    <div class="menu-items-section content-section" id="selected">
                                        <h3 class="title">Offers</h3>
                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <a class="dish-item offer-item" style="cursor: auto;" >
                                                        <div class="dish-description">
                                                            <!-- <h3 class="dish-title">{{$restaurant->offers->name,10}}</h3> -->
                                                            @if($restaurant->offers->type == 'Price')
                                                            <h3 class="dish-title">${{number_format($restaurant->offers->value,0)}} Off on all Orders</h3>
                                                            @else
                                                            <h3 class="dish-title">{{number_format($restaurant->offers->value,0)}}% Off on all Orders</h3>
                                                            @endif
                                                            <p>Apply at Check out</p>
                                                            <div class="dish-metas">
                                                                <span>Valid: {{date('d-m-Y', strtotime($restaurant->offers->start_date))}}</span>
                                                                <span>to {{date('d-m-Y', strtotime($restaurant->offers->end_date))}}</span>
                                                                <!-- @if($restaurant->offers->type == 'Price')
                                                                <span>$ {{$restaurant->offers->value}}</span>
                                                                @else
                                                                <span>{{$restaurant->offers->value}} %</span>
                                                                @endif -->
                                                            </div>
                                                         </div>
                                                         <div class="dish-image">
                                                            <!-- @if(!empty($restaurant->offers->image))
                                                            <img data-src="{{url($restaurant->offers->defaultImage('image','md'))}}" class="img-fluid lazy" alt="">
                                                            @else
                                                            <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                            @endif -->
                                                            @if($restaurant->offers->type == 'Price')
                                                            <h3>${{number_format($restaurant->offers->value,0)}}<span>Off</span></h3>
                                                            @else
                                                            <h3>{{number_format($restaurant->offers->value,0)}}%<span>Off</span></h3>
                                                            @endif
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    @if (Session::has('carterror'))
                                    <div class="alert alert-danger alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <span><b> Error - </b> {{ Session::get('carterror') }}</span>
                                    </div>
                                    @endif
                                    @if(!empty(Session::get('selected_masters[]')) && count(Restaurant::getSessionMenus(Session::get('selected_masters[]'),$restaurant->id))>0)
                                    <div class="menu-items-section content-section" id="selected">
                                        <h3 class="title">Selected Dishes</h3>
                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                @foreach(Restaurant::getSessionMenus(Session::get('selected_masters[]'),$restaurant->id) as $selected_menu)
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <div class="dish-item" data-slug="{{$selected_menu->id}}"  onclick="modalopen('{{$menu->id}}')">
                                                        <div class="dish-description">
                                                            <h3 class="dish-title"><a href="#!" class="someclass">{{$selected_menu->name,10}}</a></h3>

                                                            <p>{{str_limit($selected_menu->description, 30)}}</p>
                                                            <div class="dish-metas">
                                                                <!--<span class="mr-15"><i class="fa fa-star" style="color: #f6af00;"></i>{{$selected_menu->review}}</span>-->
                                                                <span class="price mr-15">${{$selected_menu->price}}</span>
                                                                <span><i class="ion-android-restaurant"></i>@if(!empty($selected_menu->serves)) {{$selected_menu->serves}} @else 1 @endif</span>
                                                                
                                                            </div>
                                                            <!--
                                                            @if($restaurant->published == 'Published')
                                                                <a class="dish-add-btn" href="#{{$selected_menu->slug}}" data-toggle="modal" data-target="#{{$selected_menu->slug}}" data-slug="{{$selected_menu->id}}">Add</a>
                                                            @endif
                                                            -->
                                                        </div>

                                                        <div class="dish-image">
                                                            @if(!empty($selected_menu->image))
                                                            <img data-src="{{url($selected_menu->defaultImage('image','xs'))}}" class="img-fluid lazy" alt="">
                                                            @else
                                                                 @if(!empty(@$selected_menu->master->image))
                                                                <img data-src="{{url(@$selected_menu->master->defaultImage('image','xs'))}}" class="img-fluid lazy" alt="">
                                                                @elseif(!empty(url($restaurant->mainlogo)))
                                                                 <img src="{{url($restaurant->mainlogo)}}" class="img-fluid" alt="">
                                                                @else
                                                                <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                                @endif
                                                            @endif
                                                        </div>
                                                        <!-- <div class="dish-price">${{$selected_menu->price}}</div> -->
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endif

                                    @if(count($restaurant->popular_category) > 0)
                                    <div class="menu-items-section content-section" id="popular">

                                        <h3 class="title" data-title="Popular Dishes">Popular Dishes</h3>

                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                @foreach($restaurant->popular_category as $menu)
                                                @if($menu->popular_dish_no != 0)
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <div class="dish-item"  data-slug="{{$menu->id}}" onclick="modalopen('{{$menu->id}}')">
                                                    <!-- <a class="dish-item add_to_cart"> -->
                                                        <div class="dish-description">
                                                            <h3 class="dish-title"><a href="#!" class="someclass">{{$menu->name}}</a></h3>
                                                            <p>{{str_limit($menu->description, 30)}}</p>
                                                            <div class="dish-metas">
                                                                <!--<span class="mr-15"><i class="fa fa-star" style="color: #f6af00;"></i>{{$menu->review}}</span>-->
                                                                <span class="price mr-15">${{$menu->price}}</span>
                                                                <span><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
                                                                
                                                            </div>
                                                            <!--
                                                            @if($restaurant->published == 'Published')
                                                                <a class="dish-add-btn" href="#{{$menu->slug}}" data-toggle="modal" data-target="#{{$menu->slug}}" data-slug="{{$menu->id}}"><i class="ik ik-shopping-bag"></i>Add</a>
                                                            @endif
                                                            -->
                                                        </div>

                                                        <div class="dish-image">
                                                            @if(!empty($menu->image))
                                                            <img data-src="{{url($menu->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                            @else
                                                                @if(!empty(@$menu->master->image))
                                                                <img data-src="{{url(@$menu->master->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                                @elseif(!empty(url($restaurant->mainlogo)))
                                                                 <img src="{{url($restaurant->mainlogo)}}" class="img-fluid" alt="">
                                                                @else
                                                                <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                                @endif
                                                            @endif
                                                        </div>
                                                        <!-- <div class="dish-price">${{$menu->price}}</div> -->
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                  <div id="search_result_div">
                                    @foreach($restaurant->category as $category)
                                    <div class="menu-items-section content-section" id="{{$category->id}}">

                                        <h3 class="title" data-title="{{$category->name}}">{{$category->name}}</h3>

                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                @if(isset($restaurant->menu[$category->id]))
                                                @foreach($restaurant->menu[$category->id] as $menu)
                                                @if($menu->catering != 'Yes')
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <div class="dish-item" data-slug="{{$menu->id}}" onclick="modalopen('{{$menu->id}}')">
                                                    <!-- <a class="dish-item add_to_cart"> -->
                                                        <div class="dish-description">
                                                            <h3 class="dish-title"><a href="#!" class="someclass">{{$menu->name}}</a></h3>
                                                            <p>{{str_limit($menu->description, 30)}}</p>
                                                            <div class="dish-metas">
                                                                <!--<span class="mr-15"><i class="fa fa-star" style="color: #f6af00;"></i>{{$menu->review}}</span>-->
                                                                <span class="price mr-15">${{$menu->price}}</span>
                                                                <span><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
                                                                
                                                            </div>
                                                            <!--
                                                            @if($restaurant->published == 'Published')
                                                                <a class="dish-add-btn" href="#{{$menu->slug}}" data-toggle="modal" data-target="#{{$menu->slug}}" data-slug="{{$menu->id}}"><i class="ik ik-shopping-bag"></i>Add</a>
                                                            @endif
                                                            -->
                                                        </div>

                                                        <div class="dish-image">
                                                            @if(!empty($menu->image))
                                                            <img data-src="{{url($menu->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                            @else
                                                                @if(!empty(@$menu->master->image))
                                                                <img data-src="{{url(@$menu->master->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                                @elseif(!empty(url($restaurant->mainlogo)))
                                                                 <img src="{{url($restaurant->mainlogo)}}" class="img-fluid" alt="">
                                                                @else
                                                                <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                                @endif
                                                            @endif
                                                        </div>
                                                        <!-- <div class="dish-price">${{$menu->price}}</div> -->
                                                    </div>

                                                </div>
                                                @endif
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                    <div class="card menu-cat-card" id="menu_{{$category->id}}">
                                        <div class="card-header">
                                            <h4 class="mb-0 menu-link" data-target="menu_{{$category->id}}">{{$category->name}} <span class="item-count">
                                                @if(isset($restaurant->menu[$category->id]))
                                                {{count($restaurant->menu[$category->id]->where('catering', '<>', 'Yes'))}}
                                                @endif
                                                Items</span></h4>
                                        </div>


                                        <div class="menu-cat-card-content" data-id="menu_{{$category->id}}">
                                            <div class="menu-cat-card-close-link" data-target="menu_{{$category->id}}"></div>
                                            <div class="card-body">
                                                @if(isset($restaurant->menu[$category->id]))
                                                @foreach($restaurant->menu[$category->id] as $menu)
                                                @if($menu->catering != 'Yes')
                                                <div class="dish-item" data-slug="{{$menu->id}}" onclick="modalopen('{{$menu->id}}')">
                                                    <div class="dish-description">
                                                        <h3 class="dish-title"><a href="#!" class="someclass">{{$menu->name}}</a></h3>
                                                        
                                                        <p>{{str_limit($menu->description, 30)}}</p>
                                                        <div class="dish-metas">
                                                            <span class="mr-15">${{$menu->price}}</span>
                                                            <!--<span class="mr-15"><i class="fa fa-star" style="color: #f6af00;"></i>{{$menu->review}}</span>-->
                                                            <span><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>
                                                        </div>
                                                        <!--@if($restaurant->published == 'Published')-->
                                                        <!--    <a class="dish-add-btn" href="#{{$menu->slug}}" data-toggle="modal" data-target="#{{$menu->slug}}" data-slug="{{$menu->id}}"><i class="ik ik-shopping-bag"></i>Add</a>-->
                                                        <!--@endif-->
                                                    </div>

                                                    <div class="dish-image">
                                                        @if(!empty($menu->image))
                                                        <img data-src="{{url($menu->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                        @else
                                                            @if(!empty(@$menu->master->image))
                                                            <img data-src="{{url(@$menu->master->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                             @elseif(!empty(url($restaurant->mainlogo)))
                                                             <img src="{{url($restaurant->mainlogo)}}" class="img-fluid" alt="">
                                                            @else
                                                            <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>

                                                @endif
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>


                                    </div>


                                    @endforeach

                                  </div>

                                    @foreach($restaurant->catering_category as $category)
                                    <div class="menu-items-section content-section" id="cater_{{$category->id}}">

                                        <h3 class="title">{{$category->name}}</h3>

                                        <div class="favorites-food-wrap">
                                            <div class="dish-listing-wrap dish_listing_card row gutter-10">
                                                @foreach($restaurant->menu[$category->id] as $menu)
                                                @if($menu->catering == 'Yes')
                                                <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-6 dish-grid-item">
                                                    <div class="dish-item"  data-slug="{{$menu->id}}" onclick="modalopen('{{$menu->id}}')">
                                                        <div class="dish-description">
                                                            <h3 class="dish-title"><a href="#!" class="someclass">{{$menu->name,10}}</a></h3>
                                                            <p>{{str_limit($menu->description, 30)}}</p>
                                                            <div class="dish-metas">
                                                                <!--<span class="mr-15"><i class="fa fa-star" style="color: #f6af00;"></i>{{$menu->review}}</span>-->
                                                                <span><i class="ion-android-restaurant"></i>@if(!empty($menu->serves)) {{$menu->serves}} @else 1 @endif</span>

                                                                <!--<span class="ml-15"><a href="#{{$menu->slug}}" onclick="addonModel('{{$menu->id}}')" data-toggle="modal" data-target="#{{$menu->slug}}"><i class="ik ik-shopping-bag"></i></a></span>-->
                                                            </div>
                                                            <!--
                                                             @if($restaurant->published == 'Published')
                                                                    <a class="dish-add-btn" href="#{{$menu->slug}}"  data-toggle="modal" data-target="#{{$menu->slug}}" data-slug="{{$menu->id}}"><i class="ik ik-shopping-bag"></i>Add</a>
                                                             @endif
                                                             -->
                                                        </div>

                                                        <div class="dish-image">
                                                            @if(!empty($menu->image))
                                                            <img data-src="{{url($menu->defaultImage('image','xs'))}}" class="img-fluid lazy" alt="">
                                                            @else
                                                                @if(!empty(@$menu->master->image))
                                                                <img data-src="{{url(@$menu->master->defaultImage('image','xs'))}}"  class="img-fluid lazy" alt="">
                                                                @elseif(!empty(url($restaurant->mainlogo)))
                                                                 <img src="{{url($restaurant->mainlogo)}}" class="img-fluid" alt="">
                                                                @else
                                                                <img src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="img-fluid" alt="">
                                                                @endif
                                                            @endif
                                                        </div>
                                                        <!-- <div class="dish-price">${{$menu->price}}</div> -->
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    <div class="about-wrap content-section" id="about">
                                        <div class="about-inner-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="title">About {{$restaurant->name}}</h3>
                                                    <p>{{$restaurant->description}}</p>
                                                    <div class="info-wrap">
                                                         <div id="map_canvas" style="height: 300px;width: 100%; margin-bottom: 30px">
                                                         <img style="height: 300px;width: 100%; margin-bottom: 30px" id="map_load" src="{{url('img/map.jpg')}}">
                                                         </div>
                                                        <p><b>{{$restaurant->address}}</b> </p>
                                                        @if(!empty($restaurant->phone))<p class="phone-no">Phone : {{substr($restaurant->phone, 0,3)}}-{{substr($restaurant->phone, 3,3)}}-{{substr($restaurant->phone, 6,4)}} </p>@endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="about-wrap content-section" id="gallery">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3 class="title">Gallery! </h3>
                                                @if(!empty($restaurant->images) || !empty($restaurant->photos))
                                                <div class="popup-gallery" id="lightgallery">
                                                    @if(!empty($restaurant->images))
                                                    @foreach($restaurant->getImages('gallery','lg') as $key=> $image)
                                                    <a href="{!!url($restaurant->defaultImage('gallery' ,'lg',$key))!!}">
                                                        <span class="gallery-img" style="background-image: url('{!!url($restaurant->defaultImage('gallery' ,'xs',$key))!!}')"></span></a>
                                                    @endforeach
                                                    @endif
                                                    @if(!empty($restaurant->photos))
                                                    @foreach($restaurant->photos as  $photo)
                                                    <a href="{!!$photo!!}">
                                                        <span class="gallery-img" style="background-image: url('{!!$photo!!}')"></span></a>
                                                    @endforeach
                                                    @endif
                                                </div>
                                                @else
                                                    There are no Pictures at this time
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="about-wrap content-section" id="timing">
                                        <div class="about-inner-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h3 class="title">Hours</h3>
                                                    <img src="img/home-salad-banner1.png" class="img-fluid" alt="">
                                                    <div class="card">
                                                        <div class="card-body">
                                                            @foreach($weekMap as $wkey => $wval)
                                                            <div class="row no-gutters">
                                                                <div class="col-md-4"><p>{{$wval}}</p></div>
                                                                <div class="col-md-8">
                                                                    @if(isset($restaurant->timings[$wkey]))
                                                                    @foreach($restaurant->timings[$wkey] as $val)
                                                                            <p class="m-0">
                                                                                @if($val->opening== 'off' || $val->closing == 'off')
                                                                                    Closed
                                                                                @else
                                                                                    {{date('h:i a',strtotime($val->opening))}} - {!!date('h:i a',strtotime($val->closing))!!}
                                                                                @endif
                                                                            </p>
                                                                    @endforeach
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="about-wrap content-section" id="offer">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3 class="title">Offers </h3>
                                                @if(!empty($restaurant->offer))
                                                <div class="popup-gallery" id="lightgallery">
                                                    @foreach($restaurant->getImages('offer','lg') as $key=> $image)
                                                    <a href="{!!url($restaurant->defaultImage('offer' ,'lg',$key))!!}">
                                                        <span class="gallery-img" style="background-image: url('{!!url($restaurant->defaultImage('offer' ,'xs',$key))!!}')"></span></a>
                                                    @endforeach
                                                </div>
                                                @else
                                                    There are no offers at this time
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="review-wrap content-section" id="review">
                                        <h3 class="title">Reviews for {{$restaurant->name}}</h3>
                                        <div class="widget-wraper">
                                            <div class="review-header">
                                                <h2>{{$restaurant->rating}}<span>/5</span></h2>
                                                <span class="raty" data-score="{{$restaurant->rating}}"></span>
                                                <p>{{$restaurant->review->count()}} Reviews</p>
                                            </div>
                                            
                                            @if(Auth::User())
                                            <div class="post-form-area">
                                                <h4>Post Your Review</h4>
                                                {!!Form::vertical_open()
                                                -> class('form-comment')
                                                -> method('POST') 
                                                -> action('review/post/'.$restaurant->id)!!}
                                                
                                                <div class="form-group">
                                                    {!!Form::textarea('comment')
                                                    ->placeholder('Your comment')
                                                    ->required()
                                                    ->forceValue('')
                                                    ->rows(4)
                                                    ->raw()!!}
                                                </div>
                                                <div class="form-group">
                                                    <label for="rate" class="mr-5">Your Rating </label>  
                                                    <span id="rate" class="rating" data-score="4"></span>
                                                </div>
                                                <input type="hidden" name="restaurant_id" value="{{$restaurant['id']}}">
                                                <button type="submit" id="btn_add_review" class="btn btn-theme" onclick="add_review()">Post Review</button>
                                                {!!Form::close()!!}  
                                            </div>
                                            @else
                                            <div class="alert alert-info mb-5"><span style="color: #444;">Please  <a href="{{url('review/add')}}/{{$restaurant->slug}}">Sign in</a>  to Post Reviews and Ratings</span></div>
                                            @endif
                                    <div style="margin-top:50px" id="load_data"></div>

                                   <div id="load_data_message"></div>


                                 
                                        </div>
                                    </div> 




                                </div>
                            </div>

                            <div class="col-md-4 col-lg-4 col-xl-3 eatery-cart-wrap">
                                <div class="sticky-spacer" id="cart_items_side">
                                    <!-- <div class="pickup-delivery-wrap">
                                        <div class="pickup-delivery-item">
                                            <div class="radio-block">
                                                <div class="radio-item">
                                                    <input type="radio" id="pickup" name="del_type" checked="">
                                                    <label for="pickup">Pickup</label>
                                                </div>
                                                <div class="radio-item">
                                                    <input type="radio" id="delivery" name="del_type">
                                                    <label for="delivery">Delivery</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="pickup-delivery-item-asap">
                                            <button class="btn" type="button" data-toggle="modal" data-target="#scheduleModal"><i class="ik ik-clock"></i> ASAP</button>
                                        </div>
                                    </div> -->
                                    @if($restaurant->published == 'Published')
                                        @if(Cart::count() == 0)
                                        <div class="cart-inner-wrap empty-cart">
                                            <img src="{{theme_asset('img/empty-cart.png')}}" alt="">
                                            <h2>Your Cart is Empty</h2>
                                            <p>Looks like you haven't made your choice yet</p>
                                        </div>
                                        @else
                                        <div class="cart-inner-wrap sticky-div ">
                                            <h2>Cart <span>{{Cart::count()}} Item</span></h2>
                                            <p>( @if($restaurant->delivery == 'Yes')
                                                    @if($restaurant->delivery_charge == 0)
                                                        Take out or Delivery
                                                    @elseif($restaurant->delivery_charge != '')
                                                       Take out or Delivery
                                                    @endif
                                                @else
                                                    Take out
                                                @endif) <a href="#" class="text-success" data-toggle="tooltip" data-placement="top" title="Earn Zing Points with Each Order. Use your points towards a future order to save money.">Earn Points <i class="fa fa-exclamation-circle text-dark"></i></a></p>
                                            <hr>
                                            <div class="cart-item-wrap">
                                                @foreach(Cart::content() as $cart)
                                                <?php $variation = $cart->options->variation;?>
                                                <div class="cart-item" style="{{$cart->options['menu_addon'] == 'addon' ? 'margin-left: 20px' : ''}}">
                                                    <div class="item-name-price">
                                                        <h4>{{str_limit($cart->name,17)}}</h4>
                                                         @php
                                                                $addon_price = 0;
                                                                    if(!empty($cart->options['addons'])){
                                                                foreach($cart->options['addons'] as $cart_addon){

                                                                        $addon_price = $addon_price + $cart_addon[2];
                                                                }

                                                               }
                                                            @endphp
                                                        <div class="price">${{number_format($cart->price+$addon_price,2)}}</div>
                                                    </div>
                                                    @if(!empty($variation))<p>(Variation-{{$variation}})</p>@endif
                                                    @if(!empty($cart->options['addons']))
                                                        @foreach($cart->options['addons'] as $cart_addon)
                                                         <div class="item-addon"><span data-toggle="tooltip" data-placement="top" title="{{$cart_addon[1]}}">{{str_limit($cart_addon[1],17)}}</span></div>
                                                        @endforeach
                                                    @endif
                                                     @if($cart->options['menu_addon'] != 'addon')
                                                    <div class="item-qty-price">
                                                        <div class="qty-price-inner">
                                                            <span class="enumerator">
                                                                <span class="enumerator_btn js-minus_btn" id="minus_span" onclick="decrement('{{$cart->rowId}}')">-</span>
                                                                <span class="enumerator_input">{{$cart->qty}}</span>
                                                                <input type="hidden" value="{{$cart->qty}}" readonly="readonly">
                                                                <span class="enumerator_btn js-plus_btn" id="plus_span" onclick="increment('{{$cart->rowId}}')">+</span>
                                                            </span>

                                                            <span class="remove-btn"><a href="#" class="remove-btn" onclick="removeCartItem('{{$cart->rowId}}')"><i class="ion-android-cancel"></i></a></span>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </div>
                                                @endforeach
                                            </div>
                                            <div class="cart-summary-wrap mt-20">
                                                <div class="summary-item">
                                                    <div class="summary-text">Subtotal</div>
                                                    <div class="summary-price">${{number_format(Cart::total(),2)}}</div>
                                                </div>
                                                <!-- <div class="summary-extra">Extra charges may apply</div> -->
                                                <a class="btn btn-theme checkout-btn" href="{{url('cart/checkout')}}">Checkout Now</a>
                                                <!-- <a class="btn btn-theme checkout-btn" href="{{url('cart/checkout/guest')}}" id="guest_form">Checkout As Guest</a> -->

                                            </div>
                                        </div>
                                        @endif
                                    @else
                                        <div class="cart-inner-wrap vote-wrap">
                                            <h2>Take Out or Delivery Unavailable</h2>
                                            <p>Click on Vote to add & we'll convey your request for the restaurant to join Zing's *Free Platform to take orders online.</p>

                                            <div id="feedback">
                                                <div class="progress">
                                                    <div class="progress-bar bg-success" role="progressbar" @if($restaurant->votes != null) aria-valuenow="{{count($restaurant->votes) + 3}}" style="width:{{(count($restaurant->votes) + 3)*10}}%" @else  aria-valuenow="3" style="width:30%" @endif aria-valuemin="0" aria-valuemax="30" >
                                                        <span class="vote-count">@if($restaurant->votes != null) {{count($restaurant->votes) + 3}} @else 3 @endif</span>
                                                    </div>

                                                </div>
                                                <button type="button" id="vote_to_add" class="btn btn-theme">Vote to Add</button>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- <div class="single-tab-wrap">
                    <div class="tab-nav-wrap">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="about-tab" data-toggle="tab" href="#about" role="tab" aria-controls="about" aria-selected="true">About Restaurant</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="manu-tab" data-toggle="tab" href="#manu" role="tab" aria-controls="manu" aria-selected="false">Menu</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content-wrap">
                        <div class="container">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="about-tab">...</div>
                                <div class="tab-pane fade" id="manu" role="tabpanel" aria-labelledby="manu-tab">...</div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
<div class="modal fade menu-item-modal" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalTitle" aria-hidden="true">
     {!!Form::vertical_open()
    ->id('menu-cart-add')
    ->addClass('modal-dialog modal-dialog-scrollable modal-dialog-centered')
    ->action('/carts/save/')
    ->method('GET')!!}
        <div class="modal-content">
        <div class="modal-header">
         </div>
         <div class="modal-body">
         </div>
         <div class="modal-footer">
       </div>
        </div>
    {!!Form::close()!!}
</div>

<div class="modal video-modal fade bd-example-modal-lg" id="youtubemodel" tabindex="-1" role="dialog" aria-labelledby="youtubemodelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content"></div>
    </div>
</div>

<div class="modal fade login-modal location-modal" id="favouriteModel" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                                <div class="login-header text-center">
                                  <h5><b>Please Sign in to proceed</b></h5>
                                </div>

                                <div class="text-center" >
                                   <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" id="favourite_submit">Ok</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
            {!!Form::close()!!}
        </div>
    </div>
</div>
<div class="modal fade login-modal" id="signIn_signUp_Modal" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                            @include('notifications')
                            {!!Form::vertical_open()
->id('login')
->method('POST')
->action('client/login')!!}
                                <div class="login-header text-center">
                                    <h2>Sign in</h2>
                                    <p>New to Zing My Order? <a href="{{url('client/register')}}">Sign Up</a></p>
                                </div>
                                <div class="social-btn-wrap">
                                    <a href='/client/login/facebook' class="btn btn-block btn-facebook" ><i class="fa fa-facebook"></i> Sign in with Facebook</a>
                                                <a href='/client/login/google' class="btn btn-block btn-google" ><i class="fa fa-google"></i> Sign in with Google</a>
                                    <h3>OR<br><span class="login">Log in Using</span></h3>
                                </div>
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <i class="flaticon-user"></i>
                                    {!! Form::email('email')
    ->required()
    ->placeholder('Enter Your Email')
    ->raw() !!}
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <a href="#" class="forgot_pw">Forgot your password?</a>
                                    <i class="flaticon-key"></i>
                                    {!! Form::password('password')
                                   ->placeholder('Enter Your Password')
    ->required()->raw()!!}
                                </div>
                                <div class="text-center login-footer">
                                    <button type="submit" class="btn btn-theme">Sign In</button>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="error_model" tabindex="-1" role="dialog" aria-labelledby="error_modelLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body text-center" style="background-color: #c2c8ce;">
                <h5 id="msg" class="mb-20 text-white"></h5>
                <button type="button" class="btn btn-theme" style="width: 100px;" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade added-to-cart-modal" id="cartMessageModel" tabindex="-1" role="dialog" aria-labelledby="cartMessageModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <a href="{{url('/')}}" class="close cart_close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></a>
                <div class="inner-content">
                    <!-- <i class="ion ion-ios-checkmark"></i> -->
                    <h4 class="mt-20">Your dish has been added to the cart</h4>
                    <p>Continue to add more or proceed to checkout</p>
                    <button  data-dismiss="modal" class="btn btn-secondary cart_close mr-15">Continue</button>
                    <a href="{{trans_url('cart/checkout')}}" class="btn btn-theme search-btn">Checkout</a>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
<script type="text/javascript">


        var active = '<?php echo $restaurant_fav; ?>';
            var baseUrl ="{{url('/') }}";
            var transUrl ="{{trans_url('/') }}";
            var vote_restaurant_id = "{!!$restaurant->id!!}";
            var vote_restaurant_slug = "{!!$restaurant->slug!!}";
                $(function(){

     var map,myLatlng;
      @if(!empty($restaurant->latitude) && !empty($restaurant->longitude))
         myLatlng = new google.maps.LatLng({!! @$restaurant->latitude !!},{!! @$restaurant->longitude !!});
      @else
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
      @endif
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
         disableDefaultUI: true
         }
         $(document).on('click', '#map_load', function() {
        map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
        var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });
       })
         

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
    });
       })

</script>
<!-- Hotjar Tracking Code for https://zingmyorder.com/ -->
<script>
     
        $(document).ready(function(){
        $(".menu-cat-card .card-body").click( function(e) {
            e.stopPropagation();
        });
        $(".menu-cat-card .card-header .menu-link").on('click', function(e) {
            var menu_id = $(this).data('target');
            $(".menu-cat-card-content").each(function(){
                $(this).hide();
                if($(this).data('id') == menu_id) {
                    $(this).show();
                }
            });
            $('html, body').animate({scrollTop: $('#' + menu_id).offset().top -80 }, 0);
        });
        $(".menu-cat-card .menu-cat-card-close-link").on('click', function(e) {
            var menu_close_id = $(this).data('target');
            $(".menu-cat-card-content").each(function(){
                if($(this).data('id') == menu_close_id) {
                    $(this).hide();
                }
            });
        });

 var limit =3;
 var start = 0;
 load_country_data(limit,start);
 function load_country_data(limit, start)
 {
  $.ajax({
   url:"{{url('restaurants/reviews')}}/{!!$restaurant->slug!!}",
   method:"GET",
   data:{limit:limit, start:start},
   cache:false,
   success:function(data)
   {
    $('#load_data').append(data);
    if(data == '')
    {
     $('#load_data_message').html("<p class='no_data_msg'>No More Reviews.</p>");
     action = 'active';
    }
    else
    {
     $('#load_data_message').html("<button type='button' class='new_added btn btn-theme'>See More Reviews</button>");
     action = "inactive";
    }
   }
  });
 }
 $(document).on('click', '.new_added', function() {
    start = start+limit;
    limit =10;
    load_country_data(limit,start);
   // alert(limit+limit,2);
});
//  $("#load_data_message").scroll(function() {
//      alert('dd');
//     if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
//        alert('dsdsdsdd');   
//     } else {
//         alert('not');   
//     }
// });
 
 


    });

    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:1629504,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
</script>
<style type="text/css">
.no_data_msg{
    color:red;
}
.well {
    background-color: #f6f9fc;
    -webkit-box-shadow: none;
    box-shadow: none;
    min-height: 20px;
    padding: 19px;
    margin-bottom: 20px;
    background-color: #f5f5f5;
    border: 1px solid #e3e3e3;
    border-radius: 4px;
}
.main-wrap .main-header .header-search-short-info-wrap.visible {
    display: none;
}
.main-wrap .main-header .header-search-short-info-wrap::before {
    display: none;
}
.footer-social a {
    width: 30px;
    height: 30px;
    line-height: 28px;
    text-align: center;
    color: #fff;
    background-color: #444;
    display: inline-block;
    border-radius: 50%;
    margin-right: 5px;
}
.checkout-button-wrap {
    padding: 10px;
    background-color: #fff;
    width: 100%;
}
.bottom-nav-mobile .nav-link-item {
    display: none !important;
}
@media (max-width: 991px) {
    .main-wrap .single-listing-wrap {
        padding-top: 0px;
    }
}
.modal-content {
  min-height: 50%;
}
</style>
         {!!'<!-- ' . date("F Y h:i:s u") . '-->'!!}
