Lavalite package that provides restaurant management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `restaurant/restaurant`.

    "restaurant/restaurant": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Restaurant\Restaurant\Providers\RestaurantServiceProvider::class,

And also add it to alias

    'Restaurant'  => Restaurant\Restaurant\Facades\Restaurant::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Restaurant\\RestaurantTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Restaurant\Restaurant\Providers\RestaurantServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Restaurant\Restaurant\Providers\RestaurantServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Restaurant\Restaurant\Providers\RestaurantServiceProvider" --tag="view"


## Usage


