<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'restaurant',

    /*
     * Package.
     */
    'package'   => 'restaurant',

    /*
     * Modules.
     */
    'modules'   => ['restaurant', 
'addon', 
'category', 
'menu', 
'review'],

    'restaurant'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Restaurant::class,
            'table'                 => 'restaurants',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\RestaurantPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'name',  'description',  'address',  'phone',  'email',  'price_range',  'country',    'state', 'district',  'city', 'location',  'zipcode',  'latitude',  'longitude',  'working_hours',  'rating',  'logo',  'gallery',  'delivery', 'postmate_delivery', 'type',  'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/restaurant',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            
                'images'    => 'array',
                'file'      => 'array',
                'logo'      => 'array',
                'gallery'   => 'array',
                'working_hours' => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'address' => 'like',
                'delivery' => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Restaurant',
        ],

    ],

    'addon'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Addon::class,
            'table'                 => 'restaurant_addons',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\AddonPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'name',  'price',  'slug', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/addon',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Addon',
        ],

    ],

    'category'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Category::class,
            'table'                 => 'restaurant_categories',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\CategoryPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'parent_id',  'name',  'preparation_time',  'slug', 'user_id',  'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/category',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Category',
        ],

    ],

    'menu'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Menu::class,
            'table'                 => 'restaurant_menus',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\MenuPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'category_id',  'name',  'description',  'price',  'image',  'addons',  'slug',  'status',  'user_id', 'user_type', 'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/menu',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            
                'images'    => 'array',
                'file'      => 'array',
                'image'    => 'array',
                'addons'    => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Menu',
        ],

    ],

    'review'       => [
        'model' => [
            'model'                 => \Restaurant\Restaurant\Models\Review::class,
            'table'                 => 'restaurant_reviews',
            'presenter'             => \Restaurant\Restaurant\Repositories\Presenter\ReviewPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id', 'restaurant_id',  'customer_id',  'order_id',  'rating',  'review', 'user_id', 'user_type',  'created_at',  'deleted_at',  'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurant/review',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
                'json_user'    => 'array',
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Restaurant',
            'module'    => 'Review',
        ],

    ],
];
