<?php


// Resource routes  for restaurant
// Route::group(['prefix' => set_route_guard('web').'/restaurant'], function () {
    Route::prefix('{guard}/restaurant')->group(function () {

	Route::get('settings-save/{id}', 'RestaurantResourceController@restaurantStopStart');
	Route::get('timing/{count?}/{day?}', 'RestaurantResourceController@timing');
	Route::get('restaurant/publish/{key?}', 'RestaurantResourceController@publishRestauarant');
	Route::get('restaurant/loginas/{key?}', 'RestaurantResourceController@loginAs');
	Route::get('billing', 'RestaurantResourceController@billing');
	Route::get('view-settings', 'RestaurantResourceController@settings');
	Route::get('accounts', 'RestaurantResourceController@accounts');
	Route::get('schedule', 'RestaurantResourceController@schedule');
	Route::get('favourites/{type?}', 'RestaurantResourceController@getClientfavourites');
	Route::get('bind', 'RestaurantResourceController@kitchenBind');
	Route::get('updatemaster', 'RestaurantResourceController@updatemaster');
	Route::get('transaction-log', 'RestaurantResourceController@transactionLog');
    Route::get('website', 'RestaurantResourceController@website');
    Route::get('app-setup', 'RestaurantResourceController@app');
    Route::post('update/{id}', 'RestaurantResourceController@websiteUpdate');
    Route::post('updateapp/{id}', 'RestaurantResourceController@appUpdate');
    Route::post('addpush/{id}', 'PushNotificationController@updatePushNotification');
    Route::get('edit-push/{id}', 'PushNotificationController@editPushNotification');
    Route::get('delete-push/{id}', 'PushNotificationController@deletePushNotification');
    Route::get('restaurant/report', 'RestaurantResourceController@pivotReports');
    Route::resource('restaurant', 'RestaurantResourceController');
    Route::resource('restaurant/delcharge', 'RestaurantResourceController@delcharge');
    Route::get('delivery-status', 'RestaurantResourceController@deliveryStatus');

    Route::get('appsettings', 'RestaurantResourceController@viewAppSettings');
    Route::get('menu', 'RestaurantResourceController@viewMenuSettings');
    Route::get('view-billing', 'RestaurantResourceController@viewBillingSettings');
    Route::get('view-website-setup', 'RestaurantResourceController@viewWebsiteSettings');
    Route::get('marketing-hub', 'RestaurantResourceController@viewMarketingHubSettings');
    
    Route::get('push-notifications', 'RestaurantResourceController@viewPushNotifications');
    
    Route::get('promotions', 'RestaurantResourceController@viewPromotions');


	Route::get('menu-item-status', 'MenuResourceController@menuItemStatus');
	Route::get('menu-setup', 'MenuResourceController@menuItems');
	Route::get('restaurant_addons', 'AddonResourceController@restauarnt_addons');	
    


});


// Public  routes for restaurant
Route::get('restaurants/reviews/{slug?}', 'RestaurantPublicController@restaurantReviews');
Route::get('restaurant/master/testdelete', 'RestaurantPublicController@testDelete');
Route::get('restaurant/popular/{period?}', 'RestaurantPublicController@popular');
Route::get('restaurants/', 'RestaurantPublicController@index');
Route::get('restaurants/mapview', 'RestaurantPublicController@mapView');
Route::get('restaurants/{slug?}', 'RestaurantPublicController@show');
Route::get('review/add/{slug?}', 'RestaurantPublicController@reviewAdd');
Route::get('restaurant/restaurantlisting', 'RestaurantPublicController@restaurantListing');
Route::get('restaurant/restaurantmaplisting', 'RestaurantPublicController@restaurantMapListing');
Route::get('addtofavourite/{slug?}/{status?}', 'RestaurantPublicController@addToFavourite');
Route::get('restaurant/cart/checkout/{id?}', 'RestaurantPublicController@restaurantTimeCheckout');
Route::get('eateries/', 'RestaurantPublicController@index1');
Route::get('eatery-{var?}', 'RestaurantPublicController@show1');
Route::get('time/{id}/{date}', 'RestaurantPublicController@loadTimeToCheckout');
Route::get('restaurant/vote/{id?}', 'RestaurantPublicController@VoteSubmit');
Route::get('restaurant/suggestions', 'RestaurantPublicController@searchSuggestions');
Route::get('restaurant_time/{type}/{id}', 'RestaurantPublicController@timeCheck');
Route::get('cartcheck/min_order/{type}/{id}', 'RestaurantPublicController@minorderCheck');
Route::get('restaurant/search', 'RestaurantPublicController@searchRestaurants');

Route::domain('{account}.zingmyorder.com')->group(function () {
   //Route::get('domain/{account?}/', 'RestaurantPublicController@restaurantDomain');
	Route::get('domain/about/{account}', function ($account) {
        $url = parse_url(URL::all());
        $domain = explode('.', $url['host']);
        $subdomain = $domain[0]; 
       Route::get('domain/about/$account', 'RestaurantWebsiteController@restaurantWebAbout');
    });
    Route::get('domain/menu/{account}', function ($account) {
        $url = parse_url(URL::all());
        $domain = explode('.', $url['host']);
        $subdomain = $domain[0]; 
       Route::get('domain/menu/$account', 'RestaurantWebsiteController@restaurantWebMenu');
    });
    Route::get('domain/gallery/{account}', function ($account) {
        $url = parse_url(URL::all());
        $domain = explode('.', $url['host']);
        $subdomain = $domain[0]; 
       Route::get('domain/gallery/$account', 'RestaurantWebsiteController@restaurantWebGallery');
    });
    Route::get('domain/contact/{account}', function ($account) {
        $url = parse_url(URL::all());
        $domain = explode('.', $url['host']);
        $subdomain = $domain[0]; 
       Route::get('domain/contact/$account', 'RestaurantWebsiteController@restaurantWebContact');
    });
     Route::get('domain/sendmail/{account}', function ($account) {
        $url = parse_url(URL::all());
        $domain = explode('.', $url['host']);
        $subdomain = $domain[0]; 
       Route::get('domain/sendmail/$account', 'RestaurantWebsiteController@sendMail');
    });
      Route::get('domain/locations/{account}', function ($account) {
        $url = parse_url(URL::all());
        $domain = explode('.', $url['host']);
        $subdomain = $domain[0]; 
       Route::get('domain/locations/$account', 'RestaurantWebsiteController@locations');
    });
         Route::get('domain/details/{account}', function ($account) {
        $url = parse_url(URL::all());
        $domain = explode('.', $url['host']);
        $subdomain = $domain[0]; 
       Route::get('domain/details/$account', 'RestaurantWebsiteController@newMenu');
    });
   Route::get('domain/{account}', function ($account) {
        $url = parse_url(URL::all());
        $domain = explode('.', $url['host']);
        $subdomain = $domain[0]; 
       Route::get('domain/$account', 'RestaurantWebsiteController@restaurantDomain');
    });
});
// http://zingmyorder.webnapps.net/admin/restaurant/restaurant/loginas/W4QGBnOGzl
// http://localhost/projects/zingmyorder/admin/restaurant/restaurant/loginas/QDlRNQaY50

Route::get('domain/about/{slug?}/', 'RestaurantWebsiteController@restaurantWebAbout');
Route::get('domain/menu/{slug?}/', 'RestaurantWebsiteController@restaurantWebMenu');
Route::get('domain/gallery/{slug?}/', 'RestaurantWebsiteController@restaurantWebGallery');
Route::get('domain/contact/{slug?}/', 'RestaurantWebsiteController@restaurantWebContact');
Route::get('domain/sendmail/{slug?}/', 'RestaurantWebsiteController@sendMail');
Route::get('domain/locations/{slug?}/', 'RestaurantWebsiteController@locations');
Route::get('domain/details/{slug?}/', 'RestaurantWebsiteController@newMenu');
Route::get('domain/{slug?}/', 'RestaurantWebsiteController@restaurantDomain');
Route::get('refresh/{slug?}/', 'RestaurantWebsiteController@restaurantRefresh');



Route::get('app/{slug?}/', 'RestaurantAppPublicController@restaurantApp');
Route::get('app-view/{slug?}/', 'RestaurantAppPublicController@restaurantAppView');

Route::get('app/about/{slug?}/', 'RestaurantAppController@about');
Route::get('app/menu/{slug?}/', 'RestaurantAppController@menu');
Route::get('app/gallery/{slug?}/', 'RestaurantAppController@gallery');
Route::get('app/contact/{slug?}/', 'RestaurantAppController@contact');
Route::get('app/locations/{slug?}/', 'RestaurantAppController@locations');
Route::get('app/{slug?}/', 'RestaurantAppController@restaurantApp');
Route::post('app/sendmail/{slug?}/', 'RestaurantAppController@sendMail');

// Resource routes  for addon
Route::group(['prefix' => set_route_guard('web').'/restaurant'], function () {
	Route::get('addons/{restaurant_id?}', 'AddonResourceController@getAddons');
	Route::get('addons/variations/{addon_id?}', 'AddonResourceController@getAddonVariations');
	Route::get('addons/rest_variations/{addon_id?}', 'AddonResourceController@getRestAddonVariations');
	// Route::get('restaurant_addons', 'AddonResourceController@restauarnt_addons');	
	Route::get('restaurant_addons/list', 'AddonResourceController@restauarnt_addons_list');
	Route::get('restaurant_addons/edit/{addon_id?}', 'AddonResourceController@restauarnt_addons_edit');
	Route::get('addon/status_update/{menu_id?}/{addon_id?}/{status?}', 'AddonResourceController@status_update');
    Route::resource('addon', 'AddonResourceController');
});

// Public  routes for addon
Route::get('addon/popular/{period?}', 'AddonPublicController@popular');
Route::get('addons/', 'AddonPublicController@index');
Route::get('addons/{slug?}', 'AddonPublicController@show');


// Resource routes  for category
Route::group(['prefix' => set_route_guard('web').'/restaurant'], function () {
	Route::get('category/restaurantcategory/{category_id?}/{restaurant_id?}', 'CategoryResourceController@restaurantcategories');
	Route::get('category/status_update/{category_id?}/{status?}', 'CategoryResourceController@status_update');
	Route::get('menu_items/category/editofferform/{category_id?}', 'CategoryResourceController@menuItems_categoryoffer_edit');
    Route::resource('category', 'CategoryResourceController');
});

// Public  routes for category
Route::get('category/popular/{period?}', 'CategoryPublicController@popular');
Route::get('categories/', 'CategoryPublicController@index');
Route::get('categories/{slug?}', 'CategoryPublicController@show');
Route::get('categories/Parent/{id}', 'CategoryPublicController@selectParent');
Route::get('categories/Category/{id}', 'CategoryPublicController@selectCategory');

// Resource routes  for menu
Route::group(['prefix' => set_route_guard('web').'/restaurant'], function () {
    // Route::prefix('{guard}/restaurant')->group(function () {

	// Route::get('menu_items', 'MenuResourceController@menuItems');
	// Route::get('menu-item-status', 'MenuResourceController@menuItemStatus');
	Route::get('menu_items/category/list', 'MenuResourceController@menuItems_category_list');
	Route::get('menu_items/category/edit/{category_id?}', 'MenuResourceController@menuItems_category_edit');
	Route::get('menu_items/menu/addform', 'MenuResourceController@menuItems_menu_add');
	Route::get('menu_items/menu/importform', 'MenuResourceController@menuItems_menu_import');
	Route::get('menu_items/menu/editform/{menu_id?}', 'MenuResourceController@menuItems_menu_edit');
	Route::post('menu/import', 'MenuResourceController@import_menus');
	Route::get('menu_items/menu/editscheduleform/{menu_id?}', 'MenuResourceController@menuItems_menuschedule_edit');
	Route::get('menu_items/menu/editofferform/{menu_id?}', 'MenuResourceController@menuItems_menuoffer_edit');
	Route::get('menu_items/menu/detailform/{menu_id?}', 'MenuResourceController@menuItems_menu_detail');
	Route::get('menu_items/addon/addform/{menu_id?}', 'MenuResourceController@menuItems_addon_add');
	Route::get('menu_items/addon/editform/{menu_id?}/{addon_id?}', 'MenuResourceController@menuItems_addon_edit');
	Route::get('menu_items/variation/addform/{menu_id?}', 'MenuResourceController@menuItems_variation_add');
	Route::get('menu_items/variation/editform/{menu_id?}/{key?}', 'MenuResourceController@menuItems_variation_edit');
	Route::get('menu/edit', 'MenuResourceController@menu_edit');
	Route::get('menu/add', 'MenuResourceController@menu_add');
	Route::get('menu/remove_var/{id?}/{key?}', 'MenuResourceController@removeVariation');
	Route::get('menu/remove_addon/{id?}/{addon_id?}', 'MenuResourceController@removeAddon');
	Route::get('menu/scheduleForm/{count?}', 'MenuResourceController@menu_schedule_form');
    Route::get('menu/importmaping', 'MenuResourceController@getimportMaping');
	Route::post('menu/importmapping', 'MenuResourceController@postimportMapping');
	Route::get('menu/updatesearchtext', 'MenuResourceController@updatesearchtext');

	Route::get('kitchen_menu_items', 'MenuResourceController@menuItems');
	Route::get('kitchen_menu_items/menu/detailform/{menu_id?}', 'MenuResourceController@menuItems_menu_detail');
	Route::get('menu/status_update/{menu_id?}/{status?}', 'MenuResourceController@status_update');
	Route::get('variation/status_update/{menu_id?}/{key?}/{status?}', 'MenuResourceController@variation_status_update');
    Route::resource('menu', 'MenuResourceController');
	Route::get('menu/download/csv/{filter}', 'MenuResourceController@downloadCSV');
	Route::get('menu/download-csv/{id}', 'MenuResourceController@downloadMenuCSV');

});

// Public  routes for menu
Route::get('menu/popular/{period?}', 'MenuPublicController@popular');
Route::get('restaurant/menu/search/{rest_id}', 'MenuPublicController@menuSearch');
Route::get('menus/', 'MenuPublicController@index');
// Route::get('loadmenus/', 'MenuPublicController@loadMenu');
Route::get('menus/{slug?}', 'MenuPublicController@show');
Route::get('restaurant/menu/{slug?}', 'MenuPublicController@menuModel');
Route::get('restaurant/menu/video/{slug?}', 'MenuPublicController@menuVideo');
Route::get('restaurant/menumodel/{slug?}', 'MenuPublicController@menuModelShow');
Route::get('food/', 'MenuPublicController@food');
Route::get('food-{var?}', 'MenuPublicController@food1');

// Resource routes  for review
Route::group(['prefix' => set_route_guard('web').'/restaurant'], function () {
    Route::resource('review', 'ReviewResourceController');
});

// Public  routes for review
Route::get('review/popular/{period?}', 'ReviewPublicController@popular');
Route::get('reviews/', 'ReviewPublicController@index');
Route::get('reviews/{slug?}', 'ReviewPublicController@show');
Route::post('review/post/{id?}', 'ReviewPublicController@postreview');
Route::post('feedback/post/{id?}', 'ReviewPublicController@postfeedback');
Route::get('feedback/rating/{id?}', 'ReviewPublicController@postrating');

