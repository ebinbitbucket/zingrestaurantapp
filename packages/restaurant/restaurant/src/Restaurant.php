<?php

namespace Restaurant\Restaurant;
use Restaurant\Restaurant\Models\Restauranttimings;
use DB;
use User;
use Session;
use Restaurant\Restaurant\Models\Restaurant as RestaurantModel;
class Restaurant
{
    /**
     * $restaurant object.
     */
    protected $restaurant;
    /**
     * $addon object.
     */
    protected $addon;
    /**
     * $category object.
     */
    protected $category;
    /**
     * $menu object.
     */
    protected $menu;
    /**
     * $review object.
     */
    protected $review;

    /**
     * Constructor.
     */
    public function __construct(\Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface $restaurant,
        \Restaurant\Restaurant\Interfaces\AddonRepositoryInterface $addon,
        \Restaurant\Restaurant\Interfaces\CategoryRepositoryInterface $category,
        \Restaurant\Restaurant\Interfaces\MenuRepositoryInterface $menu,
        \Restaurant\Restaurant\Interfaces\ReviewRepositoryInterface $review,
         \Laraecart\Cart\Interfaces\OrderRepositoryInterface $order)
    {
        $this->restaurant = $restaurant;
        $this->addon = $addon;
        $this->category = $category;
        $this->menu = $menu;
        $this->review = $review;
        $this->order = $order;
    }

    /**
     * Returns count of restaurant.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count($status)
    { 
        if($status == 'restaurant'){
            $restaurants = $this->restaurant->get();
        }
        if($status == 'orders'){
            $restaurants = $this->order->get();
        }
        if($status == 'reviews'){
            $restaurants = $this->review->get();
        }
        if($status == 'new_orders'){
            $restaurants = $this->order->findByField('order_status','New Orders');
        }

        return $restaurants->count();
    }
    
       public function transactionLog($restaurant_id, $status, $filter_date1 = null, $filter_date2 = null)
    {
        if ($filter_date1 == '') {
            $filter_date1 = date('Y-m-1 ');
        }

        if ($filter_date2 == '') {
            $filter_date2 = date('Y-m-d ');
        }
        $restaurant = $this->restaurant->findByField('id', $restaurant_id)->first(); 
        //$transactions = $restaurant->transactionLogs()->get();
        
        if($status == 'unique_visits'){
          return $this->order->uniqueVisits($restaurant_id, $filter_date1, $filter_date2);  
        }

        if($status == 'repeat_visits'){
          return $this->order->repeatVisits($restaurant_id, $filter_date1, $filter_date2);  
        }
        if($status == 'direct_link_visits'){
          return $this->order->directLinkVisits($restaurant_id,$filter_date1, $filter_date2);
        }
        if($status == 'direct_link_orders'){
          return $this->order->directLinkOrders($restaurant_id, $filter_date1, $filter_date2);
        }
        if($status == 'referral_visits'){
          return $this->order->referralVisits($restaurant_id, $filter_date1, $filter_date2);
        }
        if($status == 'referral_orders'){
          return $this->order->referralOrders($restaurant_id, $filter_date1, $filter_date2);
        }
    }


    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = '', $count = '')
    {
        $restaurant='';
        $data   = '';
        switch ($view) {

            case 'restaurant.gadget.list':
            break;
            
        }

        return view('restaurant::' . $view, compact('restaurant'))->render();
    }



    
     public function getRestaurant(){ 
        $restaurants=   $this->restaurant->getRestaurant();
        return $restaurants;
    }

    public function getParentCategory(){ 
        $categories=   $this->category->getParentCategory();
        return $categories;
    }

    public function getCategory(){ 
        $categories=   $this->category->getCategory();
        return $categories;
    }

     public function getRestaurantCategory($restaurant){ 
        $categories=   $this->category->getRestauarntCategories($restaurant);
        return $categories;
    }

    public function showCategory($id){ 
        $categories=   $this->category->getCategories($id);
        return $categories;
    }

    public function showAddon($id){ 
        $addon=   $this->addon->showAddon($id);
        return $addon;
    }

    public function showMenus($id){ 
        $menu=   $this->menu->showMenus($id);
        return $menu;
    }
    
    public function getGoodMenu($id){ 
        $menu=   $this->menu->getGoodMenu($id);
        return $menu;
    }
    
    public function showMenuCategory($id){ 
        $menu=   $this->category->showMenuCategory($id);
        return $menu;
    }

    public function getLatestMenus(){
        $menus = $this->menu->loadMoreMenu(); 
         return view('restaurant::public.restaurant.loadmore', compact('menus'))->render();       
    }

    public function getRestaurantDetails($id){ 
        $restaurants= $this->restaurant->getRestaurantDetails($id); 
        return $restaurants;
    }

     public function getAddonsList($id){ 
        $addons=   $this->addon->getAddonsList($id); 
        return $addons;
    }
    public function getAddon($id){ 
        $addons=   $this->addon->getAddon($id); 
        return $addons;
    }

    public function getMenuAddons($menu_id,$addon_id){ 
        $addon_det = DB::table('menu_addon')->select('menu_addon.*','restaurant_addons.name')->join('restaurant_addons','menu_addon'.'.addon_id','restaurant_addons'.'.id')->where('menu_id',$menu_id)->where('addon_id',$addon_id)->get(); 

        return $addon_det;
    }
    
    public function getKeywords(){ 
        $keywords_array = $this->restaurant->pluck('keywords','keywords');
        foreach($keywords_array as $value) {
              $keys[]= explode(',', $value);
        }
        $keywords=[];
        for($i=0;$i<count($keys);$i++){
            $keywords = array_merge($keywords,$keys[$i]);
        }
          
        return $keywords;
    }

    public function restaurantList(){ 
        $restaurants=   $this->restaurant->get()->take(10);
        return $restaurants;
    }

    public function reviewList(){ 
        $reviews=   $this->review->with('restaurant')->get()->take(10);
        return $reviews;
    }

    public function getStockStatus($menu_id,$addon_id){ 
        $menu_addon=   DB::table('menu_addon')->where('menu_id',$menu_id)->where('addon_id',$addon_id)->first();
        return $menu_addon->stock_status;
    }
    
    public function getRestaurantData($restaurant_id){ 
        $restaurant=   $this->restaurant->with('timings')->findByField('id',$restaurant_id)->first();
        return $restaurant;
    }

    public function getRestaurantsByMaster(){ 
        $data = $this->menu->getMenuIdsByMaster1();
        $restaurants_ids=   $this->menu->getRestaurantsByMaster($data);
 $restaurants=   $this->restaurant->getAllRestaurantsByMaster($restaurants_ids);
   return $restaurants;
    }
    public function getSessionMenus($master_ids,$restaurant_id){ 
        $restaurant=   $this->menu->getSessionMenus($master_ids,$restaurant_id);
        return $restaurant;
    }
     public function getMenuName($restaurant_id){ 
        $menus=   $this->menu->getSessionMenus(Session::get('selected_masters[]'),$restaurant_id);
        return $menus;
    }

    public function getRestaurantTimings($restaurant_id){ 
        return Restauranttimings::where('restaurant_id',$restaurant_id)->orderBy('day')->orderBy('opening')->get();
    }
   public function getOpenStatus($restaurant_id){ 
         if(!empty(Session::get('search_time')) && empty($search['working_hours'])){
            $search['working_date'] = Session::get('search_time');
                $search['working_hours'] =Session::get('search_time');
        }
        elseif(empty($search['working_hours']) && empty(Session::get('search_time'))){
            $search['working_date'] = date('Y-m-d');
            $search['working_hours'] = date('H:i:s');
            
        }
        
        $flag = 0;
        $timings = DB::table('restaurant_timings')->where('restaurant_id',$restaurant_id)->where('day',strtolower(substr(date('l',strtotime($search['working_date'])),0,3)))->whereNull('deleted_at')->get();
                foreach ($timings as $key => $value) { 
                    if(date('H:i',strtotime($value->opening))<=date('H:i',strtotime($search['working_hours'])) && date('H:i',strtotime($value->closing))>=date('H:i',strtotime($search['working_hours']))){ 
                         $flag = 1;
                         break;
                     }
                }
                if(empty($timings)){
                  $flag = 0;
                }
      return $flag;  
  
    }
    
     public function getTodaysTime($restaurant_id, $date = null){ 
      if($date == null){
        $date = date('Y-m-d');
      }
       $timings = DB::table('restaurant_timings')->where('restaurant_id',$restaurant_id)->where('day',strtolower(substr(date('l',strtotime($date)),0,3)))->whereNull('deleted_at')->get();
        return $timings;
    }
  
   public function foodReview($restaurant_id, $customer_id = null){ 
        $foodreviews =   $this->review->getFoodReviews($restaurant_id, $customer_id); 
        return $foodreviews;
    }
}
