<?php

namespace Restaurant\Restaurant\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class ReviewTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Restaurant\Models\Review $review)
    {
        return [
            'id'                => $review->getRouteKey(),
            'restaurant_id'       => @$review->restaurant->name,
            'customer_id'       => @$review->customer->name,
            'order_id'          => $review->order_id,
            'rating'            => $review->rating,
            'review'            => $review->review,
            'created_at'        => $review->created_at,
            'deleted_at'        => $review->deleted_at,
            'updated_at'        => $review->updated_at,
            'url'               => [
                'public'    => trans_url('restaurant/'.$review->getPublicKey()),
                'user'      => guard_url('restaurant/review/'.$review->getRouteKey()),
            ], 
            'status'            => trans($review->status),
            'created_at'        => format_date($review->created_at),
            'updated_at'        => format_date($review->updated_at),
        ];
    }
}