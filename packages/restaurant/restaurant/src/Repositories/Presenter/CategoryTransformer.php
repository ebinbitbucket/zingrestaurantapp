<?php

namespace Restaurant\Restaurant\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Restaurant\Models\Category $category)
    {
        return [
            'id'                => $category->getRouteKey(),
            'key'               => [
                'public'    => $category->getPublicKey(),
                'route'     => $category->getRouteKey(),
            ], 
            
            'restaurant_id'     => @$category->restaurant->name,
            'parent_id'         => @$category->category->name,
            'name'              => $category->name,
            'preparation_time'  => $category->preparation_time,
            'slug'              => $category->slug,
            'created_at'        => $category->created_at,
            'deleted_at'        => $category->deleted_at,
            'updated_at'        => $category->updated_at,
            'url'               => [
                'public'    => trans_url('restaurant/'.$category->getPublicKey()),
                'user'      => guard_url('restaurant/category/'.$category->getRouteKey()),
            ], 
            'status'            => trans($category->status),
            'created_at'        => format_date($category->created_at),
            'updated_at'        => format_date($category->updated_at),
        ];
    }
}