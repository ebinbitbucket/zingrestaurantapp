<?php

namespace Restaurant\Restaurant\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class MenuPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new MenuTransformer();
    }
}