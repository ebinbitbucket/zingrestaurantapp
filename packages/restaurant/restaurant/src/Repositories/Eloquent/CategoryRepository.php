<?php

namespace Restaurant\Restaurant\Repositories\Eloquent;

use Restaurant\Restaurant\Interfaces\CategoryRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.restaurant.category.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.restaurant.category.model.model');
    }

    public function getParentCategory(){  
        return $this->model->pluck('name','id');
    }

    public function getCategory(){  
        return $this->model->pluck('name','id');
    }

    public function getRestauarntCategories($id)
     { 
        return $this->model->where('restaurant_id',$id)->orderBy('name')->pluck('name','id');
     }

    public function getParent($id)
     {
        return $this->model->where('restaurant_id',$id)->where('parent_id',0)->get();
     }
     public function getCategories($id)
     { 
        return $this->model->with('menu')->where('restaurant_id',$id)->orderBy('name','ASC')->get();
     }

     public function showMenuCategory($id)
    { 
        return $this->model->where('id',$id)->get();
    }

    public function restaurantcategories($restaurant_id)
   {
       return $this->model
        ->where(function ($query)use($restaurant_id) {
          if ($restaurant_id!=0) {
              $query->whereRestaurantId($restaurant_id);
              }
            })
           ->orderBy('name','ASC')->pluck('name', 'id');
   }

}
