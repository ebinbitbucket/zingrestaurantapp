<?php

namespace Restaurant\Restaurant\Repositories\Eloquent;

use Restaurant\Restaurant\Interfaces\AddonRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class AddonRepository extends BaseRepository implements AddonRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.restaurant.addon.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.restaurant.addon.model.model');
    }

    public function showAddon($id)
     { 
        return $this->model->where('restaurant_id',$id)->get();
     }

     public function getAddons($id)
     {  
        return $this->model->where('restaurant_id',$id)->get();
    }

    public function getAddonsList($id)
     {  
        return $this->model->where('restaurant_id',$id)->pluck('name','id');
    }
    public function getAddon($id)
     {  
        return $this->model->where('id',$id)->pluck('name','id');
    }

    
}
