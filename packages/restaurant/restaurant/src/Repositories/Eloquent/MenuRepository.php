<?php

namespace Restaurant\Restaurant\Repositories\Eloquent;

use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;
use Session;
use Restaurant\Restaurant\Models\Restaurant;
use DB;

class MenuRepository extends BaseRepository implements MenuRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.restaurant.menu.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.restaurant.menu.model.model');
    }

     public function loadMoreMenu()
    {
        return $this->model->select('restaurant_menus.*')->join('restaurants',
                'restaurant_menus' . '.restaurant_id', '=', 'restaurants.id')
        ->join('restaurant_categories',
                'restaurant_menus' . '.category_id', '=', 'restaurant_categories.id')
            ->where('restaurants.published', 'Published')
            ->whereNull('restaurant_categories.deleted_at')
        ->orderBy('restaurant_menus.created_at', 'desc')->paginate(8);
    }

    public function showMenus($id)
    { 
        return $this->model->where('restaurant_id',$id)->get();
    }
    
     public function getGoodMenu($id)
    { 
        return $this->model->where('restaurant_id',$id)->take(2)->get();
    }
    
    public function getAddonDetails($menu)
    { 
        return $this->model
            ->select('menu_addon.*')
            ->join('menu_addon',
                'restaurant_menus' . '.id', '=', 'menu_id')
            ->where('menu_addon.menu_id', $menu->id)->get();
    }

    public function getAddonById($id)
    { 

        return DB::table('menu_addon')
                     ->select('menu_addon.*','restaurant_addons.name')
                     ->join('restaurant_addons', 'menu_addon.addon_id', '=', 'restaurant_addons.id')
                     ->where('menu_addon.deleted_at', '=',Null) ->where('menu_addon.menu_id', $id)->get();

    //    return $this->model
    //         ->select('menu_addon.*')
    //         ->join('menu_addon',
    //         'restaurant_addons' . '.id', '=', 'addon_id')
    //         ->join('menu_addon',
    //         'restaurant_menus' . '.id', '=', 'menu_id')
    //         ->where('menu_addon.menu_id', $id)->get();
    }
    
     public function getSessionMenus($master_ids,$restaurant_id)
    { 
        $masterSql = [];
        foreach ($master_ids as $key => $value) {
            $masterSql[] = 'master_search LIKE \'%' . $value . '%\'';
        }
        $masterSql = '(' . implode(' OR ', $masterSql) . ')';
        return  $this->model->select('restaurant_menus' . '.*')->with('master')
            // ->Join('masters', 'restaurant_menus' . '.name', '=','masters' . '.name')
            // ->Join('masters', 'restaurant_menus' . '.master_category', '=','masters' . '.id')
            ->Join('restaurant_categories', 'restaurant_menus' . '.category_id', '=','restaurant_categories' . '.id')
            ->where('restaurant_menus'.'.restaurant_id', $restaurant_id)
            ->whereRaw($masterSql)
            // ->whereNull('deleted_at')
            ->whereNull('restaurant_menus' .'.deleted_at')
            ->whereNull('restaurant_categories' .'.deleted_at')
            ->orderBy('price','ASC')->get();

    //   return  $this->model->select('restaurant_menus' . '.*')
    //         // ->Join('masters', 'restaurant_menus' . '.name', '=','masters' . '.name')
    //         ->Join('masters', 'restaurant_menus' . '.master_category', '=','masters' . '.id')
    //         ->Join('restaurant_categories', 'restaurant_menus' . '.category_id', '=','restaurant_categories' . '.id')
    //         ->whereIn('masters'.'.id',$master_ids)
    //         ->where('restaurant_menus'.'.restaurant_id',$restaurant_id)
    //         ->whereNull('restaurant_menus' .'.deleted_at')
    //         ->whereNull('restaurant_categories' .'.deleted_at')
    //         ->orderBy('created_at','DESC')->distinct()->get();
           
    }

     public function getMenuIdsByMaster($search=[])
    { 
        $data['menu_ids'] = $this->model->select('restaurant_menus' . '.id')
            ->Join('masters', 'restaurant_menus' . '.name', '=','masters' . '.name')
            ->Join('restaurant_categories', 'restaurant_menus' . '.category_id', '=','restaurant_categories' . '.id')
            ->whereIn('masters'.'.id',Session::get('selected_masters[]'))
            ->whereNull('restaurant_menus' .'.deleted_at')
            ->whereNull('restaurant_categories' .'.deleted_at')
            ->orderBy('restaurant_menus'.'.created_at','DESC')->distinct()->get();

        
        if(!empty(Session::get('latitude'))){
             $latitude =  Session::get('latitude');
        $longitude = Session::get('longitude');
        $restaurants =    DB::select(
               'SELECT id FROM
                    (SELECT id, (' . 3959   . ' * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) *
                    cos(radians(longitude) - radians(' . Session::get('longitude') . ')) +
                    sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude))))
                    AS distance
                    FROM restaurants) AS distances
                WHERE distance < ' . 20 . '
                ORDER BY distance
                ;
            ');
        }
        else{
            $restaurants = Restaurant::get();
        }
        $result = [];
            foreach ($restaurants as $key => $value) {
                $result[$key] = $value->id;
            }
        if(!empty(Session::get('search_time')) && empty($search['working_hours'])){
            $search['working_date'] = Session::get('search_time');
                $search['working_hours'] =Session::get('search_time');
        }
        elseif(empty($search['working_hours']) && empty(Session::get('search_time'))){
            $search['working_date'] = date('Y-m-d');
            $search['working_hours'] = date('H:i:s');
            
        }

        
        $restaurants_opennow = Restaurant::pricemin($search)->pluck('id')->toArray();
        $results = array_intersect($result, $restaurants_opennow); 
         $data['res_ids'] = $this->model->select('restaurant_menus' . '.restaurant_id')
            ->Join('masters', 'restaurant_menus' . '.name', '=','masters' . '.name')
            ->Join('restaurant_categories', 'restaurant_menus' . '.category_id', '=','restaurant_categories' . '.id')
            ->whereIn('masters'.'.id',Session::get('selected_masters[]'))
            ->whereIn('restaurant_menus'.'.restaurant_id',$results)
            ->whereNull('restaurant_menus' .'.deleted_at')
            ->whereNull('restaurant_categories' .'.deleted_at')
            ->orderBy('restaurant_menus'.'.created_at','DESC')->distinct()->get();

        return $data;
           
    }

public function getMenuIdsByMaster1($search=[])
    { 
        $data['menu_ids'] = $this->model->select('restaurant_menus' . '.id')
            ->Join('masters', 'restaurant_menus' . '.name', '=','masters' . '.name')
            ->Join('restaurant_categories', 'restaurant_menus' . '.category_id', '=','restaurant_categories' . '.id')
            ->whereIn('masters'.'.id',Session::get('selected_masters[]'))
            ->whereNull('restaurant_menus' .'.deleted_at')
            ->whereNull('restaurant_categories' .'.deleted_at')
            ->orderBy('restaurant_menus'.'.created_at','DESC')->distinct()->get();

        
        if(!empty(Session::get('latitude'))){
             $latitude =  Session::get('latitude');
        $longitude = Session::get('longitude');
        $restaurants =    DB::select(
               'SELECT id FROM
                    (SELECT id, (' . 3959   . ' * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) *
                    cos(radians(longitude) - radians(' . Session::get('longitude') . ')) +
                    sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude))))
                    AS distance
                    FROM restaurants) AS distances
                WHERE distance < ' . 10 . '
                ORDER BY distance
                ;
            ');
        }
        else{
            $restaurants = Restaurant::get();
        }
        $result = [];
            foreach ($restaurants as $key => $value) {
                $result[$key] = $value->id;
            }
       
        
         $data['res_ids'] = $this->model->select('restaurant_menus' . '.restaurant_id')
            ->Join('masters', 'restaurant_menus' . '.name', '=','masters' . '.name')
            ->Join('restaurant_categories', 'restaurant_menus' . '.category_id', '=','restaurant_categories' . '.id')
            ->whereIn('masters'.'.id',Session::get('selected_masters[]'))
            ->whereIn('restaurant_menus'.'.restaurant_id',$result)
            ->whereNull('restaurant_menus' .'.deleted_at')
            ->whereNull('restaurant_categories' .'.deleted_at')
            ->orderBy('restaurant_menus'.'.created_at','DESC')->distinct()->get();

        return $data;
           
    }
         public function getRestaurantsByMaster($data)
    { 
        $count = [];
        $restaurant_ids = [];
         foreach ($data['res_ids'] as $key_r => $value_r) {
            $count[$value_r['restaurant_id']] = 0;
            foreach ($data['menu_ids'] as $key_m => $value_m) {
                $exists = $this->model->where('restaurant_id',$value_r['restaurant_id'])->where('id',$value_m['id'])->count();           

                if($exists> 0){
                    $count[$value_r['restaurant_id']]++;
                }
            }
        }

        foreach ($count as $key => $value) {
            if($value >= count(Session::get('selected_masters[]')))
                $restaurant_ids[] = $key;
            
        }
        return $restaurant_ids;
    }
    
     public function getAjax($filter) {
		$menus = $this->model->with('restaurant', 'categories');
		return $this->filter($menus, $filter);

	}

	public function filter($menus, $filter) {

		if (empty($filter)) {

			return $menus->get()->toArray();
		}

		return $menus->where(function ($query) use ($filter) {
			foreach ($filter as $key => $value) {
				if ($value == '') {
					continue;
				}

				$query->where($key, '=', $value);
			}
            $query->whereNull('master_category');

		})
			->take(40000)
			->get()
			->toArray();
    }
    
    public function getResMenu($restaurant_id) {
		return $this->model->with('restaurant','master', 'categories')->where('restaurant_id',$restaurant_id)->get()->toArray();

	}
}
