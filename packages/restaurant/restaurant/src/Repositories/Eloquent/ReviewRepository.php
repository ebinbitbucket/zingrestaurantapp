<?php

namespace Restaurant\Restaurant\Repositories\Eloquent;

use Restaurant\Restaurant\Interfaces\ReviewRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class ReviewRepository extends BaseRepository implements ReviewRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.restaurant.review.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.restaurant.review.model.model');
    }
    
   public function getFoodReviews($restaurant_id, $customer_id = null){  
        return $this->model->with('menu')->where('restaurant_id', $restaurant_id)->where('customer_id', $customer_id)->whereNotNull('menu_id')->orderBy('created_at', 'DESC')->get()->unique('menu_id');
    }
}
