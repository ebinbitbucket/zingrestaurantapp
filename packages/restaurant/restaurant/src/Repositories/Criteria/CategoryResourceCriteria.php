<?php

namespace Restaurant\Restaurant\Repositories\Criteria;

use Litepie\Repository\Contracts\CriteriaInterface;
use Litepie\Repository\Contracts\RepositoryInterface;

class CategoryResourceCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
        if(user()->hasRole('restaurant')){
    		$model = $model
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type());
    	}
        elseif(user()->hasRole('kitchen')){
            dD($model);
            $model = $model
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type());
        }
    	else
    	{
    		$model = $model;
    	}
        return $model;
    }
}