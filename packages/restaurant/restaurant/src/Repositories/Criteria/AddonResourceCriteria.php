<?php

namespace Restaurant\Restaurant\Repositories\Criteria;

use Litepie\Repository\Contracts\CriteriaInterface;
use Litepie\Repository\Contracts\RepositoryInterface;

class AddonResourceCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
    	if(user()->hasRole('Restaurant')){
    		$model = $model
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type());
    	}
    	else
    	{
    		$model = $model;
    	}
        
        return $model;
    }
}