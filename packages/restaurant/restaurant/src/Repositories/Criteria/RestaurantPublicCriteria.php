<?php

namespace Restaurant\Restaurant\Repositories\Criteria;

use Litepie\Repository\Contracts\CriteriaInterface;
use Litepie\Repository\Contracts\RepositoryInterface;

class RestaurantPublicCriteria implements CriteriaInterface
{

    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereIn('published',['Published','No Sale']);
        return $model;
    }
}
