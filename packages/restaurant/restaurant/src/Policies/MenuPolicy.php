<?php

namespace Restaurant\Restaurant\Policies;

use Litepie\User\Contracts\UserPolicy;
use Restaurant\Restaurant\Models\Menu;

class MenuPolicy
{

    /**
     * Determine if the given user can view the menu.
     *
     * @param UserPolicy $user
     * @param Menu $menu
     *
     * @return bool
     */
    public function view(UserPolicy $user, Menu $menu)
    {
        if ($user->canDo('restaurant.menu.view') && $user->isAdmin()) {
            return true;
        }

        return $menu->user_id == user_id() && $menu->user_type == user_type();
    }

    /**
     * Determine if the given user can create a menu.
     *
     * @param UserPolicy $user
     * @param Menu $menu
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('restaurant.menu.create');
    }

    /**
     * Determine if the given user can update the given menu.
     *
     * @param UserPolicy $user
     * @param Menu $menu
     *
     * @return bool
     */
    public function update(UserPolicy $user, Menu $menu)
    {
        if ($user->canDo('restaurant.menu.edit') && $user->isAdmin()) {
            return true;
        }
        if(user()->hasRole('restaurant')){
            return true;
        }
        return $menu->user_id == user_id() && $menu->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given menu.
     *
     * @param UserPolicy $user
     * @param Menu $menu
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Menu $menu)
    {
        return $menu->user_id == user_id() && $menu->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given menu.
     *
     * @param UserPolicy $user
     * @param Menu $menu
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Menu $menu)
    {
        if ($user->canDo('restaurant.menu.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given menu.
     *
     * @param UserPolicy $user
     * @param Menu $menu
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Menu $menu)
    {
        if ($user->canDo('restaurant.menu.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
