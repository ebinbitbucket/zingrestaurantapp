<?php

namespace Restaurant\Restaurant\Policies;

use Litepie\User\Contracts\UserPolicy;
use Restaurant\Restaurant\Models\Restaurant;

class RestaurantPolicy
{

    /**
     * Determine if the given user can view the restaurant.
     *
     * @param UserPolicy $user
     * @param Restaurant $restaurant
     *
     * @return bool
     */
    public function view(UserPolicy $user, Restaurant $restaurant)
    {
        if ($user->canDo('restaurant.restaurant.view') && $user->isAdmin()) {
            return true;
        }

        return $restaurant->user_id == user_id() && $restaurant->user_type == user_type();
    }

    /**
     * Determine if the given user can create a restaurant.
     *
     * @param UserPolicy $user
     * @param Restaurant $restaurant
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('restaurant.restaurant.create');
    }

    /**
     * Determine if the given user can update the given restaurant.
     *
     * @param UserPolicy $user
     * @param Restaurant $restaurant
     *
     * @return bool
     */
    public function update(UserPolicy $user, Restaurant $restaurant)
    {
        
        if($user->isRestaurant() && $restaurant->id == user_id()){
            return true;
        };
        if ($user->canDo('restaurant.restaurant.edit') && $user->isAdmin()) {
            return true;
        }

        return $restaurant->user_id == user_id() && $restaurant->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given restaurant.
     *
     * @param UserPolicy $user
     * @param Restaurant $restaurant
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Restaurant $restaurant)
    {
        return $restaurant->user_id == user_id() && $restaurant->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given restaurant.
     *
     * @param UserPolicy $user
     * @param Restaurant $restaurant
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Restaurant $restaurant)
    {
        if ($user->canDo('restaurant.restaurant.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given restaurant.
     *
     * @param UserPolicy $user
     * @param Restaurant $restaurant
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Restaurant $restaurant)
    {
        if ($user->canDo('restaurant.restaurant.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
