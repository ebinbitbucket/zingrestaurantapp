<?php

namespace Restaurant\Restaurant\Policies;

use Litepie\User\Contracts\UserPolicy;
use Restaurant\Restaurant\Models\Addon;

class AddonPolicy
{

    /**
     * Determine if the given user can view the addon.
     *
     * @param UserPolicy $user
     * @param Addon $addon
     *
     * @return bool
     */
    public function view(UserPolicy $user, Addon $addon)
    {
        if ($user->canDo('restaurant.addon.view') && $user->isAdmin()) {
            return true;
        }

        return $addon->user_id == user_id() && $addon->user_type == user_type();
    }

    /**
     * Determine if the given user can create a addon.
     *
     * @param UserPolicy $user
     * @param Addon $addon
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('restaurant.addon.create');
    }

    /**
     * Determine if the given user can update the given addon.
     *
     * @param UserPolicy $user
     * @param Addon $addon
     *
     * @return bool
     */
    public function update(UserPolicy $user, Addon $addon)
    {
        if ($user->canDo('restaurant.addon.edit') || $user->isAdmin()) {
            return true;
        }

        return $addon->user_id == user_id() && $addon->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given addon.
     *
     * @param UserPolicy $user
     * @param Addon $addon
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Addon $addon)
    {
        return $addon->user_id == user_id() && $addon->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given addon.
     *
     * @param UserPolicy $user
     * @param Addon $addon
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Addon $addon)
    {
        if ($user->canDo('restaurant.addon.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given addon.
     *
     * @param UserPolicy $user
     * @param Addon $addon
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Addon $addon)
    {
        if ($user->canDo('restaurant.addon.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
