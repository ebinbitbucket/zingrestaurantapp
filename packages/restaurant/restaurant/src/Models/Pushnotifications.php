<?php

namespace Restaurant\Restaurant\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
// use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;

use Litepie\Trans\Traits\Translatable;
class Pushnotifications extends Model
{
    use Filer, SoftDeletes, Hashids, Translatable,  PresentableTrait;
//     Slugger

    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.restaurant.push_notifications.model';

    //  protected $table = 'push_notifications';
     public function restaurant()
     {
          return $this->belongsTo('Restaurant\Restaurant\Models\Restaurant','restaurant_id');
     }


}
