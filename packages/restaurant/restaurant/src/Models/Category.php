<?php

namespace Restaurant\Restaurant\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
// use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;

use Litepie\Trans\Traits\Translatable;
class Category extends Model
{
    use Filer, SoftDeletes, Hashids, Translatable,  PresentableTrait;
   //  Slugger

    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.restaurant.category.model';

     public function category()
     {
     	return $this->belongsTo('Restaurant\Restaurant\Models\Category','parent_id');
     }

     public function restaurant()
     {
     	return $this->belongsTo('Restaurant\Restaurant\Models\Restaurant','restaurant_id');
     }

     public function menu()
     {
        return $this->hasMany('Restaurant\Restaurant\Models\Menu','category_id');
     }
}
