<?php

namespace Restaurant\Restaurant\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
// use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;

use Litepie\Trans\Traits\Translatable;
use DB;
use DateTime;
use Hash;
class Restaurant extends Model
{
    use Filer, SoftDeletes, Hashids, Translatable,  PresentableTrait;
    // Slugger

    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.restaurant.restaurant.model';

       public function setPasswordAttribute($val)
    {
        if (Hash::needsRehash($val)) {
            $this->attributes['password'] = bcrypt($val);
        } else {
            $this->attributes['password'] = ($val);
        }
    }

     public function role_add()
   {
       return $this->belongsToMany('Restaurant\Restaurant\Models\Restaurant', 'role_user', 'user_id', 'role_id');
   }

    /**
     * The menu that belong to the restaurant.
     */
    public function menu(){
        return $this->hasMany('Restaurant\Restaurant\Models\Menu');
    }

     /**
     * The timing of the restaurant.
     */
    public function timings(){
        return $this->hasMany('Restaurant\Restaurant\Models\Restauranttimings');
    }


    /**
     * The review that belong to the restaurant.
     */
    public function review(){
        return $this->hasMany('Restaurant\Restaurant\Models\Review')->orderBy('created_at','DESC');
    }

    public function category(){
        return $this->hasMany('Restaurant\Restaurant\Models\Category');
    }

    public function catering_category(){
        return $this->hasMany('Restaurant\Restaurant\Models\Category');
    }
    
    public function popular_category(){
        return $this->hasMany('Restaurant\Restaurant\Models\Menu');
    }
    

    public function masterCategory(){
        return $this->belongsTo('Restaurant\Master\Models\Category','category_name');
    }
    
    public function offers(){
        return $this->hasOne('Restaurant\Offer\Models\Offer')->where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->orderBy('created_at', 'DESC');
    }
    
     public function getMainlogoAttribute($val){
    
        if(!empty($this->logo)){ 
            return url($this->defaultImage('logo'));
        }
        elseif(!empty($this->gallery)){  
            return url($this->defaultImage('gallery'));
        }elseif(!empty($this->photos)){  
            return str_replace(['/original/', '/o.'],['/xs/', '/258s.'],$this->photos[0]);
        }
        else{ 
            return url('img/default/original.jpg');
        }
    }
    // public function getMobileBannerAttribute($val){
    
    //     dd(url(@$val->defaultImage('image','xs')));
    // }


     public function scopePricemin($query, $search,$restaurants_ids=[])
    {  
        if(!empty($search['working_hours'])){ 
            $ids = [];
            if(empty($restaurants_ids)){
                $restaurants_ids = $this->pluck('id')->toArray();

            }
            foreach ($restaurants_ids as $restaurant) {       
                $flag=0;
                $timings = DB::table('restaurant_timings')->where('restaurant_id',$restaurant)->where('day',strtolower(substr(date('l',strtotime($search['working_date'])),0,3)))->get();
                foreach ($timings as $key => $value) { 
                    if(date('H:i',strtotime($value->opening))<=date('H:i',strtotime($search['working_hours'])) && date('H:i',strtotime($value->closing))>=date('H:i',strtotime($search['working_hours']))){ 
                         $flag = 1;
                     }
                }
                if($flag == 1){
                        $ids[] = $restaurant;
                     }
            }
            $query->whereIn('id',$ids);
         }
         return $query;
    }

      public function transactionLogs(){
        return $this->hasMany('Laraecart\Cart\Models\TransactionLogs');
    }
    public function holidays(){
        return $this->hasMany('Restaurant\Holiday\Models\Holiday','restaurant_id');
    }
    public function getNextDateAttribute()
    {

    $weekday = strtolower(date('D'));
      // dd($weekday);
      //$date = new DateTime('now');
      for($i=0;$i<5;$i++){ 
        $date = date('Y-m-d H:i:s', strtotime('+'.$i.' days'));

       $weekday = strtolower(date("D", strtotime($date)));
        $result='';
       if($i==0){

        $times=date("H:i", strtotime($date));
        //$times='24:00';
        $open = $this->timings->where('day', $weekday)->where('opening', '<=', $times)->where('closing', '>=', $times)->first();
        if($open){
            //$result = date('Y-m-d H:i:s', strtotime('+'.$i.' days'));
            $result = date('Y-m-d H:i:s', (strtotime(date('Y-m-d H:i:s'))));

        break;   
        } else{

            $opens = $this->timings->where('day', $weekday)->where('opening', '>', $times)->where('opening','!=', 'off')
            ->first();

            if($opens){
                    $result = date('Y-m-d H:i:s', strtotime($opens->opening));

                break; 
                
                  
            }
        }
    } else{
        
       $date_opening=date("Y-m-d", strtotime($date));
        $open = $this->timings->where('day', $weekday)->where('opening','!=', 'off')->first();

        if($open){

            $opening_time = date('H:i:s', strtotime($open->opening));
            $result = date('Y-m-d H:i:s', strtotime("$date_opening $opening_time"));
            
        break;   
        }
             }
      


      }
      return date("D d-M", strtotime($result));
    
     
       
    }
    public function getNextTimeAttribute()
    {
       
    $weekday = strtolower(date('D'));
    // dd($weekday);
    //$date = new DateTime('now');
    for($i=0;$i<5;$i++){ 
      $date = date('Y-m-d H:i:s', strtotime('+'.$i.' days'));

     $weekday = strtolower(date("D", strtotime($date)));
      $result='';
     if($i==0){

      $times=date("H:i", strtotime($date));
      //$times='24:00';
      $open = $this->timings->where('day', $weekday)->where('opening', '<=', $times)->where('closing', '>=', $times)->first();
      if($open){
          //$result = date('Y-m-d H:i:s', strtotime('+'.$i.' days'));
          $result = '';

      break;   
      } else{

          $opens = $this->timings->where('day', $weekday)->where('opening', '>', $times)->where('opening','!=', 'off')
          ->first();

          if($opens){
                  $result = date('Y-m-d H:i:s', strtotime($opens->opening));

              break; 
              
                
          }
      }
  } else{
      
     $date_opening=date("Y-m-d", strtotime($date));
      $open = $this->timings->where('day', $weekday)->where('opening','!=', 'off')->first();

      if($open){

          $opening_time = date('H:i:s', strtotime($open->opening));
          $result = date('Y-m-d H:i:s', strtotime("$date_opening $opening_time"));
          
      break;   
      }
           }
    


    }
    return date("h:i A", strtotime($result));
  
   

       
    }
}
