<?php

namespace Restaurant\Restaurant\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
// use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;

use Litepie\Trans\Traits\Translatable;
class Review extends Model
{
    use Filer, SoftDeletes, Hashids, Translatable,  PresentableTrait;

    // Slugger
    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.restaurant.review.model';

     public function customer()
     {
     	return $this->belongsTo('\App\Client','customer_id');
     }

      public function restaurant()
     {
        return $this->belongsTo('Restaurant\Restaurant\Models\Restaurant');
     }
     
      public function menu(){
        return $this->belongsTo('Restaurant\Restaurant\Models\Menu');
    }
}
