<?php

namespace Restaurant\Restaurant\Providers;

use Illuminate\Support\ServiceProvider;

class RestaurantServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'restaurant');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'restaurant');

        // Load migrations
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        // Call pblish redources function
        $this->publishResources();

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();
        $this->registerRestaurant();
        $this->registerFacade();
        $this->registerBindings();
        //$this->registerCommands();
    }


    /**
     * Register the application bindings.
     *
     * @return void
     */
    protected function registerRestaurant()
    {
        $this->app->bind('restaurant', function($app) {
            return new Restaurant($app);
        });
    }

    /**
     * Register the vault facade without the user having to add it to the app.php file.
     *
     * @return void
     */
    public function registerFacade() {
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Restaurant', 'Restaurant\Restaurant\Facades\Restaurant');
        });
    }

    /**
     * Register bindings for the provider.
     *
     * @return void
     */
    public function registerBindings() {
        // Bind facade
        $this->app->bind('restaurant.restaurant', function ($app) {
            return $this->app->make('Restaurant\Restaurant\Restaurant');
        });

        // Bind Restaurant to repository
        $this->app->bind(
            'Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface',
            \Restaurant\Restaurant\Repositories\Eloquent\RestaurantRepository::class
        );
        // Bind Addon to repository
        $this->app->bind(
            'Restaurant\Restaurant\Interfaces\AddonRepositoryInterface',
            \Restaurant\Restaurant\Repositories\Eloquent\AddonRepository::class
        );
        // Bind Category to repository
        $this->app->bind(
            'Restaurant\Restaurant\Interfaces\CategoryRepositoryInterface',
            \Restaurant\Restaurant\Repositories\Eloquent\CategoryRepository::class
        );
        // Bind Menu to repository
        $this->app->bind(
            'Restaurant\Restaurant\Interfaces\MenuRepositoryInterface',
            \Restaurant\Restaurant\Repositories\Eloquent\MenuRepository::class
        );
        // Bind Review to repository
        $this->app->bind(
            'Restaurant\Restaurant\Interfaces\ReviewRepositoryInterface',
            \Restaurant\Restaurant\Repositories\Eloquent\ReviewRepository::class
        );

        $this->app->register(\Restaurant\Restaurant\Providers\AuthServiceProvider::class);
        
        $this->app->register(\Restaurant\Restaurant\Providers\RouteServiceProvider::class);
            }

    /**
     * Merges user's and restaurant's configs.
     *
     * @return void
     */
    protected function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php', 'restaurant'
        );
    }

    /**
     * Register scaffolding command
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\MakeRestaurant::class,
            ]);
        }
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['restaurant.restaurant'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../config/config.php' => config_path('restaurant/restaurant.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../resources/views' => base_path('resources/views/vendor/restaurant')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../resources/lang' => base_path('resources/lang/vendor/restaurant')], 'lang');

        // Publish public files and assets.
        $this->publishes([__DIR__ . '/public/' => public_path('/')], 'public');
    }
}
