<?php

namespace Restaurant\Restaurant\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the package.
     *
     * @var array
     */
    protected $policies = [
        // Bind Restaurant policy
        'Restaurant\Restaurant\Models\Restaurant' => \Restaurant\Restaurant\Policies\RestaurantPolicy::class,
// Bind Addon policy
        'Restaurant\Restaurant\Models\Addon' => \Restaurant\Restaurant\Policies\AddonPolicy::class,
// Bind Category policy
        'Restaurant\Restaurant\Models\Category' => \Restaurant\Restaurant\Policies\CategoryPolicy::class,
// Bind Menu policy
        'Restaurant\Restaurant\Models\Menu' => \Restaurant\Restaurant\Policies\MenuPolicy::class,
// Bind Review policy
        'Restaurant\Restaurant\Models\Review' => \Restaurant\Restaurant\Policies\ReviewPolicy::class,
    ];

    /**
     * Register any package authentication / authorization services.
     *
     * @param \Illuminate\Contracts\Auth\Access\Gate $gate
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
    }
}
