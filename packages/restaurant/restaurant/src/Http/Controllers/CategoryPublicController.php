<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Restaurant\Interfaces\CategoryRepositoryInterface;

class CategoryPublicController extends BaseController
{
    // use CategoryWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Category\Interfaces\CategoryRepositoryInterface $category
     *
     * @return type
     */
    public function __construct(CategoryRepositoryInterface $category)
    {
        $this->repository = $category;
        parent::__construct();
    }

    /**
     * Show category's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $categories = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$restaurant::category.names'))
            ->view('restaurant::category.index')
            ->data(compact('categories'))
            ->output();
    }


    /**
     * Show category.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $category = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$category->name . trans('restaurant::category.name'))
            ->view('restaurant::category.show')
            ->data(compact('category'))
            ->output();
    }

    public function selectParent($id)
   {

       $category = $this->repository->getParent($id);

       return view('restaurant::category.optionselect', compact('category'));
   }
   public function selectCategory($id)
   {

       $category = $this->repository->getCategories($id);

       return view('restaurant::category.categoryselect', compact('category'));
   }

}
