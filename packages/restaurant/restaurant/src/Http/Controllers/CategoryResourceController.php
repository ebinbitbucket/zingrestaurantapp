<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Restaurant\Http\Requests\CategoryRequest;
use Restaurant\Restaurant\Interfaces\CategoryRepositoryInterface;
use Restaurant\Restaurant\Models\Category;
use Restaurant\Restaurant\Models\Menu;

/**
 * Resource controller class for category.
 */
class CategoryResourceController extends BaseController
{

    /**
     * Initialize category resource controller.
     *
     * @param type CategoryRepositoryInterface $category
     *
     * @return null
     */
    public function __construct(CategoryRepositoryInterface $category)
    {
        parent::__construct();
        $this->repository = $category;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Restaurant\Repositories\Criteria\CategoryResourceCriteria::class);
    }

    /**
     * Display a list of category.
     *
     * @return Response
     */
    public function index(CategoryRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Restaurant\Repositories\Presenter\CategoryPresenter::class)
                ->$function();
        }

        $categories = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('restaurant::category.names'))
            ->view('restaurant::category.index', true)
            ->data(compact('categories', 'view'))
            ->output();
    }

    /**
     * Display category.
     *
     * @param Request $request
     * @param Model   $category
     *
     * @return Response
     */
    public function show(CategoryRequest $request, Category $category)
    {

        if ($category->exists) {
            $view = 'restaurant::category.show';
        } else {
            $view = 'restaurant::category.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('restaurant::category.name'))
            ->data(compact('category'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new category.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(CategoryRequest $request)
    {
        

        $category = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('restaurant::category.name')) 
            ->view('restaurant::category.create', true) 
            ->data(compact('category'))
            ->output();
    }

    /**
     * Create new category.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(CategoryRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            if(user()->hasRole('restaurant')){
                $attributes['restaurant_id'] = user_id();
            }
            $category                 = $this->repository->create($attributes);
            if(user()->hasRole('restaurant')){
                return redirect()->back();
            }
            return $this->response->message(trans('messages.success.created', ['Module' => trans('restaurant::category.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/category/' . $category->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/restaurant/category'))
                ->redirect();
        }

    }

    /**
     * Show category for editing.
     *
     * @param Request $request
     * @param Model   $category
     *
     * @return Response
     */
    public function edit(CategoryRequest $request, Category $category)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('restaurant::category.name'))
            ->view('restaurant::category.edit', true)
            ->data(compact('category'))
            ->output();
    }

    /**
     * Update the category.
     *
     * @param Request $request
     * @param Model   $category
     *
     * @return Response
     */
    public function update(CategoryRequest $request, Category $category)
    {
        try {
            $attributes = $request->all();
            if(isset($attributes['is_check'])){

            if(isset($attributes['stock_status'])){
                $attributes['stock_status'] = 1;
            }
            else{
                $attributes['stock_status'] = 0;
            }
        }
                if(!empty($attributes['order_no'])){
                $order_no_count = $attributes['order_no'];
                $categories = Category::where('id','<>',$category->id)
                                        ->where('restaurant_id',user_id())
                                        ->whereNotNull('order_no')
                                        ->where('order_no','>=',$attributes['order_no'])
                                        ->orderBy('order_no','ASC')
                                        ->get();
                foreach ($categories as $key_ordercategory => $val_ordercategory) {
                    Category::where('id',$val_ordercategory->id)->update(['order_no' => ++$order_no_count]);

                }
                $categoriesNull = Category::where('id','<>',$category->id)
                                        ->where('restaurant_id',user_id())
                                        ->whereNull('order_no')
                                        ->get();
                foreach ($categoriesNull as $key_categoriesNull => $val_categoriesNull) {
                    
                    Category::where('id',$val_categoriesNull->id)->update(['order_no' => ++$order_no_count]);
                  
                }
                
            }
            if(!empty($attributes['offer'])){
                Menu::where('category_id',$category->id)->update(['offer' => json_encode($attributes['offer'])]);
            }
            $category->update($attributes);
            if(user()->hasRole('restaurant')){
                 return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::category.name')]))
                ->code(204)
                ->status('success')
                ->redirect();
            }
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::category.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/category/' . $category->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/category/' . $category->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the category.
     *
     * @param Model   $category
     *
     * @return Response
     */
    public function destroy(CategoryRequest $request, Category $category)
    {
        try {

            $category->delete();
            if(user()->hasRole('restaurant')){
                return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::category.name')]))
                ->code(202)
                ->status('success')
                ->redirect();
            }
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::category.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('restaurant/category/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/category/' . $category->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple category.
     *
     * @param Model   $category
     *
     * @return Response
     */
    public function delete(CategoryRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::category.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('restaurant/category'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/category'))
                ->redirect();
        }

    }

    /**
     * Restore deleted categories.
     *
     * @param Model   $category
     *
     * @return Response
     */
    public function restore(CategoryRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('restaurant::category.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/restaurant/category'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/category/'))
                ->redirect();
        }

    }

    public function restaurantcategories($restaurant_id,$selected = 0)
   {

       $options    = $this->repository->restaurantcategories($restaurant_id);
   return view('restaurant::category.select', compact('options', 'selected'));
   }

   public function status_update($id,$status){
        $category = Category::find($id);
        if($status == 'on'){
            Category::where('id',$category->id)->update(['stock_status' => 1]);
        }
        else{
            Category::where('id',$category->id)->update(['stock_status' => 0]);
        }
        return redirect()->back();
   }
   
   public function menuItems_categoryoffer_edit($menu_id)
    { 
        $edit_menu = $this->repository->findByField('id',$menu_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_category__offer_edit')
            ->layout('default')
            ->data(compact('edit_menu'))
            ->output();
    }

}
