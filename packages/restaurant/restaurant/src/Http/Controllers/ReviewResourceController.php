<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Restaurant\Http\Requests\ReviewRequest;
use Restaurant\Restaurant\Interfaces\ReviewRepositoryInterface;
use Restaurant\Restaurant\Models\Review;

/**
 * Resource controller class for review.
 */
class ReviewResourceController extends BaseController
{

    /**
     * Initialize review resource controller.
     *
     * @param type ReviewRepositoryInterface $review
     *
     * @return null
     */
    public function __construct(ReviewRepositoryInterface $review)
    {
        parent::__construct();
        $this->repository = $review;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Restaurant\Repositories\Criteria\ReviewResourceCriteria::class);
    }

    /**
     * Display a list of review.
     *
     * @return Response
     */
    public function index(ReviewRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Restaurant\Repositories\Presenter\ReviewPresenter::class)
                ->$function();
        }

        $reviews = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('restaurant::review.names'))
            ->view('restaurant::review.index', true)
            ->data(compact('reviews', 'view'))
            ->output();
    }

    /**
     * Display review.
     *
     * @param Request $request
     * @param Model   $review
     *
     * @return Response
     */
    public function show(ReviewRequest $request, Review $review)
    {

        if ($review->exists) {
            $view = 'restaurant::review.show';
        } else {
            $view = 'restaurant::review.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('restaurant::review.name'))
            ->data(compact('review'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new review.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(ReviewRequest $request)
    {

        $review = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('restaurant::review.name')) 
            ->view('restaurant::review.create', true) 
            ->data(compact('review'))
            ->output();
    }

    /**
     * Create new review.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(ReviewRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $review                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('restaurant::review.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/review/' . $review->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/restaurant/review'))
                ->redirect();
        }

    }

    /**
     * Show review for editing.
     *
     * @param Request $request
     * @param Model   $review
     *
     * @return Response
     */
    public function edit(ReviewRequest $request, Review $review)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('restaurant::review.name'))
            ->view('restaurant::review.edit', true)
            ->data(compact('review'))
            ->output();
    }

    /**
     * Update the review.
     *
     * @param Request $request
     * @param Model   $review
     *
     * @return Response
     */
    public function update(ReviewRequest $request, Review $review)
    {
        try {
            $attributes = $request->all();

            $review->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::review.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/review/' . $review->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/review/' . $review->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the review.
     *
     * @param Model   $review
     *
     * @return Response
     */
    public function destroy(ReviewRequest $request, Review $review)
    {
        try {

            $review->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::review.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('restaurant/review/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/review/' . $review->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple review.
     *
     * @param Model   $review
     *
     * @return Response
     */
    public function delete(ReviewRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::review.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('restaurant/review'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/review'))
                ->redirect();
        }

    }

    /**
     * Restore deleted reviews.
     *
     * @param Model   $review
     *
     * @return Response
     */
    public function restore(ReviewRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('restaurant::review.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/restaurant/review'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/review/'))
                ->redirect();
        }

    }

}
