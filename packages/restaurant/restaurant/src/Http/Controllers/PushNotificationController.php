<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Response;
use Restaurant\Restaurant\Http\Requests\RestaurantRequest;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
use Restaurant\Restaurant\Models\Pushnotifications;
use Restaurant\Restaurant\Models\Restaurant;
use Session;

class PushNotificationController extends BaseController
{
    // use ReviewWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Review\Interfaces\ReviewRepositoryInterface $review
     *
     * @return type
     */
    public function __construct(RestaurantRepositoryInterface $restaurant)
    {
        $this->repository = $restaurant;
        parent::__construct();
    }

    /**
     * Show review's list.
     *
     * @param string $slug
     *
     * @return response
     */
    public function updatePushNotification(RestaurantRequest $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'push_not_name' => 'required',
            'push_not_content' => 'required',
            'schedule_date' => 'required',

        ]);

        if ($validator->fails()) {

            Session::flash('error', "ERROR");
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $not = Pushnotifications::updateOrCreate(['id' => @$request->push_not_id], [
            'restaurant_id' => hashids_decode(@$request->restaurant_id),
            'title' => @$request->push_not_name,
            'content' => @$request->push_not_content,
            'schedule_date' => Carbon::parse(@$request->schedule_date)->format('Y-m-d\TH:i'),
            'notification_sent_status' => 0,
        ]);
        return redirect()->back();
    }
    public function editPushNotification($push_not_id)
    {

        $notifications = Pushnotifications::where('id', @$push_not_id)->where('notification_sent_status', 0)->get();
        return view('restaurant::restaurant.pushnotification_edit', compact('notifications'));

    }
    public function deletePushNotification($push_not_id)
    {

        $del_notification = Pushnotifications::where('id', @$push_not_id)->where('notification_sent_status', 0)->delete();
        return $del_notification;

    }

}
