<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
// use App\Http\Controllers\PublicController as BaseController;

use Auth;
use Carbon\Carbon;
use DB;
use Form;
use Laraecart\Cart\Models\Order;
use Response;
use Restaurant\Restaurant\Http\Requests\RestaurantRequest;
use Restaurant\Restaurant\Interfaces\AddonRepositoryInterface;
use Restaurant\Restaurant\Interfaces\CategoryRepositoryInterface;
use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
use Restaurant\Restaurant\Models\Favourite;
use Restaurant\Restaurant\Models\Pushnotifications;
use Restaurant\Restaurant\Models\Restaurant;
use Restaurant\Restaurant\Models\Restaurantapp;
use Restaurant\Restaurant\Models\Restauranttimings;
use Session;
use Validator;

/**
 * Resource controller class for restaurant.
 */
class RestaurantResourceController extends BaseController
{

    /**
     * Initialize restaurant resource controller.
     *
     * @param type RestaurantRepositoryInterface $restaurant
     *
     * @return null
     */
    public function __construct(RestaurantRepositoryInterface $restaurant, CategoryRepositoryInterface $category, AddonRepositoryInterface $addon, MenuRepositoryInterface $menu)
    {
        parent::__construct();
        $this->repository = $restaurant;
        $this->category = $category;
        $this->addon = $addon;
        $this->menu = $menu;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Restaurant\Repositories\Criteria\RestaurantResourceCriteria::class);

    }

    /**
     * Display a list of restaurant.
     *
     * @return Response
     */
    public function index(RestaurantRequest $request)
    {
        $this->response->theme->asset()->add('gmap', 'https://maps.googleapis.com/maps/api/js?key=' . config('services.GOOGLE_API') . '&libraries=places');

        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Restaurant\Repositories\Presenter\RestaurantPresenter::class)
                ->$function();
        }

        $restaurants = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
            ->view('restaurant::restaurant.index', true)
            ->data(compact('restaurants', 'view'))
            ->output();
    }

    /**
     * Display restaurant.
     *
     * @param Request $request
     * @param Model   $restaurant
     *
     * @return Response
     */
    public function show(RestaurantRequest $request, Restaurant $restaurant)
    {

        if ($restaurant->exists) {
            $view = 'restaurant::restaurant.show';
        } else {
            $view = 'restaurant::restaurant.new';
        }
        $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id', $restaurant->id)->orderBy('day')->orderBy('opening')->get();
        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('restaurant::restaurant.name'))
            ->data(compact('restaurant', 'restaurant_timings'))
            ->view($view, true)
            ->output();
    }

    public function showall(RestaurantRequest $request, Restaurant $restaurant, Category $category, Addon $addon, Menu $menu)
    {
        if ($restaurant->exists) {
            $category = $category = $this->category->scopeQuery(function ($query) use ($restaurant) {
                return $query->orderBy('id', 'DESC')
                    ->where('restaurant_id', $restaurant->id);
            })->first(['*']);
            $view = 'restaurant::restaurant.showall';
        } else {
            $view = 'restaurant::restaurant.new';
        }
        $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id', $restaurant->id)->get();
        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('restaurant::restaurant.name'))
            ->data(compact('restaurant', 'category', 'restaurant_timings'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new restaurant.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(RestaurantRequest $request)
    {
        $restaurant = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('restaurant::restaurant.name'))
            ->view('restaurant::restaurant.create', true)
            ->data(compact('restaurant'))
            ->output();
    }

    /**
     * Create new restaurant.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(RestaurantRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id();
            $attributes['user_type'] = user_type();
            $attributes['api_token'] = str_random(60);
            if (@$attributes['delivery'] == 'No') {
                $attributes['delivery_charge'] = 0;
            }
            $rules = [
                'email' => 'required|email|max:255|unique:restaurants',
                'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',

            ];
            $data = [

                'email' => $attributes['email'],
                'phone' => $attributes['phone'],

            ];
            $c = Validator::make($data, $rules);

            if ($c->fails()) {
                Session::flash('error', "ERROR");
                return $this->response->message($c->errors()->messages())
                    ->code(200)
                    ->status('error')
                    ->redirect();
            } else {
                $restaurant = $this->repository->create($attributes);
                foreach ($attributes['timings'] as $key => $value) {
                    foreach ($value as $x => $times) {
                        if ($times['start'] != null || $times['end'] != null) {
                            if ($times['start'] < $times['end']) {
                                foreach ($attributes['timings'][$key] as $overlapp_key => $overlapp_value) {
                                    if (($times['start'] > $overlapp_value['start'] && $times['start'] < $overlapp_value['end']) || ($times['end'] > $overlapp_value['start'] && $times['end'] < $overlapp_value['end'])) {
                                        $restaurant->forceDelete();
                                        if (user()->hasRole('restaurant')) {
                                            return $this->response->message('Overlapping of time. Please enter valid time')
                                                ->code(400)
                                                ->url($url)
                                                ->redirect();
                                        } else {
                                            return $this->response->message('Overlapping of time. Please enter valid time')
                                                ->code(400)
                                                ->status('error')
                                                ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                                                ->redirect();
                                        }
                                    }
                                }
                                $restaurant_timings = Restauranttimings::create(['restaurant_id' => $restaurant->id, 'day' => $key, 'opening' => $times['start'], 'closing' => $times['end'], 'user_id' => $attributes['user_id'], 'user_type' => $attributes['user_type']]);
                            } else {
                                $restaurant->forceDelete();
                                if (user()->hasRole('restaurant')) {
                                    return $this->response->message('Opening time should be less than closing time')
                                        ->code(400)
                                        ->url($url)
                                        ->redirect();
                                } else {
                                    return $this->response->message('Opening time should be less than closing time')
                                        ->code(400)
                                        ->status('error')
                                        ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                                        ->redirect();
                                }
                            }
                        } else {
                            $restaurant_timings = Restauranttimings::create(['restaurant_id' => $restaurant->id, 'day' => $key, 'opening' => 'off', 'closing' => 'off', 'user_id' => $attributes['user_id'], 'user_type' => $attributes['user_type']]);
                        }
                    }
                }
            }
            return $this->response->message(trans('messages.success.created', ['Module' => trans('restaurant::restaurant.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/restaurant/restaurant'))
                ->redirect();
        }

    }

    /**
     * Show restaurant for editing.
     *
     * @param Request $request
     * @param Model   $restaurant
     *
     * @return Response
     */
    public function edit(RestaurantRequest $request, Restaurant $restaurant)
    {
        $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id', $restaurant->id)->orderBy('day')->get();
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('restaurant::restaurant.name'))
            ->view('restaurant::restaurant.edit', true)
            ->data(compact('restaurant', 'restaurant_timings'))
            ->output();
    }

    /**
     * Update the restaurant.
     *
     * @param Request $request
     * @param Model   $restaurant
     *
     * @return Response
     */
    public function update(RestaurantRequest $request, Restaurant $restaurant)
    {

        try {

            $attributes = $request->all();
            // if(empty($attributes['delivery'])){
            //     $attributes['delivery_charge'] = 0;
            // }
            // elseif($attributes['delivery'] == 'No'){
            //     $attributes['delivery_charge'] = 0;
            // }

            //  if(user()->hasRole('restaurant')){
            //      if(empty($attributes['audio_alert'])){
            //         $attributes['audio_alert'] = 'off';
            //     }
            // }
            if ((empty($attributes['ac_no']) && empty($attributes['IFSC']) && empty($attributes['bank'])) && empty($attributes['timings'])) {

                $rules = [
                    'phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/',

                ];
                $data = [

                    'phone' => $attributes['phone'],

                ];
            } else {
                $rules = [];
                $data = [];
                if (isset($attributes['ac_no'])) {
                    $attributes['acc_date'] = date('Y-m-d H:i:s');

                }
            }

            $c = Validator::make($data, $rules);

            if ($c->fails()) {
                Session::flash('error', "ERROR");
                return $this->response->message($c->errors()->messages())
                    ->code(200)
                    ->status('error')
                    ->redirect();
            } else {
                if (!empty($attributes['price_range_min']) && !empty($attributes['price_range_max'])) {
                    $url = guard_url('restaurant/accounts/');
                    if ($attributes['price_range_min'] > $attributes['price_range_max']) {
                        return $this->response->message('Price range minimum should be less than price range maximum')
                            ->code(500)
                            ->url(guard_url('restaurant/accounts'))
                            ->redirect();
                    }
                }

                if (!empty($attributes['ac_no']) || !empty($attributes['IFSC']) || !empty($attributes['bank']) || !empty($attributes['GST_no'])) {

                    $url = guard_url('restaurant/billing/');
                }
                if (!empty($attributes['timings'])) {

                    $url = guard_url('restaurant/schedule/');
                }
                $restaurant->update($attributes);

                if (!empty($attributes['timings'])) {
                    foreach ($attributes['timings'] as $key => $value) {
                        foreach ($value as $x => $times) {
                            $count = DB::table('restaurant_timings')->where('day', $key)->where('restaurant_id', $restaurant->id)->where('id', $x)->count();
                            if ($count > 0) {
                                if ($times['start'] != null && $times['end'] != null) {
                                    if ($times['start'] < $times['end']) {
                                        $exists_edit = DB::table('restaurant_timings')->where('day', $key)->where('restaurant_id', $restaurant->id)->where('id', '<>', $x)->get();
                                        foreach ($exists_edit as $exists_edit_key => $exists_edit_value) {
                                            if (($times['start'] > $exists_edit_value->opening && $times['start'] < $exists_edit_value->closing) || ($times['end'] > $exists_edit_value->opening && $times['start'] < $exists_edit_value->closing)) {
                                                if (user()->hasRole('restaurant')) {
                                                    return $this->response->message('Overlapping of time. Please enter valid time')
                                                        ->code(400)
                                                        ->url($url)
                                                        ->redirect();
                                                } else {
                                                    return $this->response->message('Overlapping of time. Please enter valid time')
                                                        ->code(400)
                                                        ->status('error')
                                                        ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                                                        ->redirect();
                                                }
                                            }
                                        }
                                        $restaurant_timings = DB::table('restaurant_timings')->where('id', $x)->update(['restaurant_id' => $restaurant->id, 'day' => $key, 'opening' => $times['start'], 'closing' => $times['end']]);
                                    } else {
                                        if (user()->hasRole('restaurant')) {
                                            return $this->response->message('Opening time should be less than closing time')
                                                ->code(400)
                                                ->url($url)
                                                ->redirect();
                                        } else {
                                            return $this->response->message('Opening time should be less than closing time')
                                                ->code(400)
                                                ->status('error')
                                                ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                                                ->redirect();
                                        }
                                    }
                                } else {

                                    $count_key = DB::table('restaurant_timings')->where('day', $key)->where('restaurant_id', $restaurant->id)->count();
                                    if ($count_key == 1) {
                                        $restaurant_timings = DB::table('restaurant_timings')->where('id', $x)->update(['restaurant_id' => $restaurant->id, 'day' => $key, 'opening' => 'off', 'closing' => 'off']);
                                    } else {
                                        DB::table('restaurant_timings')->where('id', $x)->delete();
                                    }

                                }
                            } else {
                                if ($times['start'] != null && $times['end'] != null) {
                                    if ($times['start'] < $times['end']) {
                                        $exists = DB::table('restaurant_timings')->where('day', $key)->where('restaurant_id', $restaurant->id)->get();
                                        foreach ($exists as $exists_key => $exists_value) {
                                            if ($times['start'] < $exists_value->closing) {
                                                if (user()->hasRole('restaurant')) {
                                                    return $this->response->message('Overlapping of time. Please enter valid time')
                                                        ->code(400)
                                                        ->url($url)
                                                        ->redirect();
                                                } else {
                                                    return $this->response->message('Opening time should be less than closing time')
                                                        ->code(400)
                                                        ->status('error')
                                                        ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                                                        ->redirect();
                                                }
                                            }
                                        }
                                        $restaurant_timings = Restauranttimings::create(['restaurant_id' => $restaurant->id, 'day' => $key, 'opening' => $times['start'], 'closing' => $times['end'], 'user_id' => user_id(), 'user_type' => user_type()]);

                                    } else {
                                        if (user()->hasRole('restaurant')) {
                                            return $this->response->message('Opening time should be less than closing time')
                                                ->code(400)
                                                ->url($url)
                                                ->redirect();
                                        } else {
                                            return $this->response->message('Opening time should be less than closing time')
                                                ->code(400)
                                                ->status('error')
                                                ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                                                ->redirect();
                                        }
                                    }
                                } else {
                                    $restaurant_timings = Restauranttimings::create(['restaurant_id' => $restaurant->id, 'day' => $key, 'opening' => 'off', 'closing' => 'off', 'user_id' => user_id(), 'user_type' => user_type()]);
                                }
                            }
                        }
                    }
                }
            }
            if (user()->hasRole('restaurant')) {

                // Session::put('success', 'Your Account Information is securely saved!');
                // return $this->response->message(trans('Your Account Information is securely saved!', ['Module' => trans('restaurant::restaurant.name')]))
                // ->code(204)
                // ->url($url)
                // ->with('success','Item created successfully!')
                // ->redirect();
                return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::restaurant.name')]))
                    ->code(204)
                    ->url($url)
                    ->with('success', 'Item created successfully!')
                    ->redirect();
            }
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::restaurant.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the restaurant.
     *
     * @param Model   $restaurant
     *
     * @return Response
     */
    public function destroy(RestaurantRequest $request, Restaurant $restaurant)
    {
        try {

            $restaurant->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::restaurant.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('restaurant/restaurant/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/restaurant/' . $restaurant->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple restaurant.
     *
     * @param Model   $restaurant
     *
     * @return Response
     */
    public function delete(RestaurantRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::restaurant.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('restaurant/restaurant'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/restaurant'))
                ->redirect();
        }

    }

    /**
     * Restore deleted restaurants.
     *
     * @param Model   $restaurant
     *
     * @return Response
     */
    public function restore(RestaurantRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('restaurant::restaurant.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/restaurant/restaurant'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/restaurant/'))
                ->redirect();
        }

    }

    public function timing($count, $day)
    {
        return view('restaurant::admin.restaurant.timing', compact('count', 'day'));
    }
    public function publishRestauarant($key)
    {
        $restaurant = $this->repository->findByField('id', hashids_decode($key))->first();
        if ($restaurant->published == 'Published') {
            Restaurant::where('id', $restaurant->id)->update(['published' => 'Unpublished']);
            return redirect(guard_url('restaurant/restaurant/' . $key))->with('alert', 'Unpublished Successfully');
        } else {

            $days = DB::table('restaurant_timings')->where('restaurant_id', $restaurant->id)->where('opening', '<>', 'off')->where('closing', '<>', 'off')->pluck('day')->toArray();
            if ($restaurant->slug == null) {
                Restaurant::where('id', $restaurant->id)->update(['slug' => str_slug($restaurant->name, "-")]);
            }
            if ($restaurant->menu()->count() >= 1 && count(array_unique($days)) >= 1 && !empty($restaurant->logo) && !empty($restaurant->latitude) && !empty($restaurant->longitude)) {
                Restaurant::where('id', $restaurant->id)->update(['published' => 'Published']);
                return redirect(guard_url('restaurant/restaurant/' . $key))->with('alert', 'Published Successfully');
            } else {
                Restaurant::where('id', $restaurant->id)->update(['published' => 'Unpublished']);
                if (empty($restaurant->logo)) {
                    $msg = "Cannot published, logo must be added";
                    return redirect(guard_url('restaurant/restaurant/' . $key))->with('alert', $msg);
                }
                if (empty($restaurant->latitude) || empty($restaurant->longitude)) {
                    $msg = "Cannot published, location must be added";
                    return redirect(guard_url('restaurant/restaurant/' . $key))->with('alert', $msg);
                }
                if ($restaurant->menu()->count() < 1) {
                    $msg = "Cannot published, atleast 1 menu must be added";
                    return redirect(guard_url('restaurant/restaurant/' . $key))->with('alert', $msg);
                }

                if (count(array_unique($days)) < 1) {
                    $msg = "Cannot published, timing  must be entered for atleast one day";
                    return redirect(guard_url('restaurant/restaurant/' . $key))->with('alert', $msg);
                }
            }
        }
    }

    public function billing()
    {
        //    dd("hello");
        return $this->response->setMetaTitle('Account Details ')
            ->view('restaurant::default.restaurant.billing')
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }

    public function settings()
    {

        $restaurant = Restaurant::where('id', user()->id)->first();
        if ($restaurant->stop_date < Carbon::now() || is_null($restaurant->stop_date)) {
            $restaurant_stop = false;
        } else {
            $restaurant_stop = true;
        }
        // dd("hello");

        return $this->response->setMetaTitle('Account Settings ')
            ->view('restaurant::default.restaurant.settings')
            ->data(compact('restaurant_stop', 'restaurant'))
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }
    public function restaurantStopStart(RestaurantRequest $request, $id)
    {
        //dd($request->all());
        $data = [];
        if ($request->delivery) {
            $data['delivery'] = $request->delivery;
        }
        if (isset($request->non_contact)) {
            $data['non_contact'] = $request->non_contact;
        }
        if (isset($request->car_pickup)) {
            $data['car_pickup'] = $request->car_pickup;
        }
        if ($request->onoffswitch == 'on') {
            $data['kitchen_status'] = 'Yes';
            $data['stop_date'] = null;
        } else {
            $date = $request->stop_date;
            if ($date) {
                $data['kitchen_status'] = 'No';
                $data['stop_date'] = $date;
            }
        }
        $restaurant = Restaurant::where('id', hashids_decode($id))->update($data);
        return $this->response
            ->url(guard_url('restaurant/view-settings'))
            ->redirect();

    }

    public function accounts()
    {
        $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id', user_id())->orderBy('day')->orderBy('opening')->get();
        return $this->response->setMetaTitle('Restaurant Details')
            ->view('restaurant::default.restaurant.accounts')
            ->data(compact('restaurant_timings'))
            ->layout('default')
            ->output();
    }

    public function schedule()
    {
        $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id', user_id())->orderBy('day')->orderBy('opening')->get();
        return $this->response->setMetaTitle('Restaurant Schedule')
            ->view('restaurant::default.restaurant.schedule')
            ->data(compact('restaurant_timings'))
            ->layout('default')
            ->output();
    }
    public function loginAs($key)
    {
        if (Session::has('login_from_kitchen')) {
            Session::forget('login_from_kitchen');
        }
        $restaurant = $this->repository->findByField('id', hashids_decode($key))->first();
        Auth::guard('restaurant.web')->loginUsingId($restaurant->id);
        return redirect()->to(trans_url('restaurant'));
        //   return redirect()->intended(trans_url('restaurant/'));

    }

    public function getClientfavourites($type)
    {
        $date = date('Y-m-d', strtotime("-120 days"));
        if ($type == 'restaurants') {
            $favourites = Favourite::where('user_id', user_id())->where('type', 'restaurant')->whereDate('created_at', '>', $date)->pluck('favourite_id')->toArray();
            $restaurants = Restaurant::whereIn('id', $favourites)->get();
            return $this->response->setMetaTitle('Restaurant Schedule')
                ->view('restaurant::default.restaurant.user-favourites-restaurants')
                ->data(compact('restaurants'))
                ->layout('default')
                ->output();
        } else {
            $favourites = Favourite::where('user_id', user_id())->where('type', 'order')->whereDate('created_at', '>', $date)->pluck('favourite_id')->toArray();
            $orders = Order::whereIn('id', $favourites)->get();
            return $this->response->setMetaTitle('Restaurant Schedule')
                ->view('restaurant::default.restaurant.user-favourites')
                ->data(compact('orders'))
                ->layout('default')
                ->output();
        }
    }

    public function kitchenBind()
    {
        $data = json_decode(file_get_contents("http://api.memobird.cn/home/setuserbind?ak=a281e01078f44ccfa2ce31b17e92ad30&timestamp=2014-11-
14%2014:22:39&memobirdID=" . user()->memobirdID));
        Restaurant::where('id', user_id())->update(['printer_userID' => $data->showapi_userid, 'memobirdID' => user()->memobirdID, 'ak' => 'a281e01078f44ccfa2ce31b17e92ad30']);
        return redirect()->back();
    }

    public function updatemaster()
    {
        $this->repository->updateRestaurantMasters();

        echo 'Updation of all restaurants completed succesfully.';
    }

    public function transactionLog(RestaurantRequest $request)
    {

        $attributes = $request->all();
        if (empty($attributes['filter_date1'])) {
            $filter_date1 = date('Y-m-1 00:00:01');
        } else {
            $filter_date1 = $attributes['filter_date1'];
        }

        if (empty($attributes['filter_date2'])) {
            $filter_date2 = date('Y-m-d 23:59:59');
        } else {
            $filter_date2 = $attributes['filter_date2'];
        }
//dd(date('Y-m-d 00:00:01', strtotime($filter_date1)));
        $logs = Restaurant::select('restaurants' . '.name', 'restaurants' . '.id', DB::raw('count(orders.id) as orders'), DB::raw('sum(orders.total) as total_sales'), DB::raw('sum(orders.ZF) as ZTF'))
            ->leftJoin('orders', 'orders' . '.restaurant_id', 'restaurants' . '.id')
            ->where('restaurants.published', 'Published')
            ->whereDate('orders.delivery_time', '>', date('Y-m-d', strtotime('-1 day', strtotime($filter_date1))))
            ->whereDate('orders.delivery_time', '<=', date('Y-m-d', strtotime($filter_date2)))
            ->groupBy('restaurants.name', 'restaurants.id')->get();

        //return view('restaurant::admin.restaurant.transaction',compact('logs', 'view', 'filter_date1', 'filter_date2'));
        if ($request->ajax()) {
            return view('restaurant::admin.restaurant.transaction_filter', compact('logs', 'view', 'filter_date1', 'filter_date2'));
        } else {
            return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
                ->view('restaurant::restaurant.transaction', true)
                ->data(compact('logs', 'view', 'filter_date1', 'filter_date2'))
                ->output();
        }
    }

    public function website(RestaurantRequest $request)
    {
        return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
            ->view('restaurant::restaurant.restaurant_website', true)
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }

    public function websiteUpdate(RestaurantRequest $request, $id)
    {
        $attributes = $request->all();
        if (!array_key_exists('locations', $attributes)) {
            $attributes['locations'] = [];
        } else {
            $attributes['locations'] = array_values($attributes['locations']);
        }
        $restaurant = $this->repository->findByField('id', hashids_decode($id))->first();
        $restaurant->update($attributes);

        return redirect()->back();
    }

    public function viewAppSettings()
    {

        return $this->response->setMetaTitle('App Setup ')
            ->view('restaurant::default.restaurant.app_settings')
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }
    public function viewMenuSettings()
    {

        return $this->response->setMetaTitle('Menu Setup ')
            ->view('restaurant::default.restaurant.view_menu')
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }
    public function viewBillingSettings()
    {
        return $this->response->setMetaTitle('Billing Settings')
            ->view('restaurant::default.restaurant.view_billing')
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }
    public function viewWebsiteSettings()
    {
        return $this->response->setMetaTitle('Website Settings')
            ->view('restaurant::default.restaurant.view_website')
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }
    public function viewMarketingHubSettings()
    {
        return $this->response->setMetaTitle('Marketing Hub Settings')
            ->view('restaurant::default.restaurant.marketing_hub')
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }
    public function viewPushNotifications()
    {
        $new_notifications = Pushnotifications::where('restaurant_id', user()->id)->where('notification_sent_status', 0)->orderBy('created_at', 'DESC')->get();
        $past_notifications = Pushnotifications::where('restaurant_id', user()->id)->where('notification_sent_status', 1)->orderBy('created_at', 'DESC')->get();
        return $this->response->setMetaTitle('Push Notifications')
            ->view('restaurant::default.restaurant.pushnotification')
            ->data(compact('new_notifications', 'past_notifications'))
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }
    public function viewPromotions()
    {
        return $this->response->setMetaTitle('Website Promotions')
            ->view('restaurant::default.restaurant.promotions')
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }
    

    public function app(RestaurantRequest $request)
    {
        $app = Restaurantapp::where('restaurant_id', user()->id)->first();
        if ($app == null) {
            $user = Restaurantapp::create([
                'restaurant_id' => user()->id,
            ]);
            $app = Restaurantapp::where('restaurant_id', user()->id)->first();
        }
        return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
            ->view('restaurant::restaurant.restaurant_app', true)
            ->data(compact('app'))
            // ->theme('restaurant')
            ->layout('default')
            ->output();
    }

    public function appUpdate(RestaurantRequest $request, $id)
    {
        $attributes = $request->all();
        if ($request->point_percentage != null) {
            $points = ($request->point_percentage / 0.5) * 10;
        }
        if (array_key_exists('locations', $attributes)) {
            $attributes['locations'] = array_values($attributes['locations']);
            // dd($attributes['locations']);
          
            $attributes['locations'] = array_values($attributes['locations']);
            if(!empty($attributes['locations'])) {
                foreach($attributes['locations'] as $key => $locations){
                    $url = $locations['url'];
                    $attributes['locations'][$key]['url'] = str_replace("/restaurants/","/app-restaurants/",$url);
                 }
            }
        }

        // dd($attributes['featured_food']);
        $log = Restaurantapp::updateOrCreate(
            [
                'restaurant_id' => hashids_decode($id),
            ], [
                'title' => @$attributes['title'],
                'subtitle' => @$attributes['subtitle'],
                'meta_tags' => @$attributes['meta_tags'],
                'meta_keywords' => @$attributes['meta_keywords'],
                'meta_description' => @$attributes['meta_description'],
                'email' => @$attributes['customer_email'],
                'alternate_phone' => @$attributes['customer_phone'],
                'gallery' => @$attributes['gallery'],
                'featured_food' => @$attributes['featured_food'],
                'contact_info' => @$attributes['contact_info'],
                'locations' => @$attributes['locations'],
                'menus' => @$attributes['menus'],
                'slider_images' => @$attributes['slider_images'],
                'point_percentage' => @$attributes['point_percentage'],
                'points' => @$points,

            ]);
        $restaurant = $this->repository->findByField('id', hashids_decode($id))->first();
        $restaurant->update($attributes);

        return redirect()->back();
    }

    public function delcharge()
    {
        foreach ($restaurants as $restaurant) {
            if ($restaurant['delivery_charge'] != null && $restaurant['delivery_charge'] != 0 && $restaurant->delivery_limit != null && $restaurant->delivery == 'Yes') {
                $attributes['del_charge'] = array('1' => ['min_dist' => '0', 'max_dist' => $restaurant->delivery_limit, 'charge' => $restaurant->delivery_charge]);
                $restaurant->update($attributes);
            }
        }
    }
    public function deliveryStatus(RestaurantRequest $request)
    {
        $id = $request->rest_id;
        $attributes['delivery'] = $request->status;
        $result = Restaurant::where('id', hashids_decode($id))->update($attributes);
        return;
    }

    public function pivotReports(RestaurantRequest $request)
    {
        //$orders = $request->all();

        if ($request->ajax()) {

            $filter = $request->all();
            if (array_key_exists("last_day", $filter)) {
                $filter['last_day'] = 1;
            }
            if (array_key_exists("last_7_day", $filter)) {
                $filter['last_7_day'] = 1;
            }
            if (array_key_exists("month_to_date", $filter)) {
                $filter['month_to_date'] = 1;
            }
            if (array_key_exists("last_month", $filter)) {
                $filter['last_month'] = 1;
            }
            if (array_key_exists("last_6_avg", $filter)) {
                $filter['last_6_avg'] = 1;
            }
            if (array_key_exists("last_dow", $filter)) {
                $filter['last_dow'] = 1;
            }
            if (array_key_exists("published", $filter)) {
                $filter['published'] = 1;
            }

            $orders = $this->repository->getRestReports($filter);

            return $orders;
        }
        return view('restaurant::admin.restaurant.reports');

    }
}
