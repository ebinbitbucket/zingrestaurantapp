<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Restaurant\Http\Requests\MenuRequest;
use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
use Restaurant\Restaurant\Interfaces\CategoryRepositoryInterface;
use Restaurant\Restaurant\Models\Menu;
use Restaurant\Restaurant\Models\Category;
use Restaurant\Restaurant\Models\Addon;
use Restaurant\Kitchen\Models\Kitchen;
use DB;
use Illuminate\Http\Request;
use Restaurant\Restaurant\Models\Addonvariations;
use Restaurant\Master\Models\Master;
use Session;

/**
 * Resource controller class for menu.
 */
class MenuResourceController extends BaseController
{

    /**
     * Initialize menu resource controller.
     *
     * @param type MenuRepositoryInterface $menu
     *
     * @return null
     */
    public function __construct(MenuRepositoryInterface $menu,CategoryRepositoryInterface $category)
    {
        parent::__construct();
        $this->repository = $menu;
        $this->category = $category;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Restaurant\Repositories\Criteria\MenuResourceCriteria::class);
    }

    /**
     * Display a list of menu.
     *
     * @return Response
     */
    public function index(MenuRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Restaurant\Repositories\Presenter\MenuPresenter::class)
                ->paginate();
        }

        $menus = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('restaurant::menu.names'))
            ->view('restaurant::menu.index', true)
            ->data(compact('menus', 'view'))
            ->output();
    }

    /**
     * Display menu.
     *
     * @param Request $request
     * @param Model   $menu
     *
     * @return Response
     */
    public function show(MenuRequest $request, Menu $menu)
    {
        $addon_det = '';
        if ($menu->exists) {
            $view = 'restaurant::menu.show';  
            $menus = Menu::find($menu->id);
            $addon_det = DB::table('menu_addon')->select('menu_addon.*','restaurant_addons.name')->join('restaurant_addons','menu_addon'.'.addon_id','restaurant_addons'.'.id')->where('menu_id',$menu->id)->get();
        } else {
            $view = 'restaurant::menu.new';
        }
       
        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('restaurant::menu.name'))
            ->data(compact('menu','addon_det'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new menu.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(MenuRequest $request)
    {

        $menu = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('restaurant::menu.name')) 
            ->view('restaurant::menu.create', true) 
            ->data(compact('menu'))
            ->output();
    }

    /**
     * Create new menu.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(MenuRequest $request)
    {
        try {
            $attributes              = $request->all();
             if(!empty($attributes['catering'])){
                $attributes['catering'] = 'Yes';
            }
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            if(user()->hasRole('restaurant')){
                $attributes['restaurant_id'] = user_id();
            }
            $menu                 = $this->repository->create($attributes);

              if(!empty($attributes['addon_ids'])){
                if(count($attributes['addon_ids'])==1 && $attributes['addon_ids'][0]!=''){
                 $menus = Menu::find($menu->id); 
                 foreach ($attributes['addon_ids'] as $value) {
                    if(!empty($value)){
                    if(empty($attributes['av_'.$value]['required'])){
                        $attributes['av_'.$value]['required'] = "No";
                    }
                $menus->addon()->attach($value,['required' => $attributes['av_'.$value]['required'],'min' => $attributes['av_'.$value]['min'],'max' => $attributes['av_'.$value]['max'],'variation_list' => json_encode($attributes['av_'.$value]['variation_list']),'user_id' => $attributes['user_id'],'user_type' => $attributes['user_type']]);
                 }}
             }
            }
            if(user()->hasRole('restaurant')){
                return redirect()->back();
            }
            return $this->response->message(trans('messages.success.created', ['Module' => trans('restaurant::menu.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/menu/' . $menu->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/restaurant/menu'))
                ->redirect();
        }

    }

    /**
     * Show menu for editing.
     *
     * @param Request $request
     * @param Model   $menu
     *
     * @return Response
     */
    public function edit(MenuRequest $request, Menu $menu)
    {
        $addon_det = DB::table('menu_addon')->select('menu_addon.*','restaurant_addons.name')->join('restaurant_addons','menu_addon'.'.addon_id','restaurant_addons'.'.id')->where('menu_id',$menu->id)->get();
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('restaurant::menu.name'))
            ->view('restaurant::menu.edit', true)
            ->data(compact('menu','addon_det'))
            ->output();
    }

    /**
     * Update the menu.
     *
     * @param Request $request
     * @param Model   $menu
     *
     * @return Response
     */
    public function update(MenuRequest $request, Menu $menu)
    {
        try {
            $attributes = $request->all();
            if(isset($attributes['is_check'])){

            if(isset($attributes['stock_status'])){
                $attributes['stock_status'] = 1;
            }
            else{
                $attributes['stock_status'] = 0;
            }
        }

            if(!empty($attributes['catering'])){
                $attributes['catering'] = 'Yes';
            }
            else{
                $attributes['catering'] = 'No';
            }
            // if(empty($attributes['menu_variations'])){
            //         $attributes['menu_variations'] = null;
            // }             
             $menu->update($attributes);
            if(!empty($attributes['addon_ids'])){ 
                if($attributes['addon_ids'][0] != ''){
               $addons =  array_unique($attributes['addon_ids']);
                $count = 0;
                $menus = Menu::find($menu->id);
                foreach ($attributes['addon_ids'] as $value) {
                    $count = DB::table('menu_addon')->where(['addon_id'=>$value,'menu_id'=>$menu->id])->count(); 

                     if($count <= 0)
                    {
                        if(empty($attributes['av_'.$value]['required'])){
                        $attributes['av_'.$value]['required'] = "No";
                    }
                        $message = $menus->addon()->attach($value, ['required' => $attributes['av_'.$value]['required'],'min' => $attributes['av_'.$value]['min'],'max' => $attributes['av_'.$value]['max'],'variation_list' => json_encode($attributes['av_'.$value]['variation_list'])]);
                    }

                    else{
                        if(!empty($attributes['av_'.$value]['variation_list'])){
                            if(empty($attributes['av_'.$value]['required'])){
                        $attributes['av_'.$value]['required'] = "No";
                    }
                            $message = DB::table('menu_addon')->where('menu_id',$menu->id)->where('addon_id',$value)->update(['required' => $attributes['av_'.$value]['required'],'min' => $attributes['av_'.$value]['min'],'max' => $attributes['av_'.$value]['max'],'variation_list' => json_encode($attributes['av_'.$value]['variation_list'])]);
                        }
                        else{
                            DB::table('menu_addon')->where('menu_id',$menu->id)->where('addon_id',$value)->delete();
                        }
                        
                    }
                }
            }
            }
           if(user()->hasRole('restaurant')){
                return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::menu.name')]))
                ->code(204)
                ->status('success')
                ->redirect();
            }
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::menu.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/menu/' . $menu->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/menu/' . $menu->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the menu.
     *
     * @param Model   $menu
     *
     * @return Response
     */
    public function destroy(MenuRequest $request, Menu $menu)
    {
        try {

            $menu->delete();
            if(user()->hasRole('restaurant')){
                return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::menu.name')]))
                ->code(202)
                ->status('success')
                ->redirect();
            }
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::menu.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('restaurant/menu/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/menu/' . $menu->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple menu.
     *
     * @param Model   $menu
     *
     * @return Response
     */
    public function delete(MenuRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::menu.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('restaurant/menu'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/menu'))
                ->redirect();
        }

    }

    /**
     * Restore deleted menus.
     *
     * @param Model   $menu
     *
     * @return Response
     */
    public function restore(MenuRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('restaurant::menu.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/restaurant/menu'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/menu/'))
                ->redirect();
        }

    }

      public function menuItems()
    { 
        
        if(guard() == 'kitchen.web'){
            $restaurant= Kitchen::find(user_id());
            $categories = $this->category->with('menu')->orderBy('order_no')->findByField('restaurant_id',$restaurant->restaurant_id);
        $menuss = $this->repository->newInstance([]);
        return $this->response->setMetaTitle('Restaurant Menu')
            ->view('restaurant::default.restaurant.kitchen.menuitems')
            ->data(compact('categories','menuss'))
            ->layout('default')
            ->output();
        } else if(Session::has('login_from_kitchen')){
        $categories = $this->category->with('menu')->orderBy('order_no')->findByField('restaurant_id',user_id());
           $menuss = $this->repository->newInstance([]);
            return $this->response->setMetaTitle('Restaurant Menu')
                ->view('restaurant::default.restaurant.kitchen.menuitems')
                ->data(compact('categories','menuss'))
                ->layout('default')
                ->output();
        }else{
            $categories = $this->category->with('menu')->orderBy('order_no')->findByField('restaurant_id',user_id());
            $menuss = $this->repository->newInstance([]);
            return $this->response->setMetaTitle('Restaurant Menu')
            ->view('restaurant::default.restaurant.menuitems')
            ->data(compact('categories','menuss'))
            ->theme('restaurant')
            ->layout('default')
            ->output();
        }
        
    }
    public function menuItemStatus(){
        $categories = $this->category->with('menu')->orderBy('order_no')->findByField('restaurant_id',user_id());
           $menuss = $this->repository->newInstance([]);
            return $this->response->setMetaTitle('Restaurant Menu')
                ->view('restaurant::default.restaurant.menuitemstatus')
                ->data(compact('categories','menuss'))
                ->theme('restaurant')
                ->layout('default')
                ->output();
    }

    public function menu_edit()
    { 
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.menu.menu_edit')
            ->layout('default')
            ->output();
    }

    public function menu_add()
    { 
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.menu.menu_add')
            ->layout('default')
            ->output();
    }

    public function removeVariation(MenuRequest $request,$id,$key)
    { 
        $id = hashids_decode($id);
        $request = $request->all();
        $variations = $request['variations'];
        unset($variations[$key]);
        Menu::where('id',$id)->update(['menu_variations' => json_encode($variations)]);
        return redirect()->back();
        
    }

    public function removeAddon(MenuRequest $request,$id,$addon_id)
    { 
        $id = hashids_decode($id);
        $request = $request->all(); 
        DB::table('menu_addon')->where('menu_id',$id)->where('addon_id',$addon_id)->delete();
        return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::menu.name')]))
                ->code(202)
                ->status('success')
                ->redirect();
        
    }
    
     public function menuItems_category_list()
    { 
       dd("hello");
        $categories = $this->category->orderBy('order_no')->findByField('restaurant_id',user_id());
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_category_list')
            ->theme('restaurant')
            ->layout('default')
            ->output();
    }

    public function menuItems_category_edit($category_id)
    { 
        $edit_cats = $this->category->findByField('id',$category_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_category_edit')
            ->layout('default')
            ->data(compact('edit_cats'))
            ->output();
    }
     public function menuItems_menu_add()
    { 
        $menuss = $this->repository->newInstance([]);
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_menu_add')
            ->layout('default')
            ->data(compact('menuss'))
            ->output();
    }
      public function menuItems_menu_import()
    { 
        $menuss = $this->repository->newInstance([]);
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_menu_import')
            ->layout('default')
            ->data(compact('menuss'))
            ->output();
    }
    public function menuItems_menu_edit($menu_id)
    { 
        $edit_menu = $this->repository->findByField('id',$menu_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_menu_edit')
            ->layout('default')
            ->data(compact('edit_menu'))
            ->output();
    }

      public function menuItems_menuschedule_edit($menu_id)
    { 
        $edit_menu = $this->repository->findByField('id',$menu_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_menu__schedule_edit')
            ->layout('default')
            ->data(compact('edit_menu'))
            ->output();
    }
       public function menuItems_menuoffer_edit($menu_id)
    { 
        $edit_menu = $this->repository->findByField('id',$menu_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_menu__offer_edit')
            ->layout('default')
            ->data(compact('edit_menu'))
            ->output();
    }
      public function menuItems_menu_detail($menu_id)
    { 
        $menuItem = $this->repository->findByField('id',$menu_id)->first();
        if(guard() == 'kitchen.web'){
            return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.kitchen.menuitems_menu_detail')
            ->layout('default')
            ->data(compact('menuItem'))
            ->output();
        }
            else{
                return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_menu_detail')
            ->layout('default')
            ->data(compact('menuItem'))
            ->output();
            }
        
    }
     public function menuItems_addon_add($menu_id)
    { 
        $edit_addonmenu = $this->repository->findByField('id',$menu_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_addon_add')
            ->layout('default')
            ->data(compact('edit_addonmenu'))
            ->output();
    }
     public function menuItems_addon_edit($menu_id,$addon_id)
    { 
        $edit_menu_addon = $this->repository->findByField('id',$menu_id)->first();
        $edit_addon = $edit_menu_addon->addon->where('id',$addon_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_addon_edit')
            ->layout('default')
            ->data(compact('edit_menu_addon','edit_addon'))
            ->output();
    }

       public function menuItems_variation_add($menu_id)
    { 
        $edit_varmenu = $this->repository->findByField('id',$menu_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_variation_add')
            ->layout('default')
            ->data(compact('edit_varmenu'))
            ->output();
    }
     public function menuItems_variation_edit($menu_id,$key)
    { 
        $editmenuvar = $this->repository->findByField('id',$menu_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.restaurant.menuitems_variation_edit')
            ->layout('default')
            ->data(compact('editmenuvar','key'))
            ->output();
    }

    public function import_menus(Request $request)
    {

        try{

         $attributes              = $request->all();
         
        foreach($request->csv_file as $file)
            {          
              $awsUrl=env('AWS_S3_URL');     
                $csv_data = array_map('str_getcsv', file($awsUrl.'/uploads/'.$file['path']));
            //  $csvFile = file('https://zingmyorder.s3.amazonaws.com/uploads/restaurant/menu/2020/05/12/085905685/csv_file/KqjXb0NJ4ELOOk8S37OUSN68UJr2EwZyGlUsYLuy.txt');
              array_walk($csv_data, function(&$a) use ($csv_data) {
                $a = array_combine($csv_data[0], $a);
                    });
                array_shift($csv_data); 
            
            }
         foreach($csv_data as $key=>$data)
            {
                $variations = array();

                if(!empty($data['Variations'])){
                    $vars= explode(';', $data['Variations']);
                    foreach ($vars as $var_key => $var_value) {
                        $vars_details = explode(',', $var_value);
                        $a1 = array('name' => $vars_details[0],'price' => $vars_details[1],'description' => $vars_details[2]);
                        $variations[]=$a1;
                    }
                    
                }

                $menu_variations =  $variations ;

                $category = Category::firstOrCreate(['name' => $data['Category'],'restaurant_id' => user_id(),'user_id' => user_id(), 'user_type' => user_type()],['preparation_time' => "15"]);
                $exists = Menu::where('restaurant_id',user_id())->where('category_id',$category->id)->where('name',$data['Name'])->count();

                if($exists < 1){
                    if(!is_numeric($data['Price'])){
                        return $this->response->message('Price Field should be numeric.')
                                            ->code(400)
                                            ->url(guard_url('restaurant/menu_items'))
                                            ->redirect();
                    }

                    $menu    = $this->repository->create(['restaurant_id'=> user_id(), 'category_id' => $category->id, 'name' => $data['Name'], 'description' => $data['Details'], 'price' => $data['Price'], 'serves' => $data['Serves'], 'catering' => !empty($data['Catering']) ? $data['Catering'] : 'No', 'menu_variations' => $menu_variations,'user_id' => user_id(), 'user_type' => user_type()]);
                    
                    if(!empty($data['Addons'])){
                        $var_addonlist = explode('|', $data['Addons']) ;  
                        $addon=array();
                        $var_addon=array();
                        $var_addonvariations=array();
                        $addon_vars=array(); 
                        foreach ($var_addonlist as $key_addonlist => $value_addonlist) {
                            $addon_variations = array();
                            $addon_details =array();
                            $addon_var=array();
                            $addon_array =array();

                            $var_addon = explode(',', $value_addonlist);
                            $var_addonvariations=implode(',', array_slice($var_addon, 4));
                            $addon = Addon::firstOrCreate(['name' => $var_addon[0],'restaurant_id' => user_id()]);
                            $addon_vars= explode(';', $var_addonvariations);
                            foreach ($addon_vars as $addon_key => $addon_value) { 
                                if(!empty($addon_value)){
                                    $addon_details = explode(',', $addon_value);
                                    $addon_var = Addonvariations::firstOrCreate(['addon_id' => $addon->id,'name' => $addon_details[0],'price' => (!empty($addon_details[1])) ? $addon_details[1] : '0','description' => (!empty($addon_details[2])) ? $addon_details[2] : '','user_id' => user_id(),'user_type' => user_type()]);
                                    $addon_array = array('name' => $addon_details[0],'price' => (!empty($addon_details[1])) ? $addon_details[1] : '0','description' => (!empty($addon_details[2])) ? $addon_details[2] : '','id' => $addon_var->id);
                                      $addon_variations[]=$addon_array;
                                  }
                            } 
                            $menus = Menu::find($menu->id); 

                            $menus->addon()->attach($addon->id,['required' => $var_addon[3],'min' => $var_addon[1],'max' => $var_addon[2],'variation_list' => json_encode($addon_variations),'user_id' => user_id(),'user_type' => user_type()]);
                        }
                    
                    }
                }


               

                
            }

           // dd('ddd');

             if(user()->hasRole('restaurant')){


                return redirect()->back();
            }
            return $this->response->message(trans('messages.success.created', ['Module' => trans('restaurant::menu.name')]))
                ->code(204)
                ->status('success')
                //->url(guard_url('restaurant/menu/' . $menu->getRouteKey()))
                ->redirect();

        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
               // ->url(guard_url('/restaurant/menu'))
                ->redirect();
        }
    }

    
     public function status_update($id,$status){
        $menu = Menu::find($id);
        if($status == 'on'){
            Menu::where('id',$menu->id)->update(['stock_status' => 1]);
        }
        else{
            Menu::where('id',$menu->id)->update(['stock_status' => 0]);
        }
        return redirect()->back();
   }

     public function variation_status_update($menu_id,$key,$status){
        $menu = Menu::find($menu_id);
        $menu_variations = $menu->menu_variations;
        if($status == 'on'){
            $menu_variations[$key]['stock_status'] = 1;
        }
        else{
            $menu_variations[$key]['stock_status'] = 0;
        }
        Menu::where('id',$menu->id)->update(['menu_variations' => json_encode($menu_variations)]);

        return redirect()->back();
   }

   public function menu_schedule_form($count)
   {
       return view('restaurant::default.menu.menu_schedule', compact('count'));
   }
   
    public function downloadCSV(Request $request, $filter = []) {
		empty($filter) ? '' : parse_str($filter, $filter);
		$fields = ['id', 'restaurant_id', 'name', 'category_id','master_category'];

		if (isset($filter['search'])) {
			$filter = array_only($filter['search'], $fields);
		} else {
			$filter = [];
		}

		$menus = $this->repository->getAjax($filter);
		$fields = ['id' => 'Id','master_category' => 'Master','name' => 'Menu', 'category_id' => 'Category', 'restaurant_id' => 'Restaurant', 'location' => 'Location'];

		$header = implode(',', $fields);
		$body = '';
		//header('Content-Type: text/csv; charset=utf-8');
		//header('Content-Disposition: attachment; filename=Menus.csv');

		foreach ($menus as $key => $menu) {
			$row = array_only($menus, array_keys($fields));

			foreach ($fields as $hk => $hv) {

				if ($hk == 'id') {
					$body .= "\"" . @$menu['id'] . "\",";
					continue;
				}
				if ($hk == 'name') {
					$body .= "\"" . @$menu['name'] . "\",";
					continue;
				}
                if ($hk == 'master') {
                    $body .= "\"" . @$menu['master']['name'] . "\",";
                    continue;
                }
				if ($hk == 'category_id') {
					$body .= "\"" . @$menu['categories']['name'] . "\",";
					continue;
				}
				if ($hk == 'restaurant_id') {
					$body .= "\"" . @$menu['restaurant']['name'] . "\",";
					continue;
				}
				if ($hk == 'location') {
					$body .= "\"" . @$menu['restaurant']['address'] . "\"";
					continue;
				}
                

				$body .= "\"" . @$row[$hk] . "\",";
			}
			$body .= "\n";
		}
		echo $header . "\n";
		echo $body . "\n";
    }
    public function downloadMenuCSV(Request $request, $id = []) {
        $menus = $this->repository->getResMenu(hashids_decode($id));
        
		$fields = ['id' => 'Id','category_id' => 'Category','name' => 'Name','price' => 'Price',  'description' => 'Details', 'serves' => 'Serves','variations' => 'Variations','addons' => 'Addons'];

		$header = implode(',', $fields);
		$body = '';
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=Menus.csv');

		foreach ($menus as $key => $menu) {
            $row = array_only($menus, array_keys($fields));

            $addon = $this->repository->getAddonById($menu['id']);


			foreach ($fields as $hk => $hv) {

				if ($hk == 'id') {
					$body .= "\"" . @$menu['id'] . "\",";
					continue;
                }
                if ($hk == 'category_id') {
					$body .= "\"" . @$menu['categories']['name'] . "\",";
					continue;
				}
				if ($hk == 'name') {
					$body .= "\"" . @$menu['name'] . "\",";
					continue;
                }
                if ($hk == 'description') {
					$body .= "\"" . @$menu['description'] . "\",";
					continue;
                }
                if ($hk == 'price') {
					$body .= "\"" . @$menu['price'] . "\",";
					continue;
                }
                if ($hk == 'serves') {
					$body .= "\"" . @$menu['serves'] . "\",";
					continue;
                }
                if ($hk == 'variations') {
                    $var=[];
                    $variations='';
                    if(isset($menu['menu_variations'])){
                        foreach($menu['menu_variations'] as $variation){

                         $var[]=implode(",", [$variation['name'],$variation['price'],$variation['description']]);    
                        }
                        $variations=implode(";", $var);
                        //implode(",", $var);    
                       //dd($var);
                    }
					$body .= "\"" . @$variations . "\",";
					continue;
				}
               
                if ($hk == 'addons') {
                    $verient_final ='';
                    $verient=[];
                    if($addon){
                        foreach($addon as $add){
                        if(isset($add->variation_list)){
                            $variation_list=json_decode($add->variation_list);
                            $var=[];
                          $variations='';
                            foreach($variation_list as $variation){
                             $var[]=implode(",", [$variation->name,$variation->price,$variation->description]);    
                            }
                           // dd($add);
                            $variations=implode(";", $var);
                            $data=implode(",", [$add->name,$add->min,$add->max,$add->required]);    
                             $verient[]=$data . "," . $variations.";";

                        }   
                    }
                       
                    }

           $verient_final=implode("|", $verient);
					$body .= "\"" . @$verient_final . "\",";
					continue;
                }
                

				$body .= "\"" . @$row[$hk] . "\",";
			}
			$body .= "\n";
		}
		echo $header . "\n";
		echo $body . "\n";
	}
	
	    public function getimportMaping() {
        $menu = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('restaurant::menu.name'))
            ->view('restaurant::menu.importmapping', true)
            ->data(compact('menu'))
            ->output();
    }

    public function postimportMapping(Request $request) {
        $attributes = $request->all();

        foreach ($request->csv_file as $file) {
            $csv_data = array_map('str_getcsv', file(storage_path('uploads/' . $file['path'])));
            array_walk($csv_data, function (&$a) use ($csv_data) {
                $a = array_combine($csv_data[0], $a);
            });
            array_shift($csv_data);

        }
        foreach ($csv_data as $key => $value) {
           
           $menu = Menu::where('id', $value['ID'])->first();
                    if (!empty($menu)) {         

                        $master = Master::where('name', $value['Master'])->first();
                         if (!empty($master)) {
                            $menu_pp = $menu->update(['master_category' => $master->id]);
                        } 
                        // else {
                        //     return $this->response->message('Master Not Exist')
                        //         ->code(400)
                        //         ->url(guard_url('restaurant/menu'))
                        //         ->redirect();
                        // }
                    } 
                    // else {
                    //     return $this->response->message('Menu Not Exist')
                    //         ->code(400)
                    //         ->url(guard_url('restaurant/menu'))
                    //         ->redirect();
                    // }
        }
        return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::menu.name')]))
            ->status("success")
            ->code(202)
            ->url(guard_url('restaurant/menu'))
            ->redirect();

    }
    
    
    public function updatesearchtext(){
        // DB::select('UPDATE restaurant_menus JOIN restaurant_categories B ON restaurant_menus.category_id = B.id JOIN restaurants re on restaurant_menus.restaurant_id=re.id SET search_text = CONCAT(B.name,",",IFNULL(CONCAT(re.type,","),""),restaurant_menus.name)');
        DB::select('UPDATE restaurant_menus 
            JOIN restaurant_categories B ON restaurant_menus.category_id = B.id 
            JOIN restaurants re on restaurant_menus.restaurant_id=re.id 
            SET search_text = CONCAT(B.name," , ",IFNULL(re.type," ")," , ",restaurant_menus.name)');
        echo 'Updation Completed Succesfully.';
    }

}
