<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
use Illuminate\Http\Request;
use Restaurant\Restaurant\Models\Restaurant;
use Restaurant\Restaurant\Models\Restaurantapp;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Mail;
use Session;
class RestaurantAppController extends BaseController
{
    // use ReviewWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Review\Interfaces\ReviewRepositoryInterface $review
     *
     * @return type
     */
    public function __construct(RestaurantRepositoryInterface $restaurant)
    {
        $this->repository = $restaurant;
        parent::__construct();
    }

    /**
     * Show review's list.
     *
     * @param string $slug
     *
     * @return response
     */
     protected function restaurantApp($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
        'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $weekday = strtolower(date('D'));
        $restaurant =  $this->repository->scopeQuery(function($query) use ($slug) {
                return $query->orderBy('id','DESC')
                             ->where('id', $slug)
                             ->with([
                                'menu',
                                'offers',
                                'review',
                                'review.customer',
                                'menu.master',
                                
                            ]);
            })->first(['*']);

      $restaurant->menu = @$restaurant->menu->unique('category_id');
      $restaurant->timings = $restaurant->timings->groupBy('day');
      
      $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
      $restaurant->theme_design = 'Default';

      return $this->response->setMetaTitle($app->title)
      ->setMetaKeyword($app->meta_tags)
      ->setMetaTag($app->meta_keywords)
      ->setMetaDescription($app->meta_description)
        ->view('app.home')
        ->theme('app')
        ->layout('default')
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday', 'app'))
        ->output();
  }

   protected function about($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');
      $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
      $restaurant->theme_design = 'Default';
      

      return $this->response->setMetaTitle($app->title)
       ->setMetaKeyword($app->meta_tags)
      ->setMetaTag($app->meta_keywords)
      ->setMetaDescription($app->meta_description)
        ->view('app.about')
        ->theme('app')
        ->layout('default')
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday', 'app'))
        ->output();
  }

  protected function menu($slug)
    {
        $weekday=[];
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
   
       $weekday = strtolower(date('D'));
        $restaurant =  $this->repository->scopeQuery(function($query) use ($slug) {
                return $query->orderBy('id','DESC')
                             ->where('id', $slug)
                             ->with([
                                'category'=> function ($q) {
                                    $q->orderBy('order_no','ASC');
                                },
                                'popular_category'  => function ($q) {
                            $q->where('popular_dish_no', '<>', 0)
                                ->orderBy('popular_dish_no', 'DESC');
    
                            // $query->OrWhere('catering', 'No')
                            //       ->OrWhereNull('catering', 'No');
    
                        },
                                'menu',
                                'offers',
                                'review',
                                'review.customer',
                                'menu.master',
                                'catering_category' => function ($q) {
                                    $q->whereHas('menu', function (Builder $query) {
                                        $query->where('catering', 'Yes');
                                    });
                                }
                            ]);
            })->first(['*']);


        $restaurant->menu = $restaurant->menu->groupBy('category_id');
        $catering_catergories = $restaurant->catering_category;
        $restaurant->timings = $restaurant->timings->groupBy('day');

        $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
        $restaurant->theme_design = 'Default';
      

      return $this->response->setMetaTitle($app->title)
       ->setMetaKeyword($app->meta_tags)
      ->setMetaTag($app->meta_keywords)
      ->setMetaDescription($app->meta_description)
        ->view('app.menu')
        ->theme('app')
        ->layout('default')
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday', 'catering_catergories', 'app'))
        ->output();
  }

    protected function gallery($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
          'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');

     $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
      $restaurant->theme_design = 'Default';
     

      return $this->response->setMetaTitle($app->title)
       ->setMetaKeyword($app->meta_tags)
      ->setMetaTag($app->meta_keywords)
      ->setMetaDescription($app->meta_description)
        ->view('app.gallery')
        ->theme('app')
        ->layout('default')
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday', 'app'))
        ->output();
  }

   protected function contact($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
        'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');

      $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
        $restaurant->theme_design = 'Default';
    

      return $this->response->setMetaTitle($app->title)
       ->setMetaKeyword($app->meta_tags)
      ->setMetaTag($app->meta_keywords)
      ->setMetaDescription($app->meta_description)
        ->view('app.contact')
        ->theme('app')
        ->layout('default')
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday', 'app'))
        ->output();
  }


  protected function locations($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');

      $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
      $restaurant->theme_design = 'Default';
      

      return $this->response->setMetaTitle($app->title)
      ->setMetaKeyword($app->meta_tags)
      ->setMetaTag($app->meta_keywords)
      ->setMetaDescription($app->meta_description)
        ->view('app.locations')
        ->theme('app')
        ->layout('default')
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday', 'app'))
        ->output();
  }

   protected function sendMail(Request $request, $slug)
    {
      $attributes = $request->all();
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');
    
      if($restaurant->customer_email != null){
              Mail::send('design1.mail', ['restaurant' => $restaurant,'attributes' => $attributes], function ($message) use ($restaurant,$attributes) {
                       $message->from($attributes['cf_email']);
                       $message->to($restaurant->customer_email)->subject('Website Enquiry');
            });
      }
      
      // Session::flash('message', 'This is a message!');
      return redirect(url('domain/contact/'.$restaurant->id))->with('message', 'There was an error...');
     
  }
  
}
