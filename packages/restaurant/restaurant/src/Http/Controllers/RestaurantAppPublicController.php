<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
use Restaurant\Restaurant\Models\Restaurant;
use Restaurant\Restaurant\Models\Restaurantapp;
use Session;

class RestaurantAppPublicController extends BaseController
{
    // use ReviewWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Review\Interfaces\ReviewRepositoryInterface $review
     *
     * @return type
     */
    public function __construct(RestaurantRepositoryInterface $restaurant)
    {
        $this->repository = $restaurant;
        parent::__construct();
    }

    /**
     * Show review's list.
     *
     * @param string $slug
     *
     * @return response
     */
   
    protected function restaurantAppView($slug)
    {
        $restaurant = $this->repository->findByField('id', $slug)->first();
        $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
        return $this->response->setMetaTitle(@$app->title)
            ->setMetaKeyword(@$app->meta_tags)
            ->setMetaTag(@$app->meta_keywords)
            ->setMetaDescription(@$app->meta_description)
            ->view('app.app')
            ->theme('app')
            ->layout('app')
            ->data(compact('restaurant'))
            ->output();
    }
}
