<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Restaurant\Interfaces\AddonRepositoryInterface;

class AddonPublicController extends BaseController
{
    // use AddonWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Addon\Interfaces\AddonRepositoryInterface $addon
     *
     * @return type
     */
    public function __construct(AddonRepositoryInterface $addon)
    {
        $this->repository = $addon;
        parent::__construct();
    }

    /**
     * Show addon's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $addons = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$restaurant::addon.names'))
            ->view('restaurant::addon.index')
            ->data(compact('addons'))
            ->output();
    }


    /**
     * Show addon.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $addon = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$addon->name . trans('restaurant::addon.name'))
            ->view('restaurant::addon.show')
            ->data(compact('addon'))
            ->output();
    }

}
