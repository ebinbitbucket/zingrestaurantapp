<?php

namespace Restaurant\Restaurant\Http\Controllers;
use App\Http\Controllers\PublicController as BaseController;
use Illuminate\Http\Request;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
use Restaurant\Restaurant\Models\Restaurant;
use Restaurant\Master\Models\Master;
use Restaurant\Restaurant\Models\Restauranttimings;
use Restaurant\Restaurant\Models\Favourite;
use DB;
use App\Client;
use Carbon\Carbon;
use Session;
use DateTime;
use Auth;
use Restaurant\Master\Models\Category;
use Restaurant\Restaurant\Models\Category as RestaurantCategory;
use Illuminate\Database\Eloquent\Builder;
use Laraecart\Cart\Facades\Cart;
use Request as Requests;
use Laraecart\Cart\Models\TransactionLogs;
use Cache;

class RestaurantPublicController extends BaseController
{
    // use RestaurantWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface $restaurant
     *
     * @return type
     */
    public function __construct(RestaurantRepositoryInterface $restaurant,MenuRepositoryInterface $menu)
    {
        
        if (!empty(app('auth')->getDefaultDriver())) {
           $this->middleware('auth:' . app('auth')->getDefaultDriver(), ['only' => ['reviewAdd','addToFavourite']]); 

       }
        $this->repository = $restaurant;
        $this->menu = $menu;
        parent::__construct();
    }

    /**
     * Show restaurant's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index(Request $request, $slug = '')
    {
        return redirect()->to(url('restaurants/mapview'));
        $search=$request->search; 
        if(!empty($search['working_hours']) && $search['time_type'] == 'scheduled'){
            $search['working_date'] = date('Y-m-d',strtotime($search['working_date']));
            $date = new DateTime($search['working_date']);
                $time = new DateTime($search['working_hours']);
                $combined = new DateTime($date->format('Y-m-d') .' ' .$time->format('H:i:s'));
            Session::put('search_time', $combined->format('d-M-Y h:i A'));
            Session::put('search_type', 'scheduled');
            $working_date = $search['working_date'] ;
            $working_hours = $search['working_hours'] ;

            }
            elseif(!empty($search['time_type']) && $search['time_type'] == 'asap'){
                $search['working_date'] = date('Y-m-d',strtotime($search['working_date']));
                $date = new DateTime($search['working_date']);
                $time = new DateTime($search['working_hours']);
                $combined = new DateTime($date->format('Y-m-d') .' ' .$time->format('H:i:s'));
                Session::put('search_time', $combined->format('d-M-Y h:i A'));
                Session::put('search_type', 'asap');
            }
            
            elseif(Session::get('search_type')){
                $search['working_date'] = Session::get('search_time');
                $search['working_hours'] =Session::get('search_time');
            }
        $restaurants = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->pushCriteria('\Restaurant\Restaurant\Repositories\Criteria\RestaurantPublicCriteria')
        ->scopeQuery(function($query) use ($search,$request){
        if(!empty($search['working_hours'])){
              $query= Restaurant::pricemin($search);
              
            }
            if(!empty($search['price_range_min']) || !empty($search['price_range_max'])){
                Session::put('price_range_min', $search['price_range_min']);
                Session::put('price_range_max', $search['price_range_max']);
                 $search['price_range_min'] =   filter_var($search['price_range_min'], FILTER_SANITIZE_NUMBER_INT);
                 $search['price_range_max'] =   filter_var($search['price_range_max'], FILTER_SANITIZE_NUMBER_INT);

                 $query = $query->where('price_range_min', '<=', $search['price_range_max'])->where('price_range_max', '>=', $search['price_range_min']);

            }
            
            if(!empty($search['type'])){

                // $types = implode('|', $search['type']);
                Session::put('types', $search['type']); 
                $types = str_replace(",","|",$search['type']);
                 $query = $query->where('type', 'rlike', $types); 

            }
            else{
                Session::put('types', 'All'); 
            }
            if(!empty($search['rating'])){
                Session::put('rating', $search['rating']);
                 $query = $query->where('rating', '>=', $search['rating']); 

            }
            if(!empty($search['rating'])){
                Session::put('rating', $search['rating']);
                 $query = $query->where('rating', '>=', $search['rating']); 

            }
            if(!empty($search['delivery'])){
                    $query = $query->where('delivery', 'like', $search['delivery']); 
                     Session::put('delivery', 'delivery');
            }
            else{
                Session::put('delivery', 'pickup');
            }
            if(empty($search['category_name']) && !empty(Session::get('category_code'))){
                $search['category_name'] = Session::get('category_code');
            }

              if(!empty($search['category_name'])){
                $query = $query->where('category_name',$search['category_name']);
                $category_name = Category::find($search['category_name'])->name;
                    // switch ($search['category_name']) {
                    //     case 'all':
                    //             // $query = $query->whereNotNull('category_name'); 
                    //             $category_name = 'All';
                    //         break;
                    //     case 'restaurant':
                    //             $query = $query->where('category_name',$search['category_name']); 
                    //             $category_name = 'Restaurant';
                    //         break;
                    //     case 'bakery':
                    //             $query = $query->where('category_name',$search['category_name']); 
                    //             $category_name = 'Bakery';
                    //         break;
                    //     case 'food_truck':
                    //             $query = $query->where('category_name',$search['category_name']); 
                    //             $category_name = 'Food Truck';
                    //         break;
                    //     case 'farmers_market':
                    //             $query = $query->where('category_name',$search['category_name']); 
                    //             $category_name = 'Farmers Market';
                    //         break;
                    //     case 'catering':
                    //             $query = $query->where('category_name',$search['category_name']); 
                    //             $category_name = 'Catering';
                    //         break;
                        
                    // }
            Session::put('category_name', $category_name);
            Session::put('category_code', $search['category_name']);
        }

            if(empty($search['latitude']) && !empty(Session::get('latitude')) && !empty(Session::get('longitude'))){

                $restaurant_ids = DB::select(
               'SELECT id FROM
                    (SELECT id, (' . 3959 . ' * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) *
                    cos(radians(longitude) - radians(' . Session::get('longitude') . ')) +
                    sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude))))
                    AS distance
                    FROM restaurants) AS distances
                WHERE distance < ' . 5 . '
                ORDER BY distance
                ;
            ');
            $result = [];
            foreach ($restaurant_ids as $key => $value) {
                $result[$key] = $value->id;
            }
            $query = $query->whereIn('id',$result); 
            }
            if(!empty($search['address']) && !empty($search['latitude']) && !empty($search['longitude'])){
            $location = $search['address'];
            Session::put('location', $search['address']);
            if(!empty($search['latitude']) && !empty($search['longitude'])){
            Session::put('latitude', $search['latitude']);
            Session::put('longitude', $search['longitude']);
            $restaurant_ids = DB::select(
               'SELECT id FROM
                    (SELECT id, (' . 3959 . ' * acos(cos(radians(' . $search['latitude'] . ')) * cos(radians(latitude)) *
                    cos(radians(longitude) - radians(' . $search['longitude'] . ')) +
                    sin(radians(' . $search['latitude'] . ')) * sin(radians(latitude))))
                    AS distance
                    FROM restaurants) AS distances
                WHERE distance < ' . 5 . '
                ORDER BY distance
                ;
            ');
            $result = [];
            foreach ($restaurant_ids as $key => $value) {
                $result[$key] = $value->id;
            }
            $query = $query->whereIn('id',$result); 
        } }
        
           return  $query->where('published','Published')->orderBy('id','DESC');
        })->paginate();

        return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
            ->view('restaurant::restaurant.index')
            ->data(compact('restaurants','location','working_hours'))
            ->output();
    }


    /**
     * Show restaurant.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show(Request $request, $slug)
    { 
        if(isset($request->client_id)){
            $user = Client::find(hashids_decode($request->client_id));
            // Auth::login($user);
            // Auth::loginUsingId($user->id);
            Auth::guard('restaurant.web')->loginUsingId($request->client_id);
        }

        // dd(user());
    
        $dayOfTheWeek=[];  
            // $this->theme->asset()->container('footer')->usepath()->add('manifest', 'dist/js/manifest.js');
            // $this->theme->asset()->container('footer')->usepath()->add('vendor', 'dist/js/vendor.js');
            // $this->theme->asset()->container('footer')->usepath()->add('app', 'dist/js/app.js');
            // $this->theme->asset()->container('footer')->usepath()->add('plugins', 'js/plugins.js');
            // $this->theme->asset()->container('footer')->usepath()->add('theme', 'js/theme.min.js');
            // $this->theme->asset()->container('footer')->add('loader', 'https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js');
            
            // $this->theme->asset()->container('footer')->usepath()->add('map-data', 'js/map-data.js');

        $weekMap = [
            'sun' => 'Sunday',
            'mon' => 'Monday',
            'tue' => 'Tuesday',
            'wed' => 'Wednesday',
            'thu' => 'Thursday',
            'fri' => 'Friday',
            'sat' => 'Saturday',
        ]; 
        $weekday = strtolower(date('D'));
        $restaurant =  $this->repository->scopeQuery(function($query) use ($slug) {
                return $query->orderBy('id','DESC')
                             ->where('slug', $slug)
                             ->with([
                                'category'=> function ($q) {
                                    $q->orderBy('order_no','ASC');
                                },
                                'popular_category'  => function ($q) {
                            $q->where('popular_dish_no', '<>', 0)
                                ->orderBy('popular_dish_no', 'DESC');
    
                            // $query->OrWhere('catering', 'No')
                            //       ->OrWhereNull('catering', 'No');
    
                        },
                                'menu',
                                'offers',
                                'review',
                                'review.customer',
                                'menu.master',
                                'timings' => function ($q) {
                                    $q->orderBy('day')->orderBy('opening');
                                },
                                'catering_category' => function ($q) {
                                    $q->whereHas('menu', function (Builder $query) {
                                        $query->where('catering', 'Yes');
                                    });
                                }
                            ]);
            })->first(['*']);


        $restaurant->menu = @$restaurant->menu->groupBy('category_id');
        $restaurant->timings = @$restaurant->timings->groupBy('day');
        // $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id',$restaurant->id)->orderBy('day')->orderBy('opening')->get();
        if(Auth::user()){
            $restaurant_fav = 0;

            // $restaurant_fav = Favourite::where('favourite_id',$restaurant->id)->where('type','restaurant')->where('user_id',user_id())->count();
        }
        else{
            $restaurant_fav = 0;
        }
        $catering_catergories = $restaurant->catering_category;
        $attributes['user_id'] = user_id();
                    $attributes['type'] = 'eateryview';
                    $attributes['ip_address'] = request()->ip();
                    $attributes['date'] = date('Y-m-d H:i:s');
                    $attributes['restaurant_id'] = $restaurant->id;
                    $attributes['url'] = Requests::path();
                    $attributes['server_variables'] = Requests::server();
		            $attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
                    $useragent                   = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            $attributes['source'] = 'mobile';
        } else {
            $attributes['source'] = 'desktop';
        }
        $log = TransactionLogs::updateOrCreate(
			[
				'date'=>date('Y-m-d',strtotime($attributes['date'])),
				'type'=>$attributes['type'],
				'ip_address'=>$attributes['ip_address'],
				'restaurant_id'=>$attributes['restaurant_id'],
				
			],[
				'user_id'=>$attributes['user_id'],
				'type'=>$attributes['type'],
				'ip_address'=>$attributes['ip_address'],
				'date'=>date('Y-m-d',strtotime($attributes['date'])),
				'restaurant_id'=>$attributes['restaurant_id'],
				'url'=>$attributes['url'],
				'server_variables'=>$attributes['server_variables'],
				'HTTP_REFERER'=>$attributes['HTTP_REFERER'],
				'source'=>$attributes['source'],
				
            ]);
        // $log = TransactionLogs::create($attributes);
        echo '<!-- ' . date("F Y h:i:s u") . '-->';
        $this->response->theme->asset()->container('extra')->usepath()->add('eateryview', 'js/eateryview.js');
        return $this->response->setMetaTitle($restaurant->name .' '. trans('restaurant::restaurant.name'))
            ->view('restaurant::restaurant.show')
            ->layout('blank')
            ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday','restaurant_fav','catering_catergories'))
            ->output();
        echo '<!-- ' . date("F Y h:i:s u") . '-->';
    } 
   public function restaurantReviews(Request $request,$slug){
       $start=$request->start;
       $limit=$request->limit;

    $restaurant =  $this->repository->scopeQuery(function($query) use ($slug,$start,$limit) {
        return $query->orderBy('id','DESC')
                     ->where('slug', $slug)
                     ->with([
                        'review'=> function ($q) use($start,$limit) {
                            $q->skip($start)->take($limit)->orderBy('id','DESC');
                        },
                        'review.customer'                        
                    ]);
                     
    })->first(['*']);
    //$restaurant_review = $restaurant->review->skip(2)->slice(2);
    //dd($restaurant->review);
    if(count(@$restaurant->review)>0){
        return view('restaurant::restaurant.reviews', compact('restaurant'))->render();

    }




   }
     protected function mapview(Request $request){
        $this->response->theme->asset()->add('gmap', 'https://maps.googleapis.com/maps/api/js?key='.config('services.GOOGLE_API'));
        //$this->response->theme->asset()->add('gmap3', '/themes/public/assets/js/gmap3.min.js');

        //   $default_loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
        $this->response->theme->asset()->container('extra')->usepath()->add('mapview', 'js/mapview.js');
        $search=$request->search;   

     
        
                $search['working_date'] = date('Y-m-d');
                $search['working_hours'] = date('H:i:s');

       
        if(empty(Session::get('latitude'))){
                //   Session::put('latitude',$default_loc['geoplugin_latitude']) ;
                //   Session::put('longitude',$default_loc['geoplugin_longitude'])  ;
                Session::put('latitude',32.7554883) ;
                   Session::put('longitude',-97.3307658)  ;
            }
        $restaurants = $this->repository
        ->orderBy('published')
        ->orderBy('distance')
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->pushCriteria('\Restaurant\Restaurant\Repositories\Criteria\RestaurantPublicCriteria')
        ->scopeQuery(function($query) use ($search,$request){
            
            if(empty($search['latitude']) && !empty(Session::get('latitude')) && !empty(Session::get('longitude'))){

                $restaurant_ids = DB::select(
               'SELECT id FROM
                    (SELECT id, (' . 6367  . ' * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) *
                    cos(radians(longitude) - radians(' . Session::get('longitude') . ')) +
                    sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude))))
                    AS distance
                    FROM restaurants) AS distances
                WHERE distance < ' . 10 . '
                ORDER BY distance
                ;
            ');
            $result = [];
            foreach ($restaurant_ids as $key => $value) {
                $result[$key] = $value->id;
            }
            $query = $query->whereIn('id',$result); 
            }
            if(!empty($search['address']) && !empty($search['latitude']) && !empty($search['longitude'])){
            $location = $search['address'];
            Session::put('location', $search['address']);
            if(!empty($search['latitude']) && !empty($search['longitude'])){
            Session::put('latitude', $search['latitude']);
            Session::put('longitude', $search['longitude']);
            $restaurant_ids = DB::select(
               'SELECT id FROM
                    (SELECT id, (' . 3959  . ' * acos(cos(radians(' . $search['latitude'] . ')) * cos(radians(latitude)) *
                    cos(radians(longitude) - radians(' . $search['longitude'] . ')) +
                    sin(radians(' . $search['latitude'] . ')) * sin(radians(latitude))))
                    AS distance
                    FROM restaurants) AS distances
                WHERE distance < ' . 10 . '
                ORDER BY distance
                ;
            ');
            $result = [];
            foreach ($restaurant_ids as $key => $value) {
                $result[$key] = $value->id;
            }
            $query = $query->whereIn('id',$result); 
        } 
    }
        // if(!empty($search['working_hours'])){
        //       $query= Restaurant::pricemin($search);
              
        //     }
            // if(!empty($search['price_range_min']) || !empty($search['price_range_max'])){
            //     Session::put('price_range_min', $search['price_range_min']);
            //     Session::put('price_range_max', $search['price_range_max']);
            //      $search['price_range_min'] =   filter_var($search['price_range_min'], FILTER_SANITIZE_NUMBER_INT);
            //      $search['price_range_max'] =   filter_var($search['price_range_max'], FILTER_SANITIZE_NUMBER_INT);
            // }
            // else{
            //     $search['price_range_min'] =   0;
            //      $search['price_range_max'] =   30;
            // }  
            // $query = $query->where('price_range_min', '<=', $search['price_range_max'])->where('price_range_max', '>=', $search['price_range_min']);
            if(!empty($search['type'])){

                // $types = implode('|', $search['type']);
                Session::put('types', $search['type']); 
                $types = str_replace(",","|",$search['type']);
                 $query = $query->where('type', 'rlike', $types); 

            }
            else{
                Session::put('types', 'All'); 
                Session::forget('cuisine_types');
            }
            // if(!empty($search['rating'])){
            //     Session::put('rating', $search['rating']);
            //      $query = $query->where('rating', '>=', $search['rating']); 

            // }
            // else{
            //     $query = $query->where('rating', '>=', 1); 
            // }
            // if(!empty($search['delivery'])){
            //         $query = $query->where('delivery', 'like', $search['delivery']); 
            //          Session::put('delivery', 'delivery');
            // }
            // else{
            //     Session::put('delivery', 'pickup');
            // }
            // if(empty($search['category_name']) && !empty(Session::get('category_code'))){
            //     $search['category_name'] = Session::get('category_code');
            // }

            //   if(!empty($search['category_name']) ){
            //     if($search['category_name'] != 'all'){
            //         $query = $query->where('category_name',$search['category_name']);
            //     $category_name = Category::find($search['category_name'])->name;
            //     Session::put('category_name', $category_name);
            // Session::put('category_code', $search['category_name']);
            //     }
            //     else{
            //         Session::put('category_name', 'All Eateries');
            // Session::put('category_code', 'all');
            //     }
                    
            
        // }


                
        //     }
            
                //             if (!empty($search['chain_list'])) {

                //     if ($search['chain_list'] != 'all') {
                //         $query           = $query->where('chain_list', $search['chain_list']);
                //         $chain_list_name = 'No';
                //         Session::put('category_name', $chain_list_name);
                //         Session::put('chain_list', $search['chain_list']);
                //     } else {
                //         Session::put('category_name', 'All');
                //         Session::put('chain_list', 'all');
                //     }

                // } else {
                //     $query           = $query->where('chain_list', 'No');
                //     $chain_list_name = 'No';
                //     Session::put('category_name', $chain_list_name);
                //     Session::put('chain_list', 'No');
                // }
            
           return  $query->whereIn('published',['Published','No Sale'])->where('rating', '>=', 2)->where('price_range_min', '<=', 5)->where('price_range_max', '>=', 1)->where('chain_list', '=', 'No')->whereNotNull('published');
        })->paginate(25, ['*',\DB::raw("(3959  * acos(cos( radians(latitude) ) * cos( radians( ".Session::get('latitude')." ) ) * cos( radians( ".Session::get('longitude')." ) - radians(longitude) ) + sin( radians(latitude) ) * sin( radians( ".Session::get('latitude')." ) )) ) as distance"),\DB::raw("(IFNULL(price_range_min,0)+IFNULL(price_range_max,0))/2 as price_avg")]);
        // ->paginate(25);
  //$restaurants = $restaurants['Published']->merge($restaurants['No Sale']);dd($restaurants->count());
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::restaurant.mapview')
            ->data(compact('restaurants'))
            ->layout('map-view')
            ->output();
    }

     protected function reviewAdd($slug)
    {
         $weekMap = [
            0 => 'sun',
            1 => 'mon',
            2 => 'tue',
            3 => 'wed',
            4 => 'thu',
            5 => 'fri',
            6 => 'sat',
        ]; 
        $dayOfTheWeek = Carbon::now()->dayOfWeek;    
        $weekday = $weekMap[$dayOfTheWeek];
        // $restaurant = $this->repository->scopeQuery(function($query) use ($slug) {
        //     return $query->orderBy('id','DESC')
        //                  ->where('slug', $slug);
        // })->first(['*']);
        $restaurant = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug)
                         ->with([
                            'category'=> function ($q) {
                                $q->whereHas('menu', function (Builder $query) {
                                    $query->where('catering','<>', 'Yes');
                                // $query->OrWhere('catering', 'No')
                                //       ->OrWhereNull('catering', 'No');
                                });
                            },
                            'menu',
                            'menu.master',
                            'timings' => function ($q) {
                                $q->orderBy('day')->orderBy('opening');
                            },
                            'catering_category' => function ($q) {
                                $q->whereHas('menu', function (Builder $query) {
                                    $query->where('catering', 'Yes');
                                });
                            }
                        ]);
        })->first(['*']);
        if(Auth::user()){
                        $restaurant_fav = 0;

            // $restaurant_fav = Favourite::where('favourite_id',$restaurant->id)->where('type','restaurant')->where('user_id',user_id())->count();
        }
        else{
            $restaurant_fav = 0;
        }
        $restaurant->menu = $restaurant->menu->groupBy('category_id');
        $restaurant->timings = $restaurant->timings->groupBy('day');
        $catering_catergories = $restaurant->catering_category;
        // $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id',$restaurant->id)->orderBy('day')->orderBy('opening')->get();
        return $this->response->setMetaTitle($restaurant->name . trans('restaurant::restaurant.name'))
            ->view('restaurant::restaurant.show')
            ->layout('blank')
            ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday','restaurant_fav','catering_catergories'))
            ->output();
    } 


     protected function restaurantListing(Request $request)
    {
        $search = $request->all();
        $restaurants = $this->repository->restaurantMasterList($search);
        $masters = Session::get('selected_masters[]');

        $masters = $this->repository->scopeQuery(function ($query) use ($masters) {
            return $query->orderBy('id', 'DESC')
                ->whereIn('id', $masters);
        })->get();

        $date = $search['working_date'];
        $data = view('master::public.master.show_new_restaurants', compact('restaurants', 'date'))->render();
        return response()->json(['status' => 'Success', 'new_menus' => $data]);
// Delete below codes
        $search          = $request->all();
        // $data            = $this->menu->getMenuIdsByMaster1($search);
        // $restaurants_ids = $this->menu->getRestaurantsByMaster($data);
        $masters =Master::orderBy('id', 'DESC')
                ->whereIn('id', Session::get('selected_masters[]'))->get();
        $str = "";
        foreach ($masters as $key => $value) {
            if($key !=0 ){
                // $str =$str. ',';
                $str =$str. 'and';
            }
            // $str =$str."\"$value->name\"";
            $str =$str." restaurant_menus.restaurant_id in (SELECT restaurant_menus.restaurant_id FROM `restaurant_menus` join masters on masters.id=restaurant_menus.master_category AND masters.name = \"$value->name\") ";
            
        }
        // foreach ($masters as $key => $value) {
        //     if($key !=0 ){
        //         $str =$str. ',';
        //     }
        //     $str =$str."\"$value->name\"";
            
            
        // }
        // $restaurants = Restaurant::getRestaurantsByMaster();
        $query = 'SELECT id FROM `restaurants` WHERE id IN((SELECT id FROM restaurants WHERE  (6367 * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) < 10)) AND id IN (select distinct restaurant_menus.restaurant_id from restaurant_menus where '.$str.' and restaurant_menus.restaurant_id in (SELECT restaurant_id FROM `restaurant_menus` join masters m1 on m1.id=restaurant_menus.master_category)) and restaurants.published in ("Published","No Sale")';

         // $query = 'SELECT id FROM `restaurants` WHERE id IN((SELECT id FROM restaurants WHERE  (6367 * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) < 10)) AND id IN (select distinct restaurant_menus.restaurant_id from restaurant_menus where '.$str.' and restaurant_menus.restaurant_id in (SELECT restaurant_id FROM `restaurant_menus` join masters m1 on m1.id=restaurant_menus.master_category AND m1.name = "Vada")) and restaurants.published in ("Published","No Sale")';

        // $query = 'SELECT id FROM `restaurants` WHERE id IN((SELECT id FROM restaurants WHERE  (3959  * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) < 10)) AND id IN (SELECT restaurant_menus.restaurant_id FROM `restaurant_menus` join restaurant_categories on restaurant_menus.category_id=restaurant_categories.id where restaurant_menus.name IN ('.$str.') and restaurant_categories.deleted_at IS null Group BY restaurant_id HAVING count(restaurant_menus.id) >='.count(Session::get("selected_masters[]")).' and count(distinct restaurant_menus.name) = '.count(Session::get("selected_masters[]")).')';
        
        // 'SELECT id FROM `restaurants` WHERE id IN((SELECT id FROM restaurants WHERE  (6367 * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) < 10)) AND id IN (SELECT restaurant_id FROM `restaurant_menus` where name IN ('.$str.') Group BY restaurant_id HAVING count(id) >='.count(Session::get("selected_masters[]")).')';
        

        $restaurants = DB::select($query); 
        $restaurants_ids = [];
        foreach($restaurants as $restaurant){
            $restaurants_ids[] = $restaurant->id;
        }
        if (!empty($search['time_type'])) {

            if ($search['time_type'] == 'scheduled') {
                $search['working_date'] = date('Y-m-d', strtotime($search['working_date']));
                $search['working_hours'] = date('H:i:s', strtotime($search['working_hours']));
                $date                   = new DateTime($search['working_date']);
                $time                   = new DateTime($search['working_hours']);
                $combined               = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));

            } elseif ($search['time_type'] == 'asap') {
                $search['working_date']  = date('Y-m-d');
                $search['working_hours'] = date('H:i:s');
                $date                    = new DateTime($search['working_date']);
                $time                    = new DateTime($search['working_hours']);
                $combined                = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));

            }
            //  else { 
            //      $date                    = new DateTime(date('Y-m-d'));
            //     $time                    = new DateTime(date('H:i:s'));
            //     $combined                = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));
            // }

        } 

         if(!empty($search['sort_filter'])){
            if($search['sort_filter'] == 'distance'){
                $order_filter = 'distance'; 
             $order_filter_order = 'ASC';
            }
                
            elseif($search['sort_filter'] == 'pricelh'){
                $order_filter = 'price_avg'; 
             $order_filter_order = 'ASC';
            }
             elseif($search['sort_filter'] == 'pricehl'){
                $order_filter = 'price_avg'; 
             $order_filter_order = 'DESC';
            }   
            elseif($search['sort_filter'] == 'rating'){
                 $order_filter = 'rating'; 
                $order_filter_order = 'DESC';
            }
            elseif($search['sort_filter'] == 'name'){
                 $order_filter = 'name'; 
                $order_filter_order = 'ASC';
            }
               
        }
        else{
            $order_filter = 'distance'; 
            $order_filter_order = 'ASC';
        }
        $restaurants = $this->repository
        ->orderBy('published')
        ->orderBy($order_filter,$order_filter_order)
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->pushCriteria('\Restaurant\Restaurant\Repositories\Criteria\RestaurantPublicCriteria')
            ->scopeQuery(function ($query) use ($search, $request, $restaurants_ids) {
              

                    if (!empty($search['working_hours']) && $search['time_type'] != 'all') {
                        $query = Restaurant::pricemin($search, $restaurants_ids);

                    }
                    if(!empty($search['priceMob'])){
                        $search['price'] = $search['priceMob'];
                    }
              
                if(!empty($search['price'])){
                    if($search['price'] == 1){ 
                        $query = $query->where('price_range_min', '<=', 5)->where('price_range_max', '>=', 0);
                    }
                    if($search['price'] == 5){
                        $query = $query->where('price_range_min', '<=', 12)->where('price_range_max', '>=', 5);
                    }
                    if($search['price'] == 12){
                        $query = $query->where('price_range_min', '<=', 20)->where('price_range_max', '>=', 12);
                    }
                    if($search['price'] == 20){
                        $query = $query->where('price_range_min', '>=', 20);
                    }
                }
                if (!empty($search['type'])) {

                    if($search['type'] == 'pickup') {
                       $query = $query->whereIn('published',['Published','No Sale'])->whereIn('delivery',  ['Yes', 'No']);

                    }elseif($search['type'] == 'orderpickup'){
                        $query = $query->where('published', '=', 'Published')->whereIn('delivery',  ['Yes', 'No']);
                    }elseif($search['type'] == 'orderdelivery'){
                         $query = $query->where('published', '=', 'Published')->where('delivery', 'like', 'Yes');
                    }

                } 
                

                // if (!empty($search['delivery'])) {

                //     if ($search['delivery'] == 'Yes') {
                //         $query = $query->where('delivery', 'like', $search['delivery']);
                //     }
                //     elseif ($search['delivery'] == 'No'){ 
                //         $query = $query->whereIn('delivery',  ['Yes', 'No']);
                //         //$query = $query;
                //     }

                // }


                if (!empty($search['rating'])) {
                    $query = $query->where('rating', '>=', $search['rating']);
                }

                if (!empty($search['main_category'])) {

                    if($search['main_category'] != 'all' && $search['main_category']  == 5){ 
                        $category_name = Category::find($search['main_category'])->name;
                        $query = $query->where( function( $query ) use ( $search ) {
                                    $query-> where('catering', 'Yes');
                                      $query->  orWhere('category_name', $search['main_category']);
                                    });
                    }
                    elseif ($search['main_category'] != 'all') {
                        $category_name = Category::find($search['main_category'])->name;
                        $query = $query->where('category_name', $search['main_category']);
                    } else {
                        Session::forget('category_id');
                    }

                } 
               
                if (!empty($search['chain_list'])) {

                   if ($search['chain_list'] != 'all') {

                        $query = $query->where('chain_list', $search['chain_list']);
                    } else {
                        $query = $query;
                    }

                } 
               return $query->whereIn('id', $restaurants_ids);
                // ->orderBy('id', 'DESC');
            })->paginate(null, ['*',\DB::raw("(3959 * acos(cos( radians(latitude) ) * cos( radians( ".Session::get('latitude')." ) ) * cos( radians( ".Session::get('longitude')." ) - radians(longitude) ) + sin( radians(latitude) ) * sin( radians( ".Session::get('latitude')." ) )) ) as distance"),\DB::raw("(IFNULL(price_range_min,0)+IFNULL(price_range_max,0))/2 as price_avg")]);
          $date = $search['working_date'];
        $data = view('master::public.master.show_new_restaurants', compact('restaurants', 'date'))->render();
        return response()->json(['status' => 'Success', 'new_menus' => $data]);
   } 

    public function addToFavourite($slug,$status){
        $restaurant = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);
        if($status == 'on'){
            Favourite::create(['user_id' => user_id(),'favourite_id' => $restaurant->id,'type' => 'restaurant']);
        }
        else{
            $favourite = Favourite::where('favourite_id',$restaurant->id)->where('type','restaurant')->first();
            $favourite->forceDelete();
        }
        return redirect()->back();
    }
   
   protected function restaurantMapListing(Request $request)
    {
        $search = $request->all(); 
        if (!empty($search['time_type'])) {

            if ($search['time_type'] == 'scheduled') {
                $search['working_date'] = date('Y-m-d', strtotime($search['working_date']));
                $search['working_hours'] = date('Y-m-d', strtotime($search['working_hours']));
               

            } elseif ($search['time_type'] == 'asap') {
                $search['working_date']  = date('Y-m-d');
                $search['working_hours'] = date('H:i:s');
                
            } 

        } 

        if(!empty($search['sort_filter'])){
            if($search['sort_filter'] == 'distance'){
                $order_filter = 'distance'; 
             $order_filter_order = 'ASC';
            }
                
            elseif($search['sort_filter'] == 'pricelh'){
                $order_filter = 'price_avg'; 
             $order_filter_order = 'ASC';
            }
             elseif($search['sort_filter'] == 'pricehl'){
                $order_filter = 'price_avg'; 
             $order_filter_order = 'DESC';
            }   
                
            elseif($search['sort_filter'] == 'rating'){
                 $order_filter = 'rating'; 
                $order_filter_order = 'DESC';
            }
               
        }
        else{
            $order_filter = 'distance'; 
            $order_filter_order = 'ASC';
        } 
        $restaurants = $this->repository
        ->orderBy('published')
        ->orderBy($order_filter,$order_filter_order)
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        // ->pushCriteria('\Restaurant\Restaurant\Repositories\Criteria\RestaurantPublicCriteria')
            ->scopeQuery(function ($query) use ($search, $request) {
                 if (!empty($search['maxDistance'])) {      
                    $search['maxDistance'] = $search['maxDistance'];
                }else{
                    $search['maxDistance'] = 10;
                }
                if (empty($search['latitude']) && !empty(Session::get('latitude')) && !empty(Session::get('longitude'))) {

                    $restaurant_ids = $this->repository
                        ->getByLatLng(Session::get('latitude'),
                            Session::get('longitude'), null, $search['maxDistance']);
                }
                $search['address'] = Session::get('location');
                $search['latitude'] = Session::get('latitude');
                $search['longitude'] = Session::get('longitude');
                if (!empty($search['address']) && !empty($search['latitude']) && !empty($search['longitude'])) {
                    $location = $search['address'];
                    Session::put('location', $search['address']);

                    if (!empty($search['latitude']) && !empty($search['longitude'])) {
                        Session::put('latitude', $search['latitude']);
                        Session::put('longitude', $search['longitude']);
                        $restaurant_ids = $this->repository->getByLatLng($search['latitude'], $search['longitude'], null, $search['maxDistance']);
                    }

                }

                $result = [];

                foreach ($restaurant_ids as $key => $value) {
                    $result[$key] = $value->id;
                }

                $query = $query->whereIn('id', $result);
                if (!empty($search['name'])) {
                    $query = $query
                                ->whereRaw('lower(name) like (?)', ["%" . strtolower($search['name']) . "%"]);
                                // ->whereRaw("UPPER(name) LIKE '%". strtoupper($search['name'])."%'");
                    //  where('name', 'LIKE', '%'.$search['name'].'%');
                    return $query->whereIn('published', ['Published', 'No Sale'])->orderBy('id', 'DESC');

                }
                

                    if (!empty($search['working_hours']) && $search['time_type'] != 'all') {
                        $query = Restaurant::pricemin($search, $result); 

                    }

                    if(!empty($search['priceMob'])){
                        $search['price'] = $search['priceMob'];
                    }
                  if(!empty($search['price'])){
                    if($search['price'] == 1){
                        $query = $query->where('price_range_min', '<=', 5)->where('price_range_max', '>=', 0);
                    }
                    if($search['price'] == 5){
                        $query = $query->where('price_range_min', '<=', 12)->where('price_range_max', '>=', 5);
                    }
                    if($search['price'] == 12){
                        $query = $query->where('price_range_min', '<=', 20)->where('price_range_max', '>=', 12);
                    }
                    if($search['price'] == 20){
                        $query = $query->where('price_range_min', '>=', 20);
                    }
                }
       

               
              if (!empty($search['delivery'])) {

                    if($search['delivery'] == 'pickup') {
                       $query = $query->whereIn('published',['Published','No Sale'])->whereIn('delivery',  ['Yes', 'No']);

                    }elseif($search['delivery'] == 'orderpickup'){
                        $query = $query->where('published', '=', 'Published')->whereIn('delivery',  ['Yes', 'No']);
                    }elseif($search['delivery'] == 'orderdelivery'){
                         $query = $query->where('published', '=', 'Published')->where('delivery', 'like', 'Yes');
                    }

                } 
             
                //  if (!empty($search['delivery'])) {              
                //     if ($search['delivery'] == 'Yes') {
                //         $query = $query->where('delivery', 'like', $search['delivery']);
                //     }
                //     elseif ($search['delivery'] == 'No'){ 
                //         $query = $query->whereIn('delivery',  ['Yes', 'No']);
                //     }
                //  }
               

                if (!empty($search['rating'])) {
                    $query = $query->where('rating', '>=', $search['rating']);
                }

            
            
                if (!empty($search['type'])) {
                    // $types = implode('|', $search['type']);
                    // Session::put('cuisine_types', implode(',', $search['type']));
                    Session::put('cuisine_types', $search['type']);
                    

                } elseif (!empty(Session::get('cuisine_types'))) {
                    $search['type'] = Session::get('cuisine_types');
                   
                }
                if (!empty($search['type'])) {
                    $types = implode('|', $search['type']);
                    $types = str_replace(",", "|", $types);
                    $query = $query->where('type', 'rlike', $types);
                }

            

                 if (!empty($search['main_category'])) {
                    if($search['main_category'] != 'all' && $search['main_category']  == 5){
                        $category_name = Category::find($search['main_category'])->name;
                        $query = $query->where('catering', 'Yes')->orWhere('category_name', $search['main_category']);
                    }
                    elseif ($search['main_category'] != 'all') {
                        $category_name = Category::find($search['main_category'])->name;
                        $query = $query->where('category_name', $search['main_category']);
                    }

                } 
           
                if (!empty($search['chain_list'])) {
                    if ($search['chain_list'] != 'all') {
                        // if ($search['chain_list'] == 'Yes') {
                        //     $chain_list_name = 'Chain';
                        // } else {
                        //     $chain_list_name = 'Local';
                        // }

                        $query = $query->where('chain_list', $search['chain_list']);
                    }

                } 
           return  $query->whereNotNull('published');
        })->paginate(25, ['*',\DB::raw("(3959  * acos(cos( radians(latitude) ) * cos( radians( ".Session::get('latitude')." ) ) * cos( radians( ".Session::get('longitude')." ) - radians(longitude) ) + sin( radians(latitude) ) * sin( radians( ".Session::get('latitude')." ) )) ) as distance"),\DB::raw("(IFNULL(price_range_min,0)+IFNULL(price_range_max,0))/2 as price_avg")]);
        if (!empty($search['maxDistance'])) {      
            $maxDistance = $search['maxDistance'];
        }else{
            $maxDistance = 10;
        }
        $data = view('restaurant::public.restaurant.filtered_map_view', compact('restaurants', 'maxDistance'))->render();
        //return $data;
        return response()->json(['status' => 'Success', 'new_menus' => $data]);
   } 

   
    protected function index1(){

        $restaurants = $this->repository->scopeQuery(function($query) {
            return $query->orderBy('name','ASC');
        })->paginate(100);

        return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
            ->view('restaurant::restaurant.restaurant-list')
            ->layout('blankcheckout')
            ->data(compact('restaurants'))
            ->output();
    }

    protected function show1($var = null){

        if($var == 'more'){ 
            $restaurants = $this->repository->scopeQuery(function($query) use($var) {
            return $query->where('name', 'regexp', '^[0-9]+')
            //->orWhere('name', 'REGEXP', '[$&+@#|]')
            ->orderBy('name','ASC');
            })->paginate(100);
        }else{ 
            $restaurants = $this->repository->scopeQuery(function($query) use($var) {
            return $query->where('name', 'LIKE', $var.'%')->orderBy('name','ASC');
            })->paginate(100);
        }

        return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
            ->view('restaurant::restaurant.restaurant-list')
            ->layout('blankcheckout')
            ->data(compact('restaurants'))
            ->output();
    }
    
     protected function loadTimeToCheckout($id = null, $date = null){
        $restaurant = $this->repository->findByField('id', $id)->first();
        $weekday = strtolower(date('D', strtotime($date))); 
        $timings = $restaurant->timings->where('day', $weekday);
        $preparation_time = 10;
        $carts = Cart::content();

		foreach ($carts as $item) {

			foreach ($item->options as $keys => $optiond) {

				if ($keys == 'preparation_time') {

					if (is_numeric($optiond) && $optiond > $preparation_time) {
						$preparation_time =  $optiond;
					}
				}
			}
        }
        return view('restaurant::restaurant.restaurant_timing', compact('timings','restaurant','date','preparation_time'));
    }
    
     protected function VoteSubmit($id = null){
        $success = 1;
        $restaurant = $this->repository->findByField('id', $id)->first();
        $votes = $restaurant->votes;
        $ip = \Request::ip();
        $data[0]['date'] = date('Y-m-d H:i:s');
        $data[0]['ip_address'] = $ip; 
        if($votes == null){
            $restaurant->update(['votes' => $data]);
            $success = 1;
        }else{ 
            foreach($votes as $key => $vote){
                if($vote['ip_address'] == $ip){
                    $success = 0;
                }
            }
            if($success != 0){ 
                 $votes[count($votes)] = $data[0];
                $restaurant->update(['votes' => $votes]);
                $success = 1;
            }
        }
        
        return view('restaurant::public.restaurant.votesubmit', compact('success', 'restaurant'))->render();
    }
    
    protected function testDelete(Request $request){
        if($request->get('delete')){
                  \Session::put('selected_masters[]', null);
        }
        $search          = $request->all();
        return $this->repository->restaurantMasterList($search);
    }
    
     protected function searchSuggestions(Request $request)
    {

        $name = $request->get('name');
         $restaurants = $this->repository->scopeQuery(function ($query) use ($name) {
            return $query->orderBy('name', 'ASC')
                ->whereRaw("UPPER(name) LIKE '%". strtoupper($name)."%'")
                // ->where('name', 'like', '%'.$name.'%')
                ->take(7);
        })->get();
         return view('restaurant::public.restaurant.restaurant_suggestions', compact('restaurants'));
    }    
    protected function timeCheck($type = 'asap', $id = null, Request $request) {
        $msg='';
		$restaurant = $this->repository->findByField('id', hashids_decode($id))->first();
		$preparation_time = 10;

		$carts = Cart::content();

		foreach ($carts as $item) {

			foreach ($item->options as $keys => $optiond) {

				if ($keys == 'preparation_time') {

					if (is_numeric($optiond) && $optiond > $preparation_time) {
						$preparation_time =  $optiond;
					}
				}
			}
        }
       // echo $preparation_time;
     //  dd($preparation_time);
		if ($type == 'asap') {
			$time = strtotime(($preparation_time + 5) . ' minute');
            $order['delivery_time'] = date('y-m-d H:i:s', $time);
			$order['order_status'] = 'New Orders';
		} else {
			$date = new DateTime($request['scheduled_date']);
			$time = new DateTime($request['scheduled_time']);
			$combined = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));
			$order['delivery_time'] = $combined->format('y-m-d H:i:s');
			$order['order_status'] = 'Scheduled';

			if (date('y-m-d H:i') >= date('y-m-d H:i', strtotime($order['delivery_time']))) {
				return response()->json(['status' => 'Error', 'section' => 'time', 'msg' => 'Pickup/Delivery time should be a future Date and Time']);
				//return response()->json(['status' => 'Error', 'section' => 'time', 'msg' => $msg]);
			}

		}
        if ($type == 'asap') {

        $restaurant_det = Restauranttimings::where('restaurant_id', $restaurant->id)
        ->where('day', strtolower(date('D')))
        ->whereNull('deleted_at')->get();


		if (empty($restaurant_det)) {
			return response()->json(['status' => 'Error', 'section' => 'time', 'msg' => 'This restaurant is not open today.']);
        }
        
		if($restaurant_det->first()->opening=='off'||$restaurant_det->first()->closing=='off')
		{
			return response()->json(['status' => 'Error', 'section' => 'time', 'msg' => 'This restaurant is not open today.']);
        }

        $flag = 0;
		$msg = 'This restaurant ';
		foreach ($restaurant_det as $key_time => $value_time) {
            $msg .= ' opens at ' . date('h:i A', strtotime($value_time->opening)) . ' and closes at ' . date('h:i A', strtotime($value_time->closing));
           

             //die();
            if ((strtotime($value_time->opening) <= strtotime(date('H:i'))) 
              && (strtotime($value_time->closing) >= strtotime($order['delivery_time']))) {
				$flag = 1;break;
			}
        }
    } else {
        $flag = 1;
    }
       //dd($flag);

		if ($flag==0) {
			return response()->json(['status' => 'Error', 'section' => 'time', 'msg' => $msg]);
		}


		return response()->json(['status' => 'Success']);
	}

    protected function minorderCheck($type,$id) {
        $subtotal = Cart::total();
        $restaurant = $this->repository->findByField('id', hashids_decode($id))->first();
        if($type == 'Delivery'){
            $min_amount = $restaurant->min_order_amount_delivery;
            $msg = 'This Eatery requires a minimum Delivery order of $'.$min_amount.' (Pick Order Min. is $'.(!empty($restaurant->min_order_count) ? $restaurant->min_order_count : 0).'). You can pay the difference of $'.($min_amount-$subtotal).' to complete the order.';
        }
        else{
            $min_amount = $restaurant->min_order_count;
            $msg = 'This Eatery requires a minimum Pickup order of $'.$min_amount.'. ';
            if($restaurant->delivery == 'Yes'){
                $msg = $msg.'(Delivery Order Min. is $'.(!empty($restaurant->min_order_amount_delivery) ? $restaurant->min_order_amount_delivery : 0).')';
             
            }
            $msg = $msg . ' You can pay the difference of $'.($min_amount-$subtotal).' to complete the order.';
        }
        if ($min_amount > $subtotal) {
                // $msg = 'This Eatery requires a minimum order of $'.$min_amount;
                
                Session::flash('error', $msg);
                return response()->json(['status' => 'Error','section' => 'section_summary', 'msg' => $msg]);
        }
        else{
            return response()->json(['status' => 'Success']);
        }
    }

 //    protected function restaurantDomain($slug)
 //    {
 //        $weekday=[];
	//     $restaurant = $this->repository->findByField('id', $slug)->first();
	// 	$weekMap = [
	// 		'sun' => 'Sunday',
	// 		'mon' => 'Monday',
	// 		'tue' => 'Tuesday',
	// 		'wed' => 'Wednesday',
	// 		'thu' => 'Thursday',
	// 		'fri' => 'Friday',
	// 		'sat' => 'Saturday',
	// 	];
	// 	$dayOfTheWeek = Carbon::now()->dayOfWeek;
	// 	$restaurant->timings = $restaurant->timings->groupBy('day');
	// 	return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
	// 		->view('demo')
	// 		->layout('demo')
	// 		->data(compact('restaurant','weekMap','dayOfTheWeek','weekday'))
	// 		->output();
	// }
    
     protected function searchRestaurants(Request $request)
    {

        $name        = $request->get('q');
        if(strpos($name,"'")!=false)
            $sq='UPPER(name) LIKE "%' . strtoupper($name) . '%"';
        else
            $sq="UPPER(name) LIKE '%" . strtoupper($name) . "%'";
            $restaurants = $this->repository->scopeQuery(function ($query) use ($sq) {
                return $query->orderBy('name', 'ASC')
                    ->whereRaw($sq)
                // ->where('name', 'like', '%'.$name.'%')
                    ->take(10);
            })->get();
        return $restaurants;
    }

   

}
