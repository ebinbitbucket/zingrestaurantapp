<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Restaurant\Interfaces\ReviewRepositoryInterface;
use Illuminate\Http\Request;
use Restaurant\Restaurant\Models\Restaurant;
use Restaurant\Restaurant\Models\Menu;
use Restaurant\Restaurant\Models\Review;
use Laraecart\Cart\Models\Order;
use Auth;

class ReviewPublicController extends BaseController
{
    // use ReviewWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Review\Interfaces\ReviewRepositoryInterface $review
     *
     * @return type
     */
    public function __construct(ReviewRepositoryInterface $review)
    {
        $this->repository = $review;
        parent::__construct();
    }

    /**
     * Show review's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $reviews = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$restaurant::review.names'))
            ->view('restaurant::review.index')
            ->data(compact('reviews'))
            ->output();
    }


    /**
     * Show review.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $review = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$review->name . trans('restaurant::review.name'))
            ->view('restaurant::review.show')
            ->data(compact('review'))
            ->output();
    }

     protected function postreview(Request $request, $id)
        { 
               try
                   {
                       $attributes = $request->all();
                       $restaurant = Restaurant::find($id);
                       $attributes['customer_id']   = user_id();
                       $attributes['restaurant_id'] = $id;
                       $attributes['review'] = $attributes['comment'];
                       $attributes['rating'] = $attributes['score'];
                       $comment = $this->repository->create($attributes);
                       $rating = $this->repository->findByField('restaurant_id',$id)->avg('rating');
                       $restaurant->update(['rating' => $rating]);
                       return redirect(trans_url('/restaurants/'.$restaurant->slug));
                   }
               catch (Exception $e) {
                   redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
               }
       }
protected function postfeedback(Request $request,$id)
        {           
               try
                   { 
                       $attributes = $request->all(); 
                       foreach ($attributes['res'] as $key_res => $value_res) {
                          $restaurant = Restaurant::find($key_res);
                          $attributes['customer_id']   = user_id();
                          $attributes['restaurant_id'] = $key_res; 
                          $attributes['review'] = $value_res['comment'];
                          $attributes['rating'] = $value_res['rating'];
                          $attributes['order_id'] = hashids_decode($id);
                          $exists = $this->repository->findByField(['restaurant_id' => $key_res,'order_id' => hashids_decode($id)])->count();
                          if($exists > 0){
                            $comment = Review::where('restaurant_id' , $key_res)->where('order_id' , hashids_decode($id))->update(['rating' => $attributes['rating'],'review' => $attributes['review']]);

                          }
                          else{
                            $comment = $this->repository->create($attributes);

                          }
                          $rating = $this->repository->findByField('restaurant_id',$key_res)->avg('rating');
                          $restaurant->update(['rating' => $rating]);
                       }
                       foreach ($attributes['menu'] as $key_menu => $value_menu) {
                          $menu = Menu::find($key_menu);
                          $attributes['customer_id']   = user_id();
                          $attributes['menu_id'] = $key_menu; 
                          $attributes['review'] = $value_menu['comment'];
                          $attributes['rating'] = $value_menu['rating'];
                          $attributes['order_id'] = hashids_decode($id);
                        //   unset($attributes['restaurant_id']);
                          $exists = $this->repository->findByField(['menu_id' => $key_menu,'order_id' => hashids_decode($id)])->count();
                          if($exists > 0){
                            $comment = Review::where('menu_id' , $key_menu)->where('order_id' , hashids_decode($id))->update(['rating'=> $attributes['rating'],'review' => $attributes['review']]);
                          }
                          else{
                          $comment = $this->repository->create($attributes);
                        }
                          $rating = $this->repository->findByField('menu_id',$key_menu)->avg('rating');
                          $menu->update(['review' => $rating]);
                       }
                       
                      $orders = Order::where('id', hashids_decode($id))->first();
                        $restaurant_reviews = Review::where('order_id',$orders->id)->whereNotNull('restaurant_id')->select('rating','review')->first();
          $menu_ratings = Review::where('order_id',$orders->id)->whereNotNull('menu_id')->pluck('rating','menu_id')->toArray();
          $menu_reviews = Review::where('order_id',$orders->id)->whereNotNull('menu_id')->pluck('review','menu_id')->toArray();
        return $this->response->setMetaTitle(trans('cart::order.names') . ' Detail')
            ->view('cart::default.order.feedbackSuccess')
            ->layout('blank')
            ->data(compact('orders','restaurant_reviews','menu_reviews','menu_ratings'))
            ->output();
                   }
               catch (Exception $e) {
                   redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
               }
       }


       protected function postrating(Request $request,$id)
        { 
          $orders = Order::where('id', hashids_decode($id))->first();
          Auth::guard('client.web')->loginUsingId($orders->user_id);
               try
                   { 
                       $attributes = $request->all();
                       if($attributes['feedback_type'] == 'restaurant'){
                          $restaurant = Restaurant::find($attributes['feedback_id']);
                          $attributes['customer_id']   = user_id();
                          $attributes['restaurant_id'] = $attributes['feedback_id']; 
                          // $attributes['review'] = $attributes['comment'];
                          $attributes['rating'] = $attributes['rating'];
                          $attributes['order_id'] = hashids_decode($id);
                          $exists = $this->repository->findByField(['restaurant_id' => $attributes['feedback_id'],'order_id' => hashids_decode($id)])->count();
                          if($exists > 0){
                            $comment = Review::where('restaurant_id' , $attributes['feedback_id'])->where('order_id' , hashids_decode($id))->update(['rating'=> $attributes['rating']]);
                          }
                          else{
                            $comment = $this->repository->create($attributes);

                          }
                          $rating = $this->repository->findByField('restaurant_id',$attributes['feedback_id'])->avg('rating');
                          $restaurant->update(['rating' => $rating]);
                       }
                       else{
                          $menu = Menu::find($attributes['feedback_id']);    
                          $attributes['customer_id']   = user_id();
                          $attributes['menu_id'] = $attributes['feedback_id']; 
                          // $attributes['review'] = $attributes['comment'];
                          $attributes['rating'] = $attributes['rating'];
                          $attributes['order_id'] = hashids_decode($id);
                          $exists = $this->repository->findByField(['menu_id' => $attributes['feedback_id'],'order_id' => hashids_decode($id)])->count();
                          if($exists > 0){
                            $comment = Review::where('menu_id' , $attributes['feedback_id'])->where('order_id' , hashids_decode($id))->update(['rating'=> $attributes['rating']]);
                            
                          }
                          else{
                          $comment = $this->repository->create($attributes);
                        }
                          $rating = $this->repository->findByField('menu_id',$attributes['feedback_id'])->avg('rating');

                          $menu->update(['review' => $rating]);
                       }
                       
                      
                      
          $restaurant_reviews = Review::where('order_id',$orders->id)->whereNotNull('restaurant_id')->select('rating','review')->first();
          $menu_ratings = Review::where('order_id',$orders->id)->whereNotNull('menu_id')->pluck('rating','menu_id')->toArray();
          $menu_reviews = Review::where('order_id',$orders->id)->whereNotNull('menu_id')->pluck('review','menu_id')->toArray();
          //return view('cart::default.order.feedback',compact('orders','restaurant_reviews','menu_reviews','menu_ratings'));
        return $this->response->setMetaTitle(trans('cart::order.names') . ' Detail')
            ->view('cart::default.order.feedback')
            ->layout('blank')
            ->data(compact('orders','restaurant_reviews','menu_reviews','menu_ratings'))
            ->output();
                   }
               catch (Exception $e) {
                   redirect()->back()->withInput()->with('message', $e->getMessage())->with('code', 400);
               }
       }



}
