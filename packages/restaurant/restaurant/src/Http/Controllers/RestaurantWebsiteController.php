<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
use Illuminate\Http\Request;
use Restaurant\Restaurant\Models\Restaurant;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Mail;
use Session;
class RestaurantWebsiteController extends BaseController
{
    // use ReviewWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Review\Interfaces\ReviewRepositoryInterface $review
     *
     * @return type
     */
    public function __construct(RestaurantRepositoryInterface $restaurant)
    {
        $this->repository = $restaurant;
        parent::__construct();
    }

    /**
     * Show review's list.
     *
     * @param string $slug
     *
     * @return response
     */
     protected function restaurantDomain($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
        'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $weekday = strtolower(date('D'));
        $restaurant =  $this->repository->scopeQuery(function($query) use ($slug) {
                return $query->orderBy('id','DESC')
                             ->where('id', $slug)
                             ->with([
                                'menu',
                                'offers',
                                'review',
                                'review.customer',
                                'menu.master',
                                
                            ]);
            })->first(['*']);

      $restaurant->menu = @$restaurant->menu->unique('category_id');
      $restaurant->timings = $restaurant->timings->groupBy('day');

      if($restaurant->theme_design == null){
        $restaurant->theme_design = 'Default';
      }

      return $this->response->setMetaTitle($restaurant->title)
      ->setMetaKeyword($restaurant->meta_tags)
      ->setMetaTag($restaurant->meta_keywords)
      ->setMetaDescription($restaurant->meta_description)
        ->view(lcfirst(@$restaurant->theme_design))
        ->theme('website')
        ->layout(lcfirst(@$restaurant->theme_design))
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday'))
        ->output();
  }

   protected function restaurantWebAbout($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');

      if($restaurant->theme_design == null){
        $restaurant->theme_design = 'Default';
      }

      return $this->response->setMetaTitle($restaurant->title)
       ->setMetaKeyword($restaurant->meta_tags)
      ->setMetaTag($restaurant->meta_keywords)
      ->setMetaDescription($restaurant->meta_description)
        ->view(lcfirst(@$restaurant->theme_design).'.about')
        ->theme('website')
        ->layout(lcfirst(@$restaurant->theme_design))
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday'))
        ->output();
  }

  protected function restaurantWebMenu($slug)
    {
        $weekday=[];
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
   
       $weekday = strtolower(date('D'));
        $restaurant =  $this->repository->scopeQuery(function($query) use ($slug) {
                return $query->orderBy('id','DESC')
                             ->where('id', $slug)
                             ->with([
                                'category'=> function ($q) {
                                    $q->orderBy('order_no','ASC');
                                },
                                'popular_category'  => function ($q) {
                            $q->where('popular_dish_no', '<>', 0)
                                ->orderBy('popular_dish_no', 'DESC');
    
                            // $query->OrWhere('catering', 'No')
                            //       ->OrWhereNull('catering', 'No');
    
                        },
                                'menu',
                                'offers',
                                'review',
                                'review.customer',
                                'menu.master',
                                'catering_category' => function ($q) {
                                    $q->whereHas('menu', function (Builder $query) {
                                        $query->where('catering', 'Yes');
                                    });
                                }
                            ]);
            })->first(['*']);


        $restaurant->menu = $restaurant->menu->groupBy('category_id');
        $catering_catergories = $restaurant->catering_category;
        $restaurant->timings = $restaurant->timings->groupBy('day');

      if($restaurant->theme_design == null){
        $restaurant->theme_design = 'Default';
      }

      return $this->response->setMetaTitle($restaurant->title)
       ->setMetaKeyword($restaurant->meta_tags)
      ->setMetaTag($restaurant->meta_keywords)
      ->setMetaDescription($restaurant->meta_description)
        ->view(lcfirst(@$restaurant->theme_design).'.menu')
        ->theme('website')
        ->layout(lcfirst(@$restaurant->theme_design))
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday', 'catering_catergories'))
        ->output();
  }

    protected function restaurantWebGallery($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
          'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');

      if($restaurant->theme_design == null){
        $restaurant->theme_design = 'Default';
      }

      return $this->response->setMetaTitle($restaurant->title)
       ->setMetaKeyword($restaurant->meta_tags)
      ->setMetaTag($restaurant->meta_keywords)
      ->setMetaDescription($restaurant->meta_description)
        ->view(lcfirst(@$restaurant->theme_design).'.gallery')
        ->theme('website')
        ->layout(lcfirst(@$restaurant->theme_design))
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday'))
        ->output();
  }

   protected function restaurantWebContact($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
        'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');

      if($restaurant->theme_design == null){
        $restaurant->theme_design = 'Default';
      }

      return $this->response->setMetaTitle($restaurant->title)
       ->setMetaKeyword($restaurant->meta_tags)
      ->setMetaTag($restaurant->meta_keywords)
      ->setMetaDescription($restaurant->meta_description)
        ->view(lcfirst(@$restaurant->theme_design).'.contact')
        ->theme('website')
        ->layout(lcfirst(@$restaurant->theme_design))
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday'))
        ->output();
  }

   protected function sendMail(Request $request, $slug)
    {
      $attributes = $request->all();
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');
    
      if($restaurant->customer_email != null){
              Mail::send('design1.mail', ['restaurant' => $restaurant,'attributes' => $attributes], function ($message) use ($restaurant,$attributes) {
                       $message->from($attributes['cf_email']);
                       $message->to($restaurant->customer_email)->subject('Website Enquiry');
            });
      }
      
      // Session::flash('message', 'This is a message!');
      return redirect(url('domain/contact/'.$restaurant->id))->with('message', 'There was an error...');
     
  }

  protected function locations($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');

      if($restaurant->theme_design == null){
        $restaurant->theme_design = 'Default';
      }

      return $this->response->setMetaTitle($restaurant->title)
      ->setMetaKeyword($restaurant->meta_tags)
      ->setMetaTag($restaurant->meta_keywords)
      ->setMetaDescription($restaurant->meta_description)
        ->view(lcfirst(@$restaurant->theme_design).'.location')
        ->theme('website')
        ->layout(lcfirst(@$restaurant->theme_design))
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday'))
        ->output();
  }

  protected function newMenu($slug)
    {
        $weekday=[];
        $restaurant = $this->repository->findByField('id', $slug)->first();
      $weekMap = [
        'mon' => 'Monday',
        'tue' => 'Tuesday',
        'wed' => 'Wednesday',
        'thu' => 'Thursday',
        'fri' => 'Friday',
        'sat' => 'Saturday',
         'sun' => 'Sunday',
      ];
      $dayOfTheWeek = Carbon::now()->dayOfWeek;
      $restaurant->timings = $restaurant->timings->groupBy('day');

      if($restaurant->theme_design == null){
        $restaurant->theme_design = 'Default';
      }

      return $this->response->setMetaTitle($restaurant->title)
      ->setMetaKeyword($restaurant->meta_tags)
      ->setMetaTag($restaurant->meta_keywords)
      ->setMetaDescription($restaurant->meta_description)
        ->view(lcfirst(@$restaurant->theme_design).'.newmenu')
        ->theme('website')
        ->layout(lcfirst(@$restaurant->theme_design))
        ->data(compact('restaurant','weekMap','dayOfTheWeek','weekday'))
        ->output();
  }

   public function restaurantRefresh($id){
    $restaurant = $this->repository->findByField('id', $id)->first();
    if($restaurant->website_domain){
    $redirecthome=$restaurant->website_domain.'?refresh=1'; 
    $redirectabt=$restaurant->website_domain.'/about.php?refresh=1';
    $redirectmenu=$restaurant->website_domain.'/menu.php?refresh=1';
    $redirectgallery=$restaurant->website_domain.'/gallery.php?refresh=1';
    $redirectdetails=$restaurant->website_domain.'/details.php?refresh=1';
    $redirectlocations=$restaurant->website_domain.'/locations.php?refresh=1';
    $redirectcontact=$restaurant->website_domain.'/contact.php?refresh=1';
    file_get_contents($redirecthome);
    file_get_contents($redirectabt);
    file_get_contents($redirectmenu);
    file_get_contents($redirectgallery);
    file_get_contents($redirectdetails);
    file_get_contents($redirectlocations);
    file_get_contents($redirectcontact);

   return redirect()->to($restaurant->website_domain);

   }
  }
  
}
