<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Restaurant\Http\Requests\AddonRequest;
use Restaurant\Restaurant\Interfaces\AddonRepositoryInterface;
use Restaurant\Restaurant\Models\Addon;
use Restaurant\Restaurant\Models\Addonvariations;
use DB;

/**
 * Resource controller class for addon.
 */
class AddonResourceController extends BaseController
{

    /**
     * Initialize addon resource controller.
     *
     * @param type AddonRepositoryInterface $addon
     *
     * @return null
     */
    public function __construct(AddonRepositoryInterface $addon)
    {
        parent::__construct();
        $this->repository = $addon;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Restaurant\Repositories\Criteria\AddonResourceCriteria::class);
    }

    /**
     * Display a list of addon.
     *
     * @return Response
     */
    public function index(AddonRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Restaurant\Repositories\Presenter\AddonPresenter::class)
                ->$function();
        }

        $addons = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('restaurant::addon.names'))
            ->view('restaurant::addon.index', true)
            ->data(compact('addons', 'view'))
            ->output();
    }

    /**
     * Display addon.
     *
     * @param Request $request
     * @param Model   $addon
     *
     * @return Response
     */
    public function show(AddonRequest $request, Addon $addon)
    {

        if ($addon->exists) {
            $view = 'restaurant::addon.show';
        } else {
            $view = 'restaurant::addon.new';
        }
        $addon_variations = Addonvariations::where('addon_id',$addon->id)->get();
        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('restaurant::addon.name'))
            ->data(compact('addon','addon_variations'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new addon.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(AddonRequest $request)
    {

        $addon = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('restaurant::addon.name')) 
            ->view('restaurant::addon.create', true) 
            ->data(compact('addon'))
            ->output();
    }

    /**
     * Create new addon.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(AddonRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            if(user()->hasRole('restaurant')){
                $attributes['restaurant_id']   = user_id();
            }
            if(!empty($attributes['variations'])){ 
                $addon                 = $this->repository->create($attributes);
                foreach ($attributes['variations'] as $key => $value) {
                $addon_variations = Addonvariations::create(['addon_id'=> $addon->id,'name' => $value['name'],'description' => $value['description'], 'price' => $value['price'],'user_id' => $attributes['user_id'],'user_type' => $attributes['user_type']]);
                }
            }
            else{
                return $this->response->message('Add atleast 1 variation')
                ->code(400)
                ->status('error')
                ->redirect();
            }
            
            if(user()->hasRole('restaurant')){
                return redirect()->back();
            }
            return $this->response->message(trans('messages.success.created', ['Module' => trans('restaurant::addon.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/addon/' . $addon->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/restaurant/addon'))
                ->redirect();
        }

    }

    /**
     * Show addon for editing.
     *
     * @param Request $request
     * @param Model   $addon
     *
     * @return Response
     */
    public function edit(AddonRequest $request, Addon $addon)
    {
        $addon_variations = Addonvariations::where('addon_id',$addon->id)->get();
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('restaurant::addon.name'))
            ->view('restaurant::addon.edit', true)
            ->data(compact('addon','addon_variations'))
            ->output();
    }

    /**
     * Update the addon.
     *
     * @param Request $request
     * @param Model   $addon
     *
     * @return Response
     */
    public function update(AddonRequest $request, Addon $addon)
    {
        try {
            $attributes = $request->all();
            if(user()->hasRole('restaurant')){
                $attributes['restaurant_id']   = user_id();
            }
            if(!empty($attributes['variations'])){
                Addonvariations::where('addon_id',$addon->id)->forceDelete();
                 foreach ($attributes['variations'] as $key => $value) {
                $addon_variations = Addonvariations::create(['addon_id'=> $addon->id,'name' => $value['name'],'description' => $value['description'], 'price' => $value['price'],'user_id' => user_id(),'user_type' => user_type()]);
                }
            }
            else{
                // Addonvariations::where('addon_id',$addon->id)->forceDelete();
                 return $this->response->message('Add atleast 1 variation')
                ->code(400)
                ->status('error')
                ->redirect();
            }
            $addon->update($attributes);
            if(user()->hasRole('restaurant')){
                return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::addon.name')]))
                ->code(204)
                ->status('success')
                ->redirect();
            }
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurant::addon.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurant/addon/' . $addon->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/addon/' . $addon->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the addon.
     *
     * @param Model   $addon
     *
     * @return Response
     */
    public function destroy(AddonRequest $request, Addon $addon)
    {
        try {
             Addonvariations::where('addon_id',$addon->id)->forceDelete();
            $addon->delete();

            if(user()->hasRole('restaurant')){
                return redirect()->back();
            }
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::addon.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('restaurant/addon/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurant/addon/' . $addon->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple addon.
     *
     * @param Model   $addon
     *
     * @return Response
     */
    public function delete(AddonRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurant::addon.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('restaurant/addon'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/addon'))
                ->redirect();
        }

    }

    /**
     * Restore deleted addons.
     *
     * @param Model   $addon
     *
     * @return Response
     */
    public function restore(AddonRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('restaurant::addon.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/restaurant/addon'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurant/addon/'))
                ->redirect();
        }

    }

     public function getAddons($id){ 
        $addons=   $this->repository->getAddons($id);
        return view('restaurant::admin.addon.addonselect', compact('addons'));
    }

    public function getAddonVariations($id) { 
        $addon = Addonvariations::where('addon_id',$id)->get();
        return view('restaurant::admin.addon.variations', compact('addon','id'));
    }

     public function restauarnt_addons()
    { 
        $addons = Addon::with('variation')->where('restaurant_id',user_id())->get();
        return $this->response->setMetaTitle('Restaurant Addons')
            ->view('restaurant::default.addon.restauarnt_addons')
            // ->theme('restaurant')
            ->layout('default')
            ->data(compact('addons'))
            ->output();
    }

    public function getRestAddonVariations($id) { 
        $addon = Addonvariations::where('addon_id',$id)->get();
        return view('restaurant::admin.addon.rest_addonvariations', compact('addon','id'));
    }

    public function restauarnt_addons_list()
    { 
        $addons = Addon::where('restaurant_id',user_id())->get();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.addon.restauarnt_addons_list')
            ->layout('default')
            ->data(compact('addons'))
            ->output();
    }

    public function restauarnt_addons_edit($addon_id)
    { 
        $edit_addon = Addon::where('id',$addon_id)->first();
        return $this->response->setMetaTitle('Restaurants')
            ->view('restaurant::default.addon.restauarnt_addons_edit')
            ->layout('default')
            ->data(compact('edit_addon'))
            ->output();
    }
   
    public function status_update($menu_id,$addon_id,$status){
        $addon = DB::table('menu_addon')->where('menu_id',$menu_id)->where('addon_id',$addon_id)->first();
        if($status == 'on'){
            DB::table('menu_addon')->where('id',$addon->id)->update(['stock_status' => 1]);
        }
        else{
            DB::table('menu_addon')->where('id',$addon->id)->update(['stock_status' => 0]);
        }
        return redirect()->back();
   } 


}
