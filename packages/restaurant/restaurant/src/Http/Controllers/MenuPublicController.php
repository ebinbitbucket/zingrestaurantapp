<?php

namespace Restaurant\Restaurant\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;


class MenuPublicController extends BaseController
{
    // use MenuWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Menu\Interfaces\MenuRepositoryInterface $menu
     *
     * @return type
     */
    public function __construct(MenuRepositoryInterface $menu,RestaurantRepositoryInterface $restaurant)
    {
        $this->repository = $menu;
        $this->restaurant = $restaurant;
        parent::__construct();
    }

    /**
     * Show menu's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $menus = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$restaurant::menu.names'))
            ->view('restaurant::menu.index')
            ->data(compact('menus'))
            ->output();
    }

     protected function loadMenu()
    {
        $menus = $this->repository->loadMoreMenu();  
        $data['view'] =  view('restaurant::public.restaurant.loadmore', compact('menus'))->render(); 
        $data['lastpage'] = $menus->lastPage();
         return $data;
    }



    /**
     * Show menu.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $menu = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle( trans('restaurant::menu.name'))
            ->view('restaurant::menu.show')
            ->data(compact('menu'))
            ->output();
    }

   protected function menuModel($slug)
    {
        
        $menu = $this->repository->find($slug);
        // $menu = $this->repository->scopeQuery(function($query) use ($slug) {
        //     return $query->orderBy('id','DESC')
        //                              ->where('id', $slug);
        // })->first(['*']);
        $menu_addons = DB::table('menu_addon')->select('restaurant_addons.name','menu_addon.*')
                        ->join('restaurant_addons','menu_addon'.'.addon_id','restaurant_addons'.'.id')
                        ->where('menu_addon'.'.menu_id',$menu->id)->get();
                    
         if(($menu->menu_variations != null) || (($menu_addons->count() > 0))) {
            $view = view('restaurant::public.menu.menumodel',compact('menu','menu_addons'))->render();
            
            return response()->json(['view' => $view, 'status' => 'true']);
         }else{
            return response()->json(['status' => 'false']);
         }               
        
    }

     protected function menuVideo($slug)
    {
        $menu = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);
        
        return view('restaurant::public.menu.menuvideomodel',compact('menu'));
    }
    
     protected function menuModelShow($slug)
    {
    // dd('dd');

       // dd(date("Y-m-d"));
       $restaurant_stop=false; 
        $menu = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->where('id', $slug);
        })->first();

        $menu_addons = DB::table('menu_addon')->select('restaurant_addons.name','menu_addon.*')
                        ->join('restaurant_addons','menu_addon'.'.addon_id','restaurant_addons'.'.id')
                        ->where('menu_addon'.'.menu_id',$menu->id)->get();
                        //dd($menu->restaurant->stop_date);
                        if ($menu->restaurant->stop_date < Carbon::now()||is_null($menu->restaurant->stop_date)) 
                        {
                            $restaurant_stop=false; 
                        }
                    else{
                        $restaurant_stop=true; 
                    }
                   // $restaurant_stop=false; 
                  //  die($restaurant_stop);
                      //  dd($today = Carbon::now());
            $view = view('restaurant::public.menu.menumodel',compact('menu','menu_addons','restaurant_stop'))->render();
            return response()->json(['view' => $view, 'status' => 'true']);
                     
        
    }
    protected function food(){

        $menus = $this->repository->scopeQuery(function($query) {
            return $query->orderBy('name','ASC');
        })->paginate(100);

        return $this->response->setMetaTitle( trans('restaurant::menu.name'))
            ->view('restaurant::menu.menu-list')
            ->layout('blankcheckout')
            ->data(compact('menus'))
            ->output();
    }
     protected function food1($var = null){

        if($var == 'more'){ 
            $menus = $this->repository->scopeQuery(function($query) use($var) {
            return $query->where('name', 'regexp', '^[0-9]+')
            //->orWhere('name', 'REGEXP', '[$&+@#|]')
            ->orderBy('name','ASC');
            })->paginate(100);
        }else{ 
            $menus = $this->repository->scopeQuery(function($query) use($var) {
            return $query->where('name', 'LIKE', $var.'%')->orderBy('name','ASC');
            })->paginate(100);
        }

        return $this->response->setMetaTitle(trans('restaurant::restaurant.names'))
            ->view('restaurant::menu.menu-list')
            ->layout('blankcheckout')
            ->data(compact('menus'))
            ->output();
    }
    public function menuSearch(Request $request,$rest_id){
        //dd($request->phrase);
        
        $phrase=$request->phrase;
        if($phrase=='all'){
            $menus = $this->repository->scopeQuery(function($query) use($rest_id) {
                return $query->with('restaurant','master','categories')->has('categories')->where('restaurant_id',$rest_id)->orderBy('name','ASC');
                })->get();
        } else{
            $menus = $this->repository->scopeQuery(function($query) use($phrase,$rest_id) {
                return $query->with('restaurant','master','categories')->has('categories')->where('name', 'LIKE','%'.$phrase.'%')->where('restaurant_id',$rest_id)->orderBy('name','ASC');
                })->get();
        }

       $restaurant=[];
        return view('restaurant::public.menu.search_results',compact('restaurant','menus'));

       // dd($restaurant->menu);
    }

}
