<?php

namespace Restaurant\Restaurant\Http\Requests;

use App\Http\Requests\Request as FormRequest;
class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $this->model = $this->route('menu');

        if (is_null($this->model)) {
            // Determine if the user is authorized to access menu module,
            return $this->canAccess();
        }

        if ($this->isWorkflow()) {
            // Determine if the user is authorized to change status of an entry,
            return $this->can($this->getStatus());
        }

        if ($this->isCreate() || $this->isStore()) {
            // Determine if the user is authorized to create an entry,
            return $this->can('create');
        }

        if ($this->isEdit() || $this->isUpdate()) {
            // Determine if the user is authorized to update an entry,
            return $this->can('update');
        }

        if ($this->isDelete()) {
            // Determine if the user is authorized to delete an entry,
            return $this->can('destroy');
        }

        // Determine if the user is authorized to view the module.
        return $this->can('view');

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        \Validator::extend( 'composite_unique', function ( $attribute, $value, $parameters, $validator ) {
                
                // remove first parameter and assume it is the table name
                $table = array_shift( $parameters ); 

                // start building the conditions
                $fields = [ $attribute => $value ]; // current field, company_code in your case

                // iterates over the other parameters and build the conditions for all the required fields
                while ( $field = array_shift( $parameters ) ) {
                    $fields[ $field ] = $this->get( $field );
                }

                // query the table with all the conditions
                $result = \DB::table( $table )->select( \DB::raw( 1 ) )->where( $fields )->first();

                return empty( $result ); // edited here
            }, 'your custom composite unique key validation message' );

        if ($this->isStore()) { 
            // validation rule for create request.
            return [
                    'name'    => 'required|composite_unique:restaurant_menus,name,restaurant_id',
            ];
        }

        if ($this->isUpdate()) {
            // Validation rule for update request.
            return [
                    
            ];
        }

        // Default validation rule.
        return [

        ];
    }

    /**
     * Check whether the user can access the module.
     *
     * @return bool
     **/
    protected function canAccess()
    {
        return user()->canDo('restaurant.menu.view');
    }
}
