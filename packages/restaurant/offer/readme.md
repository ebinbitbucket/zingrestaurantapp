Lavalite package that provides offer management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `restaurant/offer`.

    "restaurant/offer": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Restaurant\Offer\Providers\OfferServiceProvider::class,

And also add it to alias

    'Offer'  => Restaurant\Offer\Facades\Offer::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Restaurant\\OfferTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Restaurant\Offer\Providers\OfferServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Restaurant\Offer\Providers\OfferServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Restaurant\Offer\Providers\OfferServiceProvider" --tag="view"


## Usage


