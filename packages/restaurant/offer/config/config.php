<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'restaurant',

    /*
     * Package.
     */
    'package'   => 'offer',

    /*
     * Modules.
     */
    'modules'   => ['offer'],

    
    'offer'       => [
        'model' => [
            'model'                 => \Restaurant\Offer\Models\Offer::class,
            'table'                 => 'restaurant_offers',
            'presenter'             => \Restaurant\Offer\Repositories\Presenter\OfferPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'created_at', 'updated_at', 'start_date', 'end_date'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'category_id',  'menu_id',  'type',  'value',  'start_date',  'end_date',  'image',  'name', 'user_id', 'user_type', 'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'offer/offer',
            'uploads'               => [
            
                    'image' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            
            ],

            'casts'                 => [
            
                'image'    => 'array',
            
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Restaurant',
            'package'   => 'Offer',
            'module'    => 'Offer',
        ],

    ],
];
