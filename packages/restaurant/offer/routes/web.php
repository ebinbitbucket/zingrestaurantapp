<?php

// Resource routes  for offer
Route::group(['prefix' => set_route_guard('web').'/offer'], function () {
    Route::resource('offer', 'OfferResourceController');
});

// Public  routes for offer
Route::get('offer/popular/{period?}', 'OfferPublicController@popular');
Route::get('offers/', 'OfferPublicController@index');
Route::get('offers/{slug?}', 'OfferPublicController@show');

