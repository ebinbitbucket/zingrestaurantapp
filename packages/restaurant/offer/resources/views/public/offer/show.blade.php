            @include('offer::offer.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('offer::offer.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($offer->defaultImage('images' , 'xl'))!!}" alt="{{$offer->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('offer::offer.label.id') !!}
                </label><br />
                    {!! $offer['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="restaurant_id">
                    {!! trans('offer::offer.label.restaurant_id') !!}
                </label><br />
                    {!! $offer['restaurant_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="category_id">
                    {!! trans('offer::offer.label.category_id') !!}
                </label><br />
                    {!! $offer['category_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="menu_id">
                    {!! trans('offer::offer.label.menu_id') !!}
                </label><br />
                    {!! $offer['menu_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="type">
                    {!! trans('offer::offer.label.type') !!}
                </label><br />
                    {!! $offer['type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="value">
                    {!! trans('offer::offer.label.value') !!}
                </label><br />
                    {!! $offer['value'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="start_date">
                    {!! trans('offer::offer.label.start_date') !!}
                </label><br />
                    {!! $offer['start_date'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="end_date">
                    {!! trans('offer::offer.label.end_date') !!}
                </label><br />
                    {!! $offer['end_date'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="image">
                    {!! trans('offer::offer.label.image') !!}
                </label><br />
                    {!! $offer['image'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('offer::offer.label.name') !!}
                </label><br />
                    {!! $offer['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('offer::offer.label.created_at') !!}
                </label><br />
                    {!! $offer['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('offer::offer.label.updated_at') !!}
                </label><br />
                    {!! $offer['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('offer::offer.label.deleted_at') !!}
                </label><br />
                    {!! $offer['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('offer::offer.label.restaurant_id'))
                       -> placeholder(trans('offer::offer.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('category_id')
                       -> label(trans('offer::offer.label.category_id'))
                       -> placeholder(trans('offer::offer.placeholder.category_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('menu_id')
                       -> label(trans('offer::offer.label.menu_id'))
                       -> placeholder(trans('offer::offer.placeholder.menu_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('type')
                    -> options(trans('offer::offer.options.type'))
                    -> label(trans('offer::offer.label.type'))
                    -> placeholder(trans('offer::offer.placeholder.type'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('value')
                       -> label(trans('offer::offer.label.value'))
                       -> placeholder(trans('offer::offer.placeholder.value'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='start_date' class='control-label'>{!!trans('offer::offer.label.start_date')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('start_date')
                            -> placeholder(trans('offer::offer.placeholder.start_date'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='end_date' class='control-label'>{!!trans('offer::offer.label.end_date')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('end_date')
                            -> placeholder(trans('offer::offer.placeholder.end_date'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-12 col-sm-12'>
                    <div class="form-group">
                        <label for="image" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('offer::offer.label.image') }}
                        </label>
                        <div class='col-lg-3 col-sm-12'>
                            {!! $offer->files('image')
                            ->url($offer->getUploadUrl('image'))
                            ->mime(config('filer.image_extensions'))
                            ->dropzone()!!}
                        </div>
                        <div class='col-lg-7 col-sm-12'>
                        {!! $offer->files('image')
                        ->editor()!!}
                        </div>
                    </div>
                </div>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('offer::offer.label.name'))
                       -> placeholder(trans('offer::offer.placeholder.name'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



