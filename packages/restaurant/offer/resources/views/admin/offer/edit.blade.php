    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#offer" data-toggle="tab">{!! trans('offer::offer.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#offer-offer-edit'  data-load-to='#offer-offer-entry' data-datatable='#offer-offer-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#offer-offer-entry' data-href='{{guard_url('offer/offer')}}/{{$offer->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('offer-offer-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('offer/offer/'. $offer->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="offer">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('offer::offer.name') !!} [{!!$offer->name!!}] </div>
                @include('offer::admin.offer.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>