    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('offer::offer.name') !!}</a></li>
            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#offer-offer-entry' data-href='{{guard_url('offer/offer/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($offer->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#offer-offer-entry' data-href='{{ guard_url('offer/offer') }}/{{$offer->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#offer-offer-entry' data-datatable='#offer-offer-list' data-href='{{ guard_url('offer/offer') }}/{{$offer->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('offer-offer-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('offer/offer'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('offer::offer.name') !!}  [{!! $offer->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('offer::admin.offer.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>