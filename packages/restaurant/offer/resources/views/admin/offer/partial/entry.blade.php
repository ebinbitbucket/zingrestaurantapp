            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::select('restaurant_id')
                         -> options(Restaurant::getRestaurant())
                         ->required()
                         -> label(trans('restaurant::menu.label.restaurant_id'))
                         -> placeholder(trans('restaurant::menu.placeholder.restaurant_id'))!!}
                </div>

               <!--  <div class='col-md-4 col-sm-6'>
                       {!! Form::select('category_id')
                          ->options(array())
                          -> label(trans('restaurant::menu.label.category_id'))
                           -> placeholder(trans('restaurant::menu.placeholder.category_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::select('menu_id')
                          ->options(array())
                          -> label(trans('offer::offer.label.menu_id'))
                           -> placeholder(trans('offer::offer.placeholder.menu_id'))!!}
                </div> -->

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('type')
                    -> options(trans('offer::offer.options.type'))
                    ->required()
                    -> label(trans('offer::offer.label.type'))
                    -> placeholder(trans('offer::offer.placeholder.type'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::number('value')
                       -> label(trans('offer::offer.label.value'))
                       ->required()
                       ->min(1)
                       -> placeholder(trans('offer::offer.placeholder.value'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('offer::offer.label.name'))
                       -> placeholder(trans('offer::offer.placeholder.name'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='start_date' class='control-label'>{!!trans('offer::offer.label.start_date')!!}</label>
                        <div class='input-group pickdate'>
                            {!! Form::text('start_date')
                            -> placeholder(trans('offer::offer.placeholder.start_date'))
                            -> addClass('pickdate')
                            ->required()
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='end_date' class='control-label'>{!!trans('offer::offer.label.end_date')!!}</label>
                        <div class='input-group pickdate'>
                            {!! Form::text('end_date')
                            -> placeholder(trans('offer::offer.placeholder.end_date'))
                            -> addClass('pickdate')
                            ->required()
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-12 col-sm-12'>
                    <div class="form-group">
                        <label for="image" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('offer::offer.label.image') }}
                        </label>
                        <div class='col-lg-3 col-sm-12'>
                            {!! $offer->files('image')
                            ->url($offer->getUploadUrl('image'))
                            ->mime(config('filer.image_extensions'))
                            ->dropzone()!!}
                        </div>
                        <div class='col-lg-7 col-sm-12'>
                        {!! $offer->files('image')
                        ->editor()!!}
                        </div>
                    </div>
                </div>
                
            </div>

<script type="text/javascript">
    $(document).ready(function(){
      $("#restaurant_id").on('change', function(){
        var restaurant_id = $("#restaurant_id option:selected").val();
        $("#category_id").load('{{trans_url("categories/Category")}}'+'/'+restaurant_id);
      })

      $("#category_id").on('change', function(){
        var restaurant_id = $("#restaurant_id option:selected").val();
        var category_id = $("#category_id option:selected").val();
        $("#menu_id").load('{{trans_url("menu/select")}}'+'/'+category_id+'/'+restaurant_id);
      })
    })
</script>