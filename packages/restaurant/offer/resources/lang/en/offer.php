<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for offer in offer package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  offer module in offer package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Offer',
    'names'         => 'Offers',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Offers',
        'sub'   => 'Offers',
        'list'  => 'List of offers',
        'edit'  => 'Edit offer',
        'create'    => 'Create new offer'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            'type'                => ['Price' => 'Price','Percentage' => 'Percentage'],
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter ',
        'restaurant_id'              => 'Please enter restaurant ',
        'category_id'                => 'Please enter category ',
        'menu_id'                    => 'Please enter menu ',
        'type'                       => 'Please select type',
        'value'                      => 'Please enter value',
        'start_date'                 => 'Please select start date',
        'end_date'                   => 'Please select end date',
        'image'                      => 'Please enter image',
        'name'                       => 'Please enter name',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant ',
        'category_id'                => 'Category ',
        'menu_id'                    => 'Menu ',
        'type'                       => 'Type',
        'value'                      => 'Value',
        'start_date'                 => 'Start date',
        'end_date'                   => 'End date',
        'image'                      => 'Image',
        'name'                       => 'Name',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'restaurant_id'              => ['name' => 'Restaurant ', 'data-column' => 1, 'checked'],
        'category_id'                => ['name' => 'Category ', 'data-column' => 2, 'checked'],
        'menu_id'                    => ['name' => 'Menu ', 'data-column' => 3, 'checked'],
        'type'                       => ['name' => 'Type', 'data-column' => 4, 'checked'],
        'value'                      => ['name' => 'Value', 'data-column' => 5, 'checked'],
        'start_date'                 => ['name' => 'Start date', 'data-column' => 6, 'checked'],
        'end_date'                   => ['name' => 'End date', 'data-column' => 7, 'checked'],
        'name'                       => ['name' => 'Name', 'data-column' => 8, 'checked'],
        'created_at'                 => ['name' => 'Created at', 'data-column' => 9, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Offers',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
