<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Offer package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for offer module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Offer',
    'names'         => 'Offers',
];
