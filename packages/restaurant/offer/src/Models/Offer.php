<?php

namespace Restaurant\Offer\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Trans\Traits\Translatable;
class Offer extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable, PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.offer.offer.model';

      public function category()
     {
        return $this->belongsTo('Restaurant\Restaurant\Models\Category','category_id');
     }

     public function restaurant()
     {
        return $this->belongsTo('Restaurant\Restaurant\Models\Restaurant','restaurant_id');
     }

     public function menu()
     {
        return $this->belongsTo('Restaurant\Restaurant\Models\Menu','menu_id');
     }

}
