<?php

namespace Restaurant\Offer\Repositories\Eloquent;

use Restaurant\Offer\Interfaces\OfferRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class OfferRepository extends BaseRepository implements OfferRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.offer.offer.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.offer.offer.model.model');
    }
}
