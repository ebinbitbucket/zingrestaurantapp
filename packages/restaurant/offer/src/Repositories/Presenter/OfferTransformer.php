<?php

namespace Restaurant\Offer\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class OfferTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Offer\Models\Offer $offer)
    {
        return [
            'id'                => $offer->getRouteKey(),
            'key'               => [
                'public'    => $offer->getPublicKey(),
                'route'     => $offer->getRouteKey(),
            ], 
            'restaurant_id'     => @$offer->restaurant->name,
            'category_id'       => @$offer->category->name,
            'menu_id'           => @$offer->menu->name,
            'type'              => $offer->type,
            'value'             => $offer->value,
            'start_date'        => format_date($offer->start_date),
            'end_date'          => format_date($offer->end_date),
            'image'             => $offer->image,
            'name'              => $offer->name,
            'created_at'        => $offer->created_at,
            'updated_at'        => $offer->updated_at,
            'deleted_at'        => $offer->deleted_at,
            'url'               => [
                'public'    => trans_url('offer/'.$offer->getPublicKey()),
                'user'      => guard_url('offer/offer/'.$offer->getRouteKey()),
            ], 
            'status'            => trans('app.'.$offer->status),
            'created_at'        => format_date($offer->created_at),
            'updated_at'        => format_date($offer->updated_at),
        ];
    }
}