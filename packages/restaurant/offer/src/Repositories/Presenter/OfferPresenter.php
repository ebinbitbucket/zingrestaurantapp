<?php

namespace Restaurant\Offer\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class OfferPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new OfferTransformer();
    }
}