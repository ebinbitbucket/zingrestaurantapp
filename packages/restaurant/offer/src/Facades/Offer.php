<?php

namespace Restaurant\Offer\Facades;

use Illuminate\Support\Facades\Facade;

class Offer extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'restaurant.offer';
    }
}
