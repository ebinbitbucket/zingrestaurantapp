<?php

namespace Restaurant\Offer\Policies;

use Litepie\User\Contracts\UserPolicy;
use Restaurant\Offer\Models\Offer;

class OfferPolicy
{

    /**
     * Determine if the given user can view the offer.
     *
     * @param UserPolicy $user
     * @param Offer $offer
     *
     * @return bool
     */
    public function view(UserPolicy $user, Offer $offer)
    {
        if ($user->canDo('offer.offer.view') && $user->isAdmin()) {
            return true;
        }

        return $offer->user_id == user_id() && $offer->user_type == user_type();
    }

    /**
     * Determine if the given user can create a offer.
     *
     * @param UserPolicy $user
     * @param Offer $offer
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('offer.offer.create');
    }

    /**
     * Determine if the given user can update the given offer.
     *
     * @param UserPolicy $user
     * @param Offer $offer
     *
     * @return bool
     */
    public function update(UserPolicy $user, Offer $offer)
    {
        if ($user->canDo('offer.offer.edit') && $user->isAdmin()) {
            return true;
        }

        return $offer->user_id == user_id() && $offer->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given offer.
     *
     * @param UserPolicy $user
     * @param Offer $offer
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Offer $offer)
    {
        return $offer->user_id == user_id() && $offer->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given offer.
     *
     * @param UserPolicy $user
     * @param Offer $offer
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Offer $offer)
    {
        if ($user->canDo('offer.offer.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given offer.
     *
     * @param UserPolicy $user
     * @param Offer $offer
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Offer $offer)
    {
        if ($user->canDo('offer.offer.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
