<?php

namespace Restaurant\Offer;

use User;

class Offer
{
    /**
     * $offer object.
     */
    protected $offer;

    /**
     * Constructor.
     */
    public function __construct(\Restaurant\Offer\Interfaces\OfferRepositoryInterface $offer)
    {
        $this->offer = $offer;
    }

    /**
     * Returns count of offer.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return  0;
    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.offer.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->offer->pushCriteria(new \Litepie\Restaurant\Repositories\Criteria\OfferUserCriteria());
        }

        $offer = $this->offer->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('offer::' . $view, compact('offer'))->render();
    }
}
