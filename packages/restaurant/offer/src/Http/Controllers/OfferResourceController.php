<?php

namespace Restaurant\Offer\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Offer\Http\Requests\OfferRequest;
use Restaurant\Offer\Interfaces\OfferRepositoryInterface;
use Restaurant\Offer\Models\Offer;

/**
 * Resource controller class for offer.
 */
class OfferResourceController extends BaseController
{

    /**
     * Initialize offer resource controller.
     *
     * @param type OfferRepositoryInterface $offer
     *
     * @return null
     */
    public function __construct(OfferRepositoryInterface $offer)
    {
        parent::__construct();
        $this->repository = $offer;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Offer\Repositories\Criteria\OfferResourceCriteria::class);
    }

    /**
     * Display a list of offer.
     *
     * @return Response
     */
    public function index(OfferRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Offer\Repositories\Presenter\OfferPresenter::class)
                ->$function();
        }

        $offers = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('offer::offer.names'))
            ->view('offer::offer.index', true)
            ->data(compact('offers', 'view'))
            ->output();
    }

    /**
     * Display offer.
     *
     * @param Request $request
     * @param Model   $offer
     *
     * @return Response
     */
    public function show(OfferRequest $request, Offer $offer)
    {

        if ($offer->exists) {
            $view = 'offer::offer.show';
        } else {
            $view = 'offer::offer.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('offer::offer.name'))
            ->data(compact('offer'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new offer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(OfferRequest $request)
    {

        $offer = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('offer::offer.name')) 
            ->view('offer::offer.create', true) 
            ->data(compact('offer'))
            ->output();
    }

    /**
     * Create new offer.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(OfferRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type(); 
            $attributes['start_date'] = date('Y-m-d', strtotime($attributes['start_date']));
            $attributes['end_date'] = date('Y-m-d', strtotime($attributes['end_date']));
            $offer                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('offer::offer.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('offer/offer/' . $offer->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/offer/offer'))
                ->redirect();
        }

    }

    /**
     * Show offer for editing.
     *
     * @param Request $request
     * @param Model   $offer
     *
     * @return Response
     */
    public function edit(OfferRequest $request, Offer $offer)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('offer::offer.name'))
            ->view('offer::offer.edit', true)
            ->data(compact('offer'))
            ->output();
    }

    /**
     * Update the offer.
     *
     * @param Request $request
     * @param Model   $offer
     *
     * @return Response
     */
    public function update(OfferRequest $request, Offer $offer)
    {
        try {
            $attributes = $request->all();
            $attributes['start_date'] = date('Y-m-d', strtotime($attributes['start_date']));
            $attributes['end_date'] = date('Y-m-d', strtotime($attributes['end_date']));
            $offer->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('offer::offer.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('offer/offer/' . $offer->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('offer/offer/' . $offer->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the offer.
     *
     * @param Model   $offer
     *
     * @return Response
     */
    public function destroy(OfferRequest $request, Offer $offer)
    {
        try {

            $offer->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('offer::offer.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('offer/offer/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('offer/offer/' . $offer->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple offer.
     *
     * @param Model   $offer
     *
     * @return Response
     */
    public function delete(OfferRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('offer::offer.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('offer/offer'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/offer/offer'))
                ->redirect();
        }

    }

    /**
     * Restore deleted offers.
     *
     * @param Model   $offer
     *
     * @return Response
     */
    public function restore(OfferRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('offer::offer.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/offer/offer'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/offer/offer/'))
                ->redirect();
        }

    }

}
