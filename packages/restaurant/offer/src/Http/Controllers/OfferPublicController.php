<?php

namespace Restaurant\Offer\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Offer\Interfaces\OfferRepositoryInterface;

class OfferPublicController extends BaseController
{
    // use OfferWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Offer\Interfaces\OfferRepositoryInterface $offer
     *
     * @return type
     */
    public function __construct(OfferRepositoryInterface $offer)
    {
        $this->repository = $offer;
        parent::__construct();
    }

    /**
     * Show offer's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $offers = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$offer::offer.names'))
            ->view('offer::offer.index')
            ->data(compact('offers'))
            ->output();
    }


    /**
     * Show offer.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $offer = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$offer->name . trans('offer::offer.name'))
            ->view('offer::offer.show')
            ->data(compact('offer'))
            ->output();
    }

}
