<?php

namespace Restaurant\Offer;

use DB;
use Illuminate\Database\Seeder;

class OfferTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('offers')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'offer.offer.view',
                'name'      => 'View Offer',
            ],
            [
                'slug'      => 'offer.offer.create',
                'name'      => 'Create Offer',
            ],
            [
                'slug'      => 'offer.offer.edit',
                'name'      => 'Update Offer',
            ],
            [
                'slug'      => 'offer.offer.delete',
                'name'      => 'Delete Offer',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/offer/offer',
                'name'        => 'Offer',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/offer/offer',
                'name'        => 'Offer',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'offer',
                'name'        => 'Offer',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Offer',
                'module'    => 'Offer',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'offer.offer.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
