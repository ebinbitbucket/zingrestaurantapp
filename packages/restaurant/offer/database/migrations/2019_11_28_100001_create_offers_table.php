<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateOffersTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: offers
         */
        Schema::create('offers', function ($table) {
            $table->increments('id');
            $table->integer('restaurant_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('menu_id')->nullable();
            $table->enum('type', ['price','percentage'])->nullable();
            $table->decimal('value', 10,2)->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->text('image')->nullable();
            $table->text('name')->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('offers');
    }
}
