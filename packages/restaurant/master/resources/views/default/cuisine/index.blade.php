@extends('resource.index')
@php
$links['create'] = guard_url('master/cuisine/create');
$links['search'] = guard_url('master/cuisine');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('master::cuisine.title.main') !!}
@stop

@section('sub.title') 
{!! __('master::cuisine.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('master/cuisine')!!}">{!! __('master::cuisine.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('master::cuisine.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$cuisines->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
