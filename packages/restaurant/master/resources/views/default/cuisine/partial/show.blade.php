            <div class="content">
                <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('master::cuisine.label.id') !!}
                </label><br />
                    {!! $cuisine['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('master::cuisine.label.name') !!}
                </label><br />
                    {!! $cuisine['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="image">
                    {!! trans('master::cuisine.label.image') !!}
                </label><br />
                    {!! $cuisine['image'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="filter_show">
                    {!! trans('master::cuisine.label.filter_show') !!}
                </label><br />
                    {!! $cuisine['filter_show'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_id">
                    {!! trans('master::cuisine.label.user_id') !!}
                </label><br />
                    {!! $cuisine['user_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_type">
                    {!! trans('master::cuisine.label.user_type') !!}
                </label><br />
                    {!! $cuisine['user_type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('master::cuisine.label.created_at') !!}
                </label><br />
                    {!! $cuisine['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('master::cuisine.label.updated_at') !!}
                </label><br />
                    {!! $cuisine['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('master::cuisine.label.deleted_at') !!}
                </label><br />
                    {!! $cuisine['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('master::cuisine.label.name'))
                       -> placeholder(trans('master::cuisine.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('image')
                       -> label(trans('master::cuisine.label.image'))
                       -> placeholder(trans('master::cuisine.placeholder.image'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('filter_show')
                       -> label(trans('master::cuisine.label.filter_show'))
                       -> placeholder(trans('master::cuisine.placeholder.filter_show'))!!}
                </div>
            </div>