            <table class="table" id="main-table" data-url="{!!guard_url('master/cuisine?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="name">{!! trans('master::cuisine.label.name')!!}</th>
                    <th data-field="image">{!! trans('master::cuisine.label.image')!!}</th>
                    <th data-field="filter_show">{!! trans('master::cuisine.label.filter_show')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>