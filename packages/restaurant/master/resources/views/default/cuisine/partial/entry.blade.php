            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('master::cuisine.label.name'))
                       -> placeholder(trans('master::cuisine.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('image')
                       -> label(trans('master::cuisine.label.image'))
                       -> placeholder(trans('master::cuisine.placeholder.image'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('filter_show')
                       -> label(trans('master::cuisine.label.filter_show'))
                       -> placeholder(trans('master::cuisine.placeholder.filter_show'))!!}
                </div>
            </div>