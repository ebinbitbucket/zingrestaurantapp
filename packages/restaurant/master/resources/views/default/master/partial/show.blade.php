            <div class="content">
                <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('master::master.label.id') !!}
                </label><br />
                    {!! $master['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('master::master.label.name') !!}
                </label><br />
                    {!! $master['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="description">
                    {!! trans('master::master.label.description') !!}
                </label><br />
                    {!! $master['description'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="image">
                    {!! trans('master::master.label.image') !!}
                </label><br />
                    {!! $master['image'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="cuisine">
                    {!! trans('master::master.label.cuisine') !!}
                </label><br />
                    {!! $master['cuisine'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_id">
                    {!! trans('master::master.label.user_id') !!}
                </label><br />
                    {!! $master['user_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_type">
                    {!! trans('master::master.label.user_type') !!}
                </label><br />
                    {!! $master['user_type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('master::master.label.created_at') !!}
                </label><br />
                    {!! $master['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('master::master.label.updated_at') !!}
                </label><br />
                    {!! $master['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('master::master.label.deleted_at') !!}
                </label><br />
                    {!! $master['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('master::master.label.name'))
                       -> placeholder(trans('master::master.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('description')
                    -> label(trans('master::master.label.description'))
                    -> placeholder(trans('master::master.placeholder.description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('image')
                       -> label(trans('master::master.label.image'))
                       -> placeholder(trans('master::master.placeholder.image'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('cuisine')
                    -> options(trans('master::master.options.cuisine'))
                    -> label(trans('master::master.label.cuisine'))
                    -> placeholder(trans('master::master.placeholder.cuisine'))!!}
               </div>
            </div>