            <table class="table" id="main-table" data-url="{!!guard_url('master/master?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="name">{!! trans('master::master.label.name')!!}</th>
                    <th data-field="description">{!! trans('master::master.label.description')!!}</th>
                    <th data-field="image">{!! trans('master::master.label.image')!!}</th>
                    <th data-field="cuisine">{!! trans('master::master.label.cuisine')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>