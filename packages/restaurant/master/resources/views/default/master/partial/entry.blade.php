            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('master::master.label.name'))
                       -> placeholder(trans('master::master.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('description')
                    -> label(trans('master::master.label.description'))
                    -> placeholder(trans('master::master.placeholder.description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('image')
                       -> label(trans('master::master.label.image'))
                       -> placeholder(trans('master::master.placeholder.image'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('cuisine')
                    -> options(trans('master::master.options.cuisine'))
                    -> label(trans('master::master.label.cuisine'))
                    -> placeholder(trans('master::master.placeholder.cuisine'))!!}
               </div>
            </div>