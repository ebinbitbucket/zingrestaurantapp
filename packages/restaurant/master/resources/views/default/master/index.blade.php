@extends('resource.index')
@php
$links['create'] = guard_url('master/master/create');
$links['search'] = guard_url('master/master');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('master::master.title.main') !!}
@stop

@section('sub.title') 
{!! __('master::master.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('master/master')!!}">{!! __('master::master.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('master::master.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$masters->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
