            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('master::category.label.name'))
                       -> placeholder(trans('master::category.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('icon')
                       -> label(trans('master::category.label.icon'))
                       -> placeholder(trans('master::category.placeholder.icon'))!!}
                </div>
            </div>