<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> Mapping Links</small>
        </h1>
        <ol class="breadcrumb">

    </section>
    <!-- Main content -->
    <section class="content" >
            <div id='master-master-entry'>
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">  Master mapping [Click on the below list for each updation]</h3>

                    </div>
                </div>

            </div>
            <div class="nav-tabs-custom">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <a type="button" target="_blank" class="links" href='{{guard_url("restaurant/menu/updatesearchtext")}}'><i class="fa fa-refresh"></i> Click here to Update Menu Search Text </a></br>
                        <a type="button" target="_blank" class="links" href='{{guard_url("master/updatesearchtext")}}' ><i class="fa fa-refresh"></i> Click here to  Update Masters of Menus </a></br>
                        <a type="button" target="_blank" class="links" href='{{guard_url("master/updatemasterimage")}}' ><i class="fa fa-refresh"></i> Click here to  Update Master Image of Masters </a></br>
                        <a type="button" target="_blank" class="links" href='{{guard_url("restaurant/updatemaster")}}' ><i class="fa fa-refresh"></i> Click here to  Update Restaurant Masters </a></br>

                    </div>
                </div>
            </div>
    </section>
</div>
<style type="text/css">
    .links{
        line-height: 50px;
        font-size: 20px;
    }
</style>
<script type="text/javascript">

var oTable;
var oSearch;
$(document).ready(function(){
    // app.load('#master-master-entry', '{!!guard_url('master/master/0')!!}');
    oTable = $('#master-master-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }],

        "responsive" : true,
        "order": [[1, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('master/master') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'name'},
            {data :'description'},
            {data :'image'},
            {data :'cuisine'},
        ],
        "pageLength": 25
    });

    $('#master-master-list tbody').on( 'click', 'tr td:not(:first-child)', function (e) {
        e.preventDefault();

        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#master-master-list').DataTable().row( this ).data();
        $('#master-master-entry').load('{!!guard_url('master/master')!!}' + '/' + d.id);
    });

    $('#master-master-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent');
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#master-master-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }

    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#master-master-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#master-master-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});

$('#download_csv').click(function(){
      var filter = $("#form-search").serialize();
      $('#download_csv').attr('href',"{{guard_url('master/download/csv')}}"+ "/" + filter);
      $('#download_csv').attr("target", "_blank");
    });
</script>
