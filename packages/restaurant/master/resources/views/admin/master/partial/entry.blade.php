<div class='row'>
  <div class="col-md-6">
                <div class='col-md-12 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('master::master.label.name'))
                       -> placeholder(trans('master::master.placeholder.name'))!!}
                </div>

                <div class='col-md-12 col-sm-6'>
                    {!! Form::textarea ('description')
                    -> label(trans('master::master.label.description'))
                    -> placeholder(trans('master::master.placeholder.description'))!!}
                </div>
                
                <div class='col-md-12 col-sm-12'>
              {!! Form::text('tags')
              ->id('tags')
               -> label('Tags')
               -> placeholder('Tags')!!}
        </div>
         <div class='col-md-6 col-sm-6'>
                    {!! Form::text('cuisine')
              ->id('cuisine')
               -> label(trans('master::master.label.cuisine'))
               -> placeholder(trans('master::master.placeholder.cuisine'))!!}
               </div>

               <div class='col-md-6 col-sm-6'>
                    {!! Form::select('display_status')
               -> options(trans('master::master.options.display_status'))
               -> label(trans('master::master.label.display_status'))
               -> placeholder(trans('master::master.placeholder.display_status'))!!}
               </div>
    </div>
    <div class="col-md-6">    
                <div class='col-md-12 col-sm-6'>
                       <label for="image" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('master::cuisine.label.image') }}
              </label>
              <div class='col-lg-12 col-sm-12'>
                              {!! $master->files('image')
                              ->url($master->getUploadUrl('image'))
                              ->mime(config('filer.image_extensions'))
                              ->dropzone()!!}
              </div>
              <div class='col-lg-7 col-sm-12'>
                          {!! $master->files('image')
                          ->editor()!!}
              </div>
                </div>
               
            </div>
          </div>
<script>
$( document ).ready(function() {
    $('#cuisine').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'id',
        labelField: 'types',
        searchField: 'types',
        options: [
@forelse(Master::getCuisines() as $key => $types)

    {types: "{{$types}}",id:"{{$key}}" },
    @empty
    @endif
],
        create: function(input) {
            return {
                types: input
            }
        }
    });
     $('#tags').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'tag',
        labelField: 'tag',
        create: function(input) {
            return {
                tag: input
            }
        }
    });
});
</script>            