@forelse($cuisine as $key => $val)
<div class="cuisine-gadget-box">
    <p>{!!@$val->name!!}</p>
    <p class="text-muted"><small><i class="ion ion-android-person"></i> {!!@$val->user->name!!} at {!! format_date($val->created_at)!!}</small></p>
</div>
@empty
<div class="cuisine-gadget-box">
    <p>No Cuisine</p>
</div>
@endif