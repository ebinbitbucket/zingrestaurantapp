    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#cuisine" data-toggle="tab">{!! trans('master::cuisine.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#master-cuisine-edit'  data-load-to='#master-cuisine-entry' data-datatable='#master-cuisine-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#master-cuisine-entry' data-href='{{guard_url('master/cuisine')}}/{{$cuisine->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('master-cuisine-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('master/cuisine/'. $cuisine->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="cuisine">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('master::cuisine.name') !!} [{!!$cuisine->name!!}] </div>
                @include('master::admin.cuisine.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>