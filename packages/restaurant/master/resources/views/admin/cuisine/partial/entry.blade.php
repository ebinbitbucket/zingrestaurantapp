            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('master::cuisine.label.name'))
                       -> placeholder(trans('master::cuisine.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       <label for="image" class="control-label col-lg-12 col-sm-12 text-left"> {{trans('master::cuisine.label.image') }}
              </label>
              <div class='col-lg-12 col-sm-12'>
                              {!! $cuisine->files('image')
                              ->url($cuisine->getUploadUrl('image'))
                              ->mime(config('filer.image_extensions'))
                              ->dropzone()!!}
              </div>
              <div class='col-lg-7 col-sm-12'>
                          {!! $cuisine->files('image')
                          ->editor()!!}
              </div>
                </div>
                  
                <div class='col-md-4 col-sm-6'>
                </br>
                  <input type="checkbox" name="filter_show" class="custom-control-input" value="on" id="filter_show" {{$cuisine->filter_show == 'on' ? 'checked':''}} >
                                                    <label class="custom-control-label" for="filter_show">Need To show in filter</label>
                      <!--  {!! Form::numeric('filter_show')
                       -> label(trans('master::cuisine.label.filter_show'))
                       -> placeholder(trans('master::cuisine.placeholder.filter_show'))!!} -->
                </div>
            </div>