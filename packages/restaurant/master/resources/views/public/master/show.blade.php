<section class="result-header-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="result-metas">
                                <!-- <div class="meta-item">
                                    <i class="ion-android-restaurant"></i>
                                    <span>Chinese, Indian, American Dishes</span>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>


     <section class="single-dish-wrap listing-wrap">
                <div class="container" id="home_div">
                    @if(Session::has('message'))
                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                    @endif
                    <div class="row" id="refresh_div">
                        <div class="col-md-6">
                                @if(count($masters) == 1) 
                                    @forelse($masters as $master)
                                    <div class="single-dish-item">
                                        <!-- <button type="button" class="btn btn-danger remove-btn flaticon-garbage" onclick="removeSelectedMaster('{{$master->id}}')"></button> -->
                                        <div class="item-img" style="background-image: url({{url($master->defaultImage('image','sm'))}}); height: 300px;"></div>
                                        <div class="item-content">
                                            <h3>{{$master->name}}</h3>
                                        </div>
                                    </br>
                                        <p>{{$master->description}}</p>
                                @empty
                                @endif
                                @else
                                <div class="row">
                                @forelse($masters as $master)
                                <div class="col-md-6">
                                    <div class="single-dish-item">
                                        <button type="button" class="btn btn-danger remove-btn flaticon-garbage" onclick="removeSelectedMaster('{{$master->id}}')"></button>
                                        <div class="item-img" style="background-image: url({{url($master->defaultImage('image','sm'))}})"></div>
                                        <div class="item-content">
                                            <h3>{{$master->name}}</h3>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6" id="restaurants_refresh">
                            <div class="search-meta-wrapper">
                                <p class="search-number">{{count($restaurants)}} Results found within 10 miles of the entered address.</p>
                                <a href="{{url('/')}}" class="back-btn">Back to Listing</a>
                                <!--<a href="#" onClick="history.go(0)" class="reset-btn">Reset Filter</a>-->
                            </div>
                            <select name="sort_filter" placeholder="Sort by" id="sort_filter" class="form-control" style="width: 50%;margin-left: 15px;margin-bottom: 10px;">
                                    <option>Sort By</option>
                                    <option value="distance">Distance</option>
                                    <option value="pricelh">Price (Low to High)</option>
                                    <option value="pricehl">Price (High to Low)</option>
                                    <option value="rating">Rating (High to Low)</option>
                                </select>
                            <div class="listing-wrap-inner">
                                @forelse($restaurants as $restaurant) 
                                <?php $flag=0;
                                $flag=Restaurant::getOpenStatus($restaurant->id); ?> 
                                <div class="listing-item" style="{{$flag!=1 ? 'background-color:#ededed;' : ''}}">
                                    <div class="img-holder">
                                        <figure>
                                            <?php $s = json_decode($restaurant->logo); ?> 
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}">
                                                <img src="{{!empty($s[0]->path)? url('image/original/'.$s[0]->path) : url('img/default/original.jpg')}}" class="img-fluid" alt="">
                                            </a>
                                            
                                        </figure>
                                    </div>
                                    <div class="text-holder">
                                        <h3 class="title">
                                                <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}">{{$restaurant->name}}</a>
                                           
                                            <a href="#" class="type" role="button" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="Food Truck" data-original-title="" title=""><i class="{{@$restaurant->masterCategory->icon}}"></i></a>
                                        </h3>
                                         <div class="location" style="{{$flag!=1 ? 'color:red;' : 'color:#40b659;'}}"><i class="fa fa-clock-o" style="{{$flag!=1 ? 'color:red;' : ''}}"></i> {{$flag!=1 ? 'Closed' : 'Open'}}</div>
                                        <div class="info">
                                            <p class="type">{{number_format($restaurant->distance,2)}} mi</p>
                                            <span class="raty" data-score="{{$restaurant->rating}}"></span>
                                        </div>
                                         <p class="type">{{$restaurant->type}}</p>
                                        <div class="location"><i class="ion ion-android-pin"></i> {{$restaurant->address}}</div>
                                         <div class="meta-infos"> 
                                       @if($restaurant->delivery =='Yes')
                                                 <div class="info" style="padding-left: 25px;">    <i class="flaticon-fast-delivery"></i>@if($restaurant->delivery_charge == 0)
                                            Free Delivery
                                        @elseif($restaurant->delivery_charge != '')
                                           Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                        @endif {{!empty($restaurant->delivery_limit) ? '(within '.$restaurant->delivery_limit.' mi)' : ''}}</div>
                                            @endif
                                        </div>
                                        <?php $res_menu = Restaurant::getMenuName($restaurant->id); ?>
                                        @foreach($res_menu as $res_menu_det)
                                        <div class="meta-infos"> 
                                       
                                            
                                            <div class="info" ><b>{{$res_menu_det->name}}</b></div>
                                            <div class="info">${{$res_menu_det->price}}</div>
                                            <div class="info"><span class="raty" data-score="{{$res_menu_det->review}}"></span></div>
                                        </div>  
                                        @endforeach
                                    </div>
                                </div>
                                @empty
                                    <div class="no-items-found text-center">
                                        <img src="{{theme_asset('img/no-restaurants.svg')}}" alt="">
                                        <h4>No Restaurants found.</h4>
                                        <p>No Restaurants having this menu item. Please try another menu item.</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<script type="text/javascript">
    function removeSelectedMaster(master_id){
                    $.ajax({
                    url: "{{ URL::to('master/removemaster') }}/"+master_id,
                    dataType:'JSON',
                    success: function(response){

                            console.log("New Order",response);
                            document.location.reload()
                    },
                    error: function(msg) { 
                           
                          }
                });
    }
   
   $(document).ready(function(){
                 $('.header-search-form-wrap .search-item .dropdown-menu').click(function(e) {
                    e.stopPropagation();
                 });
                 $('input[id="schedule"]').click(function() {
                    $('#scheduleModal').modal('show');
                });
                 var someSessionVariable = '<?php echo Session::get('location'); ?>';

        if(someSessionVariable == '')
         {
           $('#locationModel').modal({show:true, backdrop: 'static',
            keyboard: false}); 

         }
            });
    $('#sort_filter').change(function(e){

    $.ajax({
                    url: "{{ URL::to('restaurant/restaurantlisting') }}/",
                    data: {sort_filter:$("#sort_filter option:selected").val()},
                    dataType:'JSON',
                    success: function(response){

                            console.log("New Order",response);
                            $('#restaurants_refresh').html(response.new_menus);
                    },
                    error: function(msg) {

                          }
                });
})
</script>