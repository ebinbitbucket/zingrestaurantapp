<section class="result-header-wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="result-metas">
                    <div class="meta-item" id="cuisine_label" style="display: none;">
                        <i class="ion-android-restaurant"></i>
                        <div id="cuisine_names"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="listing-wrap dish-style-card clearfix"> 
<a  class="btn btn-theme search-btn" style="margin-left:40%;margin-top:10px;margin-bottom:10px;" id="frd_btn" href="{{trans_url('master/masters/selectedlist')}}">Find Eateries</a>
    <div class="container clearfix" id="gggg">  
        <div class="dish-grid are-images-unloaded clearfix" data-js="image-grid">
            <div class="dish-grid__col-sizer"></div>
            <div class="dish-grid__gutter-sizer"></div>
            @if(empty($datas['counts']))
  <div style="padding-top: 100px;padding-bottom: 100px;">
  <center>
      <b>This Dish is not available in your area.</b>
  </center>
</div>
   
  @endif
     @forelse($datas['menus'] as $menu) 
<div class="cbp-item col-lg-3 col-md-4 col-sm-6 col-12 with-spacing">
                            <div class="dish-item">
                                <div class="dish-image">
                                    <a class="dish-link" href="#" onclick="addToSession('{{$menu->id}}')"></a>
                                    <div class="dish-overlay"></div>
                                    <img src="{{url($menu->defaultImage('image','master'))}}" class="img-fluid" alt="">
                                    <div class="dish-actions">
                                        <a href="#" class="favorite-btn" id="{{$menu->id}}" onclick="addToSession('{{$menu->id}}')"><i class="ion ion-plus-round"></i></a>
                                    </div>
                                    <div class="dish-footer-actions">
                                        <!-- <div class="action-item">
                                            <a href="#" role="button" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Food Truck" style="color: #3f51b5"><i class="flaticon-fast-delivery"></i></a>
                                        </div> -->
                                        <div class="action-item view-more-btn">
                                            <a href="javascript:void(0);" role="button" data-placement="left" data-toggle="popover" data-trigger="focus" data-content="{{(!empty($data['counts'][$menu->id])) ? $data['counts'][$menu->id]:0}} items"><i class="ion ion-more"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="dish-description">
                                    <h3 class="dish-title"><a href="#">{{$menu->name}}</a></h3>
                                    
                                </div>
                            
                            </div>
                        </div>
           
@empty
<div class="cbp-item col-md-12 no-items-found">
    <img src="{{theme_asset('img/no-dish-items.svg')}}" alt="No items Found">
   <h4>No dishes served in your area match the search keywords used.</h4>
</div>
@endif
</div>
        <div class="page-load-status">
            <div class="loader-ellips infinite-scroll-request">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
            </div>
            <p class="infinite-scroll-last">End of Dishes</p>
            <p class="infinite-scroll-error">No more Dishes to load</p>
        </div>
    </div>
</section>
<div class="modal masters-modal fade bd-example-modal-lg" id="MasterSessionModel" tabindex="-1" role="dialog" aria-labelledby="MasterSessionModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content"> 
            
        </div>
    </div>
</div>  

<script>
            $(document).ready(function(){
                document.getElementById('master_search').value = '{{$name}}';
                 $('.header-search-form-wrap .search-item .dropdown-menu').click(function(e) {
                    e.stopPropagation();
                 });
                 $('input[id="schedule"]').click(function() {
                    $('#scheduleModal').modal('show');
                });
            
            var $grid = $('.dish-grid').masonry({
    itemSelector: 'none',
    columnWidth: '.dish-grid__col-sizer',
    gutter: '.dish-grid__gutter-sizer',
    percentPosition: true,
    stagger: 30,
    visibleStyle: { transform: 'translateY(0)', opacity: 1 },
    hiddenStyle: { transform: 'translateY(100px)', opacity: 0 },
});
var msnry = $grid.data('masonry');
$grid.imagesLoaded( function() {
    $grid.removeClass('are-images-unloaded');
    $grid.masonry( 'option', { itemSelector: '.dish-grid__item' });
    var $items = $grid.find('.dish-grid__item');
    $grid.masonry( 'appended', $items );
});

// var nextPenSlugs = [
//   'page=2'
// ];

// function getPenPath() {
//     var slug = nextPenSlugs[ this.loadCount ];
//     if ( slug ) {
//         return "{{url('loadmenus')}}" +'?'+ slug;
//     }
// }

$grid.infiniteScroll({
    path: function() {
      var pageNumber = ( this.loadCount + 2 ) * 1;
      return "{{url('loadmenus')}}" + '?' + 'page=' + pageNumber;
    },
    append: '.dish-grid__item',
    outlayer: msnry,
    status: '.page-load-status',
    history: false
});
function search(){
    // e.preventDefault();
    var master_name = document.getElementById('master_search').value;
    $.ajax({
            url: "{{ URL::to('master/masters') }}/",
            dataType:'JSON',
            data: {name:master_name},
            success: function(response){

                    console.log("New Order",response);
                    $('#gggg').html(response.new_menus);
            },
            error: function(msg) { 
                   
                  document.location.reload()
                  }
        });
}

$('#master_search').change(function(e){
    e.preventDefault();
    var master_name = document.getElementById('master_search').value;
    $.ajax({
            url: "{{ URL::to('master/masters') }}/",
            dataType:'JSON',
            data: {name:master_name},
            success: function(response){

                    console.log("New Order",response);
                    $('#gggg').html(response.new_menus);
            },
            error: function(msg) { 
                   
                  document.location.reload()
                  }
        });
})

$('#master_search_mob').change(function(e){
    e.preventDefault();
    var master_name = document.getElementById('master_search_mob').value;
    $.ajax({
            url: "{{ URL::to('master/masters') }}/",
            dataType:'JSON',
            data: {name:master_name},
            success: function(response){

                    console.log("New Order",response);
                    $('#gggg').html(response.new_menus);
            },
            error: function(msg) { 
                   
                  document.location.reload()

                  }
        });
})

function addToSessions(menu){
    if($('#'+menu).hasClass('active') == false){
        $('#'+menu).addClass('active');
    }
    else{
        $('#'+menu).removeClass('active');
    }
    
     var fav_status = $('#'+menu).hasClass('active');
    console.log(fav_status);
        if(fav_status == true)
            fav_status = 'add';
        else
            fav_status = 'remove';
    $.ajax({
            url: "{{ URL::to('master/addtomasters') }}"+'/'+fav_status,
            dataType:'JSON',
            data: {name:menu},
            success: function(response){
                if(fav_status == 'add'){                
                     $('.masters-modal .modal-content').load("{{url('master/selected_masters')}}",function(){ $('#MasterSessionModel').modal({show:true}); 
                    });
                }
                else{
                    alert('Dish removed from your list.')
                }
                
            },
            error: function(msg) { 
                   
                    document.location.reload()
                  }
        });
}

function addToSession(menu){
    var fav_status = $('#'+menu).hasClass('active');
    console.log(fav_status);
        if(fav_status == false)
            fav_status = 'add';
        else
            fav_status = 'remove';
    $.ajax({
            url: "{{ URL::to('master/addtomasters') }}"+'/'+fav_status,
            dataType:'JSON',
            data: {name:menu},
            success: function(response){
                if(fav_status == 'add'){                
                     $('.masters-modal .modal-content').load("{{url('master/selected_masters')}}",function(){ $('#MasterSessionModel').modal({show:true}); 
                    });
                }
                else{
                    alert('Dish removed from your list.')
                }
                
            },
            error: function(msg) { 
                   
                    document.location.reload()
                  }
        });
}
 function removeSelectedMaster(master_id){
    $('.masters-modal .modal-content').load("{{url('master/selected_masters?remove_id=')}}"+master_id,function()
        { $('#MasterSessionModel').modal({show:true});
    });
}
});
        </script>        