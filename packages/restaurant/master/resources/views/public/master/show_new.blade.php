<div class="col-md-6">
      @if(count($masters) == 1) 
                                    @forelse($masters as $master)
                                    <div class="single-dish-item">
                                        <!--<button type="button" class="btn btn-danger remove-btn flaticon-garbage" onclick="removeSelectedMaster('{{$master->id}}')"></button>-->
                                        <div class="item-img" style="background-image: url({{url($master->defaultImage('image','sm'))}}); height: 300px;"></div>
                                        <div class="item-content">
                                            <h3>{{$master->name}}</h3>
                                        </div>
                                    </br>
                                        <p>{{$master->description}}</p>
                                @empty
                                @endif
                                @else
                            <div class="row">
                                @forelse($masters as $master)
                                <div class="col-md-6">
                                    <div class="single-dish-item">
                                        <button type="button" class="btn btn-danger remove-btn flaticon-garbage" onclick="removeSelectedMaster('{{$master->id}}')"></button>
                                        <div class="item-img" style="background-image: url({{url($master->defaultImage('image','sm'))}})"></div>
                                        <div class="item-content">
                                            <h3>{{$master->name}}</h3>
                                        </div>
                                    </div>
                                </div>
                                @empty
                                @endif
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="search-meta-wrapper">
                                <!--<p class="search-number" style="padding-bottom:10px;font-weight: 500;"><b>Filters : </b>-->
                                <!--@if(!empty(Session::get('search_time')))-->
                                <!--    Open at <b>{{date('d M h:i A',strtotime(Session::get('search_time')))}}</b>-->
                                <!--@endif-->
                                <!--@if(!empty(Session::get('delivery_master')))-->
                                <!--    , <b>{{Session::get('delivery_master')}}</b> Only-->
                                <!--@endif-->
                                <!--@if(!empty(Session::get('price_range_min_master')))-->
                                <!--    , Dish Price <b>{{Session::get('price_range_min_master')}} - {{Session::get('price_range_max_master')}}</b>-->
                                <!--@else-->
                                <!--    , Dish Price <b>$0 - $30</b>-->
                                <!--@endif-->
                                <!--@if(!empty(Session::get('rating_master')))-->
                                <!--    , Above <b>{{Session::get('rating_master')}}</b> stars-->
                                <!--@else-->
                                <!--    , Above <b>4</b> stars-->
                                <!--@endif-->
                                <!--@if(!empty(Session::get('category_name_master')))-->
                                <!--    , All <b>{{Session::get('category_name_master')}}</b> Types-->
                                <!--@else-->
                                <!--    , <b>All Eatery Types</b>-->
                                <!--@endif-->
                                <p class="search-number">{{count($restaurants)}} Results found within 10 miles of the entered address.</p>
                                <a href="{{url('/')}}" class="back-btn">Back to Listing</a>
                                <!--<a href="#" onClick="history.go(0)" class="reset-btn">Reset Filter</a>-->
                            </div>                               

                            <div class="listing-wrap-inner">
                                @forelse($restaurants as $restaurant) 
                                   <?php $flag=0;
                                $flag=Restaurant::getOpenStatus($restaurant->id); ?> 
                                <div class="listing-item" style="{{$flag!=1 ? 'background-color:#ededed;' : ''}}">
                                    <div class="img-holder">
                                        <figure>
                                           
                                            <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}">
                                                <img src="{{!empty($s[0]->path)? url('image/original/'.$s[0]->path) : url('img/default/original.jpg')}}" class="img-fluid" alt="">
                                            </a>
                                            
                                        </figure>
                                    </div>
                                    <div class="text-holder">
                                        <h3 class="title">
                                            
                                                <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}">{{$restaurant->name}}</a> 
                                            
                                            <a href="#" class="type" role="button" data-toggle="popover" data-placement="right" data-trigger="focus" data-content="Food Truck" data-original-title="" title=""><i class="{{@$restaurant->masterCategory->icon}}"></i></a>
                                        </h3>
                                        <div class="location" style="{{$flag!=1 ? 'color:red;' : 'color:#40b659;'}}"><i class="fa fa-clock-o" style="{{$flag!=1 ? 'color:red;' : ''}}"></i> {{$flag!=1 ? 'Closed' : 'Open'}}</div>
                                        <div class="info"><span class="raty" data-score="{{$restaurant->rating}}"></span></div>
                                        <div class="location"><i class="ion ion-android-pin"></i> {{$restaurant->address}}</div>
                                        <?php $res_menu = Restaurant::getMenuName($restaurant->id); ?>
                                        @foreach($res_menu as $res_menu_det)
                                        <div class="meta-infos"> 
                                       
                                            
                                            <div class="info" ><b>{{$res_menu_det->name}}</b></div>
                                            <div class="info">${{$res_menu_det->price}}</div>
                                            <div class="info"><span class="raty" data-score="{{$res_menu_det->review}}"></span></div>
                                        </div>  
                                        @endforeach
                                    </div>
                                </div>
                                @empty
                                    <div class="no-items-found">
                                        <img src="{{theme_asset('img/no-restaurants.svg')}}" alt="">
                                        <h4>No Restaurants found.</h4>
                                        <p>No Restaurants having this menu item. Please choose another menu item</p>
                                    </div>
                                @endif
                            </div>
                        </div>
<script type="text/javascript">
        $("[data-toggle='tooltip']").tooltip();

</script>                    