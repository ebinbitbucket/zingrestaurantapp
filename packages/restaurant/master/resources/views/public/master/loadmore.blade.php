@forelse($datas->items() as $master) 
<div class="dish-grid__item">
    <div class="dish-grid__image-block">
        <a href="javascript:void(0);" class="dish-link" onclick="addToSession('{{$master->id}}')"></a>
        <img class="dish-grid__image" src="{{url($master->defaultImage('image','master'))}}">
        <div class="dish-overlay"></div>
        <a href="javascript:void(0);" class="favorite-btn" id="{{$master->id}}" onclick="addToSession('{{$master->id}}')"><i class="ion ion-plus-round"></i></a>
        <a href="javascript:void(0);" class="view-more-btn"><i class="ion ion-more"></i></a>
        <div class="dish-popover">
        	<p><i class="ion ion-social-buffer" style="font-size : 20px;"></i>&nbsp;{{$master->local}} local and {{$master->chain}} chain Eateries serving this dish within 10 miles of your address.</p>
        	<p><b style="font-size : 16px;">Description: </b>{{str_limit($master->description,150)}}</p>
        </div>
    </div>
    <h3 class="dish-title"><a href="#">{{$master->name}}</a></h3>
</div>
@empty
@endif