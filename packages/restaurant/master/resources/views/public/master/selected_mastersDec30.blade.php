<form class="advance-filter-form restaurant_filter">
<div class="modal-header-filter-wrap">
    <div class="modal-filter-wrap-inner">
        <div class="filter-item filter-item-sche">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-clock"></i> <span><span id="del_date_v">Any Hours</span>
                     <span id="del_time_v"></span></span></button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <div class="del-time-wrap">
                        <div class="time-item">
                            <input type="radio" name="del_time_type" id="all_time" value="all" checked="true">
                            <label for="all_time"><i class="ik ik-clock"></i>Any Hours</label>
                        </div>
                        <div class="time-item">
                            <input type="radio" name="del_time_type" id="asap_time" value="asap">
                            <label for="asap_time"><i class="ik ik-bell"></i>Open Now</label>
                        </div>
                        <div class="time-item">
                            <input type="radio" id="schedule_time" name="del_time_type" value="scheduled">
                            <label for="schedule_time"><i class="ik ik-calendar"></i>Change Time</label>
                        </div>
                    </div>
                    <div class="schedule-inner-block" id="scedule-block" style="display: none;">
                        <div class="form-group">
                          <p>Change Time to see eateries that are open at the selected time.</p>
                            <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                 @for($i=0;$i<5;$i++)
                                        <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('d M', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                            <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                            <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                        </div>
                                @endfor
                            </div>
                        </div>
                        <div class="form-group mb-0">
                           <!--  <p>Desired Delivery Time</p> -->
                            <select class="form-control" id="sch_time">
                                <option value="">Select a Time</option>
                                <option>12:00 AM</option>
                                <option>12:15 AM</option>
                                <option>12:30 AM</option>
                                <option>12:45 AM</option>
                                <option>1:00 AM</option>
                                <option>1:15 AM</option>
                                <option>1:30 AM</option>
                                <option>1:45 AM</option>
                                <option>2:00 AM</option>
                                <option>2:15 AM</option>
                                <option>2:30 AM</option>
                                <option>2:45 AM</option>
                                <option>3:00 AM</option>
                                <option>3:15 AM</option>
                                <option>3:30 AM</option>
                                <option>3:45 AM</option>
                                <option>4:00 AM</option>
                                <option>4:15 AM</option>
                                <option>4:30 AM</option>
                                <option>4:45 AM</option>
                                <option>5:00 AM</option>
                                <option>5:15 AM</option>
                                <option>5:30 AM</option>
                                <option>5:45 AM</option>
                                <option>6:00 AM</option>
                                <option>6:15 AM</option>
                                <option>6:30 AM</option>
                                <option>6:45 AM</option>
                                <option>7:00 AM</option>
                                <option>7:15 AM</option>
                                <option>7:30 AM</option>
                                <option>7:45 AM</option>
                                <option>8:00 AM</option>
                                <option>8:15 AM</option>
                                <option>8:30 AM</option>
                                <option>8:45 AM</option>
                                <option>9:00 AM</option>
                                <option>9:15 AM</option>
                                <option>9:30 AM</option>
                                <option>9:45 AM</option>
                                <option>10:00 AM</option>
                                <option>10:15 AM</option>
                                <option>10:30 AM</option>
                                <option>10:45 AM</option>
                                <option>11:00 AM</option>
                                <option>11:15 AM</option>
                                <option>11:30 AM</option>
                                <option>11:45 AM</option>
                                <option>12:00 PM</option>
                                <option>12:15 PM</option>
                                <option>12:30 PM</option>
                                <option>12:45 PM</option>
                                <option>1:00 PM</option>
                                <option>1:15 PM</option>
                                <option>1:30 PM</option>
                                <option>1:45 PM</option>
                                <option>2:00 PM</option>
                                <option>2:15 PM</option>
                                <option>2:30 PM</option>
                                <option>2:45 PM</option>
                                <option>3:00 PM</option>
                                <option>3:15 PM</option>
                                <option>3:30 PM</option>
                                <option>3:45 PM</option>
                                <option>4:00 PM</option>
                                <option>4:15 PM</option>
                                <option>4:30 PM</option>
                                <option>4:45 PM</option>
                                <option>5:00 PM</option>
                                <option>5:15 PM</option>
                                <option>5:30 PM</option>
                                <option>5:45 PM</option>
                                <option>6:00 PM</option>
                                <option>6:15 PM</option>
                                <option>6:30 PM</option>
                                <option>6:45 PM</option>
                                <option>7:00 PM</option>
                                <option>7:15 PM</option>
                                <option>7:30 PM</option>
                                <option>7:45 PM</option>
                                <option>8:00 PM</option>
                                <option>8:15 PM</option>
                                <option>8:30 PM</option>
                                <option>8:45 PM</option>
                                <option>9:00 PM</option>
                                <option>9:15 PM</option>
                                <option>9:30 PM</option>
                                <option>9:45 PM</option>
                                <option>10:00 PM</option>
                                <option>10:15 PM</option>
                                <option>10:30 PM</option>
                                <option>10:45 PM</option>
                                <option>11:00 PM</option>
                                <option>11:15 PM</option>
                                <option>11:30 PM</option>
                                <option>11:45 PM</option>
                            </select>
                        </div>
                    </div>
                    <div class="schedule-inner-block" id="all-block" >
                        <div class="form-group mb-0">
                            <p>See Eateries Open or Closed any time today.</p>
                        </div>
                    </div>
                    <div class="schedule-inner-block" id="open-block" style="display: none;">
                        <div class="form-group mb-0">
                            <p>See eateries that are open now.</p>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="working_date" id="datecheck" value="{{date('d M', strtotime(date('d-M-Y')))}}">
            <input type="hidden" name="working_hours" id="timecheck" value="{{date('g:i A')}}">
            <input type="hidden" name="time_type" id="time_type">
            <input type="hidden" name="sort_filter" id="sort_filter1">
        </div>
        <div class="filter-item filter-item-type">
            <select name="type" id="order_type" class="selectize">
                 <option value="pickup" >Take Out</option>
                <option value="orderpickup" >Order Take Out</option>
                <option value="orderdelivery">Order Delivery</option>
            </select>
        </div>
        <div class="filter-item filter-item-rating">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-star"></i> <input type="text" id="curRating" value="1"  readonly></button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <p>Select Rating</p>
                    <div class="select-rating-wrap">
                        <select id="rating_pill_Header" name="rating" autocomplete="off">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2" >2</option>
                            <option value="3">3</option>
                            <option value="4" >4</option>
                            <option value="5">5</option>
                        </select>
                        <div class="rating-all">
                            <input type="checkbox" id="rating_all" value="0" name="rating">
                            <label for="rating_all"><i class="fa fa-star"></i>Any</label>
                        </div>
                    </div>
                   
                   
                </div>
            </div>
        </div>
        <div class="filter-item filter-item-price">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="price_change">$ Any</span></button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <p>Select Price</p>
                    <div class="pricing-block">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" value="0" name="price" id="inlineRadio0" data-value="$ Any">
                            <label class="form-check-label" for="inlineRadio0">$ Any</label>
                            <span>Any</span>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" value="1" name="price" id="inlineRadio1" data-value="$">
                            <label class="form-check-label" for="inlineRadio1">$</label>
                            <span>0-5</span>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" value="5" name="price" id="inlineRadio2" data-value="$$">
                            <label class="form-check-label" for="inlineRadio2">$$</label>
                            <span>5-12</span>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" value="12" name="price" id="inlineRadio3" data-value="$$$">
                            <label class="form-check-label" for="inlineRadio3">$$$</label>
                            <span>12-20</span>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" value="20" name="price" id="inlineRadio4" data-value="$$$$">
                            <label class="form-check-label" for="inlineRadio4">$$$$</label>
                            <span>20+</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="filter-item filter-item-category">
            <select name="main_category" id="main_category" class="selectize">
                <option value="all" selected>All Eateries</option>
                 @forelse(Master::getMasterAllCategories() as $key => $categories)
                    <option value="{{$categories->id}}" >{{ucfirst($categories->name)}}</option>
                @empty
                @endif
            </select>
        </div>
        <div class="filter-item">
           <select class="selectize" name="chain_list" id="chain_list">
                <option value='all'>All</option>
                    <option value="No" selected="selected">Local</option>
                    <option value="Yes">Chain</option>
            </select>
        </div>
        <div class="filter-item filter-item-range">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="distance">10mi</span></button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                    <p class="mb-10">Select Distance</p>
                    <div class="range-slider">
                        <input id="distanceRange" data-type="single" data-min="3" data-max="25" data-postfix="mi" type="text" name="maxDistance" value="" />
                        <input type="hidden" name="maxDistance" id="distanceRange_val" value="10">
                    </div>
                </div>
            </div>
        </div>
        <div class="filter-item">
            <button type="button" class="btn btn-secondary" id="reset_all">Reset</button>
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ion ion-ios-close-outline"></span></button>
</div>
</form>
<div class="modal-body p-0">
    <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="ion ion-ios-close-outline"></span></button> -->
    <div class="row no-gutters">
        <div class="col-sm-3">
            <div class="selected-master-wrap">
                @if(!empty($masters))
                <div class="selected-dishes">
                    @if(count($masters) == 1)
                    @forelse($masters as $master)
                    <div class="dish-item">
                        <button type="button" class="btn btn-danger remove-btn ion-android-close" onclick="removeSelectedMaster('{{$master->id}}')"></button>
                        <div class="item-img">
                            <img src="{{url($master->defaultImage('image','master'))}}" class="img-fluid" alt="{{$master->name}}">
                        </div>
                        <div class="item-content">
                            <h3>{{$master->name}}</h3>
                        </div>
                    </div>
                    @endforeach
                    @else
                    @foreach($masters as $master)
                    <div class="dish-item">
                        <button type="button" class="btn btn-danger remove-btn ion-android-close" onclick="removeSelectedMaster('{{$master->id}}')"></button>
                        <div class="item-img">
                            <img src="{{url($master->defaultImage('image','master'))}}" class="img-fluid" alt="{{$master->name}}">
                        </div>
                        <div class="item-content">
                            <h3>{{$master->name}}</h3>
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <a class="dish-item add-more-wrap-mobile" href="#">
                         <button type="button" style="border: none; background: none;" data-dismiss="modal" aria-label="Close"><i class="ion-ios-plus-outline"></i></button>
                    </a>
                </div>
                @endif
                <div class="mt-20 text-center add-more-wrap">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">Add More</button>
                </div>
            </div>
        </div>
        <div class="col-sm-9">
            <form class="restaurant_filter_Mob">
            <div class="modal-header-filter-wrap-mobile">
                <div class="modal-filter-wrap-inner">
                    <div class="filter-item filter-item-sche">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ik ik-clock"></i> <span><span id="del_date_vMob">Any Hours</span>
                            <span id="del_time_vMob"></span></span></button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                 <div class="del-time-wrap">
                                    <div class="time-item">
                                        <input type="radio" name="del_time_typeMob" id="all_timeMob" value="all" checked="checked">
                                        <label for="all_timeMob"><i class="ik ik-clock"></i>All</label>
                                    </div>
                                    <div class="time-item">
                                        <input type="radio" name="del_time_typeMob" id="asap_timeMob" value="asap">
                                        <label for="asap_timeMob"><i class="ik ik-bell"></i>Open Now</label>
                                    </div>
                                    <div class="time-item">
                                        <input type="radio" id="schedule_timeMob" name="del_time_typeMob" value="scheduled">
                                        <label for="schedule_timeMob"><i class="ik ik-calendar"></i>Change Time</label>
                                    </div>
                                </div>
                                <div class="schedule-inner-block" id="scedule-blockMob" style="display: none;">
                                    <div class="form-group">
                                       <p>Change Time to see eateries that are open at the selected time.</p>
                                        <div class="sch-date-wrap" id="sch_DateWrapMob" name="datesearch">
                                             @for($i=0;$i<5;$i++)
                                                    <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('d M', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}">
                                                        <span class="day">{{date('D', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                                        <span class="date">{{date('d', strtotime(date('d-M-Y'). ' + '. $i .'days'))}}</span>
                                                    </div>
                                            @endfor
                                        </div>
                                    </div>
                                    <div class="form-group mb-0">
                                        <!-- <p>Desired Delivery Time</p> -->
                                        <select class="form-control" id="sch_timeMob">
                                            <option value="">Select a Time</option>
                                            <option>12:00 AM</option>
                                            <option>12:15 AM</option>
                                            <option>12:30 AM</option>
                                            <option>12:45 AM</option>
                                            <option>1:00 AM</option>
                                            <option>1:15 AM</option>
                                            <option>1:30 AM</option>
                                            <option>1:45 AM</option>
                                            <option>2:00 AM</option>
                                            <option>2:15 AM</option>
                                            <option>2:30 AM</option>
                                            <option>2:45 AM</option>
                                            <option>3:00 AM</option>
                                            <option>3:15 AM</option>
                                            <option>3:30 AM</option>
                                            <option>3:45 AM</option>
                                            <option>4:00 AM</option>
                                            <option>4:15 AM</option>
                                            <option>4:30 AM</option>
                                            <option>4:45 AM</option>
                                            <option>5:00 AM</option>
                                            <option>5:15 AM</option>
                                            <option>5:30 AM</option>
                                            <option>5:45 AM</option>
                                            <option>6:00 AM</option>
                                            <option>6:15 AM</option>
                                            <option>6:30 AM</option>
                                            <option>6:45 AM</option>
                                            <option>7:00 AM</option>
                                            <option>7:15 AM</option>
                                            <option>7:30 AM</option>
                                            <option>7:45 AM</option>
                                            <option>8:00 AM</option>
                                            <option>8:15 AM</option>
                                            <option>8:30 AM</option>
                                            <option>8:45 AM</option>
                                            <option>9:00 AM</option>
                                            <option>9:15 AM</option>
                                            <option>9:30 AM</option>
                                            <option>9:45 AM</option>
                                            <option>10:00 AM</option>
                                            <option>10:15 AM</option>
                                            <option>10:30 AM</option>
                                            <option>10:45 AM</option>
                                            <option>11:00 AM</option>
                                            <option>11:15 AM</option>
                                            <option>11:30 AM</option>
                                            <option>11:45 AM</option>
                                            <option>12:00 PM</option>
                                            <option>12:15 PM</option>
                                            <option>12:30 PM</option>
                                            <option>12:45 PM</option>
                                            <option>1:00 PM</option>
                                            <option>1:15 PM</option>
                                            <option>1:30 PM</option>
                                            <option>1:45 PM</option>
                                            <option>2:00 PM</option>
                                            <option>2:15 PM</option>
                                            <option>2:30 PM</option>
                                            <option>2:45 PM</option>
                                            <option>3:00 PM</option>
                                            <option>3:15 PM</option>
                                            <option>3:30 PM</option>
                                            <option>3:45 PM</option>
                                            <option>4:00 PM</option>
                                            <option>4:15 PM</option>
                                            <option>4:30 PM</option>
                                            <option>4:45 PM</option>
                                            <option>5:00 PM</option>
                                            <option>5:15 PM</option>
                                            <option>5:30 PM</option>
                                            <option>5:45 PM</option>
                                            <option>6:00 PM</option>
                                            <option>6:15 PM</option>
                                            <option>6:30 PM</option>
                                            <option>6:45 PM</option>
                                            <option>7:00 PM</option>
                                            <option>7:15 PM</option>
                                            <option>7:30 PM</option>
                                            <option>7:45 PM</option>
                                            <option>8:00 PM</option>
                                            <option>8:15 PM</option>
                                            <option>8:30 PM</option>
                                            <option>8:45 PM</option>
                                            <option>9:00 PM</option>
                                            <option>9:15 PM</option>
                                            <option>9:30 PM</option>
                                            <option>9:45 PM</option>
                                            <option>10:00 PM</option>
                                            <option>10:15 PM</option>
                                            <option>10:30 PM</option>
                                            <option>10:45 PM</option>
                                            <option>11:00 PM</option>
                                            <option>11:15 PM</option>
                                            <option>11:30 PM</option>
                                            <option>11:45 PM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="schedule-inner-block" id="all-blockMob" >
                                    <div class="form-group">
                                        <p>See Eateries Open or Closed any time today.</p>
                                    </div>
                                </div>
                                <div class="schedule-inner-block" id="open-blockMob" style="display: none;">
                                    <div class="form-group">
                                        <p>See eateries that are open now.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                         <input type="hidden" name="working_date" id="datecheckMob" value="{{date('d M', strtotime(date('d-M-Y')))}}">
                        <input type="hidden" name="working_hours" id="timecheckMob" value="{{date('g:i A')}}">
                        <input type="hidden" name="time_type" id="time_typeMob">
                        <input type="hidden" name="sort_filter" id="sort_filter2">
                    </div>
                    <div class="filter-item filter-item-type">
                        <select name="type" id="order_typeMob" class="selectize type-select">
                            <option value="pickup">Take Out</option>
                            <option value="orderpickup" >Order Take Out</option>
                            <option value="orderdelivery">Order Delivery</option>
                        </select>
                    </div>
                    <div class="filter-item filter-item-distance">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="distanceMob">10mi</span></button>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                <p class="mb-10">Select Distance</p>
                                <div class="range-slider">
                                    <input id="distanceRangeMob" data-type="single" data-min="3" data-max="25" data-postfix="mi" type="text" name="maxDistance" value="" />
                                    <input type="hidden" name="maxDistance" id="distanceRange_valMob" value="10">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="adv-filter-toggle ion-android-more-vertical" type="button" data-toggle="collapse" data-target="#mobile_filter_advance" aria-expanded="true" aria-controls="mobile_filter_advance"></button>
            </div>
            <div class="adv-filter-mob-wrap">
                <div class="collapse show adv-filter-mob-wrap-out" id="mobile_filter_advance">
                    <div class="adv-filter-mob-wrap-inner">
                        <div class="filter-item">
                             <select name="main_category" id="main_categoryMob" class="selectize">
                                <option value="all" selected>All Eateries</option>
                                 @forelse(Master::getMasterAllCategories() as $key => $categories)
                                    <option value="{{$categories->id}}" >{{ucfirst($categories->name)}}</option>
                                @empty
                                @endif
                            </select>
                        </div>
                        <div class="filter-item">
                           <select class="selectize" name="chain_list" id="chain_listMob">
                                <option value='all'>All</option>
                                    <option value="No" selected="selected">Local</option>
                                    <option value="Yes">Chain</option>
                            </select>
                        </div>
                        <div class="filter-item filter-item-price">
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span id="priceMob_change">$ Any</span></button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <p>Select Price</p>
                                    <div class="pricing-block">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" value="0" name="priceMob" id="inlineRadio01"  data-value="$ Any">
                                            <label class="form-check-label" for="inlineRadio01">$ Any</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" value="1" name="priceMob" id="inlineRadio11"  data-value="$">
                                            <label class="form-check-label" for="inlineRadio11">$</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" value="5" name="priceMob" id="inlineRadio21" data-value="$$">
                                            <label class="form-check-label" for="inlineRadio21">$$</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" value="12" name="priceMob" id="inlineRadio31" data-value="$$$">
                                            <label class="form-check-label" for="inlineRadio31">$$$</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" value="20" name="priceMob" id="inlineRadio41" data-value="$$$$">
                                            <label class="form-check-label" for="inlineRadio41">$$$$</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="filter-item filter-item-rating">
                            <div class="dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-star"></i> <input type="text" id="curRatingMob"  value="1"  readonly></button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                   <p>Select Rating</p>
                                   
                                     <select id="rating_pill_mobile_Header" name="rating" autocomplete="off">
                                        <option value="1" selected>1</option>
                                        <option value="2" >2</option>
                                        <option value="3">3</option>
                                        <option value="4" >4</option>
                                        <option value="5">5</option>
                                    </select>
                                   
                                </div>
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>
        </form>
            <div class="selected-restaurants-wrap" id="restaurants_refresh">
                <div class="alert-wrapper">
                    <a href="{{url('cart/checkout')}}" class="checkout-btn">Checkout <sapn class="checkout-icon"><i class="ik ik-shopping-bag"></i><span class="count" id="cart_count_mob">{{Cart::count()}}</span></sapn></a>
                    <div class="alert alert-success" role="alert" id="sectionHead_time" >Your Cart contains <span id="cart_count">{{Cart::count()}}</span> item(s). <a href="{{url('cart/checkout')}}">Click here to Checkout.</a></div>
                </div>
                <div class="selected-restaurants-filter">
                    <h4>{{count($restaurants)}} local eateries found within 10 miles of the entered address.</h4>
                    <select class="form-control" name="sort_filter" id="sort_filter">
                        <<option>Sort By</option>
                        <option value="distance">Distance</option>
                        <option value="name">Name</option>
                        <option value="pricelh">Price (Low to High)</option>
                        <!--<option value="pricehl">Price (High to Low)</option>-->
                        <option value="rating">Rating (High to Low)</option>
                    </select>
                </div>
                <div class="selected-restaurants-filter-mob">
                    <h4>{{count($restaurants)}} local eateries found within 10 miles of the entered address.</h4>
                    <select class="form-control" name="sort_filter" id="sort_filterMob">
                        <<option>Sort By</option>
                        <option value="distance">Distance</option>
                        <option value="name">Name</option>
                        <option value="pricelh">Price (Low to High)</option>
                        <!--<option value="pricehl">Price (High to Low)</option>-->
                        <option value="rating">Rating (High to Low)</option>
                    </select>
                </div>
                <div class="selected-restaurants-list">
                    @forelse($restaurants as $restaurant)
                    <?php $flag=0;
                    $flag=Restaurant::getOpenStatus($restaurant->id); ?>
                    <div class="listing-item" @if($restaurant->published == 'Published') style="border-color: #2ab051;" @endif>
                        <div class="left-image-block">
                            <div class="img-holder">
                                <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}">
                                    <figure style="background-image: url('{{$restaurant->mainlogo}}')"></figure>
                                </a>
                            </div>
                            <div class="action-holder">
                                <div class="status">
                                    @if($flag == 1)
                          <span class="open"><i class="ion-android-time"></i>Open</span>
                          @else
                          <span class="closed"><i class="ion-android-time"></i>Closed</span>
                          @endif
                                </div>
                                <!-- <a href="#" class="btn btn-theme zing-order-btn">Order Now</a> -->
                                <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}" class="btn view-menu-btn">See Menu</a>
                            </div>
                        </div>
                        <div class="right-content-block">
                            <a href="#" class="add-fav-btn ion-ios-heart"></a>
                           
                               
                                <span class="raty"><i class="fa fa-star"></i>{{$restaurant->rating}}</span>
                                <h3><a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}">{{$restaurant->name}}</a></h3>
                                @if($restaurant->published == 'Published')
                                <span class="zing-partner"><i></i>Partner - Order Online</span>
                                
                                @else
                                <span class="zing-partner" style="padding-left: 0px; background-image: none;">Ordering Unavailable</span>
                                @endif
                                <div class="rating-pricerang-wrap">
                                    <span class="rating-mobile"><i class="fa fa-star"></i> {{$restaurant->rating}}</span>
                                    <span class="price-range"><span class="badge badge-secondary">
                                        @if($restaurant->price_range_min >= 1)
                                        $
                                        @elseif($restaurant->price_range_min > 5)
                                        $$
                                        elseif($restaurant->price_range_min > 12)
                                        $$$
                                        elseif($restaurant->price_range_min > 20)
                                        $$$$
                                        @endif
                                    </span></span>
                                </div>
                                <p class="type">{{$restaurant->type}}</p>
                                <?php $timings = Restaurant::getTodaysTime($restaurant->id); ?>
                                @if(count($timings) > 0)
                                <p class="type">
                                    Today’s Hours  

                                            @if(isset($timings))
                                            @php
                                               $resultstr = [];
                                            @endphp
                                            @foreach($timings as $key => $val)
                                                @if($val->opening != 'off' && $val->opening != 'off')
                                                    @php
                                                       $resultstr[] = date('g:i a',strtotime($val->opening)).' - '.date('g:i a',strtotime($val->closing))
                                                    @endphp
                                                @endif
                                            @endforeach
                                            @php
                                                echo implode(" & ",$resultstr);
                                            @endphp
                                            @else
                                           
                                            @endif
                                </p>
                                @endif
                                <div class="location">{{number_format($restaurant->distance,2)}} mi <i class="ion ion-android-pin"></i> {{$restaurant->address}}</div>
                                <div class="right-metas">
                                    <div class="status">
                                        @if($flag == 1)
                                        <span class="open"><i class="ion-android-time"></i>Open</span>
                                        @else
                                        <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                        @endif
                                    </div>
                                    <div class="meta-infos">
                                       @if($restaurant->delivery =='Yes')
                                                 <div class="info">    <i class="flaticon-fast-delivery"></i>@if($restaurant->delivery_charge == 0)
                                            Free Delivery
                                        @elseif($restaurant->delivery_charge != '')
                                           Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                                        @endif {{!empty($restaurant->delivery_limit) ? '(within '.$restaurant->delivery_limit.' mi)' : ''}}</div>
                                            @endif
                                        </div>
                                </div>
                               
                                <div class="right-metas-mob">
                                    <div class="status">
                                        @if($flag == 1)
                                        <span class="open"><i class="ion-android-time"></i>Open</span>
                                        @else
                                        <span class="closed"><i class="ion-android-time"></i>Closed</span>
                                        @endif
                                    </div>
                                </div>
                               
                                <div class="dish-items">
                                    <?php $res_menu = Restaurant::getMenuName($restaurant->id); ?>
                                        @foreach($res_menu as $res_menu_det)
                                        <div class="dish-item-block-mob">
                                            <div class="item-cat-name">{{$res_menu_det->name}}</div>
                                            <div class="item-name-price-rat">
                                                <p>
                                                    {{@$res_menu_det->categories->name}} 
                                                    @if($restaurant->published == 'Published')
                                                    <a href="#{{$res_menu_det->slug}}" class="add-cart-btn" onclick="addonModel('{{$res_menu_det->id}}')" data-toggle="modal" data-target="#{{$res_menu_det->slug}}"><i class="ik ik-shopping-bag"></i> Add</a>
                                                    @endif
                                                </p>
                                                <div class="item-rate-price">
                                                    <span class="rating"><i class="fa fa-star"></i>{{$res_menu_det->review}}</span>
                                                    <span>${{$res_menu_det->price}}</span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        @endforeach
                                </div>
                           
                        </div>
                    </div>
                    @empty
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade menu-item-modal" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalTitle" aria-hidden="true">
     {!!Form::vertical_open()
    ->id('menu-cart-add')
    ->addClass('modal-dialog modal-dialog-scrollable modal-dialog-centered')
    ->action('/carts/save/')
    ->method('GET')!!}
        <div class="modal-content"></div>
    {!!Form::close()!!}
</div>


<script>

var pformData = [];
var formData = [];

$( document ).ajaxComplete(function() {
        $('.master_count_header').html('{{count(Session::get("selected_masters[]"))}}');
        reset = 1;
        document.getElementById('timecheck').value = '<?php echo date('g:i A'); ?>';
        document.getElementById('datecheck').value = '<?php echo date('d M', strtotime(date('d-M-Y'))); ?>';
        document.getElementById('time_type').value = 'all';

          document.getElementById('timecheckMob').value = '<?php echo date('g:i A'); ?>';
        document.getElementById('datecheckMob').value = '<?php echo date('d M', strtotime(date('d-M-Y'))); ?>';
        document.getElementById('time_typeMob').value = 'all';
        $('#frd_btn').show();
        var TYPES = [{
            name: "Take Out",
            code: "pickup"
        }, {
            name: "Delivery",
            code: "orderdelivery"
        }];
        $('#main_category').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
         $('#main_categoryMob').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
        $('#chain_list').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
        $('#chain_listMob').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
        $('#order_type, #order_typeMob').selectize({
            maxItems: 1,
            labelField: 'name',
            valueField: 'code',
            searchField: ['name', 'code'],
            preload: true,
            persist: false
        });
        $("#rating_pill_Header").barrating({
            theme: 'fontawesome-stars',
            showSelectedRating: false,
            allowEmpty: true,
            deselectable: true,
            onSelect: function(value, text, event) {
              $("#curRating").val(value);
            }
        });
        $("#rating_pill_mobile_Header").barrating({
            theme: 'fontawesome-stars',
            showSelectedRating: false,
            onSelect: function(value, text, event) {
              $("#curRatingMob").val(value);
            }
        });
        $('.modal-header-filter-wrap .dropdown-menu, .modal-header-filter-wrap-mobile .dropdown-menu').click(function (e) {
            e.stopPropagation();
        });

        $('input[name="del_time_type"]').click(function() {
            if($(this).attr('id') == 'schedule_time') {
                $('#scedule-block').show();      
                $('#open-block').hide();
                $('#all-block').hide();      
           } else if($(this).attr('id') == 'all_time') {
                $('#all-block').show();  
                $('#scedule-block').hide();
                $('#open-block').hide();
           }else if($(this).attr('id') == 'asap_time'){
                $('#all-block').hide();  
                $('#scedule-block').hide();
                $('#open-block').show();
           }
       });

         $('input[name="del_time_typeMob"]').click(function() {
           // if($(this).attr('id') == 'schedule_timeMob') {
           //      $('#scedule-blockMob').show();          
           // } else {
           //      $('#scedule-blockMob').hide();  
           // }
           if($(this).attr('id') == 'schedule_timeMob') {
                $('#scedule-blockMob').show();      
                $('#open-blockMob').hide();
                $('#all-blockMob').hide();      
           } else if($(this).attr('id') == 'all_timeMob') {
                $('#all-blockMob').show();  
                $('#scedule-blockMob').hide();
                $('#open-blockMob').hide();
           }else if($(this).attr('id') == 'asap_timeMob'){
                $('#all-blockMob').hide();  
                $('#scedule-blockMob').hide();
                $('#open-blockMob').show();
           }
       });


    var schDateitem = $("#sch_DateWrap .date-item");
    $(schDateitem).click(function() {
        var c_Date = $(this).attr("data-date");
        document.getElementById('datecheck').value = c_Date;
        $("#sch_DateWrap .date-item").removeClass("active");
        $(this).addClass("active");
        $("#del_date_v").html(c_Date);
    });

    var schDateitem = $("#sch_DateWrapMob .date-item");
    $(schDateitem).click(function() {
        var c_Date = $(this).attr("data-date");
        document.getElementById('datecheckMob').value = c_Date;
        $("#sch_DateWrapMob .date-item").removeClass("active");
        $(this).addClass("active");
        $("#del_date_vMob").html(c_Date);
    });

    $('#sch_time').on('change', function() {
        var c_Time = $(this).find(":checked").val();
        document.getElementById('timecheck').value = c_Time;
         document.getElementById('time_type').value = 'scheduled';
        $("#del_time_v").html(c_Time);
         if(reset == 1){
         $(".restaurant_filter").submit();
         }
    });
   
    $('#sch_timeMob').on('change', function() {
        var c_Time = $(this).find(":checked").val();
        document.getElementById('timecheckMob').value = c_Time;
         document.getElementById('time_typeMob').value = 'scheduled';
        $("#del_time_vMob").html(c_Time);
        $(".restaurant_filter_Mob").submit();
    });

    $('#all_time').on('click', function() {
     
         document.getElementById('time_type').value = 'all';
         $("#del_time_v").html('Any Hours');
         $("#del_date_v").html('');
         if(reset == 1){
         $(".restaurant_filter").submit();
         }
    });
    $('#all_timeMob').on('click', function() {
       
         document.getElementById('time_typeMob').value = 'all';
         $("#del_time_vMob").html('Any Hours');
         $("#del_date_vMob").html('');

        $(".restaurant_filter_Mob").submit();
    });

    $('#asap_time').on('click', function() {
       
         document.getElementById('time_type').value = 'asap';
         $("#del_time_v").html('Open Now');
         $("#del_date_v").html('');
          if(reset == 1){
          $(".restaurant_filter").submit();
          }
    });
     $('#asap_timeMob').on('click', function() {
       
         document.getElementById('time_typeMob').value = 'asap';
         $("#del_time_vMob").html('Open Now');
         $("#del_date_vMob").html('');

        $(".restaurant_filter_Mob").submit();
    });

    $('#order_type').change(function(e){
        var type = $("#order_type option:selected").val();
         if(reset == 1){
         $(".restaurant_filter").submit();
         }
    });
     $('#order_typeMob').change(function(e){
        var type = $("#order_typeMob option:selected").val();      
        $(".restaurant_filter_Mob").submit();
    });
     $('#rating_all').on('click', function() {
         if(reset == 1){
         $(".restaurant_filter").submit();
         }
    });
    $('#rating_pill_Header').change(function(e){
         if(reset == 1){
     $(".restaurant_filter").submit();
         }
    })
    $('#rating_pill_mobile_Header').change(function(e){

       $(".restaurant_filter_Mob").submit();
    })
    $("input[name='price']").change(function(){
        var value = $("input[name=price]:checked").attr("data-value");
        $("#price_change").html(value);
        var price = document.querySelector('input[name="price"]:checked').value;
         if(reset == 1){
         $(".restaurant_filter").submit();
         }
    });
    $("input[name='priceMob']").change(function(){
        var value = $("input[name=priceMob]:checked").attr("data-value");
         $("#priceMob_change").html(value);
        var price = document.querySelector('input[name="priceMob"]:checked').value;
        $(".restaurant_filter_Mob").submit();
    });

    $d1 = $("#distanceRange").ionRangeSlider({
        onStart: function (data) {
           
        },
        onChange: function (data) {
            // fired on every range slider update
        },
        onFinish: function (data) {
            var from =data.from;
            $("#distanceRange_val").val(from);
         $("#distance").html(from + "mi");
         $(".restaurant_filter").submit();
            // fired on pointer release
        },
        onUpdate: function (data) {
            // fired on changing slider with Update method
        }
    });

   
    $d2 = $("#distanceRangeMob").ionRangeSlider({
        onStart: function (data) {
           
        },
        onChange: function (data) {
            // fired on every range slider update
        },
        onFinish: function (data) {
            var from =data.from;
            $("#distanceRange_valMob").val(from);
         $("#distance").html(from + "mi");
         $(".restaurant_filter_Mob").submit();
            // fired on pointer release
        },
        onUpdate: function (data) {
            // fired on changing slider with Update method
        }
    });

    // $d1.on("change", function () {
    //     var $inp = $(this);
    //     var from = $inp.prop("value"); // reading input value
    //     $(this).append("<input type='text' name='maxDistance' id='first-slider-value' id='minval' />");
    //     $("#first-slider-value").val(from);
    //      $("#distance").html(from + "mi");
    //      $(".restaurant_filter").submit();
    // });

     

    // $("#distanceRange").each(function() {
    //     var dataMin = $(this).attr('data-min');
    //     var dataMax = $(this).attr('data-max');
    //     var dataUnit = $(this).attr('data-unit');
    //     $(this).append("<input type='text' name='minDistance' class='first-slider-value' id='minval' /><input type='text' name='maxDistance' class='second-slider-value' id='maxval' />");
    //     $(this).slider({
    //         range: "min",
    //         min: dataMin,
    //         max: dataMax,
    //         value: [dataMin],
    //         slide: function(event, ui) {
    //             event = event;
    //             $(this).children(".second-slider-value").val(ui.value + "mi");
    //         }
    //     });
    //     $(".second-slider-value").val($( "#distanceRange" ).slider( "value" ) + "mi");
    // });

    // $("#distanceRangeMob").each(function() {
    //     var dataMin = $(this).attr('data-min');
    //     var dataMax = $(this).attr('data-max');
    //     var dataUnit = $(this).attr('data-unit');
    //     $(this).append("<input type='text' name='minDistance' class='first-slider-value' id='minval' /><input type='text' name='maxDistance' class='second-slider-value' id='maxval' />");
    //     $(this).slider({
    //         range: "min",
    //         min: dataMin,
    //         max: dataMax,
    //         values: [dataMin],
    //         slide: function(event, ui) {
    //             event = event;
    //             $(this).children(".second-slider-value").val(ui.value + "mi");
    //         }
    //     });
    //    $(".second-slider-value").val($( "#distanceRange" ).slider( "value" ) + "mi");
    // });

    // $("#distanceRange").on("slidestop", function(event, ui) { alert('hi');
    //      $(".restaurant_filter").submit();
    // });

    // $("#distanceRangeMob").on("slidestop", function(event, ui) {
    //      $(".restaurant_filter_Mob").submit();
    // });
    $('#main_category').change(function(e){
         if(reset == 1){
        $(".restaurant_filter").submit();
         }
    })
    $('#main_categoryMob').change(function(e){
   
       $(".restaurant_filter_Mob").submit();
    })
    $('#chain_list').change(function(e){
         if(reset == 1){
        $(".restaurant_filter").submit();
         }
    })
   
    $('#chain_listMob').change(function(e){
   
        $(".restaurant_filter_Mob").submit();
    })
    $(".restaurant_filter").submit(function(e) {
    console.log('com');
            e.preventDefault();
            formData = $(this).serializeArray();
            if (JSON.stringify(formData) == JSON.stringify(pformData)) {
                return;
            }
            pformData = formData;

            $(".masters-modal .modal-content").LoadingOverlay("show", {
                image : "{{theme_asset('img/loader.svg')}}",
                text : "",
                imageAnimation : "",
            });
   
            $.ajax({
                    url: "{{ URL::to('master/selected_masters') }}/",
                    data: formData,
                    dataType:'JSON',
                    success: function(response){
   
                            $(".masters-modal .modal-content").LoadingOverlay("hide");
                            $('#restaurants_refresh').html(response.new_menus);
                    },
                    error: function(msg) {
                            $(".masters-modal .modal-content").LoadingOverlay("hide");
   
                    }
                });
       });
   
    $(".restaurant_filter_Mob").submit(function(e) {
    console.log('mob');
   
            e.preventDefault();
            
            formData = $(this).serializeArray();
            if (JSON.stringify(formData) == JSON.stringify(pformData)) {
                return;
            }
            pformData = formData;
            
            $(".masters-modal .modal-content").LoadingOverlay("show", {
                image : "{{theme_asset('img/loader.svg')}}",
                text : "",
                imageAnimation : "",
            });
            formData = $(this).serializeArray();
   
            $.ajax({
                    url: "{{ URL::to('master/selected_masters') }}/",
                    data: formData,
                    dataType:'JSON',
                    success: function(response){
                            $(".masters-modal .modal-content").LoadingOverlay("hide");
                            $('#restaurants_refresh').html(response.new_menus);
                    },
                    error: function(msg) {
                            $(".masters-modal .modal-content").LoadingOverlay("hide");
   
                    }
                });
       });
   
    $('#sort_filter').change(function(e){
        e.preventDefault();
        var val = $("#sort_filter option:selected").val();
        document.getElementById('sort_filter1').value = val;
   
        $(".restaurant_filter").submit();
       
    })
   
    $('#sort_filterMob').change(function(e){
            e.preventDefault();
   
        var val = $("#sort_filterMob option:selected").val();
        document.getElementById('sort_filter2').value = val;
   
        $(".restaurant_filter_Mob").submit();
       
    })
   
    $('#reset_all').click(function(e){
            e.preventDefault();
    reset = 0;
         $("#all_time").prop("checked", true);
         $(".schedule-inner-block").hide();
         $("#all-block").show();
         $("#del_date_v").html('Any Hours');
         $("#del_time_v").html('');
         $("#del_time_vMob").html('Any Hours');
   
         $('#order_type').val('pickup');
         $('#order_typeMob').val('pickup');
   
         var $select = $('#order_type').selectize();
            var control2 = $select[0].selectize;
            control2.clear();
            control2.setValue('pickup', false);
   
         $('#rating_pill_Header').val(1);
          $("#curRating").val(1);
   
         $('#inlineRadio0').attr('checked', 'checked');
          $("#price_change").html('All');
   
            var $select = $('#main_category').selectize();
            var control = $select[0].selectize;
            control.clear();
            control.setValue('all', false);
   
            var $select = $('#chain_list').selectize();
            var control1 = $select[0].selectize;
            control1.clear();
            control1.setValue('No', false);
         reset = 1;
         $(".restaurant_filter").submit();
     });
});
function addonModel(slug)
    { 
        $.ajax({
            type: 'GET',
            url : "{{url('restaurant/menu')}}"+'/'+slug,
            success: function(data) { 
                if(data.status == 'true'){  
                    var check_url = '{{url("carts/check")}}'+'/'+slug;
                   $.ajax({ 
                    type: 'GET',
                        url :  check_url,
                        success: function(data1) { 
                             if (data1.restaurant_error) {
                                swal({
                                  title: "Are you sure?",
                                  text: "Cart items from your previous restaurant will be removed.",
                                  icon: "warning",
                                  buttons: true,
                                  dangerMode: true,
                                })
                                .then((willDelete) => {
                                  if (willDelete) {
                                    // $url1 = "{{trans_url('carts/save')}}"+"/"+slug;
                                    // $.ajax({
                                    //     type: 'GET',
                                    //     url :  $url1,
                                    //     success: function(data) {
                                    //          toastr.success('Item added to cart successfully', 'sucess');
                                    //     }
                                    // });
                                $('.menu-item-modal .modal-content').html(data.view); 
                                $('#productModal').modal({show:true});
                                  } 
                                 
                                });
                            }else{
                                $('.menu-item-modal .modal-content').html(data.view); 
                                $('#productModal').modal({show:true}); 
                            }
                        }, 
                        error: function(data) {

                        }
                    });
                
                }else{ 
                    var check_url = '{{url("carts/check")}}'+'/'+slug;

                    // $url = "{{trans_url('carts/save')}}"+"/"+slug;
                    $.ajax({
                        type: 'GET',
                        url :  check_url,
                        success: function(data) {
                           if (data.restaurant_error) {
                                swal({
                                  title: "Are you sure?",
                                  text: "Cart items from your previous restaurant will be removed.",
                                  icon: "warning",
                                  buttons: true,
                                  dangerMode: true,
                                })
                                .then((willDelete) => {
                                  if (willDelete) {
                                    $url1 = "{{trans_url('carts/save')}}"+"/"+slug;
                                    $.ajax({
                                        type: 'GET',
                                        url :  $url1,
                                        success: function(data) { 
                                            $("#cart_count").html(data.count);
                                            $("#cart_count_mob").html(data.count);
                                             toastr.success('Dish added successfully', 'Success');
                                        }
                                    });
                                  } 
                                  // else {
                                  //   swal("Your imaginary file is safe!");
                                  // }
                                });
                            }else{
                                 url = "{{trans_url('carts/save')}}"+"/"+slug;
                                 $.ajax({
                        type: 'GET',
                        url :  url,
                        success: function(data) {
                            $("#cart_count").html(data.count);
                            $("#cart_count_mob").html(data.count);
                               toastr.success('Dish added successfully', 'Success'); 
                           }
                       });
                            }
                           
                            // if(data.count == '1'){
                              
                            // }
                        }
                    });
                 }
            },  
            error: function(xhr) { 

            }
        });     
    }

     $(document).on('click','.btn_add_cart',function(){   
        var slug = $(this).attr('data-value'); 
        var $f = $('#menu-cart-add');
        $.getJSON({
            type: 'GET',
            url: $f.attr('action'),
            data: $f.serialize(),
            success: function(data) { 
                $('#productModal').modal('hide'); 
                 
                 // if (data.restaurant_error) {
                 //                swal({
                 //                  title: "Are you sure?",
                 //                  text: "Cart items from your previous restaurant will be removed.",
                 //                  icon: "warning",
                 //                  buttons: true,
                 //                  dangerMode: true,
                 //                })
                 //                .then((willDelete) => {
                 //                  if (willDelete) {
                 //                    $url1 = "{{trans_url('carts/save')}}"+"/"+slug;
                 //                    $.ajax({
                 //                        type: 'GET',
                 //                        url :  $url1,
                 //                        success: function(data) {
                 //                             toastr.success('Item added to cart successfully', 'sucess');
                 //                        }
                 //                    });
                 //                  } 
                 //                  // else {
                 //                  //   swal("Your imaginary file is safe!");
                 //                  // }
                 //                });
                 //            }else{
                     $("#cart_count").html(data.count);
                      $("#cart_count_mob").html(data.count);
                               toastr.success('Dish added successfully', 'Success'); 
                            // }
            }, error: function(xhr, status, error) { 
                $("#msg").text(xhr.responseJSON.message);
                $('#error_model').modal({show:true, backdrop: 'static',
                keyboard: false}); 
                // alert(xhr.responseJSON.message, 'Error');
                // $('.error-model .modal-content').html(xhr.responseJSON.message,function(){ 
                //     $('#error_model').modal({show:true}); 
                // });
                return false;
            }
        });
        $(".cart-toggle-btn .icon").addClass("cart-item-exist");
    }); 
</script>
