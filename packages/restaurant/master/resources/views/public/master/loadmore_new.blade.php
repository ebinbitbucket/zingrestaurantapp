  <div id="dish_listing_card" class="cbp dish-listing-wrap">
     @forelse($datas['menus'] as $menu) 
<div class="cbp-item col-lg-3 col-md-4 col-sm-6 col-12 with-spacing">
                            <div class="dish-item">
                                <div class="dish-image">
                                    <a class="dish-link" href="#"></a>
                                    <div class="dish-overlay"></div>
                                    <img src="{{url($menu->defaultImage('image','original'))}}" class="img-fluid" alt="">
                                    <div class="dish-actions">
                                        <a href="#" class="favorite-btn" id="{{$menu->id}}" onclick="addToSession('{{$menu->id}}')"><i class="ion ion-plus-round"></i></a>
                                    </div>
                                    <div class="dish-footer-actions">
                                        <!-- <div class="action-item">
                                            <a href="#" role="button" data-toggle="popover" data-placement="left" data-trigger="focus" data-content="Food Truck" style="color: #3f51b5"><i class="flaticon-fast-delivery"></i></a>
                                        </div> -->
                                        <div class="action-item view-more-btn">
                                            <a href="javascript:void(0);" role="button" data-placement="left" data-toggle="popover" data-trigger="focus" data-content="{{$menu->description}}"><i class="ion ion-more"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="dish-description">
                                    <h3 class="dish-title"><a href="#">{{$menu->name}}</a></h3>
                                    
                                </div>
                            
                            </div>
                        </div>
           
@empty
<div class="cbp-item col-md-12 no-items-found">
    <img src="{{theme_asset('img/no-dish-items.svg')}}" alt="No items Found">
    <h4>No dishes served in your area match the search keywords used.</h4>
</div>
@endif
</div>
<script type="text/javascript">
    $(document).ready(function(){ 
                 $('.header-search-form-wrap .search-item .dropdown-menu').click(function(e) {
                    e.stopPropagation();
                 });
                 $('input[id="schedule"]').click(function() {
                    $('#scheduleModal').modal('show');
                });
            
                $('#dish_listing_card').cubeportfolio({
                    layoutMode: 'grid',
                    defaultFilter: '*',
                    animationType: 'frontRow',
                    gapHorizontal: 0,
                    gapVertical: 0,
                    gridAdjustment: '',
                    mediaQueries: [{
                        width: 1500,
                        cols: 5
                    }, {
                        width: 1100,
                        cols: 4
                    }, {
                        width: 800,
                        cols: 3
                    }, {
                        width: 480,
                        cols: 2
                    }, {
                        width: 320,
                        cols: 1
                    }],
                    caption: 'overlayBottomAlong',
                    displayType: 'bottomToTop',
                    displayTypeSpeed: 300,

                    // lightbox
                    lightboxDelegate: '.cbp-lightbox',
                    lightboxGallery: true,
                    lightboxTitleSrc: 'data-title'
                });
            })
        $('[data-toggle="popover"]').popover()
    $(".add-fav-btn").on('click', function(){
      $(this).toggleClass("active");
    });
</script>