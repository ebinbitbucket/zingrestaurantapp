 
 <ul class="list-unstyled result-bucket">
    @foreach($masters as $master)
    <li class="result-entry" data-suggestion="Target 1" data-position="1" data-type="type" data-analytics-type="merchant">
        @if($master->main_type == "Dish")
        <a href="#" class="result-link">
            <div class="media" onclick="master_name('{{$master->name}}', '{{$master->main_type}}')">  
                <div class="media-body" >
                    <h4 class="media-heading master_name">{{$master->name}} <span class="badge">{{@$master->main_type}}</span></h4>
                </div>
            </div>
        </a>
        @endif
        @if($master->main_type == "Restaurant")
         <a href="{{trans_url('restaurants/'.$master->slug)}}" class="result-link">
            <div class="media">
                 <div class="media-body">
                   <h4 class="media-heading">{{$master->name}} <span class="badge">{{@$master->main_type}}</span></h4>
                </div>
            </div>
        </a>
        @endif
        @if($master->main_type == "Cuisine")
        <a href="#" class="result-link">
            <div class="media" onclick="master_name('{{$master->name}}', '{{$master->id}}')">  
                <div class="media-body" >
                    <h4 class="media-heading master_name">{{$master->name}} <span class="badge">{{@$master->main_type}}</span></h4>
                </div>
            </div>
        </a>
        @endif
    </li>
    @endforeach
</ul>
<script type="text/javascript">
    

</script>
<style type="text/css">
    .result-bucket li {
    padding: 7px 15px;
}
.result-link {
    color: #4f7593;
}
.result-link .media-body {
    font-size: 13px;
    color: gray;
}
.result-link .media-heading {
    font-size: 15px;
    font-weight: 400;
    color: #4f7593;
}
.result-link:hover,
.result-link:hover .media-heading,
.result-link:hover .media-body {
    text-decoration: none;
    color: #4f7593
}
.result-link .media-object {
    width: 50px;
    padding: 3px;
    border: 1px solid #c1c1c1;
    border-radius: 3px;
}
.result-entry + .result-entry {
    border-top:1px solid #ddd;
}
.top-keyword {
    margin: 3px 0 0;
    font-size: 12px;
    font-family: Arial;
}
.top-keyword a {
    font-size: 12px;
    font-family: Arial;
}
.top-keyword a:hover {
    color: rgba(0, 0, 0, 0.7);
}
</style>