<section class="result-header-wrap">
    <div class="container_wrap">
        <div class="row">
            <div class="col-md-12">
                <div class="result-metas">
                    <div class="meta-item" id="cuisine_label" style="display: none;">
                        <div id="cuisine_names"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="listing-wrap dish-style-card clearfix">
    
        <div class="dish-grid are-images-unloaded clearfix" data-js="image-grid">
            <div class="dish-grid__col-sizer"></div>
            <div class="dish-grid__gutter-sizer"></div>

        </div>
        <div class="page-load-status">
            <div class="loader-ellips infinite-scroll-request">
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
                <span class="loader-ellips__dot"></span>
            </div>
            <p class="infinite-scroll-last">End of Dishes</p>
            <p class="infinite-scroll-error">No more Dishes to load</p>
        </div>
</section>

<div class="modal fade masters-modal bd-example-modal-lg" id="MasterSessionModel" tabindex="-1" role="dialog"
    aria-labelledby="MasterSessionModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

<div class="modal fade login-modal location-modal" id="SessionModel" tabindex="-1" role="dialog"
    aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                            <div class="login-header text-center">
                                <h5><b>Dish removed from your list.</b></h5>
                            </div>

                            <div class="text-center">
                                <button type="button"
                                    style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;"
                                    class="btn btn-theme search-btn" id="favourite_submit"
                                    data-dismiss="modal">Ok</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function addToSession(menu) {
        if ($('#' + menu).hasClass('active') == false) {
            $('#' + menu).removeClass('active');
        } else {
            $('#' + menu).addClass('active');
        }
        var fav_status = $('#' + menu).hasClass('active');
        console.log(fav_status);
        if (fav_status == false) {
            fav_status = 'add';
            $(this).addClass("active");
        } else {
    
            fav_status = 'remove';
            $(this).remove("active");
        }
    
        $.ajax({
            url: "{{ URL::to('master/addtomasters') }}" + '/' + fav_status,
            dataType: 'JSON',
            data: {
                name: menu
            },
            success: function (response) {
                if (fav_status == 'add') {
                    $('#MasterSessionModel').modal({
                        show: true
                    });
                    $(".masters-modal .modal-content").LoadingOverlay("show", {
                        image : "{{theme_asset('img/loader.svg')}}",
                        text : "",
                        imageAnimation : "",
                    });
                    $('.masters-modal .modal-content').load("{{url('master/selected_masters')}}", function(){
                        $(".masters-modal .modal-content").LoadingOverlay("hide");
                    });
                } else {
                    $('#SessionModel').modal({
                        show: true,
                        backdrop: 'static',
                        keyboard: false
                    });
                }
    
            },
            error: function (msg) {
    
                document.location.reload()
            }
        });
    }
    
    function removeSelectedMaster(master_id) {
        $('.masters-modal .modal-content').load("{{url('master/selected_masters?remove_id=')}}" + master_id, function () {
            $('#MasterSessionModel').modal({
                show: true
            });
        });
    }
$(document).ready(function () {
    if ("<?php echo !empty(Session::get('selected_masters[]')); ?>") {
        $('#frd_btn').show();
    } else {
        $('#frd_btn').hide();
    }
       

    var $grid = $('.dish-grid').masonry({
        itemSelector: 'none',
        columnWidth: '.dish-grid__col-sizer',
        gutter: '.dish-grid__gutter-sizer',
        percentPosition: true,
        stagger: 30,
        visibleStyle: {
            transform: 'translateY(0)',
            opacity: 1
        },
        hiddenStyle: {
            transform: 'translateY(100px)',
            opacity: 0
        },
    });

    var msnry = $grid.data('masonry');

    $grid.imagesLoaded(function () {
        $grid.removeClass('are-images-unloaded');
        $grid.masonry('option', {
            itemSelector: '.dish-grid__item'
        });
        var $items = $grid.find('.dish-grid__item');
        $grid.masonry('appended', $items);
    });


    var qs_couisine = '<?php echo $cuisine; ?>';
    if(qs_couisine!=''){
        var cuisine_array = [];
        cuisine_array = qs_couisine.split(',');
        cuisine_array.forEach(function(item){  
         $('#cuisine_names').append('<span class="cousine-meta-selected">'+$('#'+item).attr('data-name')+'<button type="button" class="remove_cousine ik ik-x" data-target="'+$('#'+item).attr('id')+'"></button></span>');
        // $('#cuisine_names').append('&nbsp;<i class="fa fa-times-circle-o"></i>&nbsp;');
    });
        
         if($('#master_search').val()=='' && $('#master_search_mob').val()==''){
            $('#cuisine_label').show();
        }
        else{
            $('#cuisine_label').hide();
        }
    }
    var qs_name = '<?php echo $name; ?>';
    var is_options = {
        path: function () {
            var str = 'name=' + qs_name;
            str += '&page=' + this.pageIndex
            str += '&cuisine=' + qs_couisine;

            data = "{{url('master/search')}}" + '?' + str;
            console.log(data);
            return data;
        },
        append: '.dish-grid__item',
        outlayer: msnry,
        status: '.page-load-status',
        history: false
    };
    $grid.infiniteScroll(is_options);
    $grid.infiniteScroll('loadNextPage');
    function is_reset(){
        $grid.infiniteScroll('destroy');
        $grid.data('infiniteScroll', null);
        $grid.infiniteScroll(is_options);
        // $grid.infiniteScroll('loadNextPage');
        var str = 'name=' + qs_name;
            str += '&page=' + this.pageIndex
            str += '&cuisine=' + qs_couisine;

            data = "{{url('master/search_list')}}" + '?' + str;
        $.ajax({
                url: data,
                success: function(response){
                        console.log("New Order",response);
                        $('#home_div').html(response);
                },
                error: function(msg) { 

                       document.location.reload()
                      }
            });
    }
    

    $(".remove_cousine").on("click", function() {
        var e = $(this).attr("data-target");
        $("input[name='cuisine_types[]']").each(function() {
            $(this).attr("id") == e && $(this).attr("checked",false);
        });
        $("input[name='cuisine_types_mob[]']").each(function() {
            $(this).attr("value") == e && $(this).attr("checked",false);
        });
        qs_couisine = [];
            
        $.each($("input[name='cuisine_types[]']:checked"), function(){            
            qs_couisine.push($(this).val());
            $('#cuisine_names').append('<span class="cousine-meta-selected"><span>'+$(this).attr('data-name')+'<button type="button" class="remove_cousine ik ik-x" data-target="'+$(this).attr('id')+'"></button></span>');
            // $('#cuisine_names').append('&nbsp;<i class="fa fa-times-circle-o"></i>&nbsp;');
        });
        $(this).parent().remove();
        is_reset();
    });
});
</script>
<style type="text/css">
    .main-wrap .favorite-btn.active {
        color: #f00;
    }
.cuisine_label_css {
background-color: #ededed;border-radius: 50px;
padding-top: 6px;
padding-bottom: 6px;
padding-left: 15px;
padding-right: 15px;
    }
    .loader {
        border: 16px solid #f3f3f3;
        border-radius: 50%;
        border-top: 16px solid #3498db;
        width: 120px;
        height: 120px;
        -webkit-animation: spin 2s linear infinite;
        /* Safari */
        animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
        0% {
            -webkit-transform: rotate(0deg);
        }

        100% {
            -webkit-transform: rotate(360deg);
        }
    }

    @keyframes spin {
        0% {
            transform: rotate(0deg);
        }

        100% {
            transform: rotate(360deg);
        }
    }
</style>