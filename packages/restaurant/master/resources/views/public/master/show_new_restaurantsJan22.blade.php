  <div class="alert-wrapper">
      <div class="alert alert-success" role="alert" id="sectionHead_time" >Your Cart contains <span id="cart_count">{{Cart::count()}}</span> item(s). <a href="{{url('cart/checkout')}}">Click here to Checkout.</a></div>
  </div>
 <div class="selected-restaurants-filter">
      <h4>{{count($restaurants)}} Results found within {{@$maxDistance}} miles of the entered address.</h4>
      <select class="form-control" name="sort_filter" id="sort_filter">
          <<option>Sort By</option>
          <option value="distance">Distance</option>
          <option value="name">Name</option>
          <option value="pricelh">Price (Low to High)</option>
          <!--<option value="pricehl">Price (High to Low)</option>-->
          <option value="rating">Rating (High to Low)</option>
      </select>
      </div>
    <div class="selected-restaurants-filter-mob">
      <h4>{{count($restaurants)}} Results found within {{@$maxDistance}} miles of the entered address.</h4>
      <select class="form-control" name="sort_filter" id="sort_filterMob">
          <<option>Sort By</option>
          <option value="distance">Distance</option>
          <option value="name">Name</option>
          <option value="pricelh">Price (Low to High)</option>
          <!--<option value="pricehl">Price (High to Low)</option>-->
          <option value="rating">Rating (High to Low)</option>
      </select>
  </div>
  <div class="selected-restaurants-list">
      @forelse($restaurants as $restaurant) 
      <?php $flag=0;
      $flag=Restaurant::getOpenStatus($restaurant->id); ?> 
      <div class="listing-item" @if($restaurant->published == 'Published') style="border-color: green;" @endif>
          <div class="left-image-block">
              <div class="img-holder">
                 
                  <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}">
                      <figure style="background-image: url('{{$restaurant->mainlogo}}')"></figure>
                  </a>
              </div>
              <div class="action-holder">
                  <div class="status">
                      @if($flag == 1)
                          <span class="open"><i class="ion-android-time"></i>Open</span>
                          @else
                          <span class="closed"><i class="ion-android-time"></i>Closed</span>
                          @endif
                  </div>
                 <!--  <a href="#" class="btn btn-theme zing-order-btn">Order Now</a> -->
                  <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}" class="btn view-menu-btn">See Menu</a>
              </div>
          </div>
          <div class="right-content-block">
              <a href="#" class="add-fav-btn ion-ios-heart"></a>
              <a href="{{trans_url('restaurants/')}}/{{@$restaurant->slug}}">

                   <span class="raty"><i class="fa fa-star"></i>{{$restaurant->rating}}</span>
                  <h3>{{$restaurant->name}}</h3>
                   @if($restaurant->published == 'Published')
                    <span class="zing-partner"><i></i>Partner - Order Online</span>
                    
                    @else
                    <span class="zing-partner" style="padding-left: 0px; background-image: none;">Ordering Unavailable</span>
                    @endif
                    <div class="rating-pricerang-wrap">
                        <span class="rating-mobile"><i class="fa fa-star"></i> {{$restaurant->rating}}</span>
                       <span class="price-range"><span class="badge badge-secondary">
                          @if($restaurant->price_range_min >= 1 )
                          $
                          @elseif($restaurant->price_range_min > 5)
                          $$
                          elseif($restaurant->price_range_min > 12)
                          $$$
                          elseif($restaurant->price_range_min > 20)
                          $$$$
                          @endif
                      </span></span>
                    </div>
                 <!--  <span class="rating-mobile"><i class="fa fa-star"></i> {{$restaurant->rating}}</span> -->
                  <p class="type">{{$restaurant->type}}</p>
                  <?php $timings = Restaurant::getTodaysTime($restaurant->id, $date); ?>
                        @if(count($timings) > 0)          
                      <p class="type">
                          {{(substr(date('l',strtotime($date)),0,3))}}’s Hours  

                                  @if(isset($timings))
                                  @php
                                     $resultstr = [];
                                  @endphp
                                  @foreach($timings as $key => $val) 
                                      @if($val->opening != 'off' && $val->opening != 'off') 
                                          @php
                                             $resultstr[] = date('g:i a',strtotime($val->opening)).' - '.date('g:i a',strtotime($val->closing))
                                          @endphp
                                      @endif
                                  @endforeach
                                  @php 
                                      echo implode(" & ",$resultstr);
                                  @endphp
                                  @else
                                  
                                  @endif
                      </p>
                      @endif
                  <div class="location">{{number_format($restaurant->distance,2)}} mi <i class="ion ion-android-pin"></i> {{$restaurant->address}}</div>
                  <div class="right-metas">
                      <div class="status">
                          @if($flag == 1)
                          <span class="open"><i class="ion-android-time"></i>Open</span>
                          @else
                          <span class="closed"><i class="ion-android-time"></i>Closed</span>
                          @endif
                      </div>
                      <div class="meta-infos"> 
                         @if($restaurant->delivery =='Yes')
                                   <div class="info">    <i class="flaticon-fast-delivery"></i>@if($restaurant->delivery_charge == 0)
                              Free Delivery
                          @elseif($restaurant->delivery_charge != '')
                             Delivery Fee: ${{number_format($restaurant->delivery_charge,2)}}
                          @endif {{!empty($restaurant->delivery_limit) ? '(within '.$restaurant->delivery_limit.' mi)' : ''}}</div>
                              @endif
                          </div>
                  </div>
                  
                          <div class="dish-items">
                                    <?php $res_menu = Restaurant::getMenuName($restaurant->id); ?>
                                        @foreach($res_menu as $res_menu_det)
                                        <div class="dish-item-block-mob">
                                            <div class="item-cat-name">{{$res_menu_det->name}}</div>
                                            <div class="item-name-price-rat">
                                                <p>
                                                    {{@$res_menu_det->categories->name}} 
                                                    @if($restaurant->published == 'Published')
                                                    <a href="#{{$res_menu_det->slug}}" class="add-cart-btn" onclick="addonModel('{{$res_menu_det->id}}')" data-toggle="modal" data-target="#{{$res_menu_det->slug}}"><i class="ik ik-shopping-bag"></i> Add</a>
                                                    @endif
                                                </p>
                                                <div class="item-rate-price">
                                                    <span class="rating"><i class="fa fa-star"></i>{{$res_menu_det->review}}</span>
                                                    <span>${{$res_menu_det->price}}</span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        @endforeach
                                </div>
              </a>
          </div>
      </div>
      @empty
      @endif
  </div>
<script type="text/javascript">

$(document).ready(function(){
        $("[data-toggle='tooltip']").tooltip();


    $('#sort_filter').change(function(e){
                e.preventDefault();
    
        var val = $("#sort_filter option:selected").val();
        document.getElementById('sort_filter1').value = val;
    
        $(".restaurant_filter").submit();
       
    });
    
    $('#sort_filterMob').change(function(e){
                e.preventDefault();
    
        var val = $("#sort_filterMob option:selected").val();
        document.getElementById('sort_filter2').value = val;
    
        $(".restaurant_filter_Mob").submit();
       
    });

});
</script>                         