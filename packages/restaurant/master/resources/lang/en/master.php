<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for master in master package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  master module in master package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Master',
    'names'         => 'Masters',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Masters',
        'sub'   => 'Masters',
        'list'  => 'List of masters',
        'edit'  => 'Edit master',
        'create'    => 'Create new master'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
             'display_status' => ['Yes' => 'Yes','No'=>'No'],
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'name'                       => 'Please enter name',
        'description'                => 'Please enter description',
        'image'                      => 'Please enter image',
        'display_status'             => 'Please enter display on home',
        'cuisine'                    => 'Please enter cuisine',
        'user_id'                    => 'Please enter user id',
        'user_type'                  => 'Please enter user type',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'name'                       => 'Name',
        'description'                => 'Description',
        'image'                      => 'Image',
        'cuisine'                    => 'Cuisine',
        'display_status'             => 'Display status',
        'user_id'                    => 'User id',
        'user_type'                  => 'User type',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'name'                       => ['name' => 'Name', 'data-column' => 1, 'checked'],
        'description'                => ['name' => 'Description', 'data-column' => 2, 'checked'],
        'image'                      => ['name' => 'Image', 'data-column' => 3, 'checked'],
        'cuisine'                    => ['name' => 'Cuisine', 'data-column' => 4, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Masters',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
