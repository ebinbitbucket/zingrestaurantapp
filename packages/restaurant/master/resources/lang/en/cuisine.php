<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for cuisine in master package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  cuisine module in master package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Cuisine',
    'names'         => 'Cuisines',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Cuisines',
        'sub'   => 'Cuisines',
        'list'  => 'List of cuisines',
        'edit'  => 'Edit cuisine',
        'create'    => 'Create new cuisine'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
        'filter_show' => ['on' => 'on','off'=>'off'],
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'name'                       => 'Please enter name',
        'image'                      => 'Please enter image',
        'filter_show'                => 'Please enter filter show',
        'user_id'                    => 'Please enter user id',
        'user_type'                  => 'Please enter user type',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'name'                       => 'Name',
        'image'                      => 'Image',
        'filter_show'                => 'Filter show',
        'user_id'                    => 'User id',
        'user_type'                  => 'User type',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'name'                       => ['name' => 'Name', 'data-column' => 1, 'checked'],
        'image'                      => ['name' => 'Image', 'data-column' => 2, 'checked'],
        'filter_show'                => ['name' => 'Filter show', 'data-column' => 3, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Cuisines',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
