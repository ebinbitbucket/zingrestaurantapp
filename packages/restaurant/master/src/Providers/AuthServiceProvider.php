<?php

namespace Restaurant\Master\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the package.
     *
     * @var array
     */
    protected $policies = [
        // Bind Category policy
        'Restaurant\Master\Models\Category' => \Restaurant\Master\Policies\CategoryPolicy::class,
// Bind Cuisine policy
        'Restaurant\Master\Models\Cuisine' => \Restaurant\Master\Policies\CuisinePolicy::class,
// Bind Master policy
        'Restaurant\Master\Models\Master' => \Restaurant\Master\Policies\MasterPolicy::class,
    ];

    /**
     * Register any package authentication / authorization services.
     *
     * @param \Illuminate\Contracts\Auth\Access\Gate $gate
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
    }
}
