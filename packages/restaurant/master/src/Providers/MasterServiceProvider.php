<?php

namespace Restaurant\Master\Providers;

use Illuminate\Support\ServiceProvider;

class MasterServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'master');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'master');

        // Load migrations
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        // Call pblish redources function
        $this->publishResources();

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();
        $this->registerMaster();
        $this->registerFacade();
        $this->registerBindings();
        //$this->registerCommands();
    }


    /**
     * Register the application bindings.
     *
     * @return void
     */
    protected function registerMaster()
    {
        $this->app->bind('master', function($app) {
            return new Master($app);
        });
    }

    /**
     * Register the vault facade without the user having to add it to the app.php file.
     *
     * @return void
     */
    public function registerFacade() {
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Master', 'Restaurant\Master\Facades\Master');
        });
    }

    /**
     * Register bindings for the provider.
     *
     * @return void
     */
    public function registerBindings() {
        // Bind facade
        $this->app->bind('restaurant.master', function ($app) {
            return $this->app->make('Restaurant\Master\Master');
        });

        // Bind Category to repository
        $this->app->bind(
            'Restaurant\Master\Interfaces\CategoryRepositoryInterface',
            \Restaurant\Master\Repositories\Eloquent\CategoryRepository::class
        );
        // Bind Cuisine to repository
        $this->app->bind(
            'Restaurant\Master\Interfaces\CuisineRepositoryInterface',
            \Restaurant\Master\Repositories\Eloquent\CuisineRepository::class
        );
        // Bind Master to repository
        $this->app->bind(
            'Restaurant\Master\Interfaces\MasterRepositoryInterface',
            \Restaurant\Master\Repositories\Eloquent\MasterRepository::class
        );

        $this->app->register(\Restaurant\Master\Providers\AuthServiceProvider::class);
        
        $this->app->register(\Restaurant\Master\Providers\RouteServiceProvider::class);
            }

    /**
     * Merges user's and master's configs.
     *
     * @return void
     */
    protected function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php', 'restaurant.master'
        );
    }

    /**
     * Register scaffolding command
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\MakeMaster::class,
            ]);
        }
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['restaurant.master'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../config/config.php' => config_path('restaurant/master.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../resources/views' => base_path('resources/views/vendor/master')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../resources/lang' => base_path('resources/lang/vendor/master')], 'lang');

        // Publish public files and assets.
        $this->publishes([__DIR__ . '/public/' => public_path('/')], 'public');
    }
}
