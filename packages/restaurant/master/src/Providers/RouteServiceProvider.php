<?php

namespace Restaurant\Master\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Restaurant\Master\Models\Master;
use Request;
use Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Restaurant\Master\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param   \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot()
    {
        parent::boot();

        if (Request::is('*/master/category/*')) {
            Route::bind('category', function ($category) {
                $categoryrepo = $this->app->make('Restaurant\Master\Interfaces\CategoryRepositoryInterface');
                return $categoryrepo->findorNew($category);
            });
        }

        if (Request::is('*/master/cuisine/*')) {
            Route::bind('cuisine', function ($cuisine) {
                $cuisinerepo = $this->app->make('Restaurant\Master\Interfaces\CuisineRepositoryInterface');
                return $cuisinerepo->findorNew($cuisine);
            });
        }

        if (Request::is('*/master/master/*')) {
            Route::bind('master', function ($master) {
                $masterrepo = $this->app->make('Restaurant\Master\Interfaces\MasterRepositoryInterface');
                return $masterrepo->findorNew($master);
            });
        }

    }

    /**
     * Define the routes for the package.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the package.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        if (request()->segment(1) == 'api' || request()->segment(2) == 'api') {
            return;
        }
        
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace,
            'prefix'     => trans_setlocale(),
        ], function ($router) {
            require (__DIR__ . '/../../routes/web.php');
        });
    }

}
