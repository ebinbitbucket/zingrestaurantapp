<?php

namespace Restaurant\Master\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Master\Interfaces\CuisineRepositoryInterface;

class CuisinePublicController extends BaseController
{
    // use CuisineWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Cuisine\Interfaces\CuisineRepositoryInterface $cuisine
     *
     * @return type
     */
    public function __construct(CuisineRepositoryInterface $cuisine)
    {
        $this->repository = $cuisine;
        parent::__construct();
    }

    /**
     * Show cuisine's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $cuisines = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$master::cuisine.names'))
            ->view('master::cuisine.index')
            ->data(compact('cuisines'))
            ->output();
    }


    /**
     * Show cuisine.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $cuisine = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$cuisine->name . trans('master::cuisine.name'))
            ->view('master::cuisine.show')
            ->data(compact('cuisine'))
            ->output();
    }

}
