<?php

namespace Restaurant\Master\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use DB;
use Illuminate\Http\Request;
use Restaurant\Master\Interfaces\MasterRepositoryInterface;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
// use Restaurant\Restaurant\Models\Restaurant;
use Session;
use Restaurant;
use DateTime;
use Restaurant\Master\Models\Cuisine;
use Restaurant\Master\Interfaces\CuisineRepositoryInterface;
use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
class MasterPublicController extends BaseController
{
    // use MasterWorkflow;

    /**
     * Constructor.
     *
     * @param type \Restaurant\Master\Interfaces\MasterRepositoryInterface $master
     *
     * @return type
     */

    public $restaurant;

    public function __construct(MasterRepositoryInterface $master,
        RestaurantRepositoryInterface $restaurant, MenuRepositoryInterface $menu, CuisineRepositoryInterface $cuisine) {
        $this->repository = $master;
        $this->restaurant = $restaurant;
        $this->menu = $menu;
        $this->cuisine = $cuisine;
        parent::__construct();
    }

    /**
     * Show master's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $masters = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) {
                return $query->orderBy('id', 'DESC');
            })->paginate();

        return $this->response->setMetaTitle(trans('master::master.names'))
            ->view('master::master.index')
            ->data(compact('masters'))
            ->output();
    }

    /**
     * Show master.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $master = $this->repository->scopeQuery(function ($query) use ($slug) {
            return $query->orderBy('id', 'DESC')
                ->where('slug', $slug);
        })->first(['*']);

        if (empty(Session::get('selected_masters[]')) || count(Session::get('selected_masters[]')) < 4) {
            Session::push('selected_masters[]', $master->id);
        } else {
            Session::flash('message', 'Upto 4 menus can select');
        }
        $array = Session::get('selected_masters[]');
        Session::put('selected_masters[]', array_unique($array));
        $masters = $this->repository->scopeQuery(function ($query) use ($slug) {
            return $query->orderBy('id', 'DESC')
                ->whereIn('id', Session::get('selected_masters[]'));
        })->get();

        return $this->response->setMetaTitle($master->name . trans('master::master.name'))
            ->view('master::master.show')
            ->data(compact('masters'))
            ->output();
    }

    // protected function homeLisitng(Request $request)
    // {
    //     if (!empty(Session::get('latitude'))) {
    //         $latitude = Session::get('latitude');
    //         $longitude = Session::get('longitude');
    //         $restaurants = DB::select(
    //             'SELECT id FROM
    //                 (SELECT id, (' . 3959  . ' * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) *
    //                 cos(radians(longitude) - radians(' . Session::get('longitude') . ')) +
    //                 sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude))))
    //                 AS distance
    //                 FROM restaurants) AS distances
    //             WHERE distance < ' . 18.6411 . '
    //             ORDER BY distance
    //             ;
    //         ');
    //     } else {
    //         $restaurants = $this->restaurant->get();
    //     }
    //     $result = [];
    //     foreach ($restaurants as $key => $value) {
    //         $result[$key] = $value->id;
    //     }

    //     $datas = $this->repository->getMasterMenu1($request, $result);
    //     $data = view('master::public.master.loadmore_new', compact('datas'))->render();
    //     return response()->json(['status' => 'Success', 'new_menus' => $data]);

    // }
    protected function setLocation($loc, $lat, $long)
    {
        Session::put('location', $loc);
        Session::put('latitude', $lat);
        Session::put('longitude', $long);
        return response()->json(['status' => 'Success']);

    }



   

    // protected function masterLisitng($name)
    // {
    //     if (!empty(Session::get('latitude'))) {
    //         $latitude = Session::get('latitude');
    //         $longitude = Session::get('longitude');
    //         $restaurants = DB::select(
    //             'SELECT id FROM
    //                 (SELECT id, (' . 6367 . ' * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) *
    //                 cos(radians(longitude) - radians(' . Session::get('longitude') . ')) +
    //                 sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude))))
    //                 AS distance
    //                 FROM restaurants) AS distances
    //             WHERE distance < ' . 18.6411 . '
    //             ORDER BY distance
    //             ;
    //         ');
    //     } else {
    //         $restaurants = Restaurant::get();
    //     }
    //     $result = [];
    //     foreach ($restaurants as $key => $value) {
    //         $result[$key] = $value->id;
    //     }
    //     $request['name'] = $name;
    //     $datas = $this->repository->getMasterMenu($request, $result);

    //     // $data = view('master::public.master.homemasters', compact('datas'))->render();
    //     // return response()->json(['status' => 'Success', 'new_menus' => $data]);
    //     return $this->response->setMetaTitle('Home' . trans('master::master.name'))
    //         ->layout('home')
    //         ->view('master::public.master.homemasters')
    //         ->data(compact('datas', 'name'))
    //         ->output();
    // }

    protected function addToMasters($status, Request $request)
    {
        if (empty(Session::get('selected_masters[]')) || count(Session::get('selected_masters[]')) < 4) {
            Session::push('selected_masters[]', $request->get('name'));
        } else {
            Session::flash('message', 'Upto 4 menus can select');
        }
        $array = Session::get('selected_masters[]');
        if ($status == 'remove') {
            $array = array_unique($array);
            $index = array_keys($array, $request->get('name'));
            unset($array[1]);
        }

        Session::put('selected_masters[]', array_unique($array));
        return response()->json(['status' => 'Success']);
    }

    protected function selectedMasters(Request $request)
    {
        if(!empty($request->all())){
            if(!empty($request->get('remove_id'))){
                $remove_master=$request->get('remove_id');
                $array = Session::get('selected_masters[]');
                $array = array_flip($array);
                unset($array[$remove_master]);
                $array = array_flip($array);
                Session::forget('selected_masters[]');
                Session::put('selected_masters[]', $array);
                $search = [];
            }
            else{
                $search = $request->all();
            }
        }
        else{
            $search = [];
        }
        $restaurants = $this->restaurant->restaurantMasterList($search);
        $masters = Session::get('selected_masters[]');

        $masters = $this->repository->scopeQuery(function ($query) use($masters) {
            return $query->orderBy('id', 'DESC')
                ->whereIn('id', $masters);
        })->get();
        if(!empty($request->all())){
            if(!empty($request->get('remove_id'))){
                return view('master::public.master.selected_masters', compact('masters', 'restaurants'));
            }
            else{
                $date = $search['working_date'];
                $maxDistance = $search['maxDistance'];
                $data = view('master::public.master.show_new_restaurants', compact('restaurants', 'date','maxDistance'))->render();
                return response()->json(['status' => 'Success', 'new_menus' => $data]);
            }
        }
        else{
            return view('master::public.master.selected_masters', compact('masters', 'restaurants'));
            
        }
       
        
        
    }

    protected function selectedlist()
    {
        $masters = $this->repository->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC')
                ->whereIn('id', Session::get('selected_masters[]'));
        })->get();
        
        $str = "";
        // foreach ($masters as $key => $value) {
        //     if($key !=0 ){
        //         $str =$str. ',';
        //     }
        //     $str =$str."\"$value->name\"";
            
            
        // }
        foreach ($masters as $key => $value) {
            if($key !=0 ){
                // $str =$str. ',';
                $str =$str. 'and';
            }
            // $str =$str."\"$value->name\"";
            $str =$str." restaurant_menus.restaurant_id in (SELECT restaurant_menus.restaurant_id FROM `restaurant_menus` join masters on masters.id=restaurant_menus.master_category AND masters.name = \"$value->name\") ";
            
        } 
        $search['working_date']  = date('Y-m-d');
                $search['working_hours'] = date('H:i:s');
                $date                    = new DateTime($search['working_date']);
                $time                    = new DateTime($search['working_hours']);
                $combined                = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));
        Session::put('search_time', $combined->format('d-M-Y h:i A'));
                  $default_loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$_SERVER['REMOTE_ADDR']));
if(empty(Session::get('latitude'))){
                   Session::put('latitude',$default_loc['geoplugin_latitude']) ;
                   Session::put('longitude',$default_loc['geoplugin_longitude'])  ;
            }
        // $restaurants = Restaurant::getRestaurantsByMaster();
        $query = 'SELECT *,(6367 * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) as distance FROM `restaurants` WHERE id IN((SELECT id FROM restaurants WHERE  (6367 * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) < 10)) AND id IN (select distinct restaurant_menus.restaurant_id from restaurant_menus where '.$str.' and restaurant_menus.restaurant_id in (SELECT restaurant_id FROM `restaurant_menus` join masters m1 on m1.id=restaurant_menus.master_category)) and restaurants.published in ("Published","No Sale") ORDER BY(published)'; 
         // $query = 'SELECT *,(6367 * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) as distance FROM `restaurants` WHERE id IN((SELECT id FROM restaurants WHERE  (6367 * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) < 10)) AND id IN (select distinct restaurant_menus.restaurant_id from restaurant_menus where '.$str.' and restaurant_menus.restaurant_id in (SELECT restaurant_id FROM `restaurant_menus` join masters m1 on m1.id=restaurant_menus.master_category AND m1.name = "Vada")) and restaurants.published in ("Published","No Sale") ORDER BY(published)'; 
         // 
        // $query = 'SELECT *,(3959  * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) as distance FROM `restaurants` WHERE id IN((SELECT id FROM restaurants WHERE  (3959  * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . Session::get('longitude') . ')) + sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude)))) < 10)) AND id IN (SELECT restaurant_menus.restaurant_id FROM `restaurant_menus` join restaurant_categories on restaurant_menus.category_id=restaurant_categories.id where restaurant_menus.name IN ('.$str.') and restaurant_categories.deleted_at IS null Group BY restaurant_id HAVING count(restaurant_menus.id) >='.count(Session::get("selected_masters[]")).' and count(distinct restaurant_menus.name) = '.count(Session::get("selected_masters[]")).') and restaurants.published in ("Published","No Sale") and restaurants.chain_list="No"';

        $restaurants = DB::select($query); 

        return $this->response->setMetaTitle('Masters' . trans('master::master.name'))
            ->view('master::master.show')
            ->data(compact('masters','restaurants'))
            ->output();
    }

    protected function search(Request $request)
    {
        $this->checkLatLng();
        $restIds = [];

        if (!empty(Session::get('latitude'))) {
            $restaurants = $this->restaurant
                ->getByLatLng(Session::get('latitude'),
                    Session::get('longitude'));
            $restIds = array_pluck($restaurants, 'id');
        }
        // if (empty($restIds)) {
        //     $restIds[] = 1000000;
        //     return view('master::public.master.no_dishes')->render();
        // }

        $name = $request->get('name');
        $cuisine = $request->get('cuisine');
        if(empty($name) && empty($cuisine)){
            $restIds = Null;
            $datas = $this->repository->getMasterMenu($restIds, $name, $cuisine);
        }else{
            $datas = $this->repository->getMasterMenu($restIds, $name, $cuisine);
        }
        if ($datas->count() == 0) {
            
            return response('End', 404)
                ->header('Content-Type', 'text/plain');

        }


        return view('master::public.master.loadmore', compact('datas'))->render();
    }

    protected function search_list(Request $request)
    {
        $restIds = [];
        if (!empty(Session::get('latitude'))) {
            $restaurants = $this->restaurant
                ->getByLatLng(Session::get('latitude'),
                    Session::get('longitude'));
            $restIds = array_pluck($restaurants, 'id');
        }

        $name = $request->get('name');
        $cuisine = $request->get('cuisine');
        $datas = $this->repository->getMasterMenu($restIds, $name, $cuisine);
        // if ($datas->count() == 0) {
        //     return response('End', 404)
        //         ->header('Content-Type', 'text/plain');

        // }
        return view('master::public.master.homeloadsearch', compact('datas','name','cuisine'))->render();
    }

   protected function searchSuggestions(Request $request)
    {

        $name = $request->get('name');

        $cuisines = $this->cuisine->scopeQuery(function ($query) use ($name) {
            return $query
                //->orderBy('name', 'ASC')
                ->where('filter_show','on')
                ->whereRaw("UPPER(name) LIKE '%" . strtoupper($name) . "%'")
                ->take(1);
        })->get(['*', \DB::raw('"Cuisine" as main_type')]); 

        $masters = $this->repository->scopeQuery(function ($query) use ($name) {
            return $query
                //->orderBy('name', 'ASC')
                ->whereRaw("UPPER(name) LIKE '%" . strtoupper($name) . "%'")
                ->take(5);
        })->get(['*', \DB::raw('"Dish" as main_type')]);
        
        $restaurants = $this->restaurant->scopeQuery(function ($query) use ($name) {
            return $query
                //->orderBy('name', 'ASC')
                 //->whereIn('published', ['Published', 'No Sale'])
                ->whereRaw("UPPER(name) LIKE '" . strtoupper($name) . "%'")
                ->take(5);
        })->get(['*', \DB::raw('"Restaurant" as main_type')]); 

        $c = $masters->merge($cuisines);
        $masters = $c->merge($restaurants)->sortBy('main_type')->take(11); 
        return view('master::public.master.master_suggestions', compact('masters'));
    }

    // protected function loadmenu()
    // {
    //     if (!empty(Session::get('latitude'))) {
    //         $latitude = Session::get('latitude');
    //         $longitude = Session::get('longitude');
    //         $restaurants = DB::select(
    //             'SELECT id FROM
    //                 (SELECT id, (' . 6367 . ' * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) *
    //                 cos(radians(longitude) - radians(' . Session::get('longitude') . ')) +
    //                 sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude))))
    //                 AS distance
    //                 FROM restaurants) AS distances
    //             WHERE distance < ' . 18.6411 . '
    //             ORDER BY distance
    //             ;
    //         ');
    //     } else {
    //         $restaurants = Restaurant::get();
    //     }

    //     $result = [];
    //     foreach ($restaurants as $key => $value) {
    //         $result[$key] = $value->id;
    //     }

    //     $data['menus'] = $this->repository->loadMoreMenu($result);
    //     $data['counts'] = $this->repository->getMasterCount($result);
    //     if ($data['menus']->count() > 0) {
    //         $data['view'] = view('restaurant::public.restaurant.loadmore', compact('data'))->render();
    //     }

    //     $data['lastpage'] = $data['menus']->lastPage();

    //     return view('restaurant::public.restaurant.loadmore', compact('data'));
    //     // return $data;
    // }
    
    private function checkLatLng()
    {
        if (!empty(Session::get('latitude'))) {
            return;
        } 
        Session::put('latitude', 32.7554883);
        Session::put('longitude', -97.3307658);

    }
}
