<?php

namespace Restaurant\Master\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Master\Http\Requests\CuisineRequest;
use Restaurant\Master\Interfaces\CuisineRepositoryInterface;
use Restaurant\Master\Models\Cuisine;

/**
 * Resource controller class for cuisine.
 */
class CuisineResourceController extends BaseController
{

    /**
     * Initialize cuisine resource controller.
     *
     * @param type CuisineRepositoryInterface $cuisine
     *
     * @return null
     */
    public function __construct(CuisineRepositoryInterface $cuisine)
    {
        parent::__construct();
        $this->repository = $cuisine;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Master\Repositories\Criteria\CuisineResourceCriteria::class);
    }

    /**
     * Display a list of cuisine.
     *
     * @return Response
     */
    public function index(CuisineRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Master\Repositories\Presenter\CuisinePresenter::class)
                ->$function();
        }

        $cuisines = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('master::cuisine.names'))
            ->view('master::cuisine.index', true)
            ->data(compact('cuisines', 'view'))
            ->output();
    }

    /**
     * Display cuisine.
     *
     * @param Request $request
     * @param Model   $cuisine
     *
     * @return Response
     */
    public function show(CuisineRequest $request, Cuisine $cuisine)
    {

        if ($cuisine->exists) {
            $view = 'master::cuisine.show';
        } else {
            $view = 'master::cuisine.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('master::cuisine.name'))
            ->data(compact('cuisine'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new cuisine.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(CuisineRequest $request)
    {

        $cuisine = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('master::cuisine.name')) 
            ->view('master::cuisine.create', true) 
            ->data(compact('cuisine'))
            ->output();
    }

    /**
     * Create new cuisine.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(CuisineRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $cuisine                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('master::cuisine.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('master/cuisine/' . $cuisine->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/master/cuisine'))
                ->redirect();
        }

    }

    /**
     * Show cuisine for editing.
     *
     * @param Request $request
     * @param Model   $cuisine
     *
     * @return Response
     */
    public function edit(CuisineRequest $request, Cuisine $cuisine)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('master::cuisine.name'))
            ->view('master::cuisine.edit', true)
            ->data(compact('cuisine'))
            ->output();
    }

    /**
     * Update the cuisine.
     *
     * @param Request $request
     * @param Model   $cuisine
     *
     * @return Response
     */
    public function update(CuisineRequest $request, Cuisine $cuisine)
    {
        try {
            $attributes = $request->all();
            if(empty($attributes['filter_show'])){
                $attributes['filter_show']= 'off';
            }
            $cuisine->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('master::cuisine.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('master/cuisine/' . $cuisine->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('master/cuisine/' . $cuisine->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the cuisine.
     *
     * @param Model   $cuisine
     *
     * @return Response
     */
    public function destroy(CuisineRequest $request, Cuisine $cuisine)
    {
        try {

            $cuisine->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('master::cuisine.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('master/cuisine/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('master/cuisine/' . $cuisine->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple cuisine.
     *
     * @param Model   $cuisine
     *
     * @return Response
     */
    public function delete(CuisineRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('master::cuisine.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('master/cuisine'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/master/cuisine'))
                ->redirect();
        }

    }

    /**
     * Restore deleted cuisines.
     *
     * @param Model   $cuisine
     *
     * @return Response
     */
    public function restore(CuisineRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('master::cuisine.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/master/cuisine'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/master/cuisine/'))
                ->redirect();
        }

    }

}
