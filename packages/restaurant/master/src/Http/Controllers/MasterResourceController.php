<?php

namespace Restaurant\Master\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Restaurant\Master\Http\Requests\MasterRequest;
use Restaurant\Master\Interfaces\MasterRepositoryInterface;
use Restaurant\Master\Models\Master;
use Illuminate\Http\Request;
use DB;
use Restaurant\Restaurant\Models\Restaurant;

/**
 * Resource controller class for master.
 */
class MasterResourceController extends BaseController
{

    /**
     * Initialize master resource controller.
     *
     * @param type MasterRepositoryInterface $master
     *
     * @return null
     */
    public function __construct(MasterRepositoryInterface $master)
    {
        parent::__construct();
        $this->repository = $master;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Restaurant\Master\Repositories\Criteria\MasterResourceCriteria::class);
    }

    /**
     * Display a list of master.
     *
     * @return Response
     */
    public function index(MasterRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Restaurant\Master\Repositories\Presenter\MasterPresenter::class)
                ->$function();
        }

        $masters = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('master::master.names'))
            ->view('master::master.index', true)
            ->data(compact('masters', 'view'))
            ->output();
    }

    /**
     * Display master.
     *
     * @param Request $request
     * @param Model   $master
     *
     * @return Response
     */
    public function show(MasterRequest $request, Master $master)
    {

        if ($master->exists) {
            $view = 'master::master.show';
        } else {
            $view = 'master::master.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('master::master.name'))
            ->data(compact('master'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new master.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(MasterRequest $request)
    {

        $master = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('master::master.name')) 
            ->view('master::master.create', true) 
            ->data(compact('master'))
            ->output();
    }

    /**
     * Create new master.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(MasterRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $master                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('master::master.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('master/master/' . $master->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/master/master'))
                ->redirect();
        }

    }

    /**
     * Show master for editing.
     *
     * @param Request $request
     * @param Model   $master
     *
     * @return Response
     */
    public function edit(MasterRequest $request, Master $master)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('master::master.name'))
            ->view('master::master.edit', true)
            ->data(compact('master'))
            ->output();
    }

    /**
     * Update the master.
     *
     * @param Request $request
     * @param Model   $master
     *
     * @return Response
     */
    public function update(MasterRequest $request, Master $master)
    {
        try {
            $attributes = $request->all();

            $master->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('master::master.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('master/master/' . $master->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('master/master/' . $master->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the master.
     *
     * @param Model   $master
     *
     * @return Response
     */
    public function destroy(MasterRequest $request, Master $master)
    {
        try {

            $master->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('master::master.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('master/master/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('master/master/' . $master->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple master.
     *
     * @param Model   $master
     *
     * @return Response
     */
    public function delete(MasterRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('master::master.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('master/master'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/master/master'))
                ->redirect();
        }

    }

    /**
     * Restore deleted masters.
     *
     * @param Model   $master
     *
     * @return Response
     */
    public function restore(MasterRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('master::master.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/master/master'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/master/master/'))
                ->redirect();
        }

    }
    
    public function downloadCSV(Request $request, $filter = []) {

		empty($filter) ? '' : parse_str($filter, $filter);
		$fields = ['name'];

		if (isset($filter['search'])) {
			$filter = array_only($filter['search'], $fields);
		} else {
			$filter = [];
		}

		$masters = $this->repository->getAjax($filter);

		$fields = ['id' => 'ID', 'name' => 'Name'];

		$header = implode(',', $fields);
		$body = '';
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=Masters.csv');

		foreach ($masters as $key => $master) {
			$row = array_only($masters, array_keys($fields));

			foreach ($fields as $hk => $hv) {

				if ($hk == 'id') {
					$body .= "\"" . @$master['id'] . "\",";
					continue;
				}
				if ($hk == 'name') {
					$body .= "\"" . @$master['name'] . "\",";
					continue;
				}

				$body .= "\"" . @$row[$hk] . "\",";
			}
			$body .= "\n";
		}
		echo $header . "\n";
		echo $body . "\n";
	}
	
  public function updatesearchtext(){
        $masters = Master::where('applied_at', '<', date('Y-m-d H:i:s', strtotime('-3 hour')))
        ->orWhereNull('applied_at')
        ->orderBy(\DB::raw('LENGTH(name)'))
        ->get();

        foreach ($masters as $master) {
            if(!empty($master->tags)){
                $tags = explode(',', rtrim($master->tags)); 
                $str = '(';
                if(end($tags) == ''){
                    array_pop($tags);
                }
                foreach ($tags as $key_tag => $tag) { 
                    $tag_val = explode(' ', trim($tag));
                    $str = $str .' (';
                    foreach ($tag_val as $key => $tag_val_query) {
                        if($tag_val_query != ''){
                            $str = $str . "search_text like \"% ".$tag_val_query." %\"";
                          if($key < count($tag_val)-1)
                                $str = $str . " and ";
                            else
                                $str = $str . " ) "; 
                        }
                         
                    }
                    if($key_tag < count($tags)-1)
                        $str = $str . " or "; 
                    else
                            $str = $str . " ) ";
                }

                
                DB::select('UPDATE restaurant_menus SET master_search=CONCAT(IFNULL(master_search,""),
                (CASE WHEN master_search IS NULL THEN "" ELSE "," END),"'.$master->id.'") WHERE '.$str.' and 
                (master_search not like "%'.$master->id.'%" or master_search is null)');

                Master::where('id',$master->id)->update(['applied_at' => date('Y-m-d H:i:s')]);
                echo 'Updation of Master \''. $master->name .'\' Completed succesfully.';
            echo "<br>";
         
            }
        }
            
        echo 'Updation of all menus completed succesfully.';
    }
    
     public function mappingLinks(){
        return $this->response->setMetaTitle(trans('master::master.names'))
            ->view('master::master.master_mapping', true)
            ->data(compact('orders', 'view'))
            ->output();

    }
       public function updatesearchimage($restaurant = null){
        $masters = Master::where('applied_at', '<', date('Y-m-d H:i:s', strtotime('-2 minutes')))
        ->orWhereNull('applied_at')
        ->orderBy(\DB::raw('LENGTH(name)'))
        ->get();
        
        if(!empty($restaurant)){
            $restaurant = Restaurant::find(hashids_decode($restaurant));
            $add_query = ' AND restaurant_id='.$restaurant->id;
        }
        else{
            $add_query = '';
        }
        DB::select('UPDATE restaurant_menus INNER JOIN restaurants on restaurant_menus.restaurant_id=restaurants.id  SET restaurant_menus.image_published="Yes" WHERE restaurants.published = "Published"');

        foreach ($masters as $master) {
            $tag_val = explode(' ', trim($master->name));
            $str = ' (';
            foreach ($tag_val as $key => $tag_val_query) {
                if($tag_val_query != ''){
                    $str = $str . "CONCAT(' ',restaurant_menus.name,' ') like \"% ".$tag_val_query." %\"";
                    if($key < count($tag_val)-1)
                        $str = $str . " and ";
                    else
                        $str = $str . " ) "; 
                }
                
            }
            DB::select('UPDATE restaurant_menus SET restaurant_menus.master_category="'.$master->id.'" WHERE image_published != "Yes" and '.$str.$add_query);                

            Master::where('id',$master->id)->update(['applied_at' => date('Y-m-d H:i:s')]);
            echo 'Updation of Master \''. $master->name .'\' Completed succesfully.';
            echo "<br>";
         
           
        }
            
        echo 'Updation of all menus completed succesfully.';
    }
}
