<?php

namespace Restaurant\Master\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class CategoryTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Master\Models\Category $category)
    {
        return [
            'id'                => $category->getRouteKey(),
            'key'               => [
                'public'    => $category->getPublicKey(),
                'route'     => $category->getRouteKey(),
            ], 
            'name'              => $category->name,
            'icon'              => $category->icon,
            'user_id'           => $category->user_id,
            'user_type'         => $category->user_type,
            'created_at'        => $category->created_at,
            'updated_at'        => $category->updated_at,
            'deleted_at'        => $category->deleted_at,
            'url'               => [
                'public'    => trans_url('master/'.$category->getPublicKey()),
                'user'      => guard_url('master/category/'.$category->getRouteKey()),
            ], 
            'status'            => trans('app.'.$category->status),
            'created_at'        => format_date($category->created_at),
            'updated_at'        => format_date($category->updated_at),
        ];
    }
}