<?php

namespace Restaurant\Master\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class MasterTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Master\Models\Master $master)
    {
        return [
            'id'                => $master->getRouteKey(),
            'key'               => [
                'public'    => $master->getPublicKey(),
                'route'     => $master->getRouteKey(),
            ], 
            'name'              => $master->name,
            'description'       => $master->description,
            'image'             => $master->image,
            'cuisine'           => $master->cuisine,
            'user_id'           => $master->user_id,
            'user_type'         => $master->user_type,
            'created_at'        => $master->created_at,
            'updated_at'        => $master->updated_at,
            'deleted_at'        => $master->deleted_at,
            'url'               => [
                'public'    => trans_url('master/'.$master->getPublicKey()),
                'user'      => guard_url('master/master/'.$master->getRouteKey()),
            ], 
            'status'            => trans('app.'.$master->status),
            'created_at'        => format_date($master->created_at),
            'updated_at'        => format_date($master->updated_at),
        ];
    }
}