<?php

namespace Restaurant\Master\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class CuisinePresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new CuisineTransformer();
    }
}