<?php

namespace Restaurant\Master\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class CuisineTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Master\Models\Cuisine $cuisine)
    {
        return [
            'id'                => $cuisine->getRouteKey(),
            'key'               => [
                'public'    => $cuisine->getPublicKey(),
                'route'     => $cuisine->getRouteKey(),
            ], 
            'name'              => $cuisine->name,
            'image'             => $cuisine->image,
            'filter_show'       => $cuisine->filter_show,
            'user_id'           => $cuisine->user_id,
            'user_type'         => $cuisine->user_type,
            'created_at'        => $cuisine->created_at,
            'updated_at'        => $cuisine->updated_at,
            'deleted_at'        => $cuisine->deleted_at,
            'url'               => [
                'public'    => trans_url('master/'.$cuisine->getPublicKey()),
                'user'      => guard_url('master/cuisine/'.$cuisine->getRouteKey()),
            ], 
            'status'            => trans('app.'.$cuisine->status),
            'created_at'        => format_date($cuisine->created_at),
            'updated_at'        => format_date($cuisine->updated_at),
        ];
    }
}