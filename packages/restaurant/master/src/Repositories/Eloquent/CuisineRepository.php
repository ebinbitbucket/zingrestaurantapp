<?php

namespace Restaurant\Master\Repositories\Eloquent;

use Restaurant\Master\Interfaces\CuisineRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class CuisineRepository extends BaseRepository implements CuisineRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.master.cuisine.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.master.cuisine.model.model');
    }
    public function getCuisines()
    {
        return $this->model->orderBy('name')->pluck('name','id');
    }

    public function getDetailCuisines()
    {
        return $this->model->orderBy('name')->where('filter_show','on')->get();
    }
}
