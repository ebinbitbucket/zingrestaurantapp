<?php

namespace Restaurant\Master\Repositories\Eloquent;

use Restaurant\Master\Interfaces\CategoryRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('restaurant.master.category.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.master.category.model.model');
    }

    public function getMasterCategories()
    {
        return $this->model->orderBy('name')->pluck('name','id');
    }
     public function getMasterAllCategories()
    {
        return $this->model->orderBy('name')->get();
    }
    
}
