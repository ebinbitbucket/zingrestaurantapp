<?php

namespace Restaurant\Master\Repositories\Eloquent;

use DB;
use Litepie\Repository\Eloquent\BaseRepository;
use Restaurant\Master\Interfaces\MasterRepositoryInterface;

class MasterRepository extends BaseRepository implements MasterRepositoryInterface
{

    public function boot()
    {
        $this->fieldSearchable = config('restaurant.master.master.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.master.master.model.model');
    }

    public function getMasterMenu($restIds, $name, $cuisine)
    {
        if(!empty($restIds) || !empty($name) || !empty($cuisine)){
       $masters = $this->model->select(['masters.id', 'masters.image', 'masters.name', 'masters.description',
                    DB::raw("COUNT(IF(chain_list = 'Yes', 1, NULL)) 'chain'"),
                    DB::raw("COUNT(IF(chain_list = 'No', 1, NULL)) 'local'")]
                )
                // ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name');
                // ->Join('restaurant_menus', 'masters' . '.id', 'restaurant_menus' . '.master_category');
                ->Join('restaurants', function ($join) {
                    $join->on('restaurants.menus_masters', 'LIKE', DB::raw("CONCAT('%', masters.id , '%')"));
                })->groupBy(['masters.id', 'masters.image', 'masters.name', 'masters.description']);
        }else{
            $masters = $this->model->select(['masters.id', 'masters.image', 'masters.name', 'masters.description'])
                
                ->groupBy(['masters.id', 'masters.image', 'masters.name', 'masters.description']);
                $res = $masters->where('display_status', 'Yes')->distinct()->paginate(20);
                return $res;
        }
        if (!empty($name)) {
            $masters->where('masters' . '.name', 'like', '%' . $name . '%');
        }

        if (!empty($restIds) && empty($name)) {
            $masters->whereIn('restaurants' . '.id', $restIds);
        }

        if (!empty($cuisine) && empty($name)) {
            $types = str_replace(",", "|", $cuisine);
            $type_array = explode(',',$cuisine);
            $string = '(';
            foreach ($type_array as $key => $type) {
                $string = $string . "concat(',',cuisine,',') " . 'LIKE ' . "'" . "%," . $type . ",%" . "'";
                if ($key < count($type_array) - 1) {
                    $string = $string . ' OR ';
                }

            }
            $string = $string .')';
            $masters->whereRaw($string);
            // $masters->where('masters' . '.cuisine', 'rlike', $types);

        }
        // if($cuisine != null){
            $res = $masters->distinct()->paginate(15);
        // }
        // else{
        //     $res = $masters->where('display_status', 'Yes')->distinct()->paginate(20);
        // }
        return $res;
    }


    public function getMasterMenu1($request, $restaurants)
    {
        $menus = $this->model->select('masters' . '.*')
            ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name')
            ->whereIn('restaurant_menus' . '.restaurant_id', $restaurants);

        if (!empty($request['name'])) {
            $menus->where('masters' . '.name', 'like', '%' . $request['name'] . '%');

            $data['counts'] = $this->model->select('masters' . '.id', DB::raw("COUNT(distinct restaurant_menus.restaurant_id) as count"))
                ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name')
                ->whereIn('restaurant_menus' . '.restaurant_id', $restaurants)
                ->whereNull('restaurant_menus' . '.deleted_at')
                ->where('masters' . '.name', 'like', '%' . $request['name'] . '%')
                ->groupBy('masters' . '.id', 'masters' . '.created_at')
                ->orderBy('masters' . '.created_at', 'DESC')->pluck('count', 'id')->toArray();
        }
        if (!empty($request['cuisine'])) {
            $types = str_replace(",", "|", $request['cuisine']);
            $string = '';
            foreach ($types as $key => $type) {
                $string = $string . "concat(',',cuisine,',') " . 'LIKE ' . "'" . "%," . $type . ",%" . "'";
                if ($key < count($types) - 1) {
                    $string = $string . ' OR ';
                }

                // $menus->where('masters' .'.cuisine', 'like', '%'.$types.'%');
            }
            $menus->whereRaw($string);
            // $menus->where('masters' .'.cuisine', 'rlike', $types);
            // ->whereIn('masters' .'.cuisine',$request['cuisine'])

            $data['counts'] = $this->model->select('masters' . '.id', DB::raw("COUNT(distinct restaurant_menus.restaurant_id) as count"))
                ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name')
                ->whereIn('restaurant_menus' . '.restaurant_id', $restaurants)
                ->whereNull('restaurant_menus' . '.deleted_at')
                ->whereIn('masters' . '.cuisine', $request['cuisine'])
                ->groupBy('masters' . '.id', 'masters' . '.created_at')
                ->orderBy('masters' . '.created_at', 'DESC')->pluck('count', 'id')->toArray();
        } else {
            $data['counts'] = $this->model->select('masters' . '.id', DB::raw("COUNT(distinct restaurant_menus.restaurant_id) as count"))
                ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name')
                ->whereIn('restaurant_menus' . '.restaurant_id', $restaurants)
                ->whereNull('restaurant_menus' . '.deleted_at')
                ->groupBy('masters' . '.id', 'masters' . '.created_at')
                ->orderBy('masters' . '.created_at', 'DESC')->pluck('count', 'id')->toArray();
        }
        $data['menus'] = $menus->orderBy('created_at', 'DESC')->distinct()->get();
        return $data;
    }
    public function loadMoreMenu($restaurants)
    {
        return $this->model->select('masters' . '.*')
            ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name')
            ->whereIn('restaurant_menus' . '.restaurant_id', $restaurants)
            ->orderBy('masters' . '.created_at', 'desc')->distinct()->paginate(2);
    }

    public function getMasterCount($restaurants)
    {
        return $this->model->select('masters' . '.id', DB::raw("COUNT(distinct restaurant_menus.restaurant_id) as count"))
            ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name')
            ->whereIn('restaurant_menus' . '.restaurant_id', $restaurants)
            ->whereNull('restaurant_menus' . '.deleted_at')
            ->groupBy('masters' . '.id', 'masters' . '.created_at')
            ->orderBy('masters' . '.created_at', 'desc')->pluck('count', 'id')->toArray();
    }
    public function getMasterImage($menu)
    {
        return $this->model->where('name', $menu)->first();
    }
    
   public function getOnlyMasterCount($restaurants,$master)
    {
          // return $this->model->select(DB::raw("COUNT(distinct restaurant_menus.restaurant_id) as count"))
        //     ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name')
        //     ->whereIn('restaurant_menus' . '.restaurant_id', $restaurants)
        //     ->whereNull('restaurant_menus' . '.deleted_at')
        //     ->where('masters'.'.id', $master)
        //     ->groupBy('masters' . '.id', 'masters' . '.created_at')
        //     ->orderBy('masters' . '.created_at', 'desc')->pluck('count');
        //     
        return $this->model->select(DB::raw("COUNT(distinct restaurant_menus.restaurant_id) as count"))
            // ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name')
             ->Join('restaurant_menus', 'masters' . '.id', '=', 'restaurant_menus' . '.master_category')
            ->whereIn('restaurant_menus' . '.restaurant_id', $restaurants)
            ->whereNull('restaurant_menus' . '.deleted_at')
            ->where('restaurant_menus' . '.master_category', $master)
            // ->where('masters'.'.id', $master)
            ->groupBy('masters' . '.id', 'masters' . '.created_at')
            ->orderBy('masters' . '.created_at', 'desc')->pluck('count');
    }
    
    public function getAjax($filter) {
		$master = $this->model;

		return $this->filter($master, $filter);

	}

	public function filter($master, $filter) {

		if (empty($filter)) {
			return $master->get()->toArray();
		}

		return $master->where(function ($query) use ($filter) {

			foreach ($filter as $key => $value) {
				if ($value == '') {
					continue;
				}

				$query->where($key, 'like', '%' . $value . '%');
			}

		})
		//->take(50000)
			->get()
			->toArray();

	}
	
	 public function getAllMasters()
    {
        return $this->model->orderBy('name', 'ASC')->pluck('name','id');
    }


}
