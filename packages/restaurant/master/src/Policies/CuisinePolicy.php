<?php

namespace Restaurant\Master\Policies;

use Litepie\User\Contracts\UserPolicy;
use Restaurant\Master\Models\Cuisine;

class CuisinePolicy
{

    /**
     * Determine if the given user can view the cuisine.
     *
     * @param UserPolicy $user
     * @param Cuisine $cuisine
     *
     * @return bool
     */
    public function view(UserPolicy $user, Cuisine $cuisine)
    {
        if ($user->canDo('master.cuisine.view') && $user->isAdmin()) {
            return true;
        }

        return $cuisine->user_id == user_id() && $cuisine->user_type == user_type();
    }

    /**
     * Determine if the given user can create a cuisine.
     *
     * @param UserPolicy $user
     * @param Cuisine $cuisine
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('master.cuisine.create');
    }

    /**
     * Determine if the given user can update the given cuisine.
     *
     * @param UserPolicy $user
     * @param Cuisine $cuisine
     *
     * @return bool
     */
    public function update(UserPolicy $user, Cuisine $cuisine)
    {
        if ($user->canDo('master.cuisine.edit') && $user->isAdmin()) {
            return true;
        }

        return $cuisine->user_id == user_id() && $cuisine->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given cuisine.
     *
     * @param UserPolicy $user
     * @param Cuisine $cuisine
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Cuisine $cuisine)
    {
        return $cuisine->user_id == user_id() && $cuisine->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given cuisine.
     *
     * @param UserPolicy $user
     * @param Cuisine $cuisine
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Cuisine $cuisine)
    {
        if ($user->canDo('master.cuisine.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given cuisine.
     *
     * @param UserPolicy $user
     * @param Cuisine $cuisine
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Cuisine $cuisine)
    {
        if ($user->canDo('master.cuisine.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
