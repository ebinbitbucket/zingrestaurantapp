<?php

namespace Restaurant\Master;

use User;
use Session;
use DB;
class Master
{
    /**
     * $category object.
     */
    protected $category;
    /**
     * $cuisine object.
     */
    protected $cuisine;
    /**
     * $master object.
     */
    protected $master;
    protected $restaurant;

    /**
     * Constructor.
     */
    public function __construct(\Restaurant\Master\Interfaces\CategoryRepositoryInterface $category,
        \Restaurant\Master\Interfaces\CuisineRepositoryInterface $cuisine,
        \Restaurant\Master\Interfaces\MasterRepositoryInterface $master,
    \Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface $restaurant)
    {
        $this->category = $category;
        $this->cuisine = $cuisine;
        $this->master = $master;
        $this->restaurant = $restaurant;
    }

    /**
     * Returns count of master.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return  0;
    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.category.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->category->pushCriteria(new \Litepie\Restaurant\Repositories\Criteria\CategoryUserCriteria());
        }

        $category = $this->category->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('master::' . $view, compact('category'))->render();
    }
    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget2($view = 'admin.cuisine.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->cuisine->pushCriteria(new \Litepie\Restaurant\Repositories\Criteria\CuisineUserCriteria());
        }

        $cuisine = $this->cuisine->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('master::' . $view, compact('cuisine'))->render();
    }
    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget1($view = 'admin.master.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->master->pushCriteria(new \Litepie\Restaurant\Repositories\Criteria\MasterUserCriteria());
        }

        $master = $this->master->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('master::' . $view, compact('master'))->render();
    }

    public function getCuisines()
    {
        return $this->cuisine->getCuisines();
    }
    public function getMasterMenu($menu)
    {
        return $this->master->getMasterMenu($menu);
    }
     public function getAllMasters()
    {
        return $this->master->getAllMasters();
    }
     public function getMasterCategories()
    {
        return $this->category->getMasterCategories();
    }
    public function getMasterAllCategories()
    {
        return $this->category->getMasterAllCategories();
    }
    
    
    public function getLatestMenus(){
        if(!empty(Session::get('latitude'))){
             $latitude =  Session::get('latitude');
        $longitude = Session::get('longitude');
        $restaurants =    DB::select(
               'SELECT id FROM
                    (SELECT id, (' . 3959   . ' * acos(cos(radians(' . Session::get('latitude') . ')) * cos(radians(latitude)) *
                    cos(radians(longitude) - radians(' . Session::get('longitude') . ')) +
                    sin(radians(' . Session::get('latitude') . ')) * sin(radians(latitude))))
                    AS distance
                    FROM restaurants) AS distances
                WHERE distance < ' . 18.6411 . '
                ORDER BY distance
                ;
            ');
        }
        else{
            $restaurants = $this->restaurant->get();
        }
        
        $result = [];
        foreach ($restaurants as $key => $value) {
                $result[$key] = $value->id;
            }
  
        $data['menus'] = $this->master->loadMoreMenu($result);
        $data['counts'] = $this->master->getMasterCount($result);  
         return view('restaurant::public.restaurant.loadmore', compact('data'))->render();       
    }

     public function getDetailCuisines()
    {
        return $this->cuisine->getDetailCuisines();
    }

    public function getMasterImage($menu)
    {
        $master = $this->master->getMasterImage($menu);
        return $master;
    }
    public function getRestaurantsCount($master, $chain_list = null){  
        $master_det = $this->master->findByField('id',$master);
        $restIds = [];

        if (!empty(Session::get('latitude'))) {
            $restaurants = $this->restaurant
                ->getByLatLng(Session::get('latitude'),
                    Session::get('longitude'), $chain_list);
            $restIds = array_pluck($restaurants, 'id');
        }
        // $master_count = $this->master->getOnlyMasterCount($restIds,$master); 
        $master_count = $this->restaurant->getOnlyRestaurantMasterCount($restIds,$master);
        return !empty($master_count)? $master_count : 0;
    }
}
