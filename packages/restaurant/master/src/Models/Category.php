<?php

namespace Restaurant\Master\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
// use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Trans\Traits\Translatable;
class Category extends Model
{
    use Filer, SoftDeletes, Hashids, Translatable, PresentableTrait;
    // Slugger

    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.master.category.model';


}
