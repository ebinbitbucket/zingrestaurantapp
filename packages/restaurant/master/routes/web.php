<?php

// Resource routes  for category
Route::group(['prefix' => set_route_guard('web').'/master'], function () {
    Route::resource('category', 'CategoryResourceController');
});

// Public  routes for category
Route::get('category/popular/{period?}', 'CategoryPublicController@popular');
Route::get('masters/', 'CategoryPublicController@index');
Route::get('masters/{slug?}', 'CategoryPublicController@show');


// Resource routes  for cuisine
Route::group(['prefix' => set_route_guard('web').'/master'], function () {
    Route::resource('cuisine', 'CuisineResourceController');
});

// Public  routes for cuisine
Route::get('cuisine/popular/{period?}', 'CuisinePublicController@popular');
Route::get('masters/', 'CuisinePublicController@index');
Route::get('masters/{slug?}', 'CuisinePublicController@show');


// Resource routes  for master
Route::group(['prefix' => set_route_guard('web').'/master'], function () {
    Route::get('updatesearchtext', 'MasterResourceController@updatesearchtext');
    Route::get('mapping_links', 'MasterResourceController@mappingLinks');
    Route::get('updatemasterimage/{restaurant?}', 'MasterResourceController@updatesearchimage');
    Route::resource('master', 'MasterResourceController');
    Route::get('download/csv/{filter}', 'MasterResourceController@downloadCSV');
});

// Public  routes for master
Route::get('master/popular/{period?}', 'MasterPublicController@popular');
Route::get('masters/', 'MasterPublicController@index');
Route::get('masters/{slug?}', 'MasterPublicController@show');
Route::get('master/search', 'MasterPublicController@search');
Route::get('master/search_list', 'MasterPublicController@search_list');
Route::get('master/addtomasters/{status?}', 'MasterPublicController@addToMasters');
Route::get('master/selected_masters', 'MasterPublicController@selectedMasters');
Route::get('master/masterslisting/{name?}', 'MasterPublicController@masterLisitng');
Route::get('master/masterslistings/cuisine_search', 'MasterPublicController@cuisine_search');
Route::get('master/restaurants', 'MasterPublicController@selectedList');
Route::get('master/setLocation/{loc?}/{lat?}/{long?}', 'MasterPublicController@setLocation');
Route::get('master/removemaster/{masterid?}', 'MasterPublicController@removeMaster');
Route::get('master/removemasterpopup/{masterid?}', 'MasterPublicController@removemasterpopup');
Route::get('loadmenus/', 'MasterPublicController@loadMenu');
Route::get('master/suggestions', 'MasterPublicController@searchSuggestions');
