<?php

namespace Restaurant\Master;

use DB;
use Illuminate\Database\Seeder;

class CuisineTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('cuisines')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'master.cuisine.view',
                'name'      => 'View Cuisine',
            ],
            [
                'slug'      => 'master.cuisine.create',
                'name'      => 'Create Cuisine',
            ],
            [
                'slug'      => 'master.cuisine.edit',
                'name'      => 'Update Cuisine',
            ],
            [
                'slug'      => 'master.cuisine.delete',
                'name'      => 'Delete Cuisine',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/master/cuisine',
                'name'        => 'Cuisine',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/master/cuisine',
                'name'        => 'Cuisine',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'cuisine',
                'name'        => 'Cuisine',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Master',
                'module'    => 'Cuisine',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'master.cuisine.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
