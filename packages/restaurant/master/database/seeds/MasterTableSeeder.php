<?php

namespace Restaurant\Master;

use DB;
use Illuminate\Database\Seeder;

class MasterTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('masters')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'master.master.view',
                'name'      => 'View Master',
            ],
            [
                'slug'      => 'master.master.create',
                'name'      => 'Create Master',
            ],
            [
                'slug'      => 'master.master.edit',
                'name'      => 'Update Master',
            ],
            [
                'slug'      => 'master.master.delete',
                'name'      => 'Delete Master',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/master/master',
                'name'        => 'Master',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/master/master',
                'name'        => 'Master',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'master',
                'name'        => 'Master',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Master',
                'module'    => 'Master',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'master.master.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
