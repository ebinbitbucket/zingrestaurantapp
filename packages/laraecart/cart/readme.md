Lavalite package that provides cart management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `laraecart/cart`.

    "laraecart/cart": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Laraecart\Cart\Providers\CartServiceProvider::class,

And also add it to alias

    'Cart'  => Laraecart\Cart\Facades\Cart::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Laraecart\\CartTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Laraecart\Cart\Providers\CartServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Laraecart\Cart\Providers\CartServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Laraecart\Cart\Providers\CartServiceProvider" --tag="view"


## Usage


