<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'laraecart',

    /*
     * Package.
     */
    'package'   => 'cart',

    /*
     * Modules.
     */
    'modules'   => ['cart', 
'address', 
'order', 
'details', 
'history'],

    'cart'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Cart::class,
            'table'                 => 'carts',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\CartPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'instance',  'content',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/cart',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Cart',
        ],

    ],

    'address'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Address::class,
            'table'                 => 'client_address',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\AddressPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['code' => 'title'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'client_id',  'title', 'address' , 'latitude', 'longitude', 'code', 'default_address','type', 'deleted_at', 'createdat', 'updated_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/address',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Address',
        ],

    ],

    'order'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Order::class,
            'table'                 => 'orders',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\OrderPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'name',  'email',  'phone',  'address',  'address_id', 'billing_address_id', 'restaurant_id',  'phone',  'street',  'zipcode',  'state_id',  'country_id',  'shipping_name',  'shipping_city',  'shipping_zipcode',  'shipping_email',  'shipping_address',  'shipping_phone',  'track_status',  'shipping',  'delivery_id',  'subtotal',  'tax',  'total','discount_points', 'discount_amount', 'order_status', 'payment_status',  'payment_methods',  'payment_details','loyalty_points', 'tip', 'tip_value', 'tip_value_cus', 'tax_rate','delivery_charge','CCR','CCF','ZR','ZF','ZC','feedback_mail_status','delay_mail_status', 'created_at',  'updated_at',  'deleted_at','apartment_no','delivery_instr','delivery_time','order_type','min_order_difference', 'user_id', 'user_type'],
            'translatables'         => [],
            'upload_folder'         => 'cart/order',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'order_status'=> 'like',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Order',
        ],

    ],

    'details'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Details::class,
            'table'                 => 'order_details',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\DetailsPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'order_id',  'menu_id',  'quantity',  'unit_price',  'price', 'menu_addon','special_instr','addons',  'user_id',  'user_type',  'parameters',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/details',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            'addons' => 'array',
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Details',
        ],

    ],

    'history'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\History::class,
            'table'                 => 'histories',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\HistoryPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'order_id',  'courier_name',  'order_status',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/history',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'History',
        ],

    ],
];
