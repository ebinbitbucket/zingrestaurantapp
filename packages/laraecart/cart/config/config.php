<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'laraecart',

    /*
     * Package.
     */
    'package'   => 'cart',

    /*
     * Modules.
     */
    'modules'   => ['cart', 
'address', 
'order', 
'details', 
'history'],

    'cart'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Cart::class,
            'table'                 => 'carts',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\CartPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'instance',  'content',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/cart',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Cart',
        ],

    ],

    'address'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Address::class,
            'table'                 => 'client_address',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\AddressPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['code' => 'title'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'client_id',  'title', 'address' , 'latitude', 'longitude', 'code', 'default_address','type', 'deleted_at', 'createdat', 'updated_at','apartment_no','notes'],
            'translatables'         => [],
            'upload_folder'         => 'cart/address',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Address',
        ],

    ],

    'order'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Order::class,
            'table'                 => 'orders',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\OrderPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id', 'flag', 'name', 'mail_sent', 'email',  'phone',  'address',  'address_id', 'billing_address_id', 'restaurant_id',  'phone',  'street',  'zipcode',  'state_id',  'country_id',  'shipping_name',  'shipping_city',  'shipping_zipcode',  'shipping_email',  'shipping_address',  'shipping_phone',  'track_status',  'shipping',  'delivery_id',  'subtotal',  'tax',  'total','discount_points', 'discount_amount', 'order_status', 'payment_status',  'payment_methods',  'payment_details','loyalty_points', 'tip', 'tip_value', 'tip_value_cus', 'tax_rate','delivery_charge','CCR','CCF','ZR','ZF','ZC','feedback_mail_status','delay_mail_status', 'created_at',  'updated_at',  'deleted_at','apartment_no','delivery_instr','delivery_time','preparation_time','ready_time','order_type','min_order_difference', 'user_id', 'user_type','json_data', 'guest','non_contact','car_pickup','car_details','car_pickup_date','is_car_pickup','text_notification','call_count'],
            'translatables'         => [],
            'upload_folder'         => 'cart/order',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
           
                'car_details'    => 'array',
              /*   'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'restaurant_id' => '=',
                'id'  => '=',
                'phone'  => 'like',
                'total'  => 'like',
                'payment_status'  => 'like',
                'order_status'=> 'like',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Order',
        ],

    ],

    'details'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Details::class,
            'table'                 => 'order_details',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\DetailsPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => [],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'order_id',  'menu_id',  'quantity',  'unit_price',  'price', 'menu_addon','special_instr','addons','variation',  'user_id',  'user_type',  'parameters',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/details',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            'addons' => 'array',
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'Details',
        ],

    ],
    'refund'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Refund::class,
            'table'                 => 'order_refund',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\RefundPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
           // 'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id', 'resturant', 'order_number', 'status', 'order_amount',  'last_digits',  'reason',  'email',  'created_at',  'updated_at',  'deleted_at',  'user_id',  'user_type'],
            'translatables'         => [],
            'upload_folder'         => 'refund/refund',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'order_number'  => 'like',
                'status',
                'order_amount',
                'email',
                'last_digits'
            ]
        ],

        'controller' => [
            'provider'  => 'Cart',
            'package'   => 'Refund',
            'module'    => 'Refund',
        ],

       
    ],
    'calls'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\Calls::class,
            'table'                 => 'call_log',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\CallsPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            //'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'order_id',  'restaurant_id',  'call_count',  'datetime',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'calls/calls',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'order_id'  => 'like',
                'datetime' => 'like'
            ]
        ],

        'controller' => [
            'provider'  => 'Cart',
            'package'   => 'Calls',
            'module'    => 'Calls',
        ],

    ],
    'history'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\History::class,
            'table'                 => 'histories',
            'presenter'             => \Laraecart\Cart\Repositories\Presenter\HistoryPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'order_id',  'courier_name',  'order_status',  'created_at',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'cart/history',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Laraecart',
            'package'   => 'Cart',
            'module'    => 'History',
        ],

    ],
    'transactionlogs'       => [
        'model' => [
            'model'                 => \Laraecart\Cart\Models\TransactionLogs::class,
            'table'                 => 'transaction_logs',
            'dates'                 => ['date','created_at','updated_at','deleted_at'],
            'fillable'              => ['id',  'user_id',  'date', 'type' , 'avs_code', 'total_amount',  'card',  'json_data','cart_id','ip_address','added_item','restaurant_id','url','source','created_at','updated_at','deleted_at', 'HTTP_REFERER', 'server_variables'],
            'upload_folder'         => 'cart/logs',
            'casts'                 => [
            
                'json_data'    => 'array',
                'server_variables'    => 'array',
            
            ],
        ],
     ],
];
