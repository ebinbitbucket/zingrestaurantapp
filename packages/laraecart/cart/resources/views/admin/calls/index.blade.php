<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('cart::calls.name') !!} <small> {!! trans('app.manage') !!} {!! trans('cart::calls.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('cart::calls.names') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div id='calls-calls-entry'>
    </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                    <li class="{!!(request('status') == '')?'active':'';!!}"><a href="{!!guard_url('cart/calls')!!}">{!! trans('cart::calls.names') !!}</a></li>
                   <li class="pull-right">
                    <span class="actions">
                    <!--   
                    <a  class="btn btn-xs btn-purple"  href="{!!guard_url('cart/calls/reports')!!}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Reports</span></a>
                    @include('cart::admin.calls.partial.actions')
                    -->
                    @include('cart::admin.calls.partial.filter')
                    @include('cart::admin.calls.partial.column')
                    </span> 
                </li>
            </ul>
            <div class="tab-content">
                <table id="calls-calls-list" class="table table-striped data-table">
                    <thead class="list_head">
                        <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset" style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input type="checkbox" id="calls-calls-check-all"></th>
                        <th data-field="order_id">{!! trans('cart::calls.label.order_id')!!}</th>
                    <th data-field="restaurant_id">{!! trans('cart::calls.label.restaurant_id')!!}</th>
                    <th data-field="restaurant_name">Restaurant Name</th>
                    <th data-field="call_count">{!! trans('cart::calls.label.call_count')!!}</th>
                    <th data-field="datetime">{!! trans('cart::calls.label.datetime')!!}</th>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

var oTable;
var oSearch;
$(document).ready(function(){
    oTable = $('#calls-calls-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }], 
        
        "responsive" : true,
        "order": [[1, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('cart/calls') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'order_id'},
            {data :'restaurant_id'},
            {data :'restaurant_name'},
            {data :'call_count'},
            {data :'datetime'},
        ],
        "pageLength": 25
    });

    $('#calls-calls-list tbody').on( 'click', 'tr', function () {
        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#calls-calls-list').DataTable().row( this ).data();
        $('#calls-calls-entry').load('{!!guard_url('cart/calls')!!}' + '/' + d.id);
    });

    $('#calls-calls-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent'); 
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#calls-calls-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }
        
    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#calls-calls-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#calls-calls-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});
</script>