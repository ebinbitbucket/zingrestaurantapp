            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('calls::calls.label.order_id'))
                       -> placeholder(trans('calls::calls.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('calls::calls.label.restaurant_id'))
                       -> placeholder(trans('calls::calls.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('call_count')
                       -> label(trans('calls::calls.label.call_count'))
                       -> placeholder(trans('calls::calls.placeholder.call_count'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='datetime' class='control-label'>{!!trans('calls::calls.label.datetime')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('datetime')
                            -> placeholder(trans('calls::calls.placeholder.datetime'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>
            </div>