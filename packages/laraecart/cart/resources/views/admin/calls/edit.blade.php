    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#calls" data-toggle="tab">{!! trans('calls::calls.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#calls-calls-edit'  data-load-to='#calls-calls-entry' data-datatable='#calls-calls-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#calls-calls-entry' data-href='{{guard_url('calls/calls')}}/{{$calls->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('calls-calls-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('calls/calls/'. $calls->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="calls">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('calls::calls.name') !!} [{!!$calls->name!!}] </div>
                @include('calls::admin.calls.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>