    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('calls::calls.name') !!}</a></li>
            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#calls-calls-entry' data-href='{{guard_url('calls/calls/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($calls->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#calls-calls-entry' data-href='{{ guard_url('calls/calls') }}/{{$calls->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#calls-calls-entry' data-datatable='#calls-calls-list' data-href='{{ guard_url('calls/calls') }}/{{$calls->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('calls-calls-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('calls/calls'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('calls::calls.name') !!}  [{!! $calls->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('calls::admin.calls.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>