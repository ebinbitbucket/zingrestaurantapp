    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('cart::address.name') !!}</a></li>
            <div class="box-tools pull-right">
                               <!--  <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#cart-address-entry' data-href='{{guard_url('cart/address/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button> -->
                @if($address->id )
              <!--   <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#cart-address-entry' data-href='{{ guard_url('cart/address') }}/{{$address->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button> -->
                <!-- <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#cart-address-entry' data-datatable='#cart-address-list' data-href='{{ guard_url('cart/address') }}/{{$address->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button> -->
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('cart-address-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('cart/address'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('cart::address.name') !!}  [{!! $address->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('cart::admin.address.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>