<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('
            refund.name') !!} <small> {!! trans('app.manage') !!} {!! trans('cart::refund.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('cart::refund.names') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div id='refund-refund-entry'>
    </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                    <li class="{!!(request('status') == '')?'active':'';!!}"><a href="{!!guard_url('cart/refund')!!}">{!! trans('cart::refund.names') !!}</a></li>
                    <li class="pull-right">
                    <span class="actions">
                    <!--   
                    <a  class="btn btn-xs btn-purple"  href="{!!guard_url('cart/refund/reports')!!}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Reports</span></a>
                    @include('cart::admin.refund.partial.actions')
                    -->
                    @include('cart::admin.refund.partial.filter')
                    </span> 
                </li>
            </ul>
            <div class="tab-content">
                <table id="refund-refund-list" class="table table-striped data-table">
                    <thead class="list_head">
                        <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset" style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input type="checkbox" id="refund-refund-check-all"></th>
                        <th data-field="order_number">{!! trans('cart::refund.label.order_number')!!}</th>
                    <th data-field="order_amount">{!! trans('cart::refund.label.order_amount')!!}</th>
                    <th data-field="last_digits">{!! trans('cart::refund.label.last_digits')!!}</th>
                    <th data-field="reason">{!! trans('cart::refund.label.reason')!!}</th>
                    <th data-field="resturant">Resturant</th>

                    
                    <th data-field="email">{!! trans('cart::refund.label.email')!!}</th>
                    <th data-field="action">Action</th>
                    <th data-field="status">Status</th>

                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>


<div class="modal fade order-type-modal" id="refund_reason" tabindex="-1" role="dialog" aria-labelledby="refund_reason Label" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">

<div class="modal-body">
    <form id="reason_frm">
        <div id="reason_id">
        </div>  
        <div class="form-group">
          <label for="reason">Enter The Reason:</label>
          <div id="reason">
        </div>
          
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
</div>
<div class="modal-footer">
   
</div>
        </div>
    </div>
</div>

<script type="text/javascript">
function inform_rest(id){
    $.ajax({
        url: "{!! guard_url('cart/refund/') !!}"+'/inform/'+id,
        success: function(response){
            toastr.success('Email sent successfully', 'Success');
            // if(response.order_return){
            //     toastr.success(response.order_return, 'Success');
            // //   alert('ddd');
            // }
            //     document.location.reload()
        }
    });
    }
function issue_refund(id){
    $('#reason_id').html('<input type="hidden" value="'+id+'" class="form-control" id="id" name="id"><input type="hidden" value="issue_refund" class="form-control"  name="type">');
    $('#reason').html('<textarea style="height: 110px;="  class="form-control" required name="reason">As per your request, Order number xxxx for $xx has been cancelled and the refund has been processed. Please note it may take 2 - 3 business days for the refund amount to be reflected in your account.</textarea>');

$('#refund_reason').modal('show');
    // $.ajax({
    //     url: "{!! guard_url('cart/refund/') !!}"+'/issue/'+id,
    //     success: function(response){
    //       //  toastr.success('Email sent successfully', 'Success');
    //         // if(response.order_return){
    //         //     toastr.success(response.order_return, 'Success');
    //         // //   alert('ddd');
    //         // }
    //         //     document.location.reload()
    //     }
    // });
    }
    function complete(id){
        if(confirm("Are you sure confirm")){
            $.ajax({
        url: "{!! guard_url('cart/refund/') !!}"+'/complete/'+id,
        success: function(response){
            document.location.reload()
        }
    });
        }
   
    }
function no_refund(id){
    $('#reason_id').html('<input type="hidden" value="'+id+'" class="form-control" id="id" name="id"><input type="hidden" value="no_refund" class="form-control"  name="type">');
    $('#reason').html('<textarea style="height: 110px;="  class="form-control" placeholder="Enter The Reason" required name="reason"></textarea>');
      $('#refund_reason').modal('show');

    }
var oTable;
var oSearch;
$(document).ready(function(){

    $("#reason_frm").submit(function(e) {
     e.preventDefault();
    var form = $(this);
    var url = "{!! guard_url('cart/refund/reson-save') !!}";
   $.ajax({
       type: "POST",
       url: url,
       data: form.serialize(), // serializes the form's elements.
       success: function(data)
       {
        $('#refund_reason').modal('hide');
        toastr.success('Email sent successfully', 'Success');
       }
     });


   });

    oTable = $('#refund-refund-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }], 
        
        "responsive" : true,
        "order": [[1, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('cart/refund') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'order_number'},
            {data :'order_amount'},
            {data :'last_digits'},
            {data :'reason'},
            {data :'resturant'},
            {data :'email'},
            {data :'action'},
            {data :'status'},

        ],
        "pageLength": 25
    });

    $('#refund-refund-list tbody').on( 'click', 'tr', function () {
        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#refund-refund-list').DataTable().row( this ).data();
        $('#refund-refund-entry').load('{!!guard_url('cart/refund')!!}' + '/' + d.id);
    });

    $('#refund-refund-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent'); 
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#refund-refund-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }
        
    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#refund-refund-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#refund-refund-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

  

});

</script>