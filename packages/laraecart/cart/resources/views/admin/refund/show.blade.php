    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('refund::refund.name') !!}</a></li>
            <div class="box-tools pull-right">
                                @include('refund::admin.refund.partial.workflow')
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#refund-refund-entry' data-href='{{guard_url('refund/refund/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($refund->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#refund-refund-entry' data-href='{{ guard_url('refund/refund') }}/{{$refund->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#refund-refund-entry' data-datatable='#refund-refund-list' data-href='{{ guard_url('refund/refund') }}/{{$refund->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('refund-refund-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('refund/refund'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('refund::refund.name') !!}  [{!! $refund->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('refund::admin.refund.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>