    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#refund" data-toggle="tab">{!! trans('refund::refund.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#refund-refund-edit'  data-load-to='#refund-refund-entry' data-datatable='#refund-refund-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#refund-refund-entry' data-href='{{guard_url('refund/refund')}}/{{$refund->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('refund-refund-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('refund/refund/'. $refund->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="refund">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('refund::refund.name') !!} [{!!$refund->name!!}] </div>
                @include('refund::admin.refund.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>