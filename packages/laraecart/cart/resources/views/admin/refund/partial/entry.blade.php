            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('order_number')
                       -> label(trans('refund::refund.label.order_number'))
                       -> placeholder(trans('refund::refund.placeholder.order_number'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('order_amount')
                       -> label(trans('refund::refund.label.order_amount'))
                       -> placeholder(trans('refund::refund.placeholder.order_amount'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('last_digits')
                       -> label(trans('refund::refund.label.last_digits'))
                       -> placeholder(trans('refund::refund.placeholder.last_digits'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('reason')
                       -> label(trans('refund::refund.label.reason'))
                       -> placeholder(trans('refund::refund.placeholder.reason'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('email')
                       -> label(trans('refund::refund.label.email'))
                       -> placeholder(trans('refund::refund.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::package::package.formcontrols.KEY('PRIMARY')
                       -> label(trans('refund::refund.label.PRIMARY'))
                       -> placeholder(trans('refund::refund.placeholder.PRIMARY'))!!}
                </div>
            </div>