<!doctype html>
<html class="no-js" lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <style>
            body {
                font-family: 'Lato', sans-serif;
                font-weight: 300;
                font-size: 15px;
            }
        </style>
    </head>

    <body style="background-color: #f1eff2;">
        <table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: #fff; height: 100%; width: 600px; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.10);">
            <tbody>
                <tr>
                    <td align="center">
                       <div style="background-color: #40b659; text-align: center; padding: 30px 30px;">
                           <img src="{{url('img/logo-round-white.png')}}" style="height: 80px; display: inline-block;" alt="">
                           <h1 style="color: #fff; margin-top: 10px; margin-bottom: 10px; font-size: 24px;">Greetings from Zing!
                        </h1>
                           <p style="font-size: 14px; color: #fff; margin-bottom: 0px;"> {{@$reason}}</p>
                       </div>
                       
                    </td>
                </tr>
                <tr>
                <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">

                    
                        <p style="margin: 0;">
                            <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>Order Details</b></span>
                            <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">#{!!@$order['id']!!}</span>
                             <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;"><b>{{@$order->order_type}} Order</b></span>  
                            <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">Name : {{@$user->name}}</span>
                             @if(@$order['order_type'] == 'Delivery')
                             @if(!empty($order->address_id))
                            <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">Address : {{@$order->delivery_address->full_address}}</span>
                            @endif
                            @endif
                            @if(!empty(@$user->mobile))
                            <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">Ph : {{substr(@$user->mobile, 0,3)}}-{{substr(@$user->mobile, 3,3)}}-{{substr(@$user->mobile, 6,4)}}</span>
                            @endif
                           
                            <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px; text-transform: capitalize;">{!!@$order->restaurant->name!!}</span>
                            <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">Card : {{json_decode(@$order->json_data)->card}}</span>
                                                       
                        </p>
                    </td>
                </tr>
               
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
    
                        
                            <p style="margin: 0;">
                                <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>Refund Details</b></span>
                            
                                <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">Refund Amount : {{@$refund->order_amount}}</span>
                                
                                <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">Refund Reason : {{@$refund->reason}}</span>
                                
                               
                               
                                                             
                            </p>
                            <p style="font-size: 14px; color: #666666; margin-bottom: 0px;">Thank You for using Zing! We look forward to serving you again as our valued customer. Feel free to get in touch if you have any questions or concerns
                            </p>
                        </td>
                    </tr>
              
              
                <tr>
              
                <tr>
                    <td align="center" style="padding: 20px;">
                        <img src="{{url('img/icon-2.png')}}" width="60px" height="60px">
                        <h1 style="font-size: 18px; color:#666666; margin-top: 10px; margin-bottom: 10px;">Need Order Help?</h1>
                        <p style="font-size: 16px; color: #666666; margin-bottom: 10px; margin-top: 0px;">Get Order Help at support@zingmyorder.com</p>
                        <p style="font-size: 16px; color: #666666; margin-bottom: 10px; margin-top: 0px;">Ph: {{substr(9705285689, 0,3)}}-{{substr(9705285689, 3,3)}}-{{substr(9705285689, 6,4)}}</p>
                        <!--<a href="#" style="background-color: #40b659; color: #fff; padding: 10px 20px; font-size: 14px; text-decoration: none; border-radius: 30px; display: inline-block;">Get Order Help</a>-->
                    </td>
                </tr>
                <tr>
                    <td align="center" width="310" style="padding: 20px; background-color: #eff1f2;">
                        <span style="font-size: 14px;">
                            <span style="color: #999999; margin-bottom: 5px; display: block;">© 2019 ZingMyOrder LLC | 1039 I-35E Suite 304, Carrollton, TX 75006</span>
                            <a href="{{url('restaurant/login')}}" target="_new" style="text-decoration: none; color: #999999; display: inline-block;">Be a Eatery Partner</a><span style="color: #999999;">&nbsp; | &nbsp;</span>
                            <a href="{{url('privacy.html')}}" target="_new" style="text-decoration: none; color: #999999; display: inline-block;">Privacy Policy</a><span style="color: #999999;">&nbsp; | &nbsp;
                            <a href="{{url('terms-and-conditions.html')}}" target="_new" style="text-decoration: none; color: #999999; display: inline-block;">Terms & Conditions</a></span>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
