            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('cart::details.label.order_id'))
                       -> placeholder(trans('cart::details.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('product_id')
                       -> label(trans('cart::details.label.product_id'))
                       -> placeholder(trans('cart::details.placeholder.product_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('size')
                       -> label(trans('cart::details.label.size'))
                       -> placeholder(trans('cart::details.placeholder.size'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('quantity')
                       -> label(trans('cart::details.label.quantity'))
                       -> placeholder(trans('cart::details.placeholder.quantity'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('unit_price')
                       -> label(trans('cart::details.label.unit_price'))
                       -> placeholder(trans('cart::details.placeholder.unit_price'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('price')
                       -> label(trans('cart::details.label.price'))
                       -> placeholder(trans('cart::details.placeholder.price'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('parameters')
                       -> label(trans('cart::details.label.parameters'))
                       -> placeholder(trans('cart::details.placeholder.parameters'))!!}
                </div>
            </div>