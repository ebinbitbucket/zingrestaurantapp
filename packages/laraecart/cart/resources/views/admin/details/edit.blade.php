    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">{!! trans('cart::details.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#cart-details-edit'  data-load-to='#cart-details-entry' data-datatable='#cart-details-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#cart-details-entry' data-href='{{guard_url('cart/details')}}/{{$details->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('cart-details-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('cart/details/'. $details->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="details">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('cart::details.name') !!} [{!!$details->name!!}] </div>
                @include('cart::admin.details.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>