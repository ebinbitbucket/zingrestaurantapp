    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('cart::details.name') !!}</a></li>
            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#cart-details-entry' data-href='{{guard_url('cart/details/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($details->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#cart-details-entry' data-href='{{ guard_url('cart/details') }}/{{$details->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#cart-details-entry' data-datatable='#cart-details-list' data-href='{{ guard_url('cart/details') }}/{{$details->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('cart-details-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('cart/details'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('cart::details.name') !!}  [{!! $details->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('cart::admin.details.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>