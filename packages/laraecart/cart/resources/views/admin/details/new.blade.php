<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">  {!! trans('cart::details.names') !!} [{!! trans('cart::details.text.preview') !!}]</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-primary btn-sm"  data-action='NEW' data-load-to='#cart-details-entry' data-href='{!!guard_url('cart/details/create')!!}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }} </button>
        </div>
    </div>
</div>