            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('cart::history.label.order_id'))
                       -> placeholder(trans('cart::history.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('courier_name')
                       -> label(trans('cart::history.label.courier_name'))
                       -> placeholder(trans('cart::history.placeholder.courier_name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('order_status')
                       -> label(trans('cart::history.label.order_status'))
                       -> placeholder(trans('cart::history.placeholder.order_status'))!!}
                </div>
            </div>