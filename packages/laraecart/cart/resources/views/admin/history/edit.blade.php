    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#history" data-toggle="tab">{!! trans('cart::history.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#cart-history-edit'  data-load-to='#cart-history-entry' data-datatable='#cart-history-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#cart-history-entry' data-href='{{guard_url('cart/history')}}/{{$history->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('cart-history-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('cart/history/'. $history->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="history">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('cart::history.name') !!} [{!!$history->name!!}] </div>
                @include('cart::admin.history.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>