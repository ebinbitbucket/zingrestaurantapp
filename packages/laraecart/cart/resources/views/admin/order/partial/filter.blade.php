<div class="btn-group cart-order">
<button aria-expanded="false" class="btn btn-xs btn-info " type="button">
       <a href="{{guard_url('cart/orders/report')}}" target="_blank"> <i aria-hidden="true" class="fa fa-book">
        </i>
        <span class="hidden-sm hidden-xs"> Reports</span>
        </a>
    </button>
    <button class="btn btn-xs btn-danger btn-search" type="button">
        <i aria-hidden="true" class="fa fa-search">
        </i>
        <span class="hidden-sm hidden-xs"> Search</span>
    </button>
    <button aria-expanded="false" class="btn btn-xs btn-danger dropdown-toggle" data-toggle="dropdown" type="button">
        <span class="caret">
        </span>
        <span class="sr-only">
            Toggle Dropdown
        </span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a class="btn-search" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-filter">
                </i>
                Show filters
            </a>
        </li>
        <li>
            <a class="btn-reset-filter" style="cursor:pointer;">
                <i class="fa fa-fw fa-ban text-danger">
                </i>
                Clear filters
            </a>
        </li>
        <li class="divider">
        </li>
        <li>
            <a class="btn-save" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-floppy-o">
                </i>
                Save search
            </a>
        </li>
        <li>
            <a class="btn-open" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-folder-open-o">
                </i>
                Saved searches
            </a>
        </li>
    </ul>
</div>

<div class="modal fade" id="modal-search">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #dd4b39; color: #fff;">
              <button type="button" class="close" data-dismiss="modal" aaria-hidden="true">&times;</button>
              <h4 class="modal-title">Search</h4>
            </div>
              {!!Form::horizontal_open()
              ->id('form-search')
              ->method('POST')
              ->action(guard_url('settings/settings'))!!}
                <div class="modal-body has-form clearfix">
                    <div class="modal-form">
<div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[id]" class="col-sm-2 control-label">
                                        {!! trans('cart::order.label.id')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[id]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[name]" class="col-sm-2 control-label">
                                        {!! trans('Customer Name')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[name]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-6">-->
                            <!--    <div class="form-group">-->
                                     
                            <!--        <label for="search[email]" class="col-sm-2 control-label">-->
                            <!--            {!! trans('cart::order.label.email')!!}-->
                            <!--        </label>-->
                            <!--        <div class="col-sm-10">-->
                            <!--            {!! Form::text('search[email]')->raw()!!}-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[phone]" class="col-sm-2 control-label">
                                        {!! trans('cart::order.label.phone')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[phone]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-6">-->
                            <!--    <div class="form-group">-->
                                     
                            <!--        <label for="search[street]" class="col-sm-2 control-label">-->
                            <!--            {!! trans('cart::order.label.street')!!}-->
                            <!--        </label>-->
                            <!--        <div class="col-sm-10">-->
                            <!--            {!! Form::text('search[street]')->raw()!!}-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="col-md-6">-->
                            <!--    <div class="form-group">-->
                                     
                            <!--        <label for="search[shipping_name]" class="col-sm-2 control-label">-->
                            <!--            {!! trans('cart::order.label.shipping_name')!!}-->
                            <!--        </label>-->
                            <!--        <div class="col-sm-10">-->
                            <!--            {!! Form::text('search[shipping_name]')->raw()!!}-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="col-md-6">-->
                            <!--    <div class="form-group">-->
                                     
                            <!--        <label for="search[shipping_city]" class="col-sm-2 control-label">-->
                            <!--            {!! trans('cart::order.label.shipping_city')!!}-->
                            <!--        </label>-->
                            <!--        <div class="col-sm-10">-->
                            <!--            {!! Form::text('search[shipping_city]')->raw()!!}-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="col-md-6">-->
                            <!--    <div class="form-group">-->
                                     
                            <!--        <label for="search[shipping_email]" class="col-sm-2 control-label">-->
                            <!--            {!! trans('cart::order.label.shipping_email')!!}-->
                            <!--        </label>-->
                            <!--        <div class="col-sm-10">-->
                            <!--            {!! Form::text('search[shipping_email]')->raw()!!}-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="col-md-6">-->
                            <!--    <div class="form-group">-->
                                     
                            <!--        <label for="search[shipping_phone]" class="col-sm-2 control-label">-->
                            <!--            {!! trans('cart::order.label.shipping_phone')!!}-->
                            <!--        </label>-->
                            <!--        <div class="col-sm-10">-->
                            <!--            {!! Form::text('search[shipping_phone]')->raw()!!}-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[order_status]" class="col-sm-2 control-label">
                                        {!! trans('Order Status')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[order_status]')
                                         ->label('Order Status')
                                        ->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[total]" class="col-sm-2 control-label">
                                        {!! trans('cart::order.label.total')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[total]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[restaurant_id]" class="col-sm-2 control-label">
                                        {!! trans('Restaurant')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[restaurant_id]')
                                        ->id('restaurant_id')
                                        ->label('Restaurant name')
                                        ->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-6">-->
                            <!--    <div class="form-group">-->
                                     
                            <!--        <label for="search[payment_methods]" class="col-sm-2 control-label">-->
                            <!--            {!! trans('cart::order.label.payment_methods')!!}-->
                            <!--        </label>-->
                            <!--        <div class="col-sm-10">-->
                            <!--            {!! Form::text('search[payment_methods]')->raw()!!}-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            <!--<div class="col-md-6">-->
                            <!--    <div class="form-group">-->
                                     
                            <!--        <label for="search[created_at]" class="col-sm-2 control-label">-->
                            <!--            {!! trans('cart::order.label.created_at')!!}-->
                            <!--        </label>-->
                            <!--        <div class="col-sm-10">-->
                            <!--            {!! Form::text('search[created_at]')->raw()!!}-->
                            <!--        </div>-->
                            <!--    </div>-->
                            <!--</div>-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 col-lg-12">
                        <button aria-label="Close" class="btn pull-right btn-danger" data-dismiss="modal" type="button">
                            <i class="fa fa-times-circle">
                            </i>
                            Close
                        </button>
                        <button class="btn btn-success pull-right " id="btn-apply-search" name="new" style="margin-right:1%" type="button">
                            <i class="fa fa-check-circle">
                            </i>
                            Search
                        </button>
                    </div>
                </div>
              {!!Form::close()!!}
        </div>
    </div>
</div>


<div class="modal fade" id="modal-open">
  <div class="modal-dialog">
    <div class="modal-content" style="max-width:400px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Saved</h4>
      </div>
      <div class="modal-body" style="height:210px; overflow-y: auto;">
        
        <div id="saved-list">
          
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger"  name="Closerep" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js"></script>
<script>
    /**
 * Plugin: "preserve_search" (selectize.js)
 * Based on: "preserve_on_blur" of Eric M. Klingensmith
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
Selectize.define('preserve_search', function (options) {
    var self = this;

    options.text = options.text || function (option) {
        return option[this.settings.labelField];
    };

    this.onBlur = (function (e) {
        var original = self.onBlur;

        return function (e) {
            // Capture the current input value
            var $input = this.$control_input;
            var inputValue = $input.val();

            // Do the default actions
            original.apply(this, [e]);

            // Set the value back                    
            this.setTextboxValue(inputValue);
        };
    })();

    this.onOptionSelect = (function (e) {
        var original = self.onOptionSelect;

        return function (e) {
            // Capture the current input value
            var $input = this.$control_input;
            var inputValue = $input.val();

            original.apply(this, [e]);
            this.setTextboxValue(inputValue);
            this.refreshOptions();
            if (this.currentResults.items.length <= 0) {
                this.setTextboxValue('');
                this.refreshOptions();
            }
        };
    })();
});
</script>
<script type="text/javascript">
$(document).ready(function(){

    $(".cart-order .btn-open").click(function(){
        toastr.info('This feature will be enabled soon.', 'Coming soon');
        return false;
        $('#open-list').load("{!!guard_url('/settings/setting/search/cart.order.search')!!}");
        $('#modal-open').modal("show");
    });

   $(".cart-order .btn-search").click(function(){
      $('#modal-search').modal("show");
    });
   
    $('.cart-order .btn-save').click(function(e){
        toastr.info('This feature will be enabled soon.', 'Coming soon');
        return false;
        var search = prompt("Please enter name for your search");
        if (search == null) {
            toastr.error('Please enter valid name.', 'Error');
            return false;
        }
        var formData = new FormData();
        formData.append('value', $("#form-search").serialize());
        formData.append('name', search);
        formData.append('key', 'cart.order.search');
        formData.append('package', 'Page');
        formData.append('module', 'Page');

        $.ajax({
            url : "{!!guard_url('/settings/setting')!!}",
            type: "POST",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success:function(data, textStatus, jqXHR)
            {
                toastr.success('Search saved successfully.', 'Success');
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                toastr.error('An error occurred while saving.', 'Error');
            }
        });

        e.preventDefault();
    });

    $('#btn-apply-search').click( function() {
        oSearch = {};
        $('#form-search input,#form-search select').each( function () {
          key = $(this).attr('name');
          val = $(this).val();
          oSearch[key] = val;
        });
        oTable.api().draw();
        $('#cart-order-list .btn-reset-filter').css('display', '');
        $('#modal-search').modal("hide");
        
      });
    
    $(".btn-reset-filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        oSearch = {};
        $('#form-search input,#form-search select').each( function () {
          key = $(this).attr('name');
          val = $(this).val();
          oSearch[key] = val;
        });
        oTable.api().draw();
        $('#cart-order-list .btn-reset-filter').css('display', 'none');

    });

});

 $('#restaurant_id').selectize({
        delimiter: ',',
        persist: false,
        valueField: 'id',
        labelField: 'name',
        searchField: 'name',
        plugins: ['preserve_search'],
        maxItems : 1,
        options: [] ,
        create: false,
         sortField: [
        {
            field: 'name',
            direction: 'asc'
        },
        {
            field: '$score'
        }
    ],
        render: {
        option: function(item, escape) {
          var s="";
          for(var i=0;i<item.length;i++)
          {
              s=s+item.name;
            

          }

            return '<div>'
            + '<b><span>'+item.name+'</span></b> </br>'
            + '</div>';
      }
    },

    load: function(query, callback) {
        // if (query.length<1) return callback();alert('hi');
       
        if(query.length>1){
        $.ajax({
            url: '{{url("restaurant/search")}}',
            type: 'GET',
            dataType: 'json',
            data: {
                q: query,
                // page_limit: 100,

            },
            error: function() {
                callback();
            },
            success: function(res) {
                callback(res);
            }
        });
        }
    },

    });

</script>