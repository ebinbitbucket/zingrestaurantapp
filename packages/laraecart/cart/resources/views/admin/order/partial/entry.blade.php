            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('restaurant.name')
                       -> label('Restaurant ' . trans('cart::order.label.name'))
                       -> placeholder('Restaurant' . trans('cart::order.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('restaurant.email')
                       -> label('Restaurant ' . trans('cart::order.label.email'))
                       -> placeholder('Restaurant' . trans('cart::order.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('restaurant.phone')
                       -> label('Restaurant ' . trans('cart::order.label.phone'))
                       -> placeholder('Restaurant' . trans('cart::order.placeholder.phone'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('cart::order.label.name'))
                       -> placeholder(trans('cart::order.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('user.email')
                       -> label(trans('cart::order.label.email'))
                       -> placeholder(trans('cart::order.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('address')
                       -> label(trans('cart::order.label.address'))
                       -> placeholder(trans('cart::order.placeholder.address'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('phone')
                       -> label(trans('cart::order.label.phone'))
                       -> placeholder(trans('cart::order.placeholder.phone'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('id')
                       -> value($order->id)
                       -> label('ID')!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('order_id')
                       -> value(@json_decode($order->json_data)->orderId)
                       -> label('Order ID')!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('uniqueRef')
                       -> value(@json_decode($order->json_data)->uniqueRef)
                       -> label('Unique Ref')!!}
                </div>
               <!--  <div class='col-md-4 col-sm-6'>
                       {!! Form::text('street')
                       -> label(trans('cart::order.label.street'))
                       -> placeholder(trans('cart::order.placeholder.street'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('zipcode')
                       -> label(trans('cart::order.label.zipcode'))
                       -> placeholder(trans('cart::order.placeholder.zipcode'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('state_id')
                       -> label(trans('cart::order.label.state_id'))
                       -> placeholder(trans('cart::order.placeholder.state_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('country_id')
                       -> label(trans('cart::order.label.country_id'))
                       -> placeholder(trans('cart::order.placeholder.country_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_name')
                       -> label(trans('cart::order.label.shipping_name'))
                       -> placeholder(trans('cart::order.placeholder.shipping_name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_city')
                       -> label(trans('cart::order.label.shipping_city'))
                       -> placeholder(trans('cart::order.placeholder.shipping_city'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_zipcode')
                       -> label(trans('cart::order.label.shipping_zipcode'))
                       -> placeholder(trans('cart::order.placeholder.shipping_zipcode'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_email')
                       -> label(trans('cart::order.label.shipping_email'))
                       -> placeholder(trans('cart::order.placeholder.shipping_email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_address')
                       -> label(trans('cart::order.label.shipping_address'))
                       -> placeholder(trans('cart::order.placeholder.shipping_address'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_phone')
                       -> label(trans('cart::order.label.shipping_phone'))
                       -> placeholder(trans('cart::order.placeholder.shipping_phone'))!!}
                </div>
 -->
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('order_status')
                       -> label('Order Status')
                       -> placeholder('Order Status')!!}
                </div>

               
                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('subtotal')
                       -> label(trans('cart::order.label.subtotal'))
                       -> placeholder(trans('cart::order.placeholder.subtotal'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('total')
                       -> label(trans('cart::order.label.total'))
                       -> placeholder(trans('cart::order.placeholder.total'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('payment_status')
                    -> options(trans('cart::order.options.payment_status'))
                    -> label(trans('cart::order.label.payment_status'))
                    -> placeholder(trans('cart::order.placeholder.payment_status'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('delivery_time')
                       -> label('Delivery Time')
                       -> placeholder('Delivery Time')!!}
                </div>

            </div>