 <table id="cart-order-list" class="table table-striped data-table" >
                    <thead class="list_head">
                        
                        <th data-field="name">Restaurant</th>
                        <th data-field="email">Orders</th>
                    <th data-field="email">Amount</th>
                    <th data-field="phone">Tax</th>
                    <th data-field="phone">Delivery</th>
                    <th data-field="phone">CCR</th>
                    <th data-field="phone">CCF</th>
                    <th data-field="phone">ZR</th>
                    <th data-field="phone">ZF</th>
                    <th data-field="phone">ZC</th>
                    <th data-field="phone">Tip</th>
                    <th data-field="track_status">Eatery Amount Due</th>
                    <th data-field="total">Account No</th>
                    <th data-field="payment_status">Routing No</th>
                    <th data-field="created_at">Bank</th>
<!--                     <th data-field="created_at">Status</th>
 -->                    </thead>
                    <tbody >
                        @foreach($orders as $order) 
                        <tr>
                        <td data-field="name">{{$order->name}}</td>
                         <td data-field="name">{{$order->count}}</td>
                    <td data-field="email">{{$order->Total}}</td>
                    <td data-field="phone">{{number_format($order->order_tax,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_delivery,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_CCR,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_CCF,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_ZR/100,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_ZF,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_ZC,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_tip,2)}}</td>
                    <td data-field="track_status">{{number_format($order->EateryAmount,2)}}</td>
                    <td data-field="total">{{substr_replace($order->ac_no, 'xxx', 0,-4)}}</td>
                    <td data-field="payment_status">{{$order->IFSC}}</td>
                    <td data-field="created_at">{{$order->bank}}</td>
<!--                     <td data-field="created_at">{{$order->pay_to}}</td>
 -->                </tr>
                @endforeach
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th>{{$orders->sum('count')}}</th>
                        <th>${{number_format($orders->sum('Total'),2)}}</th>
                        <th>${{number_format($orders->sum('order_tax'),2)}}</th>
                        <th>${{number_format($orders->sum('order_delivery'),2)}}</th>
                        <th>${{number_format($orders->sum('order_CCR'),2)}}</th>
                        <th>${{number_format($orders->sum('order_CCF'),2)}}</th>
                        <th>${{number_format($orders->sum('order_ZR/100'),2)}}</th>
                        <th>${{number_format($orders->sum('order_ZF'),2)}}</th>
                        <th>${{number_format($orders->sum('order_ZC'),2)}}</th>
                        <th>${{number_format($orders->sum('order_tip'),2)}}</th>
                        <th>${{number_format($orders->sum('EateryAmount'),2)}}</th>
                    </tfoot>
                </table>