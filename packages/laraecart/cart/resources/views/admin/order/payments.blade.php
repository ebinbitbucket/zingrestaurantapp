<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('cart::order.name') !!} <small> {!! trans('app.manage') !!} {!! trans('cart::order.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('cart::order.names') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div id='cart-order-entry'>
    </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                    <li class="{!!(request('status') == '')?'active':'';!!}"><a href="{!!guard_url('cart/order')!!}">{!! trans('cart::order.names') !!}</a></li>
                    <li class="pull-right" style="padding-top: 30px;">
                    <span class="actions">
                       <label>Unpublished Restaurants <input type="checkbox" id="filter_published" style="margin-right: 30px;vertical-align: middle;position: relative;
    bottom: 2px;"></label>
                    <!--   
                    <a  class="btn btn-xs btn-purple"  href="{!!guard_url('cart/order/reports')!!}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Reports</span></a>
                    @include('cart::admin.order.partial.actions')
                    -->
                    Filter Date <!-- <input type="date" name="filter_date" class="form-group" placeholder="Please select a Date" id="filter_date"> -->
                    <input type="text" id="datepicker1">
                    <input type="text" id="datepicker2">
                    <a type="button" class="btn btn-success btn-sm" href="" onclick="this.href='{{guard_url("cart/payments/download")}}?filter_date1='+document.getElementById('datepicker1').value+'&filter_date2='+document.getElementById('datepicker2').value"><i class="fa fa-download"></i> Download as CSV</a>
                    <a type="button" class="btn btn-danger btn-sm" href="" onclick="this.href='{{guard_url("cart/payments/update/processing")}}?filter_date1='+document.getElementById('datepicker1').value+'&filter_date2='+document.getElementById('datepicker2').value"><i class="fa fa-spinner"></i> Mark all as Processing</a>
                    <a type="button" class="btn btn-sm" style="background-color: #337ab7;border-color: #337ab7;color: #fff;" href="" onclick="this.href='{{guard_url("cart/payments/update/paid")}}?filter_date1='+document.getElementById('datepicker1').value+'&filter_date2='+document.getElementById('datepicker2').value"><i class="fa fa-credit-card"></i>  Mark all as Paid</a>
                    </span> 
                </li>
            </ul>
            <div class="tab-content" id="filter_table" style="overflow:scroll">
                <table  id="cart-order-list" class="table table-striped data-table" >
                    <thead class="list_head">
                        
                        <th data-field="name">Restaurant</th>
                        <th data-field="email">Orders</th>
                    <th data-field="email">Amount</th>
                    <th data-field="phone">Tax</th>
                    <th data-field="phone">Delivery</th>
                    <th data-field="phone">CCR</th>
                    <th data-field="phone">CCF</th>
                    <th data-field="phone">ZR</th>
                    <th data-field="phone">ZF</th>
                    <th data-field="phone">ZC</th>
                    <th data-field="phone">Tip</th>
                    <th data-field="track_status">Eatery Amount Due</th>
                    <th data-field="total">Account No</th>
                    <th data-field="total">Account Added</th>
                    <th data-field="payment_status">Routing No</th>
                    <th data-field="created_at">Bank</th>
<!--                     <th data-field="created_at">Status</th>
 -->                    </thead>
                    <tbody >
                        @foreach($orders as $order)
                        <tr>
                        <td data-field="name">{{$order->name}}</td>
                         <td data-field="name">{{$order->count}}</td>
                    <td data-field="email">{{$order->Total}}</td>
                    <td data-field="phone">{{number_format($order->order_tax,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_delivery,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_CCR,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_CCF,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_ZR/100,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_ZF,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_ZC,2)}}</td>
                    <td data-field="phone">{{number_format($order->order_tip,2)}}</td>
                    <td data-field="track_status">{{number_format($order->EateryAmount,2)}}</td>
                    <td data-field="total">{{substr_replace($order->ac_no, 'xxx', 0,-4)}}</td>
                    <td data-field="total">{{$order->acc_date}}</td>
                    <td data-field="payment_status">{{$order->IFSC}}</td>
                    <td data-field="created_at">{{$order->bank}}</td>
<!--                     <td data-field="created_at">{{$order->pay_to}}</td>
 -->                </tr>
                @endforeach
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th>{{$orders->sum('count')}}</td>
                        <th>${{number_format($orders->sum('Total'),2)}}</th>
                        <th>${{number_format($orders->sum('order_tax'),2)}}</th>
                        <th>${{number_format($orders->sum('order_delivery'),2)}}</th>
                        <th>${{number_format($orders->sum('order_CCR'),2)}}</th>
                        <th>${{number_format($orders->sum('order_CCF'),2)}}</th>
                        <th>${{number_format($orders->sum('order_ZR/100'),2)}}</th>
                        <th>${{number_format($orders->sum('order_ZF'),2)}}</th>
                        <th>${{number_format($orders->sum('order_ZC'),2)}}</th>
                        <th>${{number_format($orders->sum('order_tip'),2)}}</th>
                        <th>${{number_format($orders->sum('EateryAmount'),2)}}</th>
                    </tfoot>
                </table>
            </div>
        </div>
    </section>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
    $( function() {
    $( "#datepicker1" ).datepicker();
    $( "#datepicker2" ).datepicker();
  } );
    $('#datepicker2').change(function(e){
        e.preventDefault();
    var filter_date1 = $('#datepicker1').val();
    var filter_date2 = $('#datepicker2').val();
    if($('#filter_published').prop('checked') == true){
            var filter_published = 'Unpublished';
        }
        else{
            var filter_published = 'Published';
        }
    filter_ajax(filter_published,filter_date1,filter_date2);
    })
        $('#datepicker1').change(function(e){
        e.preventDefault();
    var filter_date1 = $('#datepicker1').val();
    var filter_date2 = $('#datepicker2').val();
     if($('#filter_published').prop('checked') == true){
            var filter_published = 'Unpublished';
        }
        else{
            var filter_published = 'Published';
        }
        filter_ajax(filter_published,filter_date1,filter_date2);
    })
           $('#filter_published').change(function(e){
        e.preventDefault();
        if(this.checked == true){
            var filter_published = 'Unpublished';
        }
        else{
            var filter_published = 'Published';
        }
        var filter_date1 = $('#datepicker1').val();
        var filter_date2 = $('#datepicker2').val();
        filter_ajax(filter_published,filter_date1,filter_date2);
    })
    function filter_ajax(filter_published,filter_date1,filter_date2){
        $.ajax({
            url: "{{ URL::to('admin/cart/payments/filter') }}/",
            dataType:'JSON',
            data: {filter_published:filter_published,filter_date1:filter_date1,filter_date2:filter_date2},
            success: function(response){

                    console.log("New Order",response);
                    $('#filter_table').html(response.view);
            },
            error: function(msg) { 
                   
               

                  }
        });
    }
</script>
<!-- <script type="text/javascript">

var oTable;
var oSearch;
$(document).ready(function(){
    // app.load('#cart-order-entry', '{!!guard_url('cart/order/0')!!}');
    oTable = $('#cart-order-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }], 
        
        "responsive" : true,
        "order": [[1, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('cart/order') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'name'},
            {data :'email'},
            {data :'phone'},
            {data :'order_status'},
            {data :'total'},
            {data :'payment_status'},
            {data :'delivery_time'},
        ],
        "pageLength": 25
    });

    $('#cart-order-list tbody').on( 'click', 'tr td:not(:first-child)', function (e) {
        e.preventDefault();

        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#cart-order-list').DataTable().row( this ).data();
        $('#cart-order-entry').load('{!!guard_url('cart/order')!!}' + '/' + d.id);
    });

    $('#cart-order-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent'); 
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#cart-order-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }
        
    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#cart-order-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#cart-order-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});
</script> -->