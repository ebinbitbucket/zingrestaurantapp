            @include('cart::order.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('cart::order.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($order->defaultImage('images' , 'xl'))!!}" alt="{{$order->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('cart::order.label.id') !!}
                </label><br />
                    {!! $order['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('cart::order.label.name') !!}
                </label><br />
                    {!! $order['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="email">
                    {!! trans('cart::order.label.email') !!}
                </label><br />
                    {!! $order['email'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="address">
                    {!! trans('cart::order.label.address') !!}
                </label><br />
                    {!! $order['address'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="phone">
                    {!! trans('cart::order.label.phone') !!}
                </label><br />
                    {!! $order['phone'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="street">
                    {!! trans('cart::order.label.street') !!}
                </label><br />
                    {!! $order['street'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="zipcode">
                    {!! trans('cart::order.label.zipcode') !!}
                </label><br />
                    {!! $order['zipcode'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="state_id">
                    {!! trans('cart::order.label.state_id') !!}
                </label><br />
                    {!! $order['state_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="country_id">
                    {!! trans('cart::order.label.country_id') !!}
                </label><br />
                    {!! $order['country_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="shipping_name">
                    {!! trans('cart::order.label.shipping_name') !!}
                </label><br />
                    {!! $order['shipping_name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="shipping_city">
                    {!! trans('cart::order.label.shipping_city') !!}
                </label><br />
                    {!! $order['shipping_city'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="shipping_zipcode">
                    {!! trans('cart::order.label.shipping_zipcode') !!}
                </label><br />
                    {!! $order['shipping_zipcode'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="shipping_email">
                    {!! trans('cart::order.label.shipping_email') !!}
                </label><br />
                    {!! $order['shipping_email'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="shipping_address">
                    {!! trans('cart::order.label.shipping_address') !!}
                </label><br />
                    {!! $order['shipping_address'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="shipping_phone">
                    {!! trans('cart::order.label.shipping_phone') !!}
                </label><br />
                    {!! $order['shipping_phone'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="track_status">
                    {!! trans('cart::order.label.track_status') !!}
                </label><br />
                    {!! $order['track_status'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="shipping">
                    {!! trans('cart::order.label.shipping') !!}
                </label><br />
                    {!! $order['shipping'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="delivery_id">
                    {!! trans('cart::order.label.delivery_id') !!}
                </label><br />
                    {!! $order['delivery_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="subtotal">
                    {!! trans('cart::order.label.subtotal') !!}
                </label><br />
                    {!! $order['subtotal'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tax">
                    {!! trans('cart::order.label.tax') !!}
                </label><br />
                    {!! $order['tax'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="total">
                    {!! trans('cart::order.label.total') !!}
                </label><br />
                    {!! $order['total'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="payment_status">
                    {!! trans('cart::order.label.payment_status') !!}
                </label><br />
                    {!! $order['payment_status'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="payment_methods">
                    {!! trans('cart::order.label.payment_methods') !!}
                </label><br />
                    {!! $order['payment_methods'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="payment_details">
                    {!! trans('cart::order.label.payment_details') !!}
                </label><br />
                    {!! $order['payment_details'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('cart::order.label.created_at') !!}
                </label><br />
                    {!! $order['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('cart::order.label.updated_at') !!}
                </label><br />
                    {!! $order['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('cart::order.label.deleted_at') !!}
                </label><br />
                    {!! $order['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('cart::order.label.name'))
                       -> placeholder(trans('cart::order.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('email')
                       -> label(trans('cart::order.label.email'))
                       -> placeholder(trans('cart::order.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('address')
                       -> label(trans('cart::order.label.address'))
                       -> placeholder(trans('cart::order.placeholder.address'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('phone')
                       -> label(trans('cart::order.label.phone'))
                       -> placeholder(trans('cart::order.placeholder.phone'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('street')
                       -> label(trans('cart::order.label.street'))
                       -> placeholder(trans('cart::order.placeholder.street'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('zipcode')
                       -> label(trans('cart::order.label.zipcode'))
                       -> placeholder(trans('cart::order.placeholder.zipcode'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('state_id')
                       -> label(trans('cart::order.label.state_id'))
                       -> placeholder(trans('cart::order.placeholder.state_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('country_id')
                       -> label(trans('cart::order.label.country_id'))
                       -> placeholder(trans('cart::order.placeholder.country_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_name')
                       -> label(trans('cart::order.label.shipping_name'))
                       -> placeholder(trans('cart::order.placeholder.shipping_name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_city')
                       -> label(trans('cart::order.label.shipping_city'))
                       -> placeholder(trans('cart::order.placeholder.shipping_city'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_zipcode')
                       -> label(trans('cart::order.label.shipping_zipcode'))
                       -> placeholder(trans('cart::order.placeholder.shipping_zipcode'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_email')
                       -> label(trans('cart::order.label.shipping_email'))
                       -> placeholder(trans('cart::order.placeholder.shipping_email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_address')
                       -> label(trans('cart::order.label.shipping_address'))
                       -> placeholder(trans('cart::order.placeholder.shipping_address'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping_phone')
                       -> label(trans('cart::order.label.shipping_phone'))
                       -> placeholder(trans('cart::order.placeholder.shipping_phone'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('track_status')
                       -> label(trans('cart::order.label.track_status'))
                       -> placeholder(trans('cart::order.placeholder.track_status'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('shipping')
                       -> label(trans('cart::order.label.shipping'))
                       -> placeholder(trans('cart::order.placeholder.shipping'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('delivery_id')
                       -> label(trans('cart::order.label.delivery_id'))
                       -> placeholder(trans('cart::order.placeholder.delivery_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('subtotal')
                       -> label(trans('cart::order.label.subtotal'))
                       -> placeholder(trans('cart::order.placeholder.subtotal'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('tax')
                       -> label(trans('cart::order.label.tax'))
                       -> placeholder(trans('cart::order.placeholder.tax'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('total')
                       -> label(trans('cart::order.label.total'))
                       -> placeholder(trans('cart::order.placeholder.total'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('payment_status')
                    -> options(trans('cart::order.options.payment_status'))
                    -> label(trans('cart::order.label.payment_status'))
                    -> placeholder(trans('cart::order.placeholder.payment_status'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('payment_methods')
                       -> label(trans('cart::order.label.payment_methods'))
                       -> placeholder(trans('cart::order.placeholder.payment_methods'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('payment_details')
                       -> label(trans('cart::order.label.payment_details'))
                       -> placeholder(trans('cart::order.placeholder.payment_details'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



