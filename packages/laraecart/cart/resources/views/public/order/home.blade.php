
<div class="element-content" id="filter_table">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                     <div class="col-sm-6">
                        <a class="element-box el-tablo" href="#">
                            <div class="label">Visitors</div>
                            <div class="value">{{@$totalvisitors}}</div>
                        </a>
                    </div>
                     <div class="col-sm-6">
                        <a class="element-box el-tablo" href="#">
                            <div class="label">Repeat Visitors</div>
                            <div class="value">{{@$repeatvisitors}}</div>
                        </a>
                    </div>
                   
                    
                    <div class="col-sm-6 mt-10">
                        <a class="element-box el-tablo" href="#">
                            <div class="label">No of Customers</div>
                            <div class="value">{{@$customer_count}}</div>
                        </a>
                    </div>
                     <div class="col-sm-6 mt-10">
                        <a class="element-box el-tablo" href="#">
                            <div class="label">No of repeat customers</div>
                            <div class="value">{{@$repeat_cutomer_count}}</div>
                        </a>
                    </div>
                     <div class="col-sm-6 mt-10">
                        <a class="element-box el-tablo" href="#">
                            <div class="label">Total no of transactions</div>
                            <div class="value">{{@$transactions_count}}</div>
                        </a>
                    </div>
                    <div class="col-sm-6 mt-10">
                        <a class="element-box el-tablo" href="#">
                            <div class="label">Total Sales</div>
                            <div class="value">${{number_format(@$transactions_amount,2)}}</div>
                        </a>
                    </div>
                    
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card pt-30" style="height: 309px;">
                    <div class="card-block mt-30 pb-10">
                        <div id="chartdiv"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<script type="text/javascript">
	am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdiv", am4charts.XYChart3D);
chart.paddingBottom = 30;
chart.angle = 25;

// Add data
chart.data = [{
  "country": "No of customers",
  "visits": '{{@$customer_count}}'
}, {
  "country": "Repeat Customers",
  "visits": '{{@$repeat_cutomer_count}}'
}];

// Create axes
var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "country";
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.renderer.minGridDistance = 20;
categoryAxis.renderer.inside = true;
categoryAxis.renderer.grid.template.disabled = true;

let labelTemplate = categoryAxis.renderer.labels.template;
labelTemplate.rotation = -90;
labelTemplate.horizontalCenter = "left";
labelTemplate.verticalCenter = "middle";
labelTemplate.dy = 10; // moves it a bit down;
labelTemplate.inside = false; // this is done to avoid settings which are not suitable when label is rotated

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.renderer.grid.template.disabled = true;

// Create series
var series = chart.series.push(new am4charts.ConeSeries());
series.dataFields.valueY = "visits";
series.dataFields.categoryX = "country";

var columnTemplate = series.columns.template;
columnTemplate.adapter.add("fill", function(fill, target) {
  return chart.colors.getIndex(target.dataItem.index);
})

columnTemplate.adapter.add("stroke", function(stroke, target) {
  return chart.colors.getIndex(target.dataItem.index);
})

}); // end am4core.ready()
</script>