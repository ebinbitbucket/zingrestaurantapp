<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-10 col-md-8 col-lg-6" style="padding: 10px;">
            <!-- Form -->
            <form class="form-example" action="{{url('order/car-delivery-save')}}" method="get">
                <!-- Input fields -->
                @if(@$order->order_status=='Cancelled')
                <div class="alert alert-warning" role="alert"><span style="color: black">Your Order Cancelled PLease Contact Restuarent {{$order->restaurant->phone}} </span></div>
                
                 @elseif(@@$order->order_status=='New Orders')
                 <div class="alert alert-warning" role="alert"><span style="color: black">This order is still waiting to be accepted by the restaurant. Call {{$order->restaurant->phone}} to request them to accept the order and to confirm status</span></div>
                  @else
                  <h2>Car and Parking Information</h2>
                  <span> We have notified the restaurant that you have arrived. Your food should be brought to you shortly. Just a few more moments to enjoy your delicious meal.</span>

                  @endif
                  <br>
                <input type="hidden" value="{{$order->getRoutekey()}}" class="form-control number" name="id">

                <div class="formgroup">
                    <label for="number">Car Make and Model :</label>
                    <input type="text" required class="form-control number" id="number" placeholder="" name="number">
                </div>
                <div class="form-group">
                    <label for="color">Car Color  :</label>
                    <input type="text" required class="form-control color" id="color" placeholder="" name="color">
                </div>
                <div class="form-group">
                    <label for="color">Parking And Other Information :</label>
                    <textarea class="form-control" required name="info" rows="5" id="info"></textarea>

                </div>
                <button style="margin-bottom: 50px;" type="submit" class="btn btn-theme btn-customized">Notify Restaurant </button>
            </form>
        </div>
    </div>
</div>