            @include('cart::details.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('cart::details.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($details->defaultImage('images' , 'xl'))!!}" alt="{{$details->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('cart::details.label.id') !!}
                </label><br />
                    {!! $details['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_id">
                    {!! trans('cart::details.label.order_id') !!}
                </label><br />
                    {!! $details['order_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="product_id">
                    {!! trans('cart::details.label.product_id') !!}
                </label><br />
                    {!! $details['product_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="size">
                    {!! trans('cart::details.label.size') !!}
                </label><br />
                    {!! $details['size'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="quantity">
                    {!! trans('cart::details.label.quantity') !!}
                </label><br />
                    {!! $details['quantity'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="unit_price">
                    {!! trans('cart::details.label.unit_price') !!}
                </label><br />
                    {!! $details['unit_price'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="price">
                    {!! trans('cart::details.label.price') !!}
                </label><br />
                    {!! $details['price'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_id">
                    {!! trans('cart::details.label.user_id') !!}
                </label><br />
                    {!! $details['user_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_type">
                    {!! trans('cart::details.label.user_type') !!}
                </label><br />
                    {!! $details['user_type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="parameters">
                    {!! trans('cart::details.label.parameters') !!}
                </label><br />
                    {!! $details['parameters'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('cart::details.label.created_at') !!}
                </label><br />
                    {!! $details['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('cart::details.label.updated_at') !!}
                </label><br />
                    {!! $details['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('cart::details.label.deleted_at') !!}
                </label><br />
                    {!! $details['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('cart::details.label.order_id'))
                       -> placeholder(trans('cart::details.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('product_id')
                       -> label(trans('cart::details.label.product_id'))
                       -> placeholder(trans('cart::details.placeholder.product_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('size')
                       -> label(trans('cart::details.label.size'))
                       -> placeholder(trans('cart::details.placeholder.size'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('quantity')
                       -> label(trans('cart::details.label.quantity'))
                       -> placeholder(trans('cart::details.placeholder.quantity'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('unit_price')
                       -> label(trans('cart::details.label.unit_price'))
                       -> placeholder(trans('cart::details.placeholder.unit_price'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('price')
                       -> label(trans('cart::details.label.price'))
                       -> placeholder(trans('cart::details.placeholder.price'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('parameters')
                       -> label(trans('cart::details.label.parameters'))
                       -> placeholder(trans('cart::details.placeholder.parameters'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



