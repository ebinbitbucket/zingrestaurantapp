<?php $delivery_charge =0; ?>
@foreach(Cart::content() as $cart) 
      <?php 
      $rest_id = $cart->options->restaurant;
      $rest_data = Restaurant::getRestaurantData($rest_id); 
                            if($cart->options['delivery_charge'] != '')
                            { $delivery_charge = $cart->options['delivery_charge']; }
                            ?>
                            @endforeach
<div class="break-down-items">
                                <span>Subtotal</span>
                                <span>${{number_format(Cart::total(),2)}}</span>
                                <input type="hidden" name="ref_subtotal" id="ref_subtotal" value="{{Cart::total()}}">
                            </div>
                            <div class="break-down-items">
                                <span>Tax and Fees <a href="javascript:void(0);" class="text-secondary" data-toggle="tooltip" data-placement="top" data-html="true" title="<p class='m-0 text-left'>Local Tax = {{!empty($rest_data) ? number_format($rest_data->tax_rate,2) : ''}}%</p><p class='m-0 text-left'>Zing Service = ${{!empty($rest_data) ? number_format($rest_data->ZC,2) : ''}}</p>"><i class="ion-android-alert"></i></a></span>
                                <span>${{number_format(Cart::total()*number_format(!empty($rest_data) ?  $rest_data->tax_rate : 0,2)/100+number_format(!empty($rest_data) ?  $rest_data->ZC : 0,2),2)}}</span>
                                
                            </div>
                            <input type="hidden" id="tax" name="tax" value="{{Cart::total()*number_format(!empty($rest_data) ?  $rest_data->tax_rate : 0,2)/100+number_format(!empty($rest_data) ?  $rest_data->ZC : 0,2)}}">
                            <div class="break-down-items" id="delivery_div" style="display: none;">
                                <span>Delivery</span>
                                <span id="delivery_amount_span">${{number_format($delivery_charge,2)}}</span>
                            </div>
                             <div class="break-down-items" id="difference_div">
                                <span>Minimum Order Difference</span>
                                <span style="padding-right: 50px;"><a href="#" class="remove-btn" onclick="remove_difference();"><i class="ion-close-circled" title="Remove this item from the cart"></i></a></span>
                                <span id="difference_amount_span"></span>
                                <input type="hidden" name="min_order_difference" id="min_order_difference">
                            </div>
                            <div class="break-down-items">
                                <span>Tip</span>
                                <span id="tip_charge">$0.00</span>
                            </div>
                            <div class="tip-block">
                                <input type="hidden" id="tip" name="tip">
                                <div class="tip-item" style="width: 40px;"> 
                                    <input type="radio" id="tip_0" name="tip_value" value=0 class="tipcal">
                                    <label for="tip_0">0%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;"> 
                                    <input type="radio" id="tip_5" name="tip_value" value=5 class="tipcal">
                                    <label for="tip_5">5%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;">
                                    <input type="radio" id="tip_10" name="tip_value" value=10 class="tipcal">
                                    <label for="tip_10">10%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;">
                                        <input type="radio" id="tip_15" name="tip_value" value=15 class="tipcal" checked="checked">
                                        <label for="tip_15">15%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;">
                                    <input type="radio" id="tip_20" name="tip_value" value=20 class="tipcal">
                                    <label for="tip_20">20%</label>
                                </div>
                                <div class="tip-item" >
                                    <input type="radio" name="tip_value" id="custom" value="">
                                    <label for="custom">Custom</label>
                                </div>
                                <div class="tip-item" style="width: 60px; height: 30px;">
                                    <input type="number" class="form-control" name="tip_value_cus" id="customper" style="display: none;" min=0 max=1000>
                                </div>
                               <!--  <input type="text" class="form-control" placeholder="Enter custom tip" name="tipper" id="customper" style="display: none;"> -->
                            </div>
                            <hr>  
                            <div class="break-down-items points">
                                <span>Points <a href="#" onclick="redeem()" >Redeem Points</a></span>
                                <span id="points_redeem">$0.00</span>
                            </div>
                            @if(($rest_data->offers != null))
                            <div class="break-down-items points">
                                <span>Discount <a href="#" onclick="apply_offer()" id="apply_offer_link" >Apply Offer</a></span>
                                @if($rest_data->offers->type == 'Price')
                                <span>$ {{$rest_data->offers->value}}</span>
                                @else
                                <span>{{$rest_data->offers->value}} %</span>
                                @endif
                            </div>
                            @endif
                            <input type="hidden" id="discount" name="discount">
                            <hr>
                            <div class="break-down-items total"> 
                                <span>Total Amount</span>
                                <div class="text-right">
                                    <strike class="mr-5"><small id="original_amount"></small></strike> <b>$</b><span id="total_charge">{{number_format(Cart::total(),2)}}</span>
                            </div>
                            </div>
                        </div>
<!--                         <input type="hidden" id="total_amount" name="total_amount">
 -->
<script type="text/javascript">
    var restaurant_id = '{{$rest_data->getRoutekey()}}';
    var MyData = {};
$(document).ready(function(){
    MyData.del_charge = delivery_charge;
var type = $('input[name="order_type"]:checked').val();
            $.ajax({
            url: "{{ URL::to('cartcheck/min_order')}}"+'/'+type+'/'+restaurant_id,
            success: function(response){

                if(response.status != 'Success'){
                    if(response.section == 'section_summary'){

                        $('#sectionHead_summary').show();
                        $('#section_summary').html(response.msg+'<button type="button" class="btn btn-secondary btn-sm mt-10" onclick="apply_difference();" >Pay Min. Amount</button>');
                        $('#sectionHead_delivery').hide();
                        // $('#section_delivery').html(response.msg+'<button class="btn btn-theme btn-block" onclick="apply_difference();" >Apply Minimum Order difference</button>');
                        flag_minorder ++;
                    }
                }
                else{ 
                    $('#sectionHead_summary').hide();
                    $('#sectionHead_delivery').hide();
                    flag_minorder = 0;
                }

            }
    });
});
$('#difference_div').hide();
 if( $('input[name="order_type"]:checked').val() == 'Delivery'){
        $('#delivery_div').show();
        document.getElementById('total_charge').textContent='{{number_format(Cart::total()+$delivery_charge+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0)+(15/100)*Cart::total(),2)}}';
$('#total_amount').val('{{Cart::total()+$delivery_charge+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0)+(15/100)*Cart::total()}}');
    }
    else{
        <?php $delivery_charge = 0; ?>
        document.getElementById('total_charge').textContent='{{number_format(Cart::total()+$delivery_charge+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0)+(15/100)*Cart::total(),2)}}';
$('#total_amount').val('{{Cart::total()+$delivery_charge+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0)+(15/100)*Cart::total()}}');
    }
    
var total = '<?php echo Cart::total(); ?>'; 
        var tip = (total * 15)/100;
        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
        document.getElementById("tip").value=tip;
    $('.tipcal').on('click', function() { 
        if(flag_qty!=1){
        document.getElementById('customper').style = "display:none;";
        document.getElementById('customper').value = "";
        var total = '<?php echo Cart::total(); ?>'; 
        var tip = (total * $(this).val())/100;
        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
        document.getElementById("tip").value=tip;
    //      if($('input[name="order_type"]:checked').val() != 'Pickup'){
    //     delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
    // }
    // else{
    //      delivery_charge = 0;
    // }
    var decharge = delivery_charge;
     if($('input[name="order_type"]:checked').val() == 'Pickup'){
        decharge = 0;
    }
  
      if($('#min_order_difference').val() != ''){
        var min_order_difference = $('#min_order_difference').val();
    }
    else{
        var min_order_difference = 0;
    }

        var grandtotal = '<?php echo Cart::total()+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(grandtotal)+parseFloat(decharge)+parseFloat(tip)+parseFloat(min_order_difference)).toFixed(2);
        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(decharge)+parseFloat(tip)+parseFloat(min_order_difference));
        document.getElementById('apply_offer_link').style = "display:block;";
        document.getElementById('original_amount').style = "display:none;";
    }
});



    $('#custom').on('click', function() { 
        if(flag_qty!=1){
        document.getElementById('customper').style = "display:block;";
    }
    });    
    
    $('#customper').on('input', function() {
if(document.getElementById('customper').value){
    var total = '<?php echo Cart::total(); ?>';
       // var tip = (total * document.getElementById('customper').value)/100;
        var tip = document.getElementById('customper').value;
        document.getElementById("tip_charge").textContent='$'+parseFloat(tip).toFixed(2);
        document.getElementById("tip").value=tip;
        if( $('input[name="order_type"]:checked').val() == 'Delivery'){
           
var dcharge = MyData.del_charge; 
   }
    else{
        var dcharge = 0;
           }
    //alert(delivery_charge);
          if($('#min_order_difference').val() != ''){
        var min_order_difference = $('#min_order_difference').val();
    }
    else{
        var min_order_difference = 0;
    }
        var grandtotal = '<?php echo Cart::total()+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(grandtotal)+parseFloat(dcharge)+parseFloat(tip)+parseFloat(min_order_difference)).toFixed(2);
        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(dcharge)+parseFloat(tip)+parseFloat(min_order_difference));
        document.getElementById('apply_offer_link').style = "display:block;";
        document.getElementById('original_amount').style = "display:none;";
}
        
    });
</script>