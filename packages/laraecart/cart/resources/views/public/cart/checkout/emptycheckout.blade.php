<?php $rest_id   = 0;
$delivery_charge = 0;?>
<div class="empty-content-wrap">
    <div class="container">
        <img src="{{theme_asset('img/cart-empty.svg')}}" alt="">
        <h2>Your Cart is Empty</h2>
        <p>Looks like you haven't made your choice yet</p>
        <p><a href="{{url('/')}}">Go Home</a></p>
    </div>
</div>

