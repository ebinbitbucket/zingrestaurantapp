
@if(Cart::count() <= 0)
<?php $rest_id   = 0;
$delivery_charge = 0;?>
<div class="empty-content-wrap">
    <div class="container">
        <img src="{{theme_asset('img/empty-cart.png')}}" alt="">
        <h2>Your Cart is Empty</h2>
        <p>Looks like you haven't made your choice yet</p>
        <p><a href="{{url('/')}}">Go Home</a></p>
    </div>
</div>
@else

<section class="checkout-wrap">
    <div class="container">
        @foreach($carts as $cart)
            <?php $rest_id = $cart->options->restaurant;
                $rest_data                             = Restaurant::getRestaurantData($rest_id);
            ?>
        @endforeach

        <div class="row row-checkout" id="checkout_wrapMain">
            <div class="col-md-8 position-static">
                <div class="checkout-block-wrap pb-0">
                    

                    <div class="block-item item-login customer-entry">
                        <a href="{{trans_url('restaurants/'.$rest_data->slug)}}"  class="checkout-back-btn d-none d-md-block" data-toggle="tooltip" data-placement="top" title="Back to Eatery Page" data-original-title="Go Back" id="btn_back1" ><i class="ion-android-arrow-back" style="display: inline-block;
                            width: 35px;
                            height: 35px;
                        /*    background-color: #ededed;
                        */    border-radius: 50%;
                            text-align: center;
                            line-height: 35px;
                            color: #333;
                            margin-right: 10px;"></i></a>
                        <div class="row" >
                            <div class="col-md-12">

                                @if(Auth::user())
                                <div class="login-card">
                                    @if(!empty(user()->mobile))
                                        <?php $flag_login=0; ?>
                                        <h3><span>{{user()->name}}</span>'s Order</h3>
                                        <p>Not you? <a href="#" id="logout-customer"> Click here to logout</a></p>
                                    @else
                                    <?php $flag_login=1; ?>
                                    <h3><span>{{user()->name}}</span>'s Order</h3>
                                        <p>Not you? <a href="#" id="logout-customer"> Click here to logout</a></p>
                                        <div class="alert alert-warning" role="alert" id="sectionHead_login" style="display: none;"><span id="section_login"></span></div>
                                        *This is the phone number the eatery will call in case of any issue regarding your order
                                        <div class="form-group">
                                            {!! Form::tel('mobile')
                                                                ->required()
                                                                ->label('')
                                                                ->id('client_mobile')
                                                                ->placeholder('Phone Number') !!}
                                                                
                                        </div>
                                        <button class="btn btn-theme mr-10" style="min-width: 100px;" id="mobile_submit">Add Phone</button>
                                    @endif
                                </div>
                                @else

                                <?php $flag_login=1; ?>

                                <h3>Sign In</h3>
                        <div class="row ">
                            <div class="col-md-5">
                                <div class="social-btn-wrap">
                                    <a href="/client/login/facebook" class="btn btn-block btn-facebook"><i class="fa fa-facebook mr-5"></i> Sign in with Facebook</a>
                                    <a href="/client/login/google" class="btn btn-block btn-google"><i class="fa fa-google mr-5"></i> Sign in with Google</a>
                                    <div id="changable_login_div"><a href="#" class="btn btn-theme" id="collapseGuest" style="margin-top: 10px;width: 100%;background-color: #999;">GUEST CHECKOUT</a></div>
                                </div>
                            </div> <div class="col-md-1">
                                <h3 class="login-sep"><span>OR</span></h3>
                            </div>
                          
                            <div class="col-md-6" id="login_sec">
                          {!!Form::vertical_open()
    ->id('login-customer')
    ->action(trans_url('client/login'))
    ->method('POST')!!}
    @include('notifications') 
                            <div class="col-md-12">
                                <div class="login-wrap">
                                    <div class="form-group">
                                        {!! Form::email('email')
                                ->required()
                                ->placeholder('Email')->raw() !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::password('password')
                                ->placeholder('Password')
                                ->required()->raw()!!}
                                    </div>
                                    <center><button class="btn btn-theme mr-10" style="min-width: 100px;" id="login_submit">Login</button>
                                    <a href="#" class="btn btn-link" id="collapseRegister">Sign up</a>
                                    <p class="text-small mt20">I forgot my password - <a href="{{url('client/password/reset')}}" target="_blank">Reset Here</a></p>
                                    </center>
                                    <!-- <button class="btn btn-theme" id="collapseGuest" style="margin-top: 10px;width: 100%;background-color: #999;">GUEST CHECKOUT</button></center> -->
                                </div>
                            </div>
                             {!! Form::close() !!}
                             
                         </div>
                        </div>
                                @endif
                            </div>
                        </div>
                        
                    </div>  
                    <form id="frm_checkout" action="{{url('cart/paymentsubmit')}}" method="POST">
                        <div class="block-item item-time div-item">
                            <div class="alert alert-warning" role="alert" id="sectionHead_time"><span id="section_time"></span></div>
                            <div class="row">
                                <div class="col-md-4">
                                    <h3>Time</h3>
                                </div>
                                <div class="col-md-8">
                                    <div class="custom-control custom-radio mb-10">
                                        <input type="radio" id="asap" name="delivery_time" class="custom-control-input" >
                                        <label class="custom-control-label" for="asap">ASAP</label>
                                    </div>
                                     <div class="custom-control custom-radio">
                                        <input type="radio" id="schedule_order" name="delivery_time" class="custom-control-input">
                                        <label class="custom-control-label" for="schedule_order">Schedule for Later <span id="deltypet_wrap" style="display: none;"> - <span id="del_date_v"></span></br> <span id="del_time_v"></span></span>
                                            <button type="button" data-toggle="modal" data-target="#schedule_orderModal" class="scedule-edit" id="scedule_edit" style="display: none;"><i class="ion ion-android-create"></i></button>
                                    </label>
                                    </div>
                                    <input type="hidden" name="time_type" id="time_type">
                                    <input type="hidden" name="scheduled_date" id="datecheck">
                                    <input type="hidden" name="scheduled_time" id="timecheck">
                                </div>
                            </div>
                        </div>
                        <div class="block-item item-time div-item">
                                <div class="alert alert-warning" role="alert" id="sectionHead_delivery"><span id="section_delivery" class="d-md-flex justify-content-md-between"></span></div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <h3>Order Type</h3>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="custom-control custom-radio mb-10">
                                            <input type="radio" id="pickup" name="order_type" class="custom-control-input" checked="checked" value="Pickup">
                                            <label class="custom-control-label" for="pickup">Pickup</label>
                                        </div>
                                        @if(!empty($rest_data)) 
                                            @if($rest_data->delivery == 'Yes')
                                                 <div class="custom-control custom-radio">
                                                    <input type="radio" id="delivery" name="order_type" class="custom-control-input" value="Delivery">
                                                    <label class="custom-control-label" for="delivery">Deliver To</label>
                                                </div>
                                            @else
                                                <label  for="delivery">Delivery Not available</label>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="block-item item-address div-item" id="delivery_address_block">

                            </div>
                        </div>
                        <div class="payment-wrap div-item" id="cart_SummaryWrap">
                            <div class="order-summary">
                                 @foreach($carts as $cart)
                                <?php $rest_name = $cart->options->restaurantname;
                                $rest_logo                                   = $cart->options->restaurantimage;?>
                                @endforeach

                                <div class="order-restaurant">
                                    @if(!empty($rest_logo))
                                    <div class="rest-img" style="background-image: url('{{trans_url('image/original/'.$rest_logo[0]['path'])}}')"></div>
                                    @endif
                                    <div class="rest-info">
                                        <h3><a href="{{trans_url('restaurants/'.$rest_data->slug)}}" style="color: #3e5569;">{{$rest_name}}</a></h3>
                                        <p>{{!empty($rest_data) ? $rest_data->address : ''}}</p>
                                    </div>

                                </div>
                                <div class="alert alert-warning" role="alert" id="sectionHead_summary"><span id="section_summary"></span></div>
                                <h3>Summary</h3>
                                <?php $delivery_charge = 0;?>
                                @foreach($carts as $cart)
                                <div class="item" style="{{$cart->options['menu_addon'] == 'addon' ? 'margin-left: 20px' : ''}}">
                                    <div class="item-name-price">
                                         <?php
                                            $variation = $cart->options->variation; 
                                             $addon_price = 0;

                                             if (!empty($cart->options['addons'])) {

                                                 foreach ($cart->options['addons'] as $cart_addon) {

                                                     $addon_price = $addon_price + $cart_addon[2];
                                                 }

                                             }

                                         ?>
                                        <h4 class="name">{{$cart->name}}</h4>
                                    </div>
                                    @if(!empty($variation))<p class="name"><b>(Variation-{{$variation}})</b></p>@endif
                                    @if(!empty($cart->options['addons']))
                                    @foreach($cart->options['addons'] as $cart_addon)
                                    <div class="item-addon">{{$cart_addon[1]}}</div>
                                    @endforeach
                                    @endif
                                    @if($cart->options['menu_addon'] != 'addon')
                                    <!--<div class="item-price">-->
                                    <!--    <div class="row">-->
                                    <!--        <div class="col-7">-->
                                    <!--    x<span> -->
                                    <!--        <span class="enumerator">-->
                                    <!--            <span class="enumerator_btn js-minus_btn" onclick="decrement('{{$cart->rowId}}')" id="minus_span">-</span>-->
                                    <!--            <input type="number" name="quantity[{{$cart->rowId}}]" value="{{$cart->qty}}" class="enumerator_input" style="width: 30px;" readonly="readonly">-->
                                    <!--            <span class="enumerator_btn js-plus_btn"  onclick="increment('{{$cart->rowId}}')" id="plus_span">+</span>-->

                                    <!--        </span>-->
                                    <!--    - </span>-->
                                     <div class="item-price">
                                      <div class="qty-price-wrap">
                                            <div class="qty-price">
                                                <span class="enumerator">
                                                    <span class="enumerator_btn js-minus_btn" onclick="decrement('{{$cart->rowId}}')" id="minus_span">-</span>
                                                    <input class="enumerator_input" type="number" name="quantity[{{$cart->rowId}}]" value="{{$cart->qty}}" readonly="readonly">
                                                    <span class="enumerator_btn js-plus_btn"  onclick="increment('{{$cart->rowId}}')" id="plus_span">+</span>

                                                </span>

                                            </div>
                                            <span>${{number_format($cart->price+$addon_price,2)}}</span>
                                            <a href="{{url('carts/remove/'. $cart->rowId)}}"class="remove-btn"><i class="ion-close-circled" title="Remove this item from the cart"></i></a>
                                        </div>
                                    </div>
                                    @endif

                                </div>
                                <?php

                                    if ($cart->options['delivery_charge'] != '') {$delivery_charge = $cart->options['delivery_charge'];}

                                ?>
                                @endforeach
                            </div>
                            <hr>
                            <div class="payment-breakdown-wrap mt-20">
                                <div  id="payment_details">
                                <div class="break-down-items">
                                    <span>Subtotal</span>
                                    <span>${{number_format(Cart::total(),2)}}</span>
                                    <input type="hidden" name="ref_subtotal" id="ref_subtotal" value="{{Cart::total()}}">
                                </div>
                                <div class="break-down-items">
                                    <span>Tax and Fees <a href="#" class="text-secondary" data-toggle="tooltip" data-placement="top" data-html="true" title="<p class='m-0 text-left'>Local Tax = {{!empty($rest_data) ? number_format($rest_data->tax_rate,2) : ''}}%</p><p class='m-0 text-left'>Zing Service = ${{!empty($rest_data) ? number_format($rest_data->ZC,2) : ''}}</p>"><i class="ion-android-alert"></i></a></span>
                                    <span>${{number_format(Cart::total()*(!empty($rest_data) ?  $rest_data->tax_rate/100 : 0)+(!empty($rest_data) ?  $rest_data->ZC : 0),2)}}</span>

                                </div>
                                <input type="hidden" id="tax" name="tax" value="{{Cart::total()*(!empty($rest_data) ?  $rest_data->tax_rate/100 : 0)+(!empty($rest_data) ?  $rest_data->ZC : 0)}}">
                                <div class="break-down-items" id="delivery_div">
                                    <span>Delivery</span>
                                    <span id="delivery_amount_span">${{number_format($delivery_charge,2)}}</span>
                                </div>
                                 <div class="break-down-items" id="difference_div" style="display: none;">
                                    <span>Minimum Order Difference</span>
                                    <span style="padding-right: 50px;"><a href="JavaScript:Void(0);" class="remove-btn" onclick="remove_difference();"><i class="ion-close-circled" title="Remove this item from the cart"></i></a></span>
                                <span id="difference_amount_span"></span>
                                    <input type="hidden" name="min_order_difference" id="min_order_difference">
                                </div>
                                <div class="break-down-items">
                                    <span>Tip</span>
                                    <span id="tip_charge">$0.00</span>
                                </div>
                                <div class="tip-block">
                                    <input type="hidden" id="tip" name="tip">
                                    <div class="tip-item" style="width: 40px;">
                                        <input type="radio" id="tip_0" name="tip_value" value=0 class="tipcal">
                                        <label for="tip_0">0%</label>
                                    </div>
                                    <div class="tip-item" style="width: 40px;">
                                        <input type="radio" id="tip_5" name="tip_value" value=5 class="tipcal" checked="checked">
                                        <label for="tip_5">5%</label>
                                    </div>
                                    <div class="tip-item" style="width: 40px;">
                                        <input type="radio" id="tip_10" name="tip_value" value=10 class="tipcal">
                                        <label for="tip_10">10%</label>
                                    </div>
                                    <div class="tip-item" style="width: 40px;">
                                        <input type="radio" id="tip_20" name="tip_value" value=20 class="tipcal">
                                        <label for="tip_20">20%</label>
                                    </div>
                                    <div class="tip-item" >
                                        <input type="radio" name="tip" id="custom" value="">
                                        <label for="custom">Custom</label>
                                    </div>
                                    <div class="tip-item" style="width: 60px; height: 30px;">
                                        <input type="number" class="form-control" name="tip_value_cus" id="customper" style="display: none;" min=0 max=60 >
                                    </div>
                                   <!--  <input type="text" class="form-control" placeholder="Enter custom tip" name="tipper" id="customper" style="display: none;"> -->
                                </div>
                                <hr>
                                <div class="break-down-items points">
                                    <span>Points <a href="JavaScript:Void(0);" onclick="redeem()" >Redeem Points</a></span>
                                    <span id="points_redeem">$0.00</span>
                                </div>
                                @if(($rest_data->offers != null))
                                <div class="break-down-items points">
                                    <span>Discount <a href="#" onclick="apply_offer()" id="apply_offer_link" >Apply Offer</a></span>
                                    @if(@$rest_data->offers->type == 'Price')
                                    <span>$ {{$rest_data->offers->value}}</span>
                                    @else
                                    <span>{{@$rest_data->offers->value}} %</span>
                                    @endif
                                </div>
                                @endif
                                <input type="hidden" id="discount" name="discount">
                                <input type="hidden" id="discount_amount" name="discount_amount">
                                <hr>
                                <div class="break-down-items total">
                                    <span>Total Amount</span>
                                    <div class="text-right">
                                        <strike class="mr-5"><small id="original_amount"></small></strike> <b>$</b><span id="total_charge">{{number_format(Cart::total(),2)}}</span>

                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="total_amount" name="total_amount">
                            <input type="hidden" class="form-control" name="payment_response" id="payment_response">
                            <input type="hidden" class="form-control" name="status" id="status">
                        </div>
                    </div>
                    <div class="checkout-block-wrap payment-wrap-main">
                        <div class="block-item div-item">
                            <div class="alert alert-warning" role="alert" id="sectionHead_payment"><span id="section_payment"></span></div>
                            <div class="row">
                                <div class="col-md-12">
                                    
                                   <!--  <h4>Card</h4>
                                    <div class="payment-type">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="number" step="1" class="form-control card_class" placeholder="Card Number*" id="card" name="card" required="required" autocomplete="cc-number">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="number" step="1" max="999" class="form-control cvv_class" placeholder="CVV*" id="cvv" name="cvv" required="required" autocomplete="cc-csc">
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <input type="text" name="cc-exp" required="required" class="form-control" autocomplete="cc-exp" id="exp" placeholder="MM-YYYY*" data-inputmask="'alias': 'date','mask': '99-9999','placeholder' : 'MM-YYYY'" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Zip Code*" id="zip" name="zip_code" autocomplete="shipping postal-code">
                                                </div>
                                            </div>
                                    
                                            <input type="hidden" id="ref_address" name="ref_address" value="{{str_limit($rest_data->address,30)}}">
                                            <input id="token" type="hidden" name="token" > <br />
                                            <input id="merchanttxnid" type="hidden" name="merchanttxnid" value="2108313" /> <br />
                                            <input id="gettoken" type="hidden" name="gettoken" value="y" />
                                            <input id="addtoken" type="hidden" name="addtoken" value="y" />
                                        </div>
                                        <img src="{{url('img/payment-logos.png')}}" style="height: 40px; margin-top: 15px;">
                                    </div> -->
                                    <div id="paypal-button-container"></div>
                                </div>
                            </div>
                                
                        </div>
                        <div class="text-center">
                            <!-- <button class="btn btn-theme" style="min-width: 250px; margin: 0 auto;" type="button" name="submit" onclick="pay();" id="btn_process">Pay Online
                                <i class="fa fa-spinner fa-spin"></i>
                            </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endif

            <div class="modal fade scedule-order-modal" id="schedule_orderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <div class="schedule-inner-block">
                                <div class="form-group">
                                    <p>Select a Delivery Date</p>
                                    <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                        @for($i=0;$i<5;$i++)
                                            <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days'))}}" data-val="{{date('Y-m-d', strtotime(date('D d-M'). ' + '. $i .'days'))}}">
                                                <span class="day">{{date('D', strtotime(date('D d-M'). ' + '. $i .'days'))}}</span>
                                                <span class="date">{{date('d', strtotime(date('D d-M'). ' + '. $i .'days'))}}</span>
                                            </div>
                                        @endfor

                                    </div>
                                </div>
                                <div class="form-group">
                                    <p>Desired Delivery Time</p>
                                    <select class="form-control" id="sch_time">
                                        <option value="">Select a Time</option>
                                        @if(!empty($timings))
                                            @foreach($timings as $time)
                                                @php
                                                    $time1 = $time['opening']; 
                                                    if($time1 ==  '00:00')
                                                    {
                                                         $time1 = date('H:i', (strtotime($time1) + 60*15));
                                                    }
                                                    $time2 = $time['closing']; 
                                                    for($time1;$time1<=$time2; ){ 
                                                    if ($time1 == '00:00') break;
                                                 @endphp
                                                        @if($time1 >= date('H:i', (strtotime(date('Y-m-d')))))
                                                            <option>{{date("h:i A", strtotime($time1))}}</option>
                                                        @endif
                                                        @php
                                                            $time1 = date('H:i', (strtotime($time1) + 60*15));
                                                    }
                                                        @endphp
                                            @endforeach
                                        @endif
                                    </select> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@include('cart::public.address.index')
<div class="modal fade" id="redeem_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Redeem Amount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body redeem-points-modal">
                @include('notifications')
                <p class="mb-0">You have <span>{{Cart::getLoyaltypoints()}}</span> Total Points</p>
                <p>Your points can be redeemed for <span>${{number_format(Cart::getLoyaltypoints()/2000,2)}}</span></p>
                <input type="number" class="form-control mb-15" id="redeem_amount" value="{{Cart::total()}}" min=1>
                <div class="btn-wrap modal-footer justify-content-start">
                    <button class="btn btn-theme" type="button" onclick="apply_redeem()" >Apply</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="SessionModel" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body">
                <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                                <div class="login-header text-center">
                                  <h5 id="error_msg">This Eatery requires a minimum order of $<span id="min_amt"></span></h5>
                                </div>
                                
                                <div class="text-center" >
                                   <button type="button" style="min-width: 100px;" class="btn btn-theme mt-10 search-btn" id="favourite_submit" data-dismiss="modal">Ok</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script
    src="https://www.paypal.com/sdk/js?client-id={{ config('services.paypal.SB_CLIENT_ID') }}"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script> 
 -->
<!--  <script src="https://api.convergepay.com/hosted-payments/Checkout.js"></script>
 --><!-- <script src="{{url('cart/jqueryCheckout.js')}}"></script>
 --><script type="text/javascript">
 
var firstname = '{{@user()->name}}';
var lastname = '{{@user()->name}}';
var flag_qty=1;
var flag_time = 0;
var flag_minorder = 0;
var flag_address = 0;
var restaurant_id = '{!!$rest_data->getRoutekey()!!}';
var s ={!! !Auth::check() ? '1' : '0'!!};
var flag_login = {!!$flag_login!!};
var rest_id = {!!$cart->options->restaurant!!};
var baseUrl ="{{url('/') }}";
var transUrl ="{{trans_url('/') }}";
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
var endDates =  new Date(nowDate.getFullYear(), nowDate.getMonth() +2, +0);
var difference = '{{$rest_data->min_order_amount_delivery-Cart::subtotal()}}';
var delivery_charge = '<?php echo number_format($delivery_charge,2); ?>';
var total = '<?php echo Cart::total(); ?>';
var tip = (total * 5)/100;
var total_amount = Number($('#total_charge').text());
var type = '{{@$rest_data->offers->type}}';
var offer = '{{@$rest_data->offers->value}}';
var limit = '{{@$rest_data->offers->limit_amount}}';
var client_email = '{{user()->email}}';
var client_mobile = '{{user()->mobile}}';
var client_name = '{{user()->name}}';
document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
document.getElementById("tip").value=tip;
var grandtotal = '<?php echo Cart::total() + (!empty($rest_data) ? ($rest_data->tax_rate / 100) * Cart::total() : 0) + (!empty($rest_data) ? $rest_data->ZC : 0); ?>';
document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(tip));

document.getElementById("total_charge").textContent=('<?php echo number_format(Cart::total() + (!empty($rest_data) ? ($rest_data->tax_rate / 100) * Cart::total() : 0) + (!empty($rest_data) ? $rest_data->ZC : 0) + (5 / 100) * Cart::total(), 2); ?>');

var min_order_amount_pickup = {!!(!empty($rest_data->min_order_count) ? $rest_data->min_order_count : 0)!!};
var min_order_amount_delivery = {!!(!empty($rest_data->min_order_amount_delivery) ? $rest_data->min_order_amount_delivery : 0)!!};
var today = '{{date('D d-M')}}';
var delivery_charge = {{empty($delivery_charge) ? 0 : $delivery_charge}};
var tot_points = '<?php echo Cart::getLoyaltypoints(); ?>';
var cart_total = '<?php echo Cart::total(); ?>';

if($('input[name="order_type"]:checked').val() == 'Pickup')
        {
            var min_order_amount = {!!(!empty($rest_data->min_order_count) ? $rest_data->min_order_count : 0)!!};
            // $('#difference_amount_span').text('$'+{!!number_format($rest_data->min_order_count-Cart::subtotal(),2)!!});
            // $('#min_order_difference').val({!!number_format($rest_data->min_order_count-Cart::subtotal(),2)!!});
            // var difference = {!!$rest_data->min_order_count-Cart::subtotal()!!};
        }
        else{
            var min_order_amount = {!!(!empty($rest_data->min_order_amount_delivery)? $rest_data->min_order_amount_delivery : 0)!!};
        //     $('#difference_amount_span').text('$'+{!!number_format($rest_data->min_order_amount_delivery-Cart::subtotal(),2)!!});
        //     $('#min_order_difference').val({!!number_format($rest_data->min_order_amount_delivery-Cart::subtotal(),2)!!});
        // var difference = {!!$rest_data->min_order_amount_delivery-Cart::subtotal()!!};
        }
// var geocoder = new google.maps.Geocoder();
//     google.maps.event.addDomListener(window, 'load', function () {
//         var places = new google.maps.places.Autocomplete(document.getElementById('ref_address'));
       

//         google.maps.event.addListener(places, 'place_changed', function () {
//                 geocodeAddress(geocoder);
//         });

//         google.maps.event.addListener(billing_places, 'place_changed', function () {
//                 geocodeAddress1(geocoder);
//         });
        
//     });
var totalAmount = parseFloat($('#total_amount').val()).toFixed(2);
    $('#total_amount').change(function(){
        totalAmount = parseFloat($(this).val()).toFixed(2);
    });
  $(document).ready(function(){
    var cartSummary_Div = $('#cart_SummaryWrap').height();
    $('#checkout_wrapMain').css('min-height', cartSummary_Div+'px');

    paypal.Buttons({
      createOrder: function(data, actions) {
    return actions.order.create({
      intent: 'CAPTURE',
      payer: {
        name: {
          given_name: client_name,
          surname: client_name
        },
        address: {
          address_line_1: '123 ABC Street',
          address_line_2: 'Apt 2',
          admin_area_2: 'San Jose',
          admin_area_1: 'CA',
          postal_code: '95121',
          country_code: 'US'
        },
        email_address: client_email,
        phone: {
          phone_type: "MOBILE",
          phone_number: {
            national_number: client_mobile
          }
        }
      },
      purchase_units: [
        {
          amount: {
            value: parseFloat($('#total_amount').val()).toFixed(2),
            currency_code: 'USD'
          }
        }
      ],
      application_context: {
        shipping_preference: 'NO_SHIPPING'
      }
    });
  },
    onApprove: function(data, actions) {
        totalAmount = parseFloat($('#total_amount').val()).toFixed(2);
      // This function captures the funds from the transaction.
    console.log(data);
    

      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        console.log(details);

    var $f = $('#frm_checkout');
    console.log($f.serialize()); 

        
        return  fetch('{{trans_url("cart/paypalsubmit")}}', {
          method: 'post',
          headers: {
            'content-type': 'application/json'
          },
          body: JSON.stringify({
            orderId: data.orderID,
            amount: totalAmount,
            currency: 'USD',
            data: $f.serialize()
          }),
        }).then((response) => response.json())
    .then((responseData) => {
      console.log(responseData);
      if(responseData.Status == 'COMPLETED'){
        window.location = baseUrl + "/success"+"/"+responseData.order_id;
      }
      // return responseData;
    })
    .catch(error => console.warn(error));
      });
    }
}).render('#paypal-button-container');
  });
 </script>
<style type="text/css">
    .cvv_class::-webkit-inner-spin-button, 
.cvv_class::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.card_class::-webkit-inner-spin-button, 
.card_class::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    height: 25px;
    margin-left: 80px;
    width: 50px;
    z-index: 9999;
    background: url('{{theme_asset('img/31.gif')}}') center no-repeat #40b659;
}
</style>
