@if(Cart::count() <= 0)
<?php $rest_id   = 0;
$delivery_charge = 0;?>
<div class="empty-content-wrap">
    <div class="container">
        <img src="{{theme_asset('img/empty-cart.png')}}" alt="">
        <h2>Your Cart is Empty</h2>
        <p>Looks like you haven't made your choice yet</p>
        <p><a href="{{url('/')}}">Go Home</a></p>
    </div>
</div>
@else

<section class="checkout-wrap">
    <div class="container">
        @foreach($carts as $cart)
            <?php $rest_id = $cart->options->restaurant;
                $rest_data                             = Restaurant::getRestaurantData($rest_id);
            ?>
        @endforeach

        <div class="row row-checkout" id="checkout_wrapMain">
            <div class="col-md-8 position-static">
                                    <form id="frm_checkout" action="{{url('cart/paymentsubmit')}}" method="POST">

                <div class="checkout-block-wrap pb-0">
                    

                    <div class="block-item item-login customer-entry">
                        <a href="{{trans_url('restaurants/'.$rest_data->slug)}}"  class="checkout-back-btn d-none d-md-block" data-toggle="tooltip" data-placement="top" title="Back to Eatery Page" data-original-title="Go Back" id="btn_back1" ><i class="ion-android-arrow-back" style="display: inline-block;
                            width: 35px;
                            height: 35px;
                        /*    background-color: #ededed;
                        */    border-radius: 50%;
                            text-align: center;
                            line-height: 35px;
                            color: #333;
                            margin-right: 10px;"></i></a>
                        <div class="row" >
                            <div class="col-md-12">

                                @if(Auth::user())
                                <div class="login-card">
                                    @if(!empty(user()->mobile))
                                        <?php $flag_login=0; ?>
                                        <h3><span>{{user()->name}}</span>'s Order</h3>
                                        <p>Not you? <a href="#" id="logout-customer"> Click here to logout</a></p>
                                    @else
                                    <?php $flag_login=1; ?>
                                    <h3><span>{{user()->name}}</span>'s Order</h3>
                                        <p>Not you? <a href="#" id="logout-customer"> Click here to logout</a></p>
                                        <div class="alert alert-warning" role="alert" id="sectionHead_login" style="display: none;"><span id="section_login"></span></div>
                                        *This is the phone number the eatery will call in case of any issue regarding your order
                                        <div class="form-group">
                                            {!! Form::tel('mobile')
                                                                ->required()
                                                                ->label('')
                                                                ->id('client_mobile')
                                                                ->placeholder('Phone Number') !!}
                                                                
                                        </div>
                                        <button class="btn btn-theme mr-10" style="min-width: 100px;" id="mobile_submit">Confirm Phone</button>
                                    @endif
                                </div>
                                @else

                                <?php $flag_login=1; ?>

                                <h3>Sign In</h3>
                        <div class="row ">
                            <div class="col-md-5">
                                <div class="social-btn-wrap">
                                    <a href="/client/login/facebook" class="btn btn-block btn-facebook"><i class="fa fa-facebook mr-5"></i> Sign in with Facebook</a>
                                    <a href="/client/login/google" class="btn btn-block btn-google"><i class="fa fa-google mr-5"></i> Sign in with Google</a>
                                    <div id="changable_login_div"><a href="#" class="btn btn-theme" id="collapseGuest" style="margin-top: 10px;width: 100%;background-color: #999;">GUEST CHECKOUT</a></div>
                                </div>
                            </div> <div class="col-md-1">
                                <h3 class="login-sep"><span>OR</span></h3>
                            </div>
                          
                            <div class="col-md-6" id="login_sec">
                          {!!Form::vertical_open()
    ->id('login-customer')
    ->action(trans_url('client/login'))
    ->method('POST')!!}
    @include('notifications') 
                            <div class="col-md-12">
                                <div class="login-wrap">
                                    <div class="form-group">
                                        {!! Form::email('email')
                                ->required()
                                ->placeholder('Email')->raw() !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::password('password')
                                ->placeholder('Password')
                                ->required()->raw()!!}
                                    </div>
                                    <center><button class="btn btn-theme mr-10" style="min-width: 100px;" id="login_submit">Login</button>
                                    <a href="#" class="btn btn-link" id="collapseRegister">Sign up</a>
                                    <p class="text-small mt20">I forgot my password - <a href="{{url('client/password/reset')}}" target="_blank">Reset Here</a></p>
                                    </center>
                                    <!-- <button class="btn btn-theme" id="collapseGuest" style="margin-top: 10px;width: 100%;background-color: #999;">GUEST CHECKOUT</button></center> -->
                                </div>
                            </div>
                             {!! Form::close() !!}
                             
                         </div>
                        </div>
                                @endif
                            </div>
                        </div>
                        
                    </div>  
                        
                        
                        <div class="payment-wrap div-item"  style="display:none" id="cart_SummaryWrap">
                          
                            <hr>
                       
                    </div>
                <div class="checkout-block-wrap payment-wrap-main" style="display:none">
                        <div class="block-item div-item">
                            <div class="alert alert-warning" role="alert" id="sectionHead_payment"><span id="section_payment"></span></div>
                            <div class="row">
                                
                                <div class="col-md-8">
                                    
                                    <div class="payment-type">
                                        <div class="row">
                                            
                                           
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <input type="text" name="cc-exp" required="required" class="form-control" autocomplete="cc-exp" id="exp" placeholder="MM-YYYY*" data-inputmask="'alias': 'date','mask': '99-9999','placeholder' : 'MM-YYYY'" />
                                                </div>
                                            </div>
                                          
                                     
                                        </div>
                                       
                                    </div>



                                    
                                </div>
                            </div>
                                
                        </div>
                        
                    </div>
                </div>
            </div>
        </form>
    </section>
@endif

            


<!-- <script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script> 
 -->
<!--  <script src="https://api.convergepay.com/hosted-payments/Checkout.js"></script>
 --><!-- <script src="{{url('cart/jqueryCheckout.js')}}"></script>
 --><script type="text/javascript">
 
var firstname = '{{@user()->name}}';
var lastname = '{{@user()->name}}';
var flag_qty=1;
var flag_time = 0;
var flag_minorder = 0;
var flag_address = 0;
var restaurant_id = '{!!$rest_data->getRoutekey()!!}';
var s ={!! !Auth::check() ? '1' : '0'!!};
var flag_login = {!!$flag_login!!};
var rest_id = {!!$cart->options->restaurant!!};
var baseUrl ="{{url('/') }}";
var transUrl ="{{trans_url('/') }}";
var nowDate = new Date();
var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
var endDates =  new Date(nowDate.getFullYear(), nowDate.getMonth() +2, +0);
var difference = '{{$rest_data->min_order_amount_delivery-Cart::subtotal()}}';
var total = '<?php echo Cart::total(); ?>';
var tip = (total * 15)/100;
var total_amount = Number($('#total_charge').text());
var type = '{{@$rest_data->offers->type}}';
var offer = '{{@$rest_data->offers->value}}';
var limit = '{{@$rest_data->offers->limit_amount}}';
var client_email = '{{user()->email}}';
var client_mobile = '{{user()->mobile}}';

 </script>
