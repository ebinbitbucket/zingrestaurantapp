<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Rancho" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato', sans-serif;
            font-weight: 300;
            font-size: 15px;
        }
    </style>
  </head>

    <body style="background-color: #f1eff2;">
        <table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: #fff; height: 100%; width: 600px; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.10);">
            <tbody>
                <tr>
                    <td align="center">
                       <div style="background-color: #40b659; text-align: center; padding: 30px 30px;">
                           <img src="{{url('img/logo-round-white.png')}}" style="height: 80px; display: inline-block;" alt="">
                           <h1 style="color: #fff; margin-top: 10px; margin-bottom: 0px; font-size: 24px;">Your Order is being Prepared!</h1>
                           <p style="font-size: 14px; color: #fff; margin-bottom: 0px;">Your order has been acknowledged by the Zing Eatery Partner and will be ready at the provided time below. Enjoy your food!</p>
                           <!--<a href='{{ trans_url("client/cart/order/feedback/".@$order->getRoutekey()) }}'>Click here to give feedback for your order</a>-->
                           

                       </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px; text-align: center;">
                       <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0; text-align: center;">
                            <tr>
                                <td>
                                    <p style="margin: 0;">
                                        <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 5px; display: block; text-transform: capitalize;"><b>#{!!@$order['id']!!}  {{$user->name}}</b></span>
                                        <span style="font-size: 20px; color: #666666; display: block; margin-bottom: 5px; text-transform: capitalize;"><b>{!!@$order['order_type']!!} Order</b></span>
                                        <span style="font-size: 16px; color: #666666; display: block; text-transform: capitalize;">{!!@$order['order_type']!!} Time: <img src="{{url('img/calendar.png')}}" style="display: inline-block; height: 15px; vertical-align: -2px;" alt=""> 
                                  


                             
                                   
                                    {{date("D, M d, h:i A",strtotime(@$order->ready_time))}}

                                        </span>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
                        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0; text-align: left;">
                            <tr>
                                <td width="115px">
                                    <div style="width: 100px; height: 100px; border-radius: 50%; display: inline-block; margin-right: 15px; background-image: url('{{url($order->restaurant->defaultImage('logo'))}}'); background-size: contain; background-position: center;"></div>
                                </td>
                                <td>
                                    <p style="margin: 0;">
                                        <span style="font-size: 18px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>{!!@$order->restaurant->name!!}</b></span>
                                        <span style="font-size: 16px; color: #666666; display: block; margin-bottom: 5px;"><img src="{{url('img/place.png')}}" style="display: inline-block; height: 15px; vertical-align: -2px;"> {{$order->restaurant->address}} <img src="{{url('img/phone.png')}}" style="display: inline-block; height: 15px; vertical-align: -2px; margin-left: 10px;"> {{$order->restaurant->phone}}</span>
                                        <span style="font-size: 16px; color: #666666; display: block;">
                                        	@foreach($restaurant_timings as $key => $val) 
                                                @if($val->day == $weekday && $val->opening != 'off' && $val->opening != 'closing')
                                                <span class="hours">{{date('g:i a',strtotime($val->opening))}} - {!!date('g:i a',strtotime($val->closing))!!}</span>
                                                @endif
                                            @endforeach
                                        </span>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
                        <h3 style="margin: 0; font-size: 18px; color: #40b659;">Loyality Points {{number_format(Cart::getLoyaltypoints($user->id),2)}}</h3>                       
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
                        <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>Order Summary</b></span>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 20px; padding-top: 30px;">
                            <tbody>
                            	@foreach($order_detail as $detail)
                            	<?php

                                    $addon_price = 0;

                                    if (!empty($detail->addons)) {

                                        foreach ($detail->addons as $cart_addon) {

                                            $addon_price = $addon_price + $cart_addon[2];
                                        }

                                    }

                                ?>
                                <tr style="text-align: left;">
                                    <td valign="top" width="10%" style="color: #666666; line-height: 24px; font-size: 18px; border-bottom: 1px solid #eaeaea;">{{$detail->quantity}}x</td>
                                    <td width="75%" style="color: #666666; line-height: 24px; font-size: 16px; padding-bottom: 5px; border-bottom: 1px solid #eaeaea;"><b>@if($detail->menu_addon == 'menu' && (!empty($detail->menu)))
                                            {{@$detail->menu->name}} 
                                        @elseif(!empty($detail->addon))
                                            {{@$detail->addon->name}}  
                                        @endif<br>
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: diamond; font-size: 14px;">
                                            @if(!empty($detail->addons))
                                                @foreach($detail->addons as $cart_addon)
                                                  <li>  {{$cart_addon[1]}} </li>
                                                @endforeach
                                            @endif
                                        </ul></br>
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: circle; font-size: 14px;">
                                        	@if(!empty($detail->options['special_instr']))
                                            <li>{{$detail->options['special_instr']}}</li>
                                            @endif
                                        </ul>
                                    </td>
                                    <td valign="top" width="10%" style=" color: #666666; line-height: 24px; font-size: 18px; text-align: right; border-bottom: 1px solid #eaeaea;">${{number_format($detail->quantity*$detail->unit_price+$addon_price,2)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 0px;">
                            <tbody>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Subtotal</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['subtotal'],2)!!}</td>
                                </tr>
                                @if(@$order['tax']>0)
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Tax & Fees</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['tax'],2)!!}</td>
                                </tr>
                                @endif
                                @if(@$order['delivery_charge']>0)
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Delivery</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['delivery_charge'],2)!!}</td>
                                </tr>
                                @endif
                                @if(@$order['tip']>0)
                                <tr style="text-align: left;">
                                    <td width="65%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="25%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Tip</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['tip'],2)!!}</td>
                                </tr>
                                @endif
                                @if(@$order['discount_points']>0)
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">Loyality Points</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 10px; font-size: 18px; text-align: right;">${!!number_format(@$order['discount_points']/2000,2)!!} ({{@$orders->loyalty_points}} Points)</td>
                                </tr>
                                @endif
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #333333; padding-bottom: 5px; font-size: 16px; font-weight: 600;">Total</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right; font-weight: 600;">${!!number_format(@$order['total'],2)!!}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding: 20px;border-bottom: 1px solid #d6d8de; text-align: center;">
                        @if(@$order['order_type'] == 'Pickup')
                        @if(@$order['car_pickup'] == 1)
                        <a style="background-color: #40b659; display: inline-block; padding: 10px 20px; border-radius: 30px; text-decoration: none; color: #fff; font-size: 14px; font-weight: 400;" href="{{trans_url('order/car-delivery/'.@$order->getRoutekey())}}">I AM Here</a>
                        <p>Click on “I am Here” button to let the Restaurant know that you have arrive to pick up your food.</p>
                        @endif
                        @endif
                    </td>
                </tr>
                <tr>
                    <td align="center" width="310" style="padding: 20px; background-color: #eff1f2;">
                        <span style="font-size: 14px;">
                            <span style="color: #999999; margin-bottom: 5px; display: block;">© 2019 ZingMyOrder LLC | 1039 I-35E Suite 304, Carrollton, TX 75006</span>
                            <a href="{{url('restaurant/login')}}" style="text-decoration: none; color: #999999; display: inline-block;">Be a Eatery Partner</a><span style="color: #999999;">&nbsp; | &nbsp;</span>
                            <a href="{{url('privacy.html')}}" style="text-decoration: none; color: #999999; display: inline-block;">Privacy Policy</a><span style="color: #999999;">&nbsp; | &nbsp;
                            <a href="{{url('terms-and-conditions.html')}}" style="text-decoration: none; color: #999999; display: inline-block;">Terms & Conditions</a></span>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>

    </body>
</html>
