<!doctype html>
<html class="no-js" lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
        <style>
            body {
                font-family: 'Lato', sans-serif;
                font-weight: 300;
                font-size: 15px;
            }
        </style>
    </head>

    <body style="background-color: #f1eff2;">
        <table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: #fff; height: 100%; width: 600px; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.10);">
            <tbody>
                <tr>
                    <td align="center">
                       <div style="background-color: #40b659; text-align: center; padding: 30px 30px;">
                           <img src="{{url('img/logo-round-white.png')}}" style="height: 80px; display: inline-block;" alt="">
                          <!--  <h1 style="color: #fff; margin-top: 10px; margin-bottom: 10px; font-size: 24px;">Hope you’re having a HAPPY ZING day!</h1> -->
                           <p style="font-size: 16px; color: #fff; margin-bottom: 0px;">We thought we'd let you know that re-ordering from your favourite eatery is as easy as clicking the button below. Have a Happy Zing Day!</p>
                           <a class="btn btn-secondary btn-theme btn-sm mt-10" style="background-color: #fff;color: #40b659;margin-top: 10px;" href="{{trans_url('client/cart/reorder/'.@$order->getRoutekey())}}">RE-ORDER</a>
                       </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
                         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0; text-align: center;">
                            <tr>
                                <td width="115px">
                                    <!--<img src="{{url(@$order->restaurant->defaultImage('logo'))}}">-->
                                    <div style="width: 100px; height: 100px; border-radius: 50%; display: inline-block; margin-right: 15px; background-image: url('{{url(@$order->restaurant->defaultImage('logo'))}}'); background-size: contain; background-position: center;"></div>
                                </td>
                                <td>
                                    <p style="margin: 0;">
                                        <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>Order Details</b></span>
                                        <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">#{!!@$order['id']!!}</span>
                                        <span style="font-size: 18px; color: #666666; display: block;">{!!@$order->restaurant->name!!}</span>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
                        <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>Order Summary</b></span>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 20px; padding-top: 30px;">
                            <tbody>
                                @foreach($order_detail as $detail)
                                <?php
                                    $variation = $detail->variation; 
                                     $addon_price = 0;

                                     if (!empty($detail->addons)) {

                                         foreach ($detail->addons as $cart_addon) {

                                             $addon_price = $addon_price + $cart_addon[2];
                                         }

                                     }

                                 ?>
                                <tr style="text-align: left;">
                                    <td valign="top" width="10%" style="color: #666666; line-height: 24px; font-size: 18px; border-bottom: 1px solid #eaeaea;">{{$detail->quantity}}x</td>
                                    <td width="75%" style="color: #666666; line-height: 24px; font-size: 16px; padding-bottom: 5px; border-bottom: 1px solid #eaeaea;"><b>@if($detail->menu_addon == 'menu' && (!empty($detail->menu)))
                                            {{@$detail->menu->name}} 
                                        @elseif(!empty($detail->addon))
                                            {{@$detail->addon->name}}  
                                        @endif</b><br>
                                        @if(!empty($variation))
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: diamond; font-size: 14px;">
                                                 Variation- {{$variation}}
                                        </ul>
                                        @endif
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: diamond; font-size: 14px;">
                                            @if(!empty($detail->addons))
                                                @foreach($detail->addons as $cart_addon)
                                                  <li>  {{$cart_addon[1]}} </li>
                                                @endforeach
                                            @endif
                                        </ul></br>
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: circle; font-size: 14px;">
                                            @if(!empty($detail->options['special_instr']))
                                            <li>{{$detail->options['special_instr']}}.</li>
                                            @endif
                                        </ul>
                                    </td>
                                    <td valign="top" width="10%" style=" color: #666666; line-height: 24px; font-size: 18px; text-align: right; border-bottom: 1px solid #eaeaea;">${{number_format($detail->quantity*$detail->unit_price+$addon_price,2)}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 0px;">
                            <tbody>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Subtotal</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['subtotal'],2)!!}</td>
                                </tr>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Tax & Fees</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['tax'],2)!!}</td>
                                </tr>
                                @if(@$order['order_type'] == 'Delivery')
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Delivery</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['delivery_charge'],2)!!}</td>
                                </tr>
                                @endif
                                 @if(@$order['min_order_difference'] >0)
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Minimum Order Difference</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['min_order_difference'],2)!!}</td>
                                </tr>
                                @endif
                                @if(@$order['tip']>0)
                                <tr style="text-align: left;">
                                    <td width="65%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="25%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Tip</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['tip'],2)!!}</td>
                                </tr>
                                 @endif
                                @if(@$order['discount_amount'] >0)
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">Discount</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 10px; font-size: 18px; text-align: right;">{!!number_format(@$order['discount_amount'],2)!!}</td>
                                </tr>
                                @endif
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">Loyality Points</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 10px; font-size: 18px; text-align: right;">{!!number_format(@$order['discount_points']/2000,2)!!} ({{@$order->loyalty_points}} Points)</td>
                                </tr>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #333333; padding-bottom: 5px; font-size: 16px; font-weight: 700;">Total</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right; font-weight: 700;">${!!number_format(@$order['total'],2)!!}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                 <tr>
                    <td align="center" style="padding: 20px;border-bottom: 1px solid #d6d8de;">
                         <p style="font-size: 18px; color:#666666; margin-top: 10px; margin-bottom: 10px;">You earned <span style="color:#2dce89;">{{@$order->loyalty_points}}</span> points for this order.</p>
                        <p style="font-size: 18px; color:#666666; margin-top: 10px; margin-bottom: 10px;">You total Loyalty Points is <span style="color:#2dce89;">{{number_format(Cart::getLoyaltypoints(),2)}}</span></p>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding: 20px;">
                        <img src="{{url('img/icon-2.png')}}" width="60px" height="60px">
                        <h1 style="font-size: 18px; color:#666666; margin-top: 10px; margin-bottom: 10px;">Need Order Help?</h1>
                        <p style="font-size: 16px; color: #666666; margin-bottom: 10px; margin-top: 0px;">Get Order Help at support@zingmyorder.com</p>
                        <p style="font-size: 16px; color: #666666; margin-bottom: 10px; margin-top: 0px;">Ph: {{substr(9705285689, 0,3)}}-{{substr(9705285689, 3,3)}}-{{substr(9705285689, 6,4)}}</p>
                        <!--<a href="#" style="background-color: #40b659; color: #fff; padding: 10px 20px; font-size: 14px; text-decoration: none; border-radius: 30px; display: inline-block;">Get Order Help</a>-->
                    </td>
                </tr>
                <tr>
                    <td align="center" width="310" style="padding: 20px; background-color: #eff1f2;">
                        <span style="font-size: 14px;">
                            <span style="color: #999999; margin-bottom: 5px; display: block;">© 2019 ZingMyOrder LLC | 1039 I-35E Suite 304, Carrollton, TX 75006</span>
                            <a href="{{url('restaurant/login')}}" target="_new" style="text-decoration: none; color: #999999; display: inline-block;">Be a Eatery Partner</a><span style="color: #999999;">&nbsp; | &nbsp;</span>
                            <a href="{{url('privacy.html')}}" target="_new" style="text-decoration: none; color: #999999; display: inline-block;">Privacy Policy</a><span style="color: #999999;">&nbsp; | &nbsp;
                            <a href="{{url('terms-and-conditions.html')}}" target="_new" style="text-decoration: none; color: #999999; display: inline-block;">Terms & Conditions</a></span>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>
    </body>
</html>
