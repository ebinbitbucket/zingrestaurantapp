@if(Cart::count() <= 0)
<?php $rest_id=0;
$delivery_charge=0;?>
<div class="empty-content-wrap">
    <div class="container">
        <img src="{{theme_asset('img/cart-empty.svg')}}" alt="">
        <h2>Your Cart is Empty</h2>
        <p>Looks like you haven't made your choice yet</p>
        <p><a href="{{url('/')}}">Go Home</a></p>
<!--         <p><a href="#" id="prev_res">Go To Eatery Page</a></p>
 -->
    </div>
</div>
@else
<section class="checkout-wrap" style="margin-top: 76px;">
    
    <div class="container">
                        <form id="frm_checkout" action="{{url('cart/delivery')}}">
<div class="row">
            <div class="col-md-8">
                    <div class="checkout-block-wrap">
                        <div class="block-item row"> 
                            @foreach($carts as $cart) 
                            <?php $rest_id = $cart->options->restaurant;
                                $rest_data = Restaurant::getRestaurantData($rest_id);
                            ?>
                            @endforeach 
                              <!--   <a href="#"  class="checkout-back-btn" data-toggle="tooltip" data-placement="top" title="Back to Orders" data-original-title="Go Back" id="btn_back1" ><i class="ion-android-arrow-back"></i></a> -->
                                <a href="{{trans_url('restaurants/'.$rest_data->slug)}}"  class="checkout-back-btn" data-toggle="tooltip" data-placement="top" title="Back to Orders" data-original-title="Go Back" id="btn_back1" ><i class="ion-android-arrow-back"></i></a>
                                 <a href="javascript:void(0);" class="checkout-back-btn" data-toggle="tooltip" data-placement="top" title="Back to Orders" data-original-title="Go Back" id="btn_back" style="display:none;"><i class="ion-android-arrow-back"></i></a>
                                <div class="col-md-8 block-item-inner mb-20">
                                    <div class="row">
                                        <div class="col-md-6">
                                          
                                <h3 class="mb-20">Order Type</h3>
  </div>
                            <div class="col-md-6">
                                 <select class="form-control" id="order_type" name="order_type" onchange="typeChange();">
                                     <option>Pickup</option>
                            @if(!empty($rest_data)) 
                                @if($rest_data->delivery == 'Yes')<option>Delivery</option>@endif
                            @endif

                                 </select>
                            </div></div>
                    </div>
                                <div class="col-md-8 block-item-inner">
                                    <div class="row">
                                        <div class="col-md-6">
                                    <h3 class="mb-20">Time</h3>
                                    </div>
                                <div class="col-md-6">
                                    <div class="dropdown deltype-dropdown">
                                        <button class="btn dropdown-toggle btn-block text-left" type="button" id="deltypeMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span id="del_type_v">ASAP</span>
                                            <span id="deltypet_wrap" style="display: none;">
                                                <span id="del_date_v"></span> 
                                                <span id="del_time_v"></span>
                                            </span>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="deltypeMenu">
                                           <div class="delivery-type-item">
                                                <input type="radio" name="del_type" value="ASAP" id="asap" checked="">
                                                <label for="asap"><i class="ik ik-clock"></i>ASAP</label>
                                            </div>
                                            <div class="delivery-type-item">
                                                <input type="radio" name="del_type" id="schedule_order" value="Schedule Order">
                                                <label for="schedule_order"><i class="ik ik-calendar"></i>Schedule Order</label>
                                                <div class="schedule-inner-block" id="scedule-block">
                                                    <div class="form-group">
                                                        <p>Select Date</p>
                                                        <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                                            @for($i=0;$i<5;$i++)
                                                            <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days'))}}" data-val="{{date('Y-m-d', strtotime(date('D d-M'). ' + '. $i .'days'))}}">
                                                                <span class="day">{{date('D', strtotime(date('D d-M'). ' + '. $i .'days'))}}</span>
                                                                <span class="date">{{date('d', strtotime(date('D d-M'). ' + '. $i .'days'))}}</span>
                                                            </div>
                                                            @endfor
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <p>Select Time</p>

                                                        <select class="form-control" id="sch_time">
                                                            <option value="">Select a Time</option>
                                                            @if(!empty($timings))
                                                            @foreach($timings as $time)
                                                                @php
                                                                    $time1 = $time['opening']; 
                                                                    $time2 = $time['closing']; 
                                                                    for($time1;$time1<=$time2; ){ 
                                                                    if ($time1 == '00:00') break;
                                                                @endphp
                                                                    @if($time1 >= date('H:i', (strtotime(date('Y-m-d')))))
                                                                        <option>{{date("h:i A", strtotime($time1))}}</option>
                                                                    @endif
                                                                @php
                                                                    $time1 = date('H:i', (strtotime($time1) + 60*15));
                                                                    }
                                                                @endphp
                                                            @endforeach
                                                            @endif

                                                        </select>
                                                    </div>
                                                    <button type="button" class="btn btn-xs close-dropdown" style="color: #fff; background-color: #6c757d;">Schedule Order</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <input type="hidden" name="scheduled_date" id="datecheck">
                                    <input type="hidden" name="scheduled_time" id="timecheck">
                                    
                                </div>
                               
</div></div>
                                                    
                        </div>
                        <div class="block-item row" id="delivery_address_block">
                            <div class="col-md-12 block-item-inner">
                                <div class="row">
                                    <div class="col-md-4">
                                    <h3 class="mb-20">Delivery Address</h3>
                                    </div>
                                <div class="col-md-8">
                                    <div class="address-block" style="display: none;" id="address_delivery">
                                       @include('cart::public.cart.address.delivery.index')
                                    </div>
                                         <div class="address-block" id="address_pickup">
                                        <div class="saved-address">
                                            <div class="address-item">
                                                
                                                <label for="label_address_pickup">
                                                    <h3>Pickup Address</h3>
                                                    <div class="restaurant-info-item">
                                                        @if(!empty($rest_data->logo))
                                                        <div class="rest-logo" style="background-image: url('{{trans_url('image/original/'.$rest_data->logo[0]['path'])}}')"></div>
                                                        @endif
                                                        <div class="rest-info">
                                                            <p>{{!empty($rest_data) ? $rest_data->address : ''}}</p>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    </div></div>
                                    <input type="hidden" id="latitude_search">
                                    <input type="hidden" id="longitude_search">
                                    <input type="hidden" name="cus_name" class="form-control"  placeholder="Full Name" required="required" value="{{user()->name}}">
                                    <input type="hidden" name="phone" class="form-control"  placeholder="Phone Number" required="required" value="{{user()->mobile}}" pattern="[0-9]{10}">
                                </div>
                        </div>
                        <div class="block-item row" id="billing_block">
                            <div class="col-md-12 block-item-inner">
                                 <div class="row">
                                    <div class="col-md-4">
                                        <h3 class="mb-20">Billing Address</h3>
                                    </div>
                                    <div class="col-md-8">
                                    <div class="address-block">
                                       @include('cart::public.cart.address.billing.index')
                                   </div>
                                    
                               </div> 

                                        </div>
                                    </div>
                                </div>
                            <div id="card_det"></div>
                            <!-- <div class="row">
                                <div class="col-md-4"></div>
                               <div class="col-md-6"><a href="" id="edit_orders" value="online" class="btn btn-default btn-block mt-20 " style="display: none;">Edit Order</a></div>
                              </div> -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="payment-wrap" id="payment_summary">
                        <div class="order-summary">
                             @foreach($carts as $cart) 
                            <?php $rest_name = $cart->options->restaurantname;
                                  $rest_logo = $cart->options->restaurantimage; ?>
                            @endforeach 

                            <div class="order-restaurant">
                                @if(!empty($rest_logo))
                                <div class="rest-img" style="background-image: url('{{trans_url('image/original/'.$rest_logo[0]['path'])}}')"></div>
                                @endif
                                <div class="rest-info">
                                    <h3><a href="{{trans_url('restaurants/'.$rest_data->slug)}}" style="color: #3e5569;">{{$rest_name}}</a></h3>
                                    <p>{{!empty($rest_data) ? $rest_data->address : ''}}</p>
                                </div>
                                
                            </div>
                                                       
                            <h3>Summary</h3>
                            <?php $delivery_charge =0; ?>
                            @foreach($carts as $cart) 
                            <div class="item" style="{{$cart->options['menu_addon'] == 'addon' ? 'margin-left: 20px' : ''}}">
                                <div class="item-name-price">
                                     <?php 
                                        $addon_price = 0;
                                            if(!empty($cart->options['addons'])){
                                        foreach($cart->options['addons'] as $cart_addon){
                    
                                                $addon_price = $addon_price + $cart_addon[2];
                                        }
                                                       
                                       }
                                    ?>
                                    <h4 class="name">{{$cart->name}}</h4>
<!--                                     <div class="price">${{number_format(($cart->price+$addon_price)*$cart->qty,2)}}</div>
 -->                                </div>
                                @if(!empty($cart->options['addons']))
                                @foreach($cart->options['addons'] as $cart_addon)
                                <div class="item-addon">{{$cart_addon[1]}}</div>
                                @endforeach
                                @endif
                                @if($cart->options['menu_addon'] != 'addon')
                                <!--<div class="item-price">-->
                                <!--    <div class="row">-->
                                <!--        <div class="col-7">-->
                                <!--    x<span> -->
                                <!--        <span class="enumerator">-->
                                <!--            <span class="enumerator_btn js-minus_btn" onclick="decrement('{{$cart->rowId}}')" id="minus_span">-</span>-->
                                <!--            <input type="number" name="quantity[{{$cart->rowId}}]" value="{{$cart->qty}}" class="enumerator_input" style="width: 30px;" readonly="readonly">-->
                                <!--            <span class="enumerator_btn js-plus_btn"  onclick="increment('{{$cart->rowId}}')" id="plus_span">+</span>-->
                                            
                                <!--        </span>-->
                                <!--    - </span>-->
                                 <div class="item-price">
                                    <div class="qty-price-wrap">
                                        <div class="qty-price">
                                            <span class="enumerator">
                                                <span class="enumerator_btn js-minus_btn" onclick="decrement('{{$cart->rowId}}')" id="minus_span">-</span>
                                                <input class="enumerator_input" type="number" name="quantity[{{$cart->rowId}}]" value="{{$cart->qty}}" readonly="readonly">
                                                <span class="enumerator_btn js-plus_btn"  onclick="increment('{{$cart->rowId}}')" id="plus_span">+</span>
                                                
                                            </span>
                                   
                                    </div><span>${{number_format($cart->price+$addon_price,2)}}</span>
                                    <a href="{{url('carts/remove/'. $cart->rowId)}}"class="remove-btn"><i class="ion-close-circled" title="Remove this item from the cart"></i></a>
                            </div>
                                </div>@endif
                            </div>
                            <?php 
                            if($cart->options['delivery_charge'] != '')
                            { $delivery_charge = $cart->options['delivery_charge']; }
                            ?>
                            @endforeach
                        </div>
                        <hr>
                        <div class="payment-breakdown-wrap mt-20">
                            <div  id="payment_details">
                            <div class="break-down-items">
                                <span>Subtotal</span>
                                <span>${{number_format(Cart::total(),2)}}</span>
                            </div>
                            <div class="break-down-items">
                                <span>Tax and Fees <a href="#" class="text-secondary" data-toggle="tooltip" data-placement="top" data-html="true" title="<p class='m-0 text-left'>Local Tax = {{!empty($rest_data) ? number_format($rest_data->tax_rate,2) : ''}}%</p><p class='m-0 text-left'>Zing Service = ${{!empty($rest_data) ? number_format($rest_data->ZC,2) : ''}}</p>"><i class="ion-android-alert"></i></a></span>
                                <span>${{number_format(Cart::total()*(!empty($rest_data) ?  $rest_data->tax_rate/100 : 0)+(!empty($rest_data) ?  $rest_data->ZC : 0),2)}}</span>
                                
                            </div>
                            <input type="hidden" id="tax" name="tax" value="{{Cart::total()*(!empty($rest_data) ?  $rest_data->tax_rate/100 : 0)+(!empty($rest_data) ?  $rest_data->ZC : 0)}}">
                            <div class="break-down-items" id="delivery_div">
                                <span>Delivery</span>
                                <span id="delivery_amount_span">${{number_format($delivery_charge,2)}}</span>
                            </div>
                            <div class="break-down-items">
                                <span>Tip</span>
                                <span id="tip_charge">$0.00</span>
                            </div>
                            <div class="tip-block">
                                <input type="hidden" id="tip" name="tip">
                                <div class="tip-item" style="width: 40px;"> 
                                    <input type="radio" id="tip_0" name="tip_value" value=0 class="tipcal">
                                    <label for="tip_0">0%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;"> 
                                    <input type="radio" id="tip_5" name="tip_value" value=5 class="tipcal" checked="checked">
                                    <label for="tip_5">5%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;">
                                    <input type="radio" id="tip_10" name="tip_value" value=10 class="tipcal">
                                    <label for="tip_10">10%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;">
                                    <input type="radio" id="tip_20" name="tip_value" value=20 class="tipcal">
                                    <label for="tip_20">20%</label>
                                </div>
                                <div class="tip-item" >
                                    <input type="radio" name="tip" id="custom" value="">
                                    <label for="custom">Custom</label>
                                </div>
                                <div class="tip-item" style="width: 60px; height: 30px;">
                                    <input type="number" class="form-control" name="tip_value_cus" id="customper" style="display: none;" min=0 max=100>
                                </div>
                               <!--  <input type="text" class="form-control" placeholder="Enter custom tip" name="tipper" id="customper" style="display: none;"> -->
                            </div>
                            <hr>  
                            <div class="break-down-items points">
                                <span>Points <a href="#" onclick="redeem()" >Redeem Points</a></span>
                                <span id="points_redeem">$0.00</span>
                            </div>
                            @if(($rest_data->offers != null))
                            <div class="break-down-items points">
                                <span>Discount <a href="#" onclick="apply_offer()" id="apply_offer_link" >Apply Offer</a></span>
                                @if(@$rest_data->offers->type == 'Price')
                                <span>$ {{$rest_data->offers->value}}</span>
                                @else
                                <span>{{@$rest_data->offers->value}} %</span>
                                @endif
                            </div>
                            @endif
                            <input type="hidden" id="discount" name="discount">
                            <input type="hidden" id="discount_amount" name="discount_amount">
                            <hr>
                            <div class="break-down-items total"> 
                                <span>Total Amount</span>
                                <div class="text-right">
                                    <strike class="mr-5"><small id="original_amount"></small></strike> <b>$</b><span id="total_charge">{{number_format(Cart::total(),2)}}</span>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="total_amount" name="total_amount">
                    </div>
                        <!--<button type="submit" value="cash" name="submit" class="btn btn-theme btn-block mt-20 col-md-6">Cash On Delivery</button>-->
                        <input type="hidden" name="submit" id="submit">
                             <button type="button" id="btn_getToken" value="online" name="submit" class="btn btn-theme btn-block mt-20 ">Pay Online</button>
                              
                        <!--<button type="submit" value="success" name="submit" class="btn btn-theme btn-block mt-20">Pay Now</button>-->
                       <!--  <button type="submit" value="failed" name="submit" class="btn btn-danger btn-block">Failed</button> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>  
@endif
 <div class="modal fade login-modal" id="EmptyCartModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body">
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                            <div class="login-header text-center">
                                <p>Your Cart is Empty</p> 
                            </div>
                            <div class="text-center login-footer">
                               <a href="{{trans_url('/')}}" class="btn btn-theme search-btn">OK</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>       




<div class="modal fade" id="redeem_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Redeem Amount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body redeem-points-modal">
                @include('notifications')
                <p class="mb-0">You have <span>{{Cart::getLoyaltypoints()}}</span> Total Points</p>
                <p>Your points can be redeemed for <span>${{number_format(Cart::getLoyaltypoints()/2000,2)}}</span></p>
                <input type="number" class="form-control mb-15" id="redeem_amount" value="{{Cart::total()}}" min=1>
                <div class="btn-wrap modal-footer justify-content-start">
                    <button class="btn btn-theme" type="button" onclick="apply_redeem()" >Apply</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade errorModel" id="ErrorModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
           
        </div>
    </div>
</div>
<div class="modal fade login-modal location-modal" id="SessionModel" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body">
                <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                                <div class="login-header text-center">
                                  <h5 id="error_msg">This Eatery requires a minimum order of $<span id="min_amt"></span></h5>
                                </div>
                                
                                <div class="text-center" >
                                   <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" id="favourite_submit" data-dismiss="modal">Ok</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
 @include('cart::public.cart.address.billing.show')
 @include('cart::public.cart.address.delivery.show')
  <style>
    .pac-container {
        z-index: 10000 !important;
    }
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 500px;
        }
    }

</style>
        

 <script type="text/javascript">
    $( document ).ready(function() {
        var nowDate = new Date();
      
       var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
var endDates =  new Date(nowDate.getFullYear(), nowDate.getMonth() +2, +0);


  $('.datetimepicker').each(function(){
        $(this).datetimepicker({
            stepping: 5,
            minDate: new Date(),
            maxDate: endDates,
            buttons : {
                showClose: true
            }
        })
    });

    if ('<?php echo Session::has('error'); ?>')
    {
        var restaurant_id = '<?php echo $rest_id; ?>';
        $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
                $('#ErrorModel').modal({show:true}); 
            });
        // $('#ErrorModel').modal({show:true, backdrop: 'static',
        //     keyboard: false});
    }
    var total = '<?php echo Cart::total(); ?>'; 
        var tip = (total * 5)/100;
        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
        document.getElementById("tip").value=tip;
        var grandtotal = '<?php echo Cart::total()+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0); ?>';
        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip));

    $('#delivery_address_block').hide();


    $('#delivery_div').hide();
    document.getElementById("total_charge").textContent=('<?php echo number_format(Cart::total()+$delivery_charge+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0)+(5/100)*Cart::total(),2); ?>');
        var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
   
    document.getElementById("total_charge").textContent=(document.getElementById("total_charge").textContent-parseFloat(delivery_charge)).toFixed(2);
    var f = '<?php echo (date('H:i',strtotime(Session::get('search_time')))); ?>';
    var s = '<?php echo date('H:i'); ?>';
    
    if('<?php echo !empty(Session::get('search_time')); ?>'){ 
        if(f<s){
            document.getElementById('timecheck').value = '<?php echo date('g:i A'); ?>';
            //ew Date().toLocaleTimeString();
            // $('input[name="del_type"]').val('ASAP');
        }
        else{
            document.getElementById('timecheck').value = '<?php echo date('g:i A',strtotime(date('H:i'))); ?>';
            $( "#scheduled_order" ).prop( "checked", true );
            $('input[name="del_type"]').val('Schedule Order');
            
        }
        document.getElementById('datecheck').value = '<?php echo date('D d-M',strtotime(date('Y-m-d'))); ?>';
        $("#deltypet_wrap").show();
        // if(document.getElementById('datecheck').value == '{{date("D d-M")}}')
        //      $("#del_type_v").html('Today');
        // else
        // $("#del_type_v").html(document.getElementById('datecheck').value);
        // $("#del_time_v").html(document.getElementById('timecheck').value);
    }
    else{
         document.getElementById('timecheck').value = '<?php echo date('h:i A'); ?>';
         //new Date().toLocaleTimeString();
        document.getElementById('datecheck').value = '<?php echo date('D d-M'); ?>';
    }

    $('#saved_address').disabled = true;
    $('#delivery_amount_span').text('$0.00');
            $('#btn_add_address_form').hide();

    $("#asap").on('click', function(){ 
        $( "#asap" ).prop( "checked", true );
        //$('input[name="del_type"]').val('ASAP');
        document.getElementById('timecheck').value = '<?php echo date('h:i A'); ?>';
        //new Date().toLocaleTimeString();
        document.getElementById('datecheck').value = '<?php echo date('m/d/Y'); ?>';
        //new Date().toLocaleDateString();
    });
    $("#sch_time").on('change', function(){ 
        var c_Time = $(this).find(":checked").val();  
        document.getElementById('timecheck').value = c_Time;
        var restaurant_id = '<?php echo $rest_id; ?>';
         $.ajax({
            url: "{{ URL::to('cart/checkout/restaurant_time')}}"+'/'+restaurant_id+'/'+document.getElementById('datecheck').value+'/'+c_Time,
            success: function(response){
                if(response.status == 'Error'){
                    var restaurant_id = '<?php echo $rest_id; ?>';
        $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
                $('#ErrorModel').modal({show:true}); 
            });
                }
            }
        });
    });
    // $('#sch_time').selectize({
    //     maxItems: 1,
    // });

    $('input[name="del_type"]').click(function() { 
        if($(this).attr('id') == 'schedule_order') {
            $('#scedule-block').show();
            $("#del_date_v").html('Today');
            $("#del_type_v").hide();     
            var radioValue = $(this).val(); 
            if($(this).val() == 'Schedule Order'){
                  $("#deltypet_wrap").hide();
                  $("#del_type_v").show(); 
                $("#del_type_v").html(radioValue);

            }
            else{
                $("#deltypet_wrap").show();
            }
        }
        else {
            $('#scedule-block').hide();  
            $("#del_type_v").show(); 
            $("#deltypet_wrap").hide();
        }
    });
    $(".deltype-dropdown .dropdown-menu").click(function(e){
        e.stopPropagation();
    });

    $('input[name="del_type"]').click(function() { 
        if($(this).attr('id') == 'asap'){
            $("#del_type_v").html('ASAP')
        } else{  
             var radioValue = $(this).val();
            $("#del_type_v").html(radioValue)
        }
    });
    var schDateitem = $("#sch_DateWrap .date-item");
    $(schDateitem).on('click', function() { 
        var c_Date = $(this).attr("data-date");
        var c_Date1 = $(this).attr("data-val");
        document.getElementById('datecheck').value = c_Date;
        $("#sch_DateWrap .date-item").removeClass("active");
        $(this).addClass("active");
        $('#sch_time').load('{{url("time")}}/{{$rest_id}}' + '/' + c_Date1);
        // $("#del_date_v").html(c_Date)
        if(c_Date == "{{date('D d-M')}}")
            $("#del_date_v").html('Today')
        else
                $("#del_date_v").html(c_Date)
        $("#del_date_v").hide();
        var t_date = new Date(); 
        if(new Date(c_Date).getDate() == t_date.getDate()){
            var t_timestring = t_date.getTime();
            var t_time = (new Date(t_timestring).toLocaleTimeString("en-US"));
            var selectobject=document.getElementById("sch_time-selectized");
            // for (var i=0; i<selectobject.length; i++){
            //     alert(selectobject.options[i].text+" "+selectobject.options[i].value)
            // }
        }
    });
    $('#sch_time').on('change', function() { 
        $("#deltypet_wrap").show();
          $("#del_type_v").hide();
          $("#del_date_v").show();
        var c_Time = $(this).find(":checked").val(); 
        var c_Date = $(this).attr("data-date");
        $("#del_date_v").html(c_Date)
        $("#del_time_v").html(c_Time);

    });
});
    $('.tipcal').on('click', function() { 
        if(flag_qty!=1){
        document.getElementById('customper').style = "display:none;";
        document.getElementById('customper').value = "";
        var total = '<?php echo Cart::total(); ?>'; 
        var tip = (total * $(this).val())/100;
        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
        document.getElementById("tip").value=tip;
         if($('#order_type').val() != 'Pickup'){
        var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
    }
    else{
        var delivery_charge = 0;
    }
        var grandtotal = '<?php echo Cart::total()+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip)).toFixed(2);
        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip));
        document.getElementById('apply_offer_link').style = "display:block;";
         document.getElementById('original_amount').style = "display:none;";
    }
}); 
    $('#custom').on('click', function() { 
        if(flag_qty!=1){
        document.getElementById('customper').style = "display:block;";
    }
    });    
    
    $('#customper').on('input', function() {
if(document.getElementById('customper').value >= 0 && document.getElementById('customper').value <=100){
    var total = '<?php echo Cart::total(); ?>';
        var tip = (total * document.getElementById('customper').value)/100;
        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
        document.getElementById("tip").value=tip;
         if($('#order_type').val() != 'Pickup'){
var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';    }
    else{
        var delivery_charge = 0;
    }
        
        var grandtotal = '<?php echo Cart::total()+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip)).toFixed(2);
        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip));
        document.getElementById('apply_offer_link').style = "display:block;";
         document.getElementById('original_amount').style = "display:none;";
}
        
    });
</script>
<script type="text/javascript">
  

     
    function redeem() {
        if(flag_qty!=1){
        $('#redeem_modal').modal({show:true, backdrop: 'static',
            keyboard: false}); 
    }
    }
    function apply_offer() {
            var total_amount = Number($('#total_charge').text());
            var type = '{{@$rest_data->offers->type}}';
            var offer = '{{@$rest_data->offers->value}}';
            var limit = '{{@$rest_data->offers->limit_amount}}';

            if(type == 'Price'){
                var red_amount = Number(offer);
            }else{
                var red_amount = Number(total_amount)*Number(offer)/100
            }
            
            if(Number(total_amount) < Number(limit)){
                alert('Amount could not reach the limit');
                return false;
            }

            var final_amount = Number($('#total_charge').text())-Number(red_amount);

            if(Number(final_amount) < 0){
                alert('Amount could not reach the limit');
                return false;
            } 
            $('#total_charge').html(final_amount.toFixed(2));
            document.getElementById('discount_amount').value = parseFloat(red_amount);
            document.getElementById('total_amount').value = final_amount;
            $('#original_amount').text('$'+total_amount);
             $('#original_amount').show();
            document.getElementById('apply_offer_link').style.display = "none";
             
            $('#SessionModel').modal({show:true});
            $('#error_msg').html('<span class="ik ik-check-circle" aria-hidden="true" style="font-size: 40px; display: inline-block; vertical-align: -8px; color: #40b659;"></span> Offer Applied');
    }
    function apply_redeem() {
        var red_amount = document.getElementById('redeem_amount').value;
        var tot_points = '<?php echo Cart::getLoyaltypoints(); ?>';
        var cart_total = '<?php echo Cart::total(); ?>';
         if(tot_points/2000 < red_amount){
            alert('Amount exceed the limit', 'Error');
            $('#redeem_modal').modal('hide')
            return false;
        }
        if(Number(red_amount) > Number(cart_total)){
            alert('Amount exceeded the cart amount');
            $('#redeem_modal').modal('hide')
            return false;
        }
        if(Number(red_amount) <= 0){
            alert('Please enter a valid amount');
            $('#redeem_modal').modal('hide')
            return false;
        }
        else{
            document.getElementById("points_redeem").textContent='$'+(parseFloat(red_amount).toFixed(2)); 
            $('#redeem_modal').modal('hide')
            document.getElementById('discount').value = parseFloat(red_amount);
            var total_amount = Number($('#total_charge').text());
            var final_amount = Number($('#total_charge').text())-Number(red_amount);
            $('#total_charge').html(final_amount.toFixed(2));
            document.getElementById('total_amount').value = final_amount;
        }
      
        
    }
    if ($('.enumerator').length > 0 ) { 
        $(".js-minus_btn").on('click', function() {
            if(flag_qty!=1){
            var inputEl = jQuery(this).parent().children().next();
            var qty = inputEl.val();
            if(qty!=1){
                if (jQuery(this).parent().hasClass("js-minus_btn"))
                    qty++;
                else
                    qty--;
                if (qty < 0)
                    qty = 0;
                inputEl.val(qty);
            }
        }
        });


        $(".js-plus_btn").on('click', function() { 
            if(flag_qty!=1){
                var inputEl = jQuery(this).parent().children().next();
            var qty = inputEl.val();
            if (jQuery(this).hasClass("js-plus_btn"))
                qty++;
            else
                qty--;
            if (qty < 0)
                qty = 0;
            inputEl.val(qty);
            }
            
        });
    }
    function update_cart(rowid){  
        var qty = document.getElementById(rowid).value;
            $('#payment_details').load('{{trans_url('cart/update')}}/'+qty+'/'+rowid);
           
        } 
         function increment(rowid){
            if(flag_qty!=1){
             $('#payment_details').load('{{trans_url('carts/add')}}/'+rowid);
         }
            
            //  $('#total_charge').html(final_amount.toFixed(2));
            // document.getElementById('total_amount').value = final_amount;
         }
        
          function decrement(rowid){
            if(flag_qty!=1){
             $('#payment_details').load('{{trans_url('carts/subtract')}}/'+rowid);
         }
         }     
           
   function typeChange() {
       if($('#order_type').val() == 'Pickup'){
            $('#saved_address').disabled = true;
            $('#address_delivery').hide();
            // $('#address_pickup').show();
             $('#delivery_address_block').hide();
            $('#btn_add_address_form').hide();
            document.getElementsByName('address_id').value='';
            $('#delivery_amount_span').text('$0.00');
            $('#delivery_div').hide();
            
            
       }
       else{
            $('#saved_address').disabled = false;
            $('#address_pickup').hide();
            $('#address_delivery').show();
            $('#delivery_address_block').show();
            $('#btn_add_address_form').show();
             $('#delivery_div').show();
            $('#delivery_amount_span').text('$<?php echo number_format($delivery_charge,2); ?>');

       }
       
    if($('#order_type').val() != 'Pickup'){
        var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(document.getElementById("total_charge").textContent)+parseFloat(delivery_charge)).toFixed(2);
    }
    else{
        var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(document.getElementById("total_charge").textContent)-parseFloat(delivery_charge)).toFixed(2);
    }
    
   }
var flag_qty=0;
   $(document).on('click','#btn_getToken',function(){ 
            document.getElementById('submit').value = 'online';
            var $f = $('#frm_checkout');
            $.getJSON({
                  type: 'GET',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    console.log(data);
                     if(data.status == 'Error'){
                        if(data.msg == 'min_order_count'){
                                   $('#error_msg').html('This Eatery requires a minimum order of $' + data.min_amt);

                                   $('#SessionModel').modal({show:true});  
                        }
                        else if(data.msg == 'delivery_limit'){
                                   $('#SessionModel').modal({show:true});  
                                   $('#error_msg').html('Your address is outside the delivery limit of '+data.delivery_limit+' miles for the Eatery');
                        }
                         else if(data.msg == 'Billing address'){
                                   $('#SessionModel').modal({show:true});
                                   $('#error_msg').html('Please complete your billing address');
                        }
                           else if(data.msg == 'Previous Time'){
                                   $('#SessionModel').modal({show:true});
                                   $('#error_msg').html('Pickup/Delivery time should be a future Date and Time');
                        }
                        else if(data.msg == 'Delivery_address'){
                                   $('#SessionModel').modal({show:true});
                                   $('#error_msg').html('Please complete your delivery address');
                        }
                        else if(data.msg == 'address_error'){
                                   $('#SessionModel').modal({show:true});
                                   $('#error_msg').html('Please change your address');
                        }
                        else if(data.msg == 'restaurant_time'){ 
                            var restaurant_id = '<?php echo $rest_id; ?>';
                            $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
                                $('#ErrorModel').modal({show:true}); 
                            });
                        }
                    }
                    else{
                        $('#card_det').html(data.view)
                        $('#btn_getToken').hide();
                        $('#edit_orders').attr('href',"{{url('cart/checkout_edit')}}"+ "/" + data.id);
                        $('#edit_orders').show();
                        $('#btn_back').show();
                        $('#btn_back1').hide();
                        flag_qty=1;
                        $('.remove-btn').disabled = true;
                        $('.btn_add_billing_address_form').hide();
                        const elements = document.getElementsByClassName('remove-btn');
                        for (const element of elements) {
                            element.href = '#';
                        }
                        jQuery("#card_det").attr("tabindex",-1).focus();
                        document.getElementById('card').focus();
                         $('.tip-block *').prop('disabled', true);
                        $('.block-item-inner *').prop('disabled', true);

                    }
                    
                             
                  },
                  error: function(xhr, status, error) {
                    console.log('hi');
                    var restaurant_id = '<?php echo $rest_id; ?>';
        $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
                $('#ErrorModel').modal({show:true}); 
            });
}
              });
           
        });
   $('#btn_back').click(function(){ 
    document.location.reload();
   })
     $('#btn_back1').click(function(){ 
    window.location=document.referrer;
   })

     
    //   $( document ).ajaxComplete(function() {
    //     $("#sch_time").on('change', function(){ 
    //     var c_Time = $(this).find(":checked").val();  
    //     document.getElementById('timecheck').value = c_Time;
    //     var restaurant_id = '<?php echo $rest_id; ?>';
    //      $.ajax({
    //         url: "{{ URL::to('cart/checkout/restaurant_time')}}"+'/'+restaurant_id+'/'+document.getElementById('datecheck').value+'/'+c_Time,
    //         success: function(response){
    //             if(response.status == 'Error'){
    //                 var restaurant_id = '<?php echo $rest_id; ?>';
    //     $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
    //             $('#ErrorModel').modal({show:true}); 
    //         });
    //             }
    //         }
    //     });
    // });
    //   }); 
        

</script>