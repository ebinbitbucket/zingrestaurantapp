<!doctype html>
<html class="no-js" lang="en">
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="x-ua-compatible" content="ie=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
	    <style>
	        body {
	            font-family: 'Lato', sans-serif;
	            font-weight: 300;
	            font-size: 15px;
	        }
	    </style>
  	</head>

    <body style="background-color: #f1eff2;">
        <table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: #fff; height: 100%; width: 600px; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.10);">
            <tbody>
                <tr>
                    <td align="center">
                       <div style="background-color: #40b659; text-align: center; padding: 30px 30px;">
                           <img src="{{url('assets/img/logo-round-white.png')}}" style="height: 80px; display: inline-block;" alt="">
                           <h1 style="color: red;margin-top: 10px; margin-bottom: 10px; font-size: 24px;">Order Cancellation</h1>
                       </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
                        <p style="margin: 0;">
                            <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>Order Details</b></span>
                            <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px;">#{!!@$order['id']!!}</span>
                            <span style="font-size: 18px; color: #666666; display: block; margin-bottom: 5px; text-transform: capitalize;">{!!@$order->restaurant->name!!}</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
                        <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>Order Summary</b></span>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 20px; padding-top: 30px;">
                            <tbody>
								@foreach($order_detail as $detail)
								<?php
                                    $variation = $detail->variation; 
                                     $addon_price = 0;

                                     if (!empty($detail->addons)) {

                                         foreach ($detail->addons as $cart_addon) {

                                             $addon_price = $addon_price + $cart_addon[2];
                                         }

                                     }

                                 ?>
                                <tr style="text-align: left;">
                                    <td valign="top" width="10%" style="color: #666666; line-height: 24px; font-size: 18px; border-bottom: 1px solid #eaeaea;">{{$detail->quantity}}x</td>
                                    <td width="75%" style="color: #666666; line-height: 24px; font-size: 16px; padding-bottom: 5px; border-bottom: 1px solid #eaeaea;"><b>@if($detail->menu_addon == 'menu' && (!empty($detail->menu)))
                                            {{@$detail->menu->name}} 
                                        @elseif(!empty($detail->addon))
                                            {{@$detail->addon->name}}  
                                        @endif</b><br>
                                        @if(!empty($variation))
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: diamond; font-size: 14px;">
                                                 Variation- {{$variation}}
                                        </ul>
                                        @endif
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: diamond; font-size: 14px;">
                                            @if(!empty($detail->addons))
                                                @foreach($detail->addons as $cart_addon)
                                                  <li>  {{$cart_addon[1]}} </li>
                                                @endforeach
                                            @endif
                                        </ul></br>
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: circle; font-size: 14px;">
                                        	@if(!empty($detail->options['special_instr']))
                                            <li>{{$detail->options['special_instr']}}.</li>
                                            @endif
                                        </ul>
                                    </td>
                                    <td valign="top" width="10%" style=" color: #666666; line-height: 24px; font-size: 18px; text-align: right; border-bottom: 1px solid #eaeaea;">${{number_format($detail->quantity*$detail->unit_price+$addon_price,2)}}</td>
                                </tr>
							@endforeach
                            </tbody>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 0px;">
                            <tbody>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Subtotal</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['subtotal'],2)!!}</td>
                                </tr>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Tax & Fees</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['tax'],2)!!}</td>
                                </tr>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Delivery</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['delivery_charge'],2)!!}</td>
                                </tr>
                                <tr style="text-align: left;">
                                    <td width="65%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="25%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Tip</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['tip'],2)!!}</td>
                                </tr>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">Loyality Points</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 10px; font-size: 18px; text-align: right;">{!!number_format(@$order['discount_points']/2000,2)!!} ({{@$orders->loyalty_points}} Points)</td>
                                </tr>
                                <tr style="text-align: left;">
                                    <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>
                                    <td width="20%" style="color: #333333; padding-bottom: 5px; font-size: 16px; font-weight: 700;">Total</td>
                                    <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right; font-weight: 700;">${!!number_format(@$order['total'],2)!!}</td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                
                
            </tbody>
        </table>
    </body>
</html>
