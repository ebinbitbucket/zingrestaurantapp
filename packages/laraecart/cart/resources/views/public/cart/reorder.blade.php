<section class="checkout-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                             <form method="GET" action="{{guard_url('cart/reorder/delivery/'.$orders->getRoutekey())}}">
                            <div class="checkout-block-wrap">
                                <div class="block-item">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <h3>Address</h3>
                                            <p><a href="#">Change</a></p>
                                        </div>
                                        <div class="col-md-4">
                                             <input type="text" name="cus_name" class="form-control"  placeholder="Customer Name"></br>
                                            <input type="text" name="email" class="form-control"  placeholder="Customer Email"></br>
                                            <input type="text" name="delivery_address" id="txtPlacess" class="form-control"  value="{{$orders->address}}" placeholder="Delivery Address">
                                            <input type="text" id="apartment" name="apartment" class="form-control"  value="{{$orders->apartment_no}}" placeholder="Apartment No">
                                            <input type="text" id="delivery_instr" name="delivery_instr" class="form-control" value="{{$orders->delivery_instr}}"  placeholder="Delivery Instructions">
                                            <!-- <p><a href="#">Add Delivery Instructions</a></p> -->
                                        </div>
                                    </div>
                                </div>
                                <div class="block-item">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h3>Time</h3>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="custom-control custom-radio mb-10">
                                                <input type="radio" id="asap" name="Delivery_time" class="custom-control-input" checked="">
                                                <label class="custom-control-label" for="asap">ASAP</label>
                                            </div>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" id="scheduled" name="Delivery_time" class="custom-control-input" value="off">
                                                <label class="custom-control-label" for="scheduled">Scheduled</label>
                                            </div>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <p>49 - 50 mins</p>
                                            <div class="select-date-wrap">
                                                <input type="text" name="scheduled_time" class="form-control datetimepicker" data-toggle="datetimepicker" data-target=".datetimepicker" placeholder="Select a date & time">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-item">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h3>Payment</h3>
                                            <p><a href="#">Change / Add</a></p>
                                        </div>
                                        <div class="col-md-8">
                                            <h4>Card</h4>
                                            <div class="payment-type">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <div class="form-group">
                                                            <input type="text" name="cardno" class="form-control" placeholder="Card Number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" name="cvv" class="form-control" placeholder="CVV">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <select class="form-control" name="month">
                                                                <option disabled="" selected="">Expiry Month</option>
                                                                <option>01</option>
                                                                <option>02</option>
                                                                <option>03</option>
                                                                <option>04</option>
                                                                <option>05</option>
                                                                <option>06</option>
                                                                <option>07</option>
                                                                <option>08</option>
                                                                <option>09</option>
                                                                <option>10</option>
                                                                <option>11</option>
                                                                <option>12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <select class="form-control" name="year">
                                                                <option disabled="" selected="">Expiry Year</option>
                                                                <option>23</option>
                                                                <option>24</option>
                                                                <option>25</option>
                                                                <option>26</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="text" name="name" class="form-control" placeholder="Card Holder Name">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="block-item">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h3>Summary</h3>
                                            <p><a href="{{url('restaurants')}}">Return to Menu</a></p>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="menu-item-block">
                                                @foreach($orders->detail as $cart)
                                                <div class="menu-block" id="{{$cart->menu_id}}">
                                                    <input type="hidden" name="menu_ids[]" value="{{$cart->menu_id}}">
                                                    <div class="menu qty">{{$cart->quantity}}</div>
                                                    <div class="menu item">@if($cart->menu_addon == 'menu')
                                                 {{$cart->menu->name}} 
                                                @else
                                                 {{$cart->addon->name}}  
                                                @endif</div>
                                                    <div class="menu price">
                                                        <span>x</span>${{$cart->unit_price}}
                                                        <a onclick="removeMenu({{$cart->menu_id}},{{$cart->price}})" class="remove-btn"><i class="fa fa-remove"></i></a> 
                                                        <input type="hidden" id="updated_tot" value="{{$orders->subtotal}}"></ins>
                                                    </div>

                                                </div>

                                               @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 ml-auto">
                            <div class="payment-wrap">
                              <!--   <div class="menu-restaurant-wrap">
                                    <div class="res-img">
                                        <img src="img/restaurants/thumb/restaurant-01.jpg" class="img-fluid" alt="">
                                    </div>
                                    <div class="res-content">
                                        <p>Order From <span>Jet's Kitchen</span></p>
                                    </div>
                                </div> -->
                                <div class="payment-breakdown-wrap mt-20">
                                    <div class="break-down-items">
                                        <span>Subtotal</span>
                                        <span id="subtotal">${{$orders->subtotal}}</span>
                                    </div>
                                    <div class="break-down-items">
                                        <span>Tax</span>
                                        <span>$0.00</span>
                                    </div>
                                    <div class="break-down-items">
                                        <span>Delivery</span>
                                        <span>Free</span>
                                    </div>
                                    <div class="break-down-items">
                                        <span>Service Fee</span>
                                        <span>$0.00</span>
                                    </div>
                                    <hr>
                                    <div class="break-down-items total">
                                        <span>Total</span>
                                        <span id="total">${{$orders->subtotal}}</span>
                                    </div>
                                </div>
                                <button type="submit" value="success" name="submit" class="btn btn-danger btn-block">Success</button>
                                <button type="submit" value="failed" name="submit" class="btn btn-danger btn-block">Failed</button>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </section>          
<script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.GOOGLE_API') }}&libraries=places"></script>
<script type="text/javascript">
    // $( document ).ready(function() {
    //     document.getElementById('apartment').style.display='none'; 
    //     document.getElementById('delivery_instr').style.display='none'; 
    // });
     var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlacess'));
        google.maps.event.addListener(places, 'place_changed', function () {
                document.getElementById('apartment').style.display='block'; 
                document.getElementById('delivery_instr').style.display='block'; 
        });
    });
     function geocodeAddress(geocoder) {
        var address = document.getElementById('txtPlacess').value;
        geocoder.geocode({'address': address}, function(results, status) {alert(status);
          if (status === 'OK') {
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
      function removeMenu(menu_id,price){
        document.getElementById('total').innerHTML = document.getElementById('updated_tot').value - price;
        document.getElementById('subtotal').innerHTML = document.getElementById('updated_tot').value - price;
        document.getElementById('updated_tot').value = document.getElementById('total').innerHTML;
        document.getElementById(menu_id).remove();
      }
</script>