            @include('cart::cart.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('cart::cart.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($cart->defaultImage('images' , 'xl'))!!}" alt="{{$cart->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('cart::cart.label.id') !!}
                </label><br />
                    {!! $cart['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="instance">
                    {!! trans('cart::cart.label.instance') !!}
                </label><br />
                    {!! $cart['instance'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="content">
                    {!! trans('cart::cart.label.content') !!}
                </label><br />
                    {!! $cart['content'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('cart::cart.label.created_at') !!}
                </label><br />
                    {!! $cart['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('cart::cart.label.updated_at') !!}
                </label><br />
                    {!! $cart['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('cart::cart.label.deleted_at') !!}
                </label><br />
                    {!! $cart['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('instance')
                       -> label(trans('cart::cart.label.instance'))
                       -> placeholder(trans('cart::cart.placeholder.instance'))!!}
                </div>

                <div class='col-md-12'>
                    {!! Form::textarea('content')
                    -> label(trans('cart::cart.label.content'))
                    -> dataUpload(trans_url($cart->getUploadURL('content')))
                    -> addClass('html-editor')
                    -> placeholder(trans('cart::cart.placeholder.content'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



