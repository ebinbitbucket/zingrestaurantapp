
                                <div class="block-item" >
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h3 style="padding-left:15px;">Payment</h3>
                                        </div>
                                        <div class="col-md-8">
                                            <h4>Card*</h4>
                                            <div class="payment-type">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <input type="number" step="1" class="form-control card_class" placeholder="Card Number" id="card" name="card" required="required" autocomplete="cc-number">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <input type="number" step="1" max="999" class="form-control cvv_class" placeholder="CVV*" id="cvv" name="cvv" required="required" autocomplete="cc-csc">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div class="form-group">
                                                            <!--<input type="text" name="cc-exp" id="exp" required="required" placeholder="MM-YY" autocomplete="cc-exp" class="form-control">-->
                                                            <input type="text" name="cc-exp" required="required" class="form-control" autocomplete="cc-exp" id="exp" data-inputmask="'alias': 'date','mask': '99-99','placeholder' : 'MM-YY'" />
                                                            <!--<select class="form-control" id="month" required="required">-->
                                                            <!--    <option disabled="" selected="">Expiry Month*</option>-->
                                                            <!--    <option value="01">01</option>-->
                                                            <!--    <option value="02">02</option>-->
                                                            <!--    <option value="03">03</option>-->
                                                            <!--    <option value="04">04</option>-->
                                                            <!--    <option value="05">05</option>-->
                                                            <!--    <option value="06">06</option>-->
                                                            <!--    <option value="07">07</option>-->
                                                            <!--    <option value="08">08</option>-->
                                                            <!--    <option value="09">09</option>-->
                                                            <!--    <option value="10">10</option>-->
                                                            <!--    <option value="11">11</option>-->
                                                            <!--    <option value="12">12</option>-->
                                                            <!--</select>-->
                                                        </div>
                                                    </div>
                                                    <!--<div class="col-md-4">-->
                                                    <!--    <div class="form-group">-->
                                                    <!--        <select class="form-control" id="year">-->
                                                    <!--            <option disabled="" selected="">Expiry Year*</option>-->
                                                    <!--            @for($i=0;$i<10;$i++)-->
                                                    <!--            <option value="{{date('y', strtotime(date('d-M-Y'). ' + '. $i .'years'))}}">{{date('y', strtotime(date('d-M-Y'). ' + '. $i .'years'))}}</option>-->
                                                                
                                                    <!--            @endfor-->
                                                    <!--        </select>-->
                                                    <!--    </div>-->
                                                    <!--</div>-->
                                                   
                                                    <input id="token" type="hidden" name="token" value="{{$tocken}}"> <br />
                                                    <input id="merchanttxnid" type="hidden" name="merchanttxnid" value="2108313" /> <br />
        <input id="gettoken" type="hidden" name="gettoken" value="y" />
        <input id="addtoken" type="hidden" name="addtoken" value="y" />
                                                </div>
                                                <img src="{{url('img/payment-logos.png')}}" style="height: 40px; margin-top: 15px;">
                                                <!-- <button type="button" name="submit" class="btn btn-theme btn-block mt-20 col-md-6" onclick="pay();" id="btn_process">Pay Now</button> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               <!-- <div class="block-item" >-->
                               <!--  <div class="row">-->
                               <!--         <div class="col-md-4">-->
                               <!--             <h3>Billing Address</h3>-->
                               <!--   </div>-->
                               <!--         <div class="col-md-5 block-item-inner">-->
                               <!--     <div class="address-block">-->
                               <!--         <div class="saved-address" id="saved_address1"> -->
                               <!--             @forelse(Cart::getBillingAddresses() as $address)-->
                               <!--             <?php $flag=0; ?>-->
                               <!--             <div class="address-item">-->
                               <!--                 <input type="radio" id="address_{{$address->id}}" name="billing" class="billing_address_id" onclick="address_radio('{{$address->id}}')" checked value="{{$address->id}}">-->
                               <!--                 <label for="address_{{$address->id}}">-->
                               <!--                     <h3>{{$address->title}}</h3>-->
                               <!--                     <p>{{$address->address}}</p>-->
                               <!--                 </label>-->
                               <!--               <input type="hidden" name="billing_address_detail" value="{{$address->title}},{{strtok($address->address,',')}}">  -->
                               <!--         </div>-->
                               <!--            @empty-->
                               <!--             <?php $flag=1; ?>-->
                               <!--         @endif -->
                               <!--      </div>-->
                                       
                               <!--    </div>-->
                                    
                               <!--</div>  @if($flag==1)-->
                               <!--                 <button type="button" data-toggle="modal" href="#address_model1" id="btn_add_address_form" name="button" class="btn change-address-btn">Add Address</button>-->
                               <!--@else-->
                               <!--             <button type="button" data-toggle="modal" href="#address_model1" name="button" class="btn change-address-btn">Add or Change Address</button>-->
                               <!--      @endif-->

                               <!--         </div>-->
                                            <div class="row">
                                                <div class="col-md-4"></div>
                                                <div class="col-md-3"><a href="" id="edit_orders" value="online" class="btn btn-default btn-block mt-20 " style="display: none;">Edit Order</a></div>
                                                <div class="col-md-3"><button type="button" name="submit" class="btn btn-theme btn-block mt-20" onclick="pay();" id="btn_process">Pay Now</button></div>
                                                 
                                            </div>
                                            <div class="se-pre-con" ></div>
                                        </div>
                                    </div>
                                </div>
                                 {!!Form::vertical_open()
                    ->method('GET')
                    ->id('response_form')
                    ->action(trans_url('cart/payment/response'))!!}
                   
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="payment_response" id="payment_response">
                                <input type="hidden" class="form-control" name="status" id="status">
                                <input type="hidden" class="form-control" name="order_id" id="order_id">
                                <input type="hidden" class="form-control" name="billing_address_response" id="billing_address_response">
                            </div>
                        </div>
                   
                    {!! Form::close() !!}
<div class="modal fade" id="address_model1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">
                @include('notifications')
                <div class="saved-address" id="popup_address_list">
                    @foreach(Cart::getAllBillingAddresses() as $address)
                    <div class="address-item">
                        <input type="radio" class="close" data-dismiss="modal" onclick="address_radio1('{{$address->id}}')" id="address_{{$address->id}}" name="address_id"  {{$address->default_address == 1 ? 'checked' : ''}} value="{{$address->id}}" >
                        <label for="address_{{$address->id}}">
                            <h3>{{$address->title}}</h3>
                            <p>{{$address->address}}</p>
                        </label>
                    </div>
                    @endforeach
                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="button" onclick="add_form_show1()">Add Address</button>
                    </div>
                </div>
                <div id="popup_address_add" class="add-address" style="display: none;">
<!--                     <div id="locationMap" class="mb-15" style="width: 100%; height: 250px;"></div>
 -->                    {!!Form::vertical_open()
                    ->method('POST')
                    ->id('add-billing-address-form')
                    ->action(trans_url('client/cart/address'))!!}
                    <div class="form-group">
                        <label for="name">Address</label>
                        <input id="billing_address" type="text" name="address" class="form-control" required="required">
                        <input type="hidden" name="latitude" id="latitude_address">
                        <input type="hidden" name="longitude" id="longitude_address">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Suite/Unit Number</label>
                                <input type="text" class="form-control" name="apartment_no">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Address Name</label>
                                <input type="text" class="form-control" name="title" placeholder="Office" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Delivery Notes</label>
                        <input type="text" class="form-control" name="notes">
                    </div>
                     <input type="hidden" name="type" value="billing">

                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="button" value="checkout" name="btn_add_address1" id="btn_add_address" >Save Address</button>
                        <button class="btn btn-secondary" type="button" onclick="list_form_show()" >List Address</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
        <!--<body background="SIF.png">-->
             <script src="https://api.convergepay.com/hosted-payments/Checkout.js"></script>
    <script src="{{url('cart/jqueryCheckout.js')}}"></script>
    

    <script>
    $(document).ready(function() { 
        Inputmask().mask(document.getElementById("exp"));
        // Animate loader off screen
        $(".se-pre-con").hide();
    });
        var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('billing_address'));
        google.maps.event.addListener(places, 'place_changed', function () {
                geocodeAddress(geocoder);
        });
    });
     function geocodeAddress(geocoder) {
        var address = document.getElementById('billing_address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
         function address_radio1($id){
         
        $.get("{{trans_url('client/cart/saved_billing_address')}}/"+$id, function(data) { $('#saved_address1').html(data); });
    };

        function add_form_show1() {
        document.getElementById('popup_address_list').style.display = 'none';
        document.getElementById('popup_address_add').style.display = 'block';
    }

   


        function initiateCheckoutJS() {
            var tokenRequest = {
                ssl_first_name: document.getElementById('name').value,
                ssl_last_name: document.getElementById('lastname').value,
                ssl_amount: document.getElementById('ssl_amount').value
            };
            $.get("checkoutjscurlrequestdevportal", tokenRequest, function (data) {
                console.log(data);
                document.getElementById('token').value = data;
                transactionToken = data;
            });
            return false;
        }

        function showResult(status, msg) {
            document.getElementById('txn_status').innerHTML = "<b>" + status + "</b>";
            document.getElementById('txn_response').innerHTML = msg;
        }
    </script>





    <script>
   
        function pay() { 
            if(document.getElementById('card').value == '' || document.getElementById('exp').value == ''  || document.getElementById('cvv').value == ''){
                toastr.error('Please fill mandatory fields','Error');
                return false;
            }
            $(".se-pre-con").show();
            document.getElementById("btn_process").disabled = true;

            // $('#btn_process').hide();
            var token = document.getElementById('token').value;
            var card = document.getElementById('card').value;
            // var exp = document.getElementById('month').value+document.getElementById('year').value;
            var exp = (document.getElementById('exp').value).replace(/\/20|-|\//g, '');
            var cvv = document.getElementById('cvv').value;
            var gettoken = document.getElementById('gettoken').value;
            var addtoken = document.getElementById('addtoken').value;
            var firstname = '<?php echo user()->name; ?>';
            var lastname = '<?php echo user()->name; ?>';
            var merchanttxnid = document.getElementById('merchanttxnid').value;
             var billing_address = document.querySelector('input[name="billing"]:checked').value;
            var billing_address_detail = document.querySelector('input[name="billing_address_detail"]').value;
            var paymentData = {
                ssl_txn_auth_token: token,
                ssl_card_number: card,
                ssl_exp_date: exp,
                ssl_get_token: gettoken,
                ssl_add_token: addtoken,
                ssl_first_name: firstname,
                ssl_last_name: lastname,
                ssl_cvv2cvc2: cvv,
                ssl_merchant_txn_id: merchanttxnid,
                ssl_avs_address: billing_address_detail,
                ssl_avs_zip:682019
            };
            console.log(paymentData);

            var callback = {
                onError: function (error) {
                    document.getElementById('payment_response').value = JSON.stringify(error);
                    document.getElementById('status').value = "error";
                    document.getElementById('order_id').value = '<?php echo $order->id; ?>';
                   showResult("error", error,billing_address);  
                },
                onDeclined: function (response) {
                    console.log("Result Message=" + response['ssl_result_message']);
                    document.getElementById('payment_response').value = JSON.stringify(response);
                    document.getElementById('status').value = "declined";
                    document.getElementById('order_id').value = '<?php echo $order->id; ?>';
                    showResult("declined", JSON.stringify(response),billing_address);
                },
                onApproval: function (response) {
                    console.log('succes',response);
                    document.getElementById('payment_response').value = JSON.stringify(response);
                    document.getElementById('status').value = "approval";
                    document.getElementById('order_id').value = '<?php echo $order->id; ?>';
                    console.log("Approval Code=" + response['ssl_approval_code']);
                    showResult("approval", JSON.stringify(response),billing_address);
                }
            };
            ConvergeEmbeddedPayment.pay(paymentData, callback);
            return false;
        }

        function showResult(status, msg,billing_address) {
            console.log('hi');
            $f = $('#response_form');
            document.getElementById('billing_address_response').value = billing_address;
            $f.submit();
            // window.location = "{{ URL::to('cart/payment/response') }}/?status="+status+"&msg="+msg+"&order_id="+'<?php echo $order->id; ?>';
            // // document.getElementById('txn_status').innerHTML = "<b>" + status + "</b>";
            // // document.getElementById('txn_response').innerHTML = msg;
        }
    </script>
<style type="text/css">
    .cvv_class::-webkit-inner-spin-button, 
.cvv_class::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.card_class::-webkit-inner-spin-button, 
.card_class::-webkit-outer-spin-button { 
  -webkit-appearance: none; 
  margin: 0; 
}
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    left: 0px;
    height: 100px;
    z-index: 9999;
    background: url('{{theme_asset('img/30.gif')}}') center no-repeat #fff;
}
</style>

