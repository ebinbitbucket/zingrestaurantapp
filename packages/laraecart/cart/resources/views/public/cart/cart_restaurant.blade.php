 @if(Cart::count() == 0)
<div class="cart-inner-wrap empty-cart">
     <img src="{{theme_asset('img/empty-cart.png')}}" alt="">
    <h2>Your Cart is Empty</h2>
    <p>Looks like you haven't made your choice yet</p>
</div>
@else
<div class="cart-inner-wrap sticky-div">
    <h2>Cart <span>{{Cart::count()}} Item</span></h2>
    <p>(
    @if(!empty($restaurant))
        @if($restaurant->delivery == 'Yes')
            @if($restaurant->delivery_charge == 0)
                Take out or Delivery
            @elseif($restaurant->delivery_charge != '')
               Take out or Delivery
            @endif
        @else
            Take out
        @endif
        @endif) <a href="#" class="text-success">Earn Points <i class="fa fa-exclamation-circle text-dark"></i></a></p>
    <hr>
    <div class="cart-item-wrap">
        @foreach(Cart::content() as $cart)
        <?php $variation = $cart->options->variation;?>
        <div class="cart-item" style="{{$cart->options['menu_addon'] == 'addon' ? 'margin-left: 20px' : ''}}">
            <div class="item-name-price">
                <h4>{{str_limit($cart->name,17)}}</h4>
                
                @php 
                    $addon_price = 0;
                        if(!empty($cart->options['addons'])){
                    foreach($cart->options['addons'] as $cart_addon){

                            $addon_price = $addon_price + $cart_addon[2];
                    }
                                   
                   }
                @endphp
                <div class="price">${{number_format($cart->price+$addon_price,2)}}</div>
            </div>
             @if(!empty($variation))<p>(Variation-{{$variation}})</p>@endif
            @if(!empty($cart->options['addons']))
                @foreach($cart->options['addons'] as $cart_addon)
                 <div class="item-addon"><span data-toggle="tooltip" data-placement="top" title="{{$cart_addon[1]}}">{{str_limit($cart_addon[1],17)}}</span></div>
                @endforeach
            @endif
             @if($cart->options['menu_addon'] != 'addon')
            <div class="item-qty-price">
                <div class="qty-price-inner">
                    <span class="enumerator">
                        <span class="enumerator_btn js-minus_btn" id="minus_span" onclick="decrement('{{$cart->rowId}}')">-</span>
                        <span class="enumerator_input">{{$cart->qty}}</span>
                        <input type="hidden" value="{{$cart->qty}}" readonly="readonly">
                        <span class="enumerator_btn js-plus_btn" id="plus_span" onclick="increment('{{$cart->rowId}}')">+</span>
                    </span>
                     
                    <span class="remove-btn"><a href="#" class="remove-btn" onclick="removeCartItem('{{$cart->rowId}}')"><i class="ion-android-cancel"></i></a></span>
                </div>
            </div>
            @endif
        </div>
        @endforeach
    </div>
    <div class="cart-summary-wrap mt-20">
        <div class="summary-item">
            <div class="summary-text">Subtotal</div>
            <div class="summary-price">${{number_format(Cart::total(),2)}}</div>
        </div>
        <!-- <div class="summary-extra">Extra charges may apply</div> -->
        <a class="btn btn-theme checkout-btn" href="{{url('cart/checkout')}}">Checkout Now</a>
    </div>
</div>
@endif

<script>
    $(document).ready(function(){
        $('.cart_count_header').html({!!Cart::count()!!}); 
    });
</script>