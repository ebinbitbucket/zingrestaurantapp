<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Rancho" rel="stylesheet">
    <style>
        body {
            font-family: 'Lato', sans-serif;
            font-weight: 300;
            font-size: 15px;
        }
    </style>
    <style type="text/css">
/* Star hover using lang hack in its own style wrapper, otherwise Gmail will strip it out */
    * [lang~="x-star-wrapper"]:hover *[lang~="x-star-number"]{
        color: #119da2 !important;
        border-color: #119da2 !important;
    }

    * [lang~="x-star-wrapper"]:hover *[lang~="x-full-star"],
    * [lang~="x-star-wrapper"]:hover ~ *[lang~="x-star-wrapper"] *[lang~="x-full-star"] {
      display : block !important;
      width : auto !important;
      overflow : visible !important;
      float : none !important;
      margin-top: -1px !important;
    }

    * [lang~="x-star-wrapper"]:hover *[lang~="x-empty-star"],
    * [lang~="x-star-wrapper"]:hover ~ *[lang~="x-star-wrapper"] *[lang~="x-empty-star"] {
      display : block !important;
      width : 0 !important;
      overflow : hidden !important;
      float : left !important;
      height: 0 !important;
    }
</style>


<style type="text/css">

    *[class=rating] {
      unicode-bidi: bidi-override;
      direction: rtl;
    }
    *[class=rating] > *[class=star] {
      display: inline-block;
      position: relative;
      text-decoration: none;
    }

    @media (max-width: 621px) {
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -o-box-sizing: border-box;
        }
        table {
            min-width: 0 !important;
            width: 100% !important;
        }
        *[class=body-copy] {
            padding: 0 10px !important;
        }
        *[class=main-wrapper],
        *[class=main-content]{
            min-width: 0 !important;
            width: 320px !important;
            margin: 0 auto !important;
        }
        *[class=ms-sixhundred-table] {
            width: 100% !important;
            display: block !important;
            float: left !important;
            clear: both !important;
        }
        *[class=content-padding] {
            padding-left: 10px !important;
            padding-right: 10px !important;
        }
        *[class=bottom-padding]{
            margin-bottom: 15px !important;
            font-size: 0 !important;
            line-height: 0 !important;
        }
        *[class=top-padding] {
            display: none !important;
        }
        *[class=hide-mobile] {
            display: none !important;
        }


        /* Prevent hover effects so double click issue doesn't happen on mobile devices */
        * [lang~="x-star-wrapper"]:hover *[lang~="x-star-number"]{
            color: #AEAEAE !important;
            border-color: #FFFFFF !important;
        }
        * [lang~="x-star-wrapper"]{
            pointer-events: none !important;
        }
        * [lang~="x-star-divbox"]{
            pointer-events: auto !important;
        }
        *[class=rating] *[class="star-wrapper"] a div:nth-child(2),
        *[class=rating] *[class="star-wrapper"]:hover a div:nth-child(2),
        *[class=rating] *[class="star-wrapper"] ~ *[class="star-wrapper"] a div:nth-child(2){
          display : none !important;
          width : 0 !important;
          height: 0 !important;
          overflow : hidden !important;
          float : left !important;
        }
        *[class=rating] *[class="star-wrapper"] a div:nth-child(1),
        *[class=rating] *[class="star-wrapper"]:hover a div:nth-child(1),
        *[class=rating] *[class="star-wrapper"] ~ *[class="star-wrapper"] a div:nth-child(1){
          display : block !important;
          width : auto !important;
          overflow : visible !important;
          float : none !important;
        }
    }
</style>
  </head>

    <body style="background-color: #f1eff2;">
        <table border="0" align="center" cellpadding="0" cellspacing="0" style="background-color: #fff; height: 100%; width: 600px; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.10);">
            <tbody>
                <tr>
                    <td align="center">
                       <div style="background-color: #40b659; text-align: center; padding: 30px 30px;">
                           <img src="{{url('img/logo-round-white.png')}}" style="height: 80px; display: inline-block;" alt="">
                           <h1 style="color: #fff; margin-top: 10px; margin-bottom: 0px; font-size: 24px;">Your Order Feedback.</h1>
                           <p style="font-size: 14px; color: #fff; margin-bottom: 0px;">Thank you for your order with Zing. Hope you enjoyed your meal. Would you take a moment to give your rating for each dish as seen below? At Zing we are trying to help everyone know what is best to eat at this eatery. We are extremely grateful that you would take the time to help a fellow Zinger out. Have a Great Day!</p>
<!-- <a href='{{ trans_url("client/cart/order/feedback/".@$order->getRoutekey()) }}'>Click here to give feedback for your order</a> -->
                       </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px; text-align: center;">
                       <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0; text-align: center;">
                            <tr>
                                <td>
                                    <p style="margin: 0;">
                                        <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 5px; display: block; text-transform: capitalize;"><b>#{!!@$order['id']!!}  {{$user->name}}</b></span>
                                        <span style="font-size: 20px; color: #666666; display: block; margin-bottom: 5px; text-transform: capitalize;"><b>{!!@$order['order_type']!!} Order</b></span>
                                        <span style="font-size: 16px; color: #666666; display: block; text-transform: capitalize;">{!!@$order['order_type']!!} Time: <img src="{{url('img/calendar.png')}}" style="display: inline-block; height: 15px; vertical-align: -2px;" alt=""> {{strtolower(substr(date('l',strtotime($order->delivery_time)),0,3))}}, {{date('M d',strtotime($order->delivery_time))}}, {{date('h:i A',strtotime($order->delivery_time))}}</span>
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td align="center" style="border-bottom: 1px solid #d6d8de; padding: 20px;">
                        <span style="font-size: 16px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>Order Summary</b></span>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 20px; padding-top: 30px;">
                            <tbody>
                            	@foreach($order_detail as $detail)
                                
                                <tr style="text-align: left;">
                                      <td width="50%" style="color: #666666; line-height: 24px; font-size: 16px; padding-bottom: 5px; border-bottom: 1px solid #eaeaea;">
                                        @if(!empty($order_detail->menu))
                        <div style="width: 100px; height: 100px; border-radius: 50%; display: inline-block; margin-right: 15px; background-image: url({{url($detail->menu->defaultImage('image'))}}); background-size: contain; background-position: center;"></div>
                        @endif
                    </td>
                                    <td valign="bottom" width="50%" style="color: #666666; line-height: 24px; font-size: 16px; padding-bottom: 5px; border-bottom: 1px solid #eaeaea;"><b>@if($detail->menu_addon == 'menu' && (!empty($detail->menu)))
                                            {{@$detail->menu->name}}
                                        @elseif(!empty($detail->addon))
                                            {{@$detail->addon->name}}
                                        @endif<br>
                                        <ul style="padding-left:15px; margin: 5px 0; list-style: diamond; font-size: 14px;">
                                            @if(!empty($detail->addons))
                                                @foreach($detail->addons as $cart_addon)
                                                  <li>  {{$cart_addon[1]}} </li>
                                                @endforeach
                                            @endif
                                        </ul></br>
                                    </td> 
                                    <td width="75%" style="color: #666666; line-height: 24px; font-size: 16px; padding-bottom: 5px; border-bottom: 1px solid #eaeaea;">
                                    <table style="border-collapse: collapse;border-spacing: 0;width: 275px; margin: 0 auto; font-size: 20px; direction: rtl;" dir="rtl">
                                                    <tbody><tr>
                                                       <td style="padding: 0;vertical-align: top;" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: left; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=5&feedback_type=menu&feedback_id={!!@$detail->menu->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="1">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                    <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td style="padding: 0;vertical-align: top" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: center; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=4&feedback_type=menu&feedback_id={!!@$detail->menu->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="2">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                    <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td style="padding: 0;vertical-align: top" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: center; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=3&feedback_type=menu&feedback_id={!!@$detail->menu->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="3">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                     <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td style="padding: 0;vertical-align: top" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: center; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=2&feedback_type=menu&feedback_id={!!@$detail->menu->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="4">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                    <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                               </a>
                                                            </div>
                                                        </td>
                                                        <td style="padding: 0;vertical-align: top" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: center; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=1&feedback_type=menu&feedback_id={!!@$detail->menu->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="5">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                    <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr></tbody>
                                                </table>
                                            </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!--<table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 0px;">-->
                        <!--    <tbody>-->
                        <!--        <tr style="text-align: left;">-->
                        <!--            <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>-->
                        <!--            <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Subtotal</td>-->
                        <!--            <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['subtotal'],2)!!}</td>-->
                        <!--        </tr>-->
                        <!--        @if(@$order['tax']>0)-->
                        <!--        <tr style="text-align: left;">-->
                        <!--            <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>-->
                        <!--            <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Tax & Fees</td>-->
                        <!--            <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['tax'],2)!!}</td>-->
                        <!--        </tr>-->
                        <!--        @endif-->
                        <!--        @if(@$order['delivery_charge']>0)-->
                        <!--        <tr style="text-align: left;">-->
                        <!--            <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>-->
                        <!--            <td width="20%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Delivery</td>-->
                        <!--            <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['delivery_charge'],2)!!}</td>-->
                        <!--        </tr>-->
                        <!--        @endif-->
                        <!--        @if(@$order['tip']>0)-->
                        <!--        <tr style="text-align: left;">-->
                        <!--            <td width="65%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>-->
                        <!--            <td width="25%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">Tip</td>-->
                        <!--            <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right;">${!!number_format(@$order['tip'],2)!!}</td>-->
                        <!--        </tr>-->
                        <!--        @endif-->
                        <!--        @if(@$order['discount_points']>0)-->
                        <!--        <tr style="text-align: left;">-->
                        <!--            <td width="70%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">&nbsp;</td>-->
                        <!--            <td width="20%" style="color: #666666; padding-bottom: 10px; font-size: 16px;">Loyality Points</td>-->
                        <!--            <td width="10%" valign="top" style=" color: #666666; padding-bottom: 10px; font-size: 18px; text-align: right;">${!!number_format(@$order['discount_points']/2000,2)!!} ({{@$orders->loyalty_points}} Points)</td>-->
                        <!--        </tr>-->
                        <!--        @endif-->
                        <!--        <tr style="text-align: left;">-->
                        <!--            <td width="70%" style="color: #666666; padding-bottom: 5px; font-size: 16px;">&nbsp;</td>-->
                        <!--            <td width="20%" style="color: #333333; padding-bottom: 5px; font-size: 16px; font-weight: 600;">Total</td>-->
                        <!--            <td width="10%" valign="top" style=" color: #666666; padding-bottom: 5px; font-size: 18px; text-align: right; font-weight: 600;">${!!number_format(@$order['total'],2)!!}</td>-->
                        <!--        </tr>-->
                        <!--    </tbody>-->
                        <!--</table>-->
                         <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin: 0; text-align: left;">
                            <tr>
                                <td width="115px">
                                    <div style="width: 100px; height: 100px; border-radius: 50%; display: inline-block; margin-right: 15px; background-image: url('{{url($order->restaurant->defaultImage('logo'))}}'); background-size: contain; background-position: center;"></div>
                                </td>
                                <td>
                                    <p style="margin: 0;">
                                        <span style="font-size: 18px; color: #666666; margin-top: 0px; margin-bottom: 10px; display: block"><b>{!!@$order->restaurant->name!!}</b></span>
                                         <div style="width: 100%; text-align: left; float: left;">
                                            <div class="rating" style="text-align: left; margin: 0; font-size: 50px; width: 275px; margin-top: 10px;">

                                                <table style="border-collapse: collapse;border-spacing: 0;width: 275px; margin: 0 auto; font-size: 50px; direction: rtl;" dir="rtl">
                                                    <tbody><tr>
                                                       <td style="padding: 0;vertical-align: top;" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: left; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=5&feedback_type=restaurant&feedback_id={!!@$order->restaurant->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="1">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                    <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td style="padding: 0;vertical-align: top" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: center; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=4&feedback_type=restaurant&feedback_id={!!@$order->restaurant->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="2">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                    <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td style="padding: 0;vertical-align: top" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: center; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=3&feedback_type=restaurant&feedback_id={!!@$order->restaurant->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="3">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                     <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <td style="padding: 0;vertical-align: top" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: center; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=2&feedback_type=restaurant&feedback_id={!!@$order->restaurant->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="4">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                    <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                               </a>
                                                            </div>
                                                        </td>
                                                        <td style="padding: 0;vertical-align: top" width="55" class="star-wrapper" lang="x-star-wrapper">
                                                            <div style="display: block; text-align: center; float: left;width: 55px;overflow: hidden;line-height: 60px;">
                                                                <a href="{{trans_url('feedback/rating/')}}/{{$order->getRoutekey()}}?rating=1&feedback_type=restaurant&feedback_id={!!@$order->restaurant->id!!}" class="star" target="_blank" lang="x-star-divbox" style="color: #FFCC00; text-decoration: none; display: inline-block;height: 50px;width: 55px;overflow: hidden;line-height: 60px;" tabindex="5">
                                                                    <div lang="x-empty-star" style="margin: 0;display: inline-block;">☆</div>
                                                                    <div lang="x-full-star" style="margin: 0;display: inline-block; width:0; overflow:hidden;float:left; display:none; height: 0; max-height: 0;">★</div>
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr></tbody>
                                                </table>
                                            </div>
                                        </div>

                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" width="310" style="padding: 20px; background-color: #eff1f2;">
                        <span style="font-size: 14px;">
                            <span style="color: #999999; margin-bottom: 5px; display: block;">© 2019 ZingMyOrder LLC | 1039 I-35E Suite 304, Carrollton, TX 75006</span>
                            <a href="{{url('restaurant/login')}}" style="text-decoration: none; color: #999999; display: inline-block;">Be a Eatery Partner</a><span style="color: #999999;">&nbsp; | &nbsp;</span>
                            <a href="{{url('privacy.html')}}" style="text-decoration: none; color: #999999; display: inline-block;">Privacy Policy</a><span style="color: #999999;">&nbsp; | &nbsp;
                            <a href="{{url('terms-and-conditions.html')}}" style="text-decoration: none; color: #999999; display: inline-block;">Terms & Conditions</a></span>
                        </span>
                    </td>
                </tr>
            </tbody>
        </table>

    </body>
</html>
<script type="text/javascript">
    <script type="text/javascript">
     $('.rating').raty({
              score: function() {
              return $(this).attr('data-score');
              }
            });

</script>
</script>
