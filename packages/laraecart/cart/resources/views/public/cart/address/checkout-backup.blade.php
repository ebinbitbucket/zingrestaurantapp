@if(Cart::count() <= 0)
<?php $rest_id=0;
$delivery_charge=0;?>
<div class="empty-content-wrap">
    <div class="container">
        <img src="{{theme_asset('img/cart-empty.svg')}}" alt="">
        <h2>Your Cart is Empty</h2>
        <p>Looks like you haven't made your choice yet</p>
        <p><a href="{{url('/')}}">Go Home</a></p>
<!--         <p><a href="#" id="prev_res">Go To Eatery Page</a></p>
 -->
    </div>
</div>
@else
<section class="checkout-wrap" style="margin-top: 76px;">
    
    <div class="container">
                        <form id="frm_checkout" action="{{url('cart/delivery')}}">
<div class="row">
            <div class="col-md-8">
                    <div class="checkout-block-wrap">
                        <div class="block-item"> 
                            @foreach($carts as $cart) 
                            <?php $rest_id = $cart->options->restaurant;
                                $rest_data = Restaurant::getRestaurantData($rest_id)
                            ?>
                            @endforeach 
                              <!--   <a href="#"  class="checkout-back-btn" data-toggle="tooltip" data-placement="top" title="Back to Orders" data-original-title="Go Back" id="btn_back1" ><i class="ion-android-arrow-back"></i></a> -->
                                <a href="{{trans_url('restaurants/'.$rest_data->slug)}}"  class="checkout-back-btn" data-toggle="tooltip" data-placement="top" title="Back to Orders" data-original-title="Go Back" id="btn_back1" ><i class="ion-android-arrow-back"></i></a>
                                 <a href="javascript:void(0);" class="checkout-back-btn" data-toggle="tooltip" data-placement="top" title="Back to Orders" data-original-title="Go Back" id="btn_back" style="display:none;"><i class="ion-android-arrow-back"></i></a>
                                <div class="col-md-8 block-item-inner">
                                    <div class="row">
                                        <div class="col-md-6">
                                          
                                <h3 class="mb-20">Order Type</h3>
  </div>
                            <div class="col-md-6">
                                 <select class="form-control" id="order_type" name="order_type" onchange="typeChange();">
                                     <option>Pickup</option>
                            @if(!empty($rest_data)) 
                                @if($rest_data->delivery == 'Yes')<option>Delivery</option>@endif
                            @endif

                                 </select>
                            </div></div>
                    </div>
                                        </br>
                                <div class="col-md-8 block-item-inner">
                                    <div class="row">
                                        <div class="col-md-6">
                                    <h3 class="mb-20">Pickup/Delivery Time</h3>
                                    </div>
                                <div class="col-md-6">
                                    <div class="dropdown deltype-dropdown">
                                        <button class="btn dropdown-toggle btn-block text-left" type="button" id="deltypeMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span id="del_type_v">ASAP</span>
                                            <span id="deltypet_wrap" style="display: none;">
                                                <span id="del_date_v"></span> 
                                                <span id="del_time_v"></span>
                                            </span>
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="deltypeMenu">
                                           <div class="delivery-type-item">
                                                <input type="radio" name="del_type" value="ASAP" id="asap" checked="">
                                                <label for="asap"><i class="ion ion-android-time"></i>ASAP</label>
                                            </div>
                                            <div class="delivery-type-item">
                                                <input type="radio" name="del_type" id="schedule_order" value="Schedule Order">
                                                <label for="schedule_order"><i class="ion ion-android-calendar"></i>Schedule Order</label>
                                                <div class="schedule-inner-block" id="scedule-block">
                                                    <div class="form-group">
                                                        <p>Select Date</p>
                                                        <div class="sch-date-wrap" id="sch_DateWrap" name="datesearch">
                                                            @for($i=0;$i<5;$i++)
                                                            <div class="date-item {{(date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days')) == date('D d-M', strtotime(date('D d-M'). ' + '. 0 .'days'))) ? 'active' : ''}}" data-date="{{date('D d-M', strtotime(date('D d-M'). ' + '. $i .'days'))}}">
                                                                <span class="day">{{date('D', strtotime(date('D d-M'). ' + '. $i .'days'))}}</span>
                                                                <span class="date">{{date('d', strtotime(date('D d-M'). ' + '. $i .'days'))}}</span>
                                                            </div>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-0">
                                                        <p>Select Time</p>
                                                        <select class="selectize" id="sch_time">
                                                            <option value="">Select a Time</option>
                                                            <option>12:00 AM</option>
                                                            <option>12:15 AM</option>
                                                            <option>12:30 AM</option>
                                                            <option>12:45 AM</option>
                                                            <option>1:00 AM</option>
                                                            <option>1:15 AM</option>
                                                            <option>1:30 AM</option>
                                                            <option>1:45 AM</option>
                                                            <option>2:00 AM</option>
                                                            <option>2:15 AM</option>
                                                            <option>2:30 AM</option>
                                                            <option>2:45 AM</option>
                                                            <option>3:00 AM</option>
                                                            <option>3:15 AM</option>
                                                            <option>3:30 AM</option>
                                                            <option>3:45 AM</option>
                                                            <option>4:00 AM</option>
                                                            <option>4:15 AM</option>
                                                            <option>4:30 AM</option>
                                                            <option>4:45 AM</option>
                                                            <option>5:00 AM</option>
                                                            <option>5:15 AM</option>
                                                            <option>5:30 AM</option>
                                                            <option>5:45 AM</option>
                                                            <option>6:00 AM</option>
                                                            <option>6:15 AM</option>
                                                            <option>6:30 AM</option>
                                                            <option>6:45 AM</option>
                                                            <option>7:00 AM</option>
                                                            <option>7:15 AM</option>
                                                            <option>7:30 AM</option>
                                                            <option>7:45 AM</option>
                                                            <option>8:00 AM</option>
                                                            <option>8:15 AM</option>
                                                            <option>8:30 AM</option>
                                                            <option>8:45 AM</option>
                                                            <option>9:00 AM</option>
                                                            <option>9:15 AM</option>
                                                            <option>9:30 AM</option>
                                                            <option>09:45 AM</option>
                                                            <option>10:00 AM</option>
                                                            <option>10:15 AM</option>
                                                            <option>10:30 AM</option>
                                                            <option>10:45 AM</option>
                                                            <option>11:00 AM</option>
                                                            <option>11:15 AM</option>
                                                            <option>11:30 AM</option>
                                                            <option>11:45 AM</option>
                                                            <option>12:00 PM</option>
                                                            <option>12:15 PM</option>
                                                            <option>12:30 PM</option>
                                                            <option>12:45 PM</option>
                                                            <option>1:00 PM</option>
                                                            <option>1:15 PM</option>
                                                            <option>1:30 PM</option>
                                                            <option>1:45 PM</option>
                                                            <option>2:00 PM</option>
                                                            <option>2:15 PM</option>
                                                            <option>2:30 PM</option>
                                                            <option>2:45 PM</option>
                                                            <option>3:00 PM</option>
                                                            <option>3:15 PM</option>
                                                            <option>3:30 PM</option>
                                                            <option>3:45 PM</option>
                                                            <option>4:00 PM</option>
                                                            <option>4:15 PM</option>
                                                            <option>4:30 PM</option>
                                                            <option>4:45 PM</option>
                                                            <option>5:00 PM</option>
                                                            <option>5:15 PM</option>
                                                            <option>5:30 PM</option>
                                                            <option>5:45 PM</option>
                                                            <option>6:00 PM</option>
                                                            <option>6:15 PM</option>
                                                            <option>6:30 PM</option>
                                                            <option>6:45 PM</option>
                                                            <option>7:00 PM</option>
                                                            <option>7:15 PM</option>
                                                            <option>7:30 PM</option>
                                                            <option>7:45 PM</option>
                                                            <option>8:00 PM</option>
                                                            <option>8:15 PM</option>
                                                            <option>8:30 PM</option>
                                                            <option>8:45 PM</option>
                                                            <option>9:00 PM</option>
                                                            <option>9:15 PM</option>
                                                            <option>9:30 PM</option>
                                                            <option>9:45 PM</option>
                                                            <option>10:00 PM</option>
                                                            <option>10:15 PM</option>
                                                            <option>10:30 PM</option>
                                                            <option>10:45 PM</option>
                                                            <option>11:00 PM</option>
                                                            <option>11:15 PM</option>
                                                            <option>11:30 PM</option>
                                                            <option>11:45 PM</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" name="scheduled_date" id="datecheck">
                                    <input type="hidden" name="scheduled_time" id="timecheck">
                                    
                                </div>
                               
</div></div>
                                                    
                        </div>
                        <div class="block-item">
                            <div class="col-md-12 block-item-inner">
                                <div class="row">
                                    <div class="col-md-4">
                                    <h3 class="mb-20">Address</h3>
                                    </div>
                                <div class="col-md-8">
                                    <div class="address-block" style="display: none;" id="address_delivery">
                                        <div class="saved-address" id="saved_address">
                                            @forelse(Cart::getAddresses() as $address)
                                            <?php $flag=0; ?>
                                            <div class="address-item">
                                                <input type="radio" id="address_{{$address->id}}" name="address_id" onclick="address_radio('{{$address->id}}')" {{$address->default_address == 1 ? 'checked' : ''}} value="{{$address->id}}">
                                                <label for="address_{{$address->id}}">
                                                    <h3>{{$address->title}}</h3>
                                                    <p>{{$address->address}}</p>
                                                </label>
                                            </div>
                                            @empty
                                            <?php $flag=1; ?>
                                            @endif
                                        </div>
                                        @if($flag==1)
                                                <button type="button" onclick="add_address()" id="btn_add_address_form" name="button" class="btn change-address-btn">Add Address</button>
                                        @else
                                            <button type="button" onclick="add_address()" id="btn_add_address_form" name="button" class="btn change-address-btn">Add or Change Address</button>
                                        @endif
                                    </div>
                                         <div class="address-block" id="address_pickup">
                                        <div class="saved-address">
                                            <div class="address-item">
                                                
                                                <label for="label_address_pickup">
                                                    <h3>Pickup Address</h3>
                                                    <div class="restaurant-info-item">
                                                        @if(!empty($rest_data->logo))
                                                        <div class="rest-logo" style="background-image: url('{{trans_url('image/original/'.$rest_data->logo[0]['path'])}}')"></div>
                                                        @endif
                                                        <div class="rest-info">
                                                            <p>{{!empty($rest_data) ? $rest_data->address : ''}}</p>
                                                        </div>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    </div></div>
                                    <input type="hidden" id="latitude_search">
                                    <input type="hidden" id="longitude_search">
                                    <input type="hidden" name="cus_name" class="form-control"  placeholder="Full Name" required="required" value="{{user()->name}}">
                                    <input type="hidden" name="phone" class="form-control"  placeholder="Phone Number" required="required" value="{{user()->mobile}}" pattern="[0-9]{10}">
                                </div>
                        </div>
                        <div class="block-item" id="billing_block">
                            <div class="col-md-12 block-item-inner">
                                 <div class="row">
                                    <div class="col-md-4">
                                        <h3 class="mb-20">Billing Address</h3>
                                    </div>
                                    <div class="col-md-8">
                                    <div class="address-block">
                                        <div class="saved-address" id="saved_address1"> 
                                            @forelse(Cart::getBillingAddresses() as $address)
                                            <?php $flag=0; ?>
                                            <div class="address-item">
                                                <input type="radio" id="address_{{$address->id}}" name="billing" class="billing_address_id" onclick="address_radio('{{$address->id}}')" checked value="{{$address->id}}">
                                                <label for="address_{{$address->id}}">
                                                    <h3>{{$address->title}}</h3>
                                                    <p>{{$address->address}}</p>
                                                </label>
                                              <input type="hidden" name="billing_address_detail" value="{{$address->title}},{{strtok($address->address,',')}}">  
                                        </div>
                                           @empty
                                            <?php $flag=1; ?>
                                        @endif 
                                     </div>
                                        @if($flag==1)
                                                <!-- <button type="button" data-toggle="modal" href="#address_model1" id="btn_add_billing_address_form" name="button" class="btn change-address-btn btn_add_billing_address_form">Add Address</button> -->
                                                 <button type="button" onclick="billing_address_popup();" id="btn_add_billing_address_form" name="button" class="btn change-address-btn btn_add_billing_address_form">Add Address</button>
                                        @else
                                            <button type="button" data-toggle="modal" href="#address_model1" name="button" class="btn change-address-btn btn_add_billing_address_form">Add or Change Address</button>
                                     @endif
                                   </div>
                                    
                               </div> 

                                        </div>
                                    </div>
                                </div>
                            <div id="card_det"></div>
                            <!-- <div class="row">
                                <div class="col-md-4"></div>
                               <div class="col-md-6"><a href="" id="edit_orders" value="online" class="btn btn-default btn-block mt-20 " style="display: none;">Edit Order</a></div>
                              </div> -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="payment-wrap" id="payment_summary">
                        <div class="order-summary">
                             @foreach($carts as $cart) 
                            <?php $rest_name = $cart->options->restaurantname;
                                  $rest_logo = $cart->options->restaurantimage; ?>
                            @endforeach 

                            <div class="order-restaurant">
                                @if(!empty($rest_logo))
                                <div class="rest-img" style="background-image: url('{{trans_url('image/original/'.$rest_logo[0]['path'])}}')"></div>
                                @endif
                                <div class="rest-info">
                                    <h3><a href="{{trans_url('restaurants/'.$rest_data->slug)}}">{{$rest_name}}</a></h3>
                                </div>
                            </div>
                                                          
                            <h3>Summary</h3>
                            <?php $delivery_charge =0; ?>
                            @foreach($carts as $cart) 
                            <div class="item" style="{{$cart->options['menu_addon'] == 'addon' ? 'margin-left: 20px' : ''}}">
                                <div class="item-desc">
                                    <p data-toggle="tooltip" data-placement="bottom" title="{{$cart->name}}"><b>{{str_limit($cart->name,17)}}</b></p>
                                     @if(!empty($cart->options['addons']))
                                    @foreach($cart->options['addons'] as $cart_addon)
                                     <div class="item-desc">
                                    <p data-toggle="tooltip" data-placement="bottom" title="{{$cart_addon[1]}}">{{str_limit($cart_addon[1],17)}}</p>
                                    <!-- {{$cart->options['menu_addon']}} -->
                                </div>
                                    @endforeach
                                @endif
                                </div>
                                @if($cart->options['menu_addon'] != 'addon')
                                <!--<div class="item-price">-->
                                <!--    <div class="row">-->
                                <!--        <div class="col-7">-->
                                <!--    x<span> -->
                                <!--        <span class="enumerator">-->
                                <!--            <span class="enumerator_btn js-minus_btn" onclick="decrement('{{$cart->rowId}}')" id="minus_span">-</span>-->
                                <!--            <input type="number" name="quantity[{{$cart->rowId}}]" value="{{$cart->qty}}" class="enumerator_input" style="width: 30px;" readonly="readonly">-->
                                <!--            <span class="enumerator_btn js-plus_btn"  onclick="increment('{{$cart->rowId}}')" id="plus_span">+</span>-->
                                            
                                <!--        </span>-->
                                <!--    - </span>-->
                                 <div class="item-price">
                                    <div class="qty-price-wrap">
                                        <div class="qty-price">
                                            <span> 
                                                <span class="enumerator">
                                                    <span class="enumerator_btn js-minus_btn" onclick="decrement('{{$cart->rowId}}')" id="minus_span">-</span>
                                                    <input type="number" name="quantity[{{$cart->rowId}}]" value="{{$cart->qty}}" class="enumerator_input" style="width: 30px;" readonly="readonly">
                                                    <span class="enumerator_btn js-plus_btn"  onclick="increment('{{$cart->rowId}}')" id="plus_span">+</span>
                                                    
                                                </span>
                                            -</span>
                                    <?php 
                                        $addon_price = 0;
                                            if(!empty($cart->options['addons'])){
                                        foreach($cart->options['addons'] as $cart_addon){
                    
                                                $addon_price = $addon_price + $cart_addon[2];
                                        }
                                                       
                                       }
                                    ?>
                                    </div><span>${{number_format($cart->price+$addon_price,2)}}</span>
                                     <div class="col-2">
                                    <a href="{{url('carts/remove/'. $cart->rowId)}}"class="remove-btn"><i class="ion-android-close" title="Remove this item from the cart"></i></a>
                                </div>
                            </div>
                                </div>@endif
                            </div>
                            <?php 
                            if($cart->options['delivery_charge'] != '')
                            { $delivery_charge = $cart->options['delivery_charge']; }
                            ?>
                            @endforeach
                        </div>
                        <hr>
                        <div class="payment-breakdown-wrap mt-20">
                            <div  id="payment_details">
                            <div class="break-down-items">
                                <span>Subtotal</span>
                                <span>${{number_format(Cart::total(),2)}}</span>
                            </div>
                            <div class="break-down-items">
                                <span>Tax and Fees <a href="#" class="text-secondary" data-toggle="tooltip" data-placement="top" data-html="true" title="<p class='m-0 text-left'>Local Tax = {{!empty($rest_data) ? number_format($rest_data->tax_rate,2) : ''}}%</p><p class='m-0 text-left'>Zing Service = ${{!empty($rest_data) ? number_format($rest_data->ZC,2) : ''}}</p>"><i class="ion-android-alert"></i></a></span>
                                <span>${{number_format(Cart::total()*(!empty($rest_data) ?  $rest_data->tax_rate/100 : 0)+(!empty($rest_data) ?  $rest_data->ZC : 0),2)}}</span>
                                
                            </div>
                            <input type="hidden" id="tax" name="tax" value="{{Cart::total()*(!empty($rest_data) ?  $rest_data->tax_rate/100 : 0)+(!empty($rest_data) ?  $rest_data->ZC : 0)}}">
                            <div class="break-down-items" id="delivery_div">
                                <span>Delivery</span>
                                <span id="delivery_amount_span">${{number_format($delivery_charge,2)}}</span>
                            </div>
                            <div class="break-down-items">
                                <span>Tip</span>
                                <span id="tip_charge">$0.00</span>
                            </div>
                            <div class="tip-block">
                                <input type="hidden" id="tip" name="tip">
                                <div class="tip-item" style="width: 40px;"> 
                                    <input type="radio" id="tip_0" name="tip_value" value=0 class="tipcal">
                                    <label for="tip_0">0%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;"> 
                                    <input type="radio" id="tip_5" name="tip_value" value=5 class="tipcal" checked="checked">
                                    <label for="tip_5">5%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;">
                                    <input type="radio" id="tip_10" name="tip_value" value=10 class="tipcal">
                                    <label for="tip_10">10%</label>
                                </div>
                                <div class="tip-item" style="width: 40px;">
                                    <input type="radio" id="tip_20" name="tip_value" value=20 class="tipcal">
                                    <label for="tip_20">20%</label>
                                </div>
                                <div class="tip-item" >
                                    <input type="radio" name="tip" id="custom" value="">
                                    <label for="custom">Custom</label>
                                </div>
                                <div class="tip-item" style="width: 60px; height: 30px;">
                                    <input type="number" class="form-control" name="tip_value_cus" id="customper" style="display: none;" min=0 max=100>
                                </div>
                               <!--  <input type="text" class="form-control" placeholder="Enter custom tip" name="tipper" id="customper" style="display: none;"> -->
                            </div>
                            <hr>  
                            <div class="break-down-items points">
                                <span>Points <a href="#" onclick="redeem()" >Redeem Points</a></span>
                                <span id="points_redeem">$0.00</span>
                            </div>
                            <input type="hidden" id="discount" name="discount">
                            <hr>
                            <div class="break-down-items total"> 
                                <span>Total Amount</span>
                                <div class="text-right"><b>$</b><span id="total_charge">{{number_format(Cart::total(),2)}}</span></div>
                            </div>
                        </div>
                        <input type="hidden" id="total_amount" name="total_amount">
                    </div>
                        <!--<button type="submit" value="cash" name="submit" class="btn btn-theme btn-block mt-20 col-md-6">Cash On Delivery</button>-->
                        <input type="hidden" name="submit" id="submit">
                             <button type="button" id="btn_getToken" value="online" name="submit" class="btn btn-theme btn-block mt-20 ">Pay Online</button>
                              
                        <!--<button type="submit" value="success" name="submit" class="btn btn-theme btn-block mt-20">Pay Now</button>-->
                       <!--  <button type="submit" value="failed" name="submit" class="btn btn-danger btn-block">Failed</button> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>  
@endif
 <div class="modal fade login-modal" id="EmptyCartModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body">
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                            <div class="login-header text-center">
                                <p>Your Cart is Empty</p> 
                            </div>
                            <div class="text-center login-footer">
                               <a href="{{trans_url('/')}}" class="btn btn-theme search-btn">OK</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>       



<div class="modal fade" id="address_model" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">
                @include('notifications')
                <div class="saved-address" id="popup_address_list">
                    @foreach(Cart::getAllAddresses() as $address)
                    <div class="address-item">
                        <input type="radio" id="address_{{$address->id}}" name="address_id" onclick="address_radio('{{$address->id}}')" {{$address->default_address == 1 ? 'checked' : ''}} value="{{$address->id}}">
                        <label for="address_{{$address->id}}">
                            <h3>{{$address->title}}</h3>
                            <p>{{$address->address}}</p>
                        </label>
                    </div>
                    @endforeach
                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="button" onclick="add_form_show()">Add Address</button>
                    </div>
                </div>
                <div id="popup_address_add" class="add-address" style="display: none;">
                    <div id="locationMap" class="mb-15" style="width: 100%; height: 250px;"></div>
                    {!!Form::vertical_open()
                    ->method('POST')
                    ->id('add-address-form')
                    ->action(trans_url('client/cart/address'))!!}
                    <div class="form-group">
                        <label for="name">Address*</label>
                        <input id="address" type="text" name="address" class="form-control" required="required">
                        <input type="hidden" name="latitude" id="latitude_address">
                        <input type="hidden" name="longitude" id="longitude_address">
                        <input type="hidden" name="type" value="delivery">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Suite/Unit Number</label>
                                <input type="text" class="form-control" name="apartment_no">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Address Name*</label>
                                <input type="text" class="form-control" name="title" placeholder="Office" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Delivery Notes</label>
                        <input type="text" class="form-control" name="notes">
                    </div>
                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="button" value="checkout" name="btn_add_address" id="btn_add_address" >Save Address</button>
                        <button class="btn btn-secondary" type="button" onclick="list_form_show()">Back</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="redeem_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Redeem Amount</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body redeem-points-modal">
                @include('notifications')
                <p class="mb-0">You have <span>{{Cart::getLoyaltypoints()}}</span> Total Points</p>
                <p>Your points can be redeemed for <span>${{number_format(Cart::getLoyaltypoints()/2000,2)}}</span></p>
                <input type="number" class="form-control mb-15" id="redeem_amount" value="{{Cart::total()}}" min=1>
                <div class="btn-wrap modal-footer justify-content-start">
                    <button class="btn btn-theme" type="button" onclick="apply_redeem()" >Apply</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade errorModel" id="ErrorModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
           
        </div>
    </div>
</div>
      <div class="modal fade login-modal location-modal" id="SessionModel" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body">
                <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                                <div class="login-header text-center">
                                  <h5 id="error_msg">This Eatery requires a minimum order of <span id="min_amt"></span></h5>
                                </div>
                                
                                <div class="text-center" >
                                   <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" id="favourite_submit" data-dismiss="modal">Ok</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade errorModel" id="address_model1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">
                @include('notifications')
                <div class="saved-address" id="popup_address_list1">
                    @foreach(Cart::getAllBillingAddresses() as $address)
                    <div class="address-item">
                        <input type="radio" class="close" data-dismiss="modal" onclick="address_radio1('{{$address->id}}')" id="address_{{$address->id}}" name="address_id"  {{$address->default_address == 1 ? 'checked' : ''}} value="{{$address->id}}" >
                        <label for="address_{{$address->id}}">
                            <h3>{{$address->title}}</h3>
                            <p>{{$address->address}}</p>
                        </label>
                    </div>
                    @endforeach
                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="button" onclick="add_form_show1()">Add Address</button>
                    </div>
                </div>
                <div id="popup_address_add1" class="add-address"  style="display: none;">
<!--                     <div id="locationMap" class="mb-15" style="width: 100%; height: 250px;"></div>
 -->                    {!!Form::vertical_open()
                    ->method('POST')
                    ->id('add-billing-address-form')
                    ->action(trans_url('client/cart/address'))!!}
                    <div class="form-group">
                        <label for="name">Address</label>
                        <input id="billing_address" type="text" name="address" class="form-control">
                        <input type="hidden" name="latitude" id="latitude_billing_address">
                        <input type="hidden" name="longitude" id="longitude_billing_address">
                        <input type="hidden" name="type" value="billing">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Suite/Unit Number</label>
                                <input type="text" class="form-control" name="apartment_no">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Address Name</label>
                                <input type="text" class="form-control" name="title" value="Home">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Delivery Notes</label>
                        <input type="text" class="form-control" name="notes">
                    </div>
                     <input type="hidden" name="type" value="billing">

                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="button" value="checkout" name="btn_add_address1" id="btn_add_address1" >Save Address</button>
                        <button class="btn btn-secondary" type="button" onclick="list_form_show1()" >Back</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
<script type="text/javascript">
     function add_form_show1() { 
     $('#popup_address_list1').hide();
    $('#popup_address_add1').show();
    }
</script>            
        </div>
    </div>
</div>
<!-- <script src="{{ theme_asset('js/bootstrap-datepicker.min.js') }}"></script>

 -->
 <style>
    .pac-container {
        z-index: 10000 !important;
    }
    @media (min-width: 576px) {
        .modal-dialog {
            max-width: 500px;
        }
    }

</style>
<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>            
        <script>
             var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        var billing_places = new google.maps.places.Autocomplete(document.getElementById('billing_address'));

        google.maps.event.addListener(places, 'place_changed', function () {
                geocodeAddress(geocoder);
        });
        google.maps.event.addListener(billing_places, 'place_changed', function () {
                geocodeAddress1(geocoder);
        });
    });
     function geocodeAddress(geocoder) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
            function geocodeAddress1(geocoder) { 
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
            var map;
            var marker;
            var myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();
            
            function initialize(){
                if(document.getElementById('latitude_address').value != ''){
                    var myLatlng = new google.maps.LatLng(document.getElementById('latitude_address').value,document.getElementById('longitude_address').value);                    

                }
                else{
                    var myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
                }
                var mapOptions = {
                    zoom: 18,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
               
                map = new google.maps.Map(document.getElementById("locationMap"), mapOptions);
                
                marker = new google.maps.Marker({
                    map: map,
                    position: myLatlng,
                    draggable: true 
                });     
                


                geocoder.geocode({'latLng': myLatlng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });

                               
                google.maps.event.addListener(marker, 'dragend', function() {
                    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#address').val(results[0].formatted_address);
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                          lat: position.coords.latitude,
                          lng: position.coords.longitude
                        };
                        infowindow.setPosition(pos);
                        infowindow.setContent('Location found.');
                        infowindow.open(map);
                        map.setCenter(pos);
                    }, function() {
                        handleLocationError(true, infowindow, map.getCenter());
                    });
                } else {
                    handleLocationError(false, infowindow, map.getCenter());
                }
            }
            
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>            

 <script type="text/javascript">
    $( document ).ready(function() {
        // if('<?php echo Cart::count(); ?>' <= 0 ){
        //       $('#EmptyCartModel').modal({show:true, backdrop: 'static',
        //     keyboard: false}); 
        // }


     var nowDate = new Date();
      
       var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate(), 0, 0, 0, 0);
var endDates =  new Date(nowDate.getFullYear(), nowDate.getMonth() +2, +0);
//        $('.datetimepicker1').datepicker({
//             stepping: 5,
//            startDate: today,
//            endDate: endDates,
//            format: 'yyyy-mm-dd',
//            buttons : {
//                 showClose: true
//             }
//        });
  $('.datetimepicker').each(function(){
        $(this).datetimepicker({
            stepping: 5,
            minDate: new Date(),
            maxDate: endDates,
            buttons : {
                showClose: true
            }
        })
    });
// $(document).on('click','#asap',function(){ 
// document.getElementById('scheduled_time').disabled = true; });
// $(document).on('click','#scheduled',function(){ 
// document.getElementById('scheduled_time').disabled = false; });
});
$(document).ready(function() { 
    if ('<?php echo Session::has('error'); ?>')
    {
        var restaurant_id = '<?php echo $rest_id; ?>';
        $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
                $('#ErrorModel').modal({show:true}); 
            });
        // $('#ErrorModel').modal({show:true, backdrop: 'static',
        //     keyboard: false});
    }
    var total = '<?php echo Cart::total(); ?>'; 
        var tip = (total * 5)/100;
        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
        document.getElementById("tip").value=tip;
        var grandtotal = '<?php echo Cart::total()+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0); ?>';
        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip));



    $('#delivery_div').hide();
    document.getElementById("total_charge").textContent=('<?php echo number_format(Cart::total()+$delivery_charge+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0)+(5/100)*Cart::total(),2); ?>');
        var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
   
    document.getElementById("total_charge").textContent=(document.getElementById("total_charge").textContent-parseFloat(delivery_charge)).toFixed(2);
    var f = '<?php echo (date('H:i',strtotime(Session::get('search_time')))); ?>';
    var s = '<?php echo date('H:i'); ?>';
    
    if('<?php echo !empty(Session::get('search_time')); ?>'){
        if(f<s){
            document.getElementById('timecheck').value = '<?php echo date('g:i A'); ?>';
            //ew Date().toLocaleTimeString();
            // $('input[name="del_type"]').val('ASAP');
        }
        else{
            document.getElementById('timecheck').value = '<?php echo date('g:i A',strtotime(Session::get('search_time'))); ?>';
            $('input[name="del_type"]').val('Schedule Order');
            
        }
        document.getElementById('datecheck').value = '<?php echo date('D d-M',strtotime(Session::get('search_time'))); ?>';
        $("#deltypet_wrap").show();
        if(document.getElementById('datecheck').value == '{{date('D d-M')}}')
             $("#del_type_v").html('Today');
        else
                $("#del_type_v").html(document.getElementById('datecheck').value);
        $("#del_time_v").html(document.getElementById('timecheck').value);
}
else{
     document.getElementById('timecheck').value = '<?php echo date('h:i A'); ?>';
     //new Date().toLocaleTimeString();
    document.getElementById('datecheck').value = '<?php echo date('D d-M'); ?>';
}

    $('#saved_address').disabled = true;
    $('#delivery_amount_span').text('$0.00');
            $('#btn_add_address_form').hide();

    $("#asap").on('click', function(){ 
        $('input[name="del_type"]').val('ASAP');
        document.getElementById('timecheck').value = '<?php echo date('h:i A'); ?>';
        //new Date().toLocaleTimeString();
        document.getElementById('datecheck').value = '<?php echo date('m/d/Y'); ?>';
        //new Date().toLocaleDateString();
    })
    $("#sch_time").on('change', function(){ 
        var c_Time = $(this).find(":checked").val();  
        document.getElementById('timecheck').value = c_Time;
        var restaurant_id = '<?php echo $rest_id; ?>';
         $.ajax({
            url: "{{ URL::to('cart/checkout/restaurant_time')}}"+'/'+restaurant_id+'/'+document.getElementById('datecheck').value+'/'+c_Time,
            success: function(response){
                if(response.status == 'Error'){
                    var restaurant_id = '<?php echo $rest_id; ?>';
        $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
                $('#ErrorModel').modal({show:true}); 
            });
                }
            }
        });
    })
    $('#sch_time').selectize({
        maxItems: 1,
    });

    $('input[name="del_type"]').click(function() { 
        if($(this).attr('id') == 'schedule_order') {
            $('#scedule-block').show();
            $("#del_date_v").html('Today');
            $("#del_type_v").hide();     
            var radioValue = $(this).val(); 
            if($(this).val() == 'Schedule Order'){
                  $("#deltypet_wrap").hide();
                  $("#del_type_v").show(); 
                $("#del_type_v").html(radioValue);

            }
            else{
                $("#deltypet_wrap").show();
            }
        }
        else {
            $('#scedule-block').hide();  
            $("#del_type_v").show(); 
            $("#deltypet_wrap").hide();
        }
    });
    $(".deltype-dropdown .dropdown-menu").click(function(e){
        e.stopPropagation();
    });

    $('input[name="del_type"]').click(function() { 
          if($(this).attr('id') == 'asap'){
                        $("#del_type_v").html('ASAP')
                    }
                    else{  
                        var radioValue = $(this).val();
 $("#del_type_v").html(radioValue)
                    }
    });
    var schDateitem = $("#sch_DateWrap .date-item");
    $(schDateitem).click(function() { 
        var c_Date = $(this).attr("data-date");
        document.getElementById('datecheck').value = c_Date;
        $("#sch_DateWrap .date-item").removeClass("active");
        $(this).addClass("active");
        // $("#del_date_v").html(c_Date)
        if(c_Date == "{{date('D d-M')}}")
            $("#del_date_v").html('Today')
        else
                $("#del_date_v").html(c_Date)
        $("#del_date_v").hide();
        var t_date = new Date(); 
        if(new Date(c_Date).getDate() == t_date.getDate()){
            var t_timestring = t_date.getTime();
            var t_time = (new Date(t_timestring).toLocaleTimeString("en-US"));
            var selectobject=document.getElementById("sch_time-selectized");
            // for (var i=0; i<selectobject.length; i++){
            //     alert(selectobject.options[i].text+" "+selectobject.options[i].value)
            // }
        }
       
    });
    $('#sch_time').on('change', function() { 
        $("#deltypet_wrap").show();
          $("#del_type_v").hide();
          $("#del_date_v").show();
        var c_Time = $(this).find(":checked").val(); 
        var c_Date = $(this).attr("data-date");
        $("#del_date_v").html(c_Date)
        $("#del_time_v").html(c_Time);

    });
});
    $('.tipcal').on('click', function() { 
        if(flag_qty!=1){
        document.getElementById('customper').style = "display:none;";
        document.getElementById('customper').value = "";
        var total = '<?php echo Cart::total(); ?>'; 
        var tip = (total * $(this).val())/100;
        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
        document.getElementById("tip").value=tip;
         if($('#order_type').val() != 'Pickup'){
        var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
    }
    else{
        var delivery_charge = 0;
    }
        var grandtotal = '<?php echo Cart::total()+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip)).toFixed(2);
        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip));
    }
}); 
    $('#custom').on('click', function() { 
        if(flag_qty!=1){
        document.getElementById('customper').style = "display:block;";
    }
    });    
    
    $('#customper').on('input', function() {
if(document.getElementById('customper').value >= 0 && document.getElementById('customper').value <=100){
    var total = '<?php echo Cart::total(); ?>';
        var tip = (total * document.getElementById('customper').value)/100;
        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
        document.getElementById("tip").value=tip;
         if($('#order_type').val() != 'Pickup'){
var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';    }
    else{
        var delivery_charge = 0;
    }
        
        var grandtotal = '<?php echo Cart::total()+(!empty($rest_data) ? ($rest_data->tax_rate/100)*Cart::total() : 0)+(!empty($rest_data) ? $rest_data->ZC:0); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip)).toFixed(2);
        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(delivery_charge)+parseFloat(tip));
}
        
    });
</script>
<script type="text/javascript">
    function add_address(){

       var add = '{{user()->addresses}}'; 

        $('#address_model').modal({show:true, backdrop: 'static',
            keyboard: false}); 
        if(add == '' || add == null){
            document.getElementById('popup_address_list').style.display = 'none';
            document.getElementById('popup_address_add').style.display = 'block';
        }
       
    }

     function address_radio($id){
        $('#address_model').modal('hide');
        $.get("{{trans_url('client/cart/saved_address')}}/"+$id, function(data) { $('#saved_address').html(data); });
    };

    function add_form_show() {
        document.getElementById('popup_address_list').style.display = 'none';
        document.getElementById('popup_address_add').style.display = 'block';
    }

     function list_form_show() {
        document.getElementById('popup_address_list').style.display = 'block';
        document.getElementById('popup_address_add').style.display = 'none';
    }

      function list_form_show1() {
        document.getElementById('popup_address_list1').style.display = 'block';
        document.getElementById('popup_address_add1').style.display = 'none';
    }

    function redeem() {
        if(flag_qty!=1){
        $('#redeem_modal').modal({show:true, backdrop: 'static',
            keyboard: false}); 
    }
    }

    function apply_redeem() {
        var red_amount = document.getElementById('redeem_amount').value;
        var tot_points = '<?php echo Cart::getLoyaltypoints(); ?>';
        var cart_total = '<?php echo Cart::total(); ?>';
         if(tot_points/2000 < red_amount){
            alert('Amount exceed the limit', 'Error');
            $('#redeem_modal').modal('hide')
            return false;
        }
        if(Number(red_amount) > Number(cart_total)){
            alert('Amount exceeded the cart amount');
            $('#redeem_modal').modal('hide')
            return false;
        }
        if(Number(red_amount) <= 0){
            alert('Please enter a valid amount');
            $('#redeem_modal').modal('hide')
            return false;
        }
        else{
            document.getElementById("points_redeem").textContent='$'+(parseFloat(red_amount).toFixed(2)); 
            $('#redeem_modal').modal('hide')
            document.getElementById('discount').value = parseFloat(red_amount);
            var total_amount = Number($('#total_charge').text());
            var final_amount = Number($('#total_charge').text())-Number(red_amount);
            $('#total_charge').html(final_amount.toFixed(2));
            document.getElementById('total_amount').value = final_amount;
        }
      
        
    }
    if ($('.enumerator').length > 0 ) { 
        $(".js-minus_btn").on('click', function() {
            if(flag_qty!=1){
            var inputEl = jQuery(this).parent().children().next();
            var qty = inputEl.val();
            if(qty!=1){
                if (jQuery(this).parent().hasClass("js-minus_btn"))
                    qty++;
                else
                    qty--;
                if (qty < 0)
                    qty = 0;
                inputEl.val(qty);
            }
        }
        });


        $(".js-plus_btn").on('click', function() { 
            if(flag_qty!=1){
                var inputEl = jQuery(this).parent().children().next();
            var qty = inputEl.val();
            if (jQuery(this).hasClass("js-plus_btn"))
                qty++;
            else
                qty--;
            if (qty < 0)
                qty = 0;
            inputEl.val(qty);
            }
            
        });
    }
    function update_cart(rowid){  
        var qty = document.getElementById(rowid).value;
            $('#payment_details').load('{{trans_url('cart/update')}}/'+qty+'/'+rowid);
           
        } 
         function increment(rowid){
            if(flag_qty!=1){
             $('#payment_details').load('{{trans_url('carts/add')}}/'+rowid);
         }
            
            //  $('#total_charge').html(final_amount.toFixed(2));
            // document.getElementById('total_amount').value = final_amount;
         }
        
          function decrement(rowid){
            if(flag_qty!=1){
             $('#payment_details').load('{{trans_url('carts/subtract')}}/'+rowid);
         }
         }     
           
   function typeChange() {
       if($('#order_type').val() == 'Pickup'){
            $('#saved_address').disabled = true;
            $('#address_delivery').hide();
            $('#address_pickup').show();
            $('#btn_add_address_form').hide();
            document.getElementsByName('address_id').value='';
            $('#delivery_amount_span').text('$0.00');
            $('#delivery_div').hide();
            
            
       }
       else{
            $('#saved_address').disabled = false;
            $('#address_pickup').hide();
            $('#address_delivery').show();
            $('#btn_add_address_form').show();
             $('#delivery_div').show();
            $('#delivery_amount_span').text('$<?php echo number_format($delivery_charge,2); ?>');

       }
       
    if($('#order_type').val() != 'Pickup'){
        var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(document.getElementById("total_charge").textContent)+parseFloat(delivery_charge)).toFixed(2);
    }
    else{
        var delivery_charge = '<?php echo (empty($delivery_charge) ? 0 : $delivery_charge); ?>';
        document.getElementById("total_charge").textContent=(parseFloat(document.getElementById("total_charge").textContent)-parseFloat(delivery_charge)).toFixed(2);
    }
    
   }
var flag_qty=0;
   $(document).on('click','#btn_getToken',function(){ 
            document.getElementById('submit').value = 'online';
            var $f = $('#frm_checkout');
            $.getJSON({
                  type: 'GET',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    console.log(data);
                     if(data.status == 'Error'){
                        if(data.msg == 'min_order_count'){
                            document.getElementById("min_amt").textContent="$"+data.min_amt;

                                   $('#SessionModel').modal({show:true});  
                        }
                        else if(data.msg == 'delivery_limit'){
                                   $('#SessionModel').modal({show:true});  
                                   $('#error_msg').html('Your address is outside the delivery limit of '+data.delivery_limit+' miles for the Eatery');
                        }
                         else if(data.msg == 'Billing address'){
                                   $('#SessionModel').modal({show:true});
                                   $('#error_msg').html('Please complete your billing address');
                        }
                           else if(data.msg == 'Previous Time'){
                                   $('#SessionModel').modal({show:true});
                                   $('#error_msg').html('Pickup/Delivery time should be a future Date and Time');
                        }
                        else{
                            var restaurant_id = '<?php echo $rest_id; ?>';
                            $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
                                $('#ErrorModel').modal({show:true}); 
                            });
                        }
                    }
                    else{
                        $('#card_det').html(data.view)
                        $('#btn_getToken').hide();
                        $('#edit_orders').attr('href',"{{url('cart/checkout_edit')}}"+ "/" + data.id);
                        $('#edit_orders').show();
                        $('#btn_back').show();
                        $('#btn_back1').hide();
                        flag_qty=1;
                        $('.remove-btn').disabled = true;
                        $('.btn_add_billing_address_form').hide();
                        const elements = document.getElementsByClassName('remove-btn');
                        for (const element of elements) {
                            element.href = '#';
                        }
                        jQuery("#card_det").attr("tabindex",-1).focus();
                        document.getElementById('card').focus();
                         $('.tip-block *').prop('disabled', true);
                        $('.block-item-inner *').prop('disabled', true);

                    }
                    
                             
                  },
                  error: function(xhr, status, error) {
                    console.log('hi');
                    var restaurant_id = '<?php echo $rest_id; ?>';
        $('.errorModel .modal-content').load("{{url('restaurant/cart/checkout')}}"+'/'+restaurant_id,function(){ 
                $('#ErrorModel').modal({show:true}); 
            });
}
              });
           
        });
   $('#btn_back').click(function(){ 
    document.location.reload();
   })
     $('#btn_back1').click(function(){ 
    window.location=document.referrer;
   })

   //   $('#prev_res').click(function(){ 
   // window.history.back(-2);
   // location.reload();
   // })
       $(document).on('click','#btn_add_address',function(){ 
            var $f = $('#add-address-form'); console.log($f.serialize());
            $.getJSON({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    // $('#popup_address_list').load('');
                    document.getElementById('popup_address_list').style.display = 'block';
                    document.getElementById('popup_address_add').style.display = 'none';
                    $('#address_model').modal('hide');
                    $( "#popup_address_list" ).load(window.location.href + " #popup_address_list" );
                    $.get("{{trans_url('client/cart/saved_address')}}/"+data.address, function(data) { $('#saved_address').html(data); });
                    // $('#card_det').disabled = false;
                    //document.location.reload();
                  
                  },
                  error: function(xhr, status, error) {
                    
                }
              });
           
        });
        
        $(document).on('click','#btn_add_address1',function(){ 
            var $f = $('#add-billing-address-form'); console.log($f.serialize());
            $.getJSON({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    // $('#popup_address_list').load('');
                    document.getElementById('popup_address_list1').style.display = 'block';
                    document.getElementById('popup_address_add1').style.display = 'none';
                    $('#address_model1').modal('hide');
                    $( "#popup_address_list1" ).load(window.location.href + " #popup_address_list1" );
                    $.get("{{trans_url('client/cart/saved_billing_address')}}/"+data.address, function(data) { $('#saved_address1').html(data); });
                    // $('#card_det').disabled = false;
                    //document.location.reload();
                  
                  },
                  error: function(xhr, status, error) {
                    
                }
              });
           
        });

        function billing_address_popup(){ 
            $('.errorModel .modal-content').load("{{url('cart/checkout_response')}}",function(){ 
                    $('#address_model1').modal({show:true}); 
                });
            var add = '{{user()->billingaddresses}}'; 
            if(add == '' || add == null){
                document.getElementById('popup_address_list1').style.display = 'none';
                document.getElementById('popup_address_add1').style.display = 'block';
            }
        }
     function address_radio1($id){
         
        $.get("{{trans_url('client/cart/saved_billing_address')}}/"+$id, function(data) { $('#saved_address1').html(data); });
    };
</script>
