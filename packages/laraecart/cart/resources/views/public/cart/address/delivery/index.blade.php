<div class="row">
    <div class="col-md-8">
        <div class="saved-address" id="saved_address">
            @forelse(Cart::getAddresses() as $address)
            @php 
            $flag=0; 
            @endphp
            <div class="address-item">
                <input type="radio" id="address_{{$address->id}}" name="address_id" onclick="address_radio('{{$address->id}}')" {{$address->default_address == 1 ? 'checked' : ''}} value="{{$address->id}}">
                <label for="address_{{$address->id}}">
                    <h3>{{$address->title}}</h3>
                    <p>{{$address->address}}</p>
                </label>
            </div>
            @empty
            <?php $flag=1; ?>
            @endif
        </div>
    </div>
    <div class="col-md-4">
        @if($flag==1)
        <button type="button" onclick="add_delivery_address();" id="btn_add_addr_deliveryess_form" name="button" class="btn change-address-btn" style="padding: 9px 15px;" >Add Address</button>
        @else
        <button type="button" onclick="add_delivery_address();" id="btn_add_delivery_address_form" name="button" class="btn change-address-btn" style="padding: 9px 15px;" >Change Address</button>
        @endif
    </div>
</div>