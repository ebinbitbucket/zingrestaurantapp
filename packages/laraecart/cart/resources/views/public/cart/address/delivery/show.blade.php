
<div class="modal fade" id="address_delivery_model" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">
                @include('notifications')
                <div class="saved-address" id="popup_delivery_address_list">
                    @foreach(Cart::getAllAddresses() as $address)
                    <div class="address-item">
                        <span class="delete_delivery_address delete-btn" id="{{$address->getRouteKey()}}" title="Delete"><i class="ion ion-close-circled"></i></span>
                        <input type="radio" id="address_{{$address->id}}" name="address_id" onclick="address_radio('{{$address->id}}')" {{$address->default_address == 1 ? 'checked' : ''}} value="{{$address->id}}">
                        <label for="address_{{$address->id}}">
                            <h3>{{$address->title}}</h3>
                            <p>{{$address->address}}</p>
                        </label>
                    </div>
                 
                     
                    @endforeach
                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="button" onclick="add_form_show()">Add Address</button>
                    </div>
                </div>
                <div id="popup_delivery_address_add" class="add-address" style="display: none;">
                    <div id="locationMap" class="mb-15" style="width: 100%; height: 250px;"></div>
                   {!!Form::vertical_open()
                    ->method('POST')
                    ->id('add_delivery_address_form')
                    ->action(trans_url('client/cart/address'))!!}
                    <div class="form-group">
                        <label for="name">Address*</label>
                        <input id="address" type="text" name="address" class="form-control" required="required">
                        <input type="hidden" name="latitude" id="latitude_address">
                        <input type="hidden" name="longitude" id="longitude_address">
                        <input type="hidden" name="type" value="delivery">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Suite/Unit Number</label>
                                <input type="text" class="form-control" name="apartment_no">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Address Name</label>
                                <input type="text" class="form-control" name="title" placeholder="eg: Office" required="required" value="Home">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Delivery Notes</label>
                        <input type="text" class="form-control" name="notes">
                    </div>
                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="submit" value="checkout" name="btn_add_delivery_address" id="btn_add_delivery_address" >Save Address</button>
                        <button class="btn btn-secondary" type="button" onclick="list_form_show()">Back</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function add_delivery_address(){

       var add = '{{user()->addresses}}'; 

        $('#address_delivery_model').modal({show:true, backdrop: 'static',
            keyboard: false}); 
        if(add == '' || add == null){
            document.getElementById('popup_delivery_address_list').style.display = 'none';
            document.getElementById('popup_delivery_address_add').style.display = 'block';
        }
       
    }

     function address_radio($id){
        $('#address_delivery_model').modal('hide');
        $.get("{{trans_url('client/cart/saved_address')}}/"+$id, function(data) { $('#saved_address').html(data); });
    };

        function add_form_show() {
        document.getElementById('popup_delivery_address_list').style.display = 'none';
        document.getElementById('popup_delivery_address_add').style.display = 'block';
    }

     function list_form_show() {
        document.getElementById('popup_delivery_address_list').style.display = 'block';
        document.getElementById('popup_delivery_address_add').style.display = 'none';
    }

      $( "#add_delivery_address_form" ).submit(function( e ) {
            e.preventDefault()
            var $f = $('#add_delivery_address_form');
           $f.validate({ 
            rules: {
                address: "required",
                title: "required",
              
            },
            messages: {
                address: "Please enter  address",
                title: "Please enter address name"
                
            }
         });
            $.ajax({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    $('#address_delivery_model').modal('hide');
                    document.getElementById('popup_delivery_address_list').style.display = 'block';
                    document.getElementById('popup_delivery_address_add').style.display = 'none';
                   
                    $( "#popup_delivery_address_list" ).load(window.location.href + " #popup_delivery_address_list" );
                    $.get("{{trans_url('client/cart/saved_address')}}/"+data.address, function(data) { $('#saved_address').html(data); 
                    });
                  
                  
                  },
                  error: function(xhr, status, error) {
                    
                }
              });
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

      $(".delete_delivery_address").click(function(){
        var id = $(this).attr('id');
          swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    var id = $(this).attr('id'); 
                    $.ajax({
                        url : "{{ trans_url('client/cart/address')}}" + '/' + id,
                        type : "POST",
                        data : {'_method' : 'DELETE', 'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                        success: function(data1){ 
                         $( "#popup_delivery_address_list" ).load(window.location.href + " #popup_delivery_address_list" );

                          $.get("{{trans_url('client/cart/saved_address')}}/"+data1.delivery_id, function(data) { $('#saved_address').html(data); });
                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                return;
                }
            });

    });
$( document ).ajaxComplete(function() {

      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

      $(".delete_delivery_address").click(function(){
        var id = $(this).attr('id');
          swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    var id = $(this).attr('id'); 
                    $.ajax({
                        url : "{{ trans_url('client/cart/address')}}" + '/' + id,
                        type : "POST",
                        data : {'_method' : 'DELETE', 'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                        success: function(data1){
                         $( "#popup_delivery_address_list" ).load(window.location.href + " #popup_delivery_address_list" );

                          $.get("{{trans_url('client/cart/saved_address')}}/"+data1.delivery_id, function(data) { $('#saved_address').html(data); });
                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                return;
                }
            });

    });
 });
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>            
        <script>
             var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        var billing_places = new google.maps.places.Autocomplete(document.getElementById('billing_address'));
       

        google.maps.event.addListener(places, 'place_changed', function () {
                geocodeAddress(geocoder);
        });

        google.maps.event.addListener(billing_places, 'place_changed', function () {
                geocodeAddress1(geocoder);
        });
        
    });
     function geocodeAddress(geocoder) { 
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng();
            
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }

      function geocodeAddress1(geocoder) { 
        var address = document.getElementById('billing_address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
           //  document.getElementById('latitude_address').value=results[0].geometry.location.lat();
           //  document.getElementById('longitude_address').value = results[0].geometry.location.lng();
            
           // console.log(results[0].geometry.location.lat());
           // console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
     
            var map;
            var marker;
            var myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();
            
            function initialize(){
                if(document.getElementById('latitude_address').value != ''){
                    var myLatlng = new google.maps.LatLng(document.getElementById('latitude_address').value,document.getElementById('longitude_address').value);                    

                }
                else{
                    var myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
                }
                var mapOptions = {
                    zoom: 18,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
               
                map = new google.maps.Map(document.getElementById("locationMap"), mapOptions);
                
                marker = new google.maps.Marker({
                    map: map,
                    position: myLatlng,
                    draggable: true 
                });     
                


                geocoder.geocode({'latLng': myLatlng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#latitude_address').val(results[0].geometry.location.lat());
                            $('#longitude_address').val(results[0].geometry.location.lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });

                               
                google.maps.event.addListener(marker, 'dragend', function() {
                    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#address').val(results[0].formatted_address);
                                bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });
                function bindDataToForm(address,lat,lng){
                   document.getElementById('latitude_address').value = lat;
                   document.getElementById('longitude_address').value = lng;
                }

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                          lat: position.coords.latitude,
                          lng: position.coords.longitude
                        };
                        infowindow.setPosition(pos);
                        infowindow.setContent('Location found.');
                        infowindow.open(map);
                        map.setCenter(pos);
                    }, function() {
                        handleLocationError(true, infowindow, map.getCenter());
                    });
                } else {
                    handleLocationError(false, infowindow, map.getCenter());
                }
            }
            
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>    