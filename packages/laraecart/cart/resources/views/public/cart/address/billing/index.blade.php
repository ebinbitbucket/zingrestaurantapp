<div class="row">
  <div class="col-md-8">
    <div class="saved-address" id="saved_address1"> 
          @forelse(Cart::getBillingAddresses() as $address)
          @php $flag=0; @endphp
          <div class="address-item">
              <input type="radio" id="address_{{$address->id}}" name="billing" class="billing_address_id" onclick="address_radio('{{$address->id}}')" checked value="{{$address->id}}">
              <label for="address_{{$address->id}}">
                  <h3>{{$address->title}}</h3>
                  <p>{{$address->address}}</p>
              </label>
              <input type="hidden" name="billing_address_detail" value="{{str_limit($address->address,30)}}"> 
            <!--<input type="hidden" name="billing_address_detail" value="{{$address->title}},{{strtok($address->address,',')}}">  -->
      </div>
         @empty
          @php $flag=1; @endphp
      @endif 
    </div>
  </div>
  <div class="col-md-4">
    @if($flag==1)
            <!-- <button type="button" data-toggle="modal" href="#address_model1" id="btn_add_billing_address_form" name="button" class="btn change-address-btn btn_add_billing_address_form">Add Address</button> -->
             <button type="button" onclick="billing_address_popup();" id="btn_add_billing_address_form" name="button" class="btn change-address-btn btn_add_billing_address_form" style="padding: 9px 15px;" >Add Address</button>
    @else
        <button type="button" data-toggle="modal" href="#address_model1" name="button" class="btn change-address-btn btn_add_billing_address_form" style="padding: 9px 15px;">Change Address</button>
    @endif
  </div>
</div>