

<div class="modal fade errorModel" id="address_model1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Change Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">
                @include('notifications')
                <div class="saved-address" id="popup_address_list1">
                    @foreach(Cart::getAllBillingAddresses() as $address)
                    <div class="address-item">
                         <span class="delete_billing_address delete-btn" id="{{$address->getRouteKey()}}" title="Delete"><i class="ion ion-close-circled"></i></span>
                        <input type="radio" class="close" data-dismiss="modal" onclick="address_radio1('{{$address->id}}')" id="address_{{$address->id}}" name="address_id"  {{$address->default_address == 1 ? 'checked' : ''}} value="{{$address->id}}" >
                        <label for="address_{{$address->id}}">
                            <h3>{{$address->title}}</h3>
                            <p>{{$address->address}}</p>
                        </label>
                    </div>
                    @endforeach
                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="button" onclick="add_form_show1()">Add Address</button>
                    </div>
                </div>
                <div id="popup_address_add1" class="add-address"  style="display: none;">
<!--                     <div id="locationMap" class="mb-15" style="width: 100%; height: 250px;"></div>
 -->                    {!!Form::vertical_open()
                    ->method('POST')
                    ->id('add-billing-address-form')
                    ->action(trans_url('client/cart/address'))!!}
                    <div class="form-group">
                        <label for="name">Address</label>
                        <input id="billing_address" type="text" name="address" class="form-control" required>
<!--                         <input type="hidden" name="latitude" id="latitude_billing_address">-->
<!--                         <input type="hidden" name="longitude" id="longitude_billing_address">
 -->                        <input type="hidden" name="type" value="billing">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Suite/Unit Number</label>
                                <input type="text" class="form-control" name="apartment_no">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Address Name</label>
                                <input type="text" class="form-control" name="title" value="Home" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Delivery Notes</label>
                        <input type="text" class="form-control" name="notes">
                    </div>
                     <input type="hidden" name="type" value="billing">

                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="submit" value="checkout" name="btn_add_address1" id="btn_add_address1" >Save Address</button>
                        <button class="btn btn-secondary" type="button" onclick="list_form_show1()" >Back</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <script type="text/javascript">
               function add_form_show1() { 
               $('#popup_address_list1').hide();
              $('#popup_address_add1').show();
              }
          </script>            
        </div>
    </div>
</div>

<script type="text/javascript">

  

   function list_form_show1() {
        document.getElementById('popup_address_list1').style.display = 'block';
        document.getElementById('popup_address_add1').style.display = 'none';
    }

    $( "#add-billing-address-form" ).submit(function( e ) {
             e.preventDefault();
            var $f = $('#add-billing-address-form'); console.log($f.serialize());
             $f.validate({ 
            rules: {
                address: "required",
                title: "required",
              
            },
            messages: {
                address: "Please enter  address",
                title: "Please enter address name"
                
            }
         });
            $.ajax({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(data) { 
                    // $('#popup_address_list').load('');
                    document.getElementById('popup_address_list1').style.display = 'block';
                    document.getElementById('popup_address_add1').style.display = 'none';
                    $('#address_model1').modal('hide');
                    $( "#popup_address_list1" ).load(window.location.href + " #popup_address_list1" );
                    $.get("{{trans_url('client/cart/saved_billing_address')}}/"+data.address, function(data) { $('#saved_address1').html(data); });
                    // $('#card_det').disabled = false;
                    //document.location.reload();
                  
                  },
                  error: function(xhr, status, error) {
                    
                }
              });
           
        });

        function billing_address_popup(){ 
            $('.errorModel .modal-content').load("{{url('cart/checkout_response')}}",function(){ 
                    $('#address_model1').modal({show:true}); 
                });
            var add = '{{user()->billingaddresses}}'; 
            if(add == '' || add == null){
                document.getElementById('popup_address_list1').style.display = 'none';
                document.getElementById('popup_address_add1').style.display = 'block';
            }
        }
     function address_radio1($id){
         
        $.get("{{trans_url('client/cart/saved_billing_address')}}/"+$id, function(data) { $('#saved_address1').html(data); });
    };
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

      $(".delete_billing_address").click(function(){ 
        var id = $(this).attr('id');
          swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    var id = $(this).attr('id'); 
                    $.ajax({
                        url : "{{ trans_url('client/cart/address')}}" + '/' + id,
                        type : "POST",
                        data : {'_method' : 'DELETE', 'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                        success: function(data1){
                         $( "#popup_address_list1" ).load(window.location.href + " #popup_address_list1" );
                           $.get("{{trans_url('client/cart/saved_billing_address')}}/"+data1.billing_id, function(data) { $('#saved_address1').html(data); });
                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                return;
                }
            });

    });
$( document ).ajaxComplete(function() {

      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

      $(".delete_billing_address").click(function(){
        var id = $(this).attr('id');
          swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    var id = $(this).attr('id'); 
                    $.ajax({
                        url : "{{ trans_url('client/cart/address')}}" + '/' + id,
                        type : "POST",
                        data : {'_method' : 'DELETE', 'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                        success: function(data1){
                         $( "#popup_address_list1" ).load(window.location.href + " #popup_address_list1" );
                         $.get("{{trans_url('client/cart/saved_billing_address')}}/"+data1.billing_id, function(data) { $('#saved_address1').html(data); });
                        },
                        error : function(){
                            swal({
                                title: 'Opps...',
                                text : data.message,
                                type : 'error',
                                timer : '1500'
                            })
                        }
                    })
                } else {
                return;
                }
            });

    });
 });
</script>