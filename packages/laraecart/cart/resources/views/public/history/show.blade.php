            @include('cart::history.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('cart::history.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($history->defaultImage('images' , 'xl'))!!}" alt="{{$history->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('cart::history.label.id') !!}
                </label><br />
                    {!! $history['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_id">
                    {!! trans('cart::history.label.order_id') !!}
                </label><br />
                    {!! $history['order_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="courier_name">
                    {!! trans('cart::history.label.courier_name') !!}
                </label><br />
                    {!! $history['courier_name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_status">
                    {!! trans('cart::history.label.order_status') !!}
                </label><br />
                    {!! $history['order_status'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('cart::history.label.created_at') !!}
                </label><br />
                    {!! $history['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('cart::history.label.updated_at') !!}
                </label><br />
                    {!! $history['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('cart::history.label.deleted_at') !!}
                </label><br />
                    {!! $history['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('cart::history.label.order_id'))
                       -> placeholder(trans('cart::history.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('courier_name')
                       -> label(trans('cart::history.label.courier_name'))
                       -> placeholder(trans('cart::history.placeholder.courier_name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('order_status')
                       -> label(trans('cart::history.label.order_status'))
                       -> placeholder(trans('cart::history.placeholder.order_status'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



