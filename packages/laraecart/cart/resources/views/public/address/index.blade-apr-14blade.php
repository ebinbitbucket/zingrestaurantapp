<div class="modal fade" id="address_delivery_model" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">
                @include('notifications')
              
                <div id="popup_delivery_address_add" class="add-address">
                    <div id="locationMap" class="mb-15" style="width: 100%; height: 250px;"></div>
                   {!!Form::vertical_open()
                    ->method('POST')
                    ->id('add_delivery_address_form')
                    ->action(trans_url('client/cart/address'))!!}
                    <div class="form-group">
                        <label for="name"><b>Address*</b></label>
                        <input id="address" type="text" name="address" class="form-control" required="required">
                        <input type="hidden" name="latitude" id="latitude_address" value="{{Session::get('latitude')}}">
                        <input type="hidden" name="longitude" id="longitude_address" value="{{Session::get('longitude')}}">
                        <input type="hidden" name="type" value="delivery">
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name"><b>Suite/Unit Number</b></label>
                                <input type="text" class="form-control" name="apartment_no">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Address Name</label>
                                <input type="text" class="form-control" name="title" required="required" list="ref_new_addressname">
                                <datalist id="ref_new_addressname">
                                    <option value="Home">
                                    <option value="Work">
                                </datalist>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Delivery Notes</label>
                        <input type="text" class="form-control" name="notes">
                    </div>
                    <div class="btn-wrap modal-footer justify-content-start">
                        <button class="btn btn-theme" type="submit" value="checkout" name="btn_add_delivery_address" id="btn_add_delivery_address" >Add Address</button>
                     
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function add_delivery_address(){

        $('#address_delivery_model').modal({show:true, backdrop: 'static',
            keyboard: false}); 
           
    }

     $( "#add_delivery_address_form" ).submit(function( e ) {
            e.preventDefault()
            var $f = $('#add_delivery_address_form');
           $f.validate({ 
            rules: {
                address: "required",
                title: "required",
              
            },
            messages: {
                address: "Please enter  address",
                title: "Please enter address name"
                
            }
         });
            $.ajax({
                  type: 'POST',
                  url: $f.attr('action'),
                  data: $f.serialize(),
                  success: function(response) {   
                            flag_address =0; 
                            $('#address_delivery_model').modal('hide');   
                            console.log('addresponse',response)                               ;
                            $('#delivery_address_block').html(response.view);
                            if(response.status != 'Success'){console.log('hi');
                            document.getElementById('delivery_amount_span').style = "display:none;";
                                var delivery_charge=0;
                                $('#delivery_amount_span').text(0);
                            }
                            else
                            {
                                var delivery_charge=response.charge;
                                document.getElementById('delivery_amount_span').style = "display:block;";
                                $('#delivery_amount_span').text('$'+delivery_charge);
                            }
                            $('#del_charge').val(delivery_charge);
                            document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
                            document.getElementById("tip").value=tip;
                            var grandtotal = '<?php echo Cart::total() + (!empty($rest_data) ? ($rest_data->tax_rate / 100) * Cart::total() : 0) + (!empty($rest_data) ? $rest_data->ZC : 0); ?>';
                            document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(tip));
                            var totl='<?php echo number_format(Cart::total() + (!empty($rest_data) ? ($rest_data->tax_rate / 100) * Cart::total() : 0) + (!empty($rest_data) ? $rest_data->ZC : 0) + (5 / 100) * Cart::total(), 2); ?>';
                            document.getElementById("total_charge").textContent=(parseFloat(document.getElementById('total_amount').value)+parseFloat(delivery_charge)).toFixed(2);
   
                            // $.ajax({
                            //     url: "{{ URL::to('address/delivery')}}",
                            //     success: function(response){
                            //         $('#delivery_address_block').html(response.view);
                            //     }
                            // });
                               
                       
    
                  },
                  error: function(xhr, status, error) {
                    alert('helo');
                }
              });
    });
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>            
        <script>
             var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        var billing_places = new google.maps.places.Autocomplete(document.getElementById('billing_address'));
       

        google.maps.event.addListener(places, 'place_changed', function () {
                geocodeAddress(geocoder);
        });

        google.maps.event.addListener(billing_places, 'place_changed', function () {
                geocodeAddress1(geocoder);
        });
        
    });
     function geocodeAddress(geocoder) { 
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng();
            
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }

      function geocodeAddress1(geocoder) { 
        var address = document.getElementById('billing_address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
           //  document.getElementById('latitude_address').value=results[0].geometry.location.lat();
           //  document.getElementById('longitude_address').value = results[0].geometry.location.lng();
            
           // console.log(results[0].geometry.location.lat());
           // console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
     
            var map;
            var marker;
            var myLatlng = new google.maps.LatLng(32.7554883,-97.3307658);
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();
            
            function initialize(){
                if(document.getElementById('latitude_address').value != ''){
                    var myLatlng = new google.maps.LatLng(document.getElementById('latitude_address').value,document.getElementById('longitude_address').value);                    

                }
                else{
                    var myLatlng = new google.maps.LatLng(32.7554883,-97.3307658);
                }
                var mapOptions = {
                    zoom: 9,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
               
                map = new google.maps.Map(document.getElementById("locationMap"), mapOptions);
                
                marker = new google.maps.Marker({
                    map: map,
                    position: myLatlng,
                    draggable: true 
                });     
                


                geocoder.geocode({'latLng': myLatlng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            $('#latitude_address').val(results[0].geometry.location.lat());
                            $('#longitude_address').val(results[0].geometry.location.lng());
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });

                               
                google.maps.event.addListener(marker, 'dragend', function() {
                    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#address').val(results[0].formatted_address);
                                bindDataToForm(results[0].formatted_address,marker.getPosition().lat(),marker.getPosition().lng());
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });
                function bindDataToForm(address,lat,lng){
                   document.getElementById('latitude_address').value = lat;
                   document.getElementById('longitude_address').value = lng;
                }

                // if (navigator.geolocation) {
                //     navigator.geolocation.getCurrentPosition(function(position) {
                //         var pos = {
                //           lat: position.coords.latitude,
                //           lng: position.coords.longitude
                //         };
                //         infowindow.setPosition(pos);
                //         infowindow.setContent('Location found.');
                //         infowindow.open(map);
                //         map.setCenter(pos);
                //     }, function() {
                //         handleLocationError(true, infowindow, map.getCenter());
                //     });
                // } else {
                //     handleLocationError(false, infowindow, map.getCenter());
                // }
            }
            
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>    