   @foreach(Cart::content() as $cart)
                        <?php $rest_id = $cart->options->restaurant;
                            $rest_data                             = Restaurant::getRestaurantData($rest_id);
                        ?>
                    @endforeach
                    <h3></h3></br> 
                                    <div class="alert alert-warning mb-0 mt-10" role="alert" id="sectionHead_address"><span id="section_address"></span></div> 
                                    <a href="#" class="add-new-btn" onclick="add_form_show()">Enter Address</a>                               
                                    <div class="row mt-20" id="delivery_address_section">
                                        @forelse($address as $key => $address_val)
                                            <div class="col-md-6">
                                                <label class="address-item" id="address_item_{{$key}}">
<!--                                                     <input type="radio" name="del_address" id="address_item_{{$key}}">
 -->                                                    <i class="ion ion-android-home"></i>
                                                    <div class="address-item-content">
                                                        <h4>{{$address_val->title}}</h4>
                                                        <p>{{$address_val->address}}</p>
                                                        <button type="button" class="btn btn-theme" onclick="deliver_address('{{$address_val->id}}')">Deliver Here</button>
                                                    </div>
                                                </label>
                                            </div>
                                        @empty
                                            <!-- <a href="#" class="add-new-btn" onclick="add_form_show()">Add New</a> -->
                                        @endif
                                    </div>
                                    <input type="hidden" name="address_id" id="address_id">



<script type="text/javascript">
    var restaurant_id = '{{$rest_data->getRoutekey()}}';
$(document).ready(function(){
    flag_address ++;
    console.log({!!(count($address))!!});
    if({!!(count($address))!!} > 0){
        $('#sectionHead_address').show();
        $('#section_address').html('Select your delivery address');
    }
    else{
        $('#sectionHead_address').show();
        $('#section_address').html('Please add your delivery address');

    }
    
});
function deliver_address(id){
    $.ajax({
                url: "{{ URL::to('address/delivery/saved_address')}}"+'/'+id+'/'+restaurant_id,
                success: function(response){
                    if(response.status != 'Success'){
                        document.getElementById('delivery_amount_span').style = "display:none;";
                            $('#del_charge').val(0);
                            var delivery_charge=0;

                        if(response.section == 'section_address'){
                            $('#sectionHead_address').show();
                            $('#section_address').html(response.msg);
                            toastr.warning(response.msg);
                            flag_address ++;
                            document.getElementById('delivery_amount_span').style = "display:none;";
                            $('#del_charge').val(0);
                            var delivery_charge=0;

                        }
                    }
                    else{
                        document.getElementById('delivery_amount_span').style = "display:block;";
                        $('#delivery_amount_span').text('$'+response.charge);
                        var delivery_charge=response.charge;
                        $('#del_charge').val(delivery_charge);
                        document.getElementById("tip_charge").textContent='$'+tip.toFixed(2);
                        document.getElementById("tip").value=tip;
                        var grandtotal = '<?php echo Cart::total() + (!empty($rest_data) ? ($rest_data->tax_rate / 100) * Cart::total() : 0) + (!empty($rest_data) ? $rest_data->ZC : 0); ?>';
                        document.getElementById('total_amount').value = (parseFloat(grandtotal)+parseFloat(tip));
                        var totl='<?php echo number_format(Cart::total() + (!empty($rest_data) ? ($rest_data->tax_rate / 100) * Cart::total() : 0) + (!empty($rest_data) ? $rest_data->ZC : 0) + (5 / 100) * Cart::total(), 2); ?>';
                        document.getElementById("total_charge").textContent=(parseFloat(document.getElementById('total_amount').value)+parseFloat(delivery_charge)).toFixed(2);
                      
                        $('#address_id').val(id);
                        $('#delivery_address_section').html(response.view);
                        $('#sectionHead_address').hide();
                        flag_address = 0;
                    }
                }
            });
}
function add_form_show() {
    $('#address_delivery_model').modal({show:true, backdrop: 'static',
            keyboard: false});
        $('#popup_delivery_address_add').show();
    }
</script>                                    
