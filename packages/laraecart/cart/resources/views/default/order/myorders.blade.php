<section class="user-wrap-inner">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-3 user-aside">
                {!! Theme::partial('aside') !!}
            </div>
            <div class="col-md-9">
                <div class="user-content-wrap">
                    <div class="heading-block">
                        <h3>Orders</h3>
                    </div>
                    <div class="inner-content">
                        <ul class="nav nav-tabs">
                        <!--     <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/all'))? 'active' : ''}}" href="{{guard_url('cart/myorders/all')}}">All</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/neworders'))? 'active' : ''}}" href="{{guard_url('cart/myorders/neworders')}}">New</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/scheduled'))? 'active' : ''}}" href="{{guard_url('cart/myorders/scheduled')}}">Scheduled</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/preparing'))? 'active' : ''}}" href="{{guard_url('cart/myorders/preparing')}}">Preparing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/preparing'))? 'active' : ''}}" href="{{guard_url('cart/myorders/preparing')}}">Preparing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/delivered'))? 'active' : ''}}" href="{{guard_url('cart/myorders/delivered')}}">Delivered</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/cancelled'))? 'active' : ''}}" href="{{guard_url('cart/myorders/cancelled')}}">Cancelled</a>
                            </li>
                             <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/overdue'))? 'active' : ''}}" href="{{guard_url('cart/myorders/overdue')}}">Overdue</a>
                            </li> -->
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/currorders'))? 'active' : ''}}" href="{{guard_url('cart/myorders/currorders')}}">Current Orders</a>
                            </li>
                              <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/pastorders'))? 'active' : ''}}" href="{{guard_url('cart/myorders/pastorders')}}">Past Orders</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/myorders/favourites'))? 'active' : ''}}" href="{{guard_url('cart/myorders/favourites')}}">Favorite Orders</a>
                            </li>

                        </ul>

                        <div class="orders-wrap">
                            @forelse($orders as $order)
                            <div class="order-item">
                                <div class="restaurant-detail">
                                    <div class="img-holder">
                                        <figure>
                                            <a href="{{guard_url('cart/order/details/'.$order->getRoutekey())}}">
                                                <span class="img-wrap" style="background-image: url('{{url($order->restaurant->defaultImage('logo'))}}')"></span>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="text-holder">
                                    <?php $restaurant_fav = Cart::getFavouriteStatus($order->id); ?>
                                        <h3 class="title"><a href="{{guard_url('cart/order/details/'.$order->getRoutekey())}}">{{$order->restaurant->name}}</a></h3>
                                        <h6><b>{{$order->order_type}} Order</b><button onclick="add_fav('{{$order->id}}')" class="add-fav-btn {{$restaurant_fav==1 ? 'active':''}}"><i class="fa fa-heart"></i></button></h6>
                                        <div class="location">{{$order->restaurant->address}}</div>
                                        <div class="odrer-date">{{date('D, M d, h:i A',strtotime($order->created_at))}}</div>
                                        <!-- <a href="#" class="view-detail">View Details</a> -->
                                        <div class="status delivered">{{$order->order_status}} on {{date('D, M d, h:i A',strtotime($order->ready_time))}} <i class="fa fa-check-circle text-success"></i></div>
                                        <div class="total">Total Paid: ${{$order->total}}</br>Loyalty Points: @if($order->loyalty_points == '') 0 @else {{number_format($order->loyalty_points,2)}} @endif</div>
                                        @if($order->order_status != 'New Orders' && $order->order_status != 'Scheduled' && $order->order_status != 'Preparing')
                                        <div class="button-wrap mt-20">
                                            <a href="{{guard_url('cart/reorder/'.$order->getRoutekey())}}" class="btn btn-theme">Reorder</a>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="menu-items meta-infos">
<!--                                     <div class="total">Total Paid: ${{$order->total}}</br>Loyalty Points: @if($order->loyalty_points == '') 0 @else {{$order->loyalty_points}} @endif</div>
 -->                                    @foreach($order->detail as $order_det)
                                    <div class="info pl-0">
                                        @if($order_det->menu_addon == 'menu' && (!empty($order_det->menu)))
                                            {{$order_det->menu->name}} 
                                        @elseif(!empty($order_det->addon))
                                            {{$order_det->addon->name}}  
                                        @endif 
                                      - {{$order_det->quantity}} x ${{$order_det->unit_price}} 
                                    </div>
                                    @endforeach
                                </div>
                            <!--     <div class="button-wrap mt-20">
                                    <a href="{{guard_url('cart/reorder/'.$order->getRoutekey())}}" class="btn btn-theme">Reorder</a>
                                </div> -->
                            </div>
                            @empty
                            <div class="empty-content-wrap">
                                <img src="{{theme_asset('img/order-empty.svg')}}" alt="">
                                @if(Request::is('*cart/myorders/currorders'))
                                <h2>No orders present at the moment!</h2>
                                @endif
                                @if(Request::is('*cart/myorders/pastorders'))
                                <h2>No orders present at the moment!</h2>
                                @endif
                                @if(Request::is('*cart/myorders/favourites'))
                                <h2>No Favorite Orders set!</h2>
                                @endif
                                <p><a href="{{url('/')}}">Search for popular food in your area</a></p>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
   function add_fav(order_id){
        var fav_status = $('.add-fav-btn').hasClass('active');
        if(fav_status == false)
            fav_status = 'on';
        else
            fav_status = 'off';

            $.getJSON({
                  type: 'GET',
                  url : "{{url('client/cart/order/addtofavourite')}}"+'/'+order_id+'/'+fav_status,
                  success: function(data) { 
                    if(fav_status == on)
             $('.add-fav-btn').addClass('active');
        else
             $('.add-fav-btn').removeClass('active');
                   
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
     }
</script>
<style type="text/css">
     .meta-infos {
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    display: flex;
    -ms-align-items: center;
    align-items: center;
    margin-top: 10px;
}
 .meta-infos .info+.info {
    margin-left: 20px;
}
 .meta-infos .info {
    position: relative;
    padding-left: 25px;
    line-height: 1;
    font-size: 14px;
}
 .meta-infos .info i {
    position: absolute;
    left: 0;
    top: -3px;
    font-size: 20px;
}
 .meta-infos .info:nth-child(2) {
    padding-left: 20px;
}
 .meta-infos .info:nth-child(2) i {
    font-size: 16px;
    top: -1px;
}
 .meta-infos .info:nth-child(3) {
    top: 0px;
}
 .meta-infos .info:nth-child(3) i {
    font-size: 18px;
}
 .meta-infos .info+.info::before {
    content: "";
    width: 1px;
    height: 100%;
    background-color: #ddd;
    position: absolute;
    left: -10px;
    top: 2px;
}
</style>