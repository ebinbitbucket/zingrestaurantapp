                                 @forelse($orders as $order)
                                                <div class="card-item" style="{{$order->order_status == 'New Orders' ? ($order->order_type == 'Delivery' ? 'background-color: rgba(45, 206, 137, 0.05);border-color: #2dce89' : 'background-color: rgba(251, 99, 64, 0.05);border-color: #fb6340') : ($order->order_type == 'Delivery' ? 'background-color: #dee2e6;border-color: #2dce89' : 'background-color: #dee2e6;border-color: #fb6340')}}">
                                                    <a href="javascript:void(0);" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')">
                                                        <h3><span>#{{$order->id}}</span>{{@$order->user->name}} &nbsp;&nbsp;
                                                        @if($order->order_status=='Completed')
                                                        <span>(Accepted)</span>
                                                        @endif
                                                        <div class="time"><b>{{$order->order_type}} Time:</b></div>
                                                        <p class="time ion-android-calendar ml-0">{{strtolower(substr(date('l',strtotime($order->ready_time)),0,3))}}, {{date('M d',strtotime($order->ready_time))}},  {{date('h:i A',strtotime($order->ready_time))}}</p>
                                                            <div class="time">
                                                                <b style="color:black;font-size: 18px; ">{{strtoupper($order->order_type)}} ORDER @if($order->order_status == 'New Orders') (New) @endif</b>
                                                            </div>
                                                        </h3>
                                                        @if(@$order->non_contact == 1)
                                                        <p ><h3 style="color:red;">No Contact Delivery</h3></p>
                                                         @endif
                                                        @if(@$order->car_pickup == 1)
                                                        <p  ><h3 style="color:red;">CAR PICKUP</h3></p>
                                                         @endif
                                                        @if(!empty(@$order->user->mobile))<p class="location"><i class="fa fa-phone"></i> {{substr(@$order->user->mobile, 0,3)}}-{{substr(@$order->user->mobile, 3,3)}}-{{substr(@$order->user->mobile, 6,4)}}</p>@endif
                                                        
                                                        <div class="item-details">
                                                              @foreach($order->detail as $order_detail)
                                                    <div class="item">{{$order_detail->quantity}} x  @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                 {{$order_detail->menu->name}}
                                                @elseif(!empty($order_detail->addon))
                                                 {{$order_detail->addon->name}}
                                                @endif</div>
                                                @endforeach
                                                        </div>
                                                    </a>
                                                    <div class="price"><span>Total</span>${{number_format($order->total,2)}}</div>
                                                    <div class="actions">
                                                        
                                                        <div class="order-type-btn" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')">
                                                            <span class="btn" style="background-color: black;border-color: black;border-radius: 10px;"><span id="order_status_Label">VIEW</span></span>
                                                        </div>
                                                        <div class="order-type-btn" style="margin-left: 10px;"> 
                                                            @if($order->order_status == 'New Orders')
                                                            <span class="btn" style="background-color: #28a745;border-color: #28a745;" >
                                                                <span id="order_status_Label" onclick="accept_model('{{$order->getRouteKey()}}')">@if($order->order_status) ACCEPT @endif</span></span>
                                                            @else
                                                            <span class="btn" @if($order->mail_sent > 0)
                                                            style="background-color: gray;border-color: gray;"
                                                            @else  style="background-color: black;border-color: black;" onclick="order_status_prep('ready_{{$order->order_type}}','{{$order->getRouteKey()}}');"  @endif>
                                                                <span id="order_status_Label">@if($order->order_status) @if($order->order_type=='Delivery') OUT FOR @else READY FOR @endif {{strtoupper($order->order_type)}} @endif</span></span>
                                                            @endif
                                                        </div>
                                                        @if(@$order->car_details)
                                                        <div class="order-type-btn" data-toggle="modal"
                                                        @if(@$order->is_car_pickup == 1)
                                                        @else
                                                        onclick="car_detail_model('{{$order->getRouteKey()}}')"
                                                        @endif
                                                        >
                                                            <span class="btn" 
                                                            @if(@$order->is_car_pickup == 1)
                                                            style="background-color: #b98484;border-color: #b98484;border-radius: 30px;    margin-right: 5px;"
                                                            @else
                                                            style="background-color: red;border-color: red;border-radius: 30px;    margin-right: 5px;"
                                                            @endif
                                                            ><span id="order_status_Label">CUSTOMER ARRIVED</span></span>
                                                        </div>
                                                        @endif
                                                        
                                                    </div>
                                                </div>
                                                @empty
                                                @endif
                                           
