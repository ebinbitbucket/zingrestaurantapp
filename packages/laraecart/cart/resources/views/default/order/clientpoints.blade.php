<section class="user-wrap-inner">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-3 user-aside">
                {!! Theme::partial('aside') !!}
            </div>
            <div class="col-md-9">
                <div class="user-content-wrap">
                    <div class="heading-block">
                        <h3>Loyalty Points</h3>
                    </div>
                    <div class="inner-content">
                       <p>You have <span style="color:#2dce89;">{{number_format($points,2)}}</span> Total Points</p>
					   <p>Your points can be redeemed for <span style="color:#2dce89;">${{number_format($points/2000,2)}}</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
          
