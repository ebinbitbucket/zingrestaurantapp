<section class="dashboard-wrap" >
                    <div class="row">
                            <div class="element-wrapper order-detail-wrap">
                                <div class="element-box">
                                    <div class="element-info">
                                        <div class="element-info-with-icon">
                                            <div class="element-info-icon"><div class="icon ion-cube"></div></div>
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header">#{{$order->id}} Order </h5>
                                                <div class="element-inner-desc"></div>
                                            </div>
                                             <div class="element-info-text">
                                                <h5 class="element-inner-header"><b>{{strtoupper($order->order_type)}} ORDER</b></h5>
                                                <div class="element-inner-desc"></div>
                                            </div>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ion ion-ios-close-outline"></i></button>
                                        </div>
                                    </div>
                                   
                                     <?php 
                                    $rest_data = Restaurant::getRestaurantData($order->restaurant_id)
                                   ?>
                                    <div class="order-details-wrap">
                                        <div class="row">
                                            <!--<div class="col-md-4">-->
                                            <!--    <div class="order-detail-item">-->
                                            <!--        <p><span>Order Id</span>#{{$order->id}}</p>-->
                                            <!--    </div>-->
                                            <!--</div>-->
                                        
                                            <div class="col-md-4">
                                                <div class="order-detail-item">
                                                    <p><span>Customer Name</span>{{$order->name}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="order-detail-item">
                                                    <p><span>Phone</span>{{$order->user->mobile}}</p>
                                                </div>
                                            </div>
                                            <!--<div class="col-md-4">-->
                                            <!--    <div class="order-detail-item">-->
                                            <!--        <p><span>Payment Status</span>{{$order->payment_status}}</p>-->
                                            <!--    </div>-->
                                            <!--</div> -->
                                           @if(!empty($order->address_id))
                                            <div class="col-md-8">
                                                <div class="order-detail-item">
                                                    <p><span>{{!empty($order->address_id)? 'Delivery' : 'Pickup' }} Location</span>{{!empty($order->address_id)? @$order->delivery_address->full_address : $rest_data->address}}</p>
                                                </div>
                                            </div>
                                              @endif
                                              
                                            
                                            <div class="col-md-4">
                                                <div class="order-detail-item">
                                                    <p><span>{{!empty($order->address_id)? 'Delivery' : 'Pickup' }} Time</span>{{date('M d,Y h:i A',strtotime($order->ready_time))}}</p>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <hr class="mb-0">
                                    <div class="row">
                                        <!-- <div class="col-md-6 border-right pt-30">
                                            <div class="menu-restaurant-wrap">
                                                <div class="res-img">
                                                    <img src="img/restaurants/thumb/restaurant-01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="res-content">
                                                    <p>Order From <span>Jet's Kitchen</span></p>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-md-12 pt-30">
                                            <div class="order-contains-wrap pl-0">
                                                <h4>Order Summary</h4>
                                                @foreach($order->detail as $order_detail)
                                               <?php
                                                    $variation = $order_detail->variation; 
                                                     $addon_price = 0;

                                                     if (!empty($order_detail->addons)) {

                                                         foreach ($order_detail->addons as $cart_addon) {

                                                             $addon_price = $addon_price + $cart_addon[2];
                                                         }

                                                     }

                                                 ?>
                                                <div class="item" style="{{$order_detail->menu_addon == 'addon' ? 'margin-left: 30px' : ''}}">
                                                    <div class="item-name"> 
                                                    @if(!empty($order_detail->menu))
                                                    <div class="cell-img" style="background-image: url({{url($order_detail->menu->defaultImage('image'))}})"></div>
                                                    @endif
                                                    @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                        {{$order_detail->menu->name}} 
                                                    @elseif(!empty($order_detail->addon))
                                                        {{$order_detail->addon->name}}  
                                                    @endif
                                                    <span>x {{$order_detail->quantity}} </span>
                                                </div>
                                                    <div class="item-price"> ${{number_format($order_detail->quantity * $order_detail->unit_price+$addon_price,2)}}</div>
                                            </div>
                                            @if(!empty($variation))<div class="item" style="margin-left: 20px">  <b>Variation - {{$variation}} </b></div>@endif
                                             @if(!empty($order_detail->addons))
                                        @foreach($order_detail->addons as $cart_addon) 
                                          <div class="item" style="margin-left: 20px">  {{$cart_addon[1]}} </div>
                                        @endforeach
                                        @endif
                                             @if(!empty($order_detail->special_instr))
                                             <div class="item" style="padding-left: 30px;"> 
                                        Special Instructions:<br/>
                                        {{$order_detail->special_instr}}
                                    </div>
                                    @endif
                                               @endforeach

                                               
                                    @if($order->non_contact==1)
                                    <div> 
                                        <span>No Contact Delivery</span>
                                    </div>
                                    @endif
                                        </div>
                                           @if(@$order->tax>0)
                                              <div class="order-total-wrap pl-0">
                                                <h4>Tax & Fees : </h4> 
                                                <h4>${{number_format(@$order->tax,2)}} </h4>
                                              </div>
                                            @endif
                                            @if(@$order->delivery_charge>0)
                                              <div class="order-total-wrap pl-0">
                                                <h4>Delivery :  </h4> 
                                                <h4>${{number_format(@$order->delivery_charge,2)}} </h4> 
                                              </div>
                                            @endif
                                            @if(@$order->tip>0)
                                              <div class="order-total-wrap pl-0">
                                                <h4>Tip :  </h4> 
                                                <h4>${{number_format(@$order->tip,2)}} </h4> 
                                              </div>
                                            @endif
                                            @if(@$order->discount_points>0)
                                              <div class="order-total-wrap pl-0">
                                                <h4>Loyalty Rewards :  </h4> 
                                                <h4>{{number_format(@$order->discount_points/2000,2)}} ({{@$order->loyalty_points}} Points) </h4> 
                                              </div>
                                            @endif
                                            <div class="order-total-wrap pl-0">
                                                <h4>Order Total</h4>
                                                <h4>${{number_format($order->total,2)}}</h4>
                                            </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
            </section>
