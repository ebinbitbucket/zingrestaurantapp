<div class="container bg-white">
<div class="list-view">
    <h3>Orders</h3>
    <div class="card list-view-media"  >
        <div class="card-block">
            <div class="media">
                <div class="media-body">
                    <div class="heading">  
                <table class="table table-striped table-hover table-bordered">
                	
                    <thead class="thead-dark">
                	<tr><th>Address</th><th>Shipping Address</th><th>Order Status</th><th>Order Summary</th><th></th></tr></thead>
                    <tbody>
                	<tr>
                		<td><p>{{$order->address}}</p>
                			<p>Phone: {{$order->phone}}</p> 
                			<p>Street: {{$order->street}}</p>
                			<p>Zipcode: {{$order->zipcode}}</p>
                		</td>
                        <td><p>Phone: {{$order->shipping_address}}</p>
                            <p>{{$order->shipping_phone}}</p> 
                            <p>Street: {{$order->shipping_street}}</p>
                            <p>Zipcode: {{$order->shipping_zipcode}}</p>
                        </td>
                		<td>{{$order->order_status}}</td>
                		<td><p>Total 		: ₹ <strong>{{$order->total}}</strong></p>
                            <p>Payment Method      : <strong>{{$order->payment_methods}}</strong></p>
                            <p>Payment Status      : <strong>{{$order->payment_status}}</strong></p>
                		</td>
                        <td>
                            <a href="{{guard_url('cart/order/details/'.$order->id)}}">View More</a>
                        </td>
                	</tr>
                    </tbody>
                </table>
             </div>
                </div>
            </div>
        </div>
    </div>
</div>