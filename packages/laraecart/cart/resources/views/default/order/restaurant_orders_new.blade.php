
                                         @foreach($orders as $order)
                                         <?php 
                                    $rest_data = Restaurant::getRestaurantData($order->restaurant_id)
                                   ?>
                                        <div class="card-item" style="margin-bottom:10px;">
                                            <a href="#" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')">
                                                <h3><span>#{{$order->id}}</span>{{@$order->user->name}} 
                                                    &nbsp;&nbsp;<div class="time"><b>{{$order->order_type}} Time:</b></div>
                                                    <p class="time ion-android-calendar">{{strtolower(substr(date('l',strtotime($order->delivery_time)),0,3))}}, {{date('M d',strtotime($order->delivery_time))}},  {{date('h:i A',strtotime($order->delivery_time))}}
                                              <!--   {{(!empty($order->delay_min)) ?  date('h:i A',strtotime($order->delivery_time.' + '.$order->delay_min.' minute')) : date('h:i A',strtotime($order->delivery_time))}}  --></p></h3>
                                              @if(!empty(@$order->user->mobile))
                                              <p class="location" style="padding-bottom: 5px;"><i class="fa fa-phone"></i>&nbsp;&nbsp;&nbsp;{{substr(@$order->user->mobile, 0,3)}}-{{substr(@$order->user->mobile, 3,3)}}-{{substr(@$order->user->mobile, 6,4)}}</p>
                                              @endif
                                                <div class="row">
                                                <div class="col-md-2">
                                                    <b style="color:black; ">{{$order->order_type}} Order</b>
                                                </div>
                                                @if($order->order_type == 'Delivery')
                                                <div class="col-md-10"><p class="location ion-android-pin">{{!empty($order->address_id)? @$order->delivery_address->address : $rest_data->address}}</p>
                                                </div>
                                                @endif
                                            </div>
                                                <div class="item-details">
                                                    <!-- <h5>Items</h5> -->
                                                    @foreach($order->detail as $order_detail)
                                                    <div class="item">{{$order_detail->quantity}} x  @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                 {{$order_detail->menu->name}} 
                                                @elseif(!empty($order_detail->addon))
                                                 {{$order_detail->addon->name}}  
                                                @endif</div>
                                                @endforeach
                                                </div>
                                            </a>
                                            <div class="price"><span>Total</span>${{number_format($order->total,2)}}</div>
                                            <div class="actions">
<!--                                                 <span class="label label-success payment-status">{{$order->payment_status}}</span>
 -->                                                <div class="order-type-btn">
                                                    <span class="btn" data-toggle="modal" onclick="order_status_modal('{{$order->id}}')"><span id="order_status_Label">{{$order->order_status}}</span></span>
                                                    <!-- <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                        <h4>Change Status</h4>
                                                        <div class="order-type-wrap">
                                                            <div class="order-type-item">
                                                                <input type="radio" id="new_Order" name="order_Type" value="New Order">
                                                                <label for="new_Order">New Order</label>
                                                            </div>
                                                            <div class="order-type-item">
                                                                <input type="radio" id="preparing_Food" name="order_Type" value="preparing" class="status_change" data-order="{{$order->getRouteKey()}}" data-order="{{$order->getRouteKey()}}">
                                                                <label for="preparing_Food">Preparing Food</label>
                                                            </div>
                                                            <div class="order-type-item">
                                                                <input type="radio" id="redy_for_Pickup" name="order_Type" value="completed" class="status_change" data-order="{{$order->getRouteKey()}}">
                                                                <label for="redy_for_Pickup">Completed</label>
                                                            </div>
                                                            <div class="order-type-item">
                                                                <input type="radio" id="order_Complete" name="order_Type" value="picked" class="status_change" data-order="{{$order->getRouteKey()}}">
                                                                <label for="order_Complete">Order Picked Up</label>
                                                            </div>
                                                            <div class="order-type-item">
                                                                <input type="radio" id="cancel_Order" name="order_Type" value="cancelled" class="status_change" data-order="{{$order->getRouteKey()}}">
                                                                <label for="cancel_Order">Cancel Order</label>
                                                            </div>
                                                        </div>
                                                        
                                                        <select class="form-control" id="status_change" onchange="status_change('{{$order->getRouteKey()}}')">
                                                            <option selected>Select Status</option>
                                                            <option value="preparing">Food Preparing</option>
                                                            <option value="completed">Ready for Pickup</option>
                                                            <option value="picked">Order Picked Up</option>
                                                            <option value="cancelled" >Order Cancelled</option>
                                                        </select>
                                                    </div>-->
                                                </div> 
                                                <a href="#" class="btn btn-dark print-btn ion ion-android-print" data-toggle="tooltip" data-placement="top" title="Print"></a>
                                            </div>
                                        </div>
                                        @endforeach

</audio>
<script>
    $(document).ready(function () {
        if('<?php echo user()->audio_alert; ?>' == 'on')
new Audio("{{trans_url('themes/restaurant/assets/sms-alert-3-daniel_simon.wav')}}").play()
});
  
</script>                                            
