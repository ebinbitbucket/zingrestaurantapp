<section class="dashboard-wrap" style="background-color: #ffffff;">
    <div class="element-wrapper order-detail-wrap">
       <div class="element-box">
         <div class="modal-header">

                <!--<div class="element-info-icon"><div class="icon ion-cube"></div></div>-->
                <div class="element-info-text">
                   <h5 class="element-inner-header">#{{$order->id}} </h5>
                   <div class="element-inner-desc"></div>
                </div> 
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="element-info-text">
                   <h5 class="element-inner-header"><b>Order Time Update</b></h5>
                   <div class="element-inner-desc"></div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="ion-ios-close-outline"></i>
                </button>
         
         </div>
         <div class="modal-body">
          <div class="col-md-6">
             <div class="order-detail-item">
                @if($order->order_type=='Pickup')
                <span>Estimated Time for Preparing food</span>
                @else
                <span>Estimated Time for Preparation and Delivery Combined</span>
               @endif      
                   <select class="tipcal form-control col-md-12" id="ready_time">
                  <option value="0">Select Delay Time</option>
                  <!--<option value="1">1 Min</option>-->
                  <!--<option value="3">3 Min</option>-->
                  <option value="5"@if($order->preparation_time==5) selected @endif>5 Min</option>
                  <option value="8" @if($order->preparation_time==8) selected @endif>8 Min</option>
                  <option value="10" @if($order->preparation_time==10) selected @endif>10 Min</option>
                  <option value="15" @if($order->preparation_time==15) selected @endif>15 Min</option>
                  <option value="20" @if($order->preparation_time==20) selected @endif>20 Min</option>
                  <option value="30" @if($order->preparation_time==30) selected @endif>30 Min</option>
                  <option value="35" @if($order->preparation_time==35) selected @endif>35 Min</option>
                  <option value="40" @if($order->preparation_time==40) selected @endif>40 Min</option>
                  <option value="45" @if($order->preparation_time==45) selected @endif>45 Min</option>
                  <option value="50" @if($order->preparation_time==50) selected @endif>50 Min</option>
                  <option value="55" @if($order->preparation_time==55) selected @endif>55 Min</option>
                  <option value="60" @if($order->preparation_time==60) selected @endif>1 Hr</option>
                  <option value="90" @if($order->preparation_time==90) selected @endif>1 Hr 30 Min</option>
                  <option value="120" @if($order->preparation_time==120) selected @endif>2 Hr</option>
              </select>
             </div>
          </div>
         
          
         </div>
         <div class="modal-footer">
           
            <button type="button" id="btn_complete"  class="btn btn-theme btn_add_cart"><span class="loader_spinner"></span> Complete<span></span></button>
            {{-- <button type="button"  id="btn_complete" class="btn btn-theme btn_add_cart">Complete <span></span></button> --}}

         </div>
       </div>
    </div>
 </section>
<script>
   
$(document).ready(function () {
    $('#btn_complete').click(function(e) {
      $('.loader_spinner').html('<div class="spinner-border"></div>');
      $("#btn_complete").attr("disabled",true);

      var prep = $("#ready_time option:selected").val();
        e.stopPropagation();
       $.ajax({
         headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          },
            data: {'preperation_time':prep},
            url: "{{guard_url('cart/order/status_update/accept')}}/{{$order->getRoutekey()}}",
            dataType:'JSON',
            success: function(response){
               $(".close").trigger('click');
               document.location.reload()

                }
                
           
        });
       
    });
   });
  
 </script>


 <style>
     .loader_spinner{
   float: left;
   padding: 1px 3px;
   margin-top: -5px;
   margin-left: -9px;
}
.spinner-border{
       width: 0rem; 
     height: 0rem;

}
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item {
    position: relative;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .line {
    position: absolute;
    top: 15px;
    left: 0px;
    width: 100%;
    height: 1px;
    background-color: #ccc;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-name {
    background-color: #fff;
    position: relative;
    padding-right: 10px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-price {
    position: relative;
    background-color: #fff;
    padding-left: 10px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-total-wrap {
    justify-content: flex-end;
    }
    @media (max-width: 500px) {
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item {
    display: block;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .line {
    display: none;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-price {
    padding-left: 0px;
    margin-top: 5px;
    }
    .oredr-detail-modal .modal-content {
    width: 100% !important;
    }
    }
 </style>