            @include('restaurant::default.restaurant.partial.header')
            <div class="app-content-wrap">
                @include('restaurant::default.restaurant.partial.left_menu_new')
                <div class="app-content-inner">
                    <div class="app-entry-form-wrap">
                        <div class="app-sec-title app-sec-title-with-icon">
                            <i class="flaticon-invoice app-sec-title-icon"></i>
                            <h1>Statements & Reports</h1>
                            <a href="billing.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
                        </div>
                        <div class="app-conent-tabs">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" id="monthly-satement-tab" data-toggle="tab" href="#monthly-satement" role="tab" aria-controls="monthly-satement" aria-selected="true">Monthly Satements</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" id="pick-dates-tab" data-toggle="tab" href="#pick-dates" role="tab" aria-controls="pick-dates" aria-selected="false">Pick Dates</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade active show" id="monthly-satement" role="tabpanel" aria-labelledby="monthly-satement-tab">
                                    <div class="statements-list-wrap">
                                        <div class="filter-wrap">
                                            <div class="filter-item"><p class="m-0">Select Month &amp; Year</p></div>
                                            <div class="filter-item">
                                                <select name="year" id="year" class="form-control">
                                                    @for($i=2019;$i<2025;$i++)
                                                    <option value="{{$i}}">{{$i}}</option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="filter-item">
                                                <select name="month" id="month" class="form-control">
                                                    <option selected="" value="">Select Month</option>
                                                    <option value="01">Janaury</option>
                                                    <option value="02">February</option>
                                                    <option value="03">March</option>
                                                    <option value="04">April</option>
                                                    <option value="05">May</option>
                                                    <option value="06">June</option>
                                                    <option value="07">July</option>
                                                    <option value="08">August</option>
                                                    <option value="09">September</option>
                                                    <option value="10">October</option>
                                                    <option value="11">November</option>
                                                    <option value="12">December</option>
                                                </select>
                                            </div>
                                            <div class="filter-item">
                                                <button class="btn btn-secondary" id="download_pdf" style="display: none";>Download Statement</button>
                                            </div>
                                        </div>
                                        <div id="stmnt_div">
                                        <div class="statements-list-item-wrap">
                                            @php
                                            $curYear = date('Y',strtotime('-1 month' ));

                                            for ($k = $curYear; $k >= 2019; $k--){
                                                if($k == $curYear){
                                                    $i=date('m',strtotime('-1 month' )); 
                                                }
                                                else{
                                                    $i=12;
                                                }
                                                for ( ; ($k == 2019 ) ? $i >= 11 : $i>=1; $i--) {
                                                    $year = $k ;
                                                    $month = $i;
                                                    $monthName = date("F", mktime(0, 0, 0, $i, 10));
                                                @endphp
                                            <a href="{{guard_url('cart/orders/monthstmnt/download_pdf')}}?filter_date1={{$month}}&filter_date2={{$year}}" class="statements-list-item">
                                                <h5>Download Monthly Statement Of {{$monthName}} {{$year}}</h5>
                                                <i class="fas fa-download"></i>
                                            </a>
                                             @php   }
                                               
                                            }
                                        @endphp
                                             
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pick-dates" role="tabpanel" aria-labelledby="pick-dates-tab">
                                    <div class="statements-list-wrap">
                                        <div class="filter-wrap">
                                            <div class="filter-item"><p class="m-0">Filter Date</p></div>
                                            <div class="filter-item">
                                              <input type="text" id="datepicker1" class="form-control">
                                            </div>
                                            <div class="filter-item">
                                              <input type="text" id="datepicker2" class="form-control">
                                            </div>
                                            <!-- <div class="filter-item">
                                                <div id="reportrange" class="daterange">
                                                    <i class="far fa-calendar-alt"></i>&nbsp;
                                                    <span></span> <i class="fa fa-caret-down"></i>
                                                </div>
                                            </div> -->
                                            <div class="filter-item">
                                                <button type="button" class="btn btn-secondary" id="dow_pdf_search">Download Statement</button>
                                            </div>
                                        </div>
                                        <div id="search_result" class="pt-20">
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table m-0">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Order Number</th>
                                                        <th scope="col">Customer Name</th>
                                                        <th scope="col">Purchase Date</th>
                                                        <th scope="col">Order Subtotal</th>
                                                        <th scope="col">Order Tax</th>
                                                        <th scope="col">Order Tips</th>
                                                        <th scope="col">Delivery Charge</th>
                                                        <th scope="col">Zing Customer Fee</th>
                                                        <th scope="col">Min. Order Surcharge</th>
                                                        <th scope="col">Customer Total</th>
                                                        <th scope="col">Zing Eatery Fees</th>
                                                        <th scope="col">Credit Fees</th>
                                                        <th scope="col">Eatery Amount Due</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td data-label="Order Number">3950</td>
                                                        <td data-label="Customer Name">Anal</td>
                                                        <td data-label="Purchase Date">2020-09-21</td>
                                                        <td data-label="Order Subtotal">$0.01</td>
                                                        <td data-label="Order Tax">$0.00</td>
                                                        <td data-label="Order Tips">$0.00</td>
                                                        <td data-label="Delivery Charge">$0.00</td>
                                                        <td data-label="Zing Customer Fee">$0.05</td>
                                                        <td data-label="Min. Order Surcharge">$0.00</td>
                                                        <td data-label="Customer Total">$0.06</td>
                                                        <td data-label="Zing Eatery Fees">$0.50</td>
                                                        <td data-label="Credit Fees">$0.37</td>
                                                        <td data-label="Eatery Amount Due">$-0.86</td>
                                                    </tr>
                                                    <tr>
                                                        <td scope="row" data-label="Order Number">3950</td>
                                                        <td data-label="Customer Name">Anal</td>
                                                        <td data-label="Purchase Date">2020-09-21</td>
                                                        <td data-label="Order Subtotal">$0.01</td>
                                                        <td data-label="Order Tax">$0.00</td>
                                                        <td data-label="Order Tips">$0.00</td>
                                                        <td data-label="Delivery Charge">$0.00</td>
                                                        <td data-label="Zing Customer Fee">$0.05</td>
                                                        <td data-label="Min. Order Surcharge">$0.00</td>
                                                        <td data-label="Customer Total">$0.06</td>
                                                        <td data-label="Zing Eatery Fees">$0.50</td>
                                                        <td data-label="Credit Fees">$0.37</td>
                                                        <td data-label="Eatery Amount Due">$-0.86</td>
                                                    </tr>
                                                    <tr>
                                                        <td scope="row" data-label="Order Number">3950</td>
                                                        <td data-label="Customer Name">Anal</td>
                                                        <td data-label="Purchase Date">2020-09-21</td>
                                                        <td data-label="Order Subtotal">$0.01</td>
                                                        <td data-label="Order Tax">$0.00</td>
                                                        <td data-label="Order Tips">$0.00</td>
                                                        <td data-label="Delivery Charge">$0.00</td>
                                                        <td data-label="Zing Customer Fee">$0.05</td>
                                                        <td data-label="Min. Order Surcharge">$0.00</td>
                                                        <td data-label="Customer Total">$0.06</td>
                                                        <td data-label="Zing Eatery Fees">$0.50</td>
                                                        <td data-label="Credit Fees">$0.37</td>
                                                        <td data-label="Eatery Amount Due">$-0.86</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<script type="text/javascript">
    $(function() {
       $('#datepicker1').datetimepicker({
          viewMode: 'days',
          format: 'DD MMM YYYY',
          icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-arrow-up',
            down: 'fa fa-arrow-down',
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-calendar-check-o',
            clear: 'fa fa-delete',
            close: 'fa fa-times'
          }
      });
      $('#datepicker2').datetimepicker({
        viewMode: 'days',
        format: 'DD MMM YYYY',
        icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-arrow-up',
          down: 'fa fa-arrow-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-calendar-check-o',
          clear: 'fa fa-delete',
          close: 'fa fa-times'
        }
      });
    });  

    $('#datepicker2').on("dp.change", function (e) {
        e.preventDefault();
        var filter_date1 = $('#datepicker1').val();
        var filter_date2 = $('#datepicker2').val();  
        filter_ajax(filter_date1,filter_date2);
    })

    $('#datepicker1').on("dp.change", function (e) {
        
        var newdate = new Date(e.date);
        newdate.setDate(newdate.getDate()+30);
        $('#datepicker2').data("DateTimePicker").date(newdate);
        // var filter_date1 = $('#datepicker1').val();
        // var filter_date2 = $('#datepicker2').val(); 
        // filter_ajax(filter_date1,filter_date2);
    })

    function filter_ajax(filter_date1,filter_date2){
        $.ajax({
            type: 'GET',
            url: "{{ URL::to('restaurant/cart/orders/monthstmnt/filterNew') }}",
            data: {filter_date1:filter_date1,filter_date2:filter_date2},
            success: function(response){
                $('#search_result').html(response);
                $('#dow_pdf_search').show();
            },
            error: function(msg) { 

            }
        });
    }

     $(document).on('click','#dow_pdf_search',function(){   
          var filter_date1 = $('#datepicker1').val();
          var filter_date2 = $('#datepicker2').val(); 
          var download = 'Yes';
          window.location = '{{guard_url('cart/orders/monthstmnt/filterNew')}}?filter_date1='+filter_date1+'&filter_date2='+filter_date2+'&download='+download;
           
        }); 
     $("#month").on('change', function(){              
        var month = $("#month option:selected").val();                
        var year = $("#year option:selected").val();
        var month1 = $( "#month option:selected" ).text(); 
        $('#download_pdf').show();  
        $('#download_pdf').html('Download monthly statement of '+month1+' '+year);             
        $('#download_pdf').attr('href',"{{guard_url('cart/orders/monthstmnt/download_pdf')}}"+ "?filter_date1=" + month+"&filter_date2="+ year);
         $('#download_pdf').attr("target", "_blank");
    })
</script>