<div class="modal-header">
                <h5 class="modal-title" id="order_status_modalLabel">Select Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="order-type-wrap row gutter-10">
                    <!-- <div class="order-type-item">
                        <input type="radio" id="new_Order" name="order_Type" value="New Order">
                        <label for="new_Order">New Order</label>
                    </div> -->
                    <div class="order-type-item preparing col-6">
                        <input type="radio" id="preparing_Food" name="order_Type" value="preparing" class="status_change" data-order="{{!empty($order)? $order->getRouteKey(): ''}}" >
                        <label for="preparing_Food">Preparing Food</label>
                    </div>
                    <div class="order-type-item completed col-6">
                        <input type="radio" id="redy_for_Pickup" name="order_Type" value="completed" class="status_change" data-order="{{!empty($order)? $order->getRouteKey(): ''}}">
                        <label for="redy_for_Pickup">Completed</label>
                    </div>
                    <div class="order-type-item ready col-6">
                        <input type="radio" id="order_Complete" name="order_Type" value="{{(!empty($order) && $order->order_type == 'Delivery') ? 'Out for Delivery' : 'Ready for pickup'}}" class="status_change" data-order="{{!empty($order)? $order->getRouteKey(): ''}}">
                        <label for="order_Complete">{{(!empty($order) && $order->order_type == 'Delivery') ? 'Out for Delivery' : 'Ready for pickup'}}</label>
                    </div>
                    <div class="order-type-item cancel col-6">
                        <input type="radio" id="cancel_Order" name="order_Type" value="cancelled" class="status_change" data-order="{{!empty($order)? $order->getRouteKey(): ''}}">
                        <label for="cancel_Order">Cancel Order</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
      <div class="modal fade login-modal location-modal" id="cancel_confirm" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content"> 
            <div class="modal-body">
                <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
                <div class="login-wrap" style="background-color:#e8d9d9;">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                                <div class="login-header text-center">
                                  <h5><b>Are you sure?</b></h5>
                                </div>
                                
                                <div class="text-center" >
                                   <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" onclick="cancel('{{!empty($order)? $order->getRouteKey(): ''}}');">Ok</button>
                                   <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" data-dismiss="modal">Cancel</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$('.status_change').click(function(){ 
    var selected_status = $(this).val();
    if(selected_status == 'cancelled'){
        $('#cancel_confirm').modal({show:true}); 
                return;

    }
    var order_id = $(this).attr('data-order');
    $.ajax({
        url: "{{ URL::to('kitchen/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
        success: function(response){
                document.location.reload()
        }
    });
})
function cancel(order_id){ 
    var selected_status = 'cancelled';
    $.ajax({
        url: "{{ URL::to('kitchen/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
        success: function(response){
                document.location.reload()
        }
    });
}
</script>    
<style type="text/css">
    .order-type-modal .order-type-item input:checked ~ label {
/*    background-color: #40b659;
*/    color: #fff;
}
</style>    