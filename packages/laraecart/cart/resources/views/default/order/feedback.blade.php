<div class="order-wrap listing-wrap">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="listing-wrap-inner mt-30 mb-30">
					<div class="card">
						<div class="card-header">
							<div class="row">
								<div class="col-md-6"><h5 class="card-title m-0">Your Review to help others</h5></div>
								<div class="col-md-6 text-md-right"><h5 class="m-0">Order ID : {{$orders->id}}</h5></div>
							</div>
						</div>
						<div class="card-body">
							{!!Form::vertical_open()
                            -> class('form-comment')
                            -> method('POST') 
                            -> action('feedback/post/'.$orders->getRoutekey())!!}
	                            <?php
	                            $rest_data = Restaurant::getRestaurantData($orders->restaurant_id)
	                            ;?>
								<!-- <p class="text-danger">Delivery at {{date('h:m A',strtotime($orders->delivery_time))}} </p> -->
	                            <h3>Items</h3>

	                            @foreach($orders->detail as $order_det)
	                            <div class="item mb-20" style="{{@$order_det->menu_addon == 'addon' ? 'margin-left: 20px' : ''}}">
	                            	<div class="row">
	                            		<div class="col-md-3">
	                            			<h5 style="font-size: 16px;">
	                            				<b>
	                            					@if(@$order_det->menu_addon == 'menu' && (!empty($order_det->menu)))
                                            		{{@$order_det->menu->name}}
			                                        @elseif(!empty($order_det->addon))
			                                            {{@$order_det->addon->name}}
			                                        @endif
			                                    </b>
	                            			</h5>
	                            		</div>
	                            		<div class="col-md-9">
	                            			<div class="row">
	                            				<div class="col-md-6">
	                            					<label for="rate" class="mr-5">Dish Rating</label> 
                                  					<span id="rate" class="rating" data-score="{{!empty(@$menu_ratings[@$order_det->menu->id]) ? $menu_ratings[@$order_det->menu->id] : ''}}" data-menu="{{@$order_det->menu->id}}"></span>
	                            				</div>
	                            				<div class="col-md-6">
	                            					{!!Form::textarea('menu['.@$order_det->menu->id.'][comment]')
				                                   ->placeholder('Comments')
				                                   ->forceValue(!empty($menu_reviews[@$order_det->menu->id]) ? $menu_reviews[@$order_det->menu->id] : '')
				                                   ->rows(1)
				                                   ->addClass('mb-5')
				                                   ->raw()!!}
	                            				</div>
	                            			</div>
	                            			<input type="hidden" name="menu[{{@$order_det->menu->id}}][rating]"  id="rating_menu_{{@$order_det->menu->id}}" value="{{!empty($menu_ratings[@$order_det->menu->id]) ? $menu_ratings[@$order_det->menu->id] : ''}}">
	                            			<!-- <input type="hidden" name="feedback_id" id="feedback_id" value="{{@$order_det->menu->id}}">
			                                <input type="hidden" name="feedback_type" id="feedback_type" value="menu"> -->
			                                <!-- <button type="submit" id="btn_add_review" class="btn btn-theme" onclick="add_review()">Post Review</button> -->
	                            		</div>
	                            	</div>

	                            	@if(!empty(@$order_det->addons)) 
                                    @foreach(@$order_det->addons as $cart_addon)
                                      <div class="item" style="margin-left: 20px"><h5 style="font-size: 15px;">{{@$cart_addon[1]}}</h5></div>
                                    @endforeach
                                    @endif
                                    
	                            </div>
	                            @endforeach

	                            
	                            <hr>

	                            <div class="feedback-rest-info">
	                            	<img src="{{url($orders->restaurant->defaultImage('logo'))}}" style="height: 50px;" class="mb-3" alt="{{@$orders->restaurant->name}}">
	                            	<h5 class="m-0">{{@$orders->restaurant->name}}</h5>
							    	<label for="rate" class="mr-5">Eatery Rating </label>  
                              		<span id="rate" class="ratingres" data-score="{{!empty($restaurant_reviews->rating) ? $restaurant_reviews->rating : 4}}" data-res="{{@$orders->restaurant->id}}"></span>
                              		<div class="mt-10">
                              			{!!Form::textarea('res['.@$orders->restaurant->id.'][comment]')
	                                   ->placeholder('Comments')
	                                   ->forceValue(!empty($restaurant_reviews->review) ?  $restaurant_reviews->review : '')
	                                   ->rows(2)
	                                   ->raw()!!}
                              		</div>
                              		<input type="hidden" name="res[{{@$orders->restaurant->id}}][rating]"  id="rating_res_{{@$orders->restaurant->id}}" value="{{!empty($restaurant_reviews->rating) ? $restaurant_reviews->rating : ''}}">
									<!-- <input type="hidden" name="feedback_id" id="feedback_id" value="{{@$orders->restaurant->id}}">
                                	<input type="hidden" name="feedback_type" id="feedback_type" value="restaurant"> -->
	                            </div>
								<div class="text-center mt-20">
									<button type="submit" id="btn_add_review" class="btn btn-theme" onclick="add_review()" style="min-width: 200px;">Post Review</button>
								</div>
							{!!Form::close()!!} 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('.rating').raty({
        score: function() {
            return $(this).attr('data-score');
        },
        click: function(score, evt) {
            document.getElementById('rating_menu_'+$(this).attr('data-menu')).value = score;
        }
    });
    $('.ratingres').raty({
        score: function() {
        	return $(this).attr('data-score');
    	},
    	click: function(score, evt) {
    		document.getElementById('rating_res_'+$(this).attr('data-res')).value = score;
    	}
    });
});
function review(id,type){
    $('#feedback_id').val(id);
    $('#feedback_type').val(type);
    $('#post_review').show();
 }
</script>
<style>
	.ratingres .fa-star, .rating .fa-star {
		color: #ffc51b;
	}
	.ratingres .fa-star-o, .rating .fa-star-o {
		color: #ddd;
	}
</style>