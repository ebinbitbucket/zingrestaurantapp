{{--<aside class="main-nav">
   <div class="nav-inner">
        <div class="links-wrap">
<!--             <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
 -->            <a href="{{guard_url('restaurant/kitchen_menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a class="active" href="{{guard_url('cart/orders/kitchen_orders/new')}}"><i class="icon ion-cube"></i>Orders</a>

        </div>
    </div> 
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>--}}
<section class="dashboard-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-xl-12 d-none">
               
              <aside class="dashboard-sidemenu">
                    <nav class="sidebar-nav">
                        <ul>
                            <li class="nav-head"><span class="head">Navigation</span></li>
                          <!--   <li>
                                <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                            </li> -->
                             <li>
                                <a href="{{guard_url('restaurant/kitchen_menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                            </li>
                            <li class="active">
                                <a href="{{guard_url('cart/orders/kitchen_orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                            </li>

                        </ul>
                    </nav>

                </aside>
            </div>

            <div class="col-lg-12 col-xl-12">
                <div class="element-wrapper order-wrapper">
                    <a style="margin-bottom: 20px;" href="#" class="btn btn-dark print-btn " id="more_option" data-toggle="tooltip" data-placement="top" title="Print">More Options</a>

                    @include('notifications')
                    <div class="element-box">
                        
                        <div class="element-body">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane orders-card-wrap fade show active status-{{$status}}" id="{{($status == 'new') ? 'new':'default'}}" role="tabpanel" aria-labelledby="new-tab">
                                    @forelse($orders as $order)
                                    <div class="card-item" style="{{$order->order_status == 'New Orders' ? ($order->order_type == 'Delivery' ? 'background-color: rgba(45, 206, 137, 0.05);border-color: #2dce89' : 'background-color:rgba(251, 99, 64, 0.22);border-color: #fb6340') : ($order->order_type == 'Delivery' ? 'background-color: #dee2e6;border-color: #2dce89' : 'background-color: #dee2e6;border-color: #fb6340')}}">
                                        <a href="javascript:void(0);" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')">
                                            <h3><span>#{{$order->id}}</span>{{@$order->user->name}} &nbsp;&nbsp;
                                            @if($order->order_status=='Completed')
                                            <span>(Accepted)</span>
                                            @endif
                                            <div class="time"><b>{{$order->order_type}} Time:</b></div>
                                            <p class="time ion-android-calendar ml-0">{{strtolower(substr(date('l',strtotime($order->ready_time)),0,3))}}, {{date('M d',strtotime($order->ready_time))}},  {{date('h:i A',strtotime($order->ready_time))}}</p>
                                                <div class="time">
                                                    <b style="color:black;font-size: 18px; ">{{strtoupper($order->order_type)}} ORDER @if($order->order_status == 'New Orders') (New) @endif</b>
                                                </div>
                                            </h3>
                                             @if(@$order->non_contact == 1)
                                            <p ><h3 style="color:red;">No Contact Delivery</h3></p>
                                             @endif
                                            @if(@$order->car_pickup == 1)
                                            <p ><h3 style="color:red;">CAR PICKUP</h3></p>
                                             @endif
                                            @if(!empty(@$order->user->mobile))<p class="location"><i class="fa fa-phone"></i> {{substr(@$order->user->mobile, 0,3)}}-{{substr(@$order->user->mobile, 3,3)}}-{{substr(@$order->user->mobile, 6,4)}}</p>@endif
                                            
                                            <div class="item-details">
                                                  @foreach($order->detail as $order_detail)
                                        <div class="item">{{$order_detail->quantity}} x  @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                     {{$order_detail->menu->name}}
                                    @elseif(!empty($order_detail->addon))
                                     {{$order_detail->addon->name}}
                                    @endif</div>
                                    @endforeach
                                            </div>
                                        </a>
                                        <div class="price"><span>Total</span>${{number_format($order->total,2)}}</div>
                                        <div class="actions">
                                           
                                            <div class="order-type-btn" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')">
                                                <span class="btn" style="background-color: black;border-color: black;border-radius: 10px;"><span id="order_status_Label">VIEW</span></span>
                                            </div>
                                            <div class="order-type-btn" style="margin-left: 10px;"> 
                                                @if($order->order_status == 'New Orders')
                                                    <span class="btn" style="background-color: #28a745;border-color: #28a745;" >
                                                    <span id="order_status_Label" onclick="accept_model('{{$order->getRouteKey()}}')">@if($order->order_status) ACCEPT @endif</span></span>
                                   
                                                @else
                                                <span class="btn" @if($order->mail_sent > 0)style="background-color: gray;border-color: gray;" @else
                                                 style="background-color: black;border-color: black;" onclick="order_status_prep('ready_{{$order->order_type}}','{{$order->getRouteKey()}}');" @endif>
                                                    <span id="order_status_Label"><span class="loader_spinner" id="loader_spinner_{{$order->getRouteKey()}}"></span>@if($order->order_status) @if($order->order_type=='Delivery') OUT FOR @else READY FOR @endif {{strtoupper($order->order_type)}} @endif</span></span>
                                                @endif
                                            </div>
                                            @if(@$order->car_details)
                                            <div class="order-type-btn" data-toggle="modal"
                                            @if(@$order->is_car_pickup == 1)
                                            @else
                                            onclick="car_detail_model('{{$order->getRouteKey()}}')"
                                            @endif
                                            >
                                                <span class="btn" 
                                                @if(@$order->is_car_pickup == 1)
                                                style="background-color: #b98484;border-color: #b98484;border-radius: 30px;    margin-right: 5px;"
                                                @else
                                                style="background-color: red;border-color: red;border-radius: 30px;    margin-right: 5px;"
                                                @endif
                                                ><span id="order_status_Label">CUSTOMER ARRIVED</span></span>
                                            </div>
                                            @endif
                                            
                                        </div>
                                    </div>
                                    @empty
                                    @endif
                                </div>

                            </div>
                        </div>

                        <!-- <div class="orders-box-wrap">
                            @foreach($orders as $order)
                            <div class="box-item">
                                <span><b>#</b>{{$order->id}}</span>
                                <span><b>Customer Name</b>{{$order->name}}</span>
                                <span><b>Location</b>{{$order->address}}</span>
                                <span><b>Delivery Time</b>{{date('M d,Y h:m A',strtotime($order->delivery_time))}}</span>
                                <span><b>Price</b>${{number_format($order->total,2)}}</span>
                                <span class="dropdown">
                                    <b class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Status</b>
                                    <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                        <h4>Change Status</h4>
                                        <a class="dropdown-item" href="#"><span class="bg-success"></span>Paid</a>
                                        <a class="dropdown-item" href="#"><span class="bg-primary"></span>New Order</a>
                                        <a class="dropdown-item" href="#"><span class="bg-danger"></span>Cancelled</a>
                                    </div>
                                    <span class="label label-success">Paid</span>
                                </span>
                                <span class="actions"><b>Action</b><a href="#" class="btn btn-dark print-btn ion ion-android-print" data-toggle="tooltip" data-placement="top" title="Print"></a></span>
                                <a href="{{guard_url('cart/restaurant/orders/show_detail')}}/{!! $order->getRouteKey() !!}" class="overlay-link"></a>
                            </div>
                            @endforeach
                        </div> -->
                        <!-- <div class="table-responsive element-table">
                            <table id="example" class="display table table-responsive-xl">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Customer Name</th>
                                        <th scope="col">Location</th>
                                        <th scope="col">Delivery time</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Payment Status</th>
                                        <th scope="col">Action</th>

                                    </tr>
                                </thead>

                                <tbody>

                                    @foreach($orders as $order)
                                    <tr>
                                        <td>{{$order->id}}</td>
                                        <td>{{$order->name}}</td>
                                        <td>{{$order->address}}</td>
                                        <td>{{date('M d,Y h:m A',strtotime($order->delivery_time))}}</td>
                                        <td>${{number_format($order->total,2)}}</td>
                                        <td><span class="badge badge-xs badge-primary">{{$order->order_status}}</span></td>
                                        <td>{{$order->payment_status}}</td>
                                        <td><span><a href="{{guard_url('cart/restaurant/orders/show_detail')}}/{!! $order->getRouteKey() !!}"><i class="flaticon-view"></i> </a></span>
                                    </td>
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade order-type-modal" id="order_status_modal" tabindex="-1" role="dialog" aria-labelledby="order_status_modalLabel" aria-hidden="true">
<div class="modal-dialog  modal-dialog-centered" role="document">
<div class="modal-content">

<div class="modal-body">

</div>
<div class="modal-footer">

</div>
</div>
</div>
</div>

<div class="modal fade oredr-detail-modal bd-example-modal-lg" id="restaurant_order_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
<div class="modal-content">


</div>
</div>
</div>
<div class="modal fade car_modal bd-example-modal-lg" id="car_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
<div class="modal-content" id="order_modal">

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="ion ion-ios-close-outline"></i></button>
</div>
<div class="modal-body">

</div>
<div class="modal-footer">
   
</div>
</div>
</div>
</div>
<div  class="modal fade more_option_modal bd-example-modal-lg" id="more_option_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
<div class="modal-content" style="height: 800px;width:100%">

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true"><i class="ion ion-ios-close-outline"></i></button>
</div>
<div class="modal-body">
    <iframe style="height: 100%;width:100%" src="{{guard_url('kitchen/login-restaurant')}}" title="more"></iframe>

</div>
<div class="modal-footer">
   
</div>
</div>
</div>
</div>
<div  class="modal fade accept_model bd-example-modal-lg" id="accept_model" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
<div class="modal-content">


</div>
</div>
</div>
<div class="modal fade login-modal location-modal" id="cancel_confirm" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
<div class="modal-content">
<div class="modal-body">
    <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
    <div class="login-wrap">
        <div class="sign-in-wrap">
            <div class="wrap-inner">
                    <div class="login-header text-center">
                      <h5><b>Are you sure?</b></h5>
                    </div>

                    <div class="text-center" >
                       <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" onclick="cancel('{{!empty($order)? $order->getRouteKey(): ''}}');">Ok</button>
                       <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" data-dismiss="modal">Cancel</button>
                    </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script>
                 var myAudio = new Audio("{{trans_url('themes/kitchen/assets/service-bell_daniel_simion.wav')}}");

function mute_alarm(){

myAudio.pause(); 
//   x.loop = false;
//     x.load();  
console.log('hiqqq');

}
var mail_var = true;
setInterval(function(){
console.log('1a');
}, 30000);
$(document).ready(function () {

$('#more_option').click(function(e) {
e.stopPropagation();
// alert('ddd');
$('#more_option_modal').modal({show:true}); 

});
$('.actions .dropdown-menu').click(function(e) {
e.stopPropagation();
});
var count=0;
var id = '{{$id}}';
var ringcount=1;
setInterval(function(){
console.log('1b');

getNewOrders();
}, 30000);
setInterval(function(){
console.log('2a');

getScheduledUpdate();
}, 180000);
function getNewOrders() {  
$.ajax({
url: "{{ URL::to('kitchen/cart/kitchen/getNeworders') }}",
dataType:'JSON',
success: function(response){
        $('#new').html(response.new_orders);
        //$("#new_order_modal").modal();
        console.log('hi',response.new_order_count);
        if(id<response.id){                

            toastr.options.timeOut = 0;
            toastr.options.extendedTimeOut = 0;
            toastr.options.closeButton = true;
            toastr.options.onclick = function() {
                 window.location="{{guard_url('cart/orders/kitchen_orders/new')}}"; 
            }

            toastr.success('You have New Order');
            count = response.new_order_count;
            console.log('hiau',response.audio );
            if(response.audio == 'on'){
                                    console.log('hiaud');

                  myAudio.loop = true;   
                  myAudio.play();

            }
        }
        else{
            if(response.car_order_count > 0){
                            toastr.clear();
                            toastr.options.timeOut = 0;
                            toastr.options.extendedTimeOut = 0;
                            toastr.options.closeButton = true;
                            toastr.options.onclick = function() {
                                 window.location="{{guard_url('cart/orders/kitchen_orders/new')}}"; 
                            }

                            toastr.success('You have New Car Pickup Order');
                            if(ringcount == 1){
                                myAudio.loop = true;   
                              myAudio.play();
                            }
                             ringcount++;
                        }
            if(response.total_new_order_count > 0){
                toastr.clear();
                toastr.options.timeOut = 0;
                toastr.options.extendedTimeOut = 0;
                toastr.options.closeButton = true;
                toastr.options.onclick = function() {
                     window.location="{{guard_url('cart/orders/kitchen_orders/new')}}"; 
                }

                toastr.success('You have New Order');
                if(ringcount == 1){
                    myAudio.loop = true;   
                  myAudio.play();
                }
                 ringcount++;
            }
        }
      id = response.id;
}
});
}


function getScheduledUpdate() {  
$.ajax({
url: "{{ URL::to('kitchen/cart/kitchen/getKitchenScheduledupdate') }}",
dataType:'JSON',
success: function(response){
        
        return;
        
    }
    

});
}

});
function restaurant_detail_model(order_id) {
$('.oredr-detail-modal .modal-content').load("{{url('kitchen/cart/kitchen/orders/show_detail')}}"+'/'+order_id,function(){ 
    $('#restaurant_order_modal').modal({show:true}); 
});
}
function car_detail_model(order_id) {
$('.car_modal .modal-content').load("{{url('kitchen/cart/kitchen/orders/car_detail')}}"+'/'+order_id,function(){ 
    $('#car_modal').modal({show:true}); 
});
}
function order_status_modal(order_id) {
$('.order-type-modal .modal-content').load("{{url('kitchen/cart/kitchen/orders/status_model')}}"+'/'+order_id,function(){ 
$('#order_status_modal').modal({show:true}); 
});
}
function accept_model(order_id) {

$('.accept_model .modal-content').load("{{url('kitchen/cart/kitchen/orders/accept_model')}}"+'/'+order_id,function(){ 
$('#accept_model').modal({show:true}); 
});
}
// function status_change(order_id) {
//     var selected_status = $("#status_change option:selected").val();
//     $.ajax({
//             url: "{{ URL::to('kitchen/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
//             success: function(response){
//                     document.location.reload()
//             }
//         });

// }

// $('.status_change').click(function(){
//     var selected_status = $(this).val();
//     var order_id = $(this).attr('data-order');
//     $.ajax({
//             url: "{{ URL::to('restaurant/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
//             success: function(response){
//                     document.location.reload()
//             }
//         });
// })
function order_status_prep(selected_status,order_id){
    $('#loader_spinner_'+order_id).html('<div class="spinner-border"></div>');
if(mail_var == true){
mail_var = false;
 $.ajax({
url: "{{ URL::to('kitchen/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
success: function(response){
if(response.order_return){
    toastr.success(response.order_return, 'Success');
//   alert('ddd');
}
    document.location.reload()
}
});

}
}

</script>
<style>
       .loader_spinner{
   float: left;
   padding: 1px 3px;
   margin-top: -5px;
   margin-left: -9px;
}
.spinner-border{
       width: 0rem; 
     height: 0rem;

}
    </style>
