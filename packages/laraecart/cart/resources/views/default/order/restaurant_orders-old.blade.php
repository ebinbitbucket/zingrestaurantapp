<aside class="main-nav">
    <div class="nav-inner">
    @include('restaurant::default.restaurant.partial.mobile_menu')

    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-3 d-none d-lg-block">
                            <aside class="dashboard-sidemenu">
                            @include('restaurant::default.restaurant.partial.left_menu')

                            </aside>
                        </div>
                        <div class="col-md-12 col-lg-9">
                            <div class="element-wrapper">
                                
                                    
                                    
                                <div class="element-box">
                                    <div class="element-info mb-0 border-0">
                                        <div class="element-info-with-icon">
                                            <div class="element-info-icon"><div class="icon ion-cube"></div></div>
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header">Orders<a style="margin-left: 5px;"  href="#" class="btn btn-dark print-btn" id="refresh_option">Refresh</a></h5>
                                                <div class="element-inner-desc"></div>
                                            </div>
                       
                                            <form id="frm_search_orders" action="{{guard_url('cart/restaurant/orders')}}">
                                            <div class="element-info-buttons">
                                                <div class="header-search">
                                                    <div class="input-group">
                                                        <span class="input-group-addon search-close"><i class="ion-android-close"></i></span>
                                                        <input type="text" id="search_text" class="form-control" name="search[order_status]">
                                                        <span class="input-group-addon search-btn" onclick="search();"><i class="flaticon-search"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        </div>
                                    </div>
                                     <ul class="nav nav-tabs">
                       
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('restaurant/cart/restaurant/orders/new')) || (Request::is('restaurant/cart/restaurant/orders'))? 'active' : ''}}" href="{{guard_url('cart/restaurant/orders/new')}}">New Orders</a>
                            </li>
                              <li class="nav-item">
                                <a class="nav-link {{ (Request::is('restaurant/cart/restaurant/orders/pastorders'))? 'active' : ''}}" href="{{guard_url('cart/restaurant/orders/pastorders')}}">Past Orders</a>
                            </li>
                            
                        </ul>
                                    <div class="orders-card-wrap" id="{{$status == 'new' ? 'example' : 'default'}}">
                                        @foreach($orders as $order)
                                         <?php 
                                    $rest_data = Restaurant::getRestaurantData($order->restaurant_id)
                                   ?>
                                        <div class="card-item">
                                            <a href="#" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')">
                                                <h3><span>#{{$order->id}}</span>{{@$order->user->name}} 
                                                    &nbsp;&nbsp;
                                                    @if(!empty(@$order->user->mobile))
                                                      <span class="mobile"><i class="fa fa-phone"></i>&nbsp;{{substr(@$order->user->mobile, 0,3)}}-{{substr(@$order->user->mobile, 3,3)}}-{{substr(@$order->user->mobile, 6,4)}}</span>
                                                      @endif
                                                </h3>
                                              
                                                <div class="order-type-status-block">
                                                    <div class="type-item">
                                                        <b>{{$order->order_type}} Order</b>
                                                    </div>
                                                    @if($order->order_type == 'Delivery')
                                                    <div class="type-item"><span class="location ion-android-pin">{{!empty($order->address_id)? @$order->delivery_address->full_address : $rest_data->address}}</span>
                                                    </div> 
                                                     @endif
                                                     <div class="type-item">
                                                        <span><b>{{$order->order_type}} Time:</b></span>
                                                    @if($order->ready_time)
                                                        <span class="time ion-android-calendar">
                                                        {{strtolower(substr(date('l',strtotime($order->ready_time)),0,3))}}, {{date('M d',strtotime($order->ready_time))}}, {{date('h:i A',strtotime($order->ready_time))}}</span>
                                                        @else
                                                        <span class="time ion-android-calendar">
                                                            {{strtolower(substr(date('l',strtotime($order->delivery_time)),0,3))}}, {{date('M d',strtotime($order->delivery_time))}}, {{date('h:i A',strtotime($order->delivery_time))}}</span>
                                                        @endif
                                                     </div>
                                                </div>
                                                <div class="item-details">
                                                    <!-- <h5>Items</h5> -->
                                                    @foreach($order->detail as $order_detail)
                                                    <div class="item">{{$order_detail->quantity}} x  @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                 {{$order_detail->menu->name}} 
                                                @elseif(!empty($order_detail->addon))
                                                 {{$order_detail->addon->name}}  
                                                @endif</div>
                                                @endforeach
                                                </div>
                                            </a>
                                            <div class=""><span>Refund Amount </span><b>${{number_format($order->refund,2)}}</b></div>

                                            <div class="price"><span>Total</span>${{number_format($order->total,2)}}</div>
                                            <div class="actions">
<!--                                             <span class="label label-success payment-status">{{$order->payment_status}}</span>
 -->                                             <div class="order-type-btn">
                                                    <span class="btn" data-toggle="modal"                                                     onclick="order_status_modal('{{$order->id}}')"><span id="order_status_Label">{{$order->order_status}}</span></span>


                                                    <span class="btn" data-toggle="modal"                                                     onclick="order_refund_modal('{{$order->id}}')"><span id="order_status_Label">Refund</span></span>
                                                    <!-- <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                        <h4>Change Status</h4>
                                                        <div class="order-type-wrap">
                                                            <div class="order-type-item">
                                                                <input type="radio" id="new_Order" name="order_Type" value="New Order">
                                                                <label for="new_Order">New Order</label>
                                                            </div>
                                                            <div class="order-type-item">
                                                                <input type="radio" id="preparing_Food" name="order_Type" value="preparing" class="status_change" data-order="{{$order->getRouteKey()}}" data-order="{{$order->getRouteKey()}}">
                                                                <label for="preparing_Food">Preparing Food</label>
                                                            </div>
                                                            <div class="order-type-item">
                                                                <input type="radio" id="redy_for_Pickup" name="order_Type" value="completed" class="status_change" data-order="{{$order->getRouteKey()}}">
                                                                <label for="redy_for_Pickup">Completed</label>
                                                            </div>
                                                            <div class="order-type-item">
                                                                <input type="radio" id="order_Complete" name="order_Type" value="picked" class="status_change" data-order="{{$order->getRouteKey()}}">
                                                                <label for="order_Complete">Order Picked Up</label>
                                                            </div>
                                                            <div class="order-type-item">
                                                                <input type="radio" id="cancel_Order" name="order_Type" value="cancelled" class="status_change" data-order="{{$order->getRouteKey()}}">
                                                                <label for="cancel_Order">Cancel Order</label>
                                                            </div>
                                                        </div>
                                                        
                                                        <select class="form-control" id="status_change" onchange="status_change('{{$order->getRouteKey()}}')">
                                                            <option selected>Select Status</option>
                                                            <option value="preparing">Food Preparing</option>
                                                            <option value="completed">Ready for Pickup</option>
                                                            <option value="picked">Order Picked Up</option>
                                                            <option value="cancelled" >Order Cancelled</option>
                                                        </select>
                                                    </div>-->
                                                </div> 
                                                <a href="#" class="btn btn-dark print-btn ion ion-android-print" data-toggle="tooltip" data-placement="top" title="Print"></a>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                    @if($status == 'pastorders')
                                </br>
                                      <div class="row pull-right">
                                <div class="col-md-12">
                                    {{$orders->links()}}
                                </div>
                            </div>
                        </br>
                            @endif
                                    <!-- <div class="orders-box-wrap">
                                        @foreach($orders as $order)
                                        <div class="box-item">
                                            <span><b>#</b>{{$order->id}}</span>
                                            <span><b>Customer Name</b>{{$order->name}}</span>
                                            <span><b>Location</b>{{$order->address}}</span>
                                            <span><b>Delivery Time</b>{{date('M d,Y h:m A',strtotime($order->delivery_time))}}</span>
                                            <span><b>Price</b>${{number_format($order->total,2)}}</span>
                                            <span class="dropdown">
                                                <b class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Status</b>
                                                <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                    <h4>Change Status</h4>
                                                    <a class="dropdown-item" href="#"><span class="bg-success"></span>Paid</a>
                                                    <a class="dropdown-item" href="#"><span class="bg-primary"></span>New Order</a>
                                                    <a class="dropdown-item" href="#"><span class="bg-danger"></span>Cancelled</a>
                                                </div>
                                                <span class="label label-success">Paid</span>
                                            </span>
                                            <span class="actions"><b>Action</b><a href="#" class="btn btn-dark print-btn ion ion-android-print" data-toggle="tooltip" data-placement="top" title="Print"></a></span>
                                            <a href="{{guard_url('cart/restaurant/orders/show_detail')}}/{!! $order->getRouteKey() !!}" class="overlay-link"></a>
                                        </div>
                                        @endforeach
                                    </div> -->
                                    <!-- <div class="table-responsive element-table">
                                        <table id="example" class="display table table-responsive-xl">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Customer Name</th>
                                                    <th scope="col">Location</th>
                                                    <th scope="col">Delivery time</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Payment Status</th>
                                                    <th scope="col">Action</th>

                                                </tr>
                                            </thead>

                                            <tbody>
                                                
                                                @foreach($orders as $order)
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                    <td>{{$order->name}}</td>
                                                    <td>{{$order->address}}</td>
                                                    <td>{{date('M d,Y h:m A',strtotime($order->delivery_time))}}</td>
                                                    <td>${{number_format($order->total,2)}}</td>
                                                    <td><span class="badge badge-xs badge-primary">{{$order->order_status}}</span></td>
                                                    <td>{{$order->payment_status}}</td>
                                                    <td><span><a href="{{guard_url('cart/restaurant/orders/show_detail')}}/{!! $order->getRouteKey() !!}"><i class="flaticon-view"></i> </a></span>
                                                </td>
                                                </tr>
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div> -->
                                </div>
                        </div>
                    </div>
                </div>
            </section>

<div class="modal fade order-type-modal" id="order_status_modal" tabindex="-1" role="dialog" aria-labelledby="order_status_modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-lg" id="restaurant_order_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" id="order_modal" >

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order #296</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function () {
    $('input[name="order_Type"]').on('change', function() { 
        var order_stat = $('input[name="order_Type"]:checked').val();
        $("#order_status_Label").html(order_stat)
    });



    $('.actions .dropdown-menu').click(function(e) {
        e.stopPropagation();
    });
      $('#refresh_option').on('click', function() { 
        getNewOrders();
    });

    function getNewOrders() {  
        $.ajax({
            url: "{{ URL::to('restaurant/cart/restaurant/getNeworders') }}",
            dataType:'JSON',
            success: function(response){
                    console.log("New Order",response.new_orders);
                if(response.new_order_count > 0) {
                    $('#example').prepend( response.new_orders );
                    console.log("New Order");
                    //$("#new_order_modal").modal();
                    
                }
                // setTimeout(function(){ 
                //     getNewOrders();
                //     // getNewOrdersUpdate();
                //     getScheduledUpdate();
                // }, 70000);
                 
            }
        });
    }
    function getNewOrdersUpdate() {  
        $.ajax({
            url: "{{ URL::to('restaurant/cart/restaurant/getNewordersupdate') }}",
            dataType:'JSON',
            success: function(response){
                    
                    return;
                    
                }
                
           
        });
    }
       function getScheduledUpdate() {  
        $.ajax({
            url: "{{ URL::to('restaurant/cart/restaurant/getScheduledupdate') }}",
            dataType:'JSON',
            success: function(response){
                    
                    return;
                    
                }
                
           
        });
    }




});

function search() {
    if(document.getElementById('search_text').style.width!=''){
        document.getElementById("frm_search_orders").submit();

    }
}
function restaurant_detail_model(order_id) {
    $('.modal-content').load("{{url('restaurant/cart/restaurant/orders/show_detail')}}"+'/'+order_id,function(){ 
        $('#restaurant_order_modal').modal({show:true}); 
    });
}
function order_status_modal(order_id) {
    $('.modal-content').load("{{url('restaurant/cart/restaurant/orders/status_model')}}"+'/'+order_id,function(){ 
        $('#order_status_modal').modal({show:true}); 
    });
}
function order_refund_modal(order_id) {
    $('.modal-content').load("{{url('restaurant/cart/restaurant/orders/refund_model')}}"+'/'+order_id,function(){ 
        $('#order_status_modal').modal({show:true}); 
    });
}
// function status_change(order_id) {
//     var selected_status = $("#status_change option:selected").val();
//     $.ajax({
//             url: "{{ URL::to('restaurant/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
//             success: function(response){
//                     document.location.reload()
//             }
//         });
    
// }

$('.status_change').click(function(){
    var selected_status = $(this).val();
    var order_id = $(this).attr('data-order');
    $.ajax({
            url: "{{ URL::to('restaurant/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
            success: function(response){
                    document.location.reload()
            }
        });
})
</script>
<style>
.modal .dashboard-wrap {
    min-height: auto !important;
    padding: 0 !important;
    padding-top: 0 !important;
    background-color: transparent !important;
}
.modal .dashboard-wrap .element-wrapper {
    padding-bottom: 0 !important;
}
@media (min-width: 576px) {
    .order-type-modal .modal-dialog {
        max-width: 500px;
    }
}

</style>s