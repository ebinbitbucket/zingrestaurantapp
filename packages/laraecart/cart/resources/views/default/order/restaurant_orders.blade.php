@include('restaurant::default.restaurant.partial.header')
<div class="app-content-wrap">
@include('restaurant::default.restaurant.partial.left_menu_new')
   
    <div class="app-content-inner">
        <div class="app-entry-form-wrap">
            <div class="app-sec-title app-sec-title-with-icon app-sec-title-with-action">
                <i class="flaticon-shopping-cart app-sec-title-icon"></i>
                <h1>Orders</h1>
                <div class="actions">
                    <button type="button" class="btn btn-with-icon btn-dark"><i class="fas fa-sync-alt mr-5"></i>Refresh</button>
                </div>
                <a href="index.html" class="back-nav"><i class="fas fa-chevron-left"></i></a>
            </div>

            <div class="app-conent-search mb-10">
                <form action="">
                    <input type="text" class="form-control" placeholder="Search orders...">
                    <button type="submit" class="btn"><i class="fas fa-search"></i></button>
                </form>
            </div>

            <div class="app-conent-tabs">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="new-orders-tab" data-toggle="tab" href="#new-orders" role="tab" aria-controls="new-orders" aria-selected="true">New Orders</a>
                    </li>
                    <li class="nav-item" role="presentation">
                        <a class="nav-link" id="past-orders-tab" data-toggle="tab" href="#past-orders" role="tab" aria-controls="past-orders" aria-selected="false">Past Orders</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="new-orders" role="tabpanel" aria-labelledby="new-orders-tab">
                        <div class="order-list-wrap">
                            <div class="order-item">
                                <a href="#orderDetailModal" data-toggle="modal" data-target="#orderDetailModal">
                                    <span class="order-id">#16790</span>
                                    <h1>Aniruddh John</h1>
                                    <div class="order-meta-infos">
                                        <span><i class="fas fa-phone-alt mr-5"></i> 987-654-3210</span>
                                        <span>Pickup Order</span>
                                        <span>Pickup Time : Mon, Jul 20, 05:45 PM</span>
                                    </div>
                                    <div class="order-menu-items">
                                        <span>1 x Goat Biryani</span>
                                        <span>1 x Chana Puri</span>
                                    </div>
                                </a>
                                <div class="order-meta-right">
                                    <div class="order-actions">
                                        <button type="button" class="btn btn-secondary btn-icon"><i class="fas fa-print"></i></button>
                                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#orderStatusModal">Scheduled</button>
                                    </div>
                                    <div class="order-total">
                                        <span>Total</span>$21.10
                                    </div>
                                </div>
                            </div>
                            <div class="order-item">
                                <a href="#orderDetailModal" data-toggle="modal" data-target="#orderDetailModal">
                                    <span class="order-id">#42134</span>
                                    <h1>David Abraham</h1>
                                    <div class="order-meta-infos">
                                        <span><i class="fas fa-phone-alt mr-5"></i> 987-654-3210</span>
                                        <span>Pickup Order</span>
                                        <span>Pickup Time : Mon, Jul 20, 05:45 PM</span>
                                    </div>
                                    <div class="order-menu-items">
                                        <span>1 x Beef Biryani</span>
                                        <span>1 x Chana Puri</span>
                                        <span>10 x Chicken Curry</span>
                                        <span>2 x Aloo Tikka/Samosa Chat</span>
                                    </div>
                                </a>
                                <div class="order-meta-right">
                                    <div class="order-actions">
                                        <button type="button" class="btn btn-secondary btn-icon"><i class="fas fa-print"></i></button>
                                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target="#orderStatusModal">Scheduled</button>
                                    </div>
                                    <div class="order-total">
                                        <span>Total</span>$21.10
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="past-orders" role="tabpanel" aria-labelledby="past-orders-tab">...</div>
                </div>
            </div>  
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal add-menu-modal oder-detail-modal" id="orderDetailModal" tabindex="-1" aria-labelledby="orderDetailModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
        <div class="add-menu-modal-header">
            <h1>Order #16790</h1>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="flaticon-cancel"></i></button>
        <div class="modal-body">
            <div class="oder-detail-modal-header">
                <h1>David Abraham</h1>
                <div class="order-meta-infos">
                    <span><i class="fas fa-phone-alt mr-5"></i> 987-654-3210</span>
                    <span>Pickup Order</span>
                    <span>Pickup Time : Mon, Jul 20, 05:45 PM</span>
                </div>
            </div>
            <div class="oder-detail-modal-items mt-30">
                <div class="modal-sec-title mt-5">
                    <h4>Order Summary</h4>
                </div>
                <div class="menu-items-wrap">
                    <div class="menu-item">
                        <div class="menu-item-block">
                            <div class="menu-img" style="background-image: url('https://d1ph6a2qqb4pcd.cloudfront.net/uploads/restaurant/menu/2020/01/23/033519380/image/fish.jpeg')"></div>
                            <h4>fish curry X 1</h4>
                        </div>
                        <div class="menu-item-price">$12.00</div>
                    </div>
                    <div class="menu-item">
                        <div class="menu-item-block">
                            <div class="menu-img" style="background-image: url('https://www.cookwithmanali.com/wp-content/uploads/2019/09/Vegetable-Biryani-Restaurant-Style-500x500.jpg')"></div>
                            <h4>Vegetable Biryani X 1</h4>
                        </div>
                        <div class="menu-item-price">$20.00</div>
                    </div>
                </div>
                <div class="order-total-wrap">
                    <div class="order-total-item">
                        <span>Taxes and Fees :</span>
                        <span>$0.45</span>
                    </div>
                    <div class="order-total-item">
                        <span>Tip :</span>
                        <span>$2.69</span>
                    </div>
                    <div class="order-total-item">
                        <span>Order Total</span>
                        <span>$21.10</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="modal fade entry-modal" id="orderStatusModal" tabindex="-1" aria-labelledby="orderStatusModalLabel" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="orderStatusModalLabel">Select Status</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="flaticon-cancel"></i>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col">
                    <div class="form-group m-0">
                        <div class="radio-button">
                            <input type="radio" id="redy_for_Pickup" name="order_Type">
                            <label for="redy_for_Pickup">Accept</label>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group m-0">
                        <div class="radio-button">
                            <input type="radio" id="cancel_Order" name="order_Type">
                            <label for="cancel_Order">Cancel Order</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>