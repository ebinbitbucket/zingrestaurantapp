<div class="modal-header">
                <h5 class="modal-title" id="order_status_modalLabel">Refund details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form class="form-example" id="refund_frm">
                    <!-- Input fields -->
    
                <input type="hidden"  class="form-control"  placeholder="" value="{{$order->id}}" name="id">

                  
                    <div class="form-group">
                        <label for="order_amount">Refund Amount : </label>
                        <input type="text" required class="form-control order_amount" id="order_amount" placeholder="" name="order_amount">
                    </div>
                    
                    <div class="form-group">
                        <label for="reason">Reason for Refund Request : </label>
                        <textarea id="reason" required placeholder="" name="reason" class="form-control reason"></textarea>
                    </div>
    
                        
                    <button style="margin-bottom: 50px;margin-top:20px" type="submit" 
                    class="btn btn-theme btn-customized">Submit </button>
                </form>
            </div>
            <div class="modal-footer">
                {{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> --}}
            </div>
          <script>
              $(document).ready(function(){
                $("#refund_frm").submit(function(e){
                    e.preventDefault(); // avoid to execute the actual submit of the form.

var form = $(this);
var url = "{{url('restaurant/cart/restaurant/orders-refund-save')}}";

$.ajax({
       type: "GET",
       url: url,
       data: form.serialize(), // serializes the form's elements.
       success: function(data)
       {
          window.location.reload();
       }
     });
  });
});
          </script>
<style type="text/css">
    .order-type-modal .order-type-item input:checked ~ label {
    /*background-color: #40b659;*/
    color: #fff;
}
</style>    