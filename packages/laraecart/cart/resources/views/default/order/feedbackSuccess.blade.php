<section class="payment-success-wrap" style="min-height: 563px;">
    <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-8">
            <div class="success-card-wrap">
              <div class="success-card-item success-header">
                <div class="icon"><i class="ion ion-checkmark-circled"></i></div>
                <div class="content">
                  <p>Thank you for your valuable feedback. You reviews will help many others. We really appreciate it.</p>
                </div>
              </div>

            </div>
          </div>
        </div>
    </div>
</section>
