<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                        <li >
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/billing')}}"><i class="icon ion-card"></i><span>Account Details</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('restaurant/menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/restaurant_addons')}}"><i class="icon ion-aperture"></i><span>Addons</span></a>
                                        </li>
                                        <li class="active">
                                            <a href="{{guard_url('cart/restaurant/orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                        <li >
                                            <a href="{{guard_url('restaurant/accounts')}}"><i class="icon ion-social-buffer"></i><span>Eatery Details</span></a>
                                        </li>
                                         <li>
                                            <a href="{{guard_url('restaurant/schedule')}}"><i class="icon ion-social-buffer"></i><span>Eatery Schedule</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('kitchen/restaurant/kitchen')}}"><i class="icon ion-social-buffer"></i><span>Kitchen</span></a>
                                        </li>
                                        <li>
                                            <a href="{{guard_url('expense/expense')}}"><i class="fa fa-sticky-note-o"></i><span>Invoice</span></a>
                                        </li>

                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-md-9">
                            <div class="element-wrapper order-detail-wrap">
                                <div class="element-box">
                                    <div class="element-info">
                                        <div class="element-info-with-icon">
                                            <div class="element-info-icon"><div class="icon ion-cube"></div></div>
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header">#{{$order->id}} Order </h5>
                                                <div class="element-inner-desc"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pipeline-spreads">
                                        <div class="status-stages">
                                             
                                            <div class="status-stage {{ $order->order_status =='Preparing' || $order->order_status =='Completed' || $order->order_status =='Picked up' || $order->order_status =='Delivered' ? ' active' : 'empty' }}">
                                                <div class="status-stage-label-container">
                                                    <div class="status-stage-label">
                                                        <div><span>Move to: </span>Food Preparing</div>
                                                    </div>
                                                </div>
                                                <div class="horizontal-line col-12">
                                                    <div class="horizontal-line-pre-line"></div>
                                                    <div class="horizontal-line-content">
                                                        <a href="{{guard_url('cart/order/status_update/preparing')}}/{{$order->getRoutekey()}}"><div class="status-stage-checkbox">
                                                            <span class="icon fa fa-check"></span>
                                                        </div></a>
                                                    </div>
                                                    <div style= "{{ $order->order_status =='Preparing' || $order->order_status =='Completed' || $order->order_status =='Picked up' || $order->order_status =='Delivered' ? 'border-color : #2dce89;' : '' }}" class="horizontal-line-post-line"></div>
                                                </div>
                                            </div>
                                            <div class="status-stage {{ $order->order_status =='Completed' || $order->order_status =='Picked up' || $order->order_status =='Delivered' ? ' active' : 'empty' }}">
                                                <div class="status-stage-label-container">
                                                    <div class="status-stage-label">
                                                        <div><span>Move to: </span>Ready for pickup</div>
                                                    </div>
                                                </div>
                                                <div class="horizontal-line col-12">
                                                    <div class="horizontal-line-pre-line"></div>
                                                    <div class="horizontal-line-content">
                                                        <a href="{{guard_url('cart/order/status_update/completed')}}/{{$order->getRoutekey()}}"><div class="status-stage-checkbox">
                                                            <span class="icon fa fa-check"></span>
                                                        </div></a>
                                                    </div>
                                                    <div style= "{{ $order->order_status =='Completed' || $order->order_status =='Picked up' || $order->order_status =='Delivered' ? 'border-color : #2dce89;' : '' }}" class="horizontal-line-post-line"></div>
                                                </div>
                                            </div>
                                        
                                            
                                            <div class="status-stage {{ $order->order_status =='Picked up' || $order->order_status =='Delivered' ? ' active' : 'empty' }}">
                                                <div class="status-stage-label-container">
                                                    <div class="status-stage-label">
                                                        <div><span>Move to: </span>Order Picked Up</div>
                                                    </div>
                                                </div>
                                                <div class="horizontal-line col-12">
                                                    <div class="horizontal-line-pre-line"></div>
                                                    <div class="horizontal-line-content">
                                                        <a href="{{guard_url('cart/order/status_update/picked')}}/{{$order->getRoutekey()}}"><div class="status-stage-checkbox">
                                                            <span class="icon fa fa-check"></span>
                                                        </div></a>
                                                    </div>
                                                    @if(!empty($order->address_id))
                                                    <div style= "{{ $order->order_status == 'Picked up' || $order->order_status =='Delivered' ? 'border-color : #2dce89;' : '' }}"  class="horizontal-line-post-line"></div>
                                                    @endif
                                                </div>
                                            </div>
                                            @if(!empty($order->address_id))
                                            <div class="status-stage {{ $order->order_status =='Delivered' ? ' active' : 'empty' }}">
                                                <div class="status-stage-label-container">
                                                    <div class="status-stage-label">
                                                        <div><span>Move to: </span>Order Delivered</div>
                                                    </div>
                                                </div>
                                                <div class="horizontal-line col-12">
                                                    <div class="horizontal-line-pre-line"></div>
                                                    <div class="horizontal-line-content">
                                                        <a href="{{guard_url('cart/order/status_update/delivered')}}/{{$order->getRoutekey()}}"><div class="status-stage-checkbox">
                                                            <span class="icon fa fa-check"></span>
                                                        </div></a>
                                                    </div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--                                                     <div class="horizontal-line-post-line"></div>
 -->                                                </div>
                                            </div>
                                            @endif
                                                <div class="status-stage {{ $order->order_status =='Cancelled' ? ' active' : 'empty' }}">
                                                <div class="status-stage-label-container">
                                                    <div class="status-stage-label">
                                                        <div><span>Move to: </span>Order Cancelled</div>
                                                    </div>
                                                </div>
                                                <div class="horizontal-line col-12">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<!--                                                     <div class="horizontal-line-pre-line"></div>
 -->                                                    <div class="horizontal-line-content">
                                                        <a href="{{guard_url('cart/order/status_update/cancelled')}}/{{$order->getRoutekey()}}"><div class="status-stage-checkbox">
                                                            <span class="icon fa fa-check"></span>
                                                        </div></a>
                                                    </div>
                                                    <div class="horizontal-line-post-line"></div>
                                                </div>
                                            </div>
                                                
                                              
                                            
                                        </div>
                                    </div>
                                    <hr>
                                     <?php 
                                    $rest_data = Restaurant::getRestaurantData($order->restaurant_id)
                                   ?>
                                    <div class="order-details-wrap">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="order-detail-item">
                                                    <p><span>Order Id</span>#{{$order->id}}</p>
                                                </div>
                                            </div>
                                        
                                            <div class="col-md-4">
                                                <div class="order-detail-item">
                                                    <p><span>Customer Name</span>{{$order->name}}</p>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="order-detail-item">
                                                    <p><span>Payment Status</span>{{$order->payment_status}}</p>
                                                </div>
                                            </div> 
                                            <div class="col-md-8">
                                                <div class="order-detail-item">
                                                    <p><span>{{!empty($order->address_id)? 'Delivery' : 'Pickup' }} Location</span>{{!empty($order->address_id)? @$order->delivery_address->address : $rest_data->address}}</p>
                                                </div>
                                            </div>
                                              
                                            
                                            <div class="col-md-4">
                                                <div class="order-detail-item">
                                                    <p><span>{{!empty($order->address_id)? 'Delivery' : 'Pickup' }} Time</span>{{date('M d,Y h:m A',strtotime($order->delivery_time))}}</p>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <hr class="mb-0">
                                    <div class="row">
                                        <!-- <div class="col-md-6 border-right pt-30">
                                            <div class="menu-restaurant-wrap">
                                                <div class="res-img">
                                                    <img src="img/restaurants/thumb/restaurant-01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="res-content">
                                                    <p>Order From <span>Jet's Kitchen</span></p>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-md-12 pt-30">
                                            <div class="order-contains-wrap pl-0">
                                                <h4>Order Summary</h4>
                                                @foreach($order->detail as $order_detail)
                                               
                                                <div class="item">
                                                    <div class="item-name"> @if(!empty($order_detail->menu))<div class="cell-img" style="background-image: url({{url($order_detail->menu->defaultImage('image'))}})"></div>
                                                    @endif
                                                    @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                        {{$order_detail->menu->name}} 
                                                    @elseif(!empty($order_detail->addon))
                                                        {{$order_detail->addon->name}}  
                                                    @endif
                                                    <span> </span>
                                                </div>
                                                    <div class="item-price"><span>{{$order_detail->quantity}}x</span> ${{number_format($order_detail->unit_price,2)}}</div>
                                            </div>
                                               @endforeach
                                        </div>
                                            <div class="order-total-wrap pl-0">
                                                <h4>Order Total</h4>
                                                <h4>${{number_format($order->total,2)}}</h4>
                                            </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>