<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12">
                            <div class="element-wrapper order-wrapper">
                                @include('notifications')
                                <div class="element-box">
                                    
                                    <div class="element-body">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane orders-card-wrap fade show active status-{{$status}}" id="{{($status == 'new') ? 'new':'default'}}" role="tabpanel" aria-labelledby="new-tab">
                                                @forelse($orders as $order)
                                                <div class="card-item" style="{{$order->order_status == 'New Orders' ? ($order->order_type == 'Delivery' ? 'background-color: rgba(45, 206, 137, 0.05);border-color: #2dce89' : 'background-color: rgba(251, 99, 64, 0.05);border-color: #fb6340') : ($order->order_type == 'Delivery' ? 'background-color: #dee2e6;border-color: #2dce89' : 'background-color: #f5f5f5; border-color: #fb6340')}}">
                                                    <a href="javascript:void(0);">
                                                        <div class="card-top">
                                                            <h3>#{{$order->id}} {{@$order->user->name}} <span>{{$order->order_type}} Order @if($order->order_status == 'New Orders') (New) @endif</span></h3>
                                                            <div class="meta-infos">
                                                                <span><i class="fa fa-calendar"></i> {{strtolower(substr(date('l',strtotime($order->delivery_time)),0,3))}}, {{date('M d',strtotime($order->delivery_time))}},  {{date('h:i A',strtotime($order->delivery_time))}}</span>
                                                                @if(!empty(@$order->user->mobile))
                                                                <span><i class="fa fa-phone"></i> {{substr(@$order->user->mobile, 0,3)}}-{{substr(@$order->user->mobile, 3,3)}}-{{substr(@$order->user->mobile, 6,4)}}</span>
                                                                @endif
                                                            </div>
                                                        
                                                        
                                                        <div class="item-details">
                                                              @foreach($order->detail as $order_detail)
                                                    <div class="item">{{$order_detail->quantity}} x  @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                 {{$order_detail->menu->name}}
                                                @elseif(!empty($order_detail->addon))
                                                 {{$order_detail->addon->name}}
                                                @endif</div>
                                                @endforeach
                                                        </div>
                                                        </div>
                                                    </a>
                                                    <div class="card-botom">
                                                        <div class="bottom-item"><span class="price">${{number_format($order->total,2)}}</span></div>
                                                        <div class="bottom-item bottom-actions text-right">
                                                            <div class="order-type-btn">
                                                                <span class="btn" style="background-color: black;border-color: black;" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')"><span id="order_status_Label">View</span></span>
                                                                @if($order->order_status == 'New Orders')
                                                                    <span class="btn" style="background-color: #28a745;border-color: #28a745;" onclick="order_status_prep('accept','{{$order->getRouteKey()}}');">
                                                                    <span id="order_status_Label"><i class="fa fa-check"></i> @if($order->order_status) Accept @endif</span></span>
                                                                @elseif($order->order_status == 'Preparing')
                                                                    @if($order->order_type == 'Delivery')
                                                                        <span class="btn" style="background-color: #28a745;border-color: #28a745;" onclick="order_status_prep('Out for Delivery','{{$order->getRouteKey()}}');">
                                                                        <span id="order_status_Label"><i class="fa fa-truck"></i> Out for Delivery</span></span>
                                                                    @else
                                                                        <span class="btn" style="background-color: #28a745;border-color: #28a745;" onclick="order_status_prep('Ready for pickup','{{$order->getRouteKey()}}');">
                                                                    <span id="order_status_Label"><i class="fa fa-gift"></i> Ready for Pickup</span></span>
                                                                    @endif
                                                                @elseif($order->order_status == 'Completed')
                                                                    <span class="btn" style="background-color: #28a745;border-color: #28a745;">
                                                                    <span id="order_status_Label"><i class="fa fa-check-circle"></i> Completed</span></span>
                                                                @else
                                                                    <span class="btn" style="background-color: #28a745;border-color: #28a745;" onclick="order_status_prep('completed','{{$order->getRouteKey()}}');">
                                                                    <span id="order_status_Label">Complete</span></span>
                                                                @endif
                                                            </div>
                                                              @if(!empty($order->restaurant->memobirdID))
                                                            <a href="{{guard_url('cart/orders/print')}}/{{$order->getRouteKey()}}" class="btn btn-dark print-btn ion ion-android-print" data-toggle="tooltip" data-placement="top" title="Print"></a>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                @empty
                                                No new orders.
                                                @endif
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

<div class="modal fade order-type-modal" id="order_status_modal" tabindex="-1" role="dialog" aria-labelledby="order_status_modalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>

<div class="modal fade oredr-detail-modal bd-example-modal-lg" id="restaurant_order_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" id="order_modal">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order #296</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade login-modal location-modal" id="cancel_confirm" tabindex="-1" role="dialog" aria-labelledby="signIn_signUp_ModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <!-- <a href="{{url('/')}}" class="close"><i class="ion ion-ios-close-outline"></i></a> -->
                <div class="login-wrap">
                    <div class="sign-in-wrap">
                        <div class="wrap-inner">
                                <div class="login-header text-center">
                                  <h5><b>Are you sure?</b></h5>
                                </div>

                                <div class="text-center" >
                                   <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" onclick="cancel('{{!empty($order)? $order->getRouteKey(): ''}}');">Ok</button>
                                   <button type="button" style="margin-left: 10px; position: relative; border-radius: 0%; width: 100px; margin-top: 10px;" class="btn btn-theme search-btn" data-dismiss="modal">Cancel</button>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function () {
    $('.actions .dropdown-menu').click(function(e) {
        e.stopPropagation();
    });
    var count=0;
    setInterval(function(){
        getNewOrders();
        getScheduledUpdate();
    }, 7000);
    function getNewOrders() {
        $.ajax({
            url: "{{ URL::to('kitchen/cart/kitchen/getNeworders') }}",
            dataType:'JSON',
            success: function(response){
                if(response.new_orders != '') {
                //  document.location.reload()
                }
                    // $('#new').prepend(response.new_orders);
                    // //$("#new_order_modal").modal();
                    // if(count<response.new_order_count){
                    //     toastr.options.timeOut = 0;
                    //     toastr.options.extendedTimeOut = 0;
                    //     toastr.options.closeButton = true;
                    //     toastr.options.onclick = function() {
                    //          window.location="{{guard_url('cart/orders/kitchen_orders/new')}}"; 
                    //     }

                    //     //toastr.success('You have New Order');
                    //     console.log('hi',response.audio);
                    //     count = response.new_order_count;

                    // }
                    // else{
                    //     if(response.total_new_order_count > 0){
                    //         toastr.clear();
                    //         toastr.options.timeOut = 0;
                    //         toastr.options.extendedTimeOut = 0;
                    //         toastr.options.closeButton = true;
                    //         toastr.options.onclick = function() {
                    //              window.location="{{guard_url('cart/orders/kitchen_orders/new')}}"; 
                    //         }

                    //         //toastr.success('You have New Order');
                    //     }
                    // }

            }
        });
    }
   function getScheduledUpdate() {
        $.ajax({
            url: "{{ URL::to('kitchen/cart/kitchen/getKitchenScheduledupdate') }}",
            dataType:'JSON',
            success: function(response){

                    return;

                }


        });
    }

});
function restaurant_detail_model(order_id) {
    $('.oredr-detail-modal .modal-content').load("{{url('kitchen/cart/kitchen/orders/show_detail')}}"+'/'+order_id,function(){
                $('#restaurant_order_modal').modal({show:true});
            });
}

function order_status_modal(order_id) {
    $('.order-type-modal .modal-content').load("{{url('kitchen/cart/kitchen/orders/status_model')}}"+'/'+order_id,function(){
        $('#order_status_modal').modal({show:true});
    });
}
// function status_change(order_id) {
//     var selected_status = $("#status_change option:selected").val();
//     $.ajax({
//             url: "{{ URL::to('kitchen/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
//             success: function(response){
//                     document.location.reload()
//             }
//         });

// }

$('.status_change').click(function(){
    alert('hi');
    var selected_status = $(this).val();
    var order_id = $(this).attr('data-order');
    $.ajax({
            url: "{{ URL::to('kitchen/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
            success: function(response){
                    document.location.reload()
            }
        });
})
function order_status_prep(selected_status,order_id){
    $.ajax({
        url: "{{ URL::to('kitchen/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
        success: function(response){
                document.location.reload()
        }
    });
}
</script>

<style>
    .main-wrap.auth-wrap .dashboard-wrap {
        min-height: 100vh;
        padding-top: 15px;
        padding-bottom: 15px;
        background-color: #fff;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper {
        margin-bottom: 0;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box {
        background-color: transparent;
        padding: 0;
        -webkit-box-shadow: none;
        box-shadow: none;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body {
        padding: 0;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item {
        border: none;
        /* border: 1px solid #dedede !important; */
        background-color: transparent !important;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .card-top h3 {
        margin-top: 0;
        font-size: 14px;
        text-transform: capitalize;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .card-top h3 span {
        float: right;
        color: #444;
        font-weight: normal;
        font-size: 13px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .meta-infos {
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;
        -ms-align-items: center;
        align-items: center;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .meta-infos span {
        color: #555;
        font-size: 13px;
        text-transform: capitalize;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .meta-infos span + span {
        margin-left: 10px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-box .orders-card-wrap .card-item .item-details {
        padding-right: 0;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .card-botom {
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;
        -ms-align-items: center;
        align-items: center;
        justify-content: space-between;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .bottom-actions {
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .bottom-actions .btn {
        color: #fff;
        font-size: 13px;
        padding: 7px 15px;
        cursor: pointer;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .bottom-actions .print-btn {
        width: 33px;
        height: 33px;
        display: inline-block;
        padding: 0;
        text-align: center;
        line-height: 33px;
        margin-left: 5px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .card-botom .price {
        font-size: 14px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item {
        padding: 0;
        border-radius: 6px;
        /* overflow: hidden; */
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .card-top {
        padding: 10px;
        box-shadow: 0px 5px 8px 2px rgba(0, 0, 0, 0.1);
        display: block;
        background-color: #fff;
        position: relative;
        border: 1px solid #eaeaea;
        border-radius: 6px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .element-wrapper.order-wrapper .element-box .element-body .orders-card-wrap.status-new .card-item .card-botom {
        padding: 12px 10px;
        background-color: #eaeaea;
        border-bottom-right-radius: 6px;
        border-bottom-left-radius: 6px;
    }
    .oredr-detail-modal {
        width: 100%;
    }
</style>
