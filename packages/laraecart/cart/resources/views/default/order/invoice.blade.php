
<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
    </head>

    <body>

        <style type="text/css">
            body {
                margin: 0;
                padding: 0;
                font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
                font-size: 14px;
                line-height: 1.42857143;
                color: #333;
            }
            h1, h2, h3, h4, h5, h6 {
                font-weight: 700;
                line-height: 1.2;
                margin-top: 0;
            }
            .list-unstyled {
                padding-left: 0;
                list-style: none;
            }
            ol, ul {
                margin-bottom: 10px;
            }
            .text-right {
                text-align: right;
            }
            .row {
                margin-right: -15px;
                margin-left: -15px;
            }
            .col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
                position: relative;
                min-height: 1px;
                padding-right: 15px;
                padding-left: 15px;
            }
            .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9 {
                    float: left;
            }
            .col-md-6 {
                width: 50%;
            }
            .col-sm-12 {
                width: 100%;
            }
            .text-muted {
                color: #98a6ad;
            }
            table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 20px;
                border-spacing: 0;
                border-collapse: collapse;
            }
            table>tbody>tr>td, table>tbody>tr>th, table>tfoot>tr>td, table>tfoot>tr>th, table>thead>tr>td, table>thead>tr>th {
                padding: 8px;
                line-height: 1.42857143;
                vertical-align: top;
                border-top: 1px solid #ddd;
            }
            table>thead>tr>th {
                vertical-align: bottom;
                border-bottom: 2px solid #ddd;
            }
            table>caption+thead>tr:first-child>td, table>caption+thead>tr:first-child>th, table>colgroup+thead>tr:first-child>td, table>colgroup+thead>tr:first-child>th, table>thead:first-child>tr:first-child>td, table>thead:first-child>tr:first-child>th {
                border-top: 0;
            }
            .lead {
                font-size: 21px;
                margin-bottom: 20px;
            }
            .row:after, .row:before {
                display: table;
                content: " ";
            }
            .row:after {
                clear: both;
            }
            p {
                margin: 0 0 10px;
            } 
            h6 {
                font-weight: 600;
                margin-top: 0;
                font-size: 15px;
                text-transform: capitalize;
            }
            .col-md-offset-7 {
                margin-left: 58.33333333%;
            }
        </style>
        <section class="card">
            <div id="invoice-template" class="card-block">
                <div id="invoice-company-details" class="row">
                    <div class="col-md-6 col-sm-12 text-left">
                        <h2>UKSHOPPINGCART.COM</h2>
                        <ul class="ml-2 px-0 list-unstyled">
                            <li><b>Delivery Address</b></li>
                            <li>{{$orders['shipping_name']}}</li>
                            <li>{{$orders->shipping_address}},</li>
                            <li>{{$orders->shipping_street}}, {{$orders->shipping_zipcode}}</li>
                            <li>{{$orders->shipping_phone}}</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right">
                        <h2>INVOICE</h2>
                        <p class="pb-3">ORDER#{{$orders['id']}}</p>
                        <ul class="px-0 list-unstyled">
                            <li>Total Amount</li>
                            <li class="lead">$ {{$orders['total']}}</li>
                        </ul>
                    </div>
                </div>

                <!-- Invoice Customer Details -->
                <div id="invoice-customer-details" class="row pt-2">
                    <div class="col-sm-12 text-left">
                        <p class="text-muted">Invoice Address</p>
                    </div>
                    <div class="col-md-6 col-sm-12 text-left">
                        <ul class="px-0 list-unstyled">
                            <li>{{$orders['name']}}</li>
                            <li>{{$orders->address}},</li>
                            <li>{{$orders->street}}, {{$orders->zipcode}}</li>
                            <li>{{$orders->phone}}</li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right">
                        <p><span class="text-muted">Invoice Date :</span> {{format_date($orders['created_at'])}}</p>
                        <p><span class="text-muted">Payment Status :</span> {{$orders['payment_status']}}</p>
                    </div>
                </div>
                <div id="invoice-items-details" class="pt-2">
                    <div class="row">
                        <div class="table-responsive col-sm-12">
                            <table class="table">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Item & Description</th>
                                  <th class="text-right">Qty</th>
                                  <th class="text-right">Unit Price</th>
                                  <th class="text-right">Amount</th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($orders->detail as $detail)
                                <tr>
                                  <th scope="row">
                                      {{$detail->product->code}}
                                  </th>
                                  <td>
                                    <p>{{$detail->product->title}}</p>
                                    <p class="text-muted">Size: {{$detail->size}}</p>
                                  </td>
                                  <td class="text-right">{{$detail->quantity}}</td>
                                  <td class="text-right">$ {{$detail->unit_price}}</td>
                                  <td class="text-right">$ {{$detail->price}}</td>
                                </tr>
                                @endforeach
                              </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-md-offset-7 col-sm-12">
                            <p class="lead">Total Amount</p>
                            <div class="table-responsive">
                                <table class="table">
                                  <tbody>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td class="text-right">$ {{$orders->subtotal}}</td>
                                    </tr>
                                    <tr class="bg-grey bg-lighten-4">
                                        <td><b>Total</b></td>
                                        <td class="text-right"><b>$ {{$orders->total}}</b></td>
                                    </tr>
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
</html>
