            <table class="table" id="main-table" data-url="{!!guard_url('cart/order?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="user_id">{!! trans('cart::order.label.user_id')!!}</th>
                    <th data-field="address_id">{!! trans('cart::order.label.address_id')!!}</th>
                    <th data-field="track_status">{!! trans('cart::order.label.track_status')!!}</th>
                    <th data-field="total">{!! trans('cart::order.label.total')!!}</th>
                    <th data-field="payment_status">{!! trans('cart::order.label.payment_status')!!}</th>
                    <th data-field="payment_methods">{!! trans('cart::order.label.payment_methods')!!}</th>
                    <th data-field="payment_details">{!! trans('cart::order.label.payment_details')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">app.actions</th>
                    </tr>
                </thead>
            </table>