
                        @if($orders->count() != 0) 

                            <table class="table table-hover" style="border: 1px solid #e1e2e1;">                   
                                <thead class="thead-dark">
                                    <tr>
                                        <th>#</th>
                                        <th>Order Date</th>
                                        <th>Order Status</th>
                                        <th>Payment Status</th>
                                        <th>Amount</th>
                                        <th class="text-right">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($orders as $order)
                                    <tr>                       
                                        <td>ORDER#{{$order->id}}</td>
                                        <td>{{format_date($order->created_at)}}</td>
                                        <td><span class="label label-warning">{{$order->order_status}}</span></td>
                                        <td>{{$order->payment_status}}</td>
                                        <td>{{$order->total}}</td>
                                        <td class="text-right">
                                            <a href="{{guard_url('cart/order/details/'.$order->getRoutekey())}}" class="mr-5"><i class="fa fa-eye" title="View Details"></i></a>
                                            @if($order->payment_status == 'Paid')
                                            <a href="{{guard_url('cart/invoice/'.$order->id)}}"><i class="fa fa-file-pdf-o" title="Print Invoice"></i></a>
                                            @else
                                            <a href="{!! guard_url('cart/order') !!}/{!! $order->getRouteKey() !!}" class="text-danger" data-toggle="tooltip" data-placement="left" title="Delete" data-action="DELETE" data-remove="{!! $order->getRouteKey() !!}"><i class="icon-trash"></i></a> 
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <h3>No orders found!</h3>
                        @endif
                   