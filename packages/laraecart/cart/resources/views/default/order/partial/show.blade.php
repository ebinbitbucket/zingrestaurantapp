                    <div class="list-view">
                        @forelse($orders as $order) 
                        <div class="card list-view-media"  id="{!! $order->getRouteKey() !!}">
                            <div class="card-block">
                                <div class="media">
                                    
                                    <div class="media-body">
                                        <div class="heading">
                                            <h3>Ordered on {{format_date($order->created_at)}}</h3>
                                            <table>
                                                <col width="200">
                                                <col width="150">
                                            <tr><th>Name</th><th>Price</th></tr>
                                            
                                            @foreach($order->detail as $detail)
                                            
                                            <tr><td><a href="{{url('product')}}/{{$detail->product->slug}}">{{$detail->product->name}}</td>
                                            <td>₹ {{$detail->price}}</td></tr>
                                            @endforeach
                                        </table>
                                        
                                            <div class="status">
                                                <h4 align="right"><a href="{{url('order/view')}}/{{$order->id}}">{{$order->payment_status}}</a></h4>
                                                @if($order->payment_status=='Unpaid')

                                               
                                                   <a href="{{guard_url('cart/repayment')}}/{{$order->id}}" ><input type="button" class="btn btn-call" value="Make Payment" id="btn"></a>
                                                                  
                                               @endif
                                            </div>
                                        </div>
                                        <p><h4>Total Amount : ₹ {{$order->total}}</h4></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endif
                </div>