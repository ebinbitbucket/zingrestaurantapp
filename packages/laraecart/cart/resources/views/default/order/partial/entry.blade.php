            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('address_id')
                       -> label(trans('cart::order.label.address_id'))
                       -> placeholder(trans('cart::order.placeholder.address_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('track_status')
                       -> label(trans('cart::order.label.track_status'))
                       -> placeholder(trans('cart::order.placeholder.track_status'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('delivery_id')
                       -> label(trans('cart::order.label.delivery_id'))
                       -> placeholder(trans('cart::order.placeholder.delivery_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('subtotal')
                       -> label(trans('cart::order.label.subtotal'))
                       -> placeholder(trans('cart::order.placeholder.subtotal'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('tax')
                       -> label(trans('cart::order.label.tax'))
                       -> placeholder(trans('cart::order.placeholder.tax'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('total')
                       -> label(trans('cart::order.label.total'))
                       -> placeholder(trans('cart::order.placeholder.total'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('payment_status')
                    -> options(trans('cart::order.options.payment_status'))
                    -> label(trans('cart::order.label.payment_status'))
                    -> placeholder(trans('cart::order.placeholder.payment_status'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::select('payment_methods')
                    -> options(trans('cart::order.options.payment_methods'))
                    -> label(trans('cart::order.label.payment_methods'))
                    -> placeholder(trans('cart::order.placeholder.payment_methods'))!!}
               </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('payment_details')
                       -> label(trans('cart::order.label.payment_details'))
                       -> placeholder(trans('cart::order.placeholder.payment_details'))!!}
                </div>
            </div>