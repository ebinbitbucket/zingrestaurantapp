                                  @forelse($orders as $order)
                                                <div class="card-item">
                                                    <a href="javascript:void(0);" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')">
                                                        <h3><span>#{{$order->id}}</span>{{@$order->user->name}} &nbsp;&nbsp;<div class="time"><b>{{$order->order_type}} Time:</b></div>
                                                        <p class="time ion-android-calendar ml-0">{{strtolower(substr(date('l',strtotime($order->delivery_time)),0,3))}}, {{date('M d',strtotime($order->delivery_time))}},  {{date('h:i A',strtotime($order->delivery_time))}}</p></h3>
                                                        @if(!empty(@$order->user->mobile))<p class="location"><i class="fa fa-phone"></i> {{substr(@$order->user->mobile, 0,3)}}-{{substr(@$order->user->mobile, 3,3)}}-{{substr(@$order->user->mobile, 6,4)}}</p>@endif
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <b style="color:black; ">{{$order->order_type}} Order</b>
                                                            </div>
                                                        </div>
                                                        <div class="item-details">
                                                              @foreach($order->detail as $order_detail)
                                                    <div class="item">{{$order_detail->quantity}} x  @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                 {{$order_detail->menu->name}} 
                                                @elseif(!empty($order_detail->addon)) 
                                                 {{$order_detail->addon->name}}  
                                                @endif</div>
                                                @endforeach
                                                        </div>
                                                    </a>
                                                    <div class="price"><span>Total</span>${{number_format($order->total,2)}}</div>
                                                    <div class="actions">
                                                        <div class="order-type-btn">
                                                            <span class="btn" onclick="order_status_modal('{{$order->id}}');"><span id="order_status_Label">{{$order->order_status}}</span></span>
                                                        </div> 
                                                        <a href="{{guard_url('cart/orders/print')}}/{{$order->getRouteKey()}}" target="_blank" class="btn btn-dark print-btn ion ion-android-print" data-toggle="tooltip" data-placement="top" title="Print"></a>
                                                    </div>
                                                </div>
                                                @empty
                                                @endif
