<div class="modal-header">
                <h5 class="modal-title" id="order_status_modalLabel">Select Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="order-type-wrap row gutter-10">
                    <!-- <div class="order-type-item">
                        <input type="radio" id="new_Order" name="order_Type" value="New Order">
                        <label for="new_Order">New Order</label>
                    </div> -->
                   
                  {{--  <div class="order-type-item preparing col-6">
                        <input type="radio" id="preparing_Food" name="order_Type" value="preparing" class="status_change" data-order="{{!empty($order)? $order->getRouteKey(): ''}}" >
                        <label for="preparing_Food">1. Preparing Food</label>
                    </div> --}}
                    @if($order->order_status != 'Completed')
                    <div class="order-type-item completed col-6">
                        <input type="radio" id="redy_for_Pickup" name="order_Type" value="accept" class="status_change" data-order="{{!empty($order)? $order->getRouteKey(): ''}}">
                        <label for="redy_for_Pickup">Accept</label>
                    </div>
                    @endif
                   {{-- <div class="order-type-item ready col-6">
                        <input type="radio" id="order_Complete" name="order_Type" value="{{(!empty($order) && $order->order_type == 'Delivery') ? 'Out for Delivery' : 'Ready for pickup'}}" class="status_change" data-order="{{!empty($order)? $order->getRouteKey(): ''}}">
                        <label for="order_Complete">2. {{(!empty($order) && $order->order_type == 'Delivery') ? 'Out for Delivery' : 'Ready for pickup'}}</label>
                    </div> --}}
                    @if($order->order_status != 'Cancelled')
                    <div class="order-type-item cancel col-6">
                        <input type="radio" id="cancel_Order" name="order_Type" value="cancelled" class="status_change" data-order="{{!empty($order)? $order->getRouteKey(): ''}}">
                        <label for="cancel_Order">Cancel Order</label>
                    </div>
                    @endif
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
<script type="text/javascript">
$('.status_change').click(function(){
    var selected_status = $(this).val();
    if(selected_status=='cancelled'){
   if (confirm('Are you sure you want to cancel this?')) {
    var order_id = $(this).attr('data-order');
    $.ajax({
        url: "{{ URL::to('restaurant/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
        success: function(response){
                document.location.reload()
        }
    });
} 
    }else{
        var order_id = $(this).attr('data-order');
    $.ajax({
        url: "{{ URL::to('restaurant/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
        success: function(response){
                document.location.reload()
        }
    });
    }
    
  
})
</script>    
<style type="text/css">
    .order-type-modal .order-type-item input:checked ~ label {
    /*background-color: #40b659;*/
    color: #fff;
}
</style>    