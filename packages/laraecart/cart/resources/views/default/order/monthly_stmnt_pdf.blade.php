<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
     <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <title>{{user()->name}} Monthly Statement {{$firstthisMonth}} – {{$lastthisMonth}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css" media="all">
        * {
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif;

        }
        html, body {
            font-size: 14px;
            margin: 0px;
            padding: 0px;
        }
        table {
            border-collapse: collapse;
            border-spacing: 1px;
            border: none;
        }
        table th, table td {
            border: none;
        }
        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0,0,0,.05);
        }
        @page{ margin: 0;}

        .page{
            page-break-after: always;
        }
    </style>
  </head>

    <body>
        <table border="0" cellpadding="0" cellspacing="0" style="background-color: #fff; height: 100%; width: 100%; -webkit-border-radius: 6px; -moz-border-radius: 6px; border-radius: 6px; -webkit-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); -moz-box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.75); box-shadow: 0px 0px 6px 0px rgba(0,0,0,0.10);">
            <tbody>
                <tr>
                    <td>
                       <div style="background-color: #fff; text-align: center; padding: 20px 30px;">
                           <h1 style="color: #000; margin-top: 10px; margin-bottom: 10px; font-size: 24px;">Monthly Statement</h1>
                       </div>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 20px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; padding-top: 30px;">
                            <tbody>
                                <tr>
                                    <td style="color: #333333; vertical-align: top;">
                                        <p style="margin: 6px 0px;"><b>Statement Date</b>: {{$nextMonth}}</p>
                                        <p style="margin: 6px 0px;"><b>Statement Period</b>: {{$firstthisMonth}} – {{$lastthisMonth}}  </p>
                                        <p style="margin: 6px 0px;"><b>Eatery Reference Number</b>: {{user_id()}} </p>
                                        <p style="margin: 6px 0px;"><b>Eatery Name</b>: {{user()->name}}</p>
                                        <p style="margin: 6px 0px;"><b>Eatery Address</b>: {{user()->address}}</p>
                                    </td>
                                    <td style="color: #333333; text-align: right; vertical-align: top;">
                                        <img src="https://zingmyorder.com/img/logo-round-big.png" style="height: 50px; display: inline-block;" alt="">
                                        <p style="margin: 6px 0px;"><b>ZingMyOrder LLC</b></p>
                                        <p style="margin: 6px 0px;">Interstate 35#304</p>
                                        <p style="margin: 6px 0px;">Carrollton, TX 75006</p>
                                        <p style="margin: 6px 0px;"><b>Email</b>: support@zingmyorder.com</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 0px 20px; padding-top: 0px;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; margin-bottom: 20px;">
                            <thead>
                                <tr>
                                    <th colspan="2" style="text-align: left;">Summary</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2">
                                        <div style="display: block; width: 100%; height: 1px; background-color: #ddd; margin-bottom: 20px;"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: middle; padding: 10px 0px;">
                                        <p style="color: #000; margin: 6px 0px;">Total Sales Collected: ${{number_format($totalSalesCollected,2)}}</p>
                                        <p style="color: #000; margin: 6px 0px;">Total of each ticket Subtotal: ${{number_format($totaltktsubtotald,2)}}</p>
                                        <p style="color: #000; margin: 6px 0px;">Total Tips: ${{number_format($totaltips,2)}}</p>
                                        <p style="color: #000; margin: 6px 0px;">Total Taxes: ${{number_format($totaltaxes,2)}}</p>
                                    </td>
                                    <td style="text-align: left; padding: 10px 0px; vertical-align: middle;">
                                        <p style="color: #000; margin: 6px 0px;">Total Zing Eatery Fees: ${{number_format($totalzingEateryFees,2)}}</p>
                                        <p style="color: #000; margin: 6px 0px;">Total Zing Customer Fees: ${{number_format($totalzingCustomerFees,2)}}</p>
                                        <p style="color: #000; margin: 6px 0px;">Credit Card Processing Charges: ${{number_format($totalcreditcard,2)}}</p>
                                        <p style="color: #000; margin: 6px 0px;">Total Eatery Amount Paid: ${{number_format($totalEateryAmount,2)}}</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div style="display: block; width: 100%; height: 1px; background-color: #ddd; margin-top: 20px;"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="font-size: 16px; color: #000; padding: 10px 0px; text-align: left;"><i>*Please note that the Eatery is responsible for paying the taxes for these transactions.</i></td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr class="page">
                    <td style="padding: 30px 20px; vertical-align: top;">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; border: none;" class="table-striped">
                            <thead style="background-color: #40b659;">
                                <tr>
                                    <th style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; display: table-cell;">Order Number</th>
                                    <th style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; display: table-cell;">Customer Name</th>
                                    <th style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; display: table-cell;">Purchase Date</th>
                                    <th style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; display: table-cell;">Order Subtotal</th>
                                    <th style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; display: table-cell;">Order Tax</th>
                                    <th style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; display: table-cell;">Zing Eatery Fees</th>
                                    <th style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; display: table-cell;">Zing Customer Fee</th>
                                    <th style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; display: table-cell;">Credit Card</th>
                                    <th style="color: #ffffff; padding: 10px 8px; text-align: center;">Eatery Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($orders as $key => $val)
                                <tr>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">{{$val->id}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">{{$val->name}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">{{$val->delivery_date}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_subtotal,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_tax,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_ZF,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_ZC,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_CCRF,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->EateryAmount,2)}}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="9" style="color: #000000; padding: 10px 8px; font-size: 16px; border-bottom: 1px solid #40b659;">No orders taken on this period.</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>

    </body></html>