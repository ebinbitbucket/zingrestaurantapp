<aside class="main-nav">
    <div class="nav-inner">
        <div class="links-wrap">
<!--             <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i>Dashboard</a>
 -->            <a href="{{guard_url('restaurant/kitchen_menu_items')}}"><i class="icon ion-android-restaurant"></i>Menu</a>
            <a class="active" href="{{guard_url('cart/orders/kitchen_orders/new')}}"><i class="icon ion-cube"></i>Orders</a>
        </div>
    </div>
    <div class="contact-wrap">
        <div class="logo">
            <a href="index.html">
                <img src="{{theme_asset('img/logo.png')}}" alt="">
            </a>
        </div>
        <div class="social-icons">
            <a href="#" class="fa fa-facebook-square"></a>
            <a href="#" class="fa fa-twitter"></a>
            <a href="#" class="fa fa-linkedin"></a>
            <a href="#" class=" fa fa-instagram"></a>
        </div>
    </div>
</aside>
<section class="dashboard-wrap">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-xl-12 d-none">
                             <aside class="dashboard-sidemenu">
                                <nav class="sidebar-nav">
                                    <ul>
                                        <li class="nav-head"><span class="head">Navigation</span></li>
                                      <!--   <li>
                                            <a href="{{guard_url('/')}}"><i class="icon ion-speedometer"></i><span>Dashboard</span></a>
                                        </li> -->
                                         <li>
                                            <a href="{{guard_url('restaurant/kitchen_menu_items')}}"><i class="icon ion-android-restaurant"></i><span>Menu</span></a>
                                        </li>
                                        <li class="active">
                                            <a href="{{guard_url('cart/orders/kitchen_orders')}}"><i class="icon ion-cube"></i><span>Orders</span></a>
                                        </li>
                                    </ul>
                                </nav>
                            </aside>
                        </div>
                        <div class="col-lg-12 col-xl-12">
                            <div class="element-wrapper order-wrapper">
                                @include('notifications')
                                <div class="element-box">
                                    <div class="element-header">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                          <li class="nav-item">
                                            <a href="{{guard_url('cart/orders/kitchen_orders/new')}}" class="nav-link {{ (Request::is('kitchen/cart/orders/kitchen_orders/new'))? 'active' : ''}}" >New</a>
                                          </li>
                                          <li class="nav-item">
                                            <a href="{{guard_url('cart/orders/kitchen_orders/preparing')}}" class="nav-link {{ (Request::is('kitchen/cart/orders/kitchen_orders/preparing'))? 'active' : ''}}">Preparing</a>
                                          </li>
                                          <li class="nav-item">
                                            <a href="{{guard_url('cart/orders/kitchen_orders/ready')}}" class="nav-link {{ (Request::is('kitchen/cart/orders/kitchen_orders/ready'))? 'active' : ''}}" >Ready</a>
                                          </li>
                                          <li class="nav-item">
                                            <a href="{{guard_url('cart/orders/kitchen_orders/delivery')}}" class="nav-link {{ (Request::is('kitchen/cart/orders/kitchen_orders/delivery'))? 'active' : ''}}">Out for delivery</a>
                                          </li>
                                          <li class="nav-item">
                                            <a href="{{guard_url('cart/orders/kitchen_orders/completed')}}" class="nav-link {{ (Request::is('kitchen/cart/orders/kitchen_orders/completed'))? 'active' : ''}}">Completed</a>
                                          </li>
                                          <li class="nav-item" > 
                                          <!--btn btn-dark print-btn-->
                                            <a href="#" class="nav-link" onclick="mute_alarm();" style="color:#fff;background-color: black;font-size: 25px;/*! border-radius: 10px; */padding-bottom: 8px;"><i class="fa fa-bell-slash"></i></a>
                                          </li>
                                        </ul>
                                    </div>
                                    <div class="element-body">
                                        <div class="tab-content" id="myTabContent">
                                            <div class="tab-pane orders-card-wrap fade show active status-{{$status}}" id="{{($status == 'new') ? 'new':'default'}}" role="tabpanel" aria-labelledby="new-tab">
                                                @forelse($orders as $order)
                                                <div class="card-item">
                                                    <a href="javascript:void(0);" data-toggle="modal" onclick="restaurant_detail_model('{{$order->getRouteKey()}}')">
                                                        <h3><span>#{{$order->id}}</span>{{@$order->user->name}} &nbsp;&nbsp;<div class="time"><b>{{$order->order_type}} Time:</b></div>
                                                        <p class="time ion-android-calendar ml-0">{{strtolower(substr(date('l',strtotime($order->delivery_time)),0,3))}}, {{date('M d',strtotime($order->delivery_time))}},  {{date('h:i A',strtotime($order->delivery_time))}}</p></h3>
                                                        @if(!empty(@$order->user->mobile))<p class="location"><i class="fa fa-phone"></i> {{substr(@$order->user->mobile, 0,3)}}-{{substr(@$order->user->mobile, 3,3)}}-{{substr(@$order->user->mobile, 6,4)}}</p>@endif
                                                        <div class="row">
                                                            <div class="col-md-4">
                                                                <b style="color:black; ">{{$order->order_type}} Order</b>
                                                            </div>
                                                        </div>
                                                        <div class="item-details">
                                                              @foreach($order->detail as $order_detail)
                                                    <div class="item">{{$order_detail->quantity}} x  @if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                 {{$order_detail->menu->name}} 
                                                @elseif(!empty($order_detail->addon)) 
                                                 {{$order_detail->addon->name}}  
                                                @endif</div>
                                                @endforeach
                                                        </div>
                                                    </a>
                                                    <div class="price"><span>Total</span>${{number_format($order->total,2)}}</div>
                                                    <div class="actions">
                                                        <div class="order-type-btn">
                                                            <span class="btn" onclick="order_status_modal('{{$order->id}}');"><span id="order_status_Label">{{$order->order_status}}</span></span>
                                                        </div> 
                                                        <a href="{{guard_url('cart/orders/print')}}/{{$order->getRouteKey()}}" class="btn btn-dark print-btn ion ion-android-print" data-toggle="tooltip" data-placement="top" title="Print"></a>
                                                    </div>
                                                </div>
                                                @empty
                                                @endif
                                            </div>
                                            
                                        </div>
                                    </div>
                                  
                                    <!-- <div class="orders-box-wrap">
                                        @foreach($orders as $order)
                                        <div class="box-item">
                                            <span><b>#</b>{{$order->id}}</span>
                                            <span><b>Customer Name</b>{{$order->name}}</span>
                                            <span><b>Location</b>{{$order->address}}</span>
                                            <span><b>Delivery Time</b>{{date('M d,Y h:m A',strtotime($order->delivery_time))}}</span>
                                            <span><b>Price</b>${{number_format($order->total,2)}}</span>
                                            <span class="dropdown">
                                                <b class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Status</b>
                                                <div class="dropdown-menu  dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                                    <h4>Change Status</h4>
                                                    <a class="dropdown-item" href="#"><span class="bg-success"></span>Paid</a>
                                                    <a class="dropdown-item" href="#"><span class="bg-primary"></span>New Order</a>
                                                    <a class="dropdown-item" href="#"><span class="bg-danger"></span>Cancelled</a>
                                                </div>
                                                <span class="label label-success">Paid</span>
                                            </span>
                                            <span class="actions"><b>Action</b><a href="#" class="btn btn-dark print-btn ion ion-android-print" data-toggle="tooltip" data-placement="top" title="Print"></a></span>
                                            <a href="{{guard_url('cart/restaurant/orders/show_detail')}}/{!! $order->getRouteKey() !!}" class="overlay-link"></a>
                                        </div>
                                        @endforeach
                                    </div> -->
                                    <!-- <div class="table-responsive element-table">
                                        <table id="example" class="display table table-responsive-xl">
                                            <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Customer Name</th>
                                                    <th scope="col">Location</th>
                                                    <th scope="col">Delivery time</th>
                                                    <th scope="col">Price</th>
                                                    <th scope="col">Status</th>
                                                    <th scope="col">Payment Status</th>
                                                    <th scope="col">Action</th>

                                                </tr>
                                            </thead>

                                            <tbody>
                                                
                                                @foreach($orders as $order)
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                    <td>{{$order->name}}</td>
                                                    <td>{{$order->address}}</td>
                                                    <td>{{date('M d,Y h:m A',strtotime($order->delivery_time))}}</td>
                                                    <td>${{number_format($order->total,2)}}</td>
                                                    <td><span class="badge badge-xs badge-primary">{{$order->order_status}}</span></td>
                                                    <td>{{$order->payment_status}}</td>
                                                    <td><span><a href="{{guard_url('cart/restaurant/orders/show_detail')}}/{!! $order->getRouteKey() !!}"><i class="flaticon-view"></i> </a></span>
                                                </td>
                                                </tr>
                                                @endforeach
                                            </tbody>

                                        </table>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

<div class="modal fade order-type-modal" id="order_status_modal" tabindex="-1" role="dialog" aria-labelledby="order_status_modalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">
            
        </div>
    </div>
</div>

<div class="modal oredr-detail-modal fade bd-example-modal-lg" id="restaurant_order_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content" id="order_modal">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Order #296</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true"><i class="ion ion-ios-close-outline"></i></button>
            </div>
            <div class="modal-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>
<script>
                             var myAudio = new Audio("{{trans_url('themes/kitchen/assets/service-bell_daniel_simion.wav')}}");

  function mute_alarm(){
      
      myAudio.pause(); 
    //   x.loop = false;
    //     x.load();  
        console.log('hiqqq');

  }
$(document).ready(function () {
    $('.actions .dropdown-menu').click(function(e) {
        e.stopPropagation();
    });
    var count=0;
    var id = '{{$id}}';
    setInterval(function(){
        getNewOrders();
        getScheduledUpdate();
    }, 7000);
    function getNewOrders() {  
        $.ajax({
            url: "{{ URL::to('kitchen/cart/kitchen/getNeworders') }}",
            dataType:'JSON',
            success: function(response){
                    $('#new').html(response.new_orders);
                    //$("#new_order_modal").modal();
                    console.log('hi',response.new_order_count);
                    //if(count<response.new_order_count){                
                    if(id<response.id){                

                        toastr.options.timeOut = 0;
                        toastr.options.extendedTimeOut = 0;
                        toastr.options.closeButton = true;
                        toastr.options.onclick = function() {
                             window.location="{{guard_url('cart/orders/kitchen_orders/new')}}"; 
                        }

                        toastr.success('You have New Order');
                        count = response.new_order_count;
                        console.log('hiau',response.audio );
                        if(response.audio == 'on'){
                                                console.log('hiaud');

                              myAudio.loop = true;   
                              myAudio.play();

                        }
                    }
                    else{
                        if(response.total_new_order_count > 0){
                            toastr.clear();
                            toastr.options.timeOut = 0;
                            toastr.options.extendedTimeOut = 0;
                            toastr.options.closeButton = true;
                            toastr.options.onclick = function() {
                                 window.location="{{guard_url('cart/orders/kitchen_orders/new')}}"; 
                            }

                            toastr.success('You have New Order');
                        }
                    }
                  id = response.id;
            }
        });
    }
    
    
   function getScheduledUpdate() {  
        $.ajax({
            url: "{{ URL::to('kitchen/cart/kitchen/getKitchenScheduledupdate') }}",
            dataType:'JSON',
            success: function(response){
                    
                    return;
                    
                }
                
           
        });
    }
       
});
function restaurant_detail_model(order_id) {
    $('.oredr-detail-modal .modal-content').load("{{url('kitchen/cart/kitchen/orders/show_detail')}}"+'/'+order_id,function(){ 
                $('#restaurant_order_modal').modal({show:true}); 
            });
}

function order_status_modal(order_id) {
    $('.order-type-modal .modal-content').load("{{url('kitchen/cart/kitchen/orders/status_model')}}"+'/'+order_id,function(){ 
        $('#order_status_modal').modal({show:true}); 
    });
}
// function status_change(order_id) {
//     var selected_status = $("#status_change option:selected").val();
//     $.ajax({
//             url: "{{ URL::to('kitchen/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
//             success: function(response){
//                     document.location.reload()
//             }
//         });
    
// }

// $('.status_change').click(function(){
//     var selected_status = $(this).val();
//     var order_id = $(this).attr('data-order');
//     $.ajax({
//             url: "{{ URL::to('restaurant/cart/order/status_update')}}"+'/'+selected_status+'/'+order_id,
//             success: function(response){
//                     document.location.reload()
//             }
//         });
// })
</script>