<section class="dashboard-wrap" style="background-color: #ffffff;">
    <div class="element-wrapper order-detail-wrap">
       <div class="element-box">
         <div class="modal-header">

                <!--<div class="element-info-icon"><div class="icon ion-cube"></div></div>-->
                <div class="element-info-text">
                   <h5 class="element-inner-header">#{{$order->id}} </h5>
                   <div class="element-inner-desc"></div>
                </div> 
                &nbsp;&nbsp;&nbsp;&nbsp;
                <div class="element-info-text">
                   <h5 class="element-inner-header"><b>Car & Parking Information</b></h5>
                   <div class="element-inner-desc"></div>
                </div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <i class="ion-ios-close-outline"></i>
                </button>
         
         </div>
         <div class="modal-body">
          <div class="col-md-6">
             <div class="order-detail-item">
                <span>Car Make and Model</span>
                <p>{{@$order->car_details['number']}}</p>
             </div>
          </div>
          <div class="col-md-6">
             <div class="order-detail-item">
                <span>Car Color</span>
                <p>{{@$order->car_details['color']}}</p>
             </div>
          </div>
          <div class="col-md-12">
             <div class="order-detail-item">
                <span>Parking and Other Information</span>
                <p>{{@$order->car_details['info']}}</p>
             </div>
          </div>
         </div>
         <div class="modal-footer">
            <button type="button"  @if($order->is_car_pickup==1) disabled="true" @else  id="btn_complete" @endif class="btn btn-theme btn_add_cart">Complete <span></span></button>

         </div>
       </div>
    </div>
 </section>
<script>
$(document).ready(function () {
    $('#btn_complete').click(function(e) {
     // $('#car_modal').modal('hide'); // Hide modal
        e.stopPropagation();
       $.ajax({
            url: "{{guard_url('cart/orders/car-pickup/complete')}}/{{$order->getRoutekey()}}",
            dataType:'JSON',
            success: function(response){
               $(".close").trigger('click');
               document.location.reload()

                }
                
           
        });
       
    });
   });
 </script>
 <style>
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item {
    position: relative;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .line {
    position: absolute;
    top: 15px;
    left: 0px;
    width: 100%;
    height: 1px;
    background-color: #ccc;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-name {
    background-color: #fff;
    position: relative;
    padding-right: 10px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-price {
    position: relative;
    background-color: #fff;
    padding-left: 10px;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-total-wrap {
    justify-content: flex-end;
    }
    @media (max-width: 500px) {
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item {
    display: block;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .line {
    display: none;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-price {
    padding-left: 0px;
    margin-top: 5px;
    }
    .oredr-detail-modal .modal-content {
    width: 100% !important;
    }
    }
 </style>