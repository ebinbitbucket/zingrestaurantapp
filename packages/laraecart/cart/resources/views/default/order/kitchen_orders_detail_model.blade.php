<section class="dashboard-wrap" style="background-color: #ffffff;">
                            <div class="element-wrapper order-detail-wrap">
                                <div class="element-box">
                                    <div class="element-info">
                                        <div class="element-info-with-icon">
                                            <!--<div class="element-info-icon"><div class="icon ion-cube"></div></div>-->
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header">#{{$order->id}} </h5>
                                                <div class="element-inner-desc"></div>
                                            </div>
                                            <div class="element-info-text">
                                                <h5 class="element-inner-header"><b>{{strtoupper($order->order_type)}} ORDER</b></h5>
                                                <div class="element-inner-desc"></div>
                                            </div>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <i class="ion-ios-close-outline"></i>
                                            </button>
                                        </div>
                                    </div>
                                    
                                    <?php 
                                    $rest_data = Restaurant::getRestaurantData($order->restaurant_id)
                                   ?>
                                   <div class="tip-block order-details-wrap">
                                    <div class="row">
                                        <div class="col-md-6">
                              <!--<b>Expand Time to</b></div>-->
                              <select class="tipcal form-control col-md-12" id="timeextend">
                                  <option value="0">Select Delay Time</option>
                                  <!--<option value="1">1 Min</option>-->
                                  <!--<option value="3">3 Min</option>-->
                                  <option value="5">5 Min</option>
                                  <option value="8">8 Min</option>
                                  <option value="10">10 Min</option>
                                  <option value="15">15 Min</option>
                                  <option value="20">20 Min</option>
                                  <option value="30">30 Min</option>
                                  <option value="35">35 Min</option>
                                  <option value="40">40 Min</option>
                                  <option value="45">45 Min</option>
                                  <option value="50">50 Min</option>
                                  <option value="55">55 Min</option>
                                  <option value="60">1 Hr</option>
                                  <option value="90">1 Hr 30 Min</option>
                                  <option value="120">2 Hr</option>
                              </select>
                          </div>
                              <input type="hidden" id="tip" name="tip">
                               <!--  <div class="tip-item" style="width: 50px;"> 
                                    <input type="radio" name="timeextend" id="tip_1" value=1 class="tipcal">
                                    <label for="tip_1">1M</label>
                                </div>
                                <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_3" value=3 class="tipcal">
                                    <label for="tip_3">3M</label>
                                </div>
                                <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_5" value=5 class="tipcal">
                                    <label for="tip_5">5M</label>
                                </div>
                               <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_8" value=8 class="tipcal">
                                    <label for="tip_8">8M</label>
                                </div>
                                  <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_10" value=10 class="tipcal">
                                    <label for="tip_10">10M</label>
                                </div>
                                  <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_15" value=15 class="tipcal">
                                    <label for="tip_15">15M</label>
                                </div>
                                  <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_20" value=20 class="tipcal">
                                    <label for="tip_20">20M</label>
                                </div>
                                  <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_30" value=30 class="tipcal">
                                    <label for="tip_30">30M</label>
                                </div>
                                  <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_1h" value=60 class="tipcal">
                                    <label for="tip_1h">1H</label>
                                </div>
                                  <div class="tip-item" style="width: 80px;">
                                    <input type="radio" name="timeextend" id="tip_1h30m" value=90 class="tipcal">
                                    <label for="tip_1h30m">1H 30M</label>
                                </div>
                                  <div class="tip-item" style="width: 50px;">
                                    <input type="radio" name="timeextend" id="tip_2h" value=120 class="tipcal">
                                    <label for="tip_2h">2H</label>
                                </div> -->
 <div class="col-md-6">
                                                <div class="order-detail-item">
                                                    <span>Customer Name</span><p>{{$order->name}}</p>
                                                </div>
                                            </div>
                            </div>
                                    <div class="order-details-wrap">
                                        <div class="row">
                                            <!--<div class="col-md-4">-->
                                            <!--    <div class="order-detail-item">-->
                                            <!--        <p><span>Order Id</span>#{{$order->id}}</p>-->
                                            <!--    </div>-->
                                            <!--</div>-->
                                        
                                           
                                            <div class="col-md-6">
                                                <div class="order-detail-item">
                                                    <p><span>Phone</span>{{$order->user->mobile}}</p>
                                                </div>
                                            </div>
                                            <!--<div class="col-md-4">-->
                                            <!--    <div class="order-detail-item">-->
                                            <!--        <p><span>Payment Status</span>{{$order->payment_status}}</p>-->
                                            <!--    </div>-->
                                            <!--</div> --> 
                                            <div class="col-md-6">
                                                <div class="order-detail-item">
                                                    <p><span>{{!empty($order->address_id)? 'Delivery' : 'Pickup' }} Time</span>{{date('M d,Y h:i A',strtotime($order->ready_time))}}</p>
                                                </div>
                                            </div>
                                            @if(!empty($order->address_id))
                                            <div class="col-md-8">
                                                <div class="order-detail-item">
                                                    <p><span>{{!empty($order->address_id)? 'Delivery' : 'Pickup' }} Location</span>{{!empty($order->address_id)? @$order->delivery_address->full_address : $rest_data->address}}</p>
                                                </div>
                                            </div>
                                              @endif
                                            
                                         
                                            
                                        </div>
                                    </div>
                                    <hr class="mb-0">
                                    <div class="row">
                                        <!-- <div class="col-md-6 border-right pt-30">
                                            <div class="menu-restaurant-wrap">
                                                <div class="res-img">
                                                    <img src="img/restaurants/thumb/restaurant-01.jpg" class="img-fluid" alt="">
                                                </div>
                                                <div class="res-content">
                                                    <p>Order From <span>Jet's Kitchen</span></p>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="col-md-12 pt-30">
                                            <div class="order-contains-wrap pl-0">
                                                <h4>Order Summary</h4>
                                                @foreach($order->detail as $order_detail)
                                               <?php
                                                    $variation = $order_detail->variation; 
                                                     $addon_price = 0;

                                                     if (!empty($order_detail->addons)) {

                                                         foreach ($order_detail->addons as $cart_addon) {

                                                             $addon_price = $addon_price + $cart_addon[2];
                                                         }

                                                     }

                                                 ?>
                                                <div class="item" style="{{$order_detail->menu_addon == 'addon' ? 'margin-left: 30px' : ''}}">
                                                    <span class="line"></span>
                                                    <div class="item-name"> @if(!empty($order_detail->menu))<div class="cell-img" style="background-image: url({{url($order_detail->menu->defaultImage('image'))}})"></div>
                                                    @endif
                                                    <b>@if($order_detail->menu_addon == 'menu' && (!empty($order_detail->menu)))
                                                 {{$order_detail->menu->name}} 
                                                @elseif(!empty($order_detail->addon))
                                                 {{$order_detail->addon->name}}  
                                                @endif
                                                <span>x {{$order_detail->quantity}}  </span></b></div>
                                                    <div class="item-price"> ${{number_format($order_detail->quantity * $order_detail->unit_price+$addon_price,2)}}</div>
                                                </div>
                                                 @if(!empty($variation))<div class="item" style="margin-left: 20px">  <b>Variation - {{$variation}} </b></div>@endif
                                                @if(!empty($order_detail->addons))
                                        @foreach($order_detail->addons as $cart_addon) 
                                          <div class="item" style="margin-left: 20px">  {{$cart_addon[1]}} </div>
                                        @endforeach
                                        @endif
                                                 @if(!empty($order_detail->special_instr))
                                                <div class="item" style="padding-left: 30px;"> 
                                        <li>{{$order_detail->special_instr}}</li>
                                    </div>
                                    @endif
                                               @endforeach

                                               @if(@$order->non_contact==1)
                                    <div> 
                                        <span style="color:red;">No Contact Delivery</span>
                                    </div>
                                    @endif
                                            </div>
                                               @if(@$order->tax>0)
                                              <div class="order-total-wrap pl-0">
                                                <h4>Tax & Fees : </h4> 
                                                <h4 style="width: 100px; text-align: right;">${{number_format(@$order->tax,2)}} </h4>
                                              </div>
                                            @endif
                                            @if(@$order->delivery_charge>0)
                                              <div class="order-total-wrap pl-0">
                                                <h4>Delivery :  </h4> 
                                                <h4 style="width: 100px; text-align: right;">${{number_format(@$order->delivery_charge,2)}} </h4> 
                                              </div>
                                            @endif
                                            @if(@$order->tip>0)
                                              <div class="order-total-wrap pl-0">
                                                <h4>Tip :  </h4> 
                                                <h4 style="width: 100px; text-align: right;">${{number_format(@$order->tip,2)}} </h4> 
                                              </div>
                                            @endif
                                            @if(@$order->discount_points>0)
                                              <div class="order-total-wrap pl-0">
                                                <h4>Loyalty Rewards :  </h4> 
                                                <h4 style="width: 100px; text-align: right;">{{number_format(@$order->discount_points/2000,2)}} ({{@$order->loyalty_points}} Points) </h4> 
                                              </div>
                                            @endif
                                            <div class="order-total-wrap pl-0">
                                                <h4>Order Total</h4>
                                                <h4 style="width: 100px; text-align: right;">${{number_format($order->total,2)}}</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            </section>
<script type="text/javascript">
    $('.tipcal').on('change', function() { 
        $.getJSON({
                  type: 'GET',
                  url : "{{url('kitchen/cart/orders/timeextend')}}/{{$order->getRoutekey()}}"+'/'+$(this).val(),
                  success: function(data) { 
                   console.log('hi');
                   location.reload();
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
    })
</script>
<style>
.main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item {
    position: relative;
}
.main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .line {
    position: absolute;
    top: 15px;
    left: 0px;
    width: 100%;
    height: 1px;
    background-color: #ccc;
}
.main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-name {
    background-color: #fff;
    position: relative;
    padding-right: 10px;
}
.main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-price {
    position: relative;
    background-color: #fff;
    padding-left: 10px;
}
.main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-total-wrap {
    justify-content: flex-end;
}
@media (max-width: 500px) {
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item {
        display: block;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .line {
        display: none;
    }
    .main-wrap.auth-wrap .dashboard-wrap .order-detail-wrap .order-contains-wrap .item .item-price {
        padding-left: 0px;
        margin-top: 5px;
    }
    .oredr-detail-modal .modal-content {
        width: 100% !important;
    }
}
</style>