
application/x-httpd-php success.blade.php ( HTML document, ASCII text, with very long lines )
@include('notifications')

<div class="process-stage-wrapper">
     @if (Session::has('successcheckout'))
        <div class="alert alert-success col-md-8 ml-auto mr-auto">
            <button type="button" aria-hidden="true" class="close"  data-dismiss="alert" aria-label="close">
                <i class="icon-close"></i>
            </button>
            <span>
               {{ session('successcheckout') }}</span>
        </div>
        @endif
</div>
            <section class="order-wrap listing-wrap">
                <div class="container">
                    <div class="listing-wrap-inner">
                        <div class="row">
                            <div class="col-md-8 ml-auto mr-auto" style="padding-top: 20px; padding-bottom: 20px;">
                                <div class="card" >
                                    <div class="card-header">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="row">
                                                 <a href="#" class="m-0" onClick="history.go(-1)" style="color: #29b050;font-size: 18px;font-weight: 700;padding-right: 10px;"  data-toggle="tooltip" data-placement="top" title="Back to Orders" data-original-title="Go Back"><i class="fa fa-arrow-left"></i></a>
                                                 <h5 class="m-0">Order Details</h5>&nbsp;&nbsp;&nbsp;
                                               
                                            </div>
                                           
                                            </div> 
                                            <div class="col-md-4">
                                                    <a href="{{guard_url('/')}}" class="btn btn-theme  btn-block">My Order History&nbsp;&nbsp; <i class="fa fa-arrow-right"></i></a>
                                              
                                                    <!-- <a href="{{guard_url('cart/reorder/'.$orders->getRoutekey())}}" class="btn btn-secondary btn-block">Reorder</a> -->
                                                   
                                            </div>
                                            <div class="col-md-4 text-right">
                                                        <h5>Order ID : {{$orders->id}}</h5>
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-4 border-right">
                                                <h4 class="title"><strong><img src="{{url($orders->restaurant->defaultImage('logo'))}}" style="width: 50px;height: 50px;">&nbsp; {{@$orders->restaurant->name}}</strong><button onclick="add_fav()" class="add-fav-btn"><i class="fa fa-heart"></i></button></h4>
                                                 </br>
                                                 <h4><strong>${{number_format($orders->total,2)}}</strong></h4>
                                                 <h4><b>{{!empty($orders->address_id)? 'Delivery' : 'Pickup' }} Date</b></h4>
                                                <p>{{date('l M d',strtotime($orders->delivery_time))}}th</p>
                                                 <h4><b>{{!empty($orders->address_id)? 'Delivery' : 'Pickup' }} Time</b></h4>
                                                <p>{{date('h:i A',strtotime($orders->delivery_time))}}</p>
                                               
                                </br>

                                                @if($orders->payment_status != 'Paid')
                                                <a href="{{guard_url('cart/order/payment/'.$orders->getRoutekey())}}" class="btn btn-danger btn-block">Payment</a>
                                                @endif

                                            </div>
                                            <div class="col-md-8">
                                                <div id="locationMap" class="mb-15" style="width: 100%; height: 250px;"></div>
                                                <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15716.750801923079!2d76.29128701977541!3d10.00134909999999!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1551967627669" class="mb-30" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe> -->
                                              <!--   <h5><b>Picking Up From</b></h5>
                                                <p>{{$orders->restaurant->name}} <br>{{$orders->restaurant->address}}</p>
                                                <br>
                                                <p class="text-success">7:20PM PST Estimated Pick Up</p>
                                                <br> -->
                                                <?php 
                                    $rest_data = Restaurant::getRestaurantData($orders->restaurant_id)
                                   ?>
                                                <input type="hidden" id="order_latitude" value="{{!empty($order->address_id)? @$orders->delivery_address->latitude : $rest_data->latitude}}">
                                                <input type="hidden" id="order_longitude" value="{{!empty($order->address_id)? @$orders->delivery_address->longitude : $rest_data->longitude}}">
                                                <h5><b>{{!empty($orders->address_id)? 'Delivering To' : 'Pickup From' }}</b></h5>
                                                <p>{{!empty($orders->address_id)? @$orders->delivery_address->address : $rest_data->address}}</p>
                                                <p>{{$orders->delivery_instr}}</p>
                                               <!--  <br>
                                                <p class="text-danger">Delivery at {{date('h:m A',strtotime($orders->delivery_time))}} </p> -->
                                                 <h5><b>Items</b></h5>

                                                          @foreach($orders->detail as $order_det)
                                                          <?php
                                                                $variation = $order_det->variation; 
                                                              $addon_price = 0;

                                                              if (!empty($order_det->addons)) {

                                                                  foreach ($order_det->addons as $cart_addon) {

                                                                      $addon_price = $addon_price + $cart_addon[2];
                                                                  }

                                                              }

                                                          ?>
                                    <div class="item" style="{{$order_det->menu_addon == 'addon' ? 'margin-left: 20px' : ''}}"> 
                                    <b>
                                        @if($order_det->menu_addon == 'menu' && (!empty($order_det->menu)))
                                            {{@$order_det->menu->name}} 
                                        @elseif(!empty($order_det->addon))
                                            {{@$order_det->addon->name}}  
                                        @endif
                                        x {{$order_det->quantity}} - ${{number_format($order_det->quantity * $order_det->unit_price+$addon_price,2)}} </b>
                                    </div>
                                    @if(!empty($variation))<div class="item" style="margin-left: 20px">  <b>Variation - {{$variation}} </b></div>@endif
                                     @if(!empty($order_det->addons))
                                        @foreach($order_det->addons as $cart_addon)
                                          <div class="item" style="margin-left: 20px">  {{$cart_addon[1]}} </div>
                                        @endforeach
                                        @endif
                                     @if(!empty($order_det->special_instr))
                                    <div class="item" style="padding-left: 30px;"> 
                                        <li>{{$order_det->special_instr}}</li>
                                    </div>
                                    @endif
                                    @endforeach
                                    </br>
                                    <b>
                                    <div class="item">SubTotal :  {{number_format(@$orders->subtotal,2)}} </div>
                                    @if(@$orders->tax>0)<div class="item">Tax & Fees :  {{number_format(@$orders->tax,2)}} </div>@endif
                                    @if(@$orders->delivery_charge>0)<div class="item">Delivery :  {{number_format(@$orders->delivery_charge,2)}} </div>@endif
                                    @if(@$orders->tip>0)<div class="item">Tip :  {{number_format(@$orders->tip,2)}} </div>@endif
                                    @if(@$orders->discount_amount>0)<div class="item">Discount :  {{number_format(@$orders->discount_amount,2)}} </div>@endif
                                    @if(@$orders->discount_points>0)<div class="item">Loyalty Rewards :  {{number_format(@$orders->discount_points/2000,2)}} ({{@$orders->loyalty_points}} Points) </div>@endif
                                    </b>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </section>
    <script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>     
<script type="text/javascript">
     $(document).ready(function() {
        var active = '<?php echo $restaurant_fav; ?>';
        if(active != 0){
            $(".add-fav-btn").addClass('active'); 
        }
    });
    function add_fav(){
        var fav_status = $('.add-fav-btn').hasClass('active');
        if(fav_status == false)
            fav_status = 'on';
        else
            fav_status = 'off';

            $.getJSON({
                  type: 'GET',
                  url : "{{url('client/cart/order/addtofavourite')}}/{{$orders->id}}"+'/'+fav_status,
                  success: function(data) { 
                    if(fav_status == on)
             $('.add-fav-btn').addClass('active');
        else
             $('.add-fav-btn').removeClass('active');
                   
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
     }

             
            var map;
            var marker;
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();
            
            function initialize(){ 
              if(($('#order_latitude').val()!='') && ($('#order_longitude').val()!='')){
                      var myLatlng = new google.maps.LatLng($('#order_latitude').val(),$('#order_longitude').val());
     }
     else{
        var myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
     }
              
      
                var mapOptions = {
                    zoom: 18,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
               
                map = new google.maps.Map(document.getElementById("locationMap"), mapOptions);
                
                marker = new google.maps.Marker({
                    map: map,
                    position: myLatlng,
                    draggable: true 
                });     
                
              
            }
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>  