<style type="text/css" media="all">
       /* * {
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif;

        }
        html, body {
            font-size: 11px;
            margin: 0px;
            padding: 0px;
        }*/
        table {
            border-collapse: collapse;
            border-spacing: 1px;
            border: none;
             font-size: 11px;
        }
        table th, table td {
            border: none;
        }
        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0,0,0,.05);
        }
    </style>       
       <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0; padding-top: 10px;">
            <thead style="background-color: #40b659;">
                                <tr>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Order Number</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Customer Name</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Purchase Date</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Order Subtotal</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Order Tax</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Order Tips</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Delivery Charge</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Zing Customer Fee</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Min. Order Surcharge</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Customer Total</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Zing Eatery Fees</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; border-right: 1px solid #ffffff; text-align: center; line-height: 1;">Credit Fees</th>
                                    <th height="50" style="color: #ffffff; padding: 10px 8px; text-align: center; line-height: 1;">Eatery Amount Due</th>
                                    <!--<th height="50" style="color: #ffffff; padding: 10px 8px; text-align: center; line-height: 1;">Order Status</th>-->
                                </tr>
                            </thead>
                            <tbody>
                             @forelse($orders as $key => $val)
                                <tr>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">{{$val->id}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">{{$val->name}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">{{$val->delivery_date}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_subtotal,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_tax,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_tip,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->delivery_charge,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_ZC,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->extra_payment,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_total,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_ZF,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->order_CCRF,2)}}</td>
                                    <td style="color: #000000; padding: 10px 8px; text-align: center;">${{number_format($val->EateryAmount,2)}}</td>
                                    <!--<td style="color: #000000; padding: 10px 8px; text-align: center;"><span class="badge badge-pill @if($val->order_status != 'Cancelled') badge-success @else badge-danger  @endif">{{$val->order_status}}</span></td>-->
                                </tr>
                                @empty
                                @if(empty($orders))
                                <tr>
                                    <td colspan="9" style="color: #000000; padding: 50px 8px; font-size: 16px; border-bottom: 1px ;">No orders taken on this period.</td>
                                </tr>
                                @endif
                                @endif
                            </tbody>
                        </table>                        
                        
                          