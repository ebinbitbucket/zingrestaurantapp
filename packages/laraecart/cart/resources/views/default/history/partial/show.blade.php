            <div class="content">
                <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('cart::history.label.id') !!}
                </label><br />
                    {!! $history['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_id">
                    {!! trans('cart::history.label.order_id') !!}
                </label><br />
                    {!! $history['order_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_placed">
                    {!! trans('cart::history.label.order_placed') !!}
                </label><br />
                    {!! $history['order_placed'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_dispatched">
                    {!! trans('cart::history.label.order_dispatched') !!}
                </label><br />
                    {!! $history['order_dispatched'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="Courier_name">
                    {!! trans('cart::history.label.Courier_name') !!}
                </label><br />
                    {!! $history['Courier_name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_status">
                    {!! trans('cart::history.label.order_status') !!}
                </label><br />
                    {!! $history['order_status'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('cart::history.label.created_at') !!}
                </label><br />
                    {!! $history['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('cart::history.label.updated_at') !!}
                </label><br />
                    {!! $history['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('cart::history.label.deleted_at') !!}
                </label><br />
                    {!! $history['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('cart::history.label.order_id'))
                       -> placeholder(trans('cart::history.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_placed')
                       -> label(trans('cart::history.label.order_placed'))
                       -> placeholder(trans('cart::history.placeholder.order_placed'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='order_dispatched' class='control-label'>{!!trans('cart::history.label.order_dispatched')!!}</label>
                        <div class='input-group picktime'>
                            {!! Form::text('order_dispatched')
                            -> placeholder(trans('cart::history.placeholder.order_dispatched'))
                            -> raw()!!}
                            <span class='input-group-addon'><i class='fa fa-clock-o'></i></span>
                        </div>
                    </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('Courier_name')
                       -> label(trans('cart::history.label.Courier_name'))
                       -> placeholder(trans('cart::history.placeholder.Courier_name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('order_status')
                       -> label(trans('cart::history.label.order_status'))
                       -> placeholder(trans('cart::history.placeholder.order_status'))!!}
                </div>
            </div>