            <table class="table" id="main-table" data-url="{!!guard_url('cart/history?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="order_id">{!! trans('cart::history.label.order_id')!!}</th>
                    <th data-field="order_placed">{!! trans('cart::history.label.order_placed')!!}</th>
                    <th data-field="order_dispatched">{!! trans('cart::history.label.order_dispatched')!!}</th>
                    <th data-field="Courier_name">{!! trans('cart::history.label.Courier_name')!!}</th>
                    <th data-field="order_status">{!! trans('cart::history.label.order_status')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">app.actions</th>
                    </tr>
                </thead>
            </table>