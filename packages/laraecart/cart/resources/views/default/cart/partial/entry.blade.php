            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('instance')
                       -> label(trans('cart::cart.label.instance'))
                       -> placeholder(trans('cart::cart.placeholder.instance'))!!}
                </div>

                <div class='col-md-12'>
                    {!! Form::textarea('content')
                    -> label(trans('cart::cart.label.content'))
                    -> dataUpload(trans_url($cart->getUploadURL('content')))
                    -> addClass('html-editor')
                    -> placeholder(trans('cart::cart.placeholder.content'))!!}
                </div>
            </div>