            <table class="table" id="main-table" data-url="{!!guard_url('cart/cart?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="id">{!! trans('cart::cart.label.id')!!}</th>
                    <th data-field="instance">{!! trans('cart::cart.label.instance')!!}</th>
                    <th data-field="content">{!! trans('cart::cart.label.content')!!}</th>
                    <th data-field="created_at">{!! trans('cart::cart.label.created_at')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">app.actions</th>
                    </tr>
                </thead>
            </table>