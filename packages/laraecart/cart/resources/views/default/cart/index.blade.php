@extends('resource.index')
@php
$links['create'] = guard_url('cart/cart/create');
$links['search'] = guard_url('cart/cart');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('cart::cart.title.main') !!}
@stop

@section('sub.title') 
{!! __('cart::cart.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('cart/cart')!!}">{!! __('cart::cart.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('cart::cart.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$carts->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
