
<div class="list-view">
        
       
         <div class="card list-view-media">
<div style="margin-left: 20px; margin-right: 20px;"> 
            <h3>Payment</h3> 
<div class="row">
            <div class='col-md-8 col-sm-6'>
                <p>Order id: {{$carts['id']}}</p> 
            </div>
            <div class='col-md-4 col-sm-6'>
                <p>Date: {{format_date($carts['created_at'])}}</p>
            </div>
        </div>
        <div class="row">
            <div class='col-md-8 col-sm-6'>
                <p>Payment Status: {{$carts['payment_status']}}</p> 
            </div>
            <div class='col-md-4 col-sm-6'>
                <p>Amount: {{$carts['total']}}</p>
            </div>
        </div>
        <div class="table-responsive cart_info">
             <form action="{{guard_url('cart/repayment')}}">
 
                    <table class="table table-bordered">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Pay Online</td>
                        <td class="description">Bank Transfer</td>
                        <td class="price">Cash on Delivery</td>
                        
                    </tr>
                </thead>
                <tbody>
                   
                    

                    <tr>
                        <td class="cart_product">
                           <p><input type="radio" name="online" value="paypal">Paypal </p>
                           <p><input type="radio" name="online" value="google_wallot">Google Wallot </p>
                           <p><input type="radio" name="online" value="payoneer">Payoneer</p>
                           <p><input type="radio" name="online" value="2checkout">2Checkout</p>
                        </td>
                        <td class="cart_description">
                            <p><input type="radio" name="online" value="account1">SBI Kaloor </p>
                            <p><input type="radio" name="online" value="account2">Federal Bank Aluva </p>
                            
                        </td>
                        <td class="cart_price">
                            <p><input type="radio" name="online" value="cash_on_delivery">Cash on delivery</p>
                        </td>        
                    </tr>      
                        </tbody>
                    </table>
 <input type="hidden" name="order_id" value="{{$order_id}}">

      <button type="submit"  id="button-cart" name="payment_status" value="Paid" class="btn btn-primary" style="color: #007bff" align="right">Proceed Payment</button>
       <button type="submit"  value="Unpaid" name="payment_status" id="button-cart"  class="btn btn-primary" style="color: #007bff" align="right">Cancel Payment</button>
   </form>
   <br/>
            <table class="table table-bordered">
                <thead>
                    <tr class="cart_menu">
                        <td class="description">Item</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        
                    </tr>
                </thead>
                <tbody>
                   
                    @foreach($carts->detail as $item)

                    <tr>
                        
                        <td class="cart_description">
                            <h4><a href="">{{$item->product->name}}</a></h4>
                            
                        </td>
                        <td class="cart_price">
                            <p>{{$item->unit_price}}</p>
                        </td>
                        <td class="cart_quantity">
                            <p>{{$item->quantity}}
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">{{$item->price}}</p>
                                </td>
                                
                            </tr>

                            @endforeach
                            <div align="right">
                                <table class="table table-bordered" align="right" style="width:400px">
                                    <tr>
                                        <td class="text-right"><strong>Sub-Total</strong></td>
                                        <td class="text-right">₹{{$carts['subtotal']}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><strong>Tax </strong></td>
                                        <td class="text-right">₹{{$carts['tax']}}</td>
                                    </tr>

                                    <tr>
                                        <td class="text-right"><strong>Total</strong></td>
                                        <td class="text-right">₹ {{$carts['total']}}</td>
                                    </tr>
                                </table>
                            </div>
                            
                        </tbody>
                    </table>
               
                </div>
           
</div></div></div>