<div class="col-md-10 col-md-offset-1">
  
       
            <div class="form mb30">
                <div class="title">
                    <h3> 
                        Payment
                    </h3>
                    <div class="separator">
                        <span>
                        </span>
                        <span>
                        </span>
                        <span>
                        </span>
                    </div>
                </div>
                <div class="mb30" id="success">
                </div>
                 @foreach($carts as $item)
                <div class="col-md-7 col-md-offset-1">
                <div class="well">
                   
                    <h3>{{$item->name}}</h3>
                    <p>Quantity: {{$item->qty}}</p>
                    <h5>Amount: ₹ {{$item->price}}</h5>
                    <h5>Tax Rate: % {{$item->taxRate}}</h5>
                    
                    
                </div></div>
@endforeach

<h5>Total Amount:</h5> <h4>₹ {{$amount}}</h4>
                <a href="{{guard_url('cart/payment')}}/{{$address->getRouteKey()}}"><button type="button"  id="button-cart"  class="btn btn-primary" style="color: #007bff" align="right">Proceed to Payment</button></a>
            </div>
        </div>
    
