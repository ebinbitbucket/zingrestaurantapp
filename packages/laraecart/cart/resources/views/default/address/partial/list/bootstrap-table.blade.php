            <table class="table" id="main-table" data-url="{!!guard_url('cart/address?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="name">{!! trans('cart::address.label.name')!!}</th>
                    <th data-field="mobile">{!! trans('cart::address.label.mobile')!!}</th>
                    <th data-field="zipcode">{!! trans('cart::address.label.zipcode')!!}</th>
                    <th data-field="address">{!! trans('cart::address.label.address')!!}</th>
                    <th data-field="landmark">{!! trans('cart::address.label.landmark')!!}</th>
                    <th data-field="state">{!! trans('cart::address.label.state')!!}</th>
                    <th data-field="city">{!! trans('cart::address.label.city')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">app.actions</th>
                    </tr>
                </thead>
            </table>