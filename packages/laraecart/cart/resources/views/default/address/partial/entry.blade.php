            <section class="inner-page-title">
            <div class="wrapper">
                <div class="checkout-steps clearfix">
                    <a href="#">4. Review</a>
                    <a href="#"><span class="angle"></span>3. Confirm</a>
                    <a href="#"><span class="angle"></span>3. Payment</a>
                    <a class="active" href="#"><span class="angle"></span>2. Delivery</a>
                    <a class="completed" href="#"><span class="angle"></span>1. Welcome</a>
                </div>
            </div>
        </section>

        <div class="wrapper">
            <div class="checkout-wrap shopping-cart">
                <div class="row">
                    <div class="col-md-9">
                      <div class='row'>
                        @foreach($addresses as $address)
                           <div class='col-md-4 col-sm-6'>
                              <p>Name: <span>{{$address['name']}}</span></p>
                              <p>Address: <span>{{$address['address']}}</span></p>
                              <p>Place: <span>{{$address['landmark']}}, {{$address['city']}}, {{$address['state']}}</span></p>
                             <p> Mobile: <span>{{$address['mobile']}}</span></p>
                             <a href="{{guard_url('cart/address')}}/{{$address->getRouteKey()}}" <button type="button" class="btn">Use this address</button></a>
                            </div>        
                        @endforeach
                      </div>
                       
                            <h3>Billing Address</h3>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-fn">First Name</label>
                                        <input class="form-control" type="text" id="checkout-fn" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-ln">Last Name</label>
                                        <input class="form-control" type="text" id="checkout-ln" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-email">E-Mail Address</label>
                                        <input class="form-control" type="email" id="checkout-email">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-phone">Phone Number</label>
                                        <input class="form-control" type="text" id="checkout-phone" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-company">Company</label>
                                        <input class="form-control" type="text" id="checkout-company">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-country">Country</label>
                                        {!! Form::select('country_id')
                                          -> options(Location::country())
                                          ->   addClass('form-control')
                                          -> placeholder('Select Country')!!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-city">City</label>
                                        {!! Form::select('state_id')
                                        -> options(Location::location())
                                        ->   addClass('form-control')
                                        -> placeholder('Select State')!!}
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-zip">ZIP Code</label>
                                        <input class="form-control" type="text" id="checkout-zip" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-30">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-address1">Address 1</label>
                                        <input class="form-control" type="text" id="checkout-address1" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="checkout-address2">Address 2</label>
                                        <input class="form-control" type="text" id="checkout-address2" required>
                                    </div>
                                </div>
                            </div>

                            <h3>Shipping Address</h3>
                            <hr class="padding-bottom-1x">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="same_address" checked>
                                    <label class="custom-control-label" for="same_address">Same as billing address</label>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                    <div class="col-md-3">
                        <div class="order-summery">
                            <h4>Order Summary</h4>
                            <div class="summery-wrap">
                                <div class="row no-gutters">
                                    <div class="col-6">{{Cart::count()}} items</div>
                                    <div class="col-6 text-right">$ {{Cart::total()}}</div>
                                </div>
                                <div class="row no-gutters total-row">
                                    <div class="col-6">Total</div>
                                    <div class="col-6 text-right">$ {{Cart::total()}}</div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
  $(document).ready(function(){
    $("#country_id").on('change', function(){

      var country_id = $("#country_id option:selected").val();

      $("#state_id").load('{{url("location/options")}}'+'/'+country_id);
    })

  })
</script>


           