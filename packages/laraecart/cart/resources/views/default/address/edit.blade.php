<section class="user-wrap-inner">
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-3 user-aside">
                            {!! Theme::partial('aside') !!}
                        </div>
                        <div class="col-md-9">
                            <div class="user-content-wrap">
                                <div class="heading-block">
                                    <h3>Edit delivery address</h3>
                                </div>
                                <div class="inner-content">
                                    {!!Form::vertical_open()
                            ->method('PUT')
                            ->action(guard_url('cart/address/'. $address->getRouteKey()))!!}
                                        <div class="row mb-20">
                                            <div class="col-md-12">
                                                <div id="locationMap" class="mb-15"></div> 
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label for="name">Address</label>
                                                    <input id="address" type="text" class="form-control" value="{{$address->address}}" name="address">
                                                    <input type="hidden" name="latitude" id="latitude_address" value="{{$address->latitude}}">
                                                    <input type="hidden" name="longitude" id="longitude_address" value="{{$address->longitude}}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name">Address Type</label>
                                                    <input type="text" class="form-control" name="title" value="{{$address->title}}">
                                                </div>
                                            </div>
                                        </div>
                                        <button class="btn btn-theme" type="submit">Save Address</button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section> 
<script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>            
        <script>
                 var geocoder = new google.maps.Geocoder();
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('address'));
        google.maps.event.addListener(places, 'place_changed', function () {
                geocodeAddress(geocoder);
        });
    });
     function geocodeAddress(geocoder) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            document.getElementById('latitude_address').value=results[0].geometry.location.lat();
            document.getElementById('longitude_address').value = results[0].geometry.location.lng()
           console.log(results[0].geometry.location.lat());
           console.log(results[0].geometry.location.lng());
           initialize();
          } else {
            alert('Geocode was not successful for the following reason: ' + status);
          }
        });
      }
            var map;
            var marker;
            var myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow();
            
            function initialize(){
                if(document.getElementById('latitude_address').value != ''){
                    var myLatlng = new google.maps.LatLng(document.getElementById('latitude_address').value,document.getElementById('longitude_address').value);                    

                }
                else{
                    var myLatlng = new google.maps.LatLng(32.7766642,-96.79698789999998);
                }
                var mapOptions = {
                    zoom: 18,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
               
                map = new google.maps.Map(document.getElementById("locationMap"), mapOptions);
                
                marker = new google.maps.Marker({
                    map: map,
                    position: myLatlng,
                    draggable: true 
                });     
                


                geocoder.geocode({'latLng': myLatlng }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $('#address').val(results[0].formatted_address);
                            infowindow.setContent(results[0].formatted_address);
                            infowindow.open(map, marker);
                        }
                    }
                });

                               
                google.maps.event.addListener(marker, 'dragend', function() {
                    geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[0]) {
                                $('#address').val(results[0].formatted_address);
                                infowindow.setContent(results[0].formatted_address);
                                infowindow.open(map, marker);
                            }
                        }
                    });
                });

                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function(position) {
                        var pos = {
                          lat: position.coords.latitude,
                          lng: position.coords.longitude
                        };
                        infowindow.setPosition(pos);
                        infowindow.setContent('Location found.');
                        infowindow.open(map);
                        map.setCenter(pos);
                    }, function() {
                        handleLocationError(true, infowindow, map.getCenter());
                    });
                } else {
                    handleLocationError(false, infowindow, map.getCenter());
                }
            }
            
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>            