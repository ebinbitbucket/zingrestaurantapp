
<div class="list-view">
        
       
         <div class="card list-view-media">
<div style="margin-left: 20px; margin-right: 20px;"> 
            <h3>Payment</h3> 

        <div class="table-responsive cart_info">
            <table class="table table-bordered">
                <thead>
                    <tr class="cart_menu">
                        <td class="description">Item</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        
                    </tr>
                </thead>
                <tbody>
                   
                    @foreach($carts as $item)

                    <tr>
                        
                        <td class="cart_description">
                            <h4><a href="">{{$item->name}}</a></h4>
                            
                        </td>
                        <td class="cart_price">
                            <p>${{$item->price}}</p>
                        </td>
                        <td class="cart_quantity">
                            <p>{{$item->qty}}
                                </td>
                                <td class="cart_total">
                                    <p class="cart_total_price">${{$item->subtotal}}</p>
                                </td>
                                
                            </tr>

                            @endforeach
                            <div align="right">
                                <table class="table table-bordered" align="right" style="width:400px">
                                    <tr>
                                        <td class="text-right"><strong>Sub-Total</strong></td>
                                        <td class="text-right">₹{{Cart::subtotal()}}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right"><strong>Tax </strong></td>
                                        <td class="text-right">₹{{Cart::tax()}}</td>
                                    </tr>

                                    <tr>
                                        <td class="text-right"><strong>Total</strong></td>
                                        <td class="text-right">₹ {{Cart::total()}}</td>
                                    </tr>
                                </table>
                            </div>
                            
                        </tbody>
                    </table>
                <form action="{{guard_url('cart/payment')}}">
 
                    <table class="table table-bordered">
                <thead>
                    <tr class="cart_menu">
                        <td class="image">Pay Online</td>
                        <td class="description">Bank Transfer</td>
                        <td class="price">Cash on Delivery</td>
                        
                    </tr>
                </thead>
                <tbody>
                   
                    

                    <tr>
                        <td class="cart_product">
                           <p><input type="radio" name="online" value="paypal">Paypal </p>
                           <p><input type="radio" name="online" value="google_wallot">Google Wallot </p>
                           <p><input type="radio" name="online" value="payoneer">Payoneer</p>
                           <p><input type="radio" name="online" value="2checkout">2Checkout</p>
                        </td>
                        <td class="cart_description">
                            <p><input type="radio" name="online" value="account1">SBI Kaloor </p>
                            <p><input type="radio" name="online" value="account2">Federal Bank Aluva </p>
                            
                        </td>
                        <td class="cart_price">
                            <p><input type="radio" name="online" value="cash_on_delivery">Cash on delivery</p>
                        </td>        
                    </tr>      
                        </tbody>
                    </table>
 <input type="hidden" name="address_id" value="{{$id}}">

      <button type="submit"  id="button-cart" name="payment_status" value="Paid" class="btn btn-primary" style="color: #007bff" align="right">Proceed Payment</button>
       <button type="submit"  value="Unpaid" name="payment_status" id="button-cart"  class="btn btn-primary" style="color: #007bff" align="right">Cancel Payment</button>
   </form>
                </div>
           
</div></div></div>