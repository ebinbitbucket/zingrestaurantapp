  <section class="user-wrap-inner">
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-3 user-aside">
                            {!! Theme::partial('aside') !!}
                        </div>
                        <div class="col-md-9">
                            <div class="user-content-wrap">
                                <div class="heading-block">
                                    <h3>Address</h3>
                                    <a href="{{guard_url('cart/user-address/add')}}" class="btn btn-theme">Add Delivery Address</a>
                                </div>
                                <div class="inner-content">
                        <ul class="nav nav-tabs">
                    
                            <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/address'))? 'active' : ''}}" href="{{guard_url('cart/address')}}">Delivery Addresses</a>
                            </li>
                              <li class="nav-item">
                                <a class="nav-link {{ (Request::is('client/cart/billing_address'))? 'active' : ''}}" href="{{guard_url('cart/billing_address')}}">Billing Addresses</a>
                            </li>
                           

                        </ul>
                                <div class="inner-content">
                                    <div class="address-wrap">
                                        <div class="row">
                                        	@foreach($addresses as $address)
                                            <div class="col-md-6" style="height: 150px;">
                                                <div class="address-item">
                                                    <div class="icon">
                                                        <i class="flaticon-pin"></i>
                                                    </div>
                                                    <div class="detail">
                                                        <h3>{{$address->title}}</h3>
                                                        <p>{{$address->address}}</p>
                                                        <div class="actions">
                                                            <a href="{{guard_url('cart/address') . '/' . $address->getRouteKey() . '/edit'}}">Edit</a>
                                                              <form id="delete-form_{!! $address->getRouteKey() !!}" style="display: inline-block;" method="POST" action="{!! guard_url('cart/address') !!}/{!! $address->getRouteKey() !!}">
                                              {{csrf_field()}}{{method_field('DELETE')}}
                                             <button type="submit" class="btn_delete_category" data-key="{!! $address->getRouteKey() !!}" style="border: none; background: none; padding: 0;">Delete</button></form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>