@extends('resource.index')
@php
$links['create'] = guard_url('cart/detail/create');
$links['search'] = guard_url('cart/detail');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('cart::detail.title.main') !!}
@stop

@section('sub.title') 
{!! __('cart::detail.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('cart/detail')!!}">{!! __('cart::detail.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('cart::detail.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$details->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
