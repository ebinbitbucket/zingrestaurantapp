<div class="container bg-white">
<div class="list-view">
    <h3>Orders</h3>
    <div class="card list-view-media"  >
        <div class="card-block">
            <div class="media">
                <div class="media-body">
                    <div class="heading">        
                    <table class="table table-striped">
                    <thead class="thead-light">
                        <tr class="cart_menu">
                            <td class="No">Order No</td>
                            <td class="Date">Date</td>  
                            <td class="Status">Size</td>
                            <td class="Status">Product</td>
                            <td class="Status">Payment Status</td>
                            <td class="Status">Quantity</td>
                            <td class="Status">Unit Price</td>
                            <td class="price">Price</td>
                            
                    
                        </tr>
                    </thead>
                    <tbody>                 
                    @foreach($details as $order)
                    <tr>    
                        <td class="cart_product">
                            {{$order->id}}
                        </td>
                        <td class="cart_description">
                            
                            {{format_date($order->created_at)}}
                            
                        </td>
                        
                        <td class="cart_quantity">
                            <p>{{$order->size}}</p>
                        </td>
                        <td class="cart_quantity">
                            <p>{{$order->product->title}}</p>
                        </td>
                        <td class="cart_quantity">
                            <p>{{$order->order->payment_status}}</p>
                        </td>
                         <td class="cart_quantity">
                            <p>{{$order->quantity}}</p>
                        </td>  
                        <td class="cart_quantity">
                            <p>{{$order->unit_price}}</p>
                        </td>  
                        <td class="cart_price">
                            <p>₹ {{$order->price}}</p>
                        </td>  
                        
                    </tr>
                    @endforeach
                    </tbody>
                    </table>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>