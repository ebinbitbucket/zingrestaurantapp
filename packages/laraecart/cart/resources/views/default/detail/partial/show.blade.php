            <div class="content">
                <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('cart::detail.label.id') !!}
                </label><br />
                    {!! $detail['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="order_id">
                    {!! trans('cart::detail.label.order_id') !!}
                </label><br />
                    {!! $detail['order_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="product_id">
                    {!! trans('cart::detail.label.product_id') !!}
                </label><br />
                    {!! $detail['product_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="quantity">
                    {!! trans('cart::detail.label.quantity') !!}
                </label><br />
                    {!! $detail['quantity'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="price">
                    {!! trans('cart::detail.label.price') !!}
                </label><br />
                    {!! $detail['price'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('cart::detail.label.created_at') !!}
                </label><br />
                    {!! $detail['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('cart::detail.label.updated_at') !!}
                </label><br />
                    {!! $detail['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('cart::detail.label.deleted_at') !!}
                </label><br />
                    {!! $detail['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('cart::detail.label.order_id'))
                       -> placeholder(trans('cart::detail.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('product_id')
                       -> label(trans('cart::detail.label.product_id'))
                       -> placeholder(trans('cart::detail.placeholder.product_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('quantity')
                       -> label(trans('cart::detail.label.quantity'))
                       -> placeholder(trans('cart::detail.placeholder.quantity'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('price')
                       -> label(trans('cart::detail.label.price'))
                       -> placeholder(trans('cart::detail.placeholder.price'))!!}
                </div>
            </div>