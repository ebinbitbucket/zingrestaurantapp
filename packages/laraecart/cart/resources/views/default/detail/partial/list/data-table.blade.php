            <table class="table" id="main-table" data-url="{!!guard_url('cart/detail?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="order_id">{!! trans('cart::detail.label.order_id')!!}</th>
                    <th data-field="product_id">{!! trans('cart::detail.label.product_id')!!}</th>
                    <th data-field="quantity">{!! trans('cart::detail.label.quantity')!!}</th>
                    <th data-field="price">{!! trans('cart::detail.label.price')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">app.actions</th>
                    </tr>
                </thead>
            </table>