            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('order_id')
                       -> label(trans('cart::detail.label.order_id'))
                       -> placeholder(trans('cart::detail.placeholder.order_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('product_id')
                       -> label(trans('cart::detail.label.product_id'))
                       -> placeholder(trans('cart::detail.placeholder.product_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('quantity')
                       -> label(trans('cart::detail.label.quantity'))
                       -> placeholder(trans('cart::detail.placeholder.quantity'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('price')
                       -> label(trans('cart::detail.label.price'))
                       -> placeholder(trans('cart::detail.placeholder.price'))!!}
                </div>
            </div>