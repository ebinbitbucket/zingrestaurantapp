@extends('resource.show')

@php
$links['back'] = guard_url('cart/detail');
$links['edit'] = guard_url('cart/detail') . '/' . $detail->getRouteKey() . '/edit';
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('cart::detail.title.main') !!}
@stop

@section('sub.title') 
{!! __('cart::detail.title.show') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('$cart/detail')!!}">{!! __('cart::detail.name') !!}</a></li>
  <li>{{ __('app.show') }}</li>
@stop

@section('tabs') 
@stop

@section('tools') 
    <a href="{!!guard_url('$cart/detail')!!}" rel="tooltip" class="btn btn-white btn-round btn-simple btn-icon pull-right add-new" data-original-title="" title="">
            <i class="fa fa-chevron-left"></i>
    </a>
@stop

@section('content') 
    @include('cart::detail.partial.show', ['mode' => 'show'])
@stop
