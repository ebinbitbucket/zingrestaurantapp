<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for calls in calls package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  calls module in calls package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Calls',
    'names'         => 'Calls',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Calls',
        'sub'   => 'Calls',
        'list'  => 'List of calls',
        'edit'  => 'Edit calls',
        'create'    => 'Create new calls'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'order_id'                   => 'Please enter order id',
        'restaurant_id'              => 'Please enter restaurant id',
        'call_count'                 => 'Please enter call count',
        'datetime'                   => 'Please select datetime',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'order_id'                   => 'Order id',
        'restaurant_id'              => 'Restaurant id',
        'call_count'                 => 'Call count',
        'datetime'                   => 'Datetime',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'order_id'                   => ['name' => 'Order id', 'data-column' => 1, 'checked'],
        'restaurant_id'              => ['name' => 'Restaurant id', 'data-column' => 2, 'checked'],
        'call_count'                 => ['name' => 'Call count', 'data-column' => 3, 'checked'],
        'datetime'                   => ['name' => 'Datetime', 'data-column' => 4, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Calls',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
