<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for address in cart package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  address module in cart package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Address',
    'names'         => 'Addresses',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Addresses',
        'sub'   => 'Addresses',
        'list'  => 'List of addresses',
        'edit'  => 'Edit address',
        'create'    => 'Create new address'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'name'                       => 'Please enter name',
        'email'                      => 'Please enter email',
        'phone'                      => 'Please enter phone',
        'zipcode'                    => 'Please enter zipcode',
        'address'                    => 'Please enter address',
        'street'                     => 'Please enter street',
        'state_id'                   => 'Please enter state id',
        'country_id'                 => 'Please enter country id',
        'user_id'                    => 'Please enter user id',
        'user_type'                  => 'Please enter user type',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'name'                       => 'Name',
        'email'                      => 'Email',
        'phone'                      => 'Phone',
        'zipcode'                    => 'Zipcode',
        'address'                    => 'Address',
        'street'                     => 'Street',
        'state_id'                   => 'State id',
        'country_id'                 => 'Country id',
        'user_id'                    => 'User id',
        'user_type'                  => 'User type',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'name'                       => ['name' => 'Name', 'data-column' => 1, 'checked'],
        'email'                      => ['name' => 'Email', 'data-column' => 2, 'checked'],
        'phone'                      => ['name' => 'Phone', 'data-column' => 3, 'checked'],
        'zipcode'                    => ['name' => 'Zipcode', 'data-column' => 4, 'checked'],
        'country_id'                 => ['name' => 'Country id', 'data-column' => 5, 'checked'],
        'created_at'                 => ['name' => 'Created at', 'data-column' => 6, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Addresses',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
