<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for details in cart package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  details module in cart package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Details',
    'names'         => 'Details',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Details',
        'sub'   => 'Details',
        'list'  => 'List of details',
        'edit'  => 'Edit details',
        'create'    => 'Create new details'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'order_id'                   => 'Please enter order id',
        'product_id'                 => 'Please enter product id',
        'size'                       => 'Please enter size',
        'quantity'                   => 'Please enter quantity',
        'unit_price'                 => 'Please enter unit price',
        'price'                      => 'Please enter price',
        'user_id'                    => 'Please enter user id',
        'user_type'                  => 'Please enter user type',
        'parameters'                 => 'Please enter parameters',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'order_id'                   => 'Order id',
        'product_id'                 => 'Product id',
        'size'                       => 'Size',
        'quantity'                   => 'Quantity',
        'unit_price'                 => 'Unit price',
        'price'                      => 'Price',
        'user_id'                    => 'User id',
        'user_type'                  => 'User type',
        'parameters'                 => 'Parameters',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'order_id'                   => ['name' => 'Order id', 'data-column' => 1, 'checked'],
        'product_id'                 => ['name' => 'Product id', 'data-column' => 2, 'checked'],
        'size'                       => ['name' => 'Size', 'data-column' => 3, 'checked'],
        'quantity'                   => ['name' => 'Quantity', 'data-column' => 4, 'checked'],
        'unit_price'                 => ['name' => 'Unit price', 'data-column' => 5, 'checked'],
        'price'                      => ['name' => 'Price', 'data-column' => 6, 'checked'],
        'created_at'                 => ['name' => 'Created at', 'data-column' => 7, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Details',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
