<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for history in cart package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  history module in cart package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'History',
    'names'         => 'Histories',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Histories',
        'sub'   => 'Histories',
        'list'  => 'List of histories',
        'edit'  => 'Edit history',
        'create'    => 'Create new history'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'order_id'                   => 'Please enter order id',
        'courier_name'               => 'Please enter courier name',
        'order_status'               => 'Please enter order status',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'order_id'                   => 'Order id',
        'courier_name'               => 'Courier name',
        'order_status'               => 'Order status',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'order_id'                   => ['name' => 'Order id', 'data-column' => 1, 'checked'],
        'courier_name'               => ['name' => 'Courier name', 'data-column' => 2, 'checked'],
        'order_status'               => ['name' => 'Order status', 'data-column' => 3, 'checked'],
        'created_at'                 => ['name' => 'Created at', 'data-column' => 4, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Histories',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
