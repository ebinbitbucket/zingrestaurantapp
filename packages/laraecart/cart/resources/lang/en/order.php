<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for order in cart package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  order module in cart package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Order',
    'names'         => 'Orders',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Orders',
        'sub'   => 'Orders',
        'list'  => 'List of orders',
        'edit'  => 'Edit order',
        'create'    => 'Create new order'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            'payment_status'      => ['Paid','Unpaid'],
            'order_status'      =>   ['New Orders','Delivered','Completed','Scheduled','Preparing','Picked up','Cancelled'],
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'name'                       => 'Please enter name',
        'email'                      => 'Please enter email',
        'address'                    => 'Please enter address',
        'phone'                      => 'Please enter phone',
        'street'                     => 'Please enter street',
        'zipcode'                    => 'Please enter zipcode',
        'state_id'                   => 'Please enter state id',
        'country_id'                 => 'Please enter country id',
        'shipping_name'              => 'Please enter shipping name',
        'shipping_city'              => 'Please enter shipping city',
        'shipping_zipcode'           => 'Please enter shipping zipcode',
        'shipping_email'             => 'Please enter shipping email',
        'shipping_address'           => 'Please enter shipping address',
        'shipping_phone'             => 'Please enter shipping phone',
        'track_status'               => 'Please enter track status',
        'shipping'                   => 'Please enter shipping',
        'delivery_id'                => 'Please enter delivery id',
        'subtotal'                   => 'Please enter subtotal',
        'tax'                        => 'Please enter tax',
        'total'                      => 'Please enter total',
        'payment_status'             => 'Please select payment status',
        'payment_methods'            => 'Please enter payment methods',
        'payment_details'            => 'Please enter payment details',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'name'                       => 'Name',
        'email'                      => 'Email',
        'address'                    => 'Address',
        'phone'                      => 'Phone',
        'street'                     => 'Street',
        'zipcode'                    => 'Zipcode',
        'state_id'                   => 'State id',
        'country_id'                 => 'Country id',
        'shipping_name'              => 'Shipping name',
        'shipping_city'              => 'Shipping city',
        'shipping_zipcode'           => 'Shipping zipcode',
        'shipping_email'             => 'Shipping email',
        'shipping_address'           => 'Shipping address',
        'shipping_phone'             => 'Shipping phone',
        'track_status'               => 'Track status',
        'shipping'                   => 'Shipping',
        'delivery_id'                => 'Delivery id',
        'subtotal'                   => 'Subtotal',
        'tax'                        => 'Tax',
        'total'                      => 'Total',
        'payment_status'             => 'Payment status',
        'payment_methods'            => 'Payment methods',
        'payment_details'            => 'Payment details',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'name'                       => ['name' => 'Name', 'data-column' => 1, 'checked'],
        'email'                      => ['name' => 'Email', 'data-column' => 2, 'checked'],
        'phone'                      => ['name' => 'Phone', 'data-column' => 3, 'checked'],
        'street'                     => ['name' => 'Street', 'data-column' => 4, 'checked'],
        'shipping_name'              => ['name' => 'Shipping name', 'data-column' => 5, 'checked'],
        'shipping_city'              => ['name' => 'Shipping city', 'data-column' => 6, 'checked'],
        'shipping_email'             => ['name' => 'Shipping email', 'data-column' => 7, 'checked'],
        'shipping_phone'             => ['name' => 'Shipping phone', 'data-column' => 8, 'checked'],
        'track_status'               => ['name' => 'Track status', 'data-column' => 9, 'checked'],
        'total'                      => ['name' => 'Total', 'data-column' => 10, 'checked'],
        'payment_status'             => ['name' => 'Payment status', 'data-column' => 11, 'checked'],
        'payment_methods'            => ['name' => 'Payment methods', 'data-column' => 12, 'checked'],
        'created_at'                 => ['name' => 'Created at', 'data-column' => 13, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Orders',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
