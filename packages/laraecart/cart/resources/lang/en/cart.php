<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for cart in cart package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  cart module in cart package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Cart',
    'names'         => 'Carts',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Carts',
        'sub'   => 'Carts',
        'list'  => 'List of carts',
        'edit'  => 'Edit cart',
        'create'    => 'Create new cart'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'instance'                   => 'Please enter instance',
        'content'                    => 'Please enter content',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'instance'                   => 'Instance',
        'content'                    => 'Content',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'instance'                   => ['name' => 'Instance', 'data-column' => 1, 'checked'],
        'content'                    => ['name' => 'Content', 'data-column' => 2, 'checked'],
        'created_at'                 => ['name' => 'Created at', 'data-column' => 3, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Carts',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
