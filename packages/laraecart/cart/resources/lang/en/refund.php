<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for refund in refund package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  refund module in refund package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Refund',
    'names'         => 'Refunds',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Refunds',
        'sub'   => 'Refunds',
        'list'  => 'List of refunds',
        'edit'  => 'Edit refund',
        'create'    => 'Create new refund'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'order_number'               => 'Please enter order number',
        'order_amount'               => 'Please enter order amount',
        'last_digits'                => 'Please enter last digits',
        'reason'                     => 'Please enter reason',
        'email'                      => 'Please enter email',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
        'user_id'                    => 'Please enter user id',
        'user_type'                  => 'Please enter user type',
        'PRIMARY'                    => 'Please select PRIMARY',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'order_number'               => 'Order number',
        'order_amount'               => 'Order amount',
        'last_digits'                => 'Last digits',
        'reason'                     => 'Reason',
        'email'                      => 'Email',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
        'user_id'                    => 'User id',
        'user_type'                  => 'User type',
        'PRIMARY'                    => 'PRIMARY',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'order_number'               => ['name' => 'Order number', 'data-column' => 1, 'checked'],
        'order_amount'               => ['name' => 'Order amount', 'data-column' => 2, 'checked'],
        'last_digits'                => ['name' => 'Last digits', 'data-column' => 3, 'checked'],
        'reason'                     => ['name' => 'Reason', 'data-column' => 4, 'checked'],
        'email'                      => ['name' => 'Email', 'data-column' => 5, 'checked'],
        'PRIMARY'                    => ['name' => 'PRIMARY', 'data-column' => 6, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'Refunds',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
