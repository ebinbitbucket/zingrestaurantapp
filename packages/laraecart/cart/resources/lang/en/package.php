<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Cart package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for cart module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Cart',
    'names'         => 'Carts',
];
