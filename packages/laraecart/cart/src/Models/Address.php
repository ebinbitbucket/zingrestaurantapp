<?php

namespace Laraecart\Cart\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Trans\Traits\Translatable;
class Address extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable, PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'laraecart.cart.address.model';

     public function client(){
        return $this->belongsTo('App\Client','client_id','id');
    }

    public function getFullAddressAttribute()
    {
        $address=$this->address ? $this->address : '';
        $apartment_no=$this->apartment_no ? $this->apartment_no : '';
        if($address&&$apartment_no){
            $arr = explode(",", $address);
            $arr[0] = $arr[0].' #'.$apartment_no;
            return implode(",",$arr);

        } else{
            return $this->address;
        }
    }
}
