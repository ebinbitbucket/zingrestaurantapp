<?php

namespace Laraecart\Cart\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
// use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Trans\Traits\Translatable;
class Order extends Model
{
    use Filer, SoftDeletes, Hashids, Translatable, PresentableTrait;
    // Slugger

    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'laraecart.cart.order.model';


    /**
     * The address that belong to the order.
     */
    public function address(){
        return $this->belongsTo('Cart\Order\Models\Address');
    }

    public function detail(){
        return $this->hasMany('Laraecart\Cart\Models\Details');
    }

    public function restaurant(){
        return $this->belongsTo('Restaurant\Restaurant\Models\Restaurant','restaurant_id','id');
    }

    public function delivery_address(){
        return $this->belongsTo('Laraecart\Cart\Models\Address','address_id','id');
    }
    
    public function billing_address() {
        return $this->belongsTo('Laraecart\Cart\Models\Address', 'billing_address_id', 'id');
    }

    public function user(){
        return $this->morphTo();
    }
}
