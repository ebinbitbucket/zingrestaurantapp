<?php

namespace  Laraecart\Cart\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
// use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Trans\Traits\Translatable;

class Refund extends Model
{
    use Filer, SoftDeletes, Hashids, Translatable, PresentableTrait;
    // Slugger
    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'laraecart.cart.refund.model';


}
