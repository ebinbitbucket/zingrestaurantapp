<?php

namespace Laraecart\Cart;

use Closure;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\DatabaseManager;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Collection;
use Laraecart\Cart\Contracts\Buyable;
use Laraecart\Cart\Exceptions\CartAlreadyStoredException;
use Laraecart\Cart\Exceptions\InvalidRowIDException;
use Laraecart\Cart\Exceptions\UnknownModelException;
use User;
use Session;
use Laraecart\Cart\Models\Order;
use Restaurant\Restaurant\Models\Favourite;

class Cart
{
    const DEFAULT_INSTANCE = 'default';
    /**
     * Instance of the session manager.
     *
     * @var \Illuminate\Session\SessionManager
     */
    private $session;
    /**
     * Instance of the event dispatcher.
     *
     * @var \Illuminate\Contracts\Events\Dispatcher
     */
    private $events;
    /**
     * Holds the current cart instance.
     *
     * @var string
     */
    private $instance;
    /**
     * $order object.
     */
    protected $order;
    /**
     * $cart object.
     */
    protected $cart;
    /**
     * Cart constructor.
     *
     * @param \Illuminate\Session\SessionManager      $session
     * @param \Illuminate\Contracts\Events\Dispatcher $events
     */
    public function __construct(SessionManager $session, Dispatcher $events, \Laraecart\Cart\Interfaces\CartRepositoryInterface $cart, \Laraecart\Cart\Interfaces\OrderRepositoryInterface $order, \Laraecart\Cart\Interfaces\DetailsRepositoryInterface $detail, \Laraecart\Cart\Interfaces\AddressRepositoryInterface $address)
    {
        $this->session = $session;
        $this->events  = $events;
        $this->cart    = $cart;
        $this->order   = $order;
        $this->detail   = $detail;
        $this->address   = $address;
        $this->instance(self::DEFAULT_INSTANCE);
    }

    /**
     * Set the current cart instance.
     *
     * @param string|null $instance
     * @return \Gloudemans\Shoppingcart\Cart
     */
    public function instance($instance = null)
    {
        $instance       = $instance ?: self::DEFAULT_INSTANCE;
        $this->instance = sprintf('%s.%s', 'cart', $instance);
        return $this;
    }

    /**
     * Get the current cart instance.
     *
     * @return string
     */
    public function currentInstance()
    {
        return str_replace('cart.', '', $this->instance);
    }

    /**
     * $cart object.
     */

// protected $cart;

    /**
     * Constructor.
     */

// public function __construct(\Laraecart\Cart\Interfaces\CartRepositoryInterface $cart)

// {

//     $this->cart = $cart;

// }

    /**
     * Returns count of cart.
     *
     * @param array $filter
     *
     * @return int
     */
    public function totalorder($status = '')
    {

        if (user()->hasRole('client') || user()->hasRole('restaurant')) {
            $this->order->pushCriteria(new \Laraecart\Cart\Repositories\Criteria\OrderResourceCriteria());

        }

        if ($status == '') {
            return $this->order->count();
        }

        if ($status == 'paid') {
            return $this->order->scopeQuery(function ($query) {
                return $query->where('payment_status', '=', 'Paid');

            })->count();
        }

        if ($status == 'unpaid') {
            return $this->order->scopeQuery(function ($query) {
                return $query->where('payment_status', '=', 'Unpaid');

            })->count();
        }

        if ($status == 'total') {

            return $this->order->sumtotal($status);
        }

        if ($status == 'totalpaid') {

            return $this->order->sumtotal($status);
        }

        if ($status == 'totalunpaid') {

            return $this->order->sumtotal($status);
        }
        if ($status == 'products') {
            $orders = $this->order->findByField(['restaurant_id'=>user_id(),'payment_status' => 'Paid'])->pluck('id')->toArray();
            return $this->detail->sumproducts($orders);
        }
        if ($status == 'neworders') {
            return $this->order->findByField(['restaurant_id'=>user_id(),'order_status' => 'New Orders'])->count();
        }

    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.cart.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->cart->pushCriteria(new \Litepie\Laraecart\Repositories\Criteria\CartUserCriteria());
        }

        $cart = $this->cart->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('cart::' . $view, compact('cart'))->render();
    }

    /**
     * Add an item to the cart.
     *
     * @param mixed     $id
     * @param mixed     $name
     * @param int|float $qty
     * @param float     $price
     * @param array     $options
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function add($id, $name = null, $qty = null, $price = null, $image = null, array $options = [])
    { 
        if ($this->isMulti($id)) {
            return array_map(function ($item) {
                return $this->add($item);
            }, $id);
        }

        $cartItem = $this->createCartItem($id, $name, $qty, $price, $image, $options);

        $content = $this->getContent();

        if ($content->has($cartItem->rowId)) {
            $cartItem->qty += $content->get($cartItem->rowId)->qty;
        }

        $content->put($cartItem->rowId, $cartItem);

        $this->events->fire('cart.added', $cartItem);

        $this->session->put($this->instance, $content);
        return $cartItem;
    }

    /**
     * Add an item to the cart.
     *
     * @param mixed     $id
     * @param mixed     $name
     * @param int|float $qty
     * @param float     $price
     * @param array     $options
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function subtract($id, $name = null, $qty = null, $price = null, $image = null, array $options = [])
    {
        if ($this->isMulti($id)) {
            return array_map(function ($item) {
                return $this->subtract($item);
            }, $id);
        }

        $cartItem = $this->createCartItem($id, $name, $qty, $price, $image, $options);

        $content = $this->getContent();

        if ($content->has($cartItem->rowId)) {
            $cartItem->qty = $content->get($cartItem->rowId)->qty - 1;
        }

        $content->put($cartItem->rowId, $cartItem);

        $this->events->fire('cart.added', $cartItem);

        $this->session->put($this->instance, $content);

        return $cartItem;
    }

    public function increment($rowId, $qty)
    {
        $content  = $this->getContent();
        $cartItem = $this->get($rowId);
        if ($content->has($cartItem->rowId)) {
            $cartItem->qty = $content->get($cartItem->rowId)->qty + 1;
        }

        if ($rowId !== $cartItem->rowId) {
            $content->pull($rowId);

            if ($content->has($cartItem->rowId)) {
                $existingCartItem = $this->get($cartItem->rowId);
                $cartItem->setQuantity($existingCartItem->qty + $cartItem->qty);
            }

        }

        if ($cartItem->qty <= 0) {
            $this->remove($cartItem->rowId);
            return;
        } else {
            $content->put($cartItem->rowId, $cartItem);
        }

        $this->events->fire('cart.updated', $cartItem);

        $this->session->put($this->instance, $content);

        return $cartItem;
    }

    public function decrement($rowId, $qty)
    {
        $content  = $this->getContent();
        $cartItem = $this->get($rowId);
        if ($content->has($cartItem->rowId)) {
            if($cartItem->qty!=1){
                            $cartItem->qty = $content->get($cartItem->rowId)->qty - 1;

            }
        }

        if ($rowId !== $cartItem->rowId) {
            $content->pull($rowId);

            if ($content->has($cartItem->rowId)) {
                $existingCartItem = $this->get($cartItem->rowId);
                $cartItem->setQuantity($existingCartItem->qty + $cartItem->qty);
            }

        }

        if ($cartItem->qty <= 0) {
            $this->remove($cartItem->rowId);
            return;
        } else {
            $content->put($cartItem->rowId, $cartItem);
        }

        $this->events->fire('cart.updated', $cartItem);

        $this->session->put($this->instance, $content);

        return $cartItem;
    }

    /**
     * Update the cart item with the given rowId.
     *
     * @param string $rowId
     * @param mixed  $qty
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function update($request)
    {
        foreach($request['quantity'] as $rowId=>$qty)
        {
            $cartItem = $this->get($rowId);

            if ($qty instanceof Buyable) {
                $cartItem->updateFromBuyable($qty);
            } elseif (is_array($qty)) {
                $cartItem->updateFromArray($qty);
            } else {
                $cartItem->qty = $qty;
            }

            $content = $this->getContent();

            if ($rowId !== $cartItem->rowId) {
                $content->pull($rowId);

                if ($content->has($cartItem->rowId)) {
                    $existingCartItem = $this->get($cartItem->rowId);
                    $cartItem->setQuantity($existingCartItem->qty + $cartItem->qty);
                }

            }

            if ($cartItem->qty <= 0) {
                $this->remove($cartItem->rowId);
                return;
            } else {
                $content->put($cartItem->rowId, $cartItem);
            }

            $this->events->fire('cart.updated', $cartItem);

            $this->session->put($this->instance, $content);

           
        }
        return Cart::content();
    }

    // public function update($rowId, $qty)
    // {
    //     $cartItem = $this->get($rowId);

    //     if ($qty instanceof Buyable) {
    //         $cartItem->updateFromBuyable($qty);
    //     } elseif (is_array($qty)) {
    //         $cartItem->updateFromArray($qty);
    //     } else {
    //         $cartItem->qty = $qty;
    //     }

    //     $content = $this->getContent();

    //     if ($rowId !== $cartItem->rowId) {
    //         $content->pull($rowId);

    //         if ($content->has($cartItem->rowId)) {
    //             $existingCartItem = $this->get($cartItem->rowId);
    //             $cartItem->setQuantity($existingCartItem->qty + $cartItem->qty);
    //         }

    //     }

    //     if ($cartItem->qty <= 0) {
    //         $this->remove($cartItem->rowId);
    //         return;
    //     } else {
    //         $content->put($cartItem->rowId, $cartItem);
    //     }

    //     $this->events->fire('cart.updated', $cartItem);

    //     $this->session->put($this->instance, $content);

    //     return $cartItem;
    // }
    /**
     * Remove the cart item with the given rowId from the cart.
     *
     * @param string $rowId
     * @return void
     */
    public function remove($rowId)
    {
        $cartItem = $this->get($rowId);

        $content = $this->getContent();

        $content->pull($cartItem->rowId);

        $this->events->fire('cart.removed', $cartItem);

        $this->session->put($this->instance, $content);
    }

    /**
     * Get a cart item from the cart by its rowId.
     *
     * @param string $rowId
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    public function get($rowId)
    {
        $content = $this->getContent();

        if (!$content->has($rowId)) {
            throw new InvalidRowIDException("The cart does not contain rowId {$rowId}.");
        }

        return $content->get($rowId);
    }

    /**
     * Destroy the current cart instance.
     *
     * @return void
     */
    public function destroy()
    {
        $this->session->remove($this->instance);
    }

    /**
     * Get the content of the cart.
     *
     * @return \Illuminate\Support\Collection
     */
    public function content()
    {
        if (is_null($this->session->get($this->instance))) {
            return new Collection([]);
        }

        return $this->session->get($this->instance);
    }

    /**
     * Get the number of items in the cart.
     *
     * @return int|float
     */
    public function count()
    {
        $content = $this->getContent();

        return $content->sum('qty');
    }

    /**
     * Get the total price of the items in the cart.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return string
     */
    public function total($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        $content = $this->getContent();

        $total = $content->reduce(function ($total, CartItem $cartItem) { $addon_price = 0;
            if(!empty($cartItem->options['addons'])){
                foreach ($cartItem->options['addons'] as $value) {
                $addon_price = $addon_price + ($value[2]* $cartItem->qty);
            }
            }
            
            return $total + ($cartItem->qty * $cartItem->priceTax) + $addon_price;
        }, 0);
        return $total;
        // return $this->numberFormat($total, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Get the total tax of the items in the cart.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return float
     */
    public function tax($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        $content = $this->getContent();

        $tax = $content->reduce(function ($tax, CartItem $cartItem) {
            return $tax + ($cartItem->qty * $cartItem->tax);
        }, 0);
        return $tax;
        // return $this->numberFormat($tax, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Get the subtotal (total - tax) of the items in the cart.
     *
     * @param int    $decimals
     * @param string $decimalPoint
     * @param string $thousandSeperator
     * @return float
     */
    public function subtotal($decimals = null, $decimalPoint = null, $thousandSeperator = null)
    {
        $content = $this->getContent();

        $subTotal = $content->reduce(function ($subTotal, CartItem $cartItem) {
            $addon_price = 0;
            if(!empty($cartItem->options['addons'])){
                foreach ($cartItem->options['addons'] as $value) {
                $addon_price = $addon_price + ($value[2]* $cartItem->qty);
            }
            }
            return $subTotal + ($cartItem->qty * $cartItem->price+ $addon_price);
        }, 0);
        return $subTotal;
        // return $this->numberFormat($subTotal, $decimals, $decimalPoint, $thousandSeperator);
    }

    /**
     * Search the cart content for a cart item matching the given search closure.
     *
     * @param \Closure $search
     * @return \Illuminate\Support\Collection
     */
    public function search(Closure $search)
    {
        $content = $this->getContent();

        return $content->filter($search);
    }

    /**
     * Associate the cart item with the given rowId with the given model.
     *
     * @param string $rowId
     * @param mixed  $model
     * @return void
     */
    public function associate($rowId, $model)
    {

        if (is_string($model) && !class_exists($model)) {
            throw new UnknownModelException("The supplied model {$model} does not exist.");
        }

        $cartItem = $this->get($rowId);

        $cartItem->associate($model);

        $content = $this->getContent();

        $content->put($cartItem->rowId, $cartItem);

        $this->session->put($this->instance, $content);
    }

    /**
     * Set the tax rate for the cart item with the given rowId.
     *
     * @param string    $rowId
     * @param int|float $taxRate
     * @return void
     */
    public function setTax($rowId, $taxRate)
    {
        $cartItem = $this->get($rowId);

        $cartItem->setTaxRate($taxRate);

        $content = $this->getContent();

        $content->put($cartItem->rowId, $cartItem);

        $this->session->put($this->instance, $content);
    }

    /**
     * Store an the current instance of the cart.
     *
     * @param mixed $identifier
     * @return void
     */
    public function store($identifier)
    {
        $content = $this->getContent();

        if ($this->storedCartWithIdentifierExists($identifier)) {
            throw new CartAlreadyStoredException("A cart with identifier {$identifier} was already stored.");
        }

        $this->getConnection()->table($this->getTableName())->insert([
            'identifier' => $identifier,
            'instance'   => $this->currentInstance(),
            'content'    => serialize($content),
        ]);

        $this->events->fire('cart.stored');
    }

    /**
     * Restore the cart with the given identifier.
     *
     * @param mixed $identifier
     * @return void
     */
    public function restore($identifier)
    {

        if (!$this->storedCartWithIdentifierExists($identifier)) {
            return;
        }

        $stored = $this->getConnection()->table($this->getTableName())
            ->where('identifier', $identifier)->first();

        $storedContent = unserialize($stored->content);

        $currentInstance = $this->currentInstance();

        $this->instance($stored->instance);

        $content = $this->getContent();

        foreach ($storedContent as $cartItem) {
            $content->put($cartItem->rowId, $cartItem);
        }

        $this->events->fire('cart.restored');

        $this->session->put($this->instance, $content);

        $this->instance($currentInstance);

        $this->getConnection()->table($this->getTableName())
            ->where('identifier', $identifier)->delete();
    }

    /**
     * Magic method to make accessing the total, tax and subtotal properties possible.
     *
     * @param string $attribute
     * @return float|null
     */
    public function __get($attribute)
    {

        if ($attribute === 'total') {
            return $this->total();
        }

        if ($attribute === 'tax') {
            return $this->tax();
        }

        if ($attribute === 'subtotal') {
            return $this->subtotal();
        }

        return null;
    }

    /**
     * Get the carts content, if there is no cart content set yet, return a new empty Collection
     *
     * @return \Illuminate\Support\Collection
     */
    protected function getContent()
    {
        $content = $this->session->has($this->instance)
        ? $this->session->get($this->instance)
        : new Collection;

        return $content;
    }

    /**
     * Create a new CartItem from the supplied attributes.
     *
     * @param mixed     $id
     * @param mixed     $name
     * @param int|float $qty
     * @param float     $price
     * @param array     $options
     * @return \Gloudemans\Shoppingcart\CartItem
     */
    private function createCartItem($id, $name, $qty, $price, $image, array $options)
    {

        if ($id instanceof Buyable) {
            $cartItem = CartItem::fromBuyable($id, $qty ?: []);
            $cartItem->setQuantity($name ?: 1);
            $cartItem->associate($id);
        } elseif (is_array($id)) {
            $cartItem = CartItem::fromArray($id);
            $cartItem->setQuantity($id['qty']);
        } else {
            $cartItem = CartItem::fromAttributes($id, $name, $image, $price, $options);
            $cartItem->setQuantity($qty);
        }

        $cartItem->setTaxRate(config('cart.tax'));

        return $cartItem;
    }

    /**
     * Check if the item is a multidimensional array or an array of Buyables.
     *
     * @param mixed $item
     * @return bool
     */
    private function isMulti($item)
    {

        if (is_array($item)) {
            return false;
        }
        return is_array(head($item)) || head($item) instanceof Buyable;
    }

    /**
     * @param $identifier
     * @return bool
     */
    private function storedCartWithIdentifierExists($identifier)
    {
        return $this->getConnection()->table($this->getTableName())->where('identifier', $identifier)->exists();
    }

    /**
     * Get the database connection.
     *
     * @return \Illuminate\Database\Connection
     */
    private function getConnection()
    {
        $connectionName = $this->getConnectionName();

        return app(DatabaseManager::class)->connection($connectionName);
    }

    /**
     * Get the database table name.
     *
     * @return string
     */
    private function getTableName()
    {
        return config('cart.database.table', 'carts');
    }

    /**
     * Get the database connection name.
     *
     * @return string
     */
    private function getConnectionName()
    {
        $connection = config('cart.database.connection');

        return is_null($connection) ? config('database.default') : $connection;
    }

    /**
     * Get the Formated number
     *
     * @param $value
     * @param $decimals
     * @param $decimalPoint
     * @param $thousandSeperator
     * @return string
     */
    private function numberFormat($value, $decimals, $decimalPoint, $thousandSeperator)
    {

        if (is_null($decimals)) {
            $decimals = is_null(config('cart.format.decimals')) ? 2 : config('cart.format.decimals');
        }

        if (is_null($decimalPoint)) {
            $decimalPoint = is_null(config('cart.format.decimal_point')) ? '.' : config('cart.format.decimal_point');
        }

        if (is_null($thousandSeperator)) {
            $thousandSeperator = is_null(config('cart.format.thousand_seperator')) ? ',' : config('cart.format.thousand_seperator');
        }

        return number_format($value, $decimals, $decimalPoint, $thousandSeperator);
    }

    public function myorders()
    {
        
          if (user()->hasRole('client')) {
            return  $this->order->filterOrders('currorders');

        }
        if (user()->hasRole('restaurant')) {
            return  $this->order->with('detail', 'detail.menu')->pushCriteria(new \Laraecart\Cart\Repositories\Criteria\OrderResourceCriteria())->orderBy('created_at','DESC')->get()->take(5);

        }
    }

    public function orderList()
    {
        return $this->order->with('restaurant')->get()->take(10);
    }

    public function neworderList()
    {
        return $this->order->with('restaurant')->findByField('order_status','New orders')->take(10);
    }

    public function getAddresses()
    {
        return $this->address->orderBy('id','DESC')->findByField(['client_id'=>user_id(),'default_address'=> 1,'type'=>'delivery']);
        return $this->address->orderBy('id','DESC')->findByField(['client_id'=>user_id(),'default_address'=> 1,'type'=>'delivery'])->take(1);
    }
    public function getAllAddresses()
    {
        return $this->address->findByField(['client_id'=>user_id(),'type'=>'delivery']);
    }
    
    public function getLoyaltypoints($user='')
    {
        if(empty($user)){
            $user = user_id();
        }
        $sum =  $this->order->findByField('user_id',$user)->where('guest', 'false')->sum('loyalty_points');
        $discount =  $this->order->findByField('user_id',$user)->where('guest', 'false')->sum('discount_points');
        return $sum-$discount;
    }

     public function getKitchenRestaurants($restaurant_id)
    {
        return $this->order->getKitchenRestaurants($restaurant_id);
        
    }

      public function MaxOrderCount($menu_id)
    {
        if(!empty(Session::get('search_time'))){
            $time = date('Y-m-d',strtotime(Session::get('search_time'))).'%';
        }
        else{
            $time = date('Y-m-d').'%';
        }
        $order_ids= Order::where('delivery_time', 'like', $time)->pluck('id')->toArray();
        return $this->detail->MaxOrderCount($menu_id,$order_ids);
       
        
    }
    public function getBillingAddresses()
    {
        return $this->address->orderBy('id','DESC')->findByField(['client_id'=>user_id(),'type'=>'billing'])->take(1);
    }

        public function getAllBillingAddresses()
    {
        return $this->address->findByField(['client_id'=>user_id(),'type'=>'billing']);
    }
      public function getFavouriteStatus($order_id)
    {
        return Favourite::where('favourite_id',$order_id)->where('type','order')->where('user_id',user_id())->count();
    }

}
