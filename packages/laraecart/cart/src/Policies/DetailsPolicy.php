<?php

namespace Laraecart\Cart\Policies;

use Litepie\User\Contracts\UserPolicy;
use Laraecart\Cart\Models\Details;

class DetailsPolicy
{

    /**
     * Determine if the given user can view the details.
     *
     * @param UserPolicy $user
     * @param Details $details
     *
     * @return bool
     */
    public function view(UserPolicy $user, Details $details)
    {
        if ($user->canDo('cart.details.view') && $user->isAdmin()) {
            return true;
        }

        return $details->user_id == user_id() && $details->user_type == user_type();
    }

    /**
     * Determine if the given user can create a details.
     *
     * @param UserPolicy $user
     * @param Details $details
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('cart.details.create');
    }

    /**
     * Determine if the given user can update the given details.
     *
     * @param UserPolicy $user
     * @param Details $details
     *
     * @return bool
     */
    public function update(UserPolicy $user, Details $details)
    {
        if ($user->canDo('cart.details.edit') && $user->isAdmin()) {
            return true;
        }

        return $details->user_id == user_id() && $details->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given details.
     *
     * @param UserPolicy $user
     * @param Details $details
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Details $details)
    {
        return $details->user_id == user_id() && $details->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given details.
     *
     * @param UserPolicy $user
     * @param Details $details
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Details $details)
    {
        if ($user->canDo('cart.details.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given details.
     *
     * @param UserPolicy $user
     * @param Details $details
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Details $details)
    {
        if ($user->canDo('cart.details.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
