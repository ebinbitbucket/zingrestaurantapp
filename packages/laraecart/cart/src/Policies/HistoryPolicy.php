<?php

namespace Laraecart\Cart\Policies;

use Litepie\User\Contracts\UserPolicy;
use Laraecart\Cart\Models\History;

class HistoryPolicy
{

    /**
     * Determine if the given user can view the history.
     *
     * @param UserPolicy $user
     * @param History $history
     *
     * @return bool
     */
    public function view(UserPolicy $user, History $history)
    {
        if ($user->canDo('cart.history.view') && $user->isAdmin()) {
            return true;
        }

        return $history->user_id == user_id() && $history->user_type == user_type();
    }

    /**
     * Determine if the given user can create a history.
     *
     * @param UserPolicy $user
     * @param History $history
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('cart.history.create');
    }

    /**
     * Determine if the given user can update the given history.
     *
     * @param UserPolicy $user
     * @param History $history
     *
     * @return bool
     */
    public function update(UserPolicy $user, History $history)
    {
        if ($user->canDo('cart.history.edit') && $user->isAdmin()) {
            return true;
        }

        return $history->user_id == user_id() && $history->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given history.
     *
     * @param UserPolicy $user
     * @param History $history
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, History $history)
    {
        return $history->user_id == user_id() && $history->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given history.
     *
     * @param UserPolicy $user
     * @param History $history
     *
     * @return bool
     */
    public function verify(UserPolicy $user, History $history)
    {
        if ($user->canDo('cart.history.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given history.
     *
     * @param UserPolicy $user
     * @param History $history
     *
     * @return bool
     */
    public function approve(UserPolicy $user, History $history)
    {
        if ($user->canDo('cart.history.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
