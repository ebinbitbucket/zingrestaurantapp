<?php

namespace Laraecart\Cart\Policies;

use Litepie\User\Contracts\UserPolicy;
use Laraecart\Cart\Models\Calls;

class CallsPolicy
{

    /**
     * Determine if the given user can view the calls.
     *
     * @param UserPolicy $user
     * @param Calls $calls
     *
     * @return bool
     */
    public function view(UserPolicy $user, Calls $calls)
    {
        if ($user->canDo('cart.calls.view') && $user->isAdmin()) {
            return true;
        }

        return $calls->user_id == user_id() && $calls->user_type == user_type();
    }

    /**
     * Determine if the given user can create a calls.
     *
     * @param UserPolicy $user
     * @param Calls $calls
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('cart.calls.create');
    }

    /**
     * Determine if the given user can update the given calls.
     *
     * @param UserPolicy $user
     * @param Calls $calls
     *
     * @return bool
     */
    public function update(UserPolicy $user, Calls $calls)
    {
        if ($user->canDo('cart.calls.edit') && $user->isAdmin()) {
            return true;
        }

        return $calls->user_id == user_id() && $calls->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given calls.
     *
     * @param UserPolicy $user
     * @param Calls $calls
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Calls $calls)
    {
        return $calls->user_id == user_id() && $calls->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given calls.
     *
     * @param UserPolicy $user
     * @param Calls $calls
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Calls $calls)
    {
        if ($user->canDo('cart.calls.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given calls.
     *
     * @param UserPolicy $user
     * @param Calls $calls
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Calls $calls)
    {
        if ($user->canDo('cart.calls.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
