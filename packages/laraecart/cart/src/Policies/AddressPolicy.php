<?php

namespace Laraecart\Cart\Policies;

use Litepie\User\Contracts\UserPolicy;
use Laraecart\Cart\Models\Address;

class AddressPolicy
{

    /**
     * Determine if the given user can view the address.
     *
     * @param UserPolicy $user
     * @param Address $address
     *
     * @return bool
     */
    public function view(UserPolicy $user, Address $address)
    {
        if ($user->canDo('cart.address.view') && $user->isAdmin()) {
            return true;
        }

        return $address->user_id == user_id() && $address->user_type == user_type();
    }

    /**
     * Determine if the given user can create a address.
     *
     * @param UserPolicy $user
     * @param Address $address
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('cart.address.create');
    }

    /**
     * Determine if the given user can update the given address.
     *
     * @param UserPolicy $user
     * @param Address $address
     *
     * @return bool
     */
    public function update(UserPolicy $user, Address $address)
    {
        if ($user->canDo('cart.address.edit') && $user->isAdmin()) {
            return true;
        }

        if($user->isClient()){
            return true;
        }

        return $address->user_id == user_id() && $address->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given address.
     *
     * @param UserPolicy $user
     * @param Address $address
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Address $address)
    {
        if($user->isClient()){
            return true;
        }
        return $address->user_id == user_id() && $address->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given address.
     *
     * @param UserPolicy $user
     * @param Address $address
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Address $address)
    {
        if ($user->canDo('cart.address.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given address.
     *
     * @param UserPolicy $user
     * @param Address $address
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Address $address)
    {
        if ($user->canDo('cart.address.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
