<?php

namespace Laraecart\Cart\Policies;

use Litepie\User\Contracts\UserPolicy;
use Laraecart\Cart\Models\Refund;


class RefundPolicy
{

    /**
     * Determine if the given user can view the refund.
     *
     * @param UserPolicy $user
     * @param Refund $refund
     *
     * @return bool
     */
    public function view(UserPolicy $user, Refund $refund)
    {
        if ($user->canDo('cart.refund.view') && $user->isAdmin()) {
            return true;
        }

        return $refund->user_id == user_id() && $refund->user_type == user_type();
    }

    /**
     * Determine if the given user can create a refund.
     *
     * @param UserPolicy $user
     * @param Refund $refund
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('cart.refund.create');
    }

    /**
     * Determine if the given user can update the given refund.
     *
     * @param UserPolicy $user
     * @param Refund $refund
     *
     * @return bool
     */
    public function update(UserPolicy $user, Refund $refund)
    {
        if ($user->canDo('cart.refund.edit') && $user->isAdmin()) {
            return true;
        }

        return $refund->user_id == user_id() && $refund->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given refund.
     *
     * @param UserPolicy $user
     * @param Refund $refund
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Refund $refund)
    {
        return $refund->user_id == user_id() && $refund->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given refund.
     *
     * @param UserPolicy $user
     * @param Refund $refund
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Refund $refund)
    {
        if ($user->canDo('cart.refund.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given refund.
     *
     * @param UserPolicy $user
     * @param Refund $refund
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Refund $refund)
    {
        if ($user->canDo('cart.refund.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
