<?php

namespace Laraecart\Cart\Policies;

use Litepie\User\Contracts\UserPolicy;
use Laraecart\Cart\Models\Cart;

class CartPolicy
{

    /**
     * Determine if the given user can view the cart.
     *
     * @param UserPolicy $user
     * @param Cart $cart
     *
     * @return bool
     */
    public function view(UserPolicy $user, Cart $cart)
    {
        if ($user->canDo('cart.cart.view') && $user->isAdmin()) {
            return true;
        }

        return $cart->user_id == user_id() && $cart->user_type == user_type();
    }

    /**
     * Determine if the given user can create a cart.
     *
     * @param UserPolicy $user
     * @param Cart $cart
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('cart.cart.create');
    }

    /**
     * Determine if the given user can update the given cart.
     *
     * @param UserPolicy $user
     * @param Cart $cart
     *
     * @return bool
     */
    public function update(UserPolicy $user, Cart $cart)
    {
        if ($user->canDo('cart.cart.edit') && $user->isAdmin()) {
            return true;
        }

        return $cart->user_id == user_id() && $cart->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given cart.
     *
     * @param UserPolicy $user
     * @param Cart $cart
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Cart $cart)
    {
        return $cart->user_id == user_id() && $cart->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given cart.
     *
     * @param UserPolicy $user
     * @param Cart $cart
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Cart $cart)
    {
        if ($user->canDo('cart.cart.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given cart.
     *
     * @param UserPolicy $user
     * @param Cart $cart
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Cart $cart)
    {
        if ($user->canDo('cart.cart.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
