<?php

namespace Laraecart\Cart\Console\Commands;

use Illuminate\Console\Command;
use Laraecart\Cart\Models\Order;
use Carbon\Carbon;
use DB;
use Mail;

class SendReviews extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:review';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Feedback mail to customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    
        $time = date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s')) - (60 * 60));
         $orders = Order::where('order_status','Completed')->where('feedback_mail_status',0)->where('delivery_time', '<',$time)->get();
       
           foreach ($orders as $key => $order) {
        $order_detail       = $order->detail;
        $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id', $order->restaurant->id)->orderBy('day')->orderBy('opening')->get();
        $weekMap            = [
            0 => 'sun',
            1 => 'mon',
            2 => 'tue',
            3 => 'wed',
            4 => 'thu',
            5 => 'fri',
            6 => 'sat',
        ];
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        $weekday      = $weekMap[$dayOfTheWeek];
       
        $user   = $order->user;    
             Mail::send('cart::public.cart.feedback', ['order' => $order, 'order_detail' => $order->detail, 'user' => $user, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday], function ($message) use ($user) {
                $message->from('support@zingmyorder.com','ZingMyOrder');
                $message->to($user->email)->subject('Zing Order Feedback');
            });
          
        $order->update(['feedback_mail_status' => ++$order->feedback_mail_status]);
         }
        return;
       
    }
}
