<?php

namespace Laraecart\Cart\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the package.
     *
     * @var array
     */
    protected $policies = [
        // Bind Cart policy
        'Laraecart\Cart\Models\Cart' => \Laraecart\Cart\Policies\CartPolicy::class,
// Bind Address policy
        'Laraecart\Cart\Models\Address' => \Laraecart\Cart\Policies\AddressPolicy::class,
// Bind Order policy
        'Laraecart\Cart\Models\Order' => \Laraecart\Cart\Policies\OrderPolicy::class,
// Bind Details policy
        'Laraecart\Cart\Models\Details' => \Laraecart\Cart\Policies\DetailsPolicy::class,
// Bind History policy
        'Laraecart\Cart\Models\History' => \Laraecart\Cart\Policies\HistoryPolicy::class,
    ];

    /**
     * Register any package authentication / authorization services.
     *
     * @param \Illuminate\Contracts\Auth\Access\Gate $gate
     *
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);
    }
}
