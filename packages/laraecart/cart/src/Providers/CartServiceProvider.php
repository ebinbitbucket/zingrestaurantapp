<?php

namespace Laraecart\Cart\Providers;

use Illuminate\Support\ServiceProvider;
use Laraecart\Cart\Console\Commands\sendReviews;

class CartServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'cart');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'cart');

        // Load migrations
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        // Call pblish redources function
        $this->publishResources();
        if ($this->app->runningInConsole()) {
        $this->commands([
            SendReviews::class,
        ]);
    }

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();
        $this->registerCart();
        $this->registerFacade();
        $this->registerBindings();
        //$this->registerCommands();
    }


    /**
     * Register the application bindings.
     *
     * @return void
     */
    protected function registerCart()
    {
        $this->app->bind('cart', function($app) {
            return new Cart($app);
        });
    }

    /**
     * Register the vault facade without the user having to add it to the app.php file.
     *
     * @return void
     */
    public function registerFacade() {
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Cart', 'Laraecart\Cart\Facades\Cart');
        });
    }

    /**
     * Register bindings for the provider.
     *
     * @return void
     */
    public function registerBindings() {
        // Bind facade
        $this->app->bind('laraecart.cart', function ($app) {
            return $this->app->make('Laraecart\Cart\Cart');
        });

        // Bind Cart to repository
        $this->app->bind(
            'Laraecart\Cart\Interfaces\CartRepositoryInterface',
            \Laraecart\Cart\Repositories\Eloquent\CartRepository::class
        );
        // Bind Address to repository
        $this->app->bind(
            'Laraecart\Cart\Interfaces\AddressRepositoryInterface',
            \Laraecart\Cart\Repositories\Eloquent\AddressRepository::class
        );
        // Bind Order to repository
        $this->app->bind(
            'Laraecart\Cart\Interfaces\OrderRepositoryInterface',
            \Laraecart\Cart\Repositories\Eloquent\OrderRepository::class
        );
        // Bind Details to repository
        $this->app->bind(
            'Laraecart\Cart\Interfaces\DetailsRepositoryInterface',
            \Laraecart\Cart\Repositories\Eloquent\DetailsRepository::class
        );
        // Bind History to repository
        $this->app->bind(
            'Laraecart\Cart\Interfaces\HistoryRepositoryInterface',
            \Laraecart\Cart\Repositories\Eloquent\HistoryRepository::class
        );
        $this->app->bind(
            'Laraecart\Cart\Interfaces\RefundRepositoryInterface',
            \Laraecart\Cart\Repositories\Eloquent\RefundRepository::class
        );
        $this->app->bind(
            'Laraecart\Cart\Interfaces\CallsRepositoryInterface',
            \Laraecart\Cart\Repositories\Eloquent\CallsRepository::class
        );

        $this->app->register(\Laraecart\Cart\Providers\AuthServiceProvider::class);
        
        $this->app->register(\Laraecart\Cart\Providers\RouteServiceProvider::class);
            }

    /**
     * Merges user's and cart's configs.
     *
     * @return void
     */
    protected function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php', 'laraecart.cart'
        );
    }

    /**
     * Register scaffolding command
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\MakeCart::class,
            ]);
        }
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['laraecart.cart'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../config/config.php' => config_path('laraecart/cart.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../resources/views' => base_path('resources/views/vendor/cart')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../resources/lang' => base_path('resources/lang/vendor/cart')], 'lang');

        // Publish public files and assets.
        $this->publishes([__DIR__ . '/public/' => public_path('/')], 'public');
    }
}
