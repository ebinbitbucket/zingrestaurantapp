<?php

namespace Laraecart\Cart\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Laraecart\Cart\Models\Cart;
use Request;
use Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Laraecart\Cart\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param   \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot()
    {
        parent::boot();
        
        if (Request::is('*/cart/calls/*')) {
            Route::bind('calls', function ($refund) {
                $refundrepo = $this->app->make('Laraecart\Cart\Interfaces\CallsRepositoryInterface');
                return $refundrepo->findorNew($refund);
            });
        }
        if (Request::is('*/cart/refund/*')) {
            Route::bind('refund', function ($refund) {
                $refundrepo = $this->app->make('Laraecart\Cart\Interfaces\RefundRepositoryInterface');
                return $refundrepo->findorNew($refund);
            });
        }
        if (Request::is('*/cart/cart/*')) {
            Route::bind('cart', function ($cart) {
                $cartrepo = $this->app->make('Laraecart\Cart\Interfaces\CartRepositoryInterface');
                return $cartrepo->findorNew($cart);
            });
        }

        if (Request::is('*/cart/address/*')) {
            Route::bind('address', function ($address) {
                $addressrepo = $this->app->make('Laraecart\Cart\Interfaces\AddressRepositoryInterface');
                return $addressrepo->findorNew($address);
            });
        }

        if (Request::is('*/cart/order/*')) {
            Route::bind('order', function ($order) {
                $orderrepo = $this->app->make('Laraecart\Cart\Interfaces\OrderRepositoryInterface');
                return $orderrepo->findorNew($order);
            });
        }

        if (Request::is('*/cart/details/*')) {
            Route::bind('details', function ($details) {
                $detailsrepo = $this->app->make('Laraecart\Cart\Interfaces\DetailsRepositoryInterface');
                return $detailsrepo->findorNew($details);
            });
        }

        if (Request::is('*/cart/history/*')) {
            Route::bind('history', function ($history) {
                $historyrepo = $this->app->make('Laraecart\Cart\Interfaces\HistoryRepositoryInterface');
                return $historyrepo->findorNew($history);
            });
        }
    
    }

    /**
     * Define the routes for the package.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();
        $this->mapApiRoutes();

    }

    /**
     * Define the "web" routes for the package.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        if (request()->segment(1) == 'api' || request()->segment(2) == 'api') {
            return;
        }
        
        Route::group([
            'middleware' => 'web',
            'namespace'  => $this->namespace,
            'prefix'     => trans_setlocale(),
        ], function ($router) {
            require (__DIR__ . '/../../routes/web.php');
        });
    }

    protected function mapApiRoutes()
    {
        if (request()->segment(1) == 'web' || request()->segment(2) == 'web') {
            return;
        }
        
        Route::group([
            'middleware' => 'api',
            'namespace'  => $this->namespace,
            'prefix'     => trans_setlocale(),
        ], function ($router) {
            require (__DIR__ . '/../../routes/api.php');
        });
    }

}
