<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Laraecart\Cart\Http\Requests\DetailsRequest;
use Laraecart\Cart\Interfaces\DetailsRepositoryInterface;
use Laraecart\Cart\Models\Details;

/**
 * Resource controller class for details.
 */
class DetailsResourceController extends BaseController
{

    /**
     * Initialize details resource controller.
     *
     * @param type DetailsRepositoryInterface $details
     *
     * @return null
     */
    public function __construct(DetailsRepositoryInterface $details)
    {
        parent::__construct();
        $this->repository = $details;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Laraecart\Cart\Repositories\Criteria\DetailsResourceCriteria::class);
    }

    /**
     * Display a list of details.
     *
     * @return Response
     */
    public function index(DetailsRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Laraecart\Cart\Repositories\Presenter\DetailsPresenter::class)
                ->$function();
        }

        $details = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('cart::details.names'))
            ->view('cart::details.index', true)
            ->data(compact('details', 'view'))
            ->output();
    }

    /**
     * Display details.
     *
     * @param Request $request
     * @param Model   $details
     *
     * @return Response
     */
    public function show(DetailsRequest $request, Details $details)
    {

        if ($details->exists) {
            $view = 'cart::details.show';
        } else {
            $view = 'cart::details.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('cart::details.name'))
            ->data(compact('details'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new details.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(DetailsRequest $request)
    {

        $details = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('cart::details.name')) 
            ->view('cart::details.create', true) 
            ->data(compact('details'))
            ->output();
    }

    /**
     * Create new details.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(DetailsRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $details                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('cart::details.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/details/' . $details->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/cart/details'))
                ->redirect();
        }

    }

    /**
     * Show details for editing.
     *
     * @param Request $request
     * @param Model   $details
     *
     * @return Response
     */
    public function edit(DetailsRequest $request, Details $details)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('cart::details.name'))
            ->view('cart::details.edit', true)
            ->data(compact('details'))
            ->output();
    }

    /**
     * Update the details.
     *
     * @param Request $request
     * @param Model   $details
     *
     * @return Response
     */
    public function update(DetailsRequest $request, Details $details)
    {
        try {
            $attributes = $request->all();

            $details->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('cart::details.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/details/' . $details->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/details/' . $details->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the details.
     *
     * @param Model   $details
     *
     * @return Response
     */
    public function destroy(DetailsRequest $request, Details $details)
    {
        try {

            $details->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::details.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('cart/details/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/details/' . $details->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple details.
     *
     * @param Model   $details
     *
     * @return Response
     */
    public function delete(DetailsRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::details.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('cart/details'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/details'))
                ->redirect();
        }

    }

    /**
     * Restore deleted details.
     *
     * @param Model   $details
     *
     * @return Response
     */
    public function restore(DetailsRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('cart::details.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/cart/details'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/details/'))
                ->redirect();
        }

    }

}
