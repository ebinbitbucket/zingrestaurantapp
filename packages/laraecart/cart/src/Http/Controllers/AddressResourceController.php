<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Laraecart\Cart\Http\Requests\AddressRequest;
use Laraecart\Cart\Interfaces\AddressRepositoryInterface;
use Laraecart\Cart\Models\Address;
use Laraecart\Cart\Facades\Cart;
use Restaurant\Restaurant\Models\Restaurant;

/**
 * Resource controller class for address.
 */
class AddressResourceController extends BaseController
{

    /**
     * Initialize address resource controller.
     *
     * @param type AddressRepositoryInterface $address
     *
     * @return null
     */
    public function __construct(AddressRepositoryInterface $address)
    {
        parent::__construct();
        $this->repository = $address;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Laraecart\Cart\Repositories\Criteria\AddressResourceCriteria::class);
    }

    /**
     * Display a list of address.
     *
     * @return Response
     */
    public function index(AddressRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Laraecart\Cart\Repositories\Presenter\AddressPresenter::class)
                ->$function();
        }
        $addresses = $this->repository->findByField('type','delivery');

        return $this->response->setMetaTitle('Address')
            ->view('cart::address.index', true)
            ->data(compact('addresses', 'view'))
            ->output();
    }

    /**
     * Display address.
     *
     * @param Request $request
     * @param Model   $address
     *
     * @return Response
     */
    public function show(AddressRequest $request, Address $address)
    {

        if ($address->exists) {
            $view = 'cart::address.show';
        } else {
            $view = 'cart::address.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('cart::address.name'))
            ->data(compact('address'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new address.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(AddressRequest $request)
    {

        $address = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('cart::address.name')) 
            ->view('cart::address.create', true) 
            ->data(compact('address'))
            ->output();
    }

    /**
     * Create new address.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(AddressRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['client_id']   = user_id();
            $attributes['default_address']   = 1;
            if(empty($attributes['type'])){
                $type='delivery';
            }
            else{
                $type = $attributes['type'];
            }
            $title = Address::where('title','like',$attributes['title'].'%')
                                    ->where('type',$type)
                                    ->whereNull('deleted_at')
                                    ->where('client_id',user_id())
                                    ->orderBy('created_at','title','DESC')
                                    ->pluck('title')
                                    ->first();
            if($title != ''){                                    

                if(is_numeric($title[-1])){    
                

                    $title_append = $title[-1]+1;
                    $attributes['title'] =  $attributes['title'].$title_append;  
                }
                else{
                    $attributes['title'] = $attributes['title'].'1';
                }
                

   
            }
            // else{
            //     if(is_numeric($attributes['title'][-1])){
            //         $title_append = (substr(strrchr($title, "e"), 1))+1;
                    
            //     }
            //     else{
            //         $title_append = $attributes['title'];
            //     }

            //     $attributes['title'] =  $title_append;  
            // }
            $address                 = $this->repository->create($attributes); 
            if(empty($attributes['status'])){
                   foreach(Cart::content() as $cart){
                     $rest_id = $cart->options->restaurant;
                     $rest_data                             = Restaurant::find($rest_id);
                   }
                       
                return redirect(url('address/delivery/saved_address/'.$address->id.'/'.$rest_data->getRouteKey()));
               // $view = view('cart::public.cart.address.delivery.indexnew')->render(); 
               // return response()->json(['status' => 'Success', 'address' => $address->id, 'view' => $view]);
            }
            
            //  if(!empty($attributes['type']) && !empty($attributes['btn_add_address1'])){
            //     return response()->json(['status' => 'Success']);
            // }
            // if(!empty($attributes['btn_add_address'])){
            //     return redirect()->back();
            // }
           
           else{
            return $this->response->message(trans('messages.success.created', ['Module' => trans('cart::address.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/address'))
                ->redirect();
           }
            
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/cart/address'))
                ->redirect();
        }

    }

    /**
     * Show address for editing.
     *
     * @param Request $request
     * @param Model   $address
     *
     * @return Response
     */
    public function edit(AddressRequest $request, Address $address)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('cart::address.name'))
            ->view('cart::address.edit', true)
            ->data(compact('address'))
            ->output();
    }

    /**
     * Update the address.
     *
     * @param Request $request
     * @param Model   $address
     *
     * @return Response
     */
    public function update(AddressRequest $request, Address $address)
    {
        try {
            $attributes = $request->all();
            $attributes['client_id'] = user_id();
            $address->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('cart::address.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/address/'))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/address/' . $address->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the address.
     *
     * @param Model   $address
     *
     * @return Response
     */
    public function destroy(AddressRequest $request, Address $address)
    {
         try {

            $address->delete();
            $delivery_address = $this->repository->findByField('type','delivery');
            $delivery_id = @$delivery_address->last()->id;
            $billing_address = $this->repository->findByField('type','billing');
            $billing_id = @$billing_address->last()->id;
             // return response()->json(['status' => 'Success', 'billing_id' => $billing_id, 'delivery_id' => $delivery_id]);
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::address.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('cart/address'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/address/' . $address->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple address.
     *
     * @param Model   $address
     *
     * @return Response
     */
    public function delete(AddressRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::address.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('cart/address'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/address'))
                ->redirect();
        }

    }

    /**
     * Restore deleted addresses.
     *
     * @param Model   $address
     *
     * @return Response
     */
    public function restore(AddressRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('cart::address.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/cart/address'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/address/'))
                ->redirect();
        }

    }

       public function getUseraddress()
    { 
          // $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id',user_id())->orderBy('day')->get();
        return $this->response->setMetaTitle('User Address')
            ->view('restaurant::default.restaurant.user-address')
            // ->data(compact('restaurant_timings'))
            ->layout('default')
            ->output();
    }

     public function addUseraddress()
    { 
        return $this->response->setMetaTitle('User Address')
            ->view('restaurant::default.restaurant.user-address-add')
            // ->data(compact('restaurant_timings'))
            ->layout('default')
            ->output();
    }

       public function saved_address($id)
    { 
        $address = $this->repository->findByField('id',$id)->first();
        return $this->response->setMetaTitle('User Address')
            ->view('restaurant::default.restaurant.saved_address_checkout')
            ->data(compact('address'))
            ->layout('default')
            ->output();
        
    }
    
      public function saved_billing_address($id)
    { 
        $address = $this->repository->findByField('id',$id)->first();
        return $this->response->setMetaTitle('User Address')
            ->view('restaurant::default.restaurant.saved_billing_address_checkout')
            ->data(compact('address'))
            ->layout('default')
            ->output();
        
    }
    
    public function saveUseraddress(AddressRequest $request)
    { 
        $attributes = $request->all();
        dd($request);
    }

        public function getBillingAddress(AddressRequest $request)
    {
       $view=[];
        $addresses = $this->repository->findByField('type','billing');

        return $this->response->setMetaTitle('Address')
            ->view('cart::address.index_billing', true)
            ->data(compact('addresses', 'view'))
            ->output();
    }

      public function addBillingaddress()
    { 
        return $this->response->setMetaTitle('User Address')
            ->view('restaurant::default.restaurant.billing-address-add')
            // ->data(compact('restaurant_timings'))
            ->layout('default')
            ->output();
    }

      public function editBillingAddress($id)
    {
        $address = Address::where('id',hashids_decode($id))->first();
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('cart::address.name'))
            ->view('cart::address.edit_billing', true)
            ->data(compact('address'))
            ->output();
    }

}
