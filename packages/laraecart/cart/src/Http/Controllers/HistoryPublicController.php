<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Laraecart\Cart\Interfaces\HistoryRepositoryInterface;

class HistoryPublicController extends BaseController
{
    // use HistoryWorkflow;

    /**
     * Constructor.
     *
     * @param type \Laraecart\History\Interfaces\HistoryRepositoryInterface $history
     *
     * @return type
     */
    public function __construct(HistoryRepositoryInterface $history)
    {
        $this->repository = $history;
        parent::__construct();
    }

    /**
     * Show history's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $histories = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$cart::history.names'))
            ->view('cart::history.index')
            ->data(compact('histories'))
            ->output();
    }


    /**
     * Show history.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $history = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$history->name . trans('cart::history.name'))
            ->view('cart::history.show')
            ->data(compact('history'))
            ->output();
    }

}
