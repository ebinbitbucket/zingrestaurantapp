<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use App\User;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use Laraecart\Cart\Facades\Cart;
use Laraecart\Cart\Interfaces\AddressRepositoryInterface;
use Laraecart\Cart\Interfaces\CartRepositoryInterface;
use Laraecart\Cart\Interfaces\DetailsRepositoryInterface;
use Laraecart\Cart\Interfaces\OrderRepositoryInterface;
use Laraecart\Cart\Models\Address;
use Laraecart\Cart\Models\Order;
use Laraecart\Cart\Models\TransactionLogs;
use Mail;
use Nuvei\XmlAuthRequest;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalCheckout\PayPalClient;
use Request as Requests;
use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
use Restaurant\Restaurant\Models\Restaurant;
use Restaurant\Restaurant\Models\Restauranttimings;
use Session;
use Exception;
use Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface;

class CartnewPublicController extends BaseController
{

// use CartWorkflow;

    /**
     * Constructor.
     *
     * @param type \Laraecart\Cart\Interfaces\CartRepositoryInterface $cart
     *
     * @return type
     */
    public function __construct(CartRepositoryInterface $cart, MenuRepositoryInterface $menu, OrderRepositoryInterface $order, DetailsRepositoryInterface $detail, AddressRepositoryInterface $address,KitchenRepositoryInterface $kitchen)
    {

        if (!empty(app('auth')->getDefaultDriver())) {

            $this->middleware('auth:' . app('auth')->getDefaultDriver(), ['only' => ['getCheckout']]);
        }

        $this->repository = $cart;
        $this->menu       = $menu;
        $this->order      = $order;
        $this->detail     = $detail;
        $this->address    = $address;
        $this->kitchen    = $kitchen;
        parent::__construct();
    }

    public function getCheckout()
    {
        $address=$timings=[];
        $this->response->theme->asset()->container('extra')->usepath()->add('checkoutview', 'js/checkoutview.js');
        $carts = Cart::content();

        $weekday = strtolower(date('D'));

        if (Cart::count() > 0) {

            foreach ($carts as $cart) {
                $rest_id    = $cart->options->restaurant;
                $cart_id    = $cart->rowId;
                $restaurant = Restaurant::where('id', $rest_id)->first();
               // dd($restaurant);
            }

            Session::put('url.intended', url('/cart/checkout'));

            $timings = $restaurant->timings->where('day', $weekday);
            if (Auth::user()) {
                $exists = TransactionLogs::where('cart_id', $cart_id)->where('type', 'cart')->count();
                // if($exists ==0){
                $attributes['user_id']       = user_id();
                // $attributes['type']          = 'cart';
                $attributes['type']          = 'checkout';
                $attributes['total_amount']  = Cart::total();
                $attributes['json_data']     = Cart::content();
                $attributes['cart_id']       = $cart_id;
                $attributes['ip_address']    = request()->ip();
                $attributes['date']          = date('Y-m-d H:i:s');
                $attributes['url']           = Requests::path();
                $attributes['restaurant_id'] = $rest_id;
                $attributes['server_variables'] = Requests::server();
                $attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
                $useragent                   = $_SERVER['HTTP_USER_AGENT'];

                if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                    $attributes['source'] = 'mobile';
                } else {
                    $attributes['source'] = 'desktop';
                }
                
                 $log = TransactionLogs::updateOrCreate(
                    [
                        // 'user'=>$attributes['user_id'] ,
                        'date'=>date('Y-m-d',strtotime($attributes['date'])),
                        'type'=>$attributes['type'],
                        'ip_address'=>$attributes['ip_address'],
                        'restaurant_id'=>$attributes['restaurant_id'],
                        
                    ],[
                        'user_id'=>$attributes['user_id'],
                        'type'=>$attributes['type'],
                        'total_amount'=>$attributes['total_amount'],
				        'cart_id'=>$attributes['cart_id'],
				        'json_data'=>$attributes['json_data'],
                        'ip_address'=>$attributes['ip_address'],
                        'date'=>date('Y-m-d',strtotime($attributes['date'])),
                        'restaurant_id'=>$attributes['restaurant_id'],
                        'url'=>$attributes['url'],
                        'server_variables'=>$attributes['server_variables'],
                        'HTTP_REFERER'=>$attributes['HTTP_REFERER'],
                        'source'=>$attributes['source'],
                    ]);
                    
                //$log = TransactionLogs::create($attributes);
                // }

            }
            if(empty(user()->mobile)){
                $view = 'cart::public.cart.checkout.checkout_mobile';
 
            }else{
                $view = 'cart::public.cart.checkout.checkout';
            }

        } else {
            $view = 'cart::public.cart.checkout.emptycheckout';
        }

        return $this->response->setMetaTitle('Checkout ZingMyOrder')
            ->view($view)
            ->layout('blankcheckout')
            ->data(compact('carts', 'address', 'timings'))
            ->output();
    }

    public function add($rowId)
    {
        Cart::increment($rowId, $qty = 1);
        return view('cart::public.cart.checkout.updated_cart');
    }

    public function subtract($rowId)
    {
        Cart::decrement($rowId, $qty = 1);
        return view('cart::public.cart.checkout.updated_cart');
    }

    
    public function paypalSubmit(Request $request)
    {

        $orderId  = $request->get('orderId');
        $amount   = $request->get('amount');
        $currency = $request->get('currency');
        $client   = PayPalClient::client();
        $response = $client->execute(new OrdersGetRequest($orderId));

// print "Status Code: {$response->statusCode}\n";

// print "Status: {$response->result->status}\n";

// print "Order ID: {$response->result->id}\n";

// print "Intent: {$response->result->intent}\n";

// print "Links:\n";
        foreach ($response->result->links as $link) {
            // print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
        }

// 4. Save the transaction in your database. Implement logic to save transaction to your database for future reference.

// print "Gross Amount: {$response->result->purchase_units[0]->amount->currency_code} {$response->result->purchase_units[0]->amount->value}\n";
        if ($response->result->status == 'COMPLETED') {
            parse_str($request->get('data'), $data);
            $req = $request->all();
            $req = array_merge($req, $data);

            $order_save  = $this->repository->orderSave($req);
            $order_mails = $this->repository->orderMail($order_save, $request);
            return response()->json(['Status' => $response->result->status, 'order_id' => $order_save->getRoutekey()]);
        } else {
            $payment_response = $response->result->status;
            $msg              = 'Invalid Payment Details. ';

// if(!empty($payment_response->ssl_avs_response)){

//     $attributes['avs_code'] = $payment_response->ssl_avs_response;

//     $attributes['card'] = $payment_response->ssl_card_number;

//     $count = TransactionLogs::where('card',$payment_response->ssl_card_number)->where('date','>',date('Y-m-d H:i:s', strtotime('-1 day')))->count();

//     if($count >= 1){

//         $msg = $msg . "\nYou card was declined twice. Due to the security standards we abide by you will have to use another card to complete the order.";

//     }

//     else{

//         $msg = $msg . "\nKindly ensure the details are correct for your next try. If the same card number is declined again we can’t process it for another 24 hours.";

//     }

            // }
            $carts = Cart::content();

            foreach ($carts as $cart) {
                $rest_id    = $cart->options->restaurant;
                $cart_id    = $cart->rowId;
                $restaurant = Restaurant::where('id', $rest_id)->first();
            }

            $attributes['user_id']       = user_id();
            $attributes['type']          = 'card';
            $attributes['total_amount']  = Cart::total();
            $attributes['cart_id']       = $cart_id;
            $attributes['json_data']     = $payment_response;
            $attributes['ip_address']    = request()->ip();
            $attributes['date']          = date('Y-m-d H:i:s');
            $attributes['url']           = Requests::path();
            $attributes['restaurant_id'] = $rest_id;
            $useragent                   = $_SERVER['HTTP_USER_AGENT'];

            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                $attributes['source'] = 'mobile';
            } else {
                $attributes['source'] = 'desktop';
            }

            $log = TransactionLogs::create($attributes);
            return response()->json(['status' => 'error', 'msg' => $msg]);
        }

    }

public function nuviewSubmit(Request $request)
    {
        
        
        ignore_user_abort(true);
        $request = $request->all();
        
        if(substr($request['cc-exp'],'-4')<date('Y'))
        return response()->json(['status' => 'error', 'msg' => 'AN ERROR OCCURED! Your transaction was not processed.Error details:Invalid Expiry Date']);
        
         $time = date("Y-m-d H:i:s", strtotime('-3 minutes'));
        $checkOrders = Order::where('user_id',user_id())->where('created_at','>',$time)->count(); 
        if($checkOrders > 0){
            $msg="Your previous order is in progress. Please wait for few minutes to place another order.";
            return response()->json(['status' => 'error', 'msg' => $msg]);
        }
        # These values are specific to the cardholder.
        $cardNumber     = $request['card']; # This is the full PAN (card number) of the credit card OR the SecureCard Card Reference if using a SecureCard. It must be digits only (i.e. no spaces or other characters).
        $cardType       = 'Visa Credit'; # See our Integrator Guide for a list of valid Card Type parameters
        $cardExpiry     = str_replace('-20', '', $request['cc-exp']); # (if not using SecureCard) The 4 digit expiry date (MMYY).
        $cardHolderName = user()->name; # (if not using SecureCard) The full cardholders name, as it is displayed on the credit card.
        $cvv            = $request['cvv']; # (optional) 3 digit (4 for AMEX cards) security digit on the back of the card.
# (optional) Cardholders mobile phone number for sending of a receipt. Digits only, Include international prefix.

# These values are specific to the transaction.
        $orderId     = str_random(12); # This should be unique per transaction (12 character max).
        $amount      = number_format($request['total_amount'], 2); # This should include the decimal point.
        $isMailOrder = false;
# If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.
# These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
        $address1 = $request['zip_code']; # (optional) This is the first line of the cardholders address.
        $address2 = $request['zip_code']; # (optional) This is the second line of the cardholders address.
        $postcode = $request['zip_code']; # (optional) This is the cardholders post code.
       
# (optional) This is the cardholders home phone number.

# eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
# (optional) This is the currency conversion rate returned in the rate request.

# 3D Secure reference. Only include if you have verified 3D Secure throuugh the Nuvei MPI and received an MPIREF back.

        $multicur  = false; # This should be false unless instructed otherwise by Nuvei.

# (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.

# Set up the authorisation object
        $terminalId  = config('services.nuvei.TERMINAL_ID');
        $currency    = config('services.nuvei.currency');
        $gateway     = config('services.nuvei.gateway');
        $secret      = config('services.nuvei.SECRET_KEY');
        $testAccount = config('services.nuvei.testAccount');
        $adminEmail  = config('services.nuvei.adminEmail');
        $adminPhone  = config('services.nuvei.adminPhone');
        $auth        = new XmlAuthRequest($terminalId, $orderId, $currency, $amount, $cardNumber, $cardType);

        if ($cardType != "SECURECARD") {
            $auth->SetNonSecureCardCardInfo($cardExpiry, $cardHolderName);
        }

        if ($cvv != "") {
            $auth->SetCvv($cvv);
        }

      
        if ($address1 != "" && $address2 != "" && $postcode != "") {
            $auth->SetAvs($address1, $address2, $postcode);
        }

       
        
        if ($multicur) {
            $auth->SetMultiCur();
        }

        if ($isMailOrder) {
            $auth->SetMotoTrans();
        }
# Perform the online authorisation and read in the result
        $response = $auth->ProcessRequestToGateway($secret, $testAccount, $gateway);

        $expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() . $secret);
        // if ($expectedResponseHash == $response->Hash()) {
            if ($response->ResponseCode() == 'A') {
                 try{
                     
                     $carts = Cart::content();
       
                    foreach ($carts as $cart) {
                        $rest_id    = $cart->options->restaurant;
                        $cart_id    = $cart->rowId;
                        $restaurant = Restaurant::where('id', $rest_id)->first();
                    }
    
                    $attributes['user_id']       = user_id();
                    // $attributes['type']          = 'card';
                    $attributes['type']          = 'payment';
                    $attributes['ip_address']    = request()->ip();
                    $attributes['date']          = date('Y-m-d H:i:s');
                    $attributes['url']           = Requests::path();
                    $attributes['restaurant_id'] = $rest_id;
                    $attributes['server_variables'] = Requests::server();
                    $attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
                     $attributes['total_amount']  = Cart::total();
                    $useragent                   = $_SERVER['HTTP_USER_AGENT'];
    
                    if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                        $attributes['source'] = 'mobile';
                    } else {
                        $attributes['source'] = 'desktop';
                    }
    
                    $log = TransactionLogs::updateOrCreate(
                        [
                            'date'=>date('Y-m-d',strtotime($attributes['date'])),
                            'type'=>$attributes['type'],
                            'ip_address'=>$attributes['ip_address'],
                            'restaurant_id'=>$attributes['restaurant_id'],
                            
                        ],[
                            'user_id'=>$attributes['user_id'],
                            'type'=>$attributes['type'],                              
                          'total_amount' =>$attributes['total_amount'],
                            'ip_address'=>$attributes['ip_address'],
                            'date'=>date('Y-m-d',strtotime($attributes['date'])),
                            'restaurant_id'=>$attributes['restaurant_id'],
                            'url'=>$attributes['url'],
                            'server_variables'=>$attributes['server_variables'],
                            'HTTP_REFERER'=>$attributes['HTTP_REFERER'],
                            'source'=>$attributes['source'],
                        ]);
                        $request['json_data']= json_encode([
                        'orderId'=>$orderId,
                        'responseCode'=>$response->responseCode(),
                        'responseText'=>$response->responseText(),
                        'approvalCode'=>$response->approvalCode(),
                        'authorizedAmount'=>$response->authorizedAmount(),
                        'dateTime'=>$response->dateTime(),
                        'avsResponse'=>$response->avsResponse(),
                        'cvvResponse'=>$response->cvvResponse(),
                        'uniqueRef'=>$response->UniqueRef(),
                        'card'=> str_pad(substr($cardNumber, -4), strlen($cardNumber), '*', STR_PAD_LEFT) ,
                        'cardHolderName'=>$cardHolderName,
                        ]);
                    $order_save  = $this->repository->orderSave($request);
                    $order_mails = $this->repository->orderMail($order_save, $request);
                   // $this->orderKitchen($order_save->id);
                } catch(Exception $e){
                }
                Session::flash('successcheckout', 'Your Payment has been completed Successfully. Thank you for choosing Zing. Stay tuned for an email or text message confirming the order as soon as we hear from the Restaurant.'); 
                return response()->json(['status' => 'APPROVAL', 'order' => $order_save->getRoutekey(),'guest' => user()->guest]);
            } elseif ($response->IsError()) {
                $payment_response = $response->ErrorString();
                    $msg              = 'AN ERROR OCCURED! You transaction was not processed. Error details: ' . $response->ErrorString();
                    $carts = Cart::content();

                foreach ($carts as $cart) {
                    $rest_id    = $cart->options->restaurant;
                    $cart_id    = $cart->rowId;
                    $restaurant = Restaurant::where('id', $rest_id)->first();
                }

                $attributes['user_id']       = user_id();
                $attributes['type']          = 'card';
                $attributes['total_amount']  = Cart::total();
                // $attributes['cart_id']       = $cart_id;
                $attributes['json_data']     = $payment_response;
                $attributes['ip_address']    = request()->ip();
                $attributes['date']          = date('Y-m-d H:i:s');
                $attributes['url']           = Requests::path();
                $attributes['restaurant_id'] = $rest_id;
                 $attributes['server_variables'] = Requests::server();
                    $attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
                $useragent                   = $_SERVER['HTTP_USER_AGENT'];

                if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                    $attributes['source'] = 'mobile';
                } else {
                    $attributes['source'] = 'desktop';
                }

                 $log = TransactionLogs::updateOrCreate(
                    [
                        'date'=>date('Y-m-d',strtotime($attributes['date'])),
                        'type'=>$attributes['type'],
                        'ip_address'=>$attributes['ip_address'],
                        'restaurant_id'=>$attributes['restaurant_id'],
                        
                    ],[
                        'user_id'=>$attributes['user_id'],
                        'type'=>$attributes['type'],
                        'total_amount'=> $attributes['total_amount'],
                      //  'cart_id'=>$attributes['cart_id'],
                        'json_data'=>$attributes['json_data'],
                        'ip_address'=>$attributes['ip_address'],
                        'date'=>date('Y-m-d',strtotime($attributes['date'])),
                        'restaurant_id'=>$attributes['restaurant_id'],
                        'url'=>$attributes['url'],
                        'server_variables'=>$attributes['server_variables'],
                        'HTTP_REFERER'=>$attributes['HTTP_REFERER'],
                        'source'=>$attributes['source'],
                    ]);
                return response()->json(['status' => 'error', 'msg' => $msg]);
            } else{
                    $payment_response = $response->ResponseText();
                    $msg              = 'Invalid Payment Details. ';
                    $carts = Cart::content();

                foreach ($carts as $cart) {
                    $rest_id    = $cart->options->restaurant;
                    $cart_id    = $cart->rowId;
                    $restaurant = Restaurant::where('id', $rest_id)->first();
                }

                $attributes['user_id']       = user_id();
                $attributes['type']          = 'card';
                $attributes['total_amount']  = Cart::total();
                $attributes['cart_id']       = $cart_id;
                $attributes['json_data']     = $payment_response;
                $attributes['ip_address']    = request()->ip();
                $attributes['date']          = date('Y-m-d H:i:s');
                $attributes['url']           = Requests::path();
                $attributes['restaurant_id'] = $rest_id;
                 $attributes['server_variables'] = Requests::server();
                $attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
                $useragent                   = $_SERVER['HTTP_USER_AGENT'];

                if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                    $attributes['source'] = 'mobile';
                } else {
                    $attributes['source'] = 'desktop';
                }

                 $log = TransactionLogs::updateOrCreate(
                    [
                        'date'=>date('Y-m-d',strtotime($attributes['date'])),
                        'type'=>$attributes['type'],
                        'ip_address'=>$attributes['ip_address'],
                        'restaurant_id'=>$attributes['restaurant_id'],
                        
                    ],[
                        'user_id'=>$attributes['user_id'],
                        'type'=>$attributes['type'],
                        'total_amount'=> $attributes['total_amount'],
                       
                        'json_data'=>$attributes['json_data'],
                        'ip_address'=>$attributes['ip_address'],
                        'date'=>date('Y-m-d',strtotime($attributes['date'])),
                        'restaurant_id'=>$attributes['restaurant_id'],
                        'url'=>$attributes['url'],
                        'server_variables'=>$attributes['server_variables'],
                        'HTTP_REFERER'=>$attributes['HTTP_REFERER'],
                        'source'=>$attributes['source'],
                    ]);
                return response()->json(['status' => 'error', 'msg' => $msg]);
            }


        //  else {
        //     $uniqueReference = $response->UniqueRef();
        //     $payment_response = 'PAYMENT FAILED: INVALID RESPONSE HASH.';
        //         $msg              = 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact <a href="mailto:' . $adminEmail . '">' . $adminEmail . '</a> or call ' . $adminPhone . ' to clarify if you will get charged for this order.';
        //         $carts = Cart::content();

        //     foreach ($carts as $cart) {
        //         $rest_id    = $cart->options->restaurant;
        //         $cart_id    = $cart->rowId;
        //         $restaurant = Restaurant::where('id', $rest_id)->first();
        //     }

        //     $attributes['user_id']       = user_id();
        //     $attributes['type']          = 'card';
        //     $attributes['total_amount']  = Cart::total();
        //     $attributes['cart_id']       = $cart_id;
        //     $attributes['json_data']     = $payment_response;
        //     $attributes['ip_address']    = request()->ip();
        //     $attributes['date']          = date('Y-m-d H:i:s');
        //     $attributes['url']           = Requests::path();
        //     $attributes['restaurant_id'] = $rest_id;
        //     $useragent                   = $_SERVER['HTTP_USER_AGENT'];

        //     if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
        //         $attributes['source'] = 'mobile';
        //     } else {
        //         $attributes['source'] = 'desktop';
        //     }

        //     $log = TransactionLogs::create($attributes);
        //     return response()->json(['status' => 'error', 'msg' => $msg]);
        //     if (isset($uniqueReference)) {
        //         echo 'Please quote Nuvei Terminal ID: ' . $terminalId . ', and Unique Reference: ' . $uniqueReference . ' when mailing or calling.';
        //     }

        // }

    }
    public function orderKitchen($order_id){
        $order = $this->order->findOrder($order_id);
        $kitchens = $this->kitchen->findKitchensByRestaurent($order->restaurant_id);      
        $newOrderCount =  $this->order->getNewOrdersByRestaurant($order->restaurant_id);      

                //dd($kitchens);

        foreach($kitchens as $kitchen){
            $notfy ='Order From '.$order->name.',address:'.$order->address.',Delivery Time:'.$order->delivery_time;
            //  $token =  Admintkn::select('email','token')->where('id', 1)->get()->first();
             // echo'<pre>';print_r($notfy);die();
               $id=$kitchen->fcm;
               if($id==''){
                continue;
               }
               // $id="daWGK2SlSLa7bUKA21hPO5:APA91bGjYejYx3ddOhxJx_yzau2pCz6ZJibjTcTa6lGh--TUor1PyPzhwSfiHeAkstt7WaGlmKzZFvz8Nxf_Dp_XeIeBM3Nwb7O9xGEEGDJbnrBG89sj1kcXvq5_o4WDZcJmHVCFKNuX";
               $url = 'https://fcm.googleapis.com/fcm/send';
               $fields = array (
                   'to' =>  $id,
                   'data' => $message = array(
                       "message" =>'You Have New Notification',
                       "body" => $notfy,
                       "count" => $newOrderCount->count()

                   )
                //    'notification' => $message = array(
                //        "title" =>'You Have New Notification',
                //         "body" => $notfy
                //    )
               );
               $fields = json_encode ( $fields );
               $headers = array (
                   'Authorization: key=' . "AAAAgqaQ6u0:APA91bHgE0ikhOn86tA2d6Ky_b-xYnYBN_28946GR5GqhT6_igrobvHwDVJ0n2S-7PvQTB3HLgUoI0_IjH02D27hLYk9WB6Tf7wqiL04PsfsZ1_3It_39hamr6uqd2ufyhemaYCS9W32",
                   'Content-Type: application/json'
               );
               $ch = curl_init ();
               curl_setopt ( $ch, CURLOPT_URL, $url );
               curl_setopt ( $ch, CURLOPT_POST, true );
               curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
               curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
               curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
       
               $result = curl_exec ( $ch );
             //  echo $result;
               curl_close ( $ch );
        }

     

    }

    public function sendOrderNotifications(){

        $orders = $this->order->getNewOrders();
        if(count($orders)>=1){

            foreach($orders as $order){
                $this->orderKitchen($order->id);

            }

        }

     

    }

}
