<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Laraecart\Cart\Http\Requests\OrderRequest;
use Laraecart\Cart\Interfaces\OrderRepositoryInterface;
use Laraecart\Cart\Interfaces\DetailsRepositoryInterface;
use Laraecart\Cart\Models\Order;
use Restaurant\Kitchen\Models\Kitchen;
use Restaurant\Restaurant\Models\Restaurant;
use Illuminate\Http\Request;
use Laraecart\Cart\Facades\Cart;
use Restaurant\Restaurant\Models\Favourite;
use Auth;
use App\Notifications\CustomEmail;
use App\User;
use DB;
use Mail;
use Carbon\Carbon;
use PDF;
use DateTime;
use Session;
use Litepie\User\Models\Client;
use BandwidthLib;
use Laraecart\Cart\Interfaces\RefundRepositoryInterface;


/**
 * Resource controller class for order.
 */
class OrderResourceController extends BaseController
{

    /**
     * Initialize order resource controller.
     *
     * @param type OrderRepositoryInterface $order
     *
     * @return null
     */
    public function __construct(OrderRepositoryInterface $order, DetailsRepositoryInterface $detail,RefundRepositoryInterface $refund)
    { 
        parent::__construct();
        $this->repository = $order;
        $this->refund = $refund;
        $this->detail = $detail;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Laraecart\Cart\Repositories\Criteria\OrderResourceCriteria::class);
    }

    /**
     * Display a list of order.
     *
     * @return Response
     */
    public function index(OrderRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Laraecart\Cart\Repositories\Presenter\OrderPresenter::class)
                ->$function();
        }

        $orders = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('cart::order.names'))
            ->view('cart::order.index', true)
            ->data(compact('orders', 'view'))
            ->output();
    }

    /**
     * Display order.
     *
     * @param Request $request
     * @param Model   $order
     *
     * @return Response
     */
    public function show(OrderRequest $request, Order $order)
    {

        if ($order->exists) {
            $view = 'cart::order.show';
        } else {
            $view = 'cart::order.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('cart::order.name'))
            ->data(compact('order'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new order.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(OrderRequest $request)
    {

        $order = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('cart::order.name')) 
            ->view('cart::order.create', true) 
            ->data(compact('order'))
            ->output();
    }

    /**
     * Create new order.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(OrderRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $order                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('cart::order.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/order/' . $order->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/cart/order'))
                ->redirect();
        }

    }

    /**
     * Show order for editing.
     *
     * @param Request $request
     * @param Model   $order
     *
     * @return Response
     */
    public function edit(OrderRequest $request, Order $order)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('cart::order.name'))
            ->view('cart::order.edit', true)
            ->data(compact('order'))
            ->output();
    }

    /**
     * Update the order.
     *
     * @param Request $request
     * @param Model   $order
     *
     * @return Response
     */
    public function update(OrderRequest $request, Order $order)
    {
        try {
            $attributes = $request->all();

            $order->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('cart::order.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/order/' . $order->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/order/' . $order->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the order.
     *
     * @param Model   $order
     *
     * @return Response
     */
    public function destroy(OrderRequest $request, Order $order)
    {
        try {

            $order->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::order.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('cart/order/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/order/' . $order->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple order.
     *
     * @param Model   $order
     *
     * @return Response
     */
    public function delete(OrderRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::order.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('cart/order'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/order'))
                ->redirect();
        }

    }

    /**
     * Restore deleted orders.
     *
     * @param Model   $order
     *
     * @return Response
     */
    public function restore(OrderRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('cart::order.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/cart/order'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/order/'))
                ->redirect();
        }

    }

      public function orderdetails($id)
    {
        // $id = hashids_decode($id);
     //    $orders = $this->detail->findDetail($id);
        $id     = hashids_decode($id);
        $orders = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($id) {
                return $query->with('detail')->orderBy('id', 'DESC')
                    ->where('id', $id);
            })->first();
                   if(Auth::user()){
            $restaurant_fav = Favourite::where('favourite_id',$orders->id)->where('type','order')->where('user_id',user_id())->count();
        }
        else{
            $restaurant_fav = 0;
        }
       // dd($orders->detail);
         return $this->response->setMetaTitle('Order Detail')
            ->view('cart::default.order.cart')
            ->data(compact('orders','restaurant_fav'))
            ->output();
    }
    
     public function orderSuccess($id)
    {

// $id = hashids_decode($id);
        //    $orders = $this->detail->findDetail($id);
        $id     = hashids_decode($id);
        $orders = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($id) {
                return $query->orderBy('id', 'DESC')
                    ->where('id', $id);
            })->first();

        if (Auth::user()) {
            $restaurant_fav = Favourite::where('favourite_id', $orders->id)->where('type', 'order')->where('user_id', user_id())->count();
        } else {
            $restaurant_fav = 0;
        }

        return $this->response->setMetaTitle('Order Detail')
            ->view('cart::default.order.success')
            ->data(compact('orders', 'restaurant_fav'))
            ->output();
    }


      public function reorder($id)
    {
        // $id = hashids_decode($id);
     //    $orders = $this->detail->findDetail($id);
        $id     = hashids_decode($id);
        $orders = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($id) {
                return $query->orderBy('id', 'DESC')
                    ->where('id', $id);
            })->first();
    if($orders != null){
        $order_detail = $orders->detail;
       foreach (Cart::content() as $item) { 
            foreach ($item->options as $option) {
                $prev_restaurant = $option;
                break;
            }
        }
        if(!empty($prev_restaurant)){
            if($prev_restaurant != $orders->restaurant->id){
                Cart::destroy();
            }
        }
        
        foreach ($order_detail as $key => $value) {
            if($value->menu_addon == 'menu'){
                $name = $value->menu->name;
            }
            else{
                    $name = $value->addon->name;
            }
            $image = ( $value->menu['image'] != []) ? $value->menu['image'][0]['path'] : '';
            // Cart::add(['id' => $value->menu_id, 'name' => $name, 'qty' => $value->quantity, 'image' => 'image/sm/' . $image, 'price' => $value->price, 'options' => ['restaurant' => $orders->restaurant_id ,'menu_addon' => $value->menu_addon,'restaurantname' => $orders->restaurant->name,'restaurantphone' => $orders->restaurant->phone,'restaurantimage' => $orders->restaurant->logo,'delivery_type' => $orders->restaurant->delivery,'delivery_charge' => $orders->restaurant->delivery_charge,'preparation_time' => $value->menu->categories->preparation_time]]);
            Cart::add(['id' => $value->menu_id, 'name' => $name, 'qty' => $value->quantity, 'image' => 'image/sm/' . $image, 'price' => $value->unit_price, 'options' => ['restaurant' => $orders->restaurant_id, 'menu_addon' => $value->menu_addon, 'restaurantname' => $orders->restaurant->name, 'restaurantphone' => $orders->restaurant->phone, 'restaurantimage' => $orders->restaurant->logo, 'delivery_type' => $orders->restaurant->delivery, 'delivery_charge' => $orders->restaurant->delivery_charge, 'preparation_time' => @$value->menu->categories->preparation_time,'special_instr' => $value->special_instr,'addons' =>  $value->addons,'variation' =>  $value->variation]]);            
        }
    }
        //  return redirect(trans_url('cart/checkout'));
        return redirect(trans_url('restaurants/'.@$orders->restaurant->slug));
    }

    public function reorderDelivery($id,Request $request)
    { 

        $request = $request->all(); 
        $id     = hashids_decode($id); 
        $carts   = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($id) {
                return $query->orderBy('id', 'DESC')
                    ->where('id', $id);
            })->first();
        $carts_detail   = $this->detail
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($id,$request) {
                return $query->orderBy('id', 'DESC')
                    ->whereIn('menu_id', $request['menu_ids'])
                    ->where('order_id', $id);
            })->get();
        $order['name'] = $request['cus_name'];
        $order['phone'] = $request['phone'];
        $order['address'] = $request['address'];
        if($request['Delivery_time'] == 'on'){
            $order['delivery_time'] = date('y-m-d h:m:s');
            $order['order_status'] = 'New Orders';
        }
        else{
            $order['delivery_time'] = date('y-m-d h:m:s',strtotime($request['scheduled_time']));
            $order['order_status'] = 'Scheduled';

        }
        $order['subtotal'] = 0;
        foreach ($carts_detail as $key => $value) {
            $order['subtotal'] = $order['subtotal']+$value->price;
        }
          

        $order['total']           = $order['subtotal'];
        $order['tax']             = $carts->tax;
        // $order['payment_details'] = 'paypal';
        if($request['submit'] == 'success'){
            $order['payment_status'] = 'Paid';
        }
        else{
            $order['payment_status'] = 'Unpaid';
        }
               
            $order['restaurant_id'] = $carts->restaurant_id;
    $order = $this->repository->create($order);

    $field['order_id'] = $order->id;

        foreach ($carts_detail as $item) {
            $field['menu_id'] = $item->menu_id;

            // $product = $this->product->findProduct($item->id);
            // $field['product_code'] = $product->code;
            
            $field['quantity']   = $item->quantity;
            $field['unit_price'] = $item->unit_price;
            $field['menu_addon'] = $item->menu_addon;
                
            $field['price'] = $item->unit_price * $item->quantity;
            $field['user_type'] = user_type();
            $field['user_id'] = user_id();
            $detail         = $this->detail->create($field);
        }
        $orderDetail = $this->repository->findOrder($order->id);
        if($request['submit'] == 'success'){

            return redirect()->to(url('success/'.$order->id));
        }
        else{
            return redirect()->to(url('payment/fail'));
        }

      
        // if($orderDetail['payment_methods'] == 'paypal')
        // { 
           
        //     return redirect()->to('paypal/' . $order_id);
        // }else
        // {
        //     $v['payment_status'] = 'Paid';
        //     $v['order_status'] = 'Processing'; 
        //     $v = $this->order->updateOrder($v, $id);
        //     return redirect()->to(url('success/'.$order_id));
        // }

    }

     public function orderPayment($id)
    { 
         $id     = hashids_decode($id); 
        $order   = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($id) {
                return $query->orderBy('id', 'DESC')
                    ->where('id', $id);
            })->first();

        $order->update(['payment_status' => 'Paid', 'order_status' => 'In Progress' ]);
        return redirect()->back()->with('message','Payment Succesfull');
       
    }

    public function myorders($status)
    { 
        $restaurant_fav='';
       $orders =  $this->repository->filterOrders($status);
       if($status == 'all')
       {
        $head = 'All Orders';
       }
       elseif($status == 'neworders'){
            $head = 'New Orders';
       }
       elseif($status == 'scheduled'){
            $head = 'Scheduled Orders';
       }
       elseif($status == 'preparing'){
            $head = 'Preparing Orders';
       }
       elseif($status == 'pickedup'){
            $head = 'Picked Up Orders';
       }
       elseif($status == 'delivered'){
            $head = 'Delivered Orders';
       }
       elseif($status == 'cancelled'){
            $head = 'Cancelled Orders';
       }
       elseif($status == 'overdue'){
            $head = 'Overdued Orders';
       }
        elseif($status == 'currorders'){
            $head = 'Current Orders Orders';
       }
        elseif($status == 'pastorders'){
            $head = 'Past Orders';
       }
       elseif($status == 'favourites'){
            $head = 'Favorite Orders';
       }

        return $this->response->setMetaTitle($head)
            ->view('cart::default.order.myorders')
            ->data(compact('orders','status','restaurant_fav'))
            ->output();
    }



    public function restauarntOrders($status='',OrderRequest $request=NULL)
    { 
        if($status == 'new'){
            $orders = $this->repository->getRestaurantTodayOrders();
        }
        elseif($status == 'pastorders'){
            $orders = $this->repository->getRestaurantAllOrders();
        }
        else{
            $orders = $this->repository->getRestaurantTodayOrders();
        }
        foreach($orders as $order){
            $order->refund=$this->refund->refundByorder($order->id);

        }

        return $this->response->setMetaTitle(trans('cart::order.names'))
            ->view('cart::default.order.restaurant_orders')
            ->layout('default')
            ->data(compact('orders','status'))
            ->output();
    }

     public function showDetail($id)
    { 
       $order = $this->repository->findByField('id',hashids_decode($id))->first();
        return view('cart::default.order.show_detail_model',compact('order'));            
    }
    
    public function completePickup($id)
    { 
       $order = $this->repository->findByField('id',hashids_decode($id))->first();
        $order->update(['is_car_pickup' => 1]);
        return ['ststus'=>true];
       
    }

    public function statusUpdate(Request $request,$status,$id)
    { 
       // dd($request->preperation_time,$status,$id);
        $preperation_time = $request->preperation_time;
       $order = $this->repository->findByField('id',hashids_decode($id))->first();
       $order_detail = $order->detail;
       $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id',$order->restaurant->id)->orderBy('day')->orderBy('opening')->get();
              $weekMap = [
            0 => 'sun',
            1 => 'mon',
            2 => 'tue',
            3 => 'wed',
            4 => 'thu',
            5 => 'fri',
            6 => 'sat',
        ]; 
        $dayOfTheWeek = Carbon::now()->dayOfWeek;    
        $weekday = $weekMap[$dayOfTheWeek];        
//echo $status;
       if($status == 'cancelled')
       {
            $status = "Cancelled";
            $user = $order->user;
            Mail::send('cart::public.cart.cancelled', ['order' => $order,'order_detail' =>$order->detail], function ($message) use ($user,$order) {
               $message->from('support@zingmyorder.com','ZingMyOrder');
               $message->to('support@zingmyorder.com')->subject('Zing Order Cancellation');
           });

       } else if($status == 'preparing') {
            $status = "Preparing";
            $user = $order->user;
            //return view('cart::public.cart.preparing',compact('order','order_detail','user','restaurant_timings','weekMap','dayOfTheWeek','weekday'));
            Mail::send('cart::public.cart.preparing', ['order' => $order,'order_detail' => $order->detail, 'user' => $user, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday], function ($message) use ($user) {
               $message->from('support@zingmyorder.com','ZingMyOrder');
               $message->to($user->email)->subject('Zing Order is being Prepared');
           });
       } else if($status == 'accept') {
           if($preperation_time){
              $newtimestamp = strtotime($order->delivery_time .'+'.$preperation_time.' minute');
            $order->update(['ready_time' => date('Y-m-d H:i:s', $newtimestamp),'preparation_time'=>$preperation_time]);     

           }
            $status = "Completed";
            $user = $order->user;
            //return view('cart::public.cart.ack',compact('order','order_detail','user','restaurant_timings','weekMap','dayOfTheWeek','weekday'));
            Mail::send('cart::public.cart.ack', ['order' => $order,'order_detail' => $order->detail, 'user' => $user, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday], function ($message) use ($user) {
               $message->from('support@zingmyorder.com','ZingMyOrder');
               $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
               $message->to($user->email)->subject('Zing Order Acknowledgement');
           });
           $this->sendTextMessage($order,'acknowledgement');
       }
       else if($status == 'completed'){
            $status = "Completed";
       }
       else if($status == 'picked'){
            $status = "Picked up";
                
       }
       else if($status == 'delivered'){
            $status = "Delivered";
       }
       else if($status == 'ready_Pickup'){
        $user = $order->user;
         //return view('cart::public.cart.preparing',compact('order','order_detail','user','restaurant_timings','weekMap','dayOfTheWeek','weekday'));
         Mail::send('cart::public.cart.pickup', ['order' => $order,'order_detail' => $order->detail, 'user' => $user,'status' => $status, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday], function ($message) use ($user,$status) {
        $message->from('support@zingmyorder.com','ZingMyOrder');
        $message->to($user->email)->subject('Zing Order Ready For Pickup');
         });
         $order->update(['mail_sent' => $order->mail_sent+1]);     
         $this->sendTextMessage($order,'ready_for_pickup');

         return response()->json(['order_return' => 'email mail sented successfully']);


         //return ['order_status' => 'email mail sented successfully'];         

         // $user = new User();
         //     $user->email = $order->user->email;
         //     $user->notify(new CustomEmail('Order #'.$order->id.' Pickup', 'Your order No#'.$order->id.' is ready for pickup'));
    } else if($status == 'ready_Delivery'){

        $user = $order->user;
        //return view('cart::public.cart.preparing',compact('order','order_detail','user','restaurant_timings','weekMap','dayOfTheWeek','weekday'));
        Mail::send('cart::public.cart.delivery', ['order' => $order,'order_detail' => $order->detail, 'user' => $user,'status' => $status, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday], function ($message) use ($user,$status) {
       $message->from('support@zingmyorder.com','ZingMyOrder');
       $message->to($user->email)->subject('Zing Order Out For Delivery');
        });
        $order->update(['mail_sent' => $order->mail_sent+1]);     
        $this->sendTextMessage($order,'out_for_delivery');
        return response()->json(['order_return' => 'email mail sented successfully']);

    }

       $order   = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($id) {
                return $query->orderBy('id', 'DESC')
                    ->where('id', hashids_decode($id));
            })->first(); 


       $order->update(['order_status' => $status]);         

        return redirect()->back();
    }
    
    public function sendTextMessage($order,$type)
    {

        if($order->text_notification==1){
        $message="";
$config = new BandwidthLib\Configuration(
    array(
        'messagingBasicAuthUserName' => getenv('BANDWIDTH_USER'),
        'messagingBasicAuthPassword' => getenv('BANDWIDTH_PASSWORD'),
        'voiceBasicAuthUserName' => 'username',
        'voiceBasicAuthPassword' => 'password',
        'twoFactorAuthBasicAuthUserName' => 'username',
        'twoFactorAuthBasicAuthPassword' => 'password'
        
    ) 
);
if($type=='acknowledgement'){
if($order->order_type=='Pickup'){
      if($order->car_pickup==1){
        $url=trans_url('order/car-delivery/'.$order->getRoutekey());
        $message="Hope you're having a Zing day! Your food is now being prepared by ".$order->restaurant->name.". Current estimated Pickup Time is: ".date('h:i A', strtotime($order->ready_time)).". When you arrive at the restaurant click here ".$url." to let them know. Enjoy your meal.";
      }else{
        $message="Hope you're having a Zing day! Your food is now being prepared by ".$order->restaurant->name.". Current estimated Pickup Time is: ".date('h:i A', strtotime($order->ready_time)).". Enjoy your meal.";
      }
}else{
if($order->non_contact==1){
    $message="Hope you're having a Zing day! Your No-contact delivery order is now being prepared by ".$order->restaurant->name.". Current estimated Delivery Time is: ".date('h:i A', strtotime($order->ready_time)).". Enjoy your meal.";
      }else{
        $message="Hope you're having a Zing day! Your food is now being prepared by ".$order->restaurant->name.". Current estimated Pickup Time is: ".date('h:i A', strtotime($order->ready_time)).". Enjoy your meal.";
      }
}
}else if($type=='ready_for_pickup'){
    $message="Yay! Your food is Ready for Pick Up! We hope you enjoy your meal.";
}else if($type=='out_for_delivery'){
    $message="Yay! Your food is out for delivery! We hope you enjoy your meal.";
}else if($type=='time_extend'){
    $message="We are so sorry. There is an unexpected delay to your order. The current pick up time is now ".date('h:i A', strtotime($order->ready_time));
}
$ph_number = preg_replace("/[^0-9]/", "", $order->phone);

$bclient = new BandwidthLib\BandwidthClient($config);
$messagingClient = $bclient->getMessaging()->getClient();
//dd($messagingClient);
$messagingAccountId = getenv('BANDWIDTH_ID');
$body = new BandwidthLib\Messaging\Models\MessageRequest();
$body->from = "4692240641";
$body->to = array("$ph_number");
$body->applicationId = "9f5da62f-eb1f-4a53-ab1c-30ed21756eac";
$body->text = $message;
$response = $messagingClient->createMessage($messagingAccountId, $body);
    if($response->getResult()->id){
        
        return true;
    }
    return true;


        }
    }
    public function getClientPoints()
    {
        $sum = $this->repository->findByField('user_id',user_id())->sum('loyalty_points');
        $discount =  $this->repository->findByField('user_id',user_id())->sum('discount_points');
        $points = $sum-$discount;
       return $this->response->setMetaTitle('User Loyality Points')
            ->view('cart::default.order.clientpoints')
            ->data(compact('points'))
            ->layout('default')
            ->output();
    }
    
   public function addToFavourite($id,$status){
        $order = $this->repository->find($id);
        if($status == 'on'){
            Favourite::create(['user_id' => user_id(),'favourite_id' => $order->id,'type' => 'order']);
        }
        else{
            $favourite = Favourite::where('favourite_id',$order->id)->where('type','order')->first();
            $favourite->forceDelete();
        }
        return redirect()->back();
    }

    public function kitchen_orders($status=''){
        Session::forget('restaurant_stop');
        $restaurant_stop=false; 
        $kitchen = Kitchen::find(user_id());
        if(@$kitchen->restaurant->kitchen_view == 'advanced'){
            $view = 'cart::default.order.advanced_kitchen_orders';
            $orders = Order::where('restaurant_id' , @$kitchen->restaurant->id)->whereIn('order_status',['New orders','Completed'])->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->get();
        }
        else{
            $view = 'cart::default.order.kitchen_orders';
            if($status == 'new'){
                $orders = Order::where('restaurant_id' , @$kitchen->restaurant->id)->where('order_status','New orders')->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->get();
            }
            if($status == 'preparing'){
                $orders = Order::where('restaurant_id' , @$kitchen->restaurant->id)->where('order_status','Preparing')->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->get();
            }
            if($status == 'ready'){
                $orders = Order::where('restaurant_id' , @$kitchen->restaurant->id)->where('order_status','Ready for Pickup')->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->get();
            }
            if($status == 'delivery'){
                $orders = Order::where('restaurant_id' , @$kitchen->restaurant->id)->where('order_status','Out for Delivery')->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->where('payment_status','Paid')->orderBy('id','DESC')->get();
            }
            if($status == 'completed'){
                $orders = Order::where('restaurant_id' , @$kitchen->restaurant->id)->where('order_status','Completed')->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->get();
            }
            
        }
        $id = @$orders->first()->id;
        if ($kitchen->restaurant->stop_date < Carbon::now()||is_null($kitchen->restaurant->stop_date)) 
        {
            $restaurant_stop=false; 
        }
    else{
        $restaurant_stop=true; 
    }
    Session::put('restaurant_stop',$restaurant_stop);
    //dd(Session::get('restaurant_stop'));

       // dd($restaurant_stop);
        return $this->response->setMetaTitle(trans('cart::order.names'))
            ->view($view)
            ->data(compact('orders','status','id','restaurant_stop'))
            ->output();
        
    }

      public function KitchenshowDetail($id)
    { 
       $order = $this->repository->findByField('id',hashids_decode($id))->first();
        return $this->response->setMetaTitle(trans('cart::order.names').' Detail')
            ->view('cart::default.order.kitchen_orders_detail_model')
            ->data(compact('order'))
            ->output();
    }
    public function carDetail($id)
    { 
       $order = $this->repository->findByField('id',hashids_decode($id))->first();
        return $this->response->setMetaTitle(trans('cart::order.names').' Detail')
            ->view('cart::default.order.car_detail_model')
            ->data(compact('order'))
            ->output();
    }
    
      public function restauarntNewOrders()
    { 
        $orders = $this->repository->findByField(['restaurant_id'=>user_id(),'notification_status'=> 0, 'order_status'=>'New Orders', 'payment_status'=>'Paid']);
        if($orders->count()){
            $data = view('cart::default.order.restaurant_orders_new', compact('orders'))->render();
        }
        else{
            $data=  null;
        }
         Order::where('restaurant_id',user_id())->where('notification_status',0)->where('order_status','New Orders')->where('payment_status','Paid')->update(['notification_status' => 1]);
        return response()->json(['status' => 'Success', 'new_order_count' => $orders->count(), 'new_orders' => $data]);
       
    }
    
    public function kitchenOrdersApp($status = '')
    {
        $kitchen = Kitchen::find(user_id());

            $view   = 'cart::default.order.kitchen_orders_App';
            $orders = Order::where('restaurant_id', @$kitchen->restaurant->id)
                ->whereIn('order_status', ['New orders', 'Completed'])
                ->whereDate('delivery_time', '=', date('Y-m-d'))
                ->where('payment_status', 'Paid')
                ->orderBy('id', 'DESC')
                ->get();
        return $this->response->setMetaTitle(trans('cart::order.names'))
            ->view($view)
            ->layout('blank')
            ->data(compact('orders', 'status'))
            ->output();

    }

        public function restauarntNewOrdersUpdate()
    { 

        Order::where('restaurant_id',user_id())->where('notification_status',0)->where('order_status','New Orders')->where('payment_status','Paid')->update(['notification_status' => 1]);

        return response()->json(['status' => 'Success']);
       
    }

          public function restauarntScheduledOrdersUpdate()
    { 

        $time = date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))+ (60*60));
            $orders = Order::where('order_status','Scheduled')
                        ->where('delivery_time','<',$time)->where('payment_status','Paid')->update(['order_status' => 'New Orders']);

        return response()->json(['status' => 'Success']);
       
    }
    
          public function kitchenNewOrders()
    { 

        $kitchen = Kitchen::find(user_id());
        
        $kitchen->update(['last_accessed' => date('Y-m-d H:i:m')]);
        //Order::where('restaurant_id',@$kitchen->restaurant->id)->where('notification_status',0)->whereIn('order_status',['New orders','Preparing'])->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->update(['notification_status' => 1]);
       
       $total_new_order_count = Order::where('restaurant_id' , @$kitchen->restaurant->id)->where('order_status','New orders')->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->count();

       $carPickUp = Order::where('restaurant_id' , @$kitchen->restaurant->id)->where('is_car_pickup',0)->whereNotNull('car_details')->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->count();
       
   $audio = $kitchen->restaurant->audio_alert;
   if(@$kitchen->restaurant->kitchen_view == 'advanced'){
            $orders = Order::where('restaurant_id', @$kitchen->restaurant->id)->whereIn('order_status', ['New orders', 'Completed'])->whereDate('delivery_time', '=', date('Y-m-d'))->where('payment_status', 'Paid')->orderBy('id', 'DESC')->get();
            $data = view('cart::default.order.advanced_kitchen_orders_new', compact('orders'))->render();
        }
        else{
            $orders = Order::where('restaurant_id' , @$kitchen->restaurant->id)->whereIn('order_status',['New orders'])->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->get();
            $data = view('cart::default.order.kitchen_orders_new', compact('orders'))->render();
        }
         $id = @$orders->first()->id;
         if($total_new_order_count>0){
            return response()->json(['status' => 'Success', 'new_order_count' => $orders->count(), 'new_orders' => $data, 'audio' => $audio,'total_new_order_count' => $total_new_order_count, 'id' => $id]);
         }
         if($carPickUp>0){
            return response()->json(['status' => 'Success', 'car_order_count' => $carPickUp, 'new_orders' => $data, 'audio' => $audio,'total_new_order_count' => $total_new_order_count, 'id' => $id]);
         }
        return response()->json(['status' => 'Success', 'new_order_count' => $orders->count(), 'new_orders' => $data, 'audio' => $audio,'total_new_order_count' => $total_new_order_count, 'id' => $id]);
       
    }

    public function kitchenTimeUpdate($order_id,$time)
    {
        $order = $this->repository->findByField('id',hashids_decode($order_id))->first();
        $currentDeliveryTime=$order->ready_time;
        $newtimestamp = strtotime($order->ready_time.' + '.$time.' minute');
       // dd($currentDeliveryTime);
       Order::where('id',hashids_decode($order_id))->update(['ready_time'=>date('Y-m-d H:i:s',$newtimestamp)]);
        Order::where('id',hashids_decode($order_id))->update(['delay_min'=>$time]);
        $order = $this->repository->findByField('id', hashids_decode($order_id))->first();
        $user   = $order->user;
         $restaurant_timings = DB::table('restaurant_timings')->where('restaurant_id', $order->restaurant->id)->orderBy('day')->orderBy('opening')->get();
        $weekMap            = [
            0 => 'sun',
            1 => 'mon',
            2 => 'tue',
            3 => 'wed',
            4 => 'thu',
            5 => 'fri',
            6 => 'sat',
        ];
        $dayOfTheWeek = Carbon::now()->dayOfWeek;
        $weekday      = $weekMap[$dayOfTheWeek];
       
        
       Mail::send('cart::public.cart.timeextend', ['order' => $order,'user'=>$user,'order_detail' => $order->detail, 'user' => $user, 'restaurant_timings' => $restaurant_timings, 'weekMap' => $weekMap, 'dayOfTheWeek' => $dayOfTheWeek, 'weekday' => $weekday, 'newtimestamp' => $time,'currentDeliveryTime'=>$currentDeliveryTime], function ($message) use ($user) {
               $message->from('support@zingmyorder.com','ZingMyOrder');
               $message->to($user->email)->subject('Zing Order Delay');
           });
           $this->sendTextMessage($order,'time_extend');

        return redirect()->back();
    }

    
          public function kitchenScheduledOrdersUpdate()
    { 

        $time = date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))+ (60*60));
            $orders = Order::where('order_status','Scheduled')
                        ->where('delivery_time','<',$time)->where('payment_status','Paid')->update(['order_status' => 'New Orders']);

        return response()->json(['status' => 'Success']);
       
    }

     public function statusModel($id)
    { 
        
       $order = $this->repository->findByField('id',$id)->first();
        return view('cart::default.order.show_status_model',compact('order'));            
    }
    
    
    public function acceptModel($id)
    { 
        
       $order = $this->repository->findByField('id',hashids_decode($id))->first();
      // dd( $order);
        return view('cart::default.order.order_accept_model',compact('order'));            
    }
    
    public function statusRestaurantModel($id)
    { 
        
       $order = $this->repository->findByField('id',$id)->first();
        return view('cart::default.order.show_status_restaurant_model',compact('order'));            
    }
    public function refundRestaurantModel($id)
    { 
       
       // dd($id);
        
       $order = $this->repository->findByField('id',$id)->first();
        return view('cart::default.order.refund_restaurant_model',compact('order'));            
    }
    public function restauarntOrdersRefundSave(Request $request)
   { 
    $refunddetails =$request->all();
   // Session::put('location', $search['address'])
    $order = Order::where('id',$request->id)->first();

    if($order){
        unset($refunddetails['id']);
        $refunddetails['order_number']=$order->id;
        $refunddetails['resturant']=$order->restaurant->name;
        $refunddetails['email']=$order->email;
        $refunddetails['restaurant_id']=$order->restaurant->id;
                     //   dd($rest_data);
   
    $user=$order->user;
    if($order->restaurant->refund_email){
        $refund_email=$order->restaurant->refund_email;
    }else{
        $refund_email=$order->restaurant->email;
    }
    if(DB::table('order_refund')->insert($refunddetails)){
        Mail::send('cart::public.order.mail_refund', ['order' => $order,'user'=>$user ,'order_detail' => $order->detail,'refunddetails' => $refunddetails], function ($message) use ($user,$refunddetails,$refund_email) {
            $message->from('support@zingmyorder.com', 'ZingMyOrder');
            $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
            $message->to($refunddetails['email'],$refund_email)->subject('Zing Order Refund Details');
            
        });
        return $order;


    }
} else{
    echo 'no order found';die();
}

   
    
   }
    

    public function paymentHistory(){

        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Laraecart\Cart\Repositories\Presenter\OrderPresenter::class)
                ->$function();
        }

        $orders = Order::select('restaurants'.'.name','restaurants'.'.acc_date','restaurants'.'.tax_rate','restaurants'.'.CCR','restaurants'.'.CCF','restaurants'.'.ZR','restaurants'.'.ZF','restaurants'.'.ZC',DB::raw('sum(total) as Total'), DB::raw('count(orders.id) as count'), DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),'restaurants'.'.ac_no','restaurants'.'.IFSC','restaurants'.'.bank',
                            DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                        ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
                        ->whereDate('orders'.'.delivery_time','<=',date('Y-m-d'))
                        ->where('orders'.'.pay_to','Pending')
                        ->where('payment_status','Paid')
                        ->where('order_status', 'Completed')
                        ->where('restaurants.published','Published')
                        ->groupBy('restaurants'.'.name','restaurants'.'.ac_no','restaurants'.'.acc_date','restaurants'.'.IFSC','restaurants'.'.bank','restaurants'.'.tax_rate','restaurants'.'.CCR','restaurants'.'.CCF','restaurants'.'.ZR','restaurants'.'.ZF','restaurants'.'.ZC')->get();
        return $this->response->setMetaTitle(trans('cart::order.names'))
            ->view('cart::order.payments', true)
            ->data(compact('orders', 'view'))
            ->output();
    }

     public function paymentFilterHistory(OrderRequest $request){

        $request = $request->all();   
       
        if(empty($request['filter_date1'])){
            $request['filter_date1'] = date('Y-m-d H:i:m', strtotime('1970-01-01 00:01:00'));
        }
        if(empty($request['filter_date2'])){
            $request['filter_date2'] = date('Y-m-d H:i:m');
        } 
        $dates = [date('Y-m-d',strtotime($request['filter_date1'])), date('Y-m-d',strtotime($request['filter_date2']))];

        $orders = Order::select('restaurants'.'.name','restaurants'.'.tax_rate','restaurants'.'.CCR','restaurants'.'.CCF','restaurants'.'.ZR',
        'restaurants'.'.ZF','restaurants'.'.ZC',DB::raw('sum(total) as Total'),DB::raw('count(orders.id) as count'),DB::raw('sum(tax-orders.ZC) as order_tax'),
        DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),
        DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),
        'restaurants'.'.ac_no','restaurants'.'.IFSC','restaurants'.'.bank',
                            DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                        ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
                        ->whereBetween(DB::raw('DATE(orders.delivery_time)'), $dates)
                        ->where('orders'.'.pay_to','Pending')
                        ->where('payment_status','Paid')
                        ->where('order_status', 'Completed')
                        ->where('restaurants.published',$request['filter_published'])
                        ->groupBy('restaurants'.'.name','restaurants'.'.ac_no','restaurants'.'.IFSC','restaurants'.'.bank','restaurants'.'.tax_rate','restaurants'.'.CCR','restaurants'.'.CCF','restaurants'.'.ZR','restaurants'.'.ZF','restaurants'.'.ZC')->get();

       $data = view('cart::admin.order.payment_filter', compact('orders'))->render();

        return response()->json(['status' => 'Success','view' => $data]);
    }
       public function downloadCSV(OrderRequest $request,$slug,$filter = [])
    { 
        empty($filter) ? '' : parse_str($filter, $filter);
           if (!empty($request->get('filter_date1'))) {
            $filters['filter_date1']     =  $request->get('filter_date1');    
        }
        if (!empty($request->get('filter_date2'))) {
            $filters['filter_date2']  = $request->get('filter_date2');   
        }  
        else {
            $filters     =  [];
        }     
               
        

        
        $assets     =  $this->repository->getAjax($filters);
        $fields    = ['name' => 'Restaurant','Total' => 'Amount','order_tax' => 'Tax','order_delivery' => 'Delivery','order_CCR' => 'CCR','order_CCF' => 'CCF','order_ZR' => 'ZR','order_ZF' => 'ZF','order_ZC' => 'ZC','order_tip' => 'Tip','EateryAmount' => 'Eatery Amount Due','ac_no' => 'Account No','acc_date' => 'Account Added','IFSC'=>'Routing No','IFSC'=>'Routing No','bank' => 'Bank'];        
        $header = implode(',' , $fields);
        $body   = '';
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=payments.csv');

        foreach($assets as $key => $asset){ 
            $row        = array_only($asset, array_keys($fields));     

            foreach ($fields as $hk => $hv) {
               
                
                if ($hk == 'clinic_id') {                
                    $body   .= "\"" . @$asset['clinic']['name']. "\",";
                    continue;
                }
                if ($hk == 'department_id') {                
                    $body   .= "\"" . @$asset['department']['name']. "\",";
                    continue;
                }
                if ($hk == 'doctor') {                
                    $body   .= "\"" . @$asset['doctor']['name']. "\",";
                    continue;
                }
                
               

                $body   .= "\"" . @$row[$hk] . "\",";
            }
            $body   .= "\n";
        }
        echo $header . "\n";
        echo $body . "\n";
    }

        public function paymentStatusUpdate(OrderRequest $request,$status){

        $request = $request->all(); 
        if(empty($request['filter_date1']) || empty($request['filter_date2'])){
            if($status == 'processing'){
                $orders = Order::whereDate('orders'.'.delivery_time','<=',date('Y-m-d',strtotime(date('Y-m-d'))))
                        ->where('orders'.'.pay_to','Pending')->where('payment_status','Paid')->where('payment_status','Paid')->update(['pay_to' => 'Processing']);
            }
            else{
                $orders = Order::whereDate('orders'.'.delivery_time','<=',date('Y-m-d',strtotime(date('Y-m-d'))))
                        ->where('orders'.'.pay_to','Processing')->where('payment_status','Paid')->update(['pay_to' => 'Paid']);
            }

        }
        else{
            if($status == 'processing'){
                $orders = Order::whereBetween(DB::raw('DATE(orders.delivery_time)'), array(date('Y-m-d',strtotime($request['filter_date1'])), date('Y-m-d',strtotime($request['filter_date2']))))
                        // ->whereDate('orders'.'.delivery_time','<=',date('Y-m-d',strtotime($request['filter_date1'])))
                                ->where('orders'.'.pay_to','Pending')->where('payment_status','Paid')->update(['pay_to' => 'Processing']);
            }
            else{
                $orders = Order::whereBetween(DB::raw('DATE(orders.delivery_time)'), array(date('Y-m-d',strtotime($request['filter_date1'])), date('Y-m-d',strtotime($request['filter_date2']))))
                        // ->whereDate('orders'.'.delivery_time','<=',date('Y-m-d',strtotime($request['filter_date2'])))
                        ->where('orders'.'.pay_to','Processing')->where('payment_status','Paid')->update(['pay_to' => 'Paid']);
            }
        }
        
        
        return redirect()->back();
    }

    public function orderPrint($id)
    { 
        $kitchen = Kitchen::where('id',user_id())->first();
        $data_bind = json_decode(file_get_contents("http://api.memobird.cn/home/setuserbind?ak=a281e01078f44ccfa2ce31b17e92ad30&timestamp=2014-11-
14%2014:22:39&memobirdID=".@$kitchen->restaurant->memobirdID));
        Restaurant::where('id',@$kitchen->restaurant->id)->update(['printer_userID' => $data_bind->showapi_userid,'memobirdID' => @$kitchen->restaurant->memobirdID,'ak' => 'a281e01078f44ccfa2ce31b17e92ad30']);

       $order = $this->repository->findByField('id',hashids_decode($id))->first();
       
       $restaurant = Restaurant::where('id',@$kitchen->restaurant->id)->first();
       //return view('cart::default.order.orderprint',compact('order'));
        $data =  view('cart::default.order.orderprint',compact('order'))->render(); 
// return $data;
        $print_url = urlencode(base64_encode($data));
        // $print_result = file_get_contents("http://api.memobird.cn/home/printpaperFromHtml?ak=a281e01078f44ccfa2ce31b17e92ad30&timestamp=2014-11-14%2014:22:39&printHtml=".$print_url."&memobirdID=8a2a36e114c81913&userID=640435"); 

        $ch = curl_init();
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,'http://api.memobird.cn/home/printpaperFromHtml');
curl_setopt($ch, CURLOPT_POST, FALSE);
curl_setopt($ch, CURLOPT_POSTFIELDS,"ak=".$restaurant->ak."&timestamp=2014:22:39&printHtml=".$print_url."&memobirdID=".$restaurant->memobirdID."&userID=".$restaurant->printer_userID);

// Execute
$result=curl_exec($ch);
// Closing
curl_close($ch);
// Will dump a beauty json :3
$result = json_decode($result); 
// dd($result);
if($result->showapi_res_error == 'ok'){
    return $this->response->message('Printing successfuly completed.')
                ->code(202)
                ->url('kitchen/cart/orders/kitchen_orders/new')
                ->redirect();
} 
else{
        return $this->response->message('Prinitng Failed.')
                                            ->code(400)
                                            ->url('/kitchen/cart/orders/kitchen_orders/new')
                                            ->redirect();
        
    }

}
  public function monthStmnt(){

        $orders = Order::select('clients'.'.name',DB::raw('DATE(delivery_time) as delivery_date'),DB::raw('sum(total) as Total'),DB::raw('sum(subtotal) as order_subtotal'),DB::raw('sum(orders.tax) as order_tax'),DB::raw('sum(tip) as order_tip'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.CCR+orders.CCF) as order_CCRF'),
                            DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                        ->join('clients','orders'.'.user_id','clients'.'.id')
                        ->whereDate('orders'.'.delivery_time','<=',date('Y-m-d'))
                        ->where('payment_status','Paid')
                        ->where('order_status','<>','Cancelled')
                        ->where('restaurant_id',user_id())
                        ->groupBy('orders'.'.user_id','clients'.'.name','delivery_date')
                        ->orderBy('delivery_date','DESC')->get();
        return $this->response->setMetaTitle("Monthly Statment")
             ->view('cart::default.order.monthly_stmnt')
            ->data(compact('orders'))
            ->output();
    }
    public function monthStmntFilter(Request $request){
         if(empty($request['filter_date1'])){
            $request['filter_date1'] = 'all';
        }
        if(empty($request['filter_date2'])){
            $request['filter_date2'] = 'all';
        }
        $orders = Order::select('clients'.'.name',DB::raw('DATE(delivery_time) as delivery_date'),DB::raw('sum(total) as Total'),DB::raw('sum(subtotal) as order_subtotal'),DB::raw('sum(orders.tax) as order_tax'),DB::raw('sum(tip) as order_tip'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.CCR+orders.CCF) as order_CCRF'),
                            DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                        ->join('clients','orders'.'.user_id','clients'.'.id')
                        // ->whereBetween(DB::raw('DATE(orders.delivery_time)'), array(date('Y-m-d',strtotime($request['filter_date1'])), date('Y-m-d',strtotime($request['filter_date2']))))
                        ->where(function($query) use ($request)
{
   
        $valueMonth = $request['filter_date1'];
        $valueYear = $request['filter_date2'];
        if ( $valueMonth != 'all') $query->whereMonth('orders.delivery_time', $request['filter_date1']);
        if ( $valueYear!= 'all') $query->whereYear('orders.delivery_time', $request['filter_date2']);
   
})
                        // ->whereYear('orders.delivery_time', $request['filter_date2'])
                        // ->whereMonth('orders.delivery_time', $request['filter_date1'])
                        ->where('payment_status','Paid')
                        ->where('order_status','<>','Cancelled')
                        ->where('restaurant_id',user_id())
                        ->groupBy('orders'.'.user_id','clients'.'.name','delivery_date')
                        ->orderBy('delivery_date','DESC')->get();

        return view('cart::default.order.monthly_stmnt_filter',compact('orders'));
    }

    public function monthStmntFilterNew(Request $request){
        $request = $request->all();
        if(empty($request['filter_date1'])){
            $request['filter_date1'] = date('Y-m-d');
        }else{
            $request['filter_date1'] = date('Y-m-d', strtotime($request['filter_date1']));
        }
        if(empty($request['filter_date2'])){
            $request['filter_date2'] = date('Y-m-d', strtotime(date('Y-m-d') .'+1 days'));
        }else{
            $request['filter_date2'] = date('Y-m-d', strtotime($request['filter_date2'] .'+1 days'));
        }
        $orders = Order::select('orders'.'.id','clients'.'.name',DB::raw('DATE(delivery_time) as delivery_date'),DB::raw('orders.subtotal as order_subtotal'),DB::raw('(orders.subtotal*restaurants.tax_rate/100) as order_tax'),DB::raw('orders.delivery_charge as delivery_charge'), DB::raw('orders.min_order_difference as extra_payment'), DB::raw('orders.ZF as order_ZF'),DB::raw('orders.ZR as order_ZR'),DB::raw('orders.ZC as order_ZC'),DB::raw('orders.CCR/100 * orders.total as order_CCR'),DB::raw('orders.CCF as order_CCF'),DB::raw('orders.CCR/100 * orders.total+orders.CCF as order_CCRF'),
                            DB::raw('(total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0)) as EateryAmount'),DB::raw('orders.total as order_total'),DB::raw('tip as order_tip'), DB::raw('order_status as order_status'))
                        ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
        // $orders = Order::select('clients'.'.name',DB::raw('DATE(delivery_time) as delivery_date'),DB::raw('sum(total) as Total'),DB::raw('sum(subtotal) as order_subtotal'),DB::raw('sum(orders.tax) as order_tax'),DB::raw('sum(tip) as order_tip'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.CCR+orders.CCF) as order_CCRF'),
        //                     DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
        
                        ->join('clients','orders'.'.user_id','clients'.'.id')
                        ->where(function($query) use ($request)
{
   
       
       
        $query->where('orders.delivery_time', '>=', $request['filter_date1']);
        $query->where('orders.delivery_time', '<=', $request['filter_date2']);
   
})
                        ->where('payment_status','Paid')
                        ->where('order_status','<>','Cancelled')
                        ->where('restaurant_id',user_id())
                        ->orderBy('orders.id','DESC')->get();

$date1 = $request['filter_date1'];
$date2 = $request['filter_date2'];                       
$nextMonth = date('d/m/Y');
$firstthisMonth = date('d/m/Y', strtotime($date1));

$lastthisMonth = date('d/m/Y', strtotime($date2));

$totalSalesCollected = $orders->sum('order_total');
$totaltktsubtotald = $orders->sum('order_subtotal');
$totaltips = $orders->sum('order_tip');
$totaltaxes = $orders->sum('order_tax');
$totalzingEateryFees = $orders->sum('order_ZF');
$totalzingCustomerFees = $orders->sum('order_ZC');
$totalcreditcard = $orders->sum('order_CCRF');
$totalEateryAmount = $orders->sum('EateryAmount');

       
       $viewfinal =view('cart::default.order.monthly_stmnt_first', compact('date1','date2','nextMonth','firstthisMonth','lastthisMonth','totalSalesCollected','totaltktsubtotald','totaltips','totaltaxes','totalzingEateryFees','totalzingCustomerFees','totalcreditcard','totalEateryAmount'))->render();  
      if(count($orders) > 0){
    $collection = collect($orders);

    // Split the collection into two groups.
    $limit = intdiv(count($orders),15)+1;
    if($limit == 1){
        $limit = $limit;
    }
    for($i=1;$i<$limit+1;$i++){ 
        $count = intdiv(count($orders),15)+1;
        // $groups = $collection->chunk(20); 
        // $collection = collect([1, 2, 3, 4, 5, 6, 7, 8, 9]);
        // if($i == 0){
        //     $data = $collection->forPage($i+1, 12);
        //     $collection = $collection->reject(function ($value, $key) {
        //         return $key < 12;
        //     });
        // }
        // else{
            $data = $collection->forPage($i, 15);
        // }

        $view2 = view('cart::default.order.monthly_stmnt_second', compact('data','count','i'))->render();
        $viewfinal = $viewfinal.$view2;
    }
}
else{
    $data = $orders;
    $count = 0 ;
    $view2 = view('cart::default.order.monthly_stmnt_second', compact('data','count'))->render();
    $viewfinal = $viewfinal.$view2;
}
if(!empty($request['download'])){
    $pdf = \PDF::loadView('cart::default.order.monthly_stmnt_mainNew', compact('viewfinal','nextMonth','firstthisMonth','lastthisMonth','totalSalesCollected','totaltktsubtotald','totaltips','totaltaxes','totalzingEateryFees','totalzingCustomerFees','totalcreditcard','totalEateryAmount'))->setPaper('a4', 'landscape');

     return $pdf->download('ORDER#'.$firstthisMonth.' '.$lastthisMonth.'.pdf', ['Content-Type' => 'application/pdf']);
    }else{
        return view('cart::default.order.monthly_stmnt_filter', compact('viewfinal','orders','nextMonth','firstthisMonth','lastthisMonth','totalSalesCollected','totaltktsubtotald','totaltips','totaltaxes','totalzingEateryFees','totalzingCustomerFees','totalcreditcard','totalEateryAmount'));
    }


}

public function monthStmntPDF(Request $request){
         if(empty($request['filter_date1'])){
            $request['filter_date1'] = 'all';
        }
        if(empty($request['filter_date2'])){
            $request['filter_date2'] = 'all';
        }
        $orders = Order::select('orders'.'.id','clients'.'.name',DB::raw('DATE(delivery_time) as delivery_date'),DB::raw('orders.subtotal as order_subtotal'),DB::raw('(orders.subtotal*restaurants.tax_rate/100) as order_tax'),DB::raw('orders.delivery_charge as delivery_charge'), DB::raw('orders.min_order_difference as extra_payment'), DB::raw('orders.ZF as order_ZF'),DB::raw('orders.ZR as order_ZR'),DB::raw('orders.ZC as order_ZC'),DB::raw('orders.CCR/100 * orders.total as order_CCR'),DB::raw('orders.CCF as order_CCF'),DB::raw('orders.CCR/100 * orders.total+orders.CCF as order_CCRF'),
                            DB::raw('(total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0)) as EateryAmount'),DB::raw('orders.total as order_total'),DB::raw('tip as order_tip'), DB::raw('order_status as order_status'))
                        ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
        // $orders = Order::select('clients'.'.name',DB::raw('DATE(delivery_time) as delivery_date'),DB::raw('sum(total) as Total'),DB::raw('sum(subtotal) as order_subtotal'),DB::raw('sum(orders.tax) as order_tax'),DB::raw('sum(tip) as order_tip'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.CCR+orders.CCF) as order_CCRF'),
        //                     DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
        
                        ->join('clients','orders'.'.user_id','clients'.'.id')
                        ->where(function($query) use ($request)
{
   
        $valueMonth = $request['filter_date1'];
        $valueYear = $request['filter_date2'];
        if ( $valueMonth != 'all') $query->whereMonth('orders.delivery_time', $request['filter_date1']);
        if ( $valueYear!= 'all') $query->whereYear('orders.delivery_time', $request['filter_date2']);
   
})
                        ->where('payment_status','Paid')
                        ->where('order_status','<>','Cancelled')
                        ->where('restaurant_id',user_id())
                        ->orderBy('orders.id','DESC')->get();

  $valueMonth = $request['filter_date1'];
        $valueYear = $request['filter_date2'];                       
$currentDate = $valueYear.'-'.$valueMonth.'-01';
$d = new DateTime( $currentDate );
$d->modify( 'first day of next month' );
$nextMonth = $d->format( '01/m/Y' );

$f = new DateTime( $currentDate );
$f->modify( 'first day of this month' );
$firstthisMonth = $f->format( 'd/m/Y' );

$l = new DateTime( $currentDate );
$l->modify( 'last day of this month' );
$lastthisMonth = $l->format( 'd/m/Y' );

$totalSalesCollected = $orders->sum('order_total');
$totaltktsubtotald = $orders->sum('order_subtotal');
$totaltips = $orders->sum('order_tip');
$totaltaxes = $orders->sum('order_tax');
$totalzingEateryFees = $orders->sum('order_ZF');
$totalzingCustomerFees = $orders->sum('order_ZC');
$totalcreditcard = $orders->sum('order_CCRF');
$totalEateryAmount = $orders->sum('EateryAmount');
//return view('cart::default.order.monthly_stmnt_pdf', compact('orders','valueMonth','valueYear','nextMonth','firstthisMonth','lastthisMonth','totalSalesCollected','totaltktsubtotald','totaltips','totaltaxes','totalzingEateryFees','totalzingCustomerFees','totalcreditcard','totalEateryAmount'));
        // $pdf = \PDF::loadView('cart::default.order.monthly_stmnt_pdf', compact('orders','valueMonth','valueYear','nextMonth','firstthisMonth','lastthisMonth','totalSalesCollected','totaltktsubtotald','totaltips','totaltaxes','totalzingEateryFees','totalzingCustomerFees','totalcreditcard','totalEateryAmount'));
       
       $viewfinal =view('cart::default.order.monthly_stmnt_first', compact('valueMonth','valueYear','nextMonth','firstthisMonth','lastthisMonth','totalSalesCollected','totaltktsubtotald','totaltips','totaltaxes','totalzingEateryFees','totalzingCustomerFees','totalcreditcard','totalEateryAmount'))->render();  
if(count($orders) > 0){
    $collection = collect($orders);

    // Split the collection into two groups.
    $limit = intdiv(count($orders),15)+1;
    if($limit == 1){
        $limit = $limit;
    }
    for($i=1;$i<$limit+1;$i++){ 
        $count = intdiv(count($orders),15)+1;
        // $groups = $collection->chunk(20); 
        // $collection = collect([1, 2, 3, 4, 5, 6, 7, 8, 9]);
        // if($i == 0){
        //     $data = $collection->forPage($i+1, 12);
        //     $collection = $collection->reject(function ($value, $key) {
        //         return $key < 12;
        //     });
        // }
        // else{
            $data = $collection->forPage($i, 15);
        // }

        $view2 = view('cart::default.order.monthly_stmnt_second', compact('data','count','i'))->render();
        $viewfinal = $viewfinal.$view2;
    }
}
else{
    $data = $orders;
    $count = 0 ;
    $view2 = view('cart::default.order.monthly_stmnt_second', compact('data','count'))->render();
    $viewfinal = $viewfinal.$view2;
}

//return view('cart::default.order.monthly_stmnt_main', compact('viewfinal','orders','valueMonth','valueYear','nextMonth','firstthisMonth','lastthisMonth','totalSalesCollected','totaltktsubtotald','totaltips','totaltaxes','totalzingEateryFees','totalzingCustomerFees','totalcreditcard','totalEateryAmount'));
$pdf = \PDF::loadView('cart::default.order.monthly_stmnt_main', compact('viewfinal','valueMonth','valueYear','nextMonth','firstthisMonth','lastthisMonth','totalSalesCollected','totaltktsubtotald','totaltips','totaltaxes','totalzingEateryFees','totalzingCustomerFees','totalcreditcard','totalEateryAmount'))->setPaper('a4', 'landscape');

$dateObj   = DateTime::createFromFormat('!m', $request['filter_date1']);
        if($request['filter_date1'] != 'all'){
            $monthName = $dateObj->format('F');

        }
        else{
            $monthName = '';
        }
         if($request['filter_date2'] == 'all'){
            $request['filter_date2'] = '';

        }
       
       // return view('cart::default.order.invoice', compact('orders'));
     return $pdf->stream('ORDER#'.$monthName.' '.$request['filter_date2'].'.pdf');
   }


   public function pivotReports(Request $request){
    if($request->ajax()){
    $filter = $request->all();  
    $orders = $this->repository->getOrderReports($filter);
    return $orders;
    }      
    return view('cart::order.reports');            


}
}
