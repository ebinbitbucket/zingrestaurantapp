<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Laraecart\Cart\Http\Requests\CartRequest;
use Laraecart\Cart\Interfaces\CartRepositoryInterface;
use Laraecart\Cart\Models\Cart;

/**
 * Resource controller class for cart.
 */
class CartResourceController extends BaseController
{

    /**
     * Initialize cart resource controller.
     *
     * @param type CartRepositoryInterface $cart
     *
     * @return null
     */
    public function __construct(CartRepositoryInterface $cart)
    {
        parent::__construct();
        $this->repository = $cart;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Laraecart\Cart\Repositories\Criteria\CartResourceCriteria::class);
    }

    /**
     * Display a list of cart.
     *
     * @return Response
     */
    public function index(CartRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Laraecart\Cart\Repositories\Presenter\CartPresenter::class)
                ->$function();
        }

        $carts = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('cart::cart.names'))
            ->view('cart::cart.index', true)
            ->data(compact('carts', 'view'))
            ->output();
    }

    /**
     * Display cart.
     *
     * @param Request $request
     * @param Model   $cart
     *
     * @return Response
     */
    public function show(CartRequest $request, Cart $cart)
    {

        if ($cart->exists) {
            $view = 'cart::cart.show';
        } else {
            $view = 'cart::cart.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('cart::cart.name'))
            ->data(compact('cart'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(CartRequest $request)
    {

        $cart = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('cart::cart.name')) 
            ->view('cart::cart.create', true) 
            ->data(compact('cart'))
            ->output();
    }

    /**
     * Create new cart.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(CartRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $cart                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('cart::cart.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/cart/' . $cart->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/cart/cart'))
                ->redirect();
        }

    }

    /**
     * Show cart for editing.
     *
     * @param Request $request
     * @param Model   $cart
     *
     * @return Response
     */
    public function edit(CartRequest $request, Cart $cart)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('cart::cart.name'))
            ->view('cart::cart.edit', true)
            ->data(compact('cart'))
            ->output();
    }

    /**
     * Update the cart.
     *
     * @param Request $request
     * @param Model   $cart
     *
     * @return Response
     */
    public function update(CartRequest $request, Cart $cart)
    {
        try {
            $attributes = $request->all();

            $cart->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('cart::cart.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/cart/' . $cart->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/cart/' . $cart->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the cart.
     *
     * @param Model   $cart
     *
     * @return Response
     */
    public function destroy(CartRequest $request, Cart $cart)
    {
        try {

            $cart->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::cart.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('cart/cart/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/cart/' . $cart->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple cart.
     *
     * @param Model   $cart
     *
     * @return Response
     */
    public function delete(CartRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::cart.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('cart/cart'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/cart'))
                ->redirect();
        }

    }

    /**
     * Restore deleted carts.
     *
     * @param Model   $cart
     *
     * @return Response
     */
    public function restore(CartRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('cart::cart.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/cart/cart'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/cart/'))
                ->redirect();
        }

    }

}
