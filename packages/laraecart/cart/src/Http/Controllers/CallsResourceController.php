<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Laraecart\Cart\Http\Requests\CallsRequest;
use Laraecart\Cart\Interfaces\CallsRepositoryInterface;
use Laraecart\Cart\Models\Calls;

/**
 * Resource controller class for calls.
 */
class CallsResourceController extends BaseController
{

    /**
     * Initialize calls resource controller.
     *
     * @param type CallsRepositoryInterface $calls
     *
     * @return null
     */
    public function __construct(CallsRepositoryInterface $calls)
    {
        parent::__construct();
        $this->repository = $calls;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Laraecart\Cart\Repositories\Criteria\CallsResourceCriteria::class);
    }

    /**
     * Display a list of calls.
     *
     * @return Response
     */
    public function index(CallsRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Laraecart\Cart\Repositories\Presenter\CallsPresenter::class)
                ->$function();
        }

        $calls = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('calls::calls.names'))
            ->view('cart::calls.index', true)
            ->data(compact('calls', 'view'))
            ->output();
    }

    /**
     * Display calls.
     *
     * @param Request $request
     * @param Model   $calls
     *
     * @return Response
     */
    public function show(CallsRequest $request, Calls $calls)
    {

        if ($calls->exists) {
            $view = 'calls::calls.show';
        } else {
            $view = 'calls::calls.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('calls::calls.name'))
            ->data(compact('calls'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new calls.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(CallsRequest $request)
    {

        $calls = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('calls::calls.name')) 
            ->view('calls::calls.create', true) 
            ->data(compact('calls'))
            ->output();
    }

    /**
     * Create new calls.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(CallsRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $calls                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('calls::calls.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('calls/calls/' . $calls->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/calls/calls'))
                ->redirect();
        }

    }

    /**
     * Show calls for editing.
     *
     * @param Request $request
     * @param Model   $calls
     *
     * @return Response
     */
    public function edit(CallsRequest $request, Calls $calls)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('calls::calls.name'))
            ->view('calls::calls.edit', true)
            ->data(compact('calls'))
            ->output();
    }

    /**
     * Update the calls.
     *
     * @param Request $request
     * @param Model   $calls
     *
     * @return Response
     */
    public function update(CallsRequest $request, Calls $calls)
    {
        try {
            $attributes = $request->all();

            $calls->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('calls::calls.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('calls/calls/' . $calls->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('calls/calls/' . $calls->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the calls.
     *
     * @param Model   $calls
     *
     * @return Response
     */
    public function destroy(CallsRequest $request, Calls $calls)
    {
        try {

            $calls->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('calls::calls.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('calls/calls/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('calls/calls/' . $calls->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple calls.
     *
     * @param Model   $calls
     *
     * @return Response
     */
    public function delete(CallsRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('calls::calls.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('calls/calls'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/calls/calls'))
                ->redirect();
        }

    }

    /**
     * Restore deleted calls.
     *
     * @param Model   $calls
     *
     * @return Response
     */
    public function restore(CallsRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('calls::calls.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/calls/calls'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/calls/calls/'))
                ->redirect();
        }

    }

}
