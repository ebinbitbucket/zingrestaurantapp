<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Laraecart\Cart\Http\Requests\HistoryRequest;
use Laraecart\Cart\Interfaces\HistoryRepositoryInterface;
use Laraecart\Cart\Models\History;

/**
 * Resource controller class for history.
 */
class HistoryResourceController extends BaseController
{

    /**
     * Initialize history resource controller.
     *
     * @param type HistoryRepositoryInterface $history
     *
     * @return null
     */
    public function __construct(HistoryRepositoryInterface $history)
    {
        parent::__construct();
        $this->repository = $history;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Laraecart\Cart\Repositories\Criteria\HistoryResourceCriteria::class);
    }

    /**
     * Display a list of history.
     *
     * @return Response
     */
    public function index(HistoryRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Laraecart\Cart\Repositories\Presenter\HistoryPresenter::class)
                ->$function();
        }

        $histories = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('cart::history.names'))
            ->view('cart::history.index', true)
            ->data(compact('histories', 'view'))
            ->output();
    }

    /**
     * Display history.
     *
     * @param Request $request
     * @param Model   $history
     *
     * @return Response
     */
    public function show(HistoryRequest $request, History $history)
    {

        if ($history->exists) {
            $view = 'cart::history.show';
        } else {
            $view = 'cart::history.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('cart::history.name'))
            ->data(compact('history'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new history.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(HistoryRequest $request)
    {

        $history = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('cart::history.name')) 
            ->view('cart::history.create', true) 
            ->data(compact('history'))
            ->output();
    }

    /**
     * Create new history.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(HistoryRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $history                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('cart::history.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/history/' . $history->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/cart/history'))
                ->redirect();
        }

    }

    /**
     * Show history for editing.
     *
     * @param Request $request
     * @param Model   $history
     *
     * @return Response
     */
    public function edit(HistoryRequest $request, History $history)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('cart::history.name'))
            ->view('cart::history.edit', true)
            ->data(compact('history'))
            ->output();
    }

    /**
     * Update the history.
     *
     * @param Request $request
     * @param Model   $history
     *
     * @return Response
     */
    public function update(HistoryRequest $request, History $history)
    {
        try {
            $attributes = $request->all();

            $history->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('cart::history.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('cart/history/' . $history->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/history/' . $history->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the history.
     *
     * @param Model   $history
     *
     * @return Response
     */
    public function destroy(HistoryRequest $request, History $history)
    {
        try {

            $history->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::history.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('cart/history/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('cart/history/' . $history->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple history.
     *
     * @param Model   $history
     *
     * @return Response
     */
    public function delete(HistoryRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('cart::history.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('cart/history'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/history'))
                ->redirect();
        }

    }

    /**
     * Restore deleted histories.
     *
     * @param Model   $history
     *
     * @return Response
     */
    public function restore(HistoryRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('cart::history.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/cart/history'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/cart/history/'))
                ->redirect();
        }

    }

}
