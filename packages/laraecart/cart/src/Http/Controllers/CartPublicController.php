<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Laraecart\Cart\Interfaces\CartRepositoryInterface;
use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
use Laraecart\Cart\Interfaces\OrderRepositoryInterface;
use Laraecart\Cart\Interfaces\DetailsRepositoryInterface;
use Laraecart\Cart\Interfaces\AddressRepositoryInterface;
use Illuminate\Http\Request;
use Laraecart\Cart\Facades\Cart;
use Auth;
use Session;
use Laraecart\Cart\Models\Address;
use DateTime;
use App\Notifications\CustomEmail;
use App\User;
use Mail;
use Restaurant\Restaurant\Models\Restauranttimings;
use Restaurant\Restaurant\Models\Restaurant;
use DB;
use Laraecart\Cart\Models\Order;
use Laraecart\Cart\Models\TransactionLogs;
use Request as Requests;
use Restaurant\Restaurant\Models\Addon;

class CartPublicController extends BaseController
{
    // use CartWorkflow;

    /**
     * Constructor.
     *
     * @param type \Laraecart\Cart\Interfaces\CartRepositoryInterface $cart
     *
     * @return type
     */
    public function __construct(CartRepositoryInterface $cart, MenuRepositoryInterface $menu, OrderRepositoryInterface $order, DetailsRepositoryInterface $detail,AddressRepositoryInterface $address)
    {
        if (!empty(app('auth')->getDefaultDriver())) {
           $this->middleware('auth:' . app('auth')->getDefaultDriver(), ['only' => ['welcome']]); 

       }
        $this->repository = $cart;
        $this->menu       = $menu;
        $this->order       = $order;
        $this->detail       = $detail;
        $this->address       = $address;
        parent::__construct();
    }

    /**
     * Show cart's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $carts = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$cart::cart.names'))
            ->view('cart::cart.index')
            ->data(compact('carts'))
            ->output();
    }


    /**
     * Show cart.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $cart = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$cart->name . trans('cart::cart.name'))
            ->view('cart::cart.show')
            ->data(compact('cart'))
            ->output();
    }
    
    public function restaurantCart(){
        $restaurant=[];
         if(Cart::count() > 0){
            $carts = Cart::content();
            foreach($carts as $cart) {
                $rest_id = $cart->options->restaurant;
                $restaurant = Restaurant::where('id',$rest_id)->first();
            }
        }
        return view('cart::public.cart.cart_restaurant', compact('restaurant'));
    }

    public function restaurantcartHeader(){
        return view('restaurant::public.restaurant.gadget.mycart');
    }
    
    public function restaurantcartPage(){
        return view('restaurant::public.restaurant.gadget.mycartpage');
    }
public function  cart($id = '', Request $request)
    {
        if (!empty($request->all())) {
            $qty = $request['quantity'];
        } else {
            $qty = 1;
        }

        if (!empty($id)) {
            $menu = $this->menu->scopeQuery(function ($query) use ($id) {
                return $query->orderBy('id', 'DESC')
                    ->where('id', $id);
            })->first(['*']);
        } else {
            $menu = $this->menu->scopeQuery(function ($query) use ($request) {
                return $query->orderBy('id', 'DESC')
                    ->where('id', $request->get('menu_id'));
            })->first(['*']);
        }

        $menu_addons = DB::table('menu_addon')
            ->join('restaurant_addons', 'menu_addon' . '.addon_id', 'restaurant_addons' . '.id')
            ->where('menu_addon' . '.menu_id', $menu->id)
            ->where('menu_addon' . '.required', 'Yes')->pluck('menu_addon.addon_id')->toArray();

        if (!empty($menu_addons)) {
            if (empty($request->get('variations'))) {
                return $this->response->message('Please select the required items.')
                    ->code(400)
                    ->status('required')
                    ->url(url()->previous())
                    ->redirect();
            } else {
                $result = array_diff($menu_addons, array_keys($request->get('variations')));

                if (!empty($result)) {
                    return $this->response->message('Please select the required items.')
                        ->code(400)
                        ->status('required')
                        ->url(url()->previous())
                        ->redirect();
                }
                                else {
                    foreach ($request->get('variations') as $key_item_required => $item_required) {
                        $menu_addons_minmax = DB::table('menu_addon')->select('menu_addon.*')
                            ->join('restaurant_addons', 'menu_addon' . '.addon_id', 'restaurant_addons' . '.id')
                            ->where('menu_addon' . '.menu_id', $menu->id)
                            ->where('menu_addon' . '.addon_id', $key_item_required)->first();
                        $addon_name = Addon::where('id',$key_item_required)->first();
    if (count($item_required) < $menu_addons_minmax->min) {
                           return $this->response->message('Please select atleast '. $menu_addons_minmax->min .' addon(s)' )
                                ->code($addon_name->id)
                                ->status('min')
                                ->url(url()->previous())
                                ->redirect();
                        } elseif (count($item_required) > $menu_addons_minmax->max) {
                            return $this->response->message('Maximum '.$menu_addons_minmax->max.' addons allowed. '  )
                                ->code($addon_name->id)
                                ->status('max')
                                ->url(url()->previous())
                                ->redirect();
                        }

                    }

                }

            }

        }

        if (!empty($request->get('price'))) {
            $prices = explode(',', $request->get('price'));
            $price  = $prices[1];
        } else {
            $price = $menu->price;
        }

        if (!empty($menu->offer)) {

            if (!empty($menu->offer['schedule'])) {
                $day = substr(date('l', strtotime(Session::get('search_time'))), 0, 3);

                foreach ($menu->offer['schedule'] as $offer_key => $offer_value) {
                    if(isset($offer_value['day'])){

                    if ($day == $offer_value['day']) {

                        if (date('H:i', strtotime($offer_value['from'])) <= date('H:i', strtotime(Session::get('search_time'))) && date('H:i', strtotime($offer_value['end'])) >= date('H:i', strtotime(Session::get('search_time')))) {
                            $price = $menu->offer['price'];
                        }

                    }
                }


                }

            }

        }

        foreach (Cart::content() as $item) {

            foreach ($item->options as $option) {
                $prev_restaurant = $option;
                break;
            }

        }

        if (!empty($prev_restaurant)) {

            if ($prev_restaurant != $menu->restaurant->id) {
                // if(!empty($request->get('delete'))){
                Cart::destroy();
                $qty = 1;

// }else{

//     return response()->json(['restaurant_error' => 'error']);
                // }
            }

        }

        if (!empty($request->get('variations'))) {
            $complete_addons = [];

            foreach ($request->get('variations') as $key => $value) {

                foreach ($value as $keys => $val) {
                    $addon_array = [];
                    $array       = explode(',', $val);
                    array_push($addon_array, $keys, $array[0], $array[1]);
                    // Cart::add(['id' => $keys, 'name' => $array[0], 'qty' => 1, 'image' => 'image/sm/' . $image, 'price' => $array[1], 'options' => ['restaurant' => $menu->restaurant->id,'menu_addon' => 'addon','restaurantname' => $menu->restaurant->name,'restaurantphone' => $menu->restaurant->phone,'restaurantimage' => $menu->restaurant->logo,'delivery_type' => $menu->restaurant->delivery ,'delivery_charge' => $menu->restaurant->delivery_charge,'preparation_time' => 0,'special_instr' => $request['special_instr']]]);
                    $complete_addons[] = $addon_array;

                }

            }

        }

        $image = ($menu['image'] != []) ? $menu['image'][0]['path'] : '';
        Cart::add(['id' => $menu->id, 'name' => $menu->name, 'qty' => $qty, 'image' => 'image/sm/' . $image, 'price' => $price, 'options' => ['restaurant' => $menu->restaurant->id, 'menu_addon' => 'menu', 'restaurantname' => $menu->restaurant->name, 'restaurantphone' => $menu->restaurant->phone, 'restaurantimage' => $menu->restaurant->logo, 'delivery_type' => $menu->restaurant->delivery, 'delivery_charge' => $menu->restaurant->delivery_charge, 'preparation_time' => !empty($menu->categories->preparation_time) ? $menu->categories->preparation_time : 0, 'special_instr' => $request['special_instr'], 'addons' => !empty($complete_addons) ? $complete_addons : '', 'variation' => !empty($prices) ? $prices[0] : '']]);

        $cart  = Cart::content();
        $count = Cart::count();

        foreach ($cart as $cart) {
            $rest_id    = $cart->options->restaurant;
            $cart_id    = $cart->rowId;
            $restaurant = Restaurant::where('id', $rest_id)->first();
        }

        $exists                      = TransactionLogs::where('cart_id', $cart_id)->where('type', 'restaurant')->count();
        $attributes['user_id']       = user_id();
        $attributes['type']          = 'cart';
        $attributes['total_amount']  = Cart::total();
        $attributes['json_data']     = Cart::content();
        $attributes['cart_id']       = $cart_id;
        $attributes['ip_address']    = request()->ip();
        $attributes['added_item']    = $menu->id;
        $attributes['date']          = date('Y-m-d H:i:s');
        $attributes['restaurant_id'] = $rest_id;
        $attributes['url']           = Requests::path();
        $attributes['server_variables'] = Requests::server();
		$attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
        $useragent                   = $_SERVER['HTTP_USER_AGENT'];

        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
            $attributes['source'] = 'mobile';
        } else {
            $attributes['source'] = 'desktop';
        }

      $log = TransactionLogs::updateOrCreate(
			[
				'date'=>date('Y-m-d',strtotime($attributes['date'])),
				'type'=>$attributes['type'],
				'ip_address'=>$attributes['ip_address'],
				'restaurant_id'=>$attributes['restaurant_id'],
				
			],[
				'user_id'=>$attributes['user_id'],
                'type'=>$attributes['type'],
                'total_amount'=>$attributes['total_amount'],
				'cart_id'=>$attributes['cart_id'],
				'json_data'=>$attributes['json_data'],
				'ip_address'=>$attributes['ip_address'],
				'date'=>date('Y-m-d',strtotime($attributes['date'])),
				'restaurant_id'=>$attributes['restaurant_id'],
				'url'=>$attributes['url'],
				'server_variables'=>$attributes['server_variables'],
				'HTTP_REFERER'=>$attributes['HTTP_REFERER'],
				'source'=>$attributes['source'],
			]);

// if($exists ==0){

//     $log = TransactionLogs::create($attributes);

// }

// else{

//     $log = TransactionLogs::where('cart_id',$cart_id)->where('type','restaurant')->update($attributes);
        // }

        return response()->json(['count' => $count]);
    }



      public function checkRestaurant($id = '')
    {
        $menu = $this->menu->scopeQuery(function ($query) use ($id) {
            return $query->orderBy('id', 'DESC')
                ->where('id', $id);
        })->first(['*']);

        foreach (Cart::content() as $item) {
            foreach ($item->options as $option) {
                $prev_restaurant = $option;
                break;
            }
        }

        if(!empty($prev_restaurant)){
            if($prev_restaurant != $menu->restaurant->id){
                 
                    return response()->json(['restaurant_error' => 'error']);
               
            }
        }

        return $menu;

    }

    public function remove($rowid)
    {
        $carts = Cart::remove($rowid);
        if(request()->ajax()){
            return response()->json(['status' => 'success']);
        }
        else{
            return redirect()->back();
        }
    }

    public function welcome()
    {
        $carts = Cart::content();
        $user_id = user_id(); 
        if(Auth::user())
        { 
            $address = $this->address->address($user_id); 
        }
         $weekday = strtolower(date('D'));
         if(Cart::count() > 0){
            foreach($carts as $cart) {
                $rest_id = $cart->options->restaurant;
                $restaurant = Restaurant::where('id',$rest_id)->first();
            }
        $timings = $restaurant->timings->where('day', $weekday);
         }
        return $this->response->setMetaTitle('Checkout ZingMyOrder')
            ->view('cart::public.cart.checkout')
            ->layout('blankcheckout')
            ->data(compact('carts', 'address', 'timings'))
            ->output();
    }

     public function deliveryaddress(Request $request)
    {
     
        $request = $request->all();  
            $carts   = Cart::content();
        foreach ($carts as $item) {
             foreach ($item->options as $key => $option) {
                if($key== 'restaurant')
                    $restaurant = $option;
                }
        }
        $restaurant_data = Restaurant::where('id',$restaurant)->first();
      if($request['submit'] == 'cash' || $request['submit'] == 'online'){

        $order['name'] = $request['cus_name'];
        $order['phone'] = $request['phone'];        
 

        if(empty($request['address_id']) && $request['order_type'] != 'Pickup'){        
   
            Session::flash('error', 'Please add address'); 
            // return redirect()->back();
            return response()->json(['status' => 'Error','msg' => 'Delivery_address']);
        }
         elseif($request['order_type'] != 'Pickup' && !empty($request['address_id']))
         { 
           
            $check_address = Address::where('id',$request['address_id'])->first();
           if($check_address->longitude == null){
                return response()->json(['status' => 'Error','msg' => 'address_error']);
           }
            $distance = DB::select("SELECT (3959  * acos(cos( radians(".$restaurant_data->latitude.") ) * cos( radians( ".$check_address->latitude." ) ) * cos( radians( ".$check_address->longitude." ) - radians(".$restaurant_data->longitude.") ) + sin( radians(".$restaurant_data->latitude.") ) * sin( radians( ".$check_address->latitude." ) )) ) as distance");  
          if(!empty($restaurant_data->delivery_limit) && $distance[0]->distance > $restaurant_data->delivery_limit){
                Session::flash('error',"This Restauarnt can't take orders at this time.");
                return response()->json(['status' => 'Error','msg' => 'delivery_limit','delivery_limit' => $restaurant_data->delivery_limit]);
            }
            else{
                $order['address_id'] = $request['address_id'];
            }
       
        // if(!empty($request['address_id'])){
        //     $order['address_id'] = $request['address_id'];
        
        // }
        $address_default= Address::where('client_id',user_id())->where('default_address',1)->first();
        if(empty($address_default) ){
            if(!empty($request['address_id']))
              Address::where('id',$request['address_id'])->update(['default_address'=> 1]);
        }
        elseif($address_default->id!=$request['address_id']){
            Address::where('id',$address_default->id)->update(['default_address'=> 0]);
            Address::where('id',$request['address_id'])->update(['default_address'=> 1]);
        }
         }
        if (empty($request['billing'])) {
				Session::flash('error', 'Please add address');
				return response()->json(['status' => 'Error', 'msg' => 'Billing address']);
		}else {
                $order['billing_address_id'] = $request['billing'];
        }
        if($request['order_type'] == 'Pickup'){
            $order['address_id'] =null;
        }
        $preparation_time = 0;
        foreach ($carts as $item) {      
             $order['subtotal'] = $item->price; 

             foreach ($item->options as $keys => $optiond) {
                
               if($keys == 'delivery_charge' && $request['order_type'] != 'Pickup')
                $delivery_charge = $optiond;
            else if($keys == 'preparation_time'){
                if(is_numeric($optiond))
                $preparation_time = $preparation_time + $optiond;
                else{
                    $preparation_time = $preparation_time + 10;
                }
            }
           }

        }
        if($request['del_type'] == 'ASAP'){
            $time = strtotime(($preparation_time + 5).' minute');
            $order['delivery_time'] = date('y-m-d H:i:s', $time);
            $order['order_status'] = 'New Orders';

        }
        else{
                $date = new DateTime($request['scheduled_date']);
                $time = new DateTime($request['scheduled_time']);
                $combined = new DateTime($date->format('Y-m-d') .' ' .$time->format('H:i:s'));
            $order['delivery_time'] = $combined->format('y-m-d H:i:s');
            $order['order_status'] = 'Scheduled';
        }
        
        // echo date('y-m-d H:i') . date('y-m-d H:i', strtotime($order['delivery_time']));
        
        if (date('y-m-d H:i') >= date('y-m-d H:i', strtotime($order['delivery_time']))) {
					return response()->json(['status' => 'Error', 'msg' => 'Previous Time']);
		}
        if(empty($delivery_charge) || $delivery_charge == ''  ){
             $delivery_charge = 0;
        }
        if(empty($request['tip']) ||  $request['tip']== ''){
             $request['tip'] = 0;
        }
      
        $amount                   = Cart::total();
        $order['order_type']      = $request['order_type'];
        $order['tax']             = !empty($request['tax']) ? $request['tax']:0;
        $order['discount_points'] = (!empty($request['discount']) ? $request['discount']:0)*2000;
        $order['loyalty_points']  = 0;
        $order['tip']             = $request['tip'];
        $order['tip_value_cus'] = $request['tip_value_cus'];
            if($order['tip_value_cus'] != null){
                $order['tip_value'] = null;
            }else{
                $order['tip_value'] = $request['tip_value'];
            }
            if(empty($request['discount_amount'])){
                $request['discount_amount'] = 0;
            }
        $order['delivery_charge'] = $delivery_charge;
        $order['total']           = $amount+$delivery_charge+$order['tip']+$order['tax']-(!empty($request['discount']) ? $request['discount']:0)-$request['discount_amount'];
       $order['subtotal']        = Cart::subtotal();
        // $order['payment_details'] = 'paypal';
            $order['payment_status'] = 'UnPaid';
      
      
          
        $order['tax_rate'] = $restaurant_data->tax_rate;
        $order['CCR'] = $restaurant_data->CCR;
        $order['CCF'] = $restaurant_data->CCF;
        $order['ZR'] = $restaurant_data->ZR;
        $order['ZF'] = $restaurant_data->ZF;
        $order['ZC'] = $restaurant_data->ZC;
        $restaurant_det = Restauranttimings::where('restaurant_id',$restaurant)->where('day',substr(strtolower(date('l',strtotime($order['delivery_time']))),0,3))->whereNull('deleted_at')->get();
        $flag = 0; 
        if(empty($restaurant_det)){
            $flag = 1;
        }
        foreach ($restaurant_det as $key_time => $value_time) {
            if(date('H:i',strtotime($value_time->opening))<=date('H:i',strtotime($order['delivery_time'])) && date('H:i',strtotime($value_time->closing))>=date('H:i',strtotime($order['delivery_time']))){ 
                         $flag = 1;
                         break;
                     }
        }
        if($flag!=1){
            Session::flash('error',"This Restauarnt can't take orders at this time.");
            return response()->json(['status' => 'Error', 'msg' => 'restaurant_time']);
        }
        if($restaurant_data->min_order_count > $order['subtotal']){
            Session::flash('error',"This Restauarnt can't take orders at this time.");
           return response()->json(['status' => 'Error','msg' => 'min_order_count','min_amt' => $restaurant_data->min_order_count]);
        }
            $order['restaurant_id'] = $restaurant;

            $order['user_id'] = user_id();
            if(Auth::user()){
            $order['user_type'] = 'App\Client';
            }
            else{
                $order['user_type'] = user_type();
            }
            $order['discount_amount'] = $request['discount_amount'];
    $order = $this->order->create($order);

    $field['order_id'] = $order->id;

        foreach ($carts as $item) {
            $field['menu_id'] = $item->id;

            // $product = $this->product->findProduct($item->id);
            // $field['product_code'] = $product->code;
            
            $field['quantity']   = $item->qty;
            $field['unit_price'] = $item->price;
foreach ($item->options as $key => $option) {            

if($key== 'menu_addon'){
                    $field['menu_addon'] = $option;
                }
           elseif($key== 'special_instr'){
                    $field['special_instr'] = $option;
                }
                 elseif($key== 'addons'){
                    $field['addons'] = $option;
                }
   }          
            $field['price'] = $item->price * $item->qty;
            $field['user_type'] = user_type();
            $field['user_id'] = user_id();
            $detail         = $this->detail->create($field);
        }

        //$orderDetail = $this->order->findOrder($order->id);
         $user = new User();
                $user->email = user()->email;

     if($request['submit'] == 'cash'){
            return redirect()->to(url('success/'.$order->id));
        }
        else{
            $tocken = $this->getToken($order);
            $view = view('cart::public.cart.requestResponse', compact('order', 'tocken'))->render();

            return response()->json(['status' => 'success','view' => $view, 'id' => $order->getRouteKey()]);
            // return $this->response->setMetaTitle('Checkout ZingMyOrder')
            // ->view('cart::public.cart.requestResponse')
            // ->layout('blankcheckout')
            // ->data(compact('order', 'tocken'))
            // ->output();
            // return view('cart::public.cart.requestResponse',compact('order', 'tocken'));
        }
        
        
        // $this->paymentRequest($order);
        // return view('cart::public.cart.requestHandler',compact('order', 'address'));
        // return redirect()->to(url('success/'.$order->id));
    }
      
        else{
            return redirect()->to(url('payment/fail'));
        }

      
        // if($orderDetail['payment_methods'] == 'paypal')
        // { 
           
        //     return redirect()->to('paypal/' . $order_id);
        // }else
        // {
        //     $v['payment_status'] = 'Paid';
        //     $v['order_status'] = 'Processing'; 
        //     $v = $this->order->updateOrder($v, $id);
        //     return redirect()->to(url('success/'.$order_id));
        // }

    }

    public function editCheckout($id) {
        $order = Order::where('id', hashids_decode($id))->first();
        $carts = Cart::content();
         $weekday = strtolower(date('D'));
         if(Cart::count() > 0){
            foreach($carts as $cart) {
                $rest_id = $cart->options->restaurant;
                $restaurant = Restaurant::where('id',$rest_id)->first();
            }
             $timings = $restaurant->timings->where('day', $weekday);
         }
        return $this->response->setMetaTitle('Checkout ZingMyOrder')
            ->view('cart::public.cart.checkout-edit')
            ->layout('blankcheckout')
            ->data(compact('order', 'carts', 'timings'))
            ->output();

    }

    public function deliveryaddressEdit(Request $request, $id) {

        $request = $request->all();

        $orders = $this->order->orderBy('id','DESC')->findByField('id', hashids_decode($id))->first(); 
        $carts = Cart::content();

        foreach ($carts as $item) {
            foreach ($item->options as $key => $option) {
                if ($key == 'restaurant') {
                    $restaurant = $option;
                }

            }
        } 
        $restaurant_data = Restaurant::where('id', $orders->restaurant_id)->first(); 
        if ($request['submit'] == 'cash' || $request['submit'] == 'online') {

            $order['name'] = $request['cus_name'];
            $order['phone'] = $request['phone'];
            if (empty($request['address_id']) && $request['order_type'] != 'Pickup') {
                Session::flash('error', 'Please add address');
                return response()->json(['status' => 'Error', 'msg' => 'Delivery_address']);
            } elseif ($request['order_type'] != 'Pickup' && !empty($request['address_id'])) {

                $check_address = Address::where('id', $request['address_id'])->first();

                if($check_address->longitude == null){
                    return response()->json(['status' => 'Error','msg' => 'address_error']);
                }

                $distance = DB::select("SELECT (3959  * acos(cos( radians(" . $restaurant_data->latitude . ") ) * cos( radians( " . $check_address->latitude . " ) ) * cos( radians( " . $check_address->longitude . " ) - radians(" . $restaurant_data->longitude . ") ) + sin( radians(" . $restaurant_data->latitude . ") ) * sin( radians( " . $check_address->latitude . " ) )) ) as distance");
                if (!empty($restaurant_data->delivery_limit) && $distance[0]->distance > $restaurant_data->delivery_limit) {
                    Session::flash('error', "This Restauarnt can't take orders at this time.");
                    return response()->json(['status' => 'Error', 'msg' => 'delivery_limit','delivery_limit' => $restaurant_data->delivery_limit]);
                } else {
                    $order['address_id'] = $request['address_id'];
                }

                

                $address_default = Address::where('client_id', user_id())->where('default_address', 1)->first();
                if (empty($address_default)) {
                    if (!empty($request['address_id'])) {
                        Address::where('id', $request['address_id'])->update(['default_address' => 1]);
                    }

                } elseif ($address_default->id != $request['address_id']) {
                    Address::where('id', $address_default->id)->update(['default_address' => 0]);
                    Address::where('id', $request['address_id'])->update(['default_address' => 1]);
                }
            }
            // if(!empty($request['address_id'])){

            //     $order['address_id'] = $request['address_id'];

            // }

            if (empty($request['billing'])) {
                Session::flash('error', 'Please add address');
                return response()->json(['status' => 'Error', 'msg' => 'Billing address']);
            } else {
                $order['billing_address_id'] = $request['billing'];
            }
            if ($request['order_type'] == 'Pickup') {
                $order['address_id'] = null;
            }
            $preparation_time = 0;
            foreach ($carts as $item) {
                $order['subtotal'] = $item->price;

                foreach ($item->options as $keys => $optiond) {

                    if ($keys == 'delivery_charge' && $request['order_type'] != 'Pickup') {
                        $delivery_charge = $optiond;
                    } else if ($keys == 'preparation_time') {
                        if (is_numeric($optiond)) {
                            $preparation_time = $preparation_time + $optiond;
                        } else {
                            $preparation_time = $preparation_time + 10;
                        }
                    }
                }

            }
            if ($request['del_type'] == 'ASAP') {
                $time = strtotime(date('y-m-d H:i:s') . $preparation_time . ' minute');
                $order['delivery_time'] = date('y-m-d H:i:s', $time);
                $order['order_status'] = 'New Orders';
            } else {
                $date = new DateTime($request['scheduled_date']);
                $time = new DateTime($request['scheduled_time']);
                $combined = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));
                $order['delivery_time'] = $combined->format('y-m-d H:i:s');
                $order['order_status'] = 'Scheduled';
            }
            if (date('y-m-d H:i') >= date('y-m-d H:i', strtotime($order['delivery_time']))) {
                    return response()->json(['status' => 'Error', 'msg' => 'Previous Time']);
                }
            if (empty($delivery_charge) || $delivery_charge == '') {
                $delivery_charge = 0;
            }
            if (empty($request['tip']) || $request['tip'] == '') {
                $request['tip'] = 0;
            }
            $amount = Cart::total();
            $order['order_type'] = $request['order_type'];
            $order['tax'] = $request['tax'];
            $order['discount_points'] = $request['discount'] * 2000;
            $order['loyalty_points'] = 0;
            $order['tip'] = $request['tip'];
            
            $order['tip_value_cus'] = $request['tip_value_cus'];
            if($order['tip_value_cus'] != null){
                $order['tip_value'] = null;
            }else{
                $order['tip_value'] = $request['tip_value'];
            }
            
            $order['delivery_charge'] = $delivery_charge;
            $order['total'] = $amount + $delivery_charge + $order['tip'] + $order['tax'] - $request['discount'];
            $order['subtotal'] = Cart::subtotal();
            // $order['payment_details'] = 'paypal';
            $order['payment_status'] = 'UnPaid';

            $order['tax_rate'] = $restaurant_data->tax_rate;
            $order['CCR'] = $restaurant_data->CCR;
            $order['CCF'] = $restaurant_data->CCF;
            $order['ZR'] = $restaurant_data->ZR;
            $order['ZF'] = $restaurant_data->ZF;
            $order['ZC'] = $restaurant_data->ZC;
            $restaurant_det = Restauranttimings::where('restaurant_id', $restaurant)->where('day', substr(strtolower(date('l', strtotime($order['delivery_time']))), 0, 3))->whereNull('deleted_at')->get();
            $flag = 0;
             if(empty($restaurant_det)){
            $flag = 1;
        }
            foreach ($restaurant_det as $key_time => $value_time) {
                // if (date('H:i', strtotime($value_time->closing)) >= date('H:i', strtotime($order['delivery_time']))) {
                //     $flag = 1;
                // }
                 if(date('H:i',strtotime($value_time->opening))<=date('H:i',strtotime($order['delivery_time'])) && date('H:i',strtotime($value_time->closing))>=date('H:i',strtotime($order['delivery_time']))){ 
                         $flag = 1;
                         break;
                     }
            }
            if($flag!=1){
                Session::flash('error',"This Restauarnt can't take orders at this time.");
               return response()->json(['status' => 'Error']);
            }
            if ($restaurant_data->min_order_count > $order['subtotal']) {
                Session::flash('error', "This Restauarnt can't take orders at this time.");
                return response()->json(['status' => 'Error', 'msg' => 'min_order_count']);
            }

            $order['restaurant_id'] = $restaurant;

            $order['user_id'] = user_id();
            if (Auth::user()) {
                $order['user_type'] = 'App\Client';
            } else {
                $order['user_type'] = user_type();
            }
            $order = $orders->update($order);
          

            $field['order_id'] = $orders->id;
            $details = $orders->detail->pluck('id')->toArray();
            $i = 0;
            foreach ($carts as $item) {

                $field['menu_id'] = $item->id;

                // $product = $this->product->findProduct($item->id);
                // $field['product_code'] = $product->code;

                $field['quantity'] = $item->qty;
                $field['unit_price'] = $item->price;
                foreach ($item->options as $key => $option) {

                    if ($key == 'menu_addon') {
                        $field['menu_addon'] = $option;
                    } elseif ($key == 'special_instr') {
                        $field['special_instr'] = $option;
                    } elseif ($key == 'addons') {
                        $field['addons'] = $option;
                    }
                }
                $field['price'] = $item->price * $item->qty;
                $field['user_type'] = user_type();
                $field['user_id'] = user_id();
                $detail = $this->detail->findByField('id', @$details[$i])->first(); 
                if(!empty($detail)){
                    $detail->update($field);
                }else{
                    $this->detail->create($field);
                }
                
                $i++;
            }
            $order = $this->order->findByField('id',$orders->id)->first();
           //dd($orders->id);
            $user = new User();
            $user->email = user()->email;
            // $user->notify(new CustomEmail('Order Placed', 'Your order with Order Id#'.$order->id.' amounting to $'.$order['total'] .' has been received.</br>We will send you an update when your order is ready for pickup/delivery.</br>'));

            //Cart::destroy();
            if ($request['submit'] == 'cash') {
                return redirect()->to(url('success/' . $orders->id));
            } else {

                // $tocken = $this->getToken($order);
                $tocken = 'A0DqeU1DRCWyT5mffBI8fQAAAW0Uwnld';
                $view = view('cart::public.cart.requestResponse', compact('order', 'tocken'))->render();
                return response()->json(['status' => 'success', 'view' => $view, 'id' => $orders->getRouteKey()]);

                // return $this->response->setMetaTitle('Checkout ZingMyOrder')
                // ->view('cart::public.cart.requestResponse')
                // ->layout('blankcheckout')
                // ->data(compact('order', 'tocken'))
                // ->output();
                // return view('cart::public.cart.requestResponse',compact('order', 'tocken'));
            }

            // $this->paymentRequest($order);
            // return view('cart::public.cart.requestHandler',compact('order', 'address'));
            // return redirect()->to(url('success/'.$order->id));
        } else {
            return redirect()->to(url('payment/fail'));
        }
    }



     protected function success($id)
    {
        $order = Order::where('id', hashids_decode($id))->first();
        $user = \App\Client::where('id', $order->user_id)->update(['guest' => 'false']);
        Auth::logout();
        Session::flash('successpayment', 'Your Payment has been completed Successfully');
        return $this->response->setMetaTitle('Payment ')
            ->layout('blankcheckout')
            ->view('payment::public.payment.success')
            ->data(compact('id', 'order'))
            ->output();

    }
public function add($rowId)
    {
        Cart::increment($rowId, $qty = 1);
        return view('cart::public.cart.updated_cart');
    }

    public function subtract($rowId)
    {
        Cart::decrement($rowId, $qty = 1);
        return view('cart::public.cart.updated_cart');
    }
  public function timeCheck($restaurant,$date,$time)
    {
         $date = new DateTime($date);
                $time = new DateTime($time);
                $combined = new DateTime($date->format('Y-m-d') .' ' .$time->format('H:i:s'));
            $order['delivery_time'] = $combined->format('y-m-d H:i:s');
         $restaurant_det = Restauranttimings::where('restaurant_id',$restaurant)->where('day',substr(strtolower(date('l',strtotime($order['delivery_time']))),0,3))->get();
        $flag = 0;
        if(count($restaurant_det) > 0){
            foreach ($restaurant_det as $key_time => $value_time) {
            if(date('H:i',strtotime($value_time->opening))<=date('H:i',strtotime($order['delivery_time'])) && date('H:i',strtotime($value_time->closing))>=date('H:i',strtotime($order['delivery_time']))){ 
                         $flag = 1;
                     }
        }
        }
        else{
            $flag =1;
        }
        if($flag!=1){
            return response()->json(['status' => 'Error']);
        }
    }
    public function paymentRequest(Request $request){
 // Set variables

$request = $request->all();
$merchantID = "2108313"; //Converge 6 or 7-Digit Account ID *Not the 10-Digit Elavon Merchant ID*
$merchantUserID = "zingweb"; //Converge User ID *MUST FLAG AS HOSTED API USER IN CONVERGE UI*
$merchantPIN = "5MZE69UGM2CWPB49NTO2GYXYDFAFLI85FC3BUCYINP0L03JX84TO4X9I19TRS3L1"; //Converge PIN (64 CHAR A/N)

 $url = "https://api.convergepay.com/hosted-payments/transaction_token"; // URL to Converge demo session token server
//$url = "https://api.convergepay.com/hosted-payments/transaction_token"; // URL to Converge production session token server
 
// Read the following querystring variables
$firstname=$request['ssl_first_name']; //Post first name
$lastname=$request['ssl_last_name']; //Post first name
$amount= $request['ssl_amount']; //Post Tran Amount


$ch = curl_init();    // initialize curl handle
curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
curl_setopt($ch,CURLOPT_POST, true); // set POST method
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

// Set up the post fields. If you want to add custom fields, you would add them in Converge, and add the field name in the curlopt_postfields string.
 $q = "ssl_merchant_id=$merchantID&ssl_user_id=$merchantUserID&ssl_pin=$merchantPIN&ssl_transaction_type=ccsale&ssl_amount=10";
// echo $url.'?'.$q;
curl_setopt($ch,CURLOPT_POSTFIELDS, $q);
 
  
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_VERBOSE, true);
 
$result = curl_exec($ch); // run the curl procss
curl_close($ch); // Close cURL

  //shows the session token. 
    return $result;



        // $gateway = Omnipay::create('Elavon_Converge');
        // $gateway->setMerchantId('2108313');
        // $gateway->setUsername('Developer');
        // $gateway->setPassword('AO91ZNOUG6DMC0CZA2DTWJ2DR9YWINV4DXHQN5B48TD1RQQNP6LIU0Z1X8YHAKYU');

        // // Test mode hits the demo endpoint.
        // $gateway->setTestMode(true);

        // try {
        //     $params = array(
        //         'amount'                => 10.00,
        //         'card'                  => '4000000000000002',
        //         'ssl_invoice_number'    => 1,
        //         'ssl_show_form'         => 'false',
        //         'ssl_result_format'     => 'ASCII',
        //     );

        //     $response = $gateway->purchase($params)->send();

        //     if ($response->isSuccessful()) {
        //        dd( $response);
        //     } else {
        //         throw new ApplicationException($response->getMessage());
        //     }
        // } catch (ApplicationException $e) {
        //     throw new ApplicationException($e->getMessage());
        // }
    }
    
    public function getToken($order){
        $merchantID = "2108313"; //Converge 6 or 7-Digit Account ID *Not the 10-Digit Elavon Merchant ID*
$merchantUserID = "zingweb"; //Converge User ID *MUST FLAG AS HOSTED API USER IN CONVERGE UI*
$merchantPIN = "5MZE69UGM2CWPB49NTO2GYXYDFAFLI85FC3BUCYINP0L03JX84TO4X9I19TRS3L1"; //Converge PIN (64 CHAR A/N)

 $url = "https://api.convergepay.com/hosted-payments/transaction_token"; // URL to Converge demo session token server
//$url = "https://api.convergepay.com/hosted-payments/transaction_token"; // URL to Converge production session token server
 

// Read the following querystring variables
$firstname=user()->name; //Post first name
$lastname=user()->name; //Post first name
$amount=$order->total; //Post Tran Amount


$ch = curl_init();    // initialize curl handle
curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
curl_setopt($ch,CURLOPT_POST, true); // set POST method
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// Set up the post fields. If you want to add custom fields, you would add them in Converge, and add the field name in the curlopt_postfields string.
 $q = "ssl_merchant_id=$merchantID&ssl_user_id=$merchantUserID&ssl_pin=$merchantPIN&ssl_transaction_type=ccsale&ssl_amount=$amount";
// echo $url.'?'.$q;
curl_setopt($ch,CURLOPT_POSTFIELDS, $q);
 
  
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_VERBOSE, true);
 
$result = curl_exec($ch); // run the curl procss
curl_close($ch); // Close cURL

  //shows the session token. 
//   echo $result;
    return $result;
    }

public function paymentResponse(Request $request){
    $request = $request->all();
    if($request['status'] == 'approval'){
        Order::where('id',$request['order_id'])->update(['payment_status' => 'Paid','loyalty_points'  => (Cart::total()*10),'payment_details'  => !empty($request['payment_response'])? $request['payment_response']:'','billing_address_id' => !empty($request['billing_address_response'])? $request['billing_address_response'] : null]);
        $order = Order::where('id',$request['order_id'])->first();
        $user = user();
        Mail::send('cart::public.cart.message', ['order' => $order,'order_detail' => Cart::content()], function ($message) use ($user) {
           $message->from('support@zingmyorder.com','ZingMyOrder');
           $message->to($user->email)->subject('Zing Order Confirmation');
       });
       if(!empty($order->restaurant->confirmation_mail)){
           foreach(explode(',', $order->restaurant->confirmation_mail) as $confm_email){
               Mail::send('cart::public.cart.restaurant_mail', ['order' => $order,'order_detail' => Cart::content(),'user' => $user], function ($message) use ($user,$order,$confm_email) {
                   $message->from('support@zingmyorder.com','ZingMyOrder');
                   $message->to($confm_email)->subject('Zing Order Confirmation');
               });
           }
       }
       if(!empty($order->restaurant->confirmation_text_mail)){
           Mail::send('cart::public.cart.restaurant_text_mail', ['order' => $order,'order_detail' => Cart::content()], function ($message) use ($user,$order) {
           $message->from('support@zingmyorder.com','ZingMyOrder');
           $message->to($order->restaurant->confirmation_text_mail)->subject('Zing Order Confirmation');
            });
       }
        if(!empty($order->restaurant->confirmation_fax_mail)){
            Mail::send('cart::public.cart.restaurant_mail', ['order' => $order,'order_detail' => Cart::content(),'user' => $user], function ($message) use ($user,$order,$confm_email) {
               $message->from('support@zingmyorder.com','ZingMyOrder');
               $message->to($order->restaurant->confirmation_fax_mail)->subject('Zing Order Confirmation');
           });
        }
        Cart::destroy();
        return redirect()->to(url('success/'.$order->getRoutekey()));
    }
    else{
        Order::where('id',$request['order_id'])->update(['payment_details'  => !empty($request['payment_response'])? $request['payment_response']:'','billing_address_id' => !empty($request['billing_address_response'])? $request['billing_address_response'] : null]);

        return redirect()->to(url('payment/fail'));
    }
}
}
