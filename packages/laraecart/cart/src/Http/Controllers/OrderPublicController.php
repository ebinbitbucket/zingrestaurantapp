<?php

namespace Laraecart\Cart\Http\Controllers;

use Postmates\PostmatesClient;
use Postmates\Resources\DeliveryQuote;
use App\Http\Controllers\PublicController as BaseController;
use Laraecart\Cart\Interfaces\OrderRepositoryInterface;
use Auth;
use Restaurant\Restaurant\Models\Review;
use Restaurant\Kitchen\Models\Kitchen;
use Laraecart\Cart\Models\Order;
use Litepie\User\Models\Client;
use Illuminate\Http\Request;
use Restaurant\Kitchen\Interfaces\KitchenRepositoryInterface;
use BandwidthLib;
use Illuminate\Support\Facades\DB;
use Mail;
use Restaurant\Restaurant\Models\Restaurant;
//use DB;




class OrderPublicController extends BaseController
{
    // use OrderWorkflow;

    /**
     * Constructor.
     *
     * @param type \Laraecart\Order\Interfaces\OrderRepositoryInterface $order
     *
     * @return type
     */
    public function __construct(OrderRepositoryInterface $order,KitchenRepositoryInterface $kitchen)
    {
        $this->repository = $order;
        $this->kitchen    = $kitchen;
        parent::__construct();
    }

    /**
     * Show order's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $orders = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$cart::order.names'))
            ->view('cart::order.index')
            ->data(compact('orders'))
            ->output();
    }


    /**
     * Show order.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $order = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$order->name . trans('cart::order.name'))
            ->view('cart::order.show')
            ->data(compact('order'))
            ->output();
    }
public function getFeedbackForm($id)
    {
        $orders = $this->repository->findByField('id', hashids_decode($id))->first();
        Auth::guard('client.web')->loginUsingId($orders->user_id);
        $restaurant_reviews = Review::where('order_id',$orders->id)->whereNotNull('restaurant_id')->pluck('rating')->first();
        $menu_reviews = Review::where('order_id',$orders->id)->whereNotNull('menu_id')->pluck('rating','menu_id')->toArray();
        return $this->response->setMetaTitle(trans('cart::order.names') . ' Detail')
            ->view('cart::default.order.feedback')
            ->layout('blank')
            ->data(compact('orders','restaurant_reviews','menu_reviews'))
            ->output();
    }
    
    // public function kitchenNewOrderAppCount($apitoken)
    // {
    //     $kitchen = Kitchen::where('api_token', $apitoken)->first();

    //     $orders  = Order::where('restaurant_id', @$kitchen->restaurant->id)
    //         ->whereIn('order_status', ['New orders', 'Preparing'])
    //         ->whereDate('delivery_time', '=', date('Y-m-d'))
    //         ->where('payment_status', 'Paid')
    //         ->orderBy('id', 'DESC')
    //         ->where('notification_app', 0)
    //         ->count();

    //     Order::where('restaurant_id', @$kitchen->restaurant->id)
    //         ->where('notification_app', 0)
    //         ->whereIn('order_status', ['New orders', 'Preparing'])
    //         ->whereDate('delivery_time', '=', date('Y-m-d'))
    //         ->where('payment_status', 'Paid')
    //         ->update(['notification_app' => 1]);
    //     return response()->json(['status' => 'Success', 'new_order_count' => $orders]);

    // }
    
    // public function kitchenScheduledOrdersUpdate()
    // { 

    //     $time = date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))+ (60*60));
    //         $orders = Order::where('order_status','Scheduled')
    //                     ->where('delivery_time','<',$time)->where('payment_status','Paid')->update(['order_status' => 'New Orders']);

    //     return response()->json(['status' => 'Success']);
       
    // }
    
    public function kitchenNewOrderAppCount($id,$apitoken)
    {
        $kitchen = Kitchen::where('api_token', $apitoken)->first();
        $orders  = Order::where('restaurant_id', @$kitchen->restaurant->id)
            ->whereIn('order_status', ['New orders', 'Preparing'])
            ->whereDate('delivery_time', '=', date('Y-m-d'))
            ->where('payment_status', 'Paid')
            ->orderBy('id', 'DESC')
            ->where('id', '>', $id)->get();
        $max  = Order::where('restaurant_id', @$kitchen->restaurant->id)
                ->whereIn('order_status', ['New orders', 'Preparing'])
                ->whereDate('delivery_time', '=', date('Y-m-d'))
                ->where('payment_status', 'Paid')
                ->orderBy('id', 'DESC')
                ->first();
        if($max)
        {
            $maxId = $max->id;
        }
        else
        {
            $maxId = 1;
        }
        if(count($orders) > 0)
        {
            if(count($orders) > 0 && $id != 0)
            {
                return response()->json(['new_order' => true, 'max_id' => $orders[0]->id, 'new_order_count' => count($orders)]);
            } else
            {
                return response()->json(['new_order' => false, 'max_id' => $maxId]);
            }
        } else
        {
            return response()->json(['new_order' => false, 'max_id' => $maxId]);
        }
    }
    
    public function kitchenNewOrderAppCount1($apitoken)
    {
        $kitchen = Kitchen::where('api_token', $apitoken)->first();
        $orders  = Order::where('restaurant_id', @$kitchen->restaurant->id)
            ->whereIn('order_status', ['New orders', 'Preparing'])
            ->whereDate('delivery_time', '=', date('Y-m-d'))
            ->where('payment_status', 'Paid')
            ->orderBy('id', 'DESC')->pluck('id');
        return $orders;
    }
    public function carDelivery($id)
   {
      // dd(date('Y-m-d'));
    $order=Order::where('id',hashids_decode($id))->whereDate('delivery_time', '=', date('Y-m-d'))
    ->first();
    $order_detail =  $order->detail;
    if($order){
      
    if($order->is_car_pickup==1){
        return view('cart::public.order.complete_car_pickup',compact('order','order_detail'));
    }
    if($order->car_details){

        return view('cart::public.order.complete_car_details',compact('order','order_detail'));
    }
   return $this->response->setMetaTitle($order->name . trans('cart::order.name'))
   ->view('cart::public.order.car_pickup')
   ->data(compact('order'))
   ->output();
}
    //return view('cart::public.order.car_pickup', $order);
   }
   
   public function carDeliverySave(Request $request)
   { 
    $order = Order::where('id',hashids_decode($request->id))->whereDate('delivery_time', '=', date('Y-m-d'))->first();
    if($order){
       if(($order->order_status=='Cancelled')||($order->order_status=='New Orders')){
        return $this->response->setMetaTitle($order->name . trans('cart::order.name'))
        ->view('cart::public.order.car_pickup')
        ->data(compact('order'))
        ->output();
       }
       $cardetails=[
          'number'=>$request->number,
          'color'=>$request->color,
          'info'=>$request->info
       ];
       $datetime = date('Y-m-d H:i:s');
       $order_detail =  $order->detail;
       if($order->update(['car_details'=> $cardetails,'car_pickup_date'=>$datetime])){
        $this->orderPush($order);
        return view('cart::public.order.success',compact('order','order_detail'));

       }
        }    //car_details
   }
   public function refundRequest()
   {
       $order = [];
    return $this->response->setMetaTitle(trans('cart::order.name'))
    ->view('cart::public.order.refund_request')
    ->data(compact('order'))
    ->output();
   
    
   }
   public function resturantList()
   {
    $rest = Restaurant::where('published','Published')->get()->pluck('name')->toArray();

      return $rest;
    
   }
   
   public function refundSave(Request $request)
   { 
    $refunddetails =$request->all();
   // Session::put('location', $search['address']);


    $order = Order::where('id',$request->order_number)->first();
    if($order){
        $resname=$request->resturant;
        $rest_data = Restaurant::where('name','like','%'.$resname.'%')->first();
                     //   dd($rest_data);
if($rest_data){
        $orderRest = Order::where('id',$request->order_number)->where('restaurant_id',$rest_data->id)->first();
if($orderRest){
    if($order->restaurant->refund_email){
        $refund_email=$order->restaurant->refund_email;
    }else{
        $refund_email=$order->restaurant->email;
    }
    $user=$order->user;
    if(DB::table('order_refund')->insert($refunddetails)){
        Mail::send('cart::public.order.mail_refund', ['order' => $order,'user'=>$user ,'order_detail' => $order->detail,'refunddetails' => $refunddetails], function ($message) use ($user,$refunddetails) {
            $message->from('support@zingmyorder.com', 'ZingMyOrder');
            $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
            $message->to($refunddetails['email'])->subject('Zing Order Refund Request');
        });

        Mail::send('cart::admin.refund.mail_refund_rest', ['order' => $order,'order_detail' => $order->detail,'refunddetails' => $refunddetails], function ($message) use ($order,$refund_email) {
            $message->from('support@zingmyorder.com', 'ZingMyOrder');
            $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
            $message->to($refund_email)->subject('Zing Order Refund Request');
        });
    }
} else{
    echo 'no order in this restaurant';die();
}
} else{
    echo 'No  restaurant found this name';die();
}
} else{
    echo 'Invalid Order and Restaurant Combination';die();
}
return view('cart::public.order.refund_success');
   
    
   }


   public function orderPush($order){
    $kitchens = $this->kitchen->findKitchensByRestaurent($order->restaurant_id);      
    foreach($kitchens as $kitchen){
        $notfy ='Order From '.$order->name.',Delivery Time:'.$order->ready_time;
        //  $token =  Admintkn::select('email','token')->where('id', 1)->get()->first();
         // echo'<pre>';print_r($notfy);die();
           $id=$kitchen->fcm;
           if($id==''){
            continue;
           }
           // $id="daWGK2SlSLa7bUKA21hPO5:APA91bGjYejYx3ddOhxJx_yzau2pCz6ZJibjTcTa6lGh--TUor1PyPzhwSfiHeAkstt7WaGlmKzZFvz8Nxf_Dp_XeIeBM3Nwb7O9xGEEGDJbnrBG89sj1kcXvq5_o4WDZcJmHVCFKNuX";
           $url = 'https://fcm.googleapis.com/fcm/send';
           $fields = array (
               'to' =>  $id,
               'data' => $message = array(
                   "message" =>'You Have New Car PickUp Notification',
                   "body" => $notfy,
                   "refresh"=>1,

               )
            //    'notification' => $message = array(
            //        "title" =>'You Have New Notification',
            //         "body" => $notfy
            //    )
           );
           $fields = json_encode ( $fields );
           $headers = array (
               'Authorization: key=' . "AAAAgqaQ6u0:APA91bHgE0ikhOn86tA2d6Ky_b-xYnYBN_28946GR5GqhT6_igrobvHwDVJ0n2S-7PvQTB3HLgUoI0_IjH02D27hLYk9WB6Tf7wqiL04PsfsZ1_3It_39hamr6uqd2ufyhemaYCS9W32",
               'Content-Type: application/json'
           );
           $ch = curl_init ();
           curl_setopt ( $ch, CURLOPT_URL, $url );
           curl_setopt ( $ch, CURLOPT_POST, true );
           curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
           curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
           curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
   
           $result = curl_exec ( $ch );
         //  echo $result;
           curl_close ( $ch );
    }

 

}
   public function unsubscribe($user)
   {
        $attributes['unsubscribe']='true';
        $result=Client::where('id',hashids_decode($user))
            ->update($attributes);
        return view('cart::public.cart.unsubscribed');
   }
   public function kitchenScheduledOrdersUpdate()
   { 
       $time = date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))+ (60*60));
           $orders = Order::where('order_status','Scheduled')
                       ->where('delivery_time','<',$time)->where('payment_status','Paid')->update(['order_status' => 'New Orders']);
       return response()->json(['status' => 'Success']);
      
   }
   public function sevenDayData()
   { 
       $start = date('Y-m-d', strtotime('-7 days'));
       $endate = date('Y-m-d');
      // dd($start,$endate);

       $order = Order::select(DB::raw('count(orders.id) as order_count'),DB::raw('sum(total) as Total'),
       DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
       ->selectRaw('count(orders.id)/7 as average')

   ->whereBetween('delivery_time', array($start, $endate))
  -> first();    
  $data['order_count']=$order->order_count;
  $data['Total']=$order->Total;
  $data['EateryAmount']=$order->EateryAmount;
  $data['average']=$order->average;

  $today = Order::select(DB::raw('count(orders.id) as order_count'))
->whereDate('delivery_time', '=', date('Y-m-d'))
-> first();
//dd($today);
   $restaurant = Restaurant::select(DB::raw('count(restaurants.id) as rest_count'))->where('published','Published')
   ->whereBetween('created_at', array($start, $endate))
   -> first();    
   $restaurant_partners = Restaurant::select(DB::raw('count(restaurants.id) as rest_count'))->where('published','Published')
   -> first();   
   $data['today_order_count']=$today->order_count;
   $data['rest_count']=$restaurant->rest_count;
   $data['partners']=$restaurant_partners->rest_count;

   $start_mtd = date('Y-m-01');
   $endate_mtd = date('Y-m-d');
   $order_mtd = Order::select(DB::raw('count(orders.id) as order_count'))
->whereBetween('delivery_time', array($start_mtd, $endate_mtd))
-> first();    
$data['order_count_mtd']=$order_mtd->order_count;
$last_friday = date("Y-m-d", strtotime('friday last week'));
//$data['order_count_mtd']=$order_mtd->order_count;
$friday = Order::select(DB::raw('count(orders.id) as order_count'))
->whereDate('delivery_time', '=', $last_friday)
-> first();
$data['friday_order']=$friday->order_count;

//dd($data);

   return view('cart::public.cart.report_public',$data);

  // dd($data);

   }
   public function postmatesDelivery(){
    $clientspo = new PostmatesClient([
        'customer_id' => 'cus_Mj7_dJTcCsx1d-',
        'api_key' => 'cfb63acc-a8e5-4330-9bf5-6e49e493dd40'
    ]);

    $delivery_quote = new DeliveryQuote($clientspo);
  $res =  $delivery_quote->getQuote('501-525 Brannan St, San Francisco, CA 94107', '6 Colin P Kelly Jr St, San Francisco, CA 94107');
 print_r($res);
}

   
  
}
