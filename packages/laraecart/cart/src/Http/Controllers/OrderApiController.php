<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Restaurant\Restaurant\Interfaces\RestaurantRepositoryInterface;
use Auth;
use Crypt;
use Carbon\Carbon;
use Session;
use Laraecart\Cart\Models\Order;
use Laraecart\Cart\Http\Requests\OrderRequest;
use Laraecart\Cart\Interfaces\OrderRepositoryInterface;
use BandwidthLib;
use Illuminate\Http\Request;
use DB;



class OrderApiController extends BaseController
{

    /**
     * Constructor.
     *
     * @param type \
     *
     * @return type
     */
    public function __construct(RestaurantRepositoryInterface $restaurant,OrderRepositoryInterface $order)
    {
     
        $this->repository = $order;

        parent::__construct();
    }

    protected function getAllOrder(OrderRequest $request)
    {
      $orders = $this->repository->getNewOrders();
     // dd($orders);
       return response()->json(['orders' =>  $orders]);
    }
    public function OrderAutomatiCall()
{ 
  //dd(url('api/orders/call-voice'));
  //$*9-Qa!+ = date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))- (10*60));
  $orders =Order::where('order_status','New Orders')->whereDate('delivery_time', '=', date('Y-m-d'))->Where('call_count', '<', 2)->where('payment_status','Paid')
->get()->groupBy('restaurant_id');
//dd($orders);
              foreach($orders as $order){ 
                $order = $order->first();
                if($order->call_count==1){
                  $time = date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))- ($order->restaurant->second_call_delay*60));
                } else{
                  $time = date("Y-m-d H:i:s",strtotime(date('Y-m-d H:i:s'))- ($order->restaurant->reminder*60));
                }
              //  dd($order->delivery_time);
                if($order->delivery_time<$time){
   if($order->restaurant->delay_call_number){
$ph_number = '+'.preg_replace("/[^0-9]/", "", $order->restaurant->delay_call_number);
$count_call = $order->call_count+1;
$date = date('Y-m-d H:i:s');
$data = [
  'order_id'=>$order->id,
  'restaurant_id'=>$order->restaurant->id,
  'call_count'=>$count_call,
  'datetime'=>$date
];
DB::table('call_log')->insert($data);

        
   }

                }


              }
            //  dd($orders);

   
}
public function OrderCallVoice(){
  //('dd');
//dd(url('themes/restaurant/zing.wav'));
  $playAudio1 = new BandwidthLib\Voice\Bxml\PlayAudio(url('themes/restaurant/zing.wav'));
//$playAudio2 = new BandwidthLib\Voice\Bxml\PlayAudio("https://audio.url/audio2.wav");
$response = new BandwidthLib\Voice\Bxml\Response();
$response->addVerb($playAudio1);
//$response->addVerb($playAudio2);

echo $response->toBxml();
echo "\n";
}




}
