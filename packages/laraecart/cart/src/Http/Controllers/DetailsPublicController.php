<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Laraecart\Cart\Interfaces\DetailsRepositoryInterface;

class DetailsPublicController extends BaseController
{
    // use DetailsWorkflow;

    /**
     * Constructor.
     *
     * @param type \Laraecart\Details\Interfaces\DetailsRepositoryInterface $details
     *
     * @return type
     */
    public function __construct(DetailsRepositoryInterface $details)
    {
        $this->repository = $details;
        parent::__construct();
    }

    /**
     * Show details's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $details = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$cart::details.names'))
            ->view('cart::details.index')
            ->data(compact('details'))
            ->output();
    }


    /**
     * Show details.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $details = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$details->name . trans('cart::details.name'))
            ->view('cart::details.show')
            ->data(compact('details'))
            ->output();
    }

}
