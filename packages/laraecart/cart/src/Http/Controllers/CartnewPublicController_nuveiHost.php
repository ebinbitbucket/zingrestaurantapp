<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use App\User;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use Laraecart\Cart\Facades\Cart;
use Laraecart\Cart\Interfaces\AddressRepositoryInterface;
use Laraecart\Cart\Interfaces\CartRepositoryInterface;
use Laraecart\Cart\Interfaces\DetailsRepositoryInterface;
use Laraecart\Cart\Interfaces\OrderRepositoryInterface;
use Laraecart\Cart\Models\Address;
use Laraecart\Cart\Models\Order;
use Laraecart\Cart\Models\TransactionLogs;
use Mail;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;
use PayPalCheckout\PayPalClient;
use Request as Requests;
use Restaurant\Restaurant\Interfaces\MenuRepositoryInterface;
use Restaurant\Restaurant\Models\Restaurant;
use Restaurant\Restaurant\Models\Restauranttimings;
use Session;
require('nuvei_account.inc');
// require('gateway_tps_xml.php');
require('nuvei_hpp_functions.inc');

class CartnewPublicController1 extends BaseController
{

// use CartWorkflow;

    /**
     * Constructor.
     *
     * @param type \Laraecart\Cart\Interfaces\CartRepositoryInterface $cart
     *
     * @return type
     */
    public function __construct(CartRepositoryInterface $cart, MenuRepositoryInterface $menu, OrderRepositoryInterface $order, DetailsRepositoryInterface $detail, AddressRepositoryInterface $address)
    {

        if (!empty(app('auth')->getDefaultDriver())) {

            $this->middleware('auth:' . app('auth')->getDefaultDriver(), ['only' => ['getCheckout']]);
        }

        $this->repository = $cart;
        $this->menu       = $menu;
        $this->order      = $order;
        $this->detail     = $detail;
        $this->address    = $address;
        parent::__construct();
    }

    public function getCheckout()
    {
        $this->response->theme->asset()->container('extra')->usepath()->add('checkoutview', 'js/checkoutview.js');
        $carts = Cart::content();

        $weekday = strtolower(date('D'));

        if (Cart::count() > 0) {

            foreach ($carts as $cart) {
                $rest_id    = $cart->options->restaurant;
                $cart_id    = $cart->rowId;
                $restaurant = Restaurant::where('id', $rest_id)->first();
            }

            Session::put('url.intended', url('/cart/checkout'));

            $timings = $restaurant->timings->where('day', $weekday);
            if (Auth::user()) {
                $exists = TransactionLogs::where('cart_id', $cart_id)->where('type', 'cart')->count();
                // if($exists ==0){
                $attributes['user_id']       = user_id();
                $attributes['type']          = 'cart';
                $attributes['total_amount']  = Cart::total();
                $attributes['json_data']     = Cart::content();
                $attributes['cart_id']       = $cart_id;
                $attributes['ip_address']    = request()->ip();
                $attributes['date']          = date('Y-m-d H:i:s');
                $attributes['url']           = Requests::path();
                $attributes['restaurant_id'] = $rest_id;
                $attributes['server_variables'] = Requests::server();
		        $attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
                $useragent                   = $_SERVER['HTTP_USER_AGENT'];

                if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                    $attributes['source'] = 'mobile';
                } else {
                    $attributes['source'] = 'desktop';
                }

                $log = TransactionLogs::create($attributes);
                // }

            }

            $view = 'cart::public.cart.checkout.checkout';
        } else {
            $view = 'cart::public.cart.checkout.emptycheckout';
        }

        return $this->response->setMetaTitle('Checkout ZingMyOrder')
            ->view($view)
            ->layout('blankcheckout')
            ->data(compact('carts', 'address', 'timings'))
            ->output();
    }

    public function add($rowId)
    {
        Cart::increment($rowId, $qty = 1);
        return view('cart::public.cart.checkout.updated_cart');
    }

    public function subtract($rowId)
    {
        Cart::decrement($rowId, $qty = 1);
        return view('cart::public.cart.checkout.updated_cart');
    }

    public function checkFields(Request $request)
    {
        $request = $request->all();
        $carts   = Cart::content();

        foreach ($carts as $item) {

            foreach ($item->options as $key => $option) {

                if ($key == 'restaurant') {
                    $restaurant = $option;
                }

            }

        }

        $restaurant_data = Restaurant::where('id', $restaurant)->first();

// $order['name'] = $request['cus_name'];
        // $order['phone'] = $request['phone'];

        $deliveryTime = $this->repository->checkingDeliveryTime($request);

        if (date('y-m-d H:i') >= date('y-m-d H:i', strtotime($deliveryTime['delivery_time']))) {
            return response()->json(['status' => 'Error', 'msg' => 'Previous Time']);
        }

        if ($request['order_type'] == 'Delivery' && empty($request['address_id'])) {

            Session::flash('error', 'Please add address');
            return response()->json(['status' => 'Error', 'msg' => 'Delivery_address']);
        } elseif ($request['order_type'] == 'Delivery' && !empty($request['address_id'])) {
            $check_address = Address::where('id', $request['address_id'])->first();
            $distance      = $this->repository->checkAddress($request, $restaurant_data, $check_address);

            if ($check_address->longitude == null) {
                return response()->json(['status' => 'Error', 'msg' => 'address_error']);
            }

            if (!empty($restaurant_data->delivery_limit) && $distance[0]->distance > $restaurant_data->delivery_limit) {
                Session::flash('error', "This Restauarnt can't take orders at this time.");
                return response()->json(['status' => 'Error', 'msg' => 'delivery_limit', 'delivery_limit' => $restaurant_data->delivery_limit]);
            } else {
                $order['address_id'] = $request['address_id'];
            }

            $address_default = Address::where('client_id', user_id())->where('default_address', 1)->first();

            if (empty($address_default)) {

                if (!empty($request['address_id'])) {
                    Address::where('id', $request['address_id'])->update(['default_address' => 1]);
                }

            } elseif ($address_default->id != $request['address_id']) {
                Address::where('id', $address_default->id)->update(['default_address' => 0]);
                Address::where('id', $request['address_id'])->update(['default_address' => 1]);
            }

        }

        if (empty($request['billing'])) {
            Session::flash('error', 'Please add address');
            return response()->json(['status' => 'Error', 'msg' => 'Billing address']);
        } else {
            $order['billing_address_id'] = $request['billing'];
        }

        if ($request['order_type'] == 'Pickup') {
            $order['address_id'] = null;
        }

        if (empty($delivery_charge) || $delivery_charge == '') {
            $delivery_charge = 0;
        }

        if (empty($request['tip']) || $request['tip'] == '') {
            $request['tip'] = 0;
        }

        $amount                   = Cart::total();
        $order['order_type']      = $request['order_type'];
        $order['tax']             = !empty($request['tax']) ? $request['tax'] : 0;
        $order['discount_points'] = (!empty($request['discount']) ? $request['discount'] : 0) * 2000;
        $order['loyalty_points']  = 0;
        $order['tip']             = $request['tip'];
        $order['tip_value_cus']   = $request['tip_value_cus'];

        if ($order['tip_value_cus'] != null) {
            $order['tip_value'] = null;
        } else {
            $order['tip_value'] = $request['tip_value'];
        }

        if (empty($request['discount_amount'])) {
            $request['discount_amount'] = 0;
        }

        $order['delivery_charge'] = $delivery_charge;
        $order['total']           = $amount + $delivery_charge + $order['tip'] + $order['tax'] - (!empty($request['discount']) ? $request['discount'] : 0) - $request['discount_amount'];
        $order['subtotal']        = Cart::subtotal();
        // $order['payment_details'] = 'paypal';
        $order['payment_status'] = 'UnPaid';

        $order['tax_rate'] = $restaurant_data->tax_rate;
        $order['CCR']      = $restaurant_data->CCR;
        $order['CCF']      = $restaurant_data->CCF;
        $order['ZR']       = $restaurant_data->ZR;
        $order['ZF']       = $restaurant_data->ZF;
        $order['ZC']       = $restaurant_data->ZC;
        $restaurant_det    = Restauranttimings::where('restaurant_id', $restaurant)->where('day', substr(strtolower(date('l', strtotime($order['delivery_time']))), 0, 3))->whereNull('deleted_at')->get();
        $flag              = 0;

        if (empty($restaurant_det)) {
            $flag = 1;
        }

        foreach ($restaurant_det as $key_time => $value_time) {

            if (date('H:i', strtotime($value_time->opening)) <= date('H:i', strtotime($order['delivery_time'])) && date('H:i', strtotime($value_time->closing)) >= date('H:i', strtotime($order['delivery_time']))) {
                $flag = 1;
                break;
            }

        }

        if ($flag != 1) {
            Session::flash('error', "This Restauarnt can't take orders at this time.");
            return response()->json(['status' => 'Error', 'msg' => 'restaurant_time']);
        }

        if ($restaurant_data->min_order_count > $order['subtotal']) {
            Session::flash('error', "This Restauarnt can't take orders at this time.");
            return response()->json(['status' => 'Error', 'msg' => 'min_order_count', 'min_amt' => $restaurant_data->min_order_count]);
        }

        $order['restaurant_id'] = $restaurant;

        $order['user_id'] = user_id();

        if (Auth::user()) {
            $order['user_type'] = 'App\Client';
        } else {
            $order['user_type'] = user_type();
        }

        $order['discount_amount'] = $request['discount_amount'];

    }

    public function getToken(Request $request)
    {
        $merchantID     = "2108313"; //Converge 6 or 7-Digit Account ID *Not the 10-Digit Elavon Merchant ID*
        $merchantUserID = "zingweb"; //Converge User ID *MUST FLAG AS HOSTED API USER IN CONVERGE UI*
        $merchantPIN    = "5MZE69UGM2CWPB49NTO2GYXYDFAFLI85FC3BUCYINP0L03JX84TO4X9I19TRS3L1"; //Converge PIN (64 CHAR A/N)

        $url = "https://api.convergepay.com/hosted-payments/transaction_token";

// URL to Converge demo session token server

//$url = "https://api.convergepay.com/hosted-payments/transaction_token"; // URL to Converge production session token server

// Read the following querystring variables
        $firstname = user()->name; //Post first name
        $lastname  = user()->name; //Post first name
        $amount    = number_format($request['total'], 2); //Post Tran Amount

        $ch = curl_init(); // initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
        curl_setopt($ch, CURLOPT_POST, true); // set POST method
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
// Set up the post fields. If you want to add custom fields, you would add them in Converge, and add the field name in the curlopt_postfields string.
        $q = "ssl_merchant_id=$merchantID&ssl_user_id=$merchantUserID&ssl_pin=$merchantPIN&ssl_transaction_type=ccsale&ssl_amount=$amount";
// echo $url.'?'.$q;
        curl_setopt($ch, CURLOPT_POSTFIELDS, $q);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true);

        $result = curl_exec($ch); // run the curl procss
        curl_close($ch);

// Close cURL

//shows the session token.
        //   echo $result;
        return response()->json(['result' => $result]);
    }

    public function paymentSubmit(Request $request)
    {
        $request = $request->all();
        $carts   = Cart::content();

        foreach ($carts as $item) {

            foreach ($item->options as $key => $option) {

                if ($key == 'restaurant') {
                    $restaurant = $option;
                }

            }

        }

        $restaurant_data = Restaurant::where('id', $restaurant)->first();
        $order['name']   = user()->name;
        $order['phone']  = user()->mobile;

        if ($request['order_type'] == 'Pickup') {
            $order['address_id'] = null;
        } else {
            $order['address_id'] = $request['address_id'];
            $address_default     = Address::where('client_id', user_id())->where('default_address', 1)->first();

            if (empty($address_default)) {

                if (!empty($request['address_id'])) {
                    Address::where('id', $request['address_id'])->update(['default_address' => 1]);
                }

            } elseif ($address_default->id != $request['address_id']) {
                Address::where('id', $address_default->id)->update(['default_address' => 0]);
                Address::where('id', $request['address_id'])->update(['default_address' => 1]);
            }

        }

        $preparation_time = 0;

        foreach ($carts as $item) {
            $order['subtotal'] = $item->price;

            foreach ($item->options as $keys => $optiond) {

                if ($keys == 'delivery_charge' && $request['order_type'] != 'Pickup') {
                    $delivery_charge = $optiond;
                } else

                if ($keys == 'preparation_time') {

                    if (is_numeric($optiond)) {
                        $preparation_time = $preparation_time + $optiond;
                    } else {
                        $preparation_time = $preparation_time + 10;
                    }

                }

            }

        }

        if ($request['time_type'] == 'ASAP') {
            $time                   = strtotime(($preparation_time + 5) . ' minute');
            $order['delivery_time'] = date('y-m-d H:i:s', $time);
            $order['order_status']  = 'New Orders';

        } else {
            $date                   = new DateTime($request['scheduled_date']);
            $time                   = new DateTime($request['scheduled_time']);
            $combined               = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));
            $order['delivery_time'] = $combined->format('y-m-d H:i:s');
            $order['order_status']  = 'Scheduled';
        }

        if (empty($delivery_charge) || $delivery_charge == '') {
            $delivery_charge = 0;
        }

        if (empty($request['tip']) || $request['tip'] == '') {
            $request['tip'] = 0;
        }

        $amount                   = Cart::total();
        $order['order_type']      = $request['order_type'];
        $order['tax']             = !empty($request['tax']) ? $request['tax'] : 0;
        $order['discount_points'] = (!empty($request['discount']) ? $request['discount'] : 0) * 2000;
        $order['loyalty_points']  = 0;
        $order['tip']             = $request['tip'];
        $order['tip_value_cus']   = $request['tip_value_cus'];

        if ($order['tip_value_cus'] != null) {
            $order['tip_value'] = null;
        } else {
            $order['tip_value'] = $request['tip_value'];
        }

        if (empty($request['discount_amount'])) {
            $request['discount_amount'] = 0;
        }

        if (empty($request['min_order_difference'])) {
            $request['min_order_difference'] = 0;
        }

        $order['delivery_charge'] = $delivery_charge;
        $order['total']           = $amount + $delivery_charge + $order['tip'] + $order['tax'] - (!empty($request['discount']) ? $request['discount'] : 0) - $request['discount_amount'] + $request['min_order_difference'];
        $order['subtotal']        = Cart::subtotal();
        // $order['payment_details'] = 'paypal';
        $order['payment_status'] = 'Paid';

        $order['tax_rate']             = $restaurant_data->tax_rate;
        $order['CCR']                  = $restaurant_data->CCR;
        $order['CCF']                  = $restaurant_data->CCF;
        $order['ZR']                   = $restaurant_data->ZR;
        $order['ZF']                   = $restaurant_data->ZF;
        $order['ZC']                   = $restaurant_data->ZC;
        $order['restaurant_id']        = $restaurant;
        $order['min_order_difference'] = $request['min_order_difference'];

        $order['user_id'] = user_id();

        if (Auth::user()) {
            $order['user_type'] = 'App\Client';
        } else {
            $order['user_type'] = user_type();
        }

        $order['discount_amount'] = $request['discount_amount'];

        if ($request['status'] == 'approval') {
            $order             = $this->order->create($order);
            $field['order_id'] = $order->id;

            foreach ($carts as $item) {
                $field['menu_id'] = $item->id;

                $field['quantity']   = $item->qty;
                $field['unit_price'] = $item->price;

                foreach ($item->options as $key => $option) {

                    if ($key == 'menu_addon') {
                        $field['menu_addon'] = $option;
                    } elseif ($key == 'special_instr') {
                        $field['special_instr'] = $option;
                    } elseif ($key == 'addons') {
                        $field['addons'] = $option;
                    }

                }

                $field['price']     = $item->price * $item->qty;
                $field['user_type'] = user_type();
                $field['user_id']   = user_id();
                $detail             = $this->detail->create($field);
            }

            $order = $this->paymentResponse($order, $request);
            return response()->json(['status' => 'success', 'order' => $order->getRoutekey()]);
            // return redirect()->to(url('success/'.$order->getRoutekey()));

        } else {
            $payment_response = json_decode($request['payment_response']);
            $msg              = 'Invalid Payment Details. ';

            if (!empty($payment_response->ssl_avs_response)) {
                $attributes['avs_code'] = $payment_response->ssl_avs_response;
                $attributes['card']     = $payment_response->ssl_card_number;
                $count                  = TransactionLogs::where('card', $payment_response->ssl_card_number)->where('date', '>', date('Y-m-d H:i:s', strtotime('-1 day')))->count();

                if ($count >= 1) {
                    $msg = $msg . "\nYou card was declined twice. Due to the security standards we abide by you will have to use another card to complete the order.";
                } else {
                    $msg = $msg . "\nKindly ensure the details are correct for your next try. If the same card number is declined again we can’t process it for another 24 hours.";
                }

            }

            $attributes['user_id']       = user_id();
            $attributes['type']          = 'card';
            $attributes['total_amount']  = Cart::total();
            $attributes['json_data']     = $payment_response;
            $attributes['ip_address']    = request()->ip();
            $attributes['date']          = date('Y-m-d H:i:s');
            $attributes['url']           = Requests::path();
            $attributes['restaurant_id'] = $restaurant;
            $attributes['server_variables'] = Requests::server();
		    $attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
            $useragent                   = $_SERVER['HTTP_USER_AGENT'];

            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                $attributes['source'] = 'mobile';
            } else {
                $attributes['source'] = 'desktop';
            }

            $log = TransactionLogs::create($attributes);
            return response()->json(['status' => 'error', 'msg' => $msg]);
        }

    }

    public function paymentResponse($order, $request)
    {
        Order::where('id', $order->id)->update(['payment_status' => 'Paid', 'loyalty_points' => (Cart::total() * 10), 'payment_details' => !empty($request['payment_response']) ? $request['payment_response'] : '', 'billing_address_id' => !empty($request['billing_address_response']) ? $request['billing_address_response'] : null]);
        $order = Order::where('id', $order->id)->first();
        $user  = user();

        Mail::send('cart::public.cart.message', ['order' => $order, 'order_detail' => Cart::content()], function ($message) use ($user) {
            $message->from('support@zingmyorder.com', 'ZingMyOrder');
            $message->to($user->email)->subject('Zing Order Confirmation');
        });

        if (!empty($order->restaurant->confirmation_mail)) {

            foreach (explode(',', $order->restaurant->confirmation_mail) as $confm_email) {
                Mail::send('cart::public.cart.restaurant_mail', ['order' => $order, 'order_detail' => Cart::content(), 'user' => $user], function ($message) use ($user, $order, $confm_email) {
                    $message->from('support@zingmyorder.com', 'ZingMyOrder');
                    $message->to($confm_email)->subject('Zing Order Confirmation');
                });
            }

        }

        if (!empty($order->restaurant->confirmation_text_mail)) {
            Mail::send('cart::public.cart.restaurant_text_mail', ['order' => $order, 'order_detail' => Cart::content()], function ($message) use ($user, $order) {
                $message->from('support@zingmyorder.com', 'ZingMyOrder');
                $message->to($order->restaurant->confirmation_text_mail)->subject('Zing Order Confirmation');
            });
        }

        if (!empty($order->restaurant->confirmation_fax_mail)) {
            Mail::send('cart::public.cart.restaurant_mail', ['order' => $order, 'order_detail' => Cart::content(), 'user' => $user], function ($message) use ($user, $order, $confm_email) {
                $message->from('support@zingmyorder.com', 'ZingMyOrder');
                $message->to($order->restaurant->confirmation_fax_mail)->subject('Zing Order Confirmation');
            });
        }

        Cart::destroy();
        return $order;
    }

    public function paypalSubmit(Request $request)
    {

        $orderId  = $request->get('orderId');
        $amount   = $request->get('amount');
        $currency = $request->get('currency');
        $client   = PayPalClient::client();
        $response = $client->execute(new OrdersGetRequest($orderId));

// print "Status Code: {$response->statusCode}\n";

// print "Status: {$response->result->status}\n";

// print "Order ID: {$response->result->id}\n";

// print "Intent: {$response->result->intent}\n";

// print "Links:\n";
        foreach ($response->result->links as $link) {
            // print "\t{$link->rel}: {$link->href}\tCall Type: {$link->method}\n";
        }

// 4. Save the transaction in your database. Implement logic to save transaction to your database for future reference.

// print "Gross Amount: {$response->result->purchase_units[0]->amount->currency_code} {$response->result->purchase_units[0]->amount->value}\n";
        if ($response->result->status == 'COMPLETED') {
            parse_str($request->get('data'), $data);
            $req = $request->all();
            $req = array_merge($req, $data);

            $order_save  = $this->repository->orderSave($req);
            $order_mails = $this->repository->orderMail($order_save, $request);
            return response()->json(['Status' => $response->result->status, 'order_id' => $order_save->getRoutekey()]);
        } else {
            $payment_response = $response->result->status;
            $msg              = 'Invalid Payment Details. ';

// if(!empty($payment_response->ssl_avs_response)){

//     $attributes['avs_code'] = $payment_response->ssl_avs_response;

//     $attributes['card'] = $payment_response->ssl_card_number;

//     $count = TransactionLogs::where('card',$payment_response->ssl_card_number)->where('date','>',date('Y-m-d H:i:s', strtotime('-1 day')))->count();

//     if($count >= 1){

//         $msg = $msg . "\nYou card was declined twice. Due to the security standards we abide by you will have to use another card to complete the order.";

//     }

//     else{

//         $msg = $msg . "\nKindly ensure the details are correct for your next try. If the same card number is declined again we can’t process it for another 24 hours.";

//     }

            // }
            $carts = Cart::content();

            foreach ($carts as $cart) {
                $rest_id    = $cart->options->restaurant;
                $cart_id    = $cart->rowId;
                $restaurant = Restaurant::where('id', $rest_id)->first();
            }

            $attributes['user_id']       = user_id();
            $attributes['type']          = 'card';
            $attributes['total_amount']  = Cart::total();
            $attributes['cart_id']       = $cart_id;
            $attributes['json_data']     = $payment_response;
            $attributes['ip_address']    = request()->ip();
            $attributes['date']          = date('Y-m-d H:i:s');
            $attributes['url']           = Requests::path();
            $attributes['restaurant_id'] = $rest_id;
            $attributes['server_variables'] = Requests::server();
		    $attributes['HTTP_REFERER'] = Requests::server('HTTP_REFERER');
            $useragent                   = $_SERVER['HTTP_USER_AGENT'];

            if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i', $useragent) || preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($useragent, 0, 4))) {
                $attributes['source'] = 'mobile';
            } else {
                $attributes['source'] = 'desktop';
            }

            $log = TransactionLogs::create($attributes);
            return response()->json(['status' => 'error', 'msg' => $msg]);
        }

    }

    public function nuviewSubmit1(Request $request){
        $request = $request->all();
        # These values are specific to the cardholder.
$cardNumber = $request['card'];       # This is the full PAN (card number) of the credit card OR the SecureCard Card Reference if using a SecureCard. It must be digits only (i.e. no spaces or other characters).
$cardType = 'Visa Credit';         # See our Integrator Guide for a list of valid Card Type parameters
$cardExpiry = str_replace('-20', '', $request['cc-exp']);       # (if not using SecureCard) The 4 digit expiry date (MMYY).
$cardHolderName = user()->name;       # (if not using SecureCard) The full cardholders name, as it is displayed on the credit card.
$cvv = $request['cvv'];          # (optional) 3 digit (4 for AMEX cards) security digit on the back of the card.
$issueNo = '';          # (optional) Issue number for Switch and Solo cards.
$email = '';            # (optional) If this is sent then Nuvei will send a receipt to this e-mail address.
$mobileNumber = "";     # (optional) Cardholders mobile phone number for sending of a receipt. Digits only, Include international prefix.

# These values are specific to the transaction.
$orderId = str_random(12);  # This should be unique per transaction (12 character max).
$amount = $request['total_amount'];           # This should include the decimal point.
$isMailOrder = false;       # If true the transaction will be processed as a Mail Order transaction. This is only for use with Mail Order enabled Terminal IDs.

# These fields are for AVS (Address Verification Check). This is only appropriate in the UK and the US.
$address1 = '';         # (optional) This is the first line of the cardholders address.
$address2 = '';         # (optional) This is the second line of the cardholders address.
$postcode = '';         # (optional) This is the cardholders post code.
$country = '';          # (optional) This is the cardholders country name.
$phone = '';            # (optional) This is the cardholders home phone number.

# eDCC fields. Populate these if you have retreived a rate for the transaction, offered it to the cardholder and they have accepted that rate.
$cardCurrency = '';     # (optional) This is the three character ISO currency code returned in the rate request.
$cardAmount = '';       # (optional) This is the foreign currency transaction amount returned in the rate request.
$conversionRate = '';       # (optional) This is the currency conversion rate returned in the rate request.

# 3D Secure reference. Only include if you have verified 3D Secure throuugh the Nuvei MPI and received an MPIREF back.
$mpiref = '';           # This should be blank unless instructed otherwise by Nuvei.
$deviceId = '';         # This should be blank unless instructed otherwise by Nuvei.

$autoready = '';        # (optional) (Y/N) Whether or not this transaction should be marked with a status of "ready" as apposed to "pending".
$multicur = false;      # This should be false unless instructed otherwise by Nuvei.

$description = '';      # (optional) This can is a description for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
$autoReady = '';        # (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.

# Set up the authorisation object
$auth = new XmlAuthRequest($terminalId,$orderId,$currency,$amount,$cardNumber,$cardType);
if($cardType != "SECURECARD") $auth->SetNonSecureCardCardInfo($cardExpiry,$cardHolderName);
if($cvv != "") $auth->SetCvv($cvv);
if($cardCurrency != "" && $cardAmount != "" && $conversionRate != "") $auth->SetForeignCurrencyInformation($cardCurrency,$cardAmount,$conversionRate);
if($email != "") $auth->SetEmail($email);
if($mobileNumber != "") $auth->SetMobileNumber($mobileNumber);
if($description != "") $auth->SetDescription($description);
 
if($issueNo != "") $auth->SetIssueNo($issueNo);
if($address1 != "" && $address2 != "" && $postcode != "") $auth->SetAvs($address1,$address2,$postcode);
if($country != "") $auth->SetCountry($country);
if($phone != "") $auth->SetPhone($phone);
 
if($mpiref != "") $auth->SetMpiRef($mpiref);
if($deviceId != "") $auth->SetDeviceId($deviceId);
 
if($multicur) $auth->SetMultiCur();
if($autoready) $auth->SetAutoReady($autoready);
if($isMailOrder) $auth->SetMotoTrans();
 
# Perform the online authorisation and read in the result
$response = $auth->ProcessRequestToGateway($secret,$testAccount, $gateway);
 
 
 
$expectedResponseHash = md5($terminalId . $response->UniqueRef() . ($multicur == true ? $currency : '') . $amount . $response->DateTime() . $response->ResponseCode() . $response->ResponseText() . $secret);
 
if($response->IsError()) echo 'AN ERROR OCCURED! You transaction was not processed. Error details: ' . $response->ErrorString();
elseif($expectedResponseHash == $response->Hash()) {
    switch($response->ResponseCode()) {
        case "A" :  # -- If using local database, update order as Authorised.
                echo 'Payment Processed successfully. Thanks you for your order.';
                $uniqueRef = $response->UniqueRef();
                $responseText = $response->ResponseText();
                $approvalCode = $response->ApprovalCode();
                $avsResponse = $response->AvsResponse();
                $cvvResponse = $response->CvvResponse();
                break;
        case "R" :
        case "D" :
        case "C" :
        case "S" :
 
        default  :  # -- If using local database, update order as declined/failed --
                echo 'PAYMENT DECLINED! Please try again with another card. Bank response: ' . $response->ResponseText();
    }
} else {
    $uniqueReference = $response->UniqueRef();
    echo 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact <a href="mailto:' . $adminEmail . '">' . $adminEmail . '</a> or call ' . $adminPhone . ' to clarify if you will get charged for this order.';
    if(isset($uniqueReference)) echo 'Please quote Nuvei Terminal ID: ' . $terminalId . ', and Unique Reference: ' . $uniqueReference . ' when mailing or calling.';
 }
 
    }
    public function nuviewSubmit(Request $request){
        $request = $request->all();

$orderId = str_random(12);;          # This should be unique per transaction.
$amount = $request['total_amount'];           # This should include the decimal point.

$email = '';            # (optional) If this is sent then Nuvei will send a receipt to this e-mail address.
$description = '';      # (optional) This can is a decription for the transaction that will be available in the merchant notification e-mail and in the SelfCare  system.
$autoReady = 'Y';       # (optional) Y or N. Automatically set the transaction to a status of Ready in the batch. If not present the terminal default will be used.

$cardholderName = '';   # (optional) If the cardholders name is available it should be populated here. If so it will be pre-populated on the payment page.
$address1 = '';         # (optional) This is the first line of the cardholders billing address.
$address2 = '';         # (optional) This is the second line of the cardholders billing address.
$postcode = '';         # (optional) This is the postcode of the cardholders billing address.
$host = trans_url('/');             # This should your host eg. http://localhost:8000
$dateTime = requestDateTime();
 $terminalId = '29675001';
 $currency = 'USD';
 $receiptPageURL = trans_url('cart/checkout/receiptform');
# If there's no orderId set then generate a unique time-based order ID.
if(!isset($orderId) || $orderId == '') $orderId = generateUniqueOrderId();
 
# ------ Add order to the local database here if using one ------

# Verification string
$requestHash = authRequestHash($orderId, $amount, $dateTime);
 
$requestURL = $host.'/merchant/paymentpage';
            # Write the HTML of the submission form
echo "<html><body><form id='nuveiform' action='" . $requestURL . "' method='post'>\n";
writeHiddenField("TERMINALID", $terminalId);
writeHiddenField("CURRENCY", $currency);
writeHiddenField("ORDERID", $orderId);
writeHiddenField("AMOUNT", $amount);
writeHiddenField("DATETIME", $dateTime);
if(isset($cardholderName) && $cardholderName != '') writeHiddenField("CARDHOLDERNAME", $cardholderName);
if(isset($postcode) && $postcode != '') {
    writeHiddenField("ADDRESS1", $address1);
    writeHiddenField("ADDERSS2", $address2);
    writeHiddenField("POSTCODE", $postcode);
}
if(isset($email) && $email != '') writeHiddenField("EMAIL", $email);
if(isset($description) && $description != '') writeHiddenField("DESCRIPTION", $description);
if(isset($autoReady) && $autoReady != '') writeHiddenField("AUTOREADY", $autoReady);
writeHiddenField("RECEIPTPAGEURL", $receiptPageURL);
// if($validationURL != '') writeHiddenField("VALIDATIONURL", $validationURL);
writeHiddenField("HASH", $requestHash);
 
# You can also include any other custom fields here. Their contents will for included in the response POST to the receipt page.
# writeHiddenField("Customer ID", '32856951');

# Write the JavaScript that will submit the form to Nuvei.
echo '</form>Submitting order to Nuvei for Payment...<script language="JavaScript">document.getElementById("nuveiform").submit();</script></body></html>';
 
    }

    public function getReceipt(){
        if(authResponseHashIsValid($_REQUEST["ORDERID"], $_REQUEST["AMOUNT"], $_REQUEST["DATETIME"], $_REQUEST["RESPONSECODE"], $_REQUEST["RESPONSETEXT"], $_REQUEST["HASH"])) {
    switch($_REQUEST["RESPONSECODE"]) {
        case "A" :  # -- If using local database, update order as Paid/Successful
                echo 'Payment Processed successfully. Thanks you for your order.';
                break;
        case "R" :
        case "D" :
        case "C" :
        case "S" :
        default  :  # -- If using local database, update order as declined/failed --
                echo 'PAYMENT DECLINED! Please try again with another card. Bank response: ' . $_REQUEST["RESPONSETEXT"];
    }
} else {
    echo 'PAYMENT FAILED: INVALID RESPONSE HASH. Please contact <a href="mailto:' . $adminEmail . '">' . $adminEmail . '</a> or call ' . $adminPhone . ' to clarify if you will get charged for this order.';
    if(isset($_REQUEST["ORDERID"])) echo 'Please quote Nuvei Terminal ID: ' . $terminalId . ', and Order ID: ' . $_REQUEST["ORDERID"] . ' when mailling or calling.';
}
 
    }

}
