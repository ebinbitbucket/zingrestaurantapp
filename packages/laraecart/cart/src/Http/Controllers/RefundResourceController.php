<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Laraecart\Cart\Http\Requests\RefundRequest;
use Laraecart\Cart\Interfaces\RefundRepositoryInterface;
use Laraecart\Cart\Models\Refund;
use Mail;
use Laraecart\Cart\Interfaces\OrderRepositoryInterface;



/**
 * Resource controller class for refund.
 */
class RefundResourceController extends BaseController
{

    /**
     * Initialize refund resource controller.
     *
     * @param type RefundRepositoryInterface $refund
     *
     * @return null
     */
    public function __construct(OrderRepositoryInterface $order,RefundRepositoryInterface $refund)
    {
        parent::__construct();
        $this->repository = $refund;
        $this->order = $order;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Laraecart\Cart\Repositories\Criteria\RefundResourceCriteria::class);
    }

    /**
     * Display a list of refund.
     *
     * @return Response
     */
    public function index(RefundRequest $request)
    {
        //dd('ddd');
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Laraecart\Cart\Repositories\Presenter\RefundPresenter::class)
                ->$function();
        }

        $refunds = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('refund::refund.names'))
        ->view('cart::refund.index', true)
        ->data(compact('refunds', 'view'))
            ->output();
    }

    /**
     * Display refund.
     *
     * @param Request $request
     * @param Model   $refund
     *
     * @return Response
     */
    public function show(RefundRequest $request, Refund $refund)
    {

        if ($refund->exists) {
            $view = 'refund::refund.show';
        } else {
            $view = 'refund::refund.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('refund::refund.name'))
            ->data(compact('refund'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new refund.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(RefundRequest $request)
    {

        $refund = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('refund::refund.name')) 
            ->view('refund::refund.create', true) 
            ->data(compact('refund'))
            ->output();
    }

    /**
     * Create new refund.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(RefundRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $refund                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('refund::refund.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('refund/refund/' . $refund->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/refund/refund'))
                ->redirect();
        }

    }

    /**
     * Show refund for editing.
     *
     * @param Request $request
     * @param Model   $refund
     *
     * @return Response
     */
    public function edit(RefundRequest $request, Refund $refund)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('refund::refund.name'))
            ->view('refund::refund.edit', true)
            ->data(compact('refund'))
            ->output();
    }

    /**
     * Update the refund.
     *
     * @param Request $request
     * @param Model   $refund
     *
     * @return Response
     */
    public function update(RefundRequest $request, Refund $refund)
    {
        try {
            $attributes = $request->all();

            $refund->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('refund::refund.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('refund/refund/' . $refund->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('refund/refund/' . $refund->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the refund.
     *
     * @param Model   $refund
     *
     * @return Response
     */
    public function destroy(RefundRequest $request, Refund $refund)
    {
        try {

            $refund->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('refund::refund.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('refund/refund/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('refund/refund/' . $refund->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple refund.
     *
     * @param Model   $refund
     *
     * @return Response
     */
    public function delete(RefundRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('refund::refund.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('refund/refund'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/refund/refund'))
                ->redirect();
        }

    }

    /**
     * Restore deleted refunds.
     *
     * @param Model   $refund
     *
     * @return Response
     */
    public function restore(RefundRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('refund::refund.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/refund/refund'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/refund/refund/'))
                ->redirect();
        }

    }
    public function refundEmails(RefundRequest $request,$status,$id){
        $refund = $this->repository->findByField('id',$id)->first();

        $order = $this->order->findByField('id',$refund->order_number)->first();
        //dd($order->restaurant->email);
        
        if($status=='inform'){
            Mail::send('cart::admin.refund.mail_refund_rest', ['order' => $order,'order_detail' => $order->detail], function ($message) use ($order) {
                $message->from('support@zingmyorder.com', 'ZingMyOrder');
                $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
                $message->to($order->restaurant->email)->subject('Zing Order Refund Request');
            });
        }else if($status=='issue'){
            Mail::send('cart::admin.refund.mail_refund_cust', ['order' => $order,'order_detail' => $order->detail], function ($message) use ($order) {
                $message->from('support@zingmyorder.com', 'ZingMyOrder');
                $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
                $message->to($order->restaurant->email)->subject('Zing Order Refund Request');
            });
        }else if($status=='complete'){
            if($refund->update(['status'=>1])){
               // return true;
            }
        }
        
    }
    
    public function refundEmailsSave(RefundRequest $request){
        //dd($request->all());
        $id=$request->id;
        $reason=$request->reason;
        $refund = $this->repository->findByField('id',$id)->first();
        $order = $this->order->findByField('id',$refund->order_number)->first();
        //dd($order->restaurant->email);
        if($request->type=='issue_refund'){
            Mail::send('cart::admin.refund.mail_refund_cust', ['order' => $order,'order_detail' => $order->detail,'refund' => $refund,'reason'=>$reason], function ($message) use ($order,$refund) {
                $message->from('support@zingmyorder.com', 'ZingMyOrder');
                $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
                $message->to($refund->email)->subject('Zing Order Refund Request');
            });
        }else{
            Mail::send('cart::admin.refund.mail_no_refund', ['order' => $order,'order_detail' => $order->detail,'reason'=>$reason,'refund' => $refund], function ($message) use ($order,$reason,$refund) {
                $message->from('support@zingmyorder.com', 'ZingMyOrder');
                $message->bcc('support@zingmyorder.com','ZingMyOrder Support');
                $message->to($refund->email)->subject('Zing Order Refund Request');
            });
        }
     
      
        
    }
    

}
