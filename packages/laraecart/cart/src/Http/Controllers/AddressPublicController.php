<?php

namespace Laraecart\Cart\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Laraecart\Cart\Interfaces\AddressRepositoryInterface;
use Restaurant\Restaurant\Models\Restaurant;
use DB;
use Illuminate\Http\Request;
use Laraecart\Cart\Facades\Cart;
class AddressPublicController extends BaseController
{
    // use AddressWorkflow;

    /**
     * Constructor.
     *
     * @param type \Laraecart\Address\Interfaces\AddressRepositoryInterface $address
     *
     * @return type
     */
    public function __construct(AddressRepositoryInterface $address)
    {
        $this->repository = $address;
        parent::__construct();
    }

    /**
     * Show address's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $addresses = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$cart::address.names'))
            ->view('cart::address.index')
            ->data(compact('addresses'))
            ->output();
    }


    /**
     * Show address.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $address = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$address->name . trans('cart::address.name'))
            ->view('cart::address.show')
            ->data(compact('address'))
            ->output();
    }

    protected function cartAddressList(Request $request){
        if (empty($request->get('status'))) {
            $address = $this->repository->findByField(['client_id' => user_id(), 'type' => 'delivery']);
            $view    = view('cart::address.show', compact('address'))->render();
        } else {
            $check_address = $this->repository->orderBy('created_at', 'DESC')->findByField(['client_id' => user_id(), 'type' => 'delivery', 'default_address' => 1])->first();
            if (!empty($check_address)) {
                if ($check_address->longitude == null || $check_address->latitude == null) {
                    $msg = "Please change your address";
                    return response()->json(['status' => 'Error', 'section' => 'section_address', 'msg' => $msg]);
                }

                $carts = Cart::content();
                foreach ($carts as $item) {

                    foreach ($item->options as $key => $option) {

                        if ($key == 'restaurant') {
                            $restaurant_id = $option;
                        }

                    }

                }

                $restaurant = Restaurant::find($restaurant_id);
                $distance   = DB::select("SELECT (3959  * acos(cos( radians(" . $restaurant->latitude . ") ) * cos( radians( " . $check_address->latitude . " ) ) * cos( radians( " . $check_address->longitude . " ) - radians(" . $restaurant->longitude . ") ) + sin( radians(" . $restaurant->latitude . ") ) * sin( radians( " . $check_address->latitude . " ) )) ) as distance");
                if (!empty($restaurant->delivery_limit) && $distance[0]->distance > $restaurant->delivery_limit) {
                    // $msg = 'Your address is outside the delivery limit of '.$restaurant->delivery_limit.' miles for the Eatery';
                    $msg     = 'Outside Delivery Area';
                    $address = $this->repository->findByField(['client_id' => user_id(), 'type' => 'delivery']);
                    $view    = view('cart::address.show', compact('address'))->render();
                    return response()->json(['status' => 'Error', 'section' => 'section_address', 'view' => $view, 'msg' => $msg]);
                }

                $view = view('cart::address.show_saved_address', compact('check_address'))->render();
            }
            else{
                $address = $this->repository->findByField(['client_id' => user_id(), 'type' => 'delivery']);
                $view    = view('cart::address.show', compact('address'))->render();
            }

        }

        return response()->json(['status' => 'Success', 'section' => 'address', 'view' => $view]);
           
    }
    protected function savedAddressPage($id,$rest){
        $restaurant = Restaurant::find(hashids_decode($rest));
        $check_address = $this->repository->findByField('id',$id)->first();
        if($check_address->longitude == null || $check_address->latitude ==  null){
            $msg = "Please change your address";
                return response()->json(['status' => 'Error','section' => 'section_address', 'msg' => $msg]);
           }
        $distance = DB::select("SELECT (3959  * acos(cos( radians(".$restaurant->latitude.") ) * cos( radians( ".$check_address->latitude." ) ) * cos( radians( ".$check_address->longitude." ) - radians(".$restaurant->longitude.") ) + sin( radians(".$restaurant->latitude.") ) * sin( radians( ".$check_address->latitude." ) )) ) as distance");  
          if(!empty($restaurant->delivery_limit) && $distance[0]->distance > $restaurant->delivery_limit){
                // $msg = 'Your address is outside the delivery limit of '.$restaurant->delivery_limit.' miles for the Eatery';
                $msg = 'Outside Delivery Area';
                $address = $this->repository->findByField(['client_id' => user_id(),'type' => 'delivery']);
                $view =  view('cart::address.show',compact('address'))->render();
                return response()->json(['status' => 'Error','section' => 'section_address', 'view' => $view, 'msg' => $msg]);
            }

        $view =  view('cart::address.show_saved_address',compact('check_address'))->render();
        return response()->json(['status' => 'Success','section' => 'section_address', 'view' => $view]);
           
    }


}
