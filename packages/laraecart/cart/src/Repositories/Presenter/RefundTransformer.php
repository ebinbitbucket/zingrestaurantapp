<?php

namespace  Laraecart\Cart\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class RefundTransformer extends TransformerAbstract
{
    public function transform(\Laraecart\Cart\Models\Refund $refund)
    {
        if($refund->restaurant_id){
           $act ='';
            } else {
                $act =$refund->status ? "Completed" : '<button class="btn btn-xs btn-success" onclick="issue_refund('.$refund->id.')">Issue Refund
                </button><a class="btn btn-xs btn-success" onclick="no_refund('.$refund->id.')">No Refund</a>'; 
            }
            //dd($act);
        return [
            'id'                => $refund->getRouteKey(),
            'key'               => [
                'public'    => $refund->getPublicKey(),
                'route'     => $refund->getRouteKey(),
            ], 
            'order_number'      => $refund->order_number,
            'order_amount'      => $refund->order_amount,
            'last_digits'       => $refund->last_digits,
            'reason'            => $refund->reason,
            'resturant'            => $refund->resturant,
            'email'             => $refund->email,
            'deleted_at'        => $refund->deleted_at,
            'user_id'           => $refund->user_id,
            'user_type'         => $refund->user_type,
            'action'           =>$act,
            'url'               => [
                'public'    => trans_url('refund/'.$refund->getPublicKey()),
                'user'      => guard_url('refund/refund/'.$refund->getRouteKey()),
            ], 
            'status'            =>  $refund->status ? "Completed" : '<button class="btn btn-xs btn-success" onclick="complete('.$refund->id.')">Complete
            </button>', 
            
            'created_at'        => format_date($refund->created_at),
            'updated_at'        => format_date($refund->updated_at),
        ];
    }
}