<?php

namespace Laraecart\Cart\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class CallsTransformer extends TransformerAbstract
{
    public function transform(\Laraecart\Cart\Models\Calls $calls)
    {
        return [
            'id'                => $calls->getRouteKey(),
            'key'               => [
                'public'    => $calls->getPublicKey(),
                'route'     => $calls->getRouteKey(),
            ], 
            'order_id'          => $calls->order_id,
            'restaurant_id'     => $calls->restaurant_id,
            'restaurant_name'     => $calls->restaurant->name,
            'call_count'        => $calls->call_count,
            'datetime'          => $calls->datetime,
            'deleted_at'        => $calls->deleted_at,
            'url'               => [
                'public'    => trans_url('calls/'.$calls->getPublicKey()),
                'user'      => guard_url('calls/calls/'.$calls->getRouteKey()),
            ], 
            'status'            => trans('app.'.$calls->status),
            'created_at'        => format_date($calls->created_at),
            'updated_at'        => format_date($calls->updated_at),
        ];
    }
}