<?php

namespace Laraecart\Cart\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class AddressTransformer extends TransformerAbstract
{
    public function transform(\Laraecart\Cart\Models\Address $address)
    {
        return [
            'id'                => $address->getRouteKey(),
            'key'               => [
                'public'    => $address->getPublicKey(),
                'route'     => $address->getRouteKey(),
            ], 
            'address'           => $address->address,
            'title'             => $address->title,
            'client_id'         => @$address->client->name,
            'zipcode'           => $address->zipcode,
            'address'           => $address->address,
            'street'            => $address->street,
            'state_id'          => $address->state_id,
            'country_id'        => $address->country_id,
            'user_id'           => $address->user_id,
            'user_type'         => $address->user_type,
            'created_at'        => $address->created_at,
            'updated_at'        => $address->updated_at,
            'deleted_at'        => $address->deleted_at,
            'url'               => [
                'public'    => trans_url('cart/'.$address->getPublicKey()),
                'user'      => guard_url('cart/address/'.$address->getRouteKey()),
            ], 
            'status'            => trans('app.'.$address->status),
            'created_at'        => format_date($address->created_at),
            'updated_at'        => format_date($address->updated_at),
        ];
    }
}