<?php

namespace Laraecart\Cart\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class OrderTransformer extends TransformerAbstract
{
    public function transform(\Laraecart\Cart\Models\Order $order)
    {
        return [
            'id'                => $order->getRouteKey(),
            'order_id'          => $order->id,
            'key'               => [
                'public'    => $order->getPublicKey(),
                'route'     => $order->getRouteKey(),
            ], 
            'name'              => @$order->user->name,
            'email'             => @$order->user->email,
            'restaurant_id'     => @$order->restaurant->name,
            'address'           => $order->address,
            'phone'             => $order->phone,
            'street'            => $order->street,
            'zipcode'           => $order->zipcode,
            'state_id'          => $order->state_id,
            'country_id'        => $order->country_id,
            'shipping_name'     => $order->shipping_name,
            'shipping_city'     => $order->shipping_city,
            'shipping_zipcode'  => $order->shipping_zipcode,
            'shipping_email'    => $order->shipping_email,
            'shipping_address'  => $order->shipping_address,
            'shipping_phone'    => $order->shipping_phone,
            'order_status'      => $order->order_status,
            'shipping'          => $order->shipping,
            'delivery_id'       => $order->delivery_id,
            'subtotal'          => $order->subtotal,
            'tax'               => $order->tax,
            'total'             => $order->total,
            'payment_status'    => $order->payment_status,
            'payment_methods'   => $order->payment_methods,
            'payment_details'   => $order->payment_details,
            'delivery_time'     => $order->delivery_time,
            'created_at'        => $order->created_at,
            'updated_at'        => $order->updated_at,
            'deleted_at'        => $order->deleted_at,
            'url'               => [
                'public'    => trans_url('cart/'.$order->getPublicKey()),
                'user'      => guard_url('cart/order/'.$order->getRouteKey()),
            ], 
            'status'            => trans('app.'.$order->status),
            'created_at'        => format_date($order->created_at),
            'updated_at'        => format_date($order->updated_at),
        ];
    }
}