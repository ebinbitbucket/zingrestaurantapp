<?php

namespace Laraecart\Cart\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class HistoryTransformer extends TransformerAbstract
{
    public function transform(\Laraecart\Cart\Models\History $history)
    {
        return [
            'id'                => $history->getRouteKey(),
            'key'               => [
                'public'    => $history->getPublicKey(),
                'route'     => $history->getRouteKey(),
            ], 
            'id'                => $history->id,
            'order_id'          => $history->order_id,
            'courier_name'      => $history->courier_name,
            'order_status'      => $history->order_status,
            'created_at'        => $history->created_at,
            'updated_at'        => $history->updated_at,
            'deleted_at'        => $history->deleted_at,
            'url'               => [
                'public'    => trans_url('cart/'.$history->getPublicKey()),
                'user'      => guard_url('cart/history/'.$history->getRouteKey()),
            ], 
            'status'            => trans('app.'.$history->status),
            'created_at'        => format_date($history->created_at),
            'updated_at'        => format_date($history->updated_at),
        ];
    }
}