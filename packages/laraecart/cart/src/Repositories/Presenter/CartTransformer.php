<?php

namespace Laraecart\Cart\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class CartTransformer extends TransformerAbstract
{
    public function transform(\Laraecart\Cart\Models\Cart $cart)
    {
        return [
            'id'                => $cart->getRouteKey(),
            'key'               => [
                'public'    => $cart->getPublicKey(),
                'route'     => $cart->getRouteKey(),
            ], 
            'id'                => $cart->id,
            'instance'          => $cart->instance,
            'content'           => $cart->content,
            'created_at'        => $cart->created_at,
            'updated_at'        => $cart->updated_at,
            'deleted_at'        => $cart->deleted_at,
            'url'               => [
                'public'    => trans_url('cart/'.$cart->getPublicKey()),
                'user'      => guard_url('cart/cart/'.$cart->getRouteKey()),
            ], 
            'status'            => trans('app.'.$cart->status),
            'created_at'        => format_date($cart->created_at),
            'updated_at'        => format_date($cart->updated_at),
        ];
    }
}