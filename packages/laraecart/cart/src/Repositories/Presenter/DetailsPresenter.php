<?php

namespace Laraecart\Cart\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class DetailsPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DetailsTransformer();
    }
}