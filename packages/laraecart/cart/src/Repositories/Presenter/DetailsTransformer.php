<?php

namespace Laraecart\Cart\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class DetailsTransformer extends TransformerAbstract
{
    public function transform(\Laraecart\Cart\Models\Details $details)
    {
        return [
            'id'                => $details->getRouteKey(),
            'key'               => [
                'public'    => $details->getPublicKey(),
                'route'     => $details->getRouteKey(),
            ], 
            'id'                => $details->id,
            'order_id'          => $details->order_id,
            'product_id'        => $details->product_id,
            'size'              => $details->size,
            'quantity'          => $details->quantity,
            'unit_price'        => $details->unit_price,
            'price'             => $details->price,
            'user_id'           => $details->user_id,
            'user_type'         => $details->user_type,
            'parameters'        => $details->parameters,
            'created_at'        => $details->created_at,
            'updated_at'        => $details->updated_at,
            'deleted_at'        => $details->deleted_at,
            'url'               => [
                'public'    => trans_url('cart/'.$details->getPublicKey()),
                'user'      => guard_url('cart/details/'.$details->getRouteKey()),
            ], 
            'status'            => trans('app.'.$details->status),
            'created_at'        => format_date($details->created_at),
            'updated_at'        => format_date($details->updated_at),
        ];
    }
}