<?php

namespace Laraecart\Cart\Repositories\Eloquent;

use Laraecart\Cart\Interfaces\CallsRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class CallsRepository extends BaseRepository implements CallsRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('laraecart.cart.calls.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('laraecart.cart.calls.model.model');

    }
}
