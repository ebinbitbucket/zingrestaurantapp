<?php

namespace Laraecart\Cart\Repositories\Eloquent;

use Laraecart\Cart\Interfaces\OrderRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;
use Restaurant\Restaurant\Models\Favourite;
use Laraecart\Cart\Models\Order;
use DB;
use URL;
use Laraecart\Cart\Models\TransactionLogs;
class OrderRepository extends BaseRepository implements OrderRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('laraecart.cart.order.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('laraecart.cart.order.model.model');
    }

    public function findOrder($id)
    {
        return $this->model
                           ->where('id', '=', $id)
                           ->first();
    }

    public function updateOrder($request, $id)
    {
        return $this->model
                           ->where('id', '=', $id)
                           ->update($request);
    }

    public function filterOrders($status)
    {

        if($status == 'all')
        {
            if (user()->hasRole('client')) {
            $this->model = $this->model
                        ->where('guest','=', 'false')
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type())
                        ->orderBy('id','DESC');
            }
            if (user()->hasRole('restaurant')) {
             $this->model = $this->model
                        ->where('restaurant_id','=', user_id())
                        ->orderBy('id','DESC');
            }
            return $this->model->get();
        }
        else if($status == 'overdue'){
            if (user()->hasRole('client')) {
                $this->model = $this->model
                        ->where('order_status','=', 'New orders')
                        ->whereDate('delivery_time','<', date('Y-m-d'))
                        ->where('guest','=', 'false')
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type())
                        ->orderBy('id','DESC');
              return $this->model->get();
            }
        }
       else if($status == 'pastorders'){                       

            if (user()->hasRole('client')) {
                $this->model = $this->model
                        ->whereIn('order_status',['Completed','Cancelled'])
                        ->whereDate('delivery_time','<=', date('Y-m-d'))
                        ->where('guest','=', 'false')
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type())
                        ->orderBy('id','DESC'); 
              return $this->model->get();
            }
        }
        else if($status == 'currorders'){
            if (user()->hasRole('client')) {
                $this->model = $this->model
                        ->whereIn('order_status',['New Orders','Completed','Scheduled','Preparing','Ready for Pickup','Out for Delivery'])
                        ->whereDate('delivery_time','>=', date('Y-m-d'))
                        ->where('payment_status','Paid')
                        ->where('guest','=', 'false')
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type())
                        ->orderBy('id','DESC');
              return $this->model->get();
            }
        }
         else if($status == 'neworders'){
            if (user()->hasRole('client')) {
                $this->model = $this->model
                        ->where('order_status','=', 'New orders')
                        ->whereDate('delivery_time','>=', date('Y-m-d'))
                        ->where('payment_status','Paid')
                        ->where('user_id','=', user_id())
                        ->where('guest','=', 'false')
                        ->where('user_type','=', user_type())
                        ->orderBy('id','DESC');
              return $this->model->get();
            }
        }
         else if($status == 'favourites'){
            if (user()->hasRole('client')) {
                $date = date('Y-m-d',strtotime("-120 days"));
                $favourites = Favourite::where('user_id',user_id())->where('type','order')->whereDate('created_at','>',$date)->pluck('favourite_id')->toArray();
            return $this->model->whereIn('id',$favourites)->get();
           
            }
        }
        else{
            if($status == 'scheduled'){
                $status = "Scheduled";
            }
            else if($status == 'preparing'){
                $status = "Preparing";
            }
            else if($status == 'pickedup'){
                $status = "Picked up";
            }
            else if($status == 'delivered'){
                $status = "Delivered";
            }
            else if($status == 'cancelled'){
                $status = "Cancelled";
            }
            if (user()->hasRole('client')) {
             $this->model = $this->model
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type())
                        ->orderBy('id','DESC');
        }
        if (user()->hasRole('restaurant')) {
             $this->model = $this->model
                        ->where('restaurant_id','=', user_id())
                        ->orderBy('id','DESC');
        }
            return $this->model
                           ->where('order_status', $status)->get();
        }
    
    }
    public function sumtotal($status) {
        if ($status == 'total') {
            return $this->model->where('restaurant_id',user()->id)->where('payment_status','Paid')->where('order_status','<>','Cancelled')->sum('total');
        } elseif ($status == 'totalpaid') { 
            return $this->model->where('payment_status', 'Paid')->sum('total');
        } else {
            return $this->model->where('payment_status', 'Unpaid')->sum('total');
        }

    }

      public function getKitchenRestaurants($restaurant_id) {
        return $this->model->where('restaurant_id',$restaurant_id)->whereIn('order_status',['New orders','Preparing'])->whereDate('delivery_time','=',date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->take(9)->get();
        //->where('delivery_time','>',date('Y-m-d H:i:s'))

    }
     public function getRestaurantTodayOrders() {
        return $this->model->where('restaurant_id',user_id())->whereDate('delivery_time','=', date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->get();
     }

     public function getRestaurantAllOrders() {
        return $this->model->where('restaurant_id',user_id())->whereDate('delivery_time','<', date('Y-m-d'))->where('payment_status','Paid')->orderBy('id','DESC')->paginate(5);
     }
     public function getAjax($filter)
    {

      if(!empty($filter)){
        $assets =  Order::select('restaurants'.'.name','restaurants'.'.acc_date','restaurants'.'.tax_rate','restaurants'.'.CCR','restaurants'.'.CCF','restaurants'.'.ZR','restaurants'.'.ZF','restaurants'.'.ZC',DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),'restaurants'.'.ac_no','restaurants'.'.IFSC','restaurants'.'.bank',
                            DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                        ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
                        ->whereBetween(DB::raw('DATE(orders.delivery_time)'), array(date('Y-m-d',strtotime($filter['filter_date1'])), date('Y-m-d',strtotime($filter['filter_date2']))))
                        // ->whereDate('orders'.'.delivery_time','<=',date('Y-m-d'))
                        ->where('payment_status','Paid')
                        ->where('orders'.'.pay_to','Pending')
                        ->where('payment_status','Paid')
                        ->where('order_status', 'Completed')
                        ->groupBy('restaurants'.'.name','restaurants'.'.acc_date','restaurants'.'.ac_no','restaurants'.'.IFSC','restaurants'.'.bank','restaurants'.'.tax_rate','restaurants'.'.CCR','restaurants'.'.CCF','restaurants'.'.ZR','restaurants'.'.ZF','restaurants'.'.ZC');
      }
      else{
        $assets =  Order::select('restaurants'.'.name','restaurants'.'.acc_date','restaurants'.'.tax_rate','restaurants'.'.CCR','restaurants'.'.CCF','restaurants'.'.ZR','restaurants'.'.ZF','restaurants'.'.ZC',DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),'restaurants'.'.ac_no','restaurants'.'.IFSC','restaurants'.'.bank',
                            DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                        ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
                        ->whereDate('orders'.'.delivery_time','<=',date('Y-m-d'))
                        ->where('payment_status','Paid')
                        ->where('orders'.'.pay_to','Pending')
                        ->where('payment_status','Paid')
                        ->where('order_status', 'Completed')
                        ->groupBy('restaurants'.'.name','restaurants'.'.acc_date','restaurants'.'.ac_no','restaurants'.'.IFSC','restaurants'.'.bank','restaurants'.'.tax_rate','restaurants'.'.CCR','restaurants'.'.CCF','restaurants'.'.ZR','restaurants'.'.ZF','restaurants'.'.ZC');
      }
        
        return $this -> filter($assets, $filter);
    }

    public function filter($assets, $filter)
    {


    if(empty($filter))
           return $assets -> get() -> toArray();

     return $assets-> where(function ($query) use ($filter)
                               { 

                                  foreach($filter as $key=>$value)
                                   {
                                       if ($value == '') {
                                           continue;
                                       }
                                       // $values = explode(' - ', $value);
                                       // $query->whereBetween($key,[date('Y-m-d 11:59:59',strtotime($values[0])),date('Y-m-d 11:59:59',strtotime($values[1]))]);
                                   }

                               })
                   -> take(1000)
                   -> get()
                   -> toArray();

    }
    
     public function uniqueVisits($restaurant_id, $filter_date1 = '', $filter_date2 = '') {
        $logs = TransactionLogs::select('ip_address',  DB::raw('count(ip_address) as count'))
                          ->where('restaurant_id', $restaurant_id)
                          ->whereDate('created_at', '<=', date('Y-m-d 23:59:59', strtotime($filter_date2)))
                          ->whereDate('created_at', '>=', date('Y-m-d ', strtotime('-1 day', strtotime($filter_date1))))
                          ->groupBy('ip_address')
                          ->get()->toArray();
        return count($logs);
                         
     }

     public function repeatVisits($restaurant_id, $filter_date1 = '', $filter_date2 = '') {
        // $logs = DB::table('transaction_logs')->select('ip_address',  DB::raw('count(ip_address) as count'))
        //                   ->where('restaurant_id', $restaurant_id)
        //                   ->whereDate('created_at', '<=', date('Y-m-d 23:59:59', strtotime($filter_date2)))
        //                   ->whereDate('created_at', '>=', date('Y-m-d 00:00:00', strtotime($filter_date1)))
        //                    ->whereRaw(DB::raw('date(created_at) IN (SELECT DISTINCT(date(created_at)) From transaction_logs GROUP BY ip_address)'))
        //                   ->groupBy('ip_address')
        //                   ->get()->toArray();

        $logs1 = TransactionLogs::
                          select('ip_address',  DB::raw('count(ip_address) as count'))
                          ->where('restaurant_id', $restaurant_id)
                          ->whereDate('created_at', '<=', date('Y-m-d 23:59:59', strtotime($filter_date2)))
                          ->whereDate('created_at', '>=', date('Y-m-d ', strtotime('-1 day', strtotime($filter_date1))))
                          ->groupBy('ip_address')
                       
                          ->get()->toArray();
                      
         $repeat_visits = array_filter(
                            $logs1,
                            function ($value) {  
                                return ($value['count'] > 1);
                            }
                        ); 
        return count(@$repeat_visits);
                         
     }

      public function directLinkVisits($restaurant_id, $filter_date1 = '', $filter_date2 = '') {
        $logs = DB::table('transaction_logs')->where('restaurant_id', $restaurant_id)
                        ->whereDate('created_at', '<=', date('Y-m-d 23:59:59', strtotime($filter_date2)))
                        ->whereDate('created_at', '>=', date('Y-m-d ', strtotime('-1 day', strtotime($filter_date1))))
                        ->where('HTTP_REFERER', 'like', '%'.URL::to('/').'%')->get();
        return $logs->count();
                         
     }

     public function directLinkOrders($restaurant_id, $filter_date1 = '', $filter_date2 = '') {
        $logs = DB::table('transaction_logs')->where('restaurant_id', $restaurant_id)
                        ->where('type','like','payment')
                        ->whereDate('created_at', '<=', date('Y-m-d 23:59:59', strtotime($filter_date2)))
                        ->whereDate('created_at', '>=', date('Y-m-d ', strtotime('-1 day', strtotime($filter_date1))))
                        ->where('HTTP_REFERER', 'like', '%'.URL::to('/').'%')->get();
        return $logs->count();
     }

     public function referralVisits($restaurant_id, $filter_date1 = '', $filter_date2 = '') {
        $logs = DB::table('transaction_logs')->where('restaurant_id', $restaurant_id)
                        ->whereDate('created_at', '<=', date('Y-m-d 23:59:59', strtotime($filter_date2)))
                        ->whereDate('created_at', '>=', date('Y-m-d ', strtotime('-1 day', strtotime($filter_date1))))
                       ->where('HTTP_REFERER', 'NOT LIKE', '%'.URL::to('/').'%')->get();
        return $logs->count();
                         
     }
     public function referralOrders($restaurant_id, $filter_date1 = '', $filter_date2 = '') {
        $logs = DB::table('transaction_logs')->where('restaurant_id', $restaurant_id)
                        ->whereDate('created_at', '<=', date('Y-m-d 23:59:59', strtotime($filter_date2)))
                        ->whereDate('created_at', '>=', date('Y-m-d ', strtotime('-1 day', strtotime($filter_date1))))
                       ->where('HTTP_REFERER', 'NOT LIKE', '%'.URL::to('/').'%')->get();
        return $logs->count();
                         
     }
     public function getOrderItems($id)
     {
         return  $this->model->with('detail', 'detail.menu','restaurant','delivery_address')
                            ->where('id', '=', $id)
                            ->first();
                            //dd($order);
     }  
     public function getNewOrdersByRestaurant($restaurant_id)
     {
         return  $this->model
         ->where('restaurant_id',$restaurant_id) ->where('order_status', '=', 'New Orders')
                            ->whereDate('delivery_time', '=', date('Y-m-d'))
                            ->get();
                            //dd($order);

     } 
    
     public function getOrderReports($filter=0)
     {
         if(isset($filter['frm_date'])&&isset($filter['to_date'])){
            $orders = $this->model->with('restaurant')->whereBetween('delivery_time', array($filter['frm_date'], $filter['to_date']))->get()->toArray();
         }else{
            $orders = $this->model->with('restaurant')->whereBetween('delivery_time', array(date('yy-m-d', strtotime('-2 month')), date('yy-m-d')))->get()->toArray();
         }
        $result = [];
        if ($orders) {
            foreach ($orders as $key => $value) {
                $result[$key]['Name'] =  @$value['name'];
                $result[$key]['Phone'] = @$value['phone'];
                $result[$key]['Restaurant'] = @$value['restaurant']['name'];
                $result[$key]['Status'] = @$value['order_status '];
                $result[$key]['Date'] = date('m M d',strtotime(@$value['delivery_time']));
                $result[$key]['Time'] = date('H:i A',strtotime(@$value['delivery_time']));
                $result[$key]['Subtotal'] = @$value['subtotal'];
                $result[$key]['Tax'] = @$value['tax'];
                $result[$key]['Price']= @$value['total'];
                $result[$key]['Tip'] = @$value['tip'];
                $result[$key]['Delivery Charge'] = @$value['delivery_charge'];
                $result[$key]['Guest'] = @$value['guest'];
                $result[$key]['Day'] = date('D',strtotime(@$value['delivery_time']));


            }
        }
   //dd($result);
        return $result;
                            
     } 
     
}
