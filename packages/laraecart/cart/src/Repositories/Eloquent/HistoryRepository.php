<?php

namespace Laraecart\Cart\Repositories\Eloquent;

use Laraecart\Cart\Interfaces\HistoryRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class HistoryRepository extends BaseRepository implements HistoryRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('laraecart.cart.history.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('laraecart.cart.history.model.model');
    }
}
