<?php

namespace Laraecart\Cart\Repositories\Eloquent;

use Laraecart\Cart\Interfaces\AddressRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class AddressRepository extends BaseRepository implements AddressRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('laraecart.cart.address.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('laraecart.cart.address.model.model');
    }

    public function address($user_id) {
        return $this->model->where('user_id', $user_id)->first();
    }
}
