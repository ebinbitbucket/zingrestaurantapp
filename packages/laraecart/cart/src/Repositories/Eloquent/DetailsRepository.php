<?php

namespace Laraecart\Cart\Repositories\Eloquent;

use Laraecart\Cart\Interfaces\DetailsRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class DetailsRepository extends BaseRepository implements DetailsRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('laraecart.cart.details.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('laraecart.cart.details.model.model');
    }

    public function sumproducts($orders) {
            return $this->model->whereIn('order_id',$orders)->sum('quantity');
       

    }

    public function MaxOrderCount($menu_id,$order_ids) {
            return $this->model->where('menu_id',$menu_id)->where('menu_addon','menu')->whereIn('order_id',$order_ids)->sum('quantity');
       

    }
}
