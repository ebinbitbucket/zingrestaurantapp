<?php

namespace Laraecart\Cart\Repositories\Eloquent;

use Auth;
use DateTime;
use Laraecart\Cart\Facades\Cart;
use Laraecart\Cart\Interfaces\CartRepositoryInterface;
use Laraecart\Cart\Models\Address;
use Laraecart\Cart\Models\Details;
use Laraecart\Cart\Models\Order;
use Litepie\Repository\Eloquent\BaseRepository;
use Mail;
use Restaurant\Restaurant\Models\Restaurant;

class CartRepository extends BaseRepository implements CartRepositoryInterface
{

    public function boot()
    {
        $this->fieldSearchable = config('laraecart.cart.cart.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('laraecart.cart.cart.model.model');
    }

    public function orderSave($request)
    {
        $carts   = Cart::content();
        
        foreach ($carts as $item) {

            foreach ($item->options as $key => $option) {

                if ($key == 'restaurant') {
                    $restaurant = $option;
                }

            }

        }

        $restaurant_data = Restaurant::where('id', $restaurant)->first();
        $order['name']   = user()->name;
        $order['guest']  = user()->guest;
        if(isset($request['mobile']) && !empty($request['mobile'])) {
            $order['phone']  = $request['mobile'];
        } else {
            $order['phone'] = user()->mobile;
        }
        
        if ($request['order_type'] == 'Pickup') {
            $order['address_id'] = null;
        } else {
            $order['address_id'] = $request['address_id'];
            $address_default     = Address::where('client_id', user_id())->where('default_address', 1)->first();

            if (empty($address_default)) {

                if (!empty($request['address_id'])) {
                    Address::where('id', $request['address_id'])->update(['default_address' => 1]);
                }

            } elseif ($address_default->id != $request['address_id']) {
                Address::where('id', $address_default->id)->update(['default_address' => 0]);
                Address::where('id', $request['address_id'])->update(['default_address' => 1]);
            }

        }

        $preparation_time = 10;

        foreach ($carts as $item) {
            $order['subtotal'] = $item->price;

            foreach ($item->options as $keys => $optiond) {

                if ($keys == 'delivery_charge' && $request['order_type'] != 'Pickup') {
                    $delivery_charge = $optiond;
                } else

                if ($keys == 'preparation_time') {
                    if (is_numeric($optiond) && $optiond > $preparation_time) {
                        $preparation_time = $optiond;
                    } 

                }

            }


        }

        if ($request['time_type'] == 'asap') {
            $date                   = new DateTime();
            $time                   = new DateTime();
            $combined               = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));
            $order['delivery_time'] = $combined->format('y-m-d H:i:s');
            $order['preparation_time'] = $preparation_time;
            $combined->modify('+'.$preparation_time.' minutes');
            $order['ready_time'] = $combined->format('y-m-d H:i:s');
            $order['order_status']  = 'New Orders';

        } else {
            $date                   = new DateTime($request['scheduled_date']);
            $time                   = new DateTime($request['scheduled_time']);
            $combined               = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));
            $order['delivery_time'] = $combined->format('y-m-d H:i:s');
            $order['preparation_time'] = 0;
            $order['ready_time'] = $combined->format('y-m-d H:i:s');
            $order['order_status']  = 'Scheduled';
        }
        if (empty($delivery_charge) || $delivery_charge == '') {
            $delivery_charge = 0;
        }

        if (empty($request['tip']) || $request['tip'] == '') {
            if(!empty($request['tip_value_cus'])){
                $request['tip'] = $request['ref_subtotal'] * $request['tip_value_cus'] / 100;
            }else{
                 $request['tip'] = 0;
            }
        }

        $amount                   = Cart::total();
        $order['order_type']      = $request['order_type'];
        $order['tax']             = !empty($request['tax']) ? $request['tax'] : 0;
        $order['discount_points'] = (!empty($request['discount']) ? $request['discount'] : 0) * 2000;
        $order['loyalty_points']  = 0;
        $order['tip']             = $request['tip'];
        $order['tip_value_cus']   = $request['tip_value_cus'];

        $order['non_contact']   = !empty($request['non_contact']) ? $request['non_contact'] : 0;
        $order['car_pickup']   = !empty($request['car_pickup']) ? $request['car_pickup'] : 0;
        $order['text_notification']   = !empty($request['text_notification']) ? $request['text_notification'] : 0;

        if ($order['tip_value_cus'] != null) {
            $order['tip_value'] = null;
        } else {
            $order['tip_value'] = $request['tip_value'];
        }

        if (empty($request['discount_amount'])) {
            $request['discount_amount'] = 0;
        }

        if (empty($request['min_order_difference'])) {
            $request['min_order_difference'] = 0;
        }

        $order['delivery_charge'] = $delivery_charge;
        $order['total']           = $amount + $delivery_charge + $order['tip'] + $order['tax'] - (!empty($request['discount']) ? $request['discount'] : 0) - $request['discount_amount'] + $request['min_order_difference'];
        $order['subtotal']        = Cart::subtotal();
        // $order['payment_details'] = 'paypal';
        $order['payment_status'] = 'Paid';

        $order['tax_rate']             = $restaurant_data->tax_rate;
        $order['CCR']                  = $restaurant_data->CCR;
        $order['CCF']                  = $restaurant_data->CCF;
        $order['ZR']                   = $restaurant_data->ZR;
        $order['ZF']                   = $restaurant_data->ZF;
        $order['ZC']                   = $restaurant_data->ZC;
        $order['restaurant_id']        = $restaurant;
        $order['min_order_difference'] = $request['min_order_difference'];

        $order['user_id'] = user_id();

        if (Auth::user()) {
            $order['user_type'] = 'App\Client';
        } else {
            $order['user_type'] = user_type();
        }
 $order['json_data']=$request['json_data'];
        $order['discount_amount'] = $request['discount_amount'];
        $order                    = Order::create($order);
        $field['order_id']        = $order->id;

        foreach ($carts as $item) {
            $field['menu_id'] = $item->id;

            $field['quantity']   = $item->qty;
            $field['unit_price'] = $item->price;

            foreach ($item->options as $key => $option) {

                if ($key == 'menu_addon') {
                    $field['menu_addon'] = $option;
                } elseif ($key == 'special_instr') {
                    $field['special_instr'] = $option;
                } elseif ($key == 'addons') {
                    $field['addons'] = $option;
                }
                elseif ($key == 'variation') {
                    $field['variation'] = $option;
                }

            }

            $field['price']     = $item->price * $item->qty;
            $field['user_type'] = user_type();
            $field['user_id']   = user_id();
            $detail             = Details::create($field);
        }

        return $order;
    }

    public function orderMail($order,$request)
    {
       // dd($order);

        Order::where('id', $order->id)->update(['payment_status' => 'Paid', 'loyalty_points' => (Cart::total() * 10), 'payment_details' => !empty($request['payment_response']) ? $request['payment_response'] : '', 'billing_address_id' => !empty($request['billing_address_response']) ? $request['billing_address_response'] : null]);
        $order = Order::where('id', $order->id)->first();
        $user  = user();
      //  return view('cart::public.cart.message', ['order' => $order,'user'=>$user ,'order_detail' => Cart::content()]);

        Mail::send('cart::public.cart.message', ['order' => $order,'user'=>$user ,'order_detail' => Cart::content()], function ($message) use ($user) {
            $message->from('support@zingmyorder.com', 'ZingMyOrder');
            $message->to($user->email)->subject('Zing Order Confirmation');
        });

        if (!empty($order->restaurant->confirmation_mail)) {
            foreach (explode(',', $order->restaurant->confirmation_mail) as $confm_email) {
                Mail::send('cart::public.cart.restaurant_mail', ['order' => $order,'user'=>$user, 'order_detail' => Cart::content(), 'user' => $user], function ($message) use ($user, $order, $confm_email) {
                    $message->from('support@zingmyorder.com', 'ZingMyOrder');
                    $message->to($confm_email)->subject('Zing Order Confirmation');
                });
            }
        }
        if (!empty($order->restaurant->confirmation_text_mail)) {
            Mail::send('cart::public.cart.restaurant_text_mail', ['order' => $order, 'order_detail' => Cart::content()], function ($message) use ($user, $order) {
                $message->from('support@zingmyorder.com', 'ZingMyOrder');
                $message->to($order->restaurant->confirmation_text_mail)->subject('Zing Order Confirmation');
            });
        }
        if (!empty($order->restaurant->confirmation_fax_mail)) {
            Mail::send('cart::public.cart.restaurant_mail', ['order' => $order, 'order_detail' => Cart::content(), 'user' => $user], function ($message) use ($user, $order) {
                $message->from('support@zingmyorder.com', 'ZingMyOrder');
                $message->to($order->restaurant->confirmation_fax_mail)->subject('Zing Order Confirmation');
            });
        }
         Cart::destroy();
        return $order;
    }

}
