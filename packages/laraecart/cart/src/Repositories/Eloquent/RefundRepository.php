<?php

namespace Laraecart\Cart\Repositories\Eloquent;

use Laraecart\Cart\Interfaces\RefundRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class RefundRepository extends BaseRepository implements RefundRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('laraecart.cart.refund.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('laraecart.cart.refund.model.model');

    }
    public function refundByorder($id){
        $return= $this->model->where('order_number',$id)->sum('order_amount');
        return $return;

    }
}
