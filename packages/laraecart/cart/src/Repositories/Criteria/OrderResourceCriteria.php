<?php

namespace Laraecart\Cart\Repositories\Criteria;

use Litepie\Repository\Contracts\CriteriaInterface;
use Litepie\Repository\Contracts\RepositoryInterface;

class OrderResourceCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
        if (user()->hasRole('client')) {
    		$model = $model
                        ->where('user_id','=', user_id())
                        ->where('user_type','=', user_type())
                        ->orderBy('id','DESC');
    	}
        if (user()->hasRole('restaurant')) {
            $model = $model
                        ->where('restaurant_id','=', user_id())
                        ->where('payment_status','Paid')
                        ->orderBy('id','DESC');
        }
        return $model;
    }
}