<?php

namespace Laraecart\Cart\Repositories\Criteria;

use Litepie\Repository\Contracts\CriteriaInterface;
use Litepie\Repository\Contracts\RepositoryInterface;

class AddressResourceCriteria implements CriteriaInterface {

    public function apply($model, RepositoryInterface $repository)
    {
    	if(user()->hasRole('client')){
    		$model = $model->where('client_id',user_id());
    	}
    	else{
    		$model = $model;
    	}
        
        return $model;
    }
}