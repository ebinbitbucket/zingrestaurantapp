<?php

namespace Laraecart\Cart;

use DB;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('orders')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'cart.order.view',
                'name'      => 'View Order',
            ],
            [
                'slug'      => 'cart.order.create',
                'name'      => 'Create Order',
            ],
            [
                'slug'      => 'cart.order.edit',
                'name'      => 'Update Order',
            ],
            [
                'slug'      => 'cart.order.delete',
                'name'      => 'Delete Order',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/cart/order',
                'name'        => 'Order',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/cart/order',
                'name'        => 'Order',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'order',
                'name'        => 'Order',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Cart',
                'module'    => 'Order',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'cart.order.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
