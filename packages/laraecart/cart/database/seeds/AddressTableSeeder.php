<?php

namespace Laraecart\Cart;

use DB;
use Illuminate\Database\Seeder;

class AddressTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('addresses')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'cart.address.view',
                'name'      => 'View Address',
            ],
            [
                'slug'      => 'cart.address.create',
                'name'      => 'Create Address',
            ],
            [
                'slug'      => 'cart.address.edit',
                'name'      => 'Update Address',
            ],
            [
                'slug'      => 'cart.address.delete',
                'name'      => 'Delete Address',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/cart/address',
                'name'        => 'Address',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/cart/address',
                'name'        => 'Address',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'address',
                'name'        => 'Address',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Cart',
                'module'    => 'Address',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'cart.address.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
