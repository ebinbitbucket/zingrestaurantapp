<?php

namespace Laraecart\Cart;

use DB;
use Illuminate\Database\Seeder;

class HistoryTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('histories')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'cart.history.view',
                'name'      => 'View History',
            ],
            [
                'slug'      => 'cart.history.create',
                'name'      => 'Create History',
            ],
            [
                'slug'      => 'cart.history.edit',
                'name'      => 'Update History',
            ],
            [
                'slug'      => 'cart.history.delete',
                'name'      => 'Delete History',
            ],
            
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/cart/history',
                'name'        => 'History',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/cart/history',
                'name'        => 'History',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'history',
                'name'        => 'History',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Cart',
                'module'    => 'History',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'cart.history.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
