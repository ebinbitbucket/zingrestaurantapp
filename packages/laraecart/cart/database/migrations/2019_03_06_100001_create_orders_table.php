<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: orders
         */
        Schema::create('orders', function ($table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->string('email', 255)->nullable();
            $table->text('address')->nullable();
            $table->string('phone', 15)->nullable();
            $table->text('street')->nullable();
            $table->integer('zipcode')->nullable();
            $table->integer('state_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->text('shipping_name')->nullable();
            $table->string('shipping_city', 255)->nullable();
            $table->string('shipping_zipcode', 255)->nullable();
            $table->string('shipping_email', 255)->nullable();
            $table->string('shipping_address', 255)->nullable();
            $table->string('shipping_phone', 255)->nullable();
            $table->string('track_status', 255)->nullable();
            $table->text('shipping')->nullable();
            $table->integer('delivery_id')->nullable();
            $table->increments('subtotal')->nullable();
            $table->increments('tax')->nullable();
            $table->increments('total')->nullable();
            $table->enum('payment_status', ['Paid','Unpaid'])->nullable();
            $table->string('payment_methods', 255)->nullable();
            $table->string('payment_details', 255)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('orders');
    }
}
