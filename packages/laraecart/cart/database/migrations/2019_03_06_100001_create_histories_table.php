<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateHistoriesTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: histories
         */
        Schema::create('histories', function ($table) {
            $table->increments('id');
            $table->integer('order_id')->nullable();
            $table->string('courier_name', 255)->nullable();
            $table->string('order_status', 555)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('histories');
    }
}
