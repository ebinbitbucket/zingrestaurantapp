<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateDetailsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: details
         */
        Schema::create('details', function ($table) {
            $table->increments('id');
            $table->integer('order_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->text('size')->nullable();
            $table->integer('quantity')->nullable();
            $table->string('unit_price', 255)->nullable();
            $table->string('price', 255)->nullable();
            $table->integer('user_id')->nullable();
            $table->text('user_type')->nullable();
            $table->string('parameters', 255)->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('details');
    }
}
