<?php

Route::group(['prefix' => 'api'], function () {

    Route::group(['prefix' => 'admin/orders'], function () {
        Route::middleware('auth:admin.api')->group(function () {
            Route::post('get-orders', 'OrderApiController@getAllOrder');
           

        });
    });
    Route::get('orders/automatic-call', 'OrderApiController@OrderAutomatiCall');
    Route::post('orders/call-voice', 'OrderApiController@OrderCallVoice');
});
