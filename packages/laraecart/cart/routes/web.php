<?php


Route::prefix('{guard}/cart')->group(function () {
	Route::get('restaurant/orders/{status?}', 'OrderResourceController@restauarntOrders');

});


// Resource routes  for cart
Route::group(['prefix' => set_route_guard('web').'/cart'], function () {
	Route::resource('cart', 'CartResourceController');
	
});

// Public  routes for cart
Route::get('cart/popular/{period?}', 'CartPublicController@popular');
Route::get('carts/save/{id?}', 'CartPublicController@cart');
Route::get('carts/remove/{id?}', 'CartPublicController@remove');
Route::get('carts/check/{id?}', 'CartPublicController@checkRestaurant');
// Route::get('cart/checkout/', 'CartPublicController@welcome');
Route::get('cart/checkout_edit/{id}', 'CartPublicController@editCheckout');
Route::get('cart/checkout/restaurant_time/{restaurant_id?}/{date?}/{time?}', 'CartPublicController@timeCheck'); 
// Route::get('carts/add/{id?}', 'CartPublicController@add');
// Route::get('carts/subtract/{id?}', 'CartPublicController@subtract');
Route::get('cart/delivery/', 'CartPublicController@deliveryaddress');
Route::get('cart/deliveryedit/{id}', 'CartPublicController@deliveryaddressEdit');
Route::get('success/{id?}', 'CartPublicController@success');
Route::get('cart/checkoutjscurlrequestdevportal', 'CartPublicController@paymentRequest');
Route::get('cart/payment/response', 'CartPublicController@paymentResponse');
Route::get('cart/restaurantcart', 'CartPublicController@restaurantCart');
Route::get('cart/restaurantcartHeader', 'CartPublicController@restaurantcartHeader');
Route::get('cart/restaurantcartPage', 'CartPublicController@restaurantcartPage');



// Resource routes  for address
Route::group(['prefix' => set_route_guard('web').'/cart'], function () {
	Route::get('saved_address/{id?}', 'AddressResourceController@saved_address');
	Route::get('saved_billing_address/{id?}', 'AddressResourceController@saved_billing_address');
	Route::get('user-address', 'AddressResourceController@getUseraddress');
    Route::get('user-address/add', 'AddressResourceController@addUseraddress');
    Route::post('user-address/save', 'AddressResourceController@saveUseraddress');
    Route::get('billing_address', 'AddressResourceController@getBillingAddress');
    Route::get('billing_address/{id?}/edit', 'AddressResourceController@editBillingAddress');
    Route::get('billing_address/add', 'AddressResourceController@addBillingaddress');
    Route::resource('address', 'AddressResourceController');
});

// Public  routes for address
Route::get('address/delivery', 'AddressPublicController@cartAddressList');
Route::get('address/delivery/saved_address/{id}/{rest}', 'AddressPublicController@savedAddressPage');
Route::get('address/popular/{period?}', 'AddressPublicController@popular');



// Resource routes  for order
// Route::group(['prefix' => set_route_guard('web').'/cart'], function () {
    Route::prefix('{guard}/cart')->group(function () {

	Route::get('reorder/{id?}', 'OrderResourceController@reorder');
	Route::get('order/details/{id?}', 'OrderResourceController@orderdetails');
	Route::get('order/success/{id?}', 'OrderResourceController@orderSuccess');
	Route::get('reorder/delivery/{id?}', 'OrderResourceController@reorderDelivery');
	Route::get('order/payment/{id?}', 'OrderResourceController@orderPayment');
	Route::get('myorders/{status?}', 'OrderResourceController@myorders');
	// Route::get('restaurant/orders/{status?}', 'OrderResourceController@restauarntOrders');
	Route::get('restaurant/orders-refund-save', 'OrderResourceController@restauarntOrdersRefundSave');
	Route::get('restaurant/getNeworders', 'OrderResourceController@restauarntNewOrders');
	Route::get('restaurant/getNewordersupdate', 'OrderResourceController@restauarntNewOrdersUpdate');
	Route::get('restaurant/getScheduledupdate', 'OrderResourceController@restauarntScheduledOrdersUpdate');
	Route::get('restaurant/orders/show_detail/{id?}', 'OrderResourceController@showDetail');
	Route::get('kitchen/orders/show_detail/{id?}', 'OrderResourceController@KitchenshowDetail');
	Route::get('kitchen/orders/car_detail/{id?}', 'OrderResourceController@carDetail');
	Route::get('restaurant/orders/status_model/{id?}', 'OrderResourceController@statusRestaurantModel');
	Route::get('restaurant/orders/refund_model/{id?}', 'OrderResourceController@refundRestaurantModel');
	Route::get('kitchen/orders/status_model/{id?}', 'OrderResourceController@statusModel');
	Route::get('kitchen/orders/accept_model/{id?}', 'OrderResourceController@acceptModel');
	Route::get('order/status_update/{status?}/{id?}', 'OrderResourceController@statusUpdate');
	Route::get('points', 'OrderResourceController@getClientPoints');
	Route::get('order/addtofavourite/{id?}/{status?}', 'OrderResourceController@addToFavourite');
	Route::get('orders/kitchen_orders/{status?}', 'OrderResourceController@kitchen_orders');
	Route::get('kitchen/getNeworders', 'OrderResourceController@kitchenNewOrders');
	Route::get('orders/timeextend/{order?}/{time_value?}', 'OrderResourceController@kitchenTimeUpdate');
	Route::get('kitchen/getKitchenScheduledupdate', 'OrderResourceController@kitchenScheduledOrdersUpdate');
	Route::get('payments', 'OrderResourceController@paymentHistory');
	Route::get('payments/download', 'OrderResourceController@downloadCSV');
	Route::get('payments/filter', 'OrderResourceController@paymentFilterHistory');
	Route::get('payments/update/{status?}', 'OrderResourceController@paymentStatusUpdate');
	Route::get('orders/print/{id?}', 'OrderResourceController@orderPrint');
	Route::get('kitchen/getnewordersapp', 'OrderResourceController@kitchenNewOrderApp');
// 	Route::get('app/getnewordersappcount', 'OrderResourceController@kitchenNewOrderAppCount');
	Route::get('app/{status?}', 'OrderResourceController@kitchenOrdersApp');
	Route::get('orders/monthstmnt', 'OrderResourceController@monthStmnt');
	Route::get('orders/monthstmnt/filter', 'OrderResourceController@monthStmntFilter');
	Route::get('orders/monthstmnt/filterNew', 'OrderResourceController@monthStmntFilterNew');
	Route::get('orders/monthstmnt/download_pdf', 'OrderResourceController@monthStmntPDF');
	Route::get('orders/report', 'OrderResourceController@pivotReports');
	Route::get('orders/import-missingdata', 'OrderResourceController@orderImport');
	Route::get('orders/car-pickup/complete/{id}', 'OrderResourceController@completePickup');
	Route::any('order/refund', 'OrderResourceController@orderRefund');
	Route::resource('order', 'OrderResourceController');
	Route::get('refund/{status}/{id}', 'RefundResourceController@refundEmails');
	Route::post('refund/reson-save', 'RefundResourceController@refundEmailsSave');
	Route::resource('refund', 'RefundResourceController');
    Route::resource('calls', 'CallsResourceController');


});



Route::get('order/popular/{period?}', 'OrderPublicController@popular');
Route::get('unsubscribe/{id?}', 'OrderPublicController@unsubscribe');
Route::get('order/car-delivery/{id?}', 'OrderPublicController@carDelivery');
Route::get('order/car-delivery-save', 'OrderPublicController@carDeliverySave');
Route::get('refund', 'OrderPublicController@refundRequest');
Route::get('order/refund-request-save', 'OrderPublicController@refundSave');
Route::get('order/resturant-list', 'OrderPublicController@resturantList');
Route::get('order/postmates-delivery', 'OrderPublicController@postmatesDelivery');

Route::get('client/cart/order/feedback/{id?}', 'OrderPublicController@getFeedbackForm');
Route::get('app/getnewordersappcount/{apitoken}', 'OrderPublicController@kitchenNewOrderAppCount');
Route::get('app/getnewordersappcount/{id}/{apitoken}', 'OrderPublicController@kitchenNewOrderAppCount');
Route::get('app/getnewordersappcount/{apitoken}', 'OrderPublicController@kitchenNewOrderAppCount1');
Route::get('cart/kitchen/scheduled-update', 'OrderPublicController@kitchenScheduledOrdersUpdate');
Route::get('order/cart/details/last-seven/datas', 'OrderPublicController@sevenDayData');




// Resource routes  for details
Route::group(['prefix' => set_route_guard('web').'/cart'], function () {
    Route::resource('details', 'DetailsResourceController');
});

// Public  routes for details
Route::get('details/popular/{period?}', 'DetailsPublicController@popular');



// Resource routes  for history
Route::group(['prefix' => set_route_guard('web').'/cart'], function () {
    Route::resource('history', 'HistoryResourceController');
});

// Public  routes for history
Route::get('history/popular/{period?}', 'HistoryPublicController@popular');


Route::get('cart/checkout/', 'CartnewPublicController@getCheckout');
Route::get('carts/add/{id?}', 'CartnewPublicController@add');
Route::get('carts/subtract/{id?}', 'CartnewPublicController@subtract');
Route::get('cart/checkFields', 'CartnewPublicController@checkFields');
Route::get('cart/gettoken', 'CartnewPublicController@getToken');
// Route::post('cart/paymentsubmit', 'CartnewPublicController@paymentSubmit');
Route::post('cart/paypalsubmit', 'CartnewPublicController@paypalSubmit');
Route::post('cart/paymentsubmit', 'CartnewPublicController@nuviewSubmit');
Route::get('cart/order-test/{order_id}', 'CartnewPublicController@orderKitchen');
Route::get('cart/send-order-notification', 'CartnewPublicController@sendOrderNotifications');






// Route::get('cart/checkout/receiptform', 'CartnewPublicController@getReceipt');

