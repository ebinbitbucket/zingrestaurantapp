<?php

// API routes  for restaurant_app
Route::prefix('{guard}/restaurantapp')->group(function () {
    Route::get('restaurant_app/form/{element}', 'RestaurantAppAPIController@form');
    Route::resource('restaurant_app', 'RestaurantAppAPIController');
});

// Public routes for restaurant_app
Route::get('restaurantapps/', 'RestaurantAppPublicController@index');
Route::get('restaurantapps/{slug?}', 'RestaurantAppPublicController@show');

if (Trans::isMultilingual()) {
    Route::group(
        [
            'prefix' => '{trans}',
            'where'  => ['trans' => Trans::keys('|')],
        ],
        function () {
            // Guard routes for restaurantapps
            Route::prefix('{guard}/restaurantapp')->group(function () {
                Route::get('restaurant_app/form/{element}', 'RestaurantAppAPIController@form');
                Route::apiResource('restaurant_app', 'RestaurantAppAPIController');
            });
            // Public routes for restaurantapps
            Route::get('restaurantapp/RestaurantApp', 'RestaurantAppPublicController@getRestaurantApp');
        }
    );
}

