<?php

// web routes  for restaurant_app
Route::prefix('{guard}/app')->group(function () {
    Route::resource('restaurant_app', 'RestaurantAppResourceController');



    Route::get('about/{slug?}/', 'RestaurantAppResourceController@about');
    Route::get('menu/{slug?}/', 'RestaurantAppResourceController@menu');
    Route::get('gallery/{slug?}/', 'RestaurantAppResourceController@gallery');
    Route::get('contact/{slug?}/', 'RestaurantAppResourceController@contact');
    Route::get('locations/{slug?}/', 'RestaurantAppResourceController@locations');
    Route::get('account/{slug?}/', 'RestaurantAppResourceController@myAccount');
    Route::get('home/{slug?}/', 'RestaurantAppResourceController@restaurantAppHome');
    Route::get('orders/{status}/{slug?}/', 'RestaurantAppResourceController@myOrders');
    
    Route::get('address/{slug?}/', 'RestaurantAppResourceController@myAddress');
    Route::get('edit-profile/{slug?}/', 'RestaurantAppResourceController@editProfile');
    Route::get('favorites/{slug?}/', 'RestaurantAppResourceController@viewFavorites');
    Route::get('points/{slug?}/', 'RestaurantAppResourceController@viewPoints');
    
    Route::post('sendmail/{slug?}/', 'RestaurantAppResourceController@sendMail');
    
    Route::get('logout/{slug?}', 'RestaurantAppResourceController@logoutClient');

	Route::get('order/addtofavourite/{id?}/{status?}', 'RestaurantAppResourceController@addToFavourite');

    


});

// Route::group(['prefix' => set_route_guard('web').'/app'], function () {

    
    // Route::get('about/{slug?}/', 'RestaurantAppController@about');
    // Route::get('menu/{slug?}/', 'RestaurantAppController@menu');
    // Route::get('gallery/{slug?}/', 'RestaurantAppController@gallery');
    // Route::get('contact/{slug?}/', 'RestaurantAppController@contact');
    // Route::get('locations/{slug?}/', 'RestaurantAppController@locations');
    // Route::get('account/{slug?}/', 'RestaurantAppController@myAccount');
    // Route::get('home/{slug?}/', 'RestaurantAppResourceController@restaurantAppHome');
    
    // Route::get('address/{slug?}/', 'RestaurantAppController@myAddress');
    // Route::get('orders/{status}/{slug?}/', 'RestaurantAppController@myOrders');
    // Route::get('edit-profile/{slug?}/', 'RestaurantAppController@editProfile');
    // Route::get('favorites/{slug?}/', 'RestaurantAppController@viewFavorites');
    // Route::get('points/{slug?}/', 'RestaurantAppController@viewPoints');
    
    // Route::post('sendmail/{slug?}/', 'RestaurantAppController@sendMail');
    
    // Route::get('logout/{slug?}', 'RestaurantAppController@logoutClient');
    
    
    // });

// Public routes for restaurant_app
Route::get('app/register/{slug?}/', 'RestaurantAppPublicController@restaurantAppRegister');
Route::get('app/{slug?}/', 'RestaurantAppPublicController@restaurantAppLogin');
Route::get('app/password/reset/{slug?}', 'RestaurantAppPublicController@passwordReset');
Route::post('app/passwordUpdate', 'RestaurantAppPublicController@postPassword');
$this->get('password/change/{token}/{slug}/{client_id}', 'RestaurantAppPublicController@showResetForm');
// Route::get('client/app/orders/{status}/{slug?}/', 'RestaurantAppPublicController@myOrders');


// Route::get('restaurantapps/', 'RestaurantAppPublicController@index');
// Route::get('restaurantapps/{slug?}', 'RestaurantAppPublicController@show');

if (Trans::isMultilingual()) {
    Route::group(
        [
            'prefix' => '{trans}',
            'where'  => ['trans' => Trans::keys('|')],
        ],
        function () {
            // Guard routes for pages
            Route::prefix('{guard}/page')->group(function () {
                Route::apiResource('page', 'RestaurantAppResourceController');
            });
            // Public routes for pages
            Route::get('restaurantapps/', 'RestaurantAppPublicController@index');
            Route::get('restaurantapps/{slug?}', 'RestaurantAppPublicController@show');
        }
    );
}

