# Installation

The instructions below will help you to properly install the generated package to the lavalite project.

## Location

Extract the package contents to the folder 

`/packages/litecms/restaurantapp/`

## Composer

Add the below entries in the `composer.json` file's autoload section and run the command `composer dump-autoload` in terminal.

```json

...
     "autoload": {
         ...

        "classmap": [
            ...
            
            "packages/litecms/restaurantapp/database/seeds",
            
            ...
        ],
        "psr-4": {
            ...
            
            "Litecms\\Restaurantapp\\": "packages/litecms/restaurantapp/src",
            
            ...
        }
    },
...

```

## Config

Add the entries in service provider in `config/app.php`

```php

...
    'providers'       => [
        ...
        
        Litecms\Restaurantapp\Providers\RestaurantappServiceProvider::class,
        
        ...
    ],

    ...

    'alias'             => [
        ...
        
        'Restaurantapp'  => Litecms\Restaurantapp\Facades\Restaurantapp::class,
        
        ...
    ]
...

```

## Migrate

After service provider is set run the commapnd to migrate and seed the database.


    php artisan migrate
    php artisan db:seed --class=Litecms\\RestaurantappTableSeeder

## Publishing


**Publishing configuration**

    php artisan vendor:publish --provider="Litecms\Restaurantapp\Providers\RestaurantappServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Litecms\Restaurantapp\Providers\RestaurantappServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Litecms\Restaurantapp\Providers\RestaurantappServiceProvider" --tag="view"


## URLs and APIs


### Web Urls

**Admin**

    http://path-to-route-folder/admin/restaurantapp/{modulename}

**User**

    http://path-to-route-folder/user/restaurantapp/{modulename}

**Public**

    http://path-to-route-folder/restaurantapps


### API endpoints

**List**
 
    http://path-to-route-folder/api/user/restaurantapp/{modulename}
    METHOD: GET

**Create**

    http://path-to-route-folder/api/user/restaurantapp/{modulename}
    METHOD: POST

**Edit**

    http://path-to-route-folder/api/user/restaurantapp/{modulename}/{id}
    METHOD: PUT

**Delete**

    http://path-to-route-folder/api/user/restaurantapp/{modulename}/{id}
    METHOD: DELETE

**Public List**

    http://path-to-route-folder/api/restaurantapp/{modulename}
    METHOD: GET

**Public Single**

    http://path-to-route-folder/api/restaurantapp/{modulename}/{slug}
    METHOD: GET