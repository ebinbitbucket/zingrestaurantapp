Lavalite package that provides restaurantapp management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `litecms/restaurantapp`.

    "litecms/restaurantapp": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Litecms\Restaurantapp\Providers\RestaurantappServiceProvider::class,

And also add it to alias

    'Restaurantapp'  => Litecms\Restaurantapp\Facades\Restaurantapp::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Litecms\\RestaurantappTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Litecms\Restaurantapp\Providers\RestaurantappServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Litecms\Restaurantapp\Providers\RestaurantappServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Litecms\Restaurantapp\Providers\RestaurantappServiceProvider" --tag="view"


### Web Urls

**Admin**

    http://path-to-route-folder/admin/restaurantapp/{modulename}

**User**

    http://path-to-route-folder/user/restaurantapp/{modulename}

**Public**

    http://path-to-route-folder/restaurantapps


### API endpoints

**List**

    http://path-to-route-folder/api/user/restaurantapp/{modulename}
    METHOD: GET

**Create**

    http://path-to-route-folder/api/user/restaurantapp/{modulename}
    METHOD: POST

**Edit**

    http://path-to-route-folder/api/user/restaurantapp/{modulename}/{id}
    METHOD: PUT

**Delete**

    http://path-to-route-folder/api/user/restaurantapp/{modulename}/{id}
    METHOD: DELETE

**Public List**

    http://path-to-route-folder/api/restaurantapp/{modulename}
    METHOD: GET

**Public Single**

    http://path-to-route-folder/api/restaurantapp/{modulename}/{slug}
    METHOD: GET