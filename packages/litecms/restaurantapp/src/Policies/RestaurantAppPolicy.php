<?php

namespace Litecms\Restaurantapp\Policies;

use Litepie\User\Contracts\UserPolicy;
use Litecms\Restaurantapp\Models\RestaurantApp;

class RestaurantAppPolicy
{

    /**
     * Determine if the given user can view the restaurant_app.
     *
     * @param UserPolicy $user
     * @param RestaurantApp $restaurant_app
     *
     * @return bool
     */
    public function view(UserPolicy $user, RestaurantApp $restaurant_app)
    {
        if ($user->canDo('restaurantapp.restaurant_app.view') && $user->isAdmin()) {
            return true;
        }

        return $restaurant_app->user_id == user_id() && $restaurant_app->user_type == user_type();
    }

    /**
     * Determine if the given user can create a restaurant_app.
     *
     * @param UserPolicy $user
     * @param RestaurantApp $restaurant_app
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('restaurantapp.restaurant_app.create');
    }

    /**
     * Determine if the given user can update the given restaurant_app.
     *
     * @param UserPolicy $user
     * @param RestaurantApp $restaurant_app
     *
     * @return bool
     */
    public function update(UserPolicy $user, RestaurantApp $restaurant_app)
    {
        if ($user->canDo('restaurantapp.restaurant_app.edit') && $user->isAdmin()) {
            return true;
        }

        return $restaurant_app->user_id == user_id() && $restaurant_app->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given restaurant_app.
     *
     * @param UserPolicy $user
     * @param RestaurantApp $restaurant_app
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, RestaurantApp $restaurant_app)
    {
        return $restaurant_app->user_id == user_id() && $restaurant_app->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given restaurant_app.
     *
     * @param UserPolicy $user
     * @param RestaurantApp $restaurant_app
     *
     * @return bool
     */
    public function verify(UserPolicy $user, RestaurantApp $restaurant_app)
    {
        if ($user->canDo('restaurantapp.restaurant_app.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given restaurant_app.
     *
     * @param UserPolicy $user
     * @param RestaurantApp $restaurant_app
     *
     * @return bool
     */
    public function approve(UserPolicy $user, RestaurantApp $restaurant_app)
    {
        if ($user->canDo('restaurantapp.restaurant_app.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
