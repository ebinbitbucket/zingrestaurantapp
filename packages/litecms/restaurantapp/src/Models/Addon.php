<?php

namespace Restaurant\Restaurant\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;

use Litepie\Trans\Traits\Translatable;
class Addon extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable,  PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.restaurant.addon.model';

     public function restaurant()
     {
          return $this->belongsTo('Restaurant\Restaurant\Models\Restaurant','restaurant_id');
     }

     public function variation()
    {
        return $this->hasMany('Restaurant\Restaurant\Models\Addonvariations','addon_id');
    }
    

}
