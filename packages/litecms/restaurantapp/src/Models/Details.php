<?php

namespace Litecms\Restaurantapp\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Trans\Traits\Translatable;
class Details extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable, PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
    //  protected $config = 'laraecart.cart.details.model';
     protected $config = 'restaurant.restaurant.details.model';



    /**
     * The order that belong to the details.
     */
    public function order(){
        return $this->belongsTo('Cart\Details\Models\Order');
    }

    public function restaurant() {

        return $this->belongsTo('Restaurant\Restaurant\Models\restaurant', 'restaurant');
    }

    public function menu() {

        return $this->belongsTo('Litecms\Restaurantapp\Models\Menu', 'menu_id');
    }

    public function addon() {

        return $this->belongsTo('Restaurant\Restaurant\Models\Addonvariations', 'menu_id');
    }

}
