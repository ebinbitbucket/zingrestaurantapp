<?php

namespace Litecms\Restaurantapp\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;

use Litepie\Trans\Traits\Translatable;
class Menu extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable,  PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.restaurant.menu.model';


    /**
     * The category that belong to the menu.
     */
    public function category(){
        return $this->hasMany('Restaurant\Menu\Models\Category');
    }


    public function categories()
     {
        return $this->belongsTo('Restaurant\Restaurant\Models\Category','category_id');
     }

     public function restaurant()
     {
        return $this->belongsTo('Restaurant\Restaurant\Models\Restaurant','restaurant_id');
     }

     public function addon()
    {
        return $this->belongsToMany('Restaurant\Restaurant\Models\Addon', 'menu_addon', 'menu_id', 'addon_id');
    }

    public function menuaddon(){
        return $this->hasMany('Restaurant\Restaurant\Models\MenuAddon','menu_id');
    }
    
    public function master()
     {
        return $this->belongsTo('Restaurant\Master\Models\Master','master_category');
     }

}
