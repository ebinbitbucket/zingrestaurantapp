<?php

// namespace Restaurant\Restaurant\Models;
namespace Litecms\Restaurantapp\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;

use Litepie\Trans\Traits\Translatable;
class Restaurantapp extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable,  PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
    //  protected $config = 'restaurant_app.model';
    //  protected $config = 'Litecms.Restaurantapp.Restaurantapp.model';
     protected $config = 'restaurant.restaurant.restaurant_app.model';


     public function restaurant()
     {
          return $this->belongsTo('Litecms\Restaurantapp\Models\Restaurant','restaurant_id');
     }


}
