<?php

namespace Restaurant\Restaurant\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Trans\Traits\Translatable;

class Location extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable,  PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.restaurant.location.model';

      public function parent(){
        return $this->belongsTo('Restaurant\Restaurant\Models\Location','parent_id');
    }

     public function locations(){
        return $this->hasMany('Restaurant\Restaurant\Models\Location','parent_id','id');
    }
}
