<?php

namespace Litecms\Restaurantapp\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;

use Litepie\Trans\Traits\Translatable;
class Category extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable,  PresentableTrait;


    /**
     * Configuartion for the model.
     *
     * @var array
     */
     protected $config = 'restaurant.restaurant.category.model';

     public function category()
     {
     	return $this->belongsTo('Litecms\Restaurantapp\Models\Category','parent_id');
     }

     public function restaurant()
     {
     	return $this->belongsTo('Litecms\Restaurantapp\Models\Restaurant','restaurant_id');
     }

     public function menu()
     {
        return $this->hasMany('Litecms\Restaurantapp\Models\Menu','category_id');
     }
}
