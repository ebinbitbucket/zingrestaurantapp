<?php

namespace Litecms\Restaurantapp\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Litepie\Database\Model;
use Litepie\Database\Traits\Slugger;
use Litepie\Filer\Traits\Filer;
use Litepie\Hashids\Traits\Hashids;
use Litepie\Repository\Traits\PresentableTrait;
use Litepie\Trans\Traits\Translatable;

class Cart extends Model
{
    use Filer, SoftDeletes, Hashids, Slugger, Translatable, PresentableTrait;

    /**
     * Configuartion for the model.
     *
     * @var array
     */
    protected $config = 'restaurant.restaurant.cart.model';

    public function getFavouriteStatus($order_id)
    {
        return Favourite::where('favourite_id',$order_id)->where('type','order')->where('user_id',user_id())->count();
    }

}
