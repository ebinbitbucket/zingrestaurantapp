<?php

namespace Litecms\Restaurantapp\Providers;

use Litepie\Contracts\Workflow\Workflow as WorkflowContract;
use Litepie\Foundation\Support\Providers\WorkflowServiceProvider as ServiceProvider;

class WorkflowServiceProvider extends ServiceProvider
{
    /**
     * The validators mappings for the package.
     *
     * @var array
     */
    protected $validators = [
        
        // Bind RestaurantApp workflow validator
        'Litecms\Restaurantapp\Models\RestaurantApp' => \Litecms\Restaurantapp\Workflow\RestaurantAppValidator::class,
    ];

    /**
     * The actions mappings for the package.
     *
     * @var array
     */
    protected $actions = [
        
        // Bind RestaurantApp workflow actions
        'Litecms\Restaurantapp\Models\RestaurantApp' => \Litecms\Restaurantapp\Workflow\RestaurantAppAction::class,
    ];

    /**
     * The notifiers mappings for the package.
     *
     * @var array
     */
    protected $notifiers = [
       
        // Bind RestaurantApp workflow notifiers
        'Litecms\Restaurantapp\Models\RestaurantApp' => \Litecms\Restaurantapp\Workflow\RestaurantAppNotifier::class,
    ];

    /**
     * Register any package workflow validation services.
     *
     * @param \Litepie\Contracts\Workflow\Workflow $workflow
     *
     * @return void
     */
    public function boot(WorkflowContract $workflow)
    {
        parent::registerValidators($workflow);
        parent::registerActions($workflow);
        parent::registerNotifiers($workflow);
    }
}
