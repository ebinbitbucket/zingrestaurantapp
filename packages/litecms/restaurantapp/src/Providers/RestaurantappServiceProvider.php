<?php

namespace Litecms\Restaurantapp\Providers;

use Illuminate\Support\ServiceProvider;

class RestaurantappServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Load view
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'restaurantapp');

        // Load translation
        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'restaurantapp');

        // Load migrations
        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        // Call pblish redources function
        $this->publishResources();

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();
        $this->registerRestaurantapp();
        $this->registerFacade();
        $this->registerBindings();
        //$this->registerCommands();
    }


    /**
     * Register the application bindings.
     *
     * @return void
     */
    protected function registerRestaurantapp()
    {
        $this->app->bind('restaurantapp', function($app) {
            return new Restaurantapp($app);
        });
    }

    /**
     * Register the vault facade without the user having to add it to the app.php file.
     *
     * @return void
     */
    public function registerFacade() {
        $this->app->booting(function()
        {
            $loader = \Illuminate\Foundation\AliasLoader::getInstance();
            $loader->alias('Restaurantapp', 'Litecms\Restaurantapp\Facades\Restaurantapp');
        });
    }

    /**
     * Register bindings for the provider.
     *
     * @return void
     */
    public function registerBindings() {
        // Bind facade
        $this->app->bind('litecms.restaurantapp', function ($app) {
            return $this->app->make('Litecms\Restaurantapp\Restaurantapp');
        });
        

                // Bind RestaurantApp to repository
        $this->app->bind(
            'Litecms\Restaurantapp\Interfaces\RestaurantAppRepositoryInterface',
            \Litecms\Restaurantapp\Repositories\Eloquent\RestaurantAppRepository::class
        );

        $this->app->bind(
            'Litecms\Restaurantapp\Interfaces\RestaurantRepositoryInterface',
             \Litecms\Restaurantapp\Repositories\Eloquent\RestaurantRepository::class
        );
        $this->app->bind(
            'Litecms\Restaurantapp\Interfaces\OrderRepositoryInterface',
             \Litecms\Restaurantapp\Repositories\Eloquent\OrderRepository::class
        );

        $this->app->register(\Litecms\Restaurantapp\Providers\AuthServiceProvider::class);
                $this->app->register(\Litecms\Restaurantapp\Providers\EventServiceProvider::class);
        
        $this->app->register(\Litecms\Restaurantapp\Providers\RouteServiceProvider::class);
                // $this->app->register(\Litecms\Restaurantapp\Providers\WorkflowServiceProvider::class);
            
        }

    /**
     * Merges user's and restaurantapp's configs.
     *
     * @return void
     */
    protected function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/config.php', 'litecms.restaurantapp'
        );
    }

    /**
     * Register scaffolding command
     */
    protected function registerCommands()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Commands\MakeRestaurantapp::class,
            ]);
        }
    }
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['litecms.restaurantapp'];
    }

    /**
     * Publish resources.
     *
     * @return void
     */
    private function publishResources()
    {
        // Publish configuration file
        $this->publishes([__DIR__ . '/../../config/config.php' => config_path('litecms/restaurantapp.php')], 'config');

        // Publish admin view
        $this->publishes([__DIR__ . '/../../resources/views' => base_path('resources/views/vendor/restaurantapp')], 'view');

        // Publish language files
        $this->publishes([__DIR__ . '/../../resources/lang' => base_path('resources/lang/vendor/restaurantapp')], 'lang');

        // Publish public files and assets.
        $this->publishes([__DIR__ . '/public/' => public_path('/')], 'public');
    }
}
