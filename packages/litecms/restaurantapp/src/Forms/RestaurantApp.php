<?php

namespace Litecms\Restaurantapp\Forms;

class RestaurantApp
{
    /**
     * Variable to store form configuration.
     *
     * @var collection
     */
    protected $form;

    /**
     * Variable to store form configuration.
     *
     * @var collection
     */
    protected $element;

    /**
     * Initialize the form.
     *
     * @return null
     */
    public function __construct()
    {
        $this->setForm();
    }

    /**
     * Return form elements.
     *
     * @return array.
     */
    public function form($element = 'fields', $grouped = true)
    {
        $item = collect($this->form->get($element));
        if ($element == 'fields' && $grouped == true) {
            return $item->groupBy(['group', 'section']);
        }
        return $item;

    }

    /**
     * Sets the form and form elements.
     * @return null.
     */
    public function setForm()
    {
        $this->form = collect([
            'form' => [
                'store' => [],
                'update' => [],
            ],
            'groups' => [
                'main' => 'Main',
            ],
            'fields' => [
                'id' => [
                    "type" => 'numeric',
                    "label" => trans('restaurantapp::restaurant_app.label.id'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.id'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'restaurant_id' => [
                    "type" => 'numeric',
                    "label" => trans('restaurantapp::restaurant_app.label.restaurant_id'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.restaurant_id'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'point_percentage' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.point_percentage'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.point_percentage'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'points' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.points'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.points'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'title' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.title'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.title'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'subtitle' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.subtitle'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.subtitle'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'meta_tags' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.meta_tags'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.meta_tags'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'meta_keywords' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.meta_keywords'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.meta_keywords'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'meta_description' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.meta_description'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.meta_description'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'email' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.email'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.email'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'alternate_phone' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.alternate_phone'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.alternate_phone'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'gallery' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.gallery'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.gallery'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'slider_images' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.slider_images'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.slider_images'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'featured_food' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.featured_food'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.featured_food'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'locations' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.locations'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.locations'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'contact_info' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.contact_info'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.contact_info'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'menus' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.menus'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.menus'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                ')' => [
                    "type" => 'text',
                    "label" => trans('restaurantapp::restaurant_app.label.)'),
                    "placeholder" => trans('restaurantapp::restaurant_app.placeholder.)'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
            ]
        );

    }
}
