<?php

namespace Litecms\Restaurantapp\Facades;

use Illuminate\Support\Facades\Facade;

class Restaurantapp extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'litecms.restaurantapp';
    }
}
