<?php

namespace Restaurant\Restaurant\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class RestaurantTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Restaurant\Models\Restaurant $restaurant)
    {
        return [
            'id'                => $restaurant->getRouteKey(),
            'key'               => [
                'public'    => $restaurant->getPublicKey(),
                'route'     => $restaurant->getRouteKey(),
            ], 
            
            'name'              => $restaurant->name,
            'description'       => $restaurant->description,
            'address'           => $restaurant->address,
            'phone'             => $restaurant->phone,
            'email'             => $restaurant->email,
            'price_range'       => $restaurant->price_range,
            'country'           => $restaurant->country,
            'city'              => $restaurant->city,
            'state'             => $restaurant->state,
            'location'          => $restaurant->location,
            'zipcode'           => $restaurant->zipcode,
            'latitude'          => $restaurant->latitude,
            'longitude'         => $restaurant->longitude,
            'rating'            => $restaurant->rating,
            'logo'              => $restaurant->logo,
            'gallery'           => $restaurant->gallery,
            'delivery'          => $restaurant->delivery,
            'type'              => $restaurant->type,
            'created_at'        => $restaurant->created_at,
            'deleted_at'        => $restaurant->deleted_at,
            'updated_at'        => $restaurant->updated_at,
            'working_hours'     => $restaurant->working_hours,
            'url'               => [
                'public'    => trans_url('restaurant/'.$restaurant->getPublicKey()),
                'user'      => guard_url('restaurant/restaurant/'.$restaurant->getRouteKey()),
            ], 
            'status'            => trans($restaurant->status),
            'created_at'        => format_date($restaurant->created_at),
            'updated_at'        => format_date($restaurant->updated_at),
        ];
    }
}