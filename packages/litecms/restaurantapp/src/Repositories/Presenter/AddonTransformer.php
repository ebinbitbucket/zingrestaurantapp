<?php

namespace Restaurant\Restaurant\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class AddonTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Restaurant\Models\Addon $addon)
    {
        return [
            'id'                => $addon->getRouteKey(),
            'key'               => [
                'public'    => $addon->getPublicKey(),
                'route'     => $addon->getRouteKey(),
            ], 
            
            'restaurant_id'     => @$addon->restaurant->name,
            'name'              => $addon->name,
            'price'             => $addon->price,
            'slug'              => $addon->slug,
            'created_at'        => $addon->created_at,
            'deleted_at'        => $addon->deleted_at,
            'updated_at'        => $addon->updated_at,
            'url'               => [
                'public'    => trans_url('restaurant/'.$addon->getPublicKey()),
                'user'      => guard_url('restaurant/addon/'.$addon->getRouteKey()),
            ], 
            'status'            => trans($addon->status),
            'created_at'        => format_date($addon->created_at),
            'updated_at'        => format_date($addon->updated_at),
        ];
    }
}