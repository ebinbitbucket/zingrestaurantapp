<?php

namespace Restaurant\Restaurant\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class MenuTransformer extends TransformerAbstract
{
    public function transform(\Restaurant\Restaurant\Models\Menu $menu)
    {
        $stock_status=trans($menu->stock_status);
        if($stock_status=='1')
            $stock_status='On';
        else
            $stock_status='Off';
        return [
            'id'                => $menu->getRouteKey(),
            'key'               => [
                'public'    => $menu->getPublicKey(),
                'route'     => $menu->getRouteKey(),
            ], 
            
            'restaurant_id'     => @$menu->restaurant->name,
            'category_id'       => @$menu->categories->name,
            'master'            => @$menu->master->name,
            'name'              => $menu->name,
            'description'       => $menu->description,
            'price'             => $menu->price,
            'image'             => $menu->image,
            'addons'            => $menu->addons,
            'slug'              => $menu->slug,
            'status'            => $menu->status,
            'created_at'        => $menu->created_at,
            'deleted_at'        => $menu->deleted_at,
            'updated_at'        => $menu->updated_at,
            'url'               => [
                'public'    => trans_url('restaurant/'.$menu->getPublicKey()),
                'user'      => guard_url('restaurant/menu/'.$menu->getRouteKey()),
            ], 
            'status'            => trans($menu->status),
            'stock_status'      => $stock_status,
            'created_at'        => format_date($menu->created_at),
            'updated_at'        => format_date($menu->updated_at),
        ];
    }
}