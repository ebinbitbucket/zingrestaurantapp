<?php

namespace Litecms\Restaurantapp\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class RestaurantAppTransformer extends TransformerAbstract
{
    public function transform(\Litecms\Restaurantapp\Models\RestaurantApp $restaurant_app)
    {
        return [
            'id'                => $restaurant_app->getRouteKey(),
            'key'               => [
                'public'    => $restaurant_app->getPublicKey(),
                'route'     => $restaurant_app->getRouteKey(),
            ], 
            'restaurant_id'     => $restaurant_app->restaurant_id,
            'point_percentage'  => $restaurant_app->point_percentage,
            'points'            => $restaurant_app->points,
            'title'             => $restaurant_app->title,
            'subtitle'          => $restaurant_app->subtitle,
            'meta_tags'         => $restaurant_app->meta_tags,
            'meta_keywords'     => $restaurant_app->meta_keywords,
            'meta_description'  => $restaurant_app->meta_description,
            'email'             => $restaurant_app->email,
            'alternate_phone'   => $restaurant_app->alternate_phone,
            'gallery'           => $restaurant_app->gallery,
            'slider_images'     => $restaurant_app->slider_images,
            'featured_food'     => $restaurant_app->featured_food,
            'locations'         => $restaurant_app->locations,
            'contact_info'      => $restaurant_app->contact_info,
            'menus'             => $restaurant_app->menus,
            ')'                 => $restaurant_app->),
            'deleted_at'        => $restaurant_app->deleted_at,
            'url'               => [
                'public'    => trans_url('restaurantapp/'.$restaurant_app->getPublicKey()),
                'user'      => guard_url('restaurantapp/restaurant_app/'.$restaurant_app->getRouteKey()),
            ], 
            'status'            => trans('app.'.$restaurant_app->status),
            'created_at'        => format_date($restaurant_app->created_at),
            'updated_at'        => format_date($restaurant_app->updated_at),
        ];
    }
}