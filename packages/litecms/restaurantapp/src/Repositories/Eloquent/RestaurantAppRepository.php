<?php

namespace Litecms\Restaurantapp\Repositories\Eloquent;

use Litecms\Restaurantapp\Interfaces\RestaurantAppRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class RestaurantAppRepository extends BaseRepository implements RestaurantAppRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('litecms.restaurantapp.restaurant_app.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('litecms.restaurantapp.restaurant_app.model.model');
    }
}
