<?php

namespace Litecms\Restaurantapp\Repositories\Eloquent;

use DateTime;
use DB;
use Litepie\Repository\Eloquent\BaseRepository;
use Restaurant\Master\Models\Category;
use Litecms\Restaurantapp\Interfaces\RestaurantRepositoryInterface;
use Restaurant\Restaurant\Models\Restauranttimings;
use Session;
use Laraecart\Cart\Models\Order;
use Litecms\Restaurantapp\Models\Restaurant;



class RestaurantRepository extends BaseRepository implements RestaurantRepositoryInterface
{

    public function boot()
    {
        $this->fieldSearchable = config('restaurant.restaurant.restaurant.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('restaurant.restaurant.restaurant.model.model');
    }

    public function getRestaurant()
    {
        return $this->model->orderBy('name', 'ASC')->pluck('name', 'id');
    }
    public function getRestaurantDetails($id)
    {
        return $this->model->where('id', $id)->get();
    }

    public function getAllRestaurantsByMaster($restaurants_ids)
    {
        return $this->model->whereIn('id', $restaurants_ids)->whereIn('published', ['Published', 'No Sale'])->orderBy('id', 'DESC')->get();
        // return $this->model->select('restaurants.*',DB::raw("restaurant_menus.name AS menu_name"),'restaurant_menus.price')
        // ->Join('restaurant_menus','restaurants'.'.id','restaurant_menus'.'.restaurant_id')
        // ->Join('masters','restaurant_menus'.'.name','=','masters'.'.name')
        // ->where('masters'.'.id',$master_id)
        // ->whereNull('restaurant_menus'.'.deleted_at')
        // ->distinct()->get();

    }

    public function getMasterMenu($restIds, $name, $cuisine)
    {
        $masters = $this->model->select('masters' . '.*')
            ->Join('restaurant_menus', 'masters' . '.name', '=', 'restaurant_menus' . '.name');

        if (!empty($name)) {
            $masters->where('masters' . '.name', 'like', '%' . $name . '%');
        }

        if (!empty($restIds)) {
            $masters->whereIn('restaurant_menus' . '.restaurant_id', $restIds);
        }

        if (!empty($cuisine)) {
            $types = str_replace(",", "|", $cuisine);
            $masters->where('masters' . '.cuisine', 'rlike', $types);
        }
        $res = $masters->distinct()->paginate(15);
        return $res;
    }
    public function getByLatLng($lat, $lng, $chain_list = null, $dist = 10)
    {

        if ($chain_list == 'Local') {
            return DB::select(
                'SELECT id FROM
                            (SELECT id, (' . 6367 . ' * acos(cos(radians(' . $lat . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $lng . ')) +
                            sin(radians(' . $lat . ')) * sin(radians(latitude)))) AS distance
                            FROM restaurants WHERE published in ("Published","No Sale") and chain_list = "No") AS distances WHERE distance < ' . 10 . '  ORDER BY distance;
                    ');
        } elseif ($chain_list == 'Chain') {
            return DB::select(
                'SELECT id FROM
                            (SELECT id, (' . 6367 . ' * acos(cos(radians(' . $lat . ')) * cos(radians(latitude)) * cos(radians(longitude) - radians(' . $lng . ')) +
                            sin(radians(' . $lat . ')) * sin(radians(latitude))))
                            AS distance FROM restaurants WHERE published in ("Published","No Sale") and chain_list = "Yes") AS distances
                        WHERE distance < ' . 10 . '  ORDER BY distance
                        ;
                    ');
        } elseif ($chain_list == null) {

            return DB::select(
                'SELECT id FROM
                            (SELECT id, (' . 6367 . ' 
                            * acos(cos(radians(' . $lat . ')) 
                            * cos(radians(latitude)) 
                            * cos(radians(longitude) - radians(' . $lng . ')) 
                            + sin(radians(' . $lat . ')) 
                            * sin(radians(latitude))))
                            AS distance
                            FROM restaurants WHERE published in ("Published","No Sale")) AS distance
                        WHERE  distance <= '.$dist.'
                        ORDER BY distance
                        ;
                    ');
        }

        //   return DB::select(
        //               'SELECT id FROM
        //                     (SELECT id, 111.111 *  DEGREES(ACOS(LEAST(1.0, COS(RADIANS(' . $lat . ')) * COS(RADIANS(latitude))  * COS(RADIANS(' . $lng . ' - longitude))+ SIN(RADIANS(' . $lat . '))* SIN(RADIANS(latitude))))) AS distance FROM restaurants) AS distances
        //                 WHERE distance < ' . 10 . '
        //                 ORDER BY distance
        //                 ;
        //             ');
    }

    public function restaurantMasterList($search)
    {
        $this->checkLatLng();
        $latitude = Session::get('latitude', '32.768799');
        $longitude = Session::get('longitude', '-97.309341');

        $masters = Session::get('selected_masters[]');
        $masterSql = 1;
        $masterSql = [];
        $masterReg = '';
        if(!empty($masters)){
            if (is_array($masters)) {
                foreach ($masters as $master) {
                   // $masterSql .= ' AND menus_masters LIKE \'%' . $master . '%\'';
                    $masterSql = array_merge($masterSql, [' menus_masters LIKE \'%' . $master . '%\'']);
                }
                $masterReg = implode("|", $masters);
            }
        }
        else{
            $masterReg = ' ';
        }
        if (!empty($search['sort_filter'])) {
            if ($search['sort_filter'] == 'distance') {
                $order_filter = 'distance';
                $order_filter_order = 'ASC';
            } elseif ($search['sort_filter'] == 'pricelh') {
                $order_filter = 'price';
                $order_filter_order = 'ASC';
            } elseif ($search['sort_filter'] == 'pricehl') {
                $order_filter = 'price';
                $order_filter_order = 'DESC';
            } elseif ($search['sort_filter'] == 'rating') {
                $order_filter = 'rating';
                $order_filter_order = 'DESC';
            } elseif ($search['sort_filter'] == 'name') {
                $order_filter = 'name';
                $order_filter_order = 'ASC';
            }
        } else {
            $order_filter = 'distance';
            $order_filter_order = 'ASC';
        }
        $sqlDist = '(
                6367
                * acos(
                    cos(radians(' . $latitude . '))
                    * cos(radians(latitude))
                    * cos(radians(longitude) - radians(' . $longitude . '))
                    + sin(radians(' . $latitude . '))
                    * sin(radians(latitude))
                )
            )';
            if (!empty($search['maxDistance'])) {      
                //$search['maxDistance'] = substr($search['maxDistance'], 0, -2);
                $search['maxDistance'] = $search['maxDistance'];
            }else{
                $search['maxDistance'] = 10;
            }
            $restaurants = $this->model
            ->select(['restaurants.*',
                DB::raw($sqlDist . ' as distance, price'),

            ])
            ->join(DB::raw(" (SELECT restaurant_id, min(price) as price
            FROM `restaurant_menus`
            WHERE  `master_search` REGEXP '{$masterReg}'
            GROUP BY restaurant_id) AS RM"), "restaurants.id", 'RM.restaurant_id')
           ->where(function ($query) use ($masterSql){
                foreach($masterSql as $sql){
               $query = $query->orWhereRaw($sql);

                }
           })
           // ->whereRaw($masterSql)
            //->whereRaw($sqlDist . ' < ' . '25')
            //->whereRaw($sqlDist. '>=' . $search['minDistance'])
            ->whereRaw($sqlDist . '<=' .  $search['maxDistance'])
            ->orderBy('published')
            ->orderBy($order_filter, $order_filter_order);

        if (!empty($search['time_type'])) {

            if ($search['time_type'] == 'scheduled') {
                $search['working_date'] = date('Y-m-d', strtotime($search['working_date']));
                $search['working_hours'] = date('H:i:s', strtotime($search['working_hours']));
                $date = new DateTime($search['working_date']);
                $time = new DateTime($search['working_hours']);
                $combined = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));

            } elseif ($search['time_type'] == 'asap') {
                $search['working_date'] = date('Y-m-d');
                $search['working_hours'] = date('H:i:s');
                $date = new DateTime($search['working_date']);
                $time = new DateTime($search['working_hours']);
                $combined = new DateTime($date->format('Y-m-d') . ' ' . $time->format('H:i:s'));
            }
        }
        if (!empty($search['working_hours']) && $search['time_type'] != 'all') {
                    $time_res = $this->pricemin($search, $restaurants);
                    $restaurants->whereIn('id', $time_res);
        }
        if (!empty($search['priceMob'])) {
            $search['price'] = $search['priceMob'];
        }


        if (!empty($search['price'])) {
            if ($search['price'] == 1) {
                $restaurants->where('price_range_min', '<=', 5)->where('price_range_max', '>=', 0);
            }
            if ($search['price'] == 5) {
                $restaurants->where('price_range_min', '<=', 12)->where('price_range_max', '>=', 5);
            }
            if ($search['price'] == 12) {
                $restaurants->where('price_range_min', '<=', 20)->where('price_range_max', '>=', 12);
            }
            if ($search['price'] == 20) {
                $restaurants->where('price_range_min', '>=', 20);
            }
        }
       
        if (!empty($search['type'])) {
            if ($search['type'] == 'pickup') {
                 $restaurants->whereIn('published', ['Published', 'No Sale'])->whereIn('delivery', ['Yes', 'No']);
            } elseif ($search['type'] == 'orderpickup') {
                $restaurants->where('published', '=', 'Published')->whereIn('delivery', ['Yes', 'No']);
            } elseif ($search['type'] == 'orderdelivery') {
                $restaurants->where('published', '=', 'Published')->where('delivery', 'like', 'Yes');
            }
        }
        else{ 
            $restaurants->whereIn('published', ['Published', 'No Sale'])->whereIn('delivery', ['Yes', 'No']);
        }

        if (!empty($search['rating'])) {   
            $restaurants->where('rating', '>=', $search['rating']);
        }
       
        if (!empty($search['main_category'])) {
            if ($search['main_category'] != 'all' && $search['main_category'] == 5) {
                $category_name = Category::find($search['main_category'])->name;
                $restaurants->where(function ($query) use ($search) {
                    $query->where('catering', 'Yes');
                    $query->orWhere('category_name', $search['main_category']);
                });
            } elseif ($search['main_category'] != 'all') {
                $category_name = Category::find($search['main_category'])->name;
                $restaurants->where('category_name', $search['main_category']);
            } else {
                Session::forget('category_id');
            }
        }
        if (!empty($search['chain_list'])) {
            if ($search['chain_list'] != 'all') {
                $restaurants->where('chain_list', $search['chain_list']);
            }
        }
        else{
            $restaurants->where('chain_list', 'No');
        }
        return $restaurants->get();

    }

      public function updateRestaurantMasters()
    {
        $restaurants = $this->model
            ->where('menus_masters_status', '<=', date('Y-m-d H:i:s', strtotime('-2 days')))
            ->orWhereNull('menus_masters_status')
            ->take(5000)
            ->get();
        foreach ($restaurants as $restaurant) {
            if (!empty($restaurant->menu)) {
                $a = [];
                foreach ($restaurant->menu as $menu) {
                    if (!empty($menu->master_search)) {
                        foreach (explode(',', $menu->master_search) as $master) {
                            $a[]=$master;
                        }
                    } 

                }
                $a = array_unique($a);
                $master_data = implode(',', $a);
                // DB::select('UPDATE restaurants SET menus_masters="' . $master_data . '" WHERE id="' . $restaurant->id . '"');
                $this->model->where('id', $restaurant->id)->update(['menus_masters' => $master_data, 'menus_masters_status' => date('Y-m-d H:i:s')]);
                echo 'Updation of Restaurant \''. $restaurant->name .'\' Completed succesfully.';
                echo "<br>";
            }
        }
    }
    public function updateTimings()
    {
        $restaurants = $this->model
            ->whereNotIn('restaurants.id', function ($query) {
                $query->select('restaurant_timings.restaurant_id')->from('restaurant_timings');
            })
            ->where('timings_status', '<>', 'Yes')
            ->orWhereNull('timings_status')
            ->WhereNotNull('yelp_json')
            ->orderBy('restaurants.id')
            ->get();
        foreach ($restaurants as $restaurant) {
            $yelp_data = json_decode($restaurant->yelp_json);
            if (!empty($yelp_data->hours)) {
                $res = $this->getHours($yelp_data->hours[0]->open);
                foreach ($res as $key => $value) {
                    $restaurant_timings = Restauranttimings::create(['restaurant_id' => $restaurant->id, 'day' => $key, 'opening' => $value['start'], 'closing' => $value['end'], 'user_id' => 1, 'user_type' => 'App\User']);

                }
            }

            $this->model->where('id', $restaurant->id)->update(['timings_status' => 'Yes']);
        }
        return;

    }

    public function getHours($hours)
    {
        $ret = [];
        $days = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
        foreach ($hours as $key => $hour) {
            $ret[$days[$hour->day]]['start'] = @str_pad(intval($hour->start / 100), 2, '0', STR_PAD_LEFT) . ':' . str_pad($hour->start % 100, 2, '0');
            $ret[$days[$hour->day]]['end'] = @str_pad(intval($hour->end / 100), 2, '0', STR_PAD_LEFT) . ':' . str_pad($hour->end % 100, 2, '0');
        }
        return $ret;
    }

    private function checkLatLng()
    {
        if (!empty(Session::get('latitude'))) {
            return;
        } 
        
        // $default_loc = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $_SERVER['REMOTE_ADDR']));
        if (empty(Session::get('latitude'))) {
            // Session::put('latitude', $default_loc['geoplugin_latitude']);
            // Session::put('longitude', $default_loc['geoplugin_longitude']);
            Session::put('latitude', 32.7554883);
            Session::put('longitude', -97.3307658);
        }
        if (empty($default_loc['geoplugin_latitude'])) {
            Session::put('latitude', '32.768799');
            Session::put('longitude', '-97.309341');
        }

    }

    private function pricemin($search,$restaurants_ids)
    {  
        if(!empty($search['working_hours'])){ 
            $ids = [];
            if(empty($restaurants_ids)){
                $restaurants_ids = $this->pluck('id')->toArray();

            }
            else{
                $restaurants_ids = $restaurants_ids->pluck('id')->toArray();
            }
            foreach ($restaurants_ids as $restaurant) {       
                $flag=0;
                $timings = DB::table('restaurant_timings')->where('restaurant_id',$restaurant)->where('day',strtolower(substr(date('l',strtotime($search['working_date'])),0,3)))->get();
                foreach ($timings as $key => $value) { 
                    if(date('H:i',strtotime($value->opening))<=date('H:i',strtotime($search['working_hours'])) && date('H:i',strtotime($value->closing))>=date('H:i',strtotime($search['working_hours']))){ 
                         $flag = 1;
                     }
                }
                if($flag == 1){
                        $ids[] = $restaurant;
                     }
            }
         }
         return $ids;
    }
    public function getOnlyRestaurantMasterCount($restIds,$master)
    {
        $this->checkLatLng();
        $latitude = Session::get('latitude', '32.768799');
        $longitude = Session::get('longitude', '-97.309341');

        $masterSql = 1;
        $masterReg = '';
        if(!empty($master)){
                $masterSql = 'menus_masters LIKE \'%' . $master . '%\'';
                $masterReg = ' ';
        }
        else{
            $masterReg = ' ';
        }
        
        

        $restaurants = $this->model
            ->whereRaw($masterSql)
            ->whereIn('id',$restIds)->count();

       
        return $restaurants;

    }
    public function getRestReports($filter=0)
    {
        //DD($filter);
        $dow_diff=0;
        if(isset($filter['last_day'])){
            $date = date('Y-m-d',strtotime('yesterday'));
            $restaurant = Order::select('restaurants'.'.name',DB::raw('count(orders.id) as order_count'),DB::raw('count(orders.id) as average'),DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),
            DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
            ->selectRaw("count(case when orders.order_status = 'Cancelled' then 1 end) as cancelled")
            ->selectRaw("count(case when orders.order_status = 'Completed' then 1 end) as completed")

        ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
      //  ->whereBetween(DB::raw('DATE(orders.delivery_time)'), array(date('Y-m-d',strtotime($filter['filter_date1'])), date('Y-m-d',strtotime($filter['filter_date2']))))
        // ->whereDate('orders'.'.delivery_time','<=',date('Y-m-d'))
       // ->where('order_status', 'Completed')
       ->whereDate('delivery_time', '=', $date)
       ->groupBy('restaurants'.'.name')-> get()->toArray();   
            } else if(isset($filter['last_7_day'])){
                $start = date('Y-m-d', strtotime('-7 days'));
                $endate = date('Y-m-d');
               // dd(date('Y-m-d', strtotime('-7 days')));
                $date = date('Y-m-d',strtotime('yesterday'));
                $restaurant = Order::select('restaurants'.'.name',DB::raw('count(orders.id) as order_count'),DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),
                DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                ->selectRaw("count(case when orders.order_status = 'Cancelled' then 1 end) as cancelled")
                ->selectRaw("count(case when orders.order_status = 'Completed' then 1 end) as completed")
                ->selectRaw('count(orders.id)/7 as average')

            ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
            ->whereBetween('delivery_time', array($start, $endate))
           // ->where('order_status', 'Completed')
          // ->whereDate('delivery_time', '=', $date)
           ->groupBy('restaurants'.'.name')-> get()->toArray();   
            } else if(isset($filter['last_month'])){
               
                $start = date("Y-n-j", strtotime("first day of previous month"));
                $endate = date("Y-n-j", strtotime("last day of previous month"));
                $day = date('j', strtotime($endate));
                //$date = date('Y-m-d',strtotime('yesterday'));
                $restaurant = Order::select('restaurants'.'.name',DB::raw('count(orders.id) as order_count'),DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),
                DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                ->selectRaw("count(case when orders.order_status = 'Cancelled' then 1 end) as cancelled")
                ->selectRaw("count(case when orders.order_status = 'Completed' then 1 end) as completed")
                ->selectRaw('count(orders.id)/'.$day.' as average')

            ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
            ->whereBetween('delivery_time', array($start, $endate))
           // ->where('order_status', 'Completed')
          // ->whereDate('delivery_time', '=', $date)
           ->groupBy('restaurants'.'.name')-> get()->toArray();   
            } else if(isset($filter['last_6_avg'])){
               $start = date('Y-m-d');
               $second = date('Y-m-d',strtotime("-1 week"));
                $third = date('Y-m-d',strtotime("-2 week"));
                $fourth = date('Y-m-d',strtotime("-3 week"));
                $fifth = date('Y-m-d',strtotime("-4 week"));
                $sixth = date('Y-m-d',strtotime("-5 week"));
                $day=6;

                //$date = date('Y-m-d',strtotime('yesterday'));
                $restaurant = Order::select('restaurants'.'.name',DB::raw('count(orders.id) as order_count'),DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),
                DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                ->selectRaw("count(case when orders.order_status = 'Cancelled' then 1 end) as cancelled")
                ->selectRaw("count(case when orders.order_status = 'Completed' then 1 end) as completed")
                ->selectRaw('count(orders.id)/'.$day.' as average')

            ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
           // ->where('order_status', 'Completed')
           ->whereDate('delivery_time', '=', $start)
           ->orWhereDate('delivery_time', '=', $second)
           ->orWhereDate('delivery_time', '=', $third)
           ->orWhereDate('delivery_time', '=', $fourth)
           ->orWhereDate('delivery_time', '=', $fifth)
           ->orWhereDate('delivery_time', '=', $sixth)
           ->groupBy('restaurants'.'.name')-> get()->toArray();
         //  dd($restaurant);

            } else if(isset($filter['last_dow'])){
                $start = date('Y-m-d');
                $second = date('Y-m-d',strtotime("-1 week"));
                 $day=2;
 
                 //$date = date('Y-m-d',strtotime('yesterday'));
                 $restaurant = Order::select('restaurants'.'.name',DB::raw('count(orders.id) as order_count'),DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),
                 DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                 ->selectRaw("(count(case when orders.delivery_time like '%".$second."%' then 1 end))-(count(case when orders.delivery_time like '%".$start."%' then 1 end)) as diff")
                 ->selectRaw("count(case when orders.order_status = 'Cancelled' then 1 end) as cancelled")
                 ->selectRaw("count(case when orders.order_status = 'Completed' then 1 end) as completed")
                 ->selectRaw('count(orders.id)/'.$day.' as average')

             ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
            // ->where('order_status', 'Completed')
            ->whereDate('delivery_time', '=', $start)
            ->orWhereDate('delivery_time', '=', $second)
            ->groupBy('restaurants'.'.name')-> get()->toArray();
            //dd($restaurant);
          //  $todayorder = Order::select(DB::raw('count(orders.id) as order_count'))->whereDate('delivery_time', '=', $start)->pluck('order_count')->first();
           // $lastweek =Order::select(DB::raw('count(orders.id) as order_count'))->whereDate('delivery_time', '=', $second)->pluck('order_count')->first();
           // $dow_diff = $lastweek-$todayorder; 
             } else if(isset($filter['published'])){
                
                 //$date = date('Y-m-d',strtotime('yesterday'));
                 $restaurant = Restaurant::where('published','published')->get()->toArray();

                 $result = [];
                 if ($restaurant) {
                     foreach ($restaurant as $key => $value) {
                         //dd($value);
                         $result[$key]['Name'] =  @$value['name'];
                         $result[$key]['Email'] = @$value['email'];
                         $result[$key]['phone'] = @$value['phone'];          
          
                     }
                 }
                 return $result; 
          
             } else if(isset($filter['month_to_date'])){
               // dd(date('j'));
                $start = date('Y-m-01');
                $endate = date('Y-m-d');
                $day = date('j');
               // dd(date('Y-m-d', strtotime('-7 days')));
                $date = date('Y-m-d',strtotime('yesterday'));
                $restaurant = Order::select('restaurants'.'.name',DB::raw('count(orders.id) as order_count'),DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),
                DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
                ->selectRaw("count(case when orders.order_status = 'Cancelled' then 1 end) as cancelled")
                ->selectRaw("count(case when orders.order_status = 'Completed' then 1 end) as completed")
                ->selectRaw('count(orders.id)/'.$day.' as average')
                ->selectRaw('(count(orders.id)/'.$day.')*30 as current_month_forecast ')


            ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
            ->whereBetween('delivery_time', array($start, $endate))
           // ->where('order_status', 'Completed')
          // ->whereDate('delivery_time', '=', $date)
           ->groupBy('restaurants'.'.name')-> get()->toArray(); 
           $result = [];
       if ($restaurant) {
           foreach ($restaurant as $key => $value) {
               //dd($value);
               $result[$key]['Name'] =  @$value['name'];
               $result[$key]['Count'] = @$value['order_count'];
               $result[$key]['Total'] = @$value['Total'];
               $result[$key]['Tax'] = @$value['order_tax'];
               $result[$key]['Delivery'] = @$value['order_delivery'];
               $result[$key]['Tip'] = @$value['order_tip'];
               $result[$key]['Eatery Amount'] = @$value['EateryAmount'];
               $result[$key]['Cancelled Count'] = @$value['cancelled'];
               $result[$key]['Completed Count'] = @$value['completed'];
               $result[$key]['Average'] = @$value['average'];
               if($dow_diff){
                $result[$key]['Current Mth Forecast'] = @$value['current_month_forecast'];

               }



           }
       }
       return $result;  

            } else{
            $restaurant = Order::select('restaurants'.'.name',DB::raw('count(orders.id) as order_count'),DB::raw('count(orders.id) as average'),DB::raw('sum(total) as Total'),DB::raw('sum(tax-orders.ZC) as order_tax'),DB::raw('sum(orders.delivery_charge) as order_delivery'),DB::raw('sum(orders.CCR/100 * orders.total) as order_CCR'),DB::raw('sum(orders.CCF) as order_CCF'),DB::raw('sum(orders.ZR) as order_ZR'),DB::raw('sum(orders.ZF) as order_ZF'),DB::raw('sum(orders.ZC) as order_ZC'),DB::raw('sum(orders.tip) as order_tip'),
            DB::raw('sum((total - (IFNULL(orders.CCR,0)/100 * total) - IFNULL(orders.CCF,0) - (IFNULL(orders.ZR,0) *subtotal) - IFNULL(orders.ZF,0) -IFNULL(orders.ZC,0))) as EateryAmount'))
            ->selectRaw("count(case when orders.order_status = 'Cancelled' then 1 end) as cancelled")
            ->selectRaw("count(case when orders.order_status = 'Completed' then 1 end) as completed")

        ->join('restaurants','orders'.'.restaurant_id','restaurants'.'.id')
      //  ->whereBetween(DB::raw('DATE(orders.delivery_time)'), array(date('Y-m-d',strtotime($filter['filter_date1'])), date('Y-m-d',strtotime($filter['filter_date2']))))
        // ->whereDate('orders'.'.delivery_time','<=',date('Y-m-d'))
       // ->where('order_status', 'Completed')
       ->whereDate('delivery_time', '=', date('Y-m-d'))
       ->groupBy('restaurants'.'.name')-> get()->toArray();          
            

    }
       // dd($restaurant);
       $result = [];
       if ($restaurant) {
           foreach ($restaurant as $key => $value) {
               //dd($value);
               $result[$key]['Name'] =  @$value['name'];
               $result[$key]['Count'] = @$value['order_count'];
               $result[$key]['Total'] = @$value['Total'];
               $result[$key]['Tax'] = @$value['order_tax'];
               $result[$key]['Delivery'] = @$value['order_delivery'];
               $result[$key]['Tip'] = @$value['order_tip'];
               $result[$key]['Eatery Amount'] = @$value['EateryAmount'];
               $result[$key]['Cancelled Count'] = @$value['cancelled'];
               $result[$key]['Completed Count'] = @$value['completed'];
               $result[$key]['Average'] = @$value['average'];
               if(isset($value['diff'])){
                $result[$key]['Difference'] = @$value['diff'];

               }

           }
       }
  //dd($result);
       return $result;
                           
    } 
    
}
