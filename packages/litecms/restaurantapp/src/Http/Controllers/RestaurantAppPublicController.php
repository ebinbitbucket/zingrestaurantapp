<?php

namespace Litecms\Restaurantapp\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Litecms\Restaurantapp\Interfaces\RestaurantAppRepositoryInterface;
use Litecms\Restaurantapp\Interfaces\RestaurantRepositoryInterface;
use Litecms\Restaurantapp\Models\Restaurant;
use Litecms\Restaurantapp\Models\Restaurantapp;

use Litecms\Restaurantapp\Interfaces\OrderRepositoryInterface;
use Litecms\Restaurantapp\Models\Order;
use Litecms\Restaurantapp\Models\Favourite;
use Auth;
use Form;
use Carbon\Carbon;
use Mail;
use Session;
use Illuminate\Http\Request;
use App\Client;


class RestaurantAppPublicController extends BaseController
{
    // use RestaurantAppWorkflow;

    /**
     * Constructor.
     *
     * @param type \Litecms\RestaurantApp\Interfaces\RestaurantAppRepositoryInterface $restaurant_app
     *
     * @return type
     */
    public function __construct(RestaurantAppRepositoryInterface $restaurant_app, RestaurantRepositoryInterface $restaurant, OrderRepositoryInterface $order)
    {
        $this->repository = $restaurant_app;
        $this->restaurant = $restaurant;
        $this->order = $order;


        parent::__construct();
    }

    /**
     * Show restaurant_app's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $restaurant_apps = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('restaurantapp::restaurant_app.names'))
            ->view('restaurantapp::public.restaurant_app.index')
            ->data(compact('restaurant_apps'))
            ->output();
    }


    /**
     * Show restaurant_app.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $restaurant_app = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($restaurant_app->name . trans('restaurantapp::restaurant_app.name'))
            ->view('restaurantapp::public.restaurant_app.show')
            ->data(compact('restaurant_app'))
            ->output();
    }

    public function restaurantAppLogin(Request $request, $slug)
    {
        $fcm_token ='';
        if(isset($request->token)){
            $fcm_token =$request->token;
        }

        if (Session::has('login_client')) {
            return redirect('client/app/home/'.$slug);

        }
        $restaurant = $this->restaurant->findByField('id', $slug)->first();

        $restaurant['email'] = '';
        $restaurant['name'] = '';
        return $this->response
        ->setMetaTitle('Restaurant App - Login')
        ->layout('default')
        ->view('restaurantapp::default.restaurant_app.login')
        ->data(compact('restaurant','fcm_token'))
        ->output();

        // return $this->response->setMetaTitle('Login Restaurant')
        //     ->view('app.login')
        //     ->theme('app')
        //     ->layout('default')
        //     ->data(compact('restaurant'))
        //     ->output();

    }

    public function restaurantAppRegister(Request $request, $slug)
    {
        $fcm_token ='';
        if(isset($request->token)){
            $fcm_token =$request->token;
        }
        if (Session::has('login_client')) {
            // do some thing if the key is exist
            return redirect('client/app/home/'.$slug);

        }
        $restaurant = $this->restaurant->findByField('id', $slug)->first();

        $restaurant['name'] = '';
        $restaurant['email'] = '';

        return $this->response->setMetaTitle('Register Restaurant')
        ->view('restaurantapp::default.restaurant_app.register')
        ->layout('default')
        ->data(compact('restaurant','fcm_token'))
        ->output();
    }
    public function passwordReset($slug)
    {
        if (Session::has('login_client')) {
            // do some thing if the key is exist
            return redirect('client/app/home/'.$slug);

        }
        $restaurant = $this->restaurant->findByField('id', $slug)->first();

        $restaurant['name'] = '';
        $restaurant['email'] = '';

        return $this->response->setMetaTitle('Reset Password')
        ->view('restaurantapp::default.restaurant_app.reset_password')
        ->layout('default')
        ->data(compact('restaurant'))
        ->output();
    }
    public function postPassword(Request $request){
        $this->validate($request, [
            'password'     => 'required|confirmed|min:6',
        ]);
        if($request->client_id != null){
            $user = Client::where('id',$request->client_id)->first();
            $password = $request->get('password');
            $user->password = bcrypt($password);
            if ($user->save()) {
                return redirect(url('app/'.$request->restaurant_id));
            }
            else{
                return redirect()->back();
            }
        }
        return redirect()->back();
    }
    public function showResetForm(Request $request,$token,$restaurant_id,$client_id){
        
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($restaurant_id) {
            return $query->where('id', $restaurant_id)
                ->select('logo', 'id');
        })->first(['*']);
        return $this->response->setMetaTitle('Reset Password')
        ->view('restaurantapp::default.restaurant_app.reset')
        ->layout('default')
        ->data(compact('restaurant','client_id','token'))
        ->output();
    }
    protected function myOrders(Request $request, $status, $slug)
    {

        // dd($request->all());
        // if(isset($request->client_id)){
        //     $user = Client::find(hashids_decode($request->client_id));
        //     Auth::login($user);
        // }
        $restaurant_fav='';
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);

        $restaurant->theme_design = 'Default';

        // $orders = $this->order->scopeQuery(function ($query) use ($status) {
        //     return $query->where('user_id', user_id())
        //         ->select('id');
        // })->first(['*']);

        $users = Order::leftJoin('favourites','orders.id','=','favourites.favourite_id')
   ->selectRaw('profileAddress.*,   MIN(3959 * acos( cos( radians( '. $lat.') ) * cos( radians( profileAddress.lat ) ) * cos( radians( profileAddress.lng ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) *  sin( radians(lat) ) ) ) as closest')
    ->groupBy('users.id')
    ->having('closest', '<', 20)
    ->orderBy('closest')
    ->get();

        dd($orders);

        $orders = $this->order->filterOrders($status);

        
        // $client_address= Clientaddress::where('client_id',user_id())->get();
        // $myorders= Order::where('user_id', user_id())->get();
        if ($status == 'currorders') {
            $head = 'Current Orders Orders';
        } elseif ($status == 'pastorders') {
            $head = 'Past Orders';
        } elseif ($status == 'favourites') {
            $head = 'Favorite Orders';
        }

        return $this->response->setMetaTitle($head)
        ->view('restaurantapp::default.restaurant_app.orders')
        ->layout('default')
        ->data(compact('restaurant', 'orders','restaurant_fav'))
        ->output();
    }


}
