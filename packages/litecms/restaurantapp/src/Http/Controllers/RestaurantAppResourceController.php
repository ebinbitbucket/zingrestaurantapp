<?php

namespace Litecms\Restaurantapp\Http\Controllers;
use Illuminate\Support\Facades\Facade;

use App\Http\Controllers\ResourceController as BaseController;
use Litecms\Restaurantapp\Http\Requests\RestaurantAppRequest;
use Litecms\Restaurantapp\Interfaces\RestaurantAppRepositoryInterface;
use Litecms\Restaurantapp\Interfaces\RestaurantRepositoryInterface;
use Litecms\Restaurantapp\Interfaces\OrderRepositoryInterface;

use Litecms\Restaurantapp\Models\RestaurantApp;
use Litecms\Restaurantapp\Models\Order;
use Litecms\Restaurantapp\Models\Restaurant;
use Litecms\Restaurantapp\Models\Clientaddress;
use Litecms\Restaurantapp\Models\Favourite;
use Litecms\Restaurantapp\Models\Cart;
use Auth;
use Form;
use App\Client;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Session;
/**
 * Resource controller class for restaurant_app.
 */
class RestaurantAppResourceController extends BaseController
{

    /**
     * Initialize restaurant_app resource controller.
     *
     * @param type RestaurantAppRepositoryInterface $restaurant_app
     *
     * @return null
     */
    public function __construct(RestaurantAppRepositoryInterface $restaurant_app, RestaurantRepositoryInterface $restaurant, OrderRepositoryInterface $order)
    {
        parent::__construct();
        $this->repository = $restaurant_app;
        $this->restaurant = $restaurant;
        $this->order = $order;

        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Litecms\Restaurantapp\Repositories\Criteria\RestaurantAppResourceCriteria::class);
    }

    /**
     * Display a list of restaurant_app.
     *
     * @return Response
     */
    public function index(RestaurantAppRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Litecms\Restaurantapp\Repositories\Presenter\RestaurantAppPresenter::class)
                ->$function();
        }

        $restaurant_apps = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('restaurantapp::restaurant_app.names'))
            ->view('restaurantapp::restaurant_app.index', true)
            ->data(compact('restaurant_apps', 'view'))
            ->output();
    }

    /**
     * Display restaurant_app.
     *
     * @param Request $request
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function show(RestaurantAppRequest $request, RestaurantApp $restaurant_app)
    {

        if ($restaurant_app->exists) {
            $view = 'restaurantapp::restaurant_app.show';
        } else {
            $view = 'restaurantapp::restaurant_app.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('restaurantapp::restaurant_app.name'))
            ->data(compact('restaurant_app'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new restaurant_app.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(RestaurantAppRequest $request)
    {

        $restaurant_app = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('restaurantapp::restaurant_app.name')) 
            ->view('restaurantapp::restaurant_app.create', true) 
            ->data(compact('restaurant_app'))
            ->output();
    }

    /**
     * Create new restaurant_app.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(RestaurantAppRequest $request)
    {
        try {
            $attributes              = $request->all();
            $attributes['user_id']   = user_id();
            $attributes['user_type'] = user_type();
            $restaurant_app                 = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('restaurantapp::restaurant_app.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurantapp/restaurant_app/' . $restaurant_app->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/restaurantapp/restaurant_app'))
                ->redirect();
        }

    }

    /**
     * Show restaurant_app for editing.
     *
     * @param Request $request
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function edit(RestaurantAppRequest $request, RestaurantApp $restaurant_app)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('restaurantapp::restaurant_app.name'))
            ->view('restaurantapp::restaurant_app.edit', true)
            ->data(compact('restaurant_app'))
            ->output();
    }

    /**
     * Update the restaurant_app.
     *
     * @param Request $request
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function update(RestaurantAppRequest $request, RestaurantApp $restaurant_app)
    {
        try {
            $attributes = $request->all();

            $restaurant_app->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('restaurantapp::restaurant_app.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('restaurantapp/restaurant_app/' . $restaurant_app->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurantapp/restaurant_app/' . $restaurant_app->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the restaurant_app.
     *
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function destroy(RestaurantAppRequest $request, RestaurantApp $restaurant_app)
    {
        try {

            $restaurant_app->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurantapp::restaurant_app.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('restaurantapp/restaurant_app/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('restaurantapp/restaurant_app/' . $restaurant_app->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple restaurant_app.
     *
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function delete(RestaurantAppRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('restaurantapp::restaurant_app.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('restaurantapp/restaurant_app'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurantapp/restaurant_app'))
                ->redirect();
        }

    }

    /**
     * Restore deleted restaurant_apps.
     *
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function restore(RestaurantAppRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('restaurantapp::restaurant_app.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/restaurantapp/restaurant_app'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/restaurantapp/restaurant_app/'))
                ->redirect();
        }

    }

    protected function restaurantAppHome(Request $request, $slug)
    {
        if(isset($request->client_id)){
            // dd($request->client_id);
            if(Session::get('login_client')){
               Session::forget('login_client');
            }
           Auth::guard('client.web')->loginUsingId(hashids_decode($request->client_id));
       }

        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->orderBy('id', 'DESC')
                ->where('id', $slug)
                ->select('logo', 'id','name')
                ->with(['menu']);
        })->first(['*']);


        $restaurant_menu = @$restaurant->menu->unique('category_id');

        $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
        $restaurant->theme_design = 'Default';
        return $this->response
        ->view('restaurantapp::default.restaurant_app.home')
        ->layout('default')
        ->data(compact('restaurant', 'app', 'restaurant_menu'))
        ->output();

    }
    protected function menu($slug)
    {
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->orderBy('id', 'DESC')
                ->where('id', $slug)
                ->select('logo', 'id','is_subcategory')
                ->with([
                    'category' => function ($q) {
                        $q->orderBy('order_no', 'ASC');
                    },
                    'menu',
                ]);
        })->first(['*']);

        $restaurant_menu = $restaurant->menu->groupBy('category_id');
        $restaurant_category = $restaurant->category;
        $restaurant->theme_design = 'Default';
        $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();

        
        if(@$restaurant->is_subcategory==1){
            return $this->response
            ->view('restaurantapp::default.restaurant_app.menu_sub')
            ->layout('default')
            ->data(compact('restaurant', 'restaurant_menu','app','restaurant_category'))
            ->output();
          }

        return $this->response
        ->view('restaurantapp::default.restaurant_app.menu')
        ->layout('default')
        ->data(compact('restaurant', 'restaurant_menu','app','restaurant_category'))
        ->output();
    }

    protected function gallery($slug)
    {
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);

        $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();
        $restaurant->theme_design = 'Default';

        return $this->response
        ->view('restaurantapp::default.restaurant_app.gallery')
        ->layout('default')
        ->data(compact('restaurant', 'app'))
        ->output();
    }

    protected function contact($slug)
    {

        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);

        $weekMap = [
            'mon' => 'Monday',
            'tue' => 'Tuesday',
            'wed' => 'Wednesday',
            'thu' => 'Thursday',
            'fri' => 'Friday',
            'sat' => 'Saturday',
            'sun' => 'Sunday',
        ];
        $restaurant_timings = $restaurant->timings->groupBy('day');
        $restaurant->theme_design = 'Default';

        return $this->response
        ->view('restaurantapp::default.restaurant_app.contact')
        ->layout('default')
        ->data(compact('restaurant', 'weekMap', 'restaurant_timings'))
        ->output();
    }

    protected function locations($slug)
    {
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);

        $app = Restaurantapp::where('restaurant_id', $restaurant->id)->first();

        $restaurant->theme_design = 'Default';

        return $this->response
        ->view('restaurantapp::default.restaurant_app.locations')
        ->layout('default')
        ->data(compact('restaurant', 'app'))
        ->output();
    }
    protected function myAccount($slug)
    {
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);

        $restaurant->theme_design = 'Default';

        return $this->response
        ->view('restaurantapp::default.restaurant_app.account')
        ->layout('default')
        ->data(compact('restaurant'))
        ->output();
    }

    protected function sendMail(Request $request, $slug)
    {
        $attributes = $request->all();
        // dd($attributes);
        $attributes['cf_subject'] = 'Contact Form';
        $weekday = [];
        $restaurant = $this->restaurant->findByField('id', $slug)->first();

        if ($attributes['send_mail'] != null && $attributes['cf_email'] != null) {
            Mail::send('restaurantapp::default.restaurant_app.contact_mail', ['restaurant' => $restaurant, 'attributes' => $attributes], function ($message) use ($restaurant, $attributes) {
                $message->from($attributes['cf_email']);
                // $message->to($restaurant->customer_email)->subject('Website Enquiry');
                $message->to($attributes['send_mail'])->subject('App Enquiry');

            });
        }
        return redirect()->back();

    }
    public function logoutClient(Request $request, $slug)
    {

        Session::forget('login_client');

        $request->session()->invalidate();

        return redirect(guard_url('app/home/'.$slug));

    }
    protected function myAddress($slug)
    {
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);

        $restaurant->theme_design = 'Default';

        $client_address = Clientaddress::where('client_id', user_id())->get();

        return $this->response
        ->view('restaurantapp::default.restaurant_app.address')
        ->layout('default')
        ->data(compact('restaurant', 'client_address'))
        ->output();
    }
    protected function myOrders(Request $request, $status, $slug)
    {
         if(isset($request->client_id)){
            if(Session::get('login_client')){
                   Session::forget('login_client');
                }
               Auth::guard('client.web')->loginUsingId(hashids_decode($request->client_id));
        }

        $restaurant_fav='';
        $head='';

        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);
        $restaurant->theme_design = 'Default';


        $orders = $this->order->filterOrders($status);

        // $client_address= Clientaddress::where('client_id',user_id())->get();
        // $myorders= Order::where('user_id', user_id())->get();
        if ($status == 'currorders') {
            $head = 'Current Orders Orders';
        } elseif ($status == 'pastorders') {
            $head = 'Past Orders';
        } elseif ($status == 'favourites') {
            $head = 'Favorite Orders';
        }

        return $this->response->setMetaTitle($head)
        ->view('restaurantapp::default.restaurant_app.orders')
        ->layout('default')
        ->data(compact('restaurant', 'orders','restaurant_fav','head'))
        ->output();
    }

    protected function editProfile($slug)
    {
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);

        // $restaurant->theme_design = 'Default';

        return $this->response
        ->view('restaurantapp::default.restaurant_app.profile')
        ->layout('default')
        ->data(compact('restaurant'))
        ->output();
    }
    protected function viewFavorites($slug)
    {
        $restaurant_fav='';

        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);


        $orders = $this->order->filterOrders('favourites');

        return $this->response->setMetaTitle('Favorite Orders')
        ->view('restaurantapp::default.restaurant_app.favorites')
        ->layout('default')
        ->data(compact('restaurant', 'orders','restaurant_fav'))
        ->output();
    }
    protected function viewPoints($slug)
    {
        $restaurant = $this->restaurant->scopeQuery(function ($query) use ($slug) {
            return $query->where('id', $slug)
                ->select('logo', 'id');
        })->first(['*']);

        $sum = $this->order->findByField('user_id',user_id())->sum('loyalty_points');
        $discount =  $this->order->findByField('user_id',user_id())->sum('discount_points');
        $points = $sum-$discount;


        $orders = $this->order->filterOrders('favourites');

        return $this->response->setMetaTitle('Favorite Orders')
        ->view('restaurantapp::default.restaurant_app.points')
        ->layout('default')
        ->data(compact('restaurant','points'))
        ->output();
    }
    protected function addToFavourite($id,$status){

        $favourite = Favourite::where('favourite_id',$id)->where('type','order')->first();

        if(isset($favourite->favourite_id)){
            $favourite->forceDelete();
        }else{
            Favourite::create(['user_id' => user_id(),'favourite_id' => $id,'type' => 'order']);
        }
        // if($status == 'on'){
        //     Favourite::create(['user_id' => user_id(),'favourite_id' => $id,'type' => 'order']);
        // }
        // else{
        //     $favourite = Favourite::where('favourite_id',$id)->where('type','order')->first();
        //     $favourite->forceDelete();
        // }
        return redirect()->back();
    }

    


}
