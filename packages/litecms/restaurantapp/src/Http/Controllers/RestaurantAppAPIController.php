<?php

namespace Litecms\Restaurantapp\Http\Controllers;

use App\Http\Controllers\APIController as BaseController;
use Litecms\Restaurantapp\Http\Requests\RestaurantAppRequest;
use Litecms\Restaurantapp\Interfaces\RestaurantAppRepositoryInterface;
use Litecms\Restaurantapp\Models\RestaurantApp;
use Litecms\Restaurantapp\Forms\RestaurantApp as Form;

/**
 * APIController  class for restaurant_app.
 */
class RestaurantAppAPIController extends BaseController
{

    /**
     * Initialize restaurant_app resource controller.
     *
     * @param type RestaurantAppRepositoryInterface $restaurant_app
     *
     * @return null
     */
    public function __construct(RestaurantAppRepositoryInterface $restaurant_app)
    {
        parent::__construct();
        $this->repository = $restaurant_app;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Litecms\Restaurantapp\Repositories\Criteria\RestaurantAppResourceCriteria::class);
    }

    /**
     * Display a list of restaurant_app.
     *
     * @return Response
     */
    public function index(RestaurantAppRequest $request)
    {
        return $this->repository
            ->setPresenter(\Litecms\Restaurantapp\Repositories\Presenter\RestaurantAppPresenter::class)
            ->paginate();
    }

    /**
     * Display restaurant_app.
     *
     * @param Request $request
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function show(RestaurantAppRequest $request, RestaurantApp $restaurant_app)
    {
        return $restaurant_app->setPresenter(\Litecms\Restaurantapp\Repositories\Presenter\RestaurantAppListPresenter::class);
        ;
    }

    /**
     * Create new restaurant_app.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(RestaurantAppRequest $request)
    {
        try {
            $data              = $request->all();
            $data['user_id']   = user_id();
            $data['user_type'] = user_type();
            $data              = $this->repository->create($data);
            $message           = trans('messages.success.created', ['Module' => trans('restaurantapp::restaurant_app.name')]);
            $code              = 204;
            $status            = 'success';
            $url               = guard_url('restaurantapp/restaurant_app/' . $restaurant_app->getRouteKey());
        } catch (Exception $e) {
            $message = $e->getMessage();
            $code    = 400;
            $status  = 'error';
            $url     = guard_url('restaurantapp/restaurant_app');
        }
        return compact('data', 'message', 'code', 'status', 'url');
    }

    /**
     * Update the restaurant_app.
     *
     * @param Request $request
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function update(RestaurantAppRequest $request, RestaurantApp $restaurant_app)
    {
        try {
            $data = $request->all();

            $restaurant_app->update($data);
            $message = trans('messages.success.updated', ['Module' => trans('restaurantapp::restaurant_app.name')]);
            $code    = 204;
            $status  = 'success';
            $url     = guard_url('restaurantapp/restaurant_app/' . $restaurant_app->getRouteKey());
        } catch (Exception $e) {
            $message = $e->getMessage();
            $code    = 400;
            $status  = 'error';
            $url     = guard_url('restaurantapp/restaurant_app/' . $restaurant_app->getRouteKey());
        }
        return compact('data', 'message', 'code', 'status', 'url');
    }

    /**
     * Remove the restaurant_app.
     *
     * @param Model   $restaurant_app
     *
     * @return Response
     */
    public function destroy(RestaurantAppRequest $request, RestaurantApp $restaurant_app)
    {
        try {
            $restaurant_app->delete();
            $message = trans('messages.success.deleted', ['Module' => trans('restaurantapp::restaurant_app.name')]);
            $code    = 202;
            $status  = 'success';
            $url     = guard_url('restaurantapp/restaurant_app/0');
        } catch (Exception $e) {
            $message = $e->getMessage();
            $code    = 400;
            $status  = 'error';
            $url     = guard_url('restaurantapp/restaurant_app/' . $restaurant_app->getRouteKey());
        }
        return compact('message', 'code', 'status', 'url');
    }

    /**
     * Return the form elements as json.
     *
     * @param String   $element
     *
     * @return json
     */
    public function form($element = 'fields')
    {
        $form = new Form();
        return $form->form($element, true);
    }

}
