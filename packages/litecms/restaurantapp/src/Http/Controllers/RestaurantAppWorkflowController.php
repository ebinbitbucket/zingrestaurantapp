<?php

namespace Litecms\Restaurantapp\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Litecms\Restaurantapp\Http\Requests\RestaurantAppRequest;
use Litecms\Restaurantapp\Models\RestaurantApp;

/**
 * Admin web controller class.
 */
class RestaurantAppWorkflowController extends BaseController
{

    /**
     * Workflow controller function for restaurant_app.
     *
     * @param Model   $restaurant_app
     * @param step    next step for the workflow.
     *
     * @return Response
     */

    public function putWorkflow(RestaurantAppRequest $request, RestaurantApp $restaurant_app, $step)
    {

        try {

            $restaurant_app->updateWorkflow($step);

            return response()->json([
                'message'  => trans('messages.success.changed', ['Module' => trans('restaurantapp::restaurant_app.name'), 'status' => trans("app.{$step}")]),
                'code'     => 204,
                'redirect' => trans_url('/admin/restaurant_app/restaurant_app/' . $restaurant_app->getRouteKey()),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/restaurant_app/restaurant_app/' . $restaurant_app->getRouteKey()),
            ], 400);

        }

    }

    /**
     * Workflow controller function for restaurant_app.
     *
     * @param Model   $restaurant_app
     * @param step    next step for the workflow.
     * @param user    encrypted user id.
     *
     * @return Response
     */

    public function getWorkflow(RestaurantApp $restaurant_app, $step, $user)
    {
        try {
            $user_id = decrypt($user);

            Auth::onceUsingId($user_id);

            $restaurant_app->updateWorkflow($step);

            $data = [
                'message' => trans('messages.success.changed', ['Module' => trans('restaurantapp::restaurant_app.name'), 'status' => trans("app.{$step}")]),
                'status'  => 'success',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('restaurantapp::admin.restaurant_app.message', $data)->render();

        } catch (ValidationException $e) {

            $data = [
                'message' => '<b>' . $e->getMessage() . '</b> <br /><br />' . implode('<br />', $e->validator->errors()->all()),
                'status'  => 'error',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('restaurantapp::admin.restaurant_app.message', $data)->render();

        } catch (Exception $e) {

            $data = [
                'message' => '<b>' . $e->getMessage() . '</b>',
                'status'  => 'error',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('restaurantapp::admin.restaurant_app.message', $data)->render();

        }

    }
}
