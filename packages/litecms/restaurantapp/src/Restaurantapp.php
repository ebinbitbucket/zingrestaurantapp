<?php

namespace Litecms\Restaurantapp;
use Litecms\Restaurantapp\Models\Favourite;
use Litecms\Restaurantapp\Models\Restaurant;

use User;

class Restaurantapp
{
    /**
     * $restaurant_app object.
     */
    protected $restaurant_app;

    /**
     * Constructor.
     */
    public function __construct(\Litecms\Restaurantapp\Interfaces\RestaurantAppRepositoryInterface $restaurant_app)
    {
        $this->restaurant_app = $restaurant_app;
    }

    /**
     * Returns count of restaurantapp.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return  0;
    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.restaurant_app.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->restaurant_app->pushCriteria(new \Litepie\Litecms\Repositories\Criteria\RestaurantAppUserCriteria());
        }

        $restaurant_app = $this->restaurant_app->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('restaurantapp::' . $view, compact('restaurant_app'))->render();
    }

    public function getFavouriteStatus($order_id)
    {
        dd(Favourite::get());
        return Favourite::where('favourite_id',$order_id)->where('type','order')->where('user_id',user_id())->count();
    }
   

}
