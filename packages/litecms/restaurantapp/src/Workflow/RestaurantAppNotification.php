<?php

namespace Litecms\Restaurantapp\Workflow;

use Litecms\Restaurantapp\Models\RestaurantApp;
use Litecms\Restaurantapp\Notifications\RestaurantAppWorkflow as RestaurantAppNotifyer;
use Notification;

class RestaurantAppNotification
{

    /**
     * Send the notification to the users after complete.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return void
     */
    public function complete(RestaurantApp $restaurant_app)
    {
        return Notification::send($restaurant_app->user, new RestaurantAppNotifyer($restaurant_app, 'complete'));;
    }

    /**
     * Send the notification to the users after verify.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return void
     */
    public function verify(RestaurantApp $restaurant_app)
    {
        return Notification::send($restaurant_app->user, new RestaurantAppNotifyer($restaurant_app, 'verify'));;
    }

    /**
     * Send the notification to the users after approve.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return void
     */
    public function approve(RestaurantApp $restaurant_app)
    {
        return Notification::send($restaurant_app->user, new RestaurantAppNotifyer($restaurant_app, 'approve'));;

    }

    /**
     * Send the notification to the users after publish.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return void
     */
    public function publish(RestaurantApp $restaurant_app)
    {
        return Notification::send($restaurant_app->user, new RestaurantAppNotifyer($restaurant_app, 'publish'));;
    }

    /**
     * Send the notification to the users after archive.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return void
     */
    public function archive(RestaurantApp $restaurant_app)
    {
        return Notification::send($restaurant_app->user, new RestaurantAppNotifyer($restaurant_app, 'archive'));;

    }

    /**
     * Send the notification to the users after unpublish.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return void
     */
    public function unpublish(RestaurantApp $restaurant_app)
    {
        return Notification::send($restaurant_app->user, new RestaurantAppNotifyer($restaurant_app, 'unpublish'));;

    }
}
