<?php

namespace Litecms\Restaurantapp\Workflow;

use Litecms\Restaurantapp\Models\RestaurantApp;
use Validator;

class RestaurantAppValidator
{

    /**
     * Determine if the given restaurant_app is valid for complete status.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return bool / Validator
     */
    public function complete(RestaurantApp $restaurant_app)
    {
        return Validator::make($restaurant_app->toArray(), [
            'title' => 'required|min:15',
        ]);
    }

    /**
     * Determine if the given restaurant_app is valid for verify status.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return bool / Validator
     */
    public function verify(RestaurantApp $restaurant_app)
    {
        return Validator::make($restaurant_app->toArray(), [
            'title'  => 'required|min:15',
            'status' => 'in:complete',
        ]);
    }

    /**
     * Determine if the given restaurant_app is valid for approve status.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return bool / Validator
     */
    public function approve(RestaurantApp $restaurant_app)
    {
        return Validator::make($restaurant_app->toArray(), [
            'title'  => 'required|min:15',
            'status' => 'in:verify',
        ]);

    }

    /**
     * Determine if the given restaurant_app is valid for publish status.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return bool / Validator
     */
    public function publish(RestaurantApp $restaurant_app)
    {
        return Validator::make($restaurant_app->toArray(), [
            'title'       => 'required|min:15',
            'description' => 'required|min:50',
            'status'      => 'in:approve,archive,unpublish',
        ]);

    }

    /**
     * Determine if the given restaurant_app is valid for archive status.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return bool / Validator
     */
    public function archive(RestaurantApp $restaurant_app)
    {
        return Validator::make($restaurant_app->toArray(), [
            'title'       => 'required|min:15',
            'description' => 'required|min:50',
            'status'      => 'in:approve,publish,unpublish',
        ]);

    }

    /**
     * Determine if the given restaurant_app is valid for unpublish status.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return bool / Validator
     */
    public function unpublish(RestaurantApp $restaurant_app)
    {
        return Validator::make($restaurant_app->toArray(), [
            'title'       => 'required|min:15',
            'description' => 'required|min:50',
            'status'      => 'in:publish',
        ]);

    }
}
