<?php

namespace Litecms\Restaurantapp\Workflow;

use Exception;
use Litepie\Workflow\Exceptions\WorkflowActionNotPerformedException;

use Litecms\Restaurantapp\Models\RestaurantApp;

class RestaurantAppAction
{
    /**
     * Perform the complete action.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return RestaurantApp
     */
    public function complete(RestaurantApp $restaurant_app)
    {
        try {
            $restaurant_app->status = 'complete';
            return $restaurant_app->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the verify action.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return RestaurantApp
     */public function verify(RestaurantApp $restaurant_app)
    {
        try {
            $restaurant_app->status = 'verify';
            return $restaurant_app->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the approve action.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return RestaurantApp
     */public function approve(RestaurantApp $restaurant_app)
    {
        try {
            $restaurant_app->status = 'approve';
            return $restaurant_app->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the publish action.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return RestaurantApp
     */public function publish(RestaurantApp $restaurant_app)
    {
        try {
            $restaurant_app->status = 'publish';
            return $restaurant_app->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the archive action.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return RestaurantApp
     */
    public function archive(RestaurantApp $restaurant_app)
    {
        try {
            $restaurant_app->status = 'archive';
            return $restaurant_app->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the unpublish action.
     *
     * @param RestaurantApp $restaurant_app
     *
     * @return RestaurantApp
     */
    public function unpublish(RestaurantApp $restaurant_app)
    {
        try {
            $restaurant_app->status = 'unpublish';
            return $restaurant_app->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }
}
