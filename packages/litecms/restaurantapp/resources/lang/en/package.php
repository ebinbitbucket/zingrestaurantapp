<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Restaurantapp package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for restaurantapp module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Restaurantapp',
    'names'         => 'Restaurantapps',
];
