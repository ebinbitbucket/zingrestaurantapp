<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for restaurant_app in restaurantapp package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  restaurant_app module in restaurantapp package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'RestaurantApp',
    'names'         => 'RestaurantApps',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'RestaurantApps',
        'sub'   => 'RestaurantApps',
        'list'  => 'List of restaurant_apps',
        'edit'  => 'Edit restaurant_app',
        'create'    => 'Create new restaurant_app'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'restaurant_id'              => 'Please enter restaurant id',
        'point_percentage'           => 'Please enter point percentage',
        'points'                     => 'Please enter points',
        'title'                      => 'Please enter title',
        'subtitle'                   => 'Please enter subtitle',
        'meta_tags'                  => 'Please enter meta tags',
        'meta_keywords'              => 'Please enter meta keywords',
        'meta_description'           => 'Please enter meta description',
        'email'                      => 'Please enter email',
        'alternate_phone'            => 'Please enter alternate phone',
        'gallery'                    => 'Please enter gallery',
        'slider_images'              => 'Please enter slider images',
        'featured_food'              => 'Please enter featured food',
        'locations'                  => 'Please enter locations',
        'contact_info'               => 'Please enter contact info',
        'menus'                      => 'Please enter menus',
        'created_at'                 => 'Please select created at',
        ')'                          => 'Please select )',
        'updated_at'                 => 'Please select updated at',
        'deleted_at'                 => 'Please select deleted at',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'restaurant_id'              => 'Restaurant id',
        'point_percentage'           => 'Point percentage',
        'points'                     => 'Points',
        'title'                      => 'Title',
        'subtitle'                   => 'Subtitle',
        'meta_tags'                  => 'Meta tags',
        'meta_keywords'              => 'Meta keywords',
        'meta_description'           => 'Meta description',
        'email'                      => 'Email',
        'alternate_phone'            => 'Alternate phone',
        'gallery'                    => 'Gallery',
        'slider_images'              => 'Slider images',
        'featured_food'              => 'Featured food',
        'locations'                  => 'Locations',
        'contact_info'               => 'Contact info',
        'menus'                      => 'Menus',
        'created_at'                 => 'Created at',
        ')'                          => ')',
        'updated_at'                 => 'Updated at',
        'deleted_at'                 => 'Deleted at',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'id'                         => ['name' => 'Id', 'data-column' => 1, 'checked'],
        'restaurant_id'              => ['name' => 'Restaurant id', 'data-column' => 2, 'checked'],
        'point_percentage'           => ['name' => 'Point percentage', 'data-column' => 3, 'checked'],
        'points'                     => ['name' => 'Points', 'data-column' => 4, 'checked'],
        'title'                      => ['name' => 'Title', 'data-column' => 5, 'checked'],
        'subtitle'                   => ['name' => 'Subtitle', 'data-column' => 6, 'checked'],
        'meta_tags'                  => ['name' => 'Meta tags', 'data-column' => 7, 'checked'],
        'meta_keywords'              => ['name' => 'Meta keywords', 'data-column' => 8, 'checked'],
        'meta_description'           => ['name' => 'Meta description', 'data-column' => 9, 'checked'],
        'email'                      => ['name' => 'Email', 'data-column' => 10, 'checked'],
        'alternate_phone'            => ['name' => 'Alternate phone', 'data-column' => 11, 'checked'],
        'gallery'                    => ['name' => 'Gallery', 'data-column' => 12, 'checked'],
        'slider_images'              => ['name' => 'Slider images', 'data-column' => 13, 'checked'],
        'featured_food'              => ['name' => 'Featured food', 'data-column' => 14, 'checked'],
        'locations'                  => ['name' => 'Locations', 'data-column' => 15, 'checked'],
        'contact_info'               => ['name' => 'Contact info', 'data-column' => 16, 'checked'],
        'menus'                      => ['name' => 'Menus', 'data-column' => 17, 'checked'],
        ')'                          => ['name' => ')', 'data-column' => 18, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'name'  => 'RestaurantApps',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
