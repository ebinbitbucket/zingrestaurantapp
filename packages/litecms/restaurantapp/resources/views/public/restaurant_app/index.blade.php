            @include('restaurantapp::public.restaurant_app.partial.header')

            <section class="grid">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('restaurantapp::public.restaurant_app.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="main-area parent-border list-item">
                                @foreach($restaurant_apps as $restaurant_app)
                                <div class="item border">
                                    <div class="feature">
                                        <a href="{{trans_url('restaurant_apps')}}/{{@$restaurant_app['slug']}}">
                                            <img src="{{url($restaurant_app->defaultImage('images'))}}" class="img-responsive center-block" alt="">
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h4><a href="{{trans_url('restaurant_app')}}/{{$restaurant_app['slug']}}">{{str_limit($restaurant_app['title'], 300)}}</a> 
                                        </h4>
                                        <div class="metas mt20">
                                            <div class="tag pull-left">
                                                <a href="#" class="">Seo Tips</a>
                                            </div>
                                            <div class="date-time pull-right">
                                                <span><i class="fa fa-comments"></i>{{@$restaurant_app->viewcount}}</span>
                                                <span><i class="fa fa-calendar"></i>{{format_date($restaurant_app['posted_on'])}}</span>
                                            </div>
                                        </div>
                                        <div class="divider"></div>
                                        <div class="author">
                                            <div class="avatar pull-left">
                                                {{@$restaurant_app->user->badge}}
                                            </div>
                                            <div class="actions">
                                                

                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                @endforeach
                            </div>
                            <div class="pagination text-center">
                            {{ $restaurant_apps->links() }}
                            </div>
                        </div>
                    </div>
                </div>
            </section> 