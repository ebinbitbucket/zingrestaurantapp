            @include('restaurantapp::restaurant_app.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('restaurantapp::restaurant_app.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($restaurant_app->defaultImage('images' , 'xl'))!!}" alt="{{$restaurant_app->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('restaurantapp::restaurant_app.label.id') !!}
                </label><br />
                    {!! $restaurant_app['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="restaurant_id">
                    {!! trans('restaurantapp::restaurant_app.label.restaurant_id') !!}
                </label><br />
                    {!! $restaurant_app['restaurant_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="point_percentage">
                    {!! trans('restaurantapp::restaurant_app.label.point_percentage') !!}
                </label><br />
                    {!! $restaurant_app['point_percentage'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="points">
                    {!! trans('restaurantapp::restaurant_app.label.points') !!}
                </label><br />
                    {!! $restaurant_app['points'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="title">
                    {!! trans('restaurantapp::restaurant_app.label.title') !!}
                </label><br />
                    {!! $restaurant_app['title'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="subtitle">
                    {!! trans('restaurantapp::restaurant_app.label.subtitle') !!}
                </label><br />
                    {!! $restaurant_app['subtitle'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="meta_tags">
                    {!! trans('restaurantapp::restaurant_app.label.meta_tags') !!}
                </label><br />
                    {!! $restaurant_app['meta_tags'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="meta_keywords">
                    {!! trans('restaurantapp::restaurant_app.label.meta_keywords') !!}
                </label><br />
                    {!! $restaurant_app['meta_keywords'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="meta_description">
                    {!! trans('restaurantapp::restaurant_app.label.meta_description') !!}
                </label><br />
                    {!! $restaurant_app['meta_description'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="email">
                    {!! trans('restaurantapp::restaurant_app.label.email') !!}
                </label><br />
                    {!! $restaurant_app['email'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="alternate_phone">
                    {!! trans('restaurantapp::restaurant_app.label.alternate_phone') !!}
                </label><br />
                    {!! $restaurant_app['alternate_phone'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="gallery">
                    {!! trans('restaurantapp::restaurant_app.label.gallery') !!}
                </label><br />
                    {!! $restaurant_app['gallery'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="slider_images">
                    {!! trans('restaurantapp::restaurant_app.label.slider_images') !!}
                </label><br />
                    {!! $restaurant_app['slider_images'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="featured_food">
                    {!! trans('restaurantapp::restaurant_app.label.featured_food') !!}
                </label><br />
                    {!! $restaurant_app['featured_food'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="locations">
                    {!! trans('restaurantapp::restaurant_app.label.locations') !!}
                </label><br />
                    {!! $restaurant_app['locations'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="contact_info">
                    {!! trans('restaurantapp::restaurant_app.label.contact_info') !!}
                </label><br />
                    {!! $restaurant_app['contact_info'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="menus">
                    {!! trans('restaurantapp::restaurant_app.label.menus') !!}
                </label><br />
                    {!! $restaurant_app['menus'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('restaurantapp::restaurant_app.label.created_at') !!}
                </label><br />
                    {!! $restaurant_app['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for=")">
                    {!! trans('restaurantapp::restaurant_app.label.)') !!}
                </label><br />
                    {!! $restaurant_app[')'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('restaurantapp::restaurant_app.label.updated_at') !!}
                </label><br />
                    {!! $restaurant_app['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('restaurantapp::restaurant_app.label.deleted_at') !!}
                </label><br />
                    {!! $restaurant_app['deleted_at'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('id')
                       -> label(trans('restaurantapp::restaurant_app.label.id'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('restaurantapp::restaurant_app.label.restaurant_id'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('point_percentage')
                       -> label(trans('restaurantapp::restaurant_app.label.point_percentage'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.point_percentage'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('points')
                       -> label(trans('restaurantapp::restaurant_app.label.points'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.points'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('title')
                       -> label(trans('restaurantapp::restaurant_app.label.title'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.title'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('subtitle')
                       -> label(trans('restaurantapp::restaurant_app.label.subtitle'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.subtitle'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('meta_tags')
                       -> label(trans('restaurantapp::restaurant_app.label.meta_tags'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.meta_tags'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('meta_keywords')
                       -> label(trans('restaurantapp::restaurant_app.label.meta_keywords'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.meta_keywords'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('meta_description')
                       -> label(trans('restaurantapp::restaurant_app.label.meta_description'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.meta_description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('email')
                       -> label(trans('restaurantapp::restaurant_app.label.email'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('alternate_phone')
                       -> label(trans('restaurantapp::restaurant_app.label.alternate_phone'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.alternate_phone'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('gallery')
                       -> label(trans('restaurantapp::restaurant_app.label.gallery'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.gallery'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('slider_images')
                       -> label(trans('restaurantapp::restaurant_app.label.slider_images'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.slider_images'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('featured_food')
                       -> label(trans('restaurantapp::restaurant_app.label.featured_food'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.featured_food'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('locations')
                       -> label(trans('restaurantapp::restaurant_app.label.locations'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.locations'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('contact_info')
                       -> label(trans('restaurantapp::restaurant_app.label.contact_info'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.contact_info'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('menus')
                       -> label(trans('restaurantapp::restaurant_app.label.menus'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.menus'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text(')')
                       -> label(trans('restaurantapp::restaurant_app.label.)'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.)'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



