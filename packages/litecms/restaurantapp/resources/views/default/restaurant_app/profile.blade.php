<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="{{ url('app/address/' . @$restaurant->id) }}" class="header-title">My Profile</a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>
    <div id="footer-bar" class="footer-bar-6">
        @include('restaurantapp::default.restaurant_app.footer', ['page' => 'account'])
    </div>
    <div class="page-title page-title-fixed">
        <div>
            <h1>My Profile</h1>

        </div>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i
                class="fa fa-moon"></i></a>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i
                class="fa fa-lightbulb color-yellow-dark"></i></a>
        {{-- <a href="#"
            class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;">.envOnline</a>
        --}}
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i
                class="fa fa-bars"></i></a>
    </div>
    <div class="page-title-clear"></div>

    <div class="page-content pb-3">
        <div class="card card-style" data-card-height="27vh" style="height: 27vh;">
            <div class="card-bottom text-center ml-3 mr-3">
                {{-- <img src="images/avatars/3.jpg" class="rounded-m mb-2" width="80"> --}}
                <h1 class="font-30 line-height-xl color-dark">{{ user()->name }}</h1>
                <p class="color-dark font-16 opacity-60"><i class="fa fa-map-marker mr-2"></i>{{ user()->address }}</p>
            </div>
        </div>
    
        <div class="card card-style">
            <div class="content">
                {{-- <p class="mb-n1 color-highlight font-600 font-12">Edit your Account</p> --}}
                <h4>Edit Profile</h4>
                <p></p>
                {!!Form::vertical_open()
                    ->id('form-update-profile')
                    ->method('POST')
                    ->action(guard_url('profile'))
                    ->class('update-profile')!!}
                <div class="mt-5 mb-3">
                    <div class="input-style input-style-2 mb-3 pb-1">
                        <span class="input-style-1-active">Full Name</span>
                        <em>(required)</em>
                        {!! Form::text('name')
                        -> label('name')
                        ->required()
                        ->value(user()->name)
                        ->raw()!!}
                    </div>
                    <div class="input-style input-style-2 mb-3 pb-1">
                        <span class="input-style-1-active">Email</span>
                        <em>(required)</em>
                        {!! Form::email('email')
                        -> label(trans('user::user.label.email'))
                        ->value(user()->user_email)
                        ->required()
                        -> raw() !!}
                    </div>
                    <div class="input-style input-style-2 mb-3 pb-1">
                        <span class="input-style-1-active">Phone</span>
                        <em>(required)</em>
                        {!! Form::text('mobile')
                        -> label(trans('user::user.label.mobile'))
                        ->value(user()->mobile)
                        ->required()
                        ->raw() !!}
                    </div>
                    {!! Form::hidden('restaurant_id')->value(@$restaurant->id) !!}
                    {!! Form::hidden('is_app')->value(1) !!}
                </div>
                <button type="submit" class="btn btn-full btn-m bg-highlight rounded-s font-13 font-600 mt-4">Update</button>
                {!! Form::close() !!}
            </div>
        </div>

        <div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
            @include('restaurantapp::default.restaurant_app.header')
        </div>
    </div>

</div>