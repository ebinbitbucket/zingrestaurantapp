            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('id')
                       -> label(trans('restaurantapp::restaurant_app.label.id'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('restaurant_id')
                       -> label(trans('restaurantapp::restaurant_app.label.restaurant_id'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.restaurant_id'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('point_percentage')
                       -> label(trans('restaurantapp::restaurant_app.label.point_percentage'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.point_percentage'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('points')
                       -> label(trans('restaurantapp::restaurant_app.label.points'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.points'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('title')
                       -> label(trans('restaurantapp::restaurant_app.label.title'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.title'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('subtitle')
                       -> label(trans('restaurantapp::restaurant_app.label.subtitle'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.subtitle'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('meta_tags')
                       -> label(trans('restaurantapp::restaurant_app.label.meta_tags'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.meta_tags'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('meta_keywords')
                       -> label(trans('restaurantapp::restaurant_app.label.meta_keywords'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.meta_keywords'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('meta_description')
                       -> label(trans('restaurantapp::restaurant_app.label.meta_description'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.meta_description'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('email')
                       -> label(trans('restaurantapp::restaurant_app.label.email'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('alternate_phone')
                       -> label(trans('restaurantapp::restaurant_app.label.alternate_phone'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.alternate_phone'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('gallery')
                       -> label(trans('restaurantapp::restaurant_app.label.gallery'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.gallery'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('slider_images')
                       -> label(trans('restaurantapp::restaurant_app.label.slider_images'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.slider_images'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('featured_food')
                       -> label(trans('restaurantapp::restaurant_app.label.featured_food'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.featured_food'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('locations')
                       -> label(trans('restaurantapp::restaurant_app.label.locations'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.locations'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('contact_info')
                       -> label(trans('restaurantapp::restaurant_app.label.contact_info'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.contact_info'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('menus')
                       -> label(trans('restaurantapp::restaurant_app.label.menus'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.menus'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text(')')
                       -> label(trans('restaurantapp::restaurant_app.label.)'))
                       -> placeholder(trans('restaurantapp::restaurant_app.placeholder.)'))!!}
                </div>
            </div>