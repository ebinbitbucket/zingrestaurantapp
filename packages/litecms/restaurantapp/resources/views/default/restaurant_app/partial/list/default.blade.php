                    <div class="list-view">
                        @forelse($restaurant_apps as $restaurant_app)
                        <div class="card list-view-media"  id="{!! $restaurant_app->getRouteKey() !!}">
                            <div class="card-block">
                                <div class="media">
                                    <a class="media-left" href="#"><img class="media-object card-list-img" src="{!!$restaurant_app->picture!!}"></a>
                                    <div class="media-body">
                                        <div class="heading">
                                            <h3>{!! $restaurant_app->name !!}</h3>
                                            <h6>{!! $restaurant_app->email !!}</h6>
                                            <div class="status">
                                                <span class="verified">Verified</span>
                                                <span class="approved">Approved</span>
                                            </div>
                                        </div>
                                        <p>{!! $restaurant_app->details !!}</p>
                                        <div class="actions">

                                            <a href="{!! guard_url('restaurantapp/restaurant_app') !!}/{!! $restaurant_app->getRouteKey() !!}/edit" class="text-primary" data-toggle="tooltip" data-placement="left" title="Edit" data-action="EDIT" ><i class="icon-pencil"></i></a>

                                            <a href="{!! guard_url('restaurantapp/restaurant_app') !!}/{!! $restaurant_app->getRouteKey() !!}" class="text-danger" data-toggle="tooltip" data-placement="left" title="Delete" data-action="DELETE" data-remove="{!! $restaurant_app->getRouteKey() !!}"><i class="icon-trash"></i></a>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endif
                </div>