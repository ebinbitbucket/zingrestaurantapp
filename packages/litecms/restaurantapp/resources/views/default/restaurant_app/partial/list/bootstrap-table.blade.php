            <table class="table" id="main-table" data-url="{!!guard_url('restaurantapp/restaurant_app?withdata=Y')!!}">
                <thead>
                    <tr>
                        <th data-field="id">{!! trans('restaurantapp::restaurant_app.label.id')!!}</th>
                    <th data-field="restaurant_id">{!! trans('restaurantapp::restaurant_app.label.restaurant_id')!!}</th>
                    <th data-field="point_percentage">{!! trans('restaurantapp::restaurant_app.label.point_percentage')!!}</th>
                    <th data-field="points">{!! trans('restaurantapp::restaurant_app.label.points')!!}</th>
                    <th data-field="title">{!! trans('restaurantapp::restaurant_app.label.title')!!}</th>
                    <th data-field="subtitle">{!! trans('restaurantapp::restaurant_app.label.subtitle')!!}</th>
                    <th data-field="meta_tags">{!! trans('restaurantapp::restaurant_app.label.meta_tags')!!}</th>
                    <th data-field="meta_keywords">{!! trans('restaurantapp::restaurant_app.label.meta_keywords')!!}</th>
                    <th data-field="meta_description">{!! trans('restaurantapp::restaurant_app.label.meta_description')!!}</th>
                    <th data-field="email">{!! trans('restaurantapp::restaurant_app.label.email')!!}</th>
                    <th data-field="alternate_phone">{!! trans('restaurantapp::restaurant_app.label.alternate_phone')!!}</th>
                    <th data-field="gallery">{!! trans('restaurantapp::restaurant_app.label.gallery')!!}</th>
                    <th data-field="slider_images">{!! trans('restaurantapp::restaurant_app.label.slider_images')!!}</th>
                    <th data-field="featured_food">{!! trans('restaurantapp::restaurant_app.label.featured_food')!!}</th>
                    <th data-field="locations">{!! trans('restaurantapp::restaurant_app.label.locations')!!}</th>
                    <th data-field="contact_info">{!! trans('restaurantapp::restaurant_app.label.contact_info')!!}</th>
                    <th data-field="menus">{!! trans('restaurantapp::restaurant_app.label.menus')!!}</th>
                    <th data-field=")">{!! trans('restaurantapp::restaurant_app.label.)')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">Actions</th>
                    </tr>
                </thead>
            </table>