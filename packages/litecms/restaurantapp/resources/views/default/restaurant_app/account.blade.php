<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="{{url('app/account'.@$restaurant->id)}}" class="header-title">My Account</a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i class="fas fa-moon"></i></a>
    </div>
    <div id="footer-bar" class="footer-bar-6">
        @include('restaurantapp::default.restaurant_app.footer', ['page' => 'account'])
    </div>
    <div class="page-title page-title-fixed">
        <div>
            <h1>Account</h1>
        </div>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i class="fa fa-moon"></i></a>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i class="fa fa-lightbulb color-yellow-dark"></i></a>
        {{-- <a href="#" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;">Order Online</a> --}}
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i class="fa fa-bars"></i></a>
    </div>
    <div class="page-title-clear"></div>
    <div class="page-content">
        <div class="card card-style" data-card-height="30vh" style="height: 27vh;">
            <div class="card-bottom text-center ml-3 mr-3">
                {{-- <img src="{{ url(@$app_client->defaultImage('photo')) }}" class="rounded-m mb-2" width="80"> --}}
                <h1 class="font-30 line-height-xl color-dark">{{ user()->name }}</h1>
                <p class="color-dark font-16 opacity-60"><i class="fa fa-map-marker mr-2"></i>{{ user()->address }}</p>
            </div>
        </div>
        <div class="card card-style">
            <div class="content mt-0">
                <div class="accordion" id="accordion-1">
                  <div class="list-group list-custom-small list-icon-0">
                      <a href="{{guard_url('app/orders/currorders/'.@$restaurant->id)}}" aria-expanded="fasle">
                           <i class="fa fa-shopping-cart"></i>
                          <span class="font-16">Orders</span>
                          <i class="fa fa-angle-right"></i>
                      </a>
                      <a href="{{guard_url('app/address/'.@$restaurant->id)}}" aria-expanded="false">
                        <i class="fa fa-map-marker"></i>
                        <span class="font-16">Address</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <a href="{{guard_url('app/edit-profile/'.@$restaurant->id)}}" aria-expanded="false">
                        <i class="fa fa-edit"></i>
                        <span class="font-16">Edit Profile</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    {{-- <a href="#" aria-expanded="false">
                        <i class="fas fa-key"></i>
                        <span class="font-16">Reset Password</span>
                        <i class="fa fa-angle-right"></i>
                    </a> --}}
                    <a href="{{guard_url('app/points/'.@$restaurant->id)}}" aria-expanded="false">
                        <i class="fa fa-star"></i>
                        <span class="font-16">Points</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <a href="{{guard_url('app/favorites/'.@$restaurant->id)}}" aria-expanded="false">
                        <i class="fa fa-heart"></i>
                        <span class="font-16">Favorites</span>
                        <i class="fa fa-angle-right"></i>
                        
                    </a>
                    <a href="{{trans_url('client/app/logout/'.@$restaurant->id)}}" aria-expanded="false">
                        <i class="fa fa-user"></i>
                        <span class="font-16">Sign Out</span>
                        <i class="fa fa-angle-right"></i>
                        
                    </a>
                  </div>
              </div>
            </div>
        </div>
    </div>

    <div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
        {{-- @include('app.header') --}}
        @include('restaurantapp::default.restaurant_app.header', ['page' => 'account'])

    </div>
</div>