@if(!empty($restaurant->logo))
 <div class="card rounded-0" style="background-image: url('images/menu-bg.jpg'); height:200px">
    <div class="card-top">
        <a href="#" class="close-menu float-right mr-2 text-center mt-3 icon-40 notch-clear"><i class="fa fa-times color-white"></i></a>
    </div>
    <div class="card-bottom">
        <h1 class="color-white pl-3 mb-2 font-28">
            <img src="{{url(@$restaurant->defaultImage('logo'))}}" style="height: 60px;" alt="">
        </h1>
    </div>
    <div class="card-overlay bg-gradient"></div>

</div>

@endif
<div class="mt-4"></div>

<h6 class="menu-divider">Menu</h6>
<div class="list-group list-custom-small list-menu">
    <a id="nav-welcome" href="{{guard_url('app/home/'.@$restaurant->id)}}">
        <i class="fa fa-home gradient-orange color-white"></i>
        <span>Home</span>
        <i class="fa fa-angle-right"></i>
    </a>
    <a id="nav-welcome" href="{{guard_url('app/menu/'.@$restaurant->id)}}">
        <i class="fa fa-utensils gradient-red color-white"></i>
        <span>Menu</span>
        <i class="fa fa-angle-right"></i>
    </a>
    <a id="nav-homepages" href="{{guard_url('app/gallery/'.@$restaurant->id)}}">
        <i class="fa fa-images gradient-green color-white"></i>
        <span>Images</span>
        <i class="fa fa-angle-right"></i>
    </a>
    <a id="nav-components" href="{{guard_url('app/contact/'.@$restaurant->id)}}">
        <i class="fa fa-address-book gradient-yellow color-white"></i>
        <span>Hours</span>
        <i class="fa fa-angle-right"></i>
    </a>
    <a id="nav-components" href="{{guard_url('app/locations/'.@$restaurant->id)}}">
        <i class="fa fa-store gradient-blue color-white"></i>
        <span>Contact</span>
        <i class="fa fa-angle-right"></i>
    </a>
    <a id="nav-components" href="{{guard_url('app/account/'.@$restaurant->id)}}">
        <i class="fa fa-user gradient-orange color-white"></i>
        <span>My Account</span>
        <i class="fa fa-angle-right"></i>
    </a>
    {{-- <a id="nav-components" href="{{url('app/'.$restaurant->id)}}">
        <i class="fa fa-user gradient-orange color-white"></i>
        <span>Login</span>
        <i class="fa fa-angle-right"></i>
    </a>
    <a id="nav-components" href="{{url('app/register/'.$restaurant->id)}}">
        <i class="fa fa-user gradient-orange color-white"></i>
        <span>Register</span>
        <i class="fa fa-angle-right"></i>
    </a> --}}
    <a id="nav-components" href="{{trans_url('client/app/logout/'.@$restaurant->id)}}">
        <i class="fa fa-user gradient-orange color-white"></i>
        <span>Sign Out</span>
        <i class="fa fa-angle-right"></i>
    </a>

</div>
<h6 class="menu-divider mt-4">Settings</h6>
<div class="list-group list-custom-small list-menu">
    <a href="#" data-toggle-theme data-trigger-switch="switch-dark-mode">
        <i class="fa fa-moon gradient-dark color-white"></i>
        <span>Dark Mode</span>
        <div class="custom-control scale-switch ios-switch highlight-switch switch-s">
        <input type="checkbox" class="ios-input" id="switch-22">
        <label class="custom-control-label" for="switch-1"></label>
        </div>
    </a>
    {{-- @if(is_array(@$restaurant->locations) && count(@$restaurant->locations)>=1) --}}
        <a href="{{guard_url('app/locations/'.$restaurant->id)}}" class="btn btn-full btn-sm rounded-s ml-3 mb-3 font-600 bg-highlight mt-3">Order Online</a>
    {{-- @else
        <a href="{{trans_url('restaurants/'.@$restaurant->slug)}}" class="btn btn-full btn-sm rounded-s ml-3 mb-3 font-600 bg-highlight mt-3">Order Online</a>
    @endif --}}
</div>