<div id="page">
		<div class="header header-auto-show header-fixed header-logo-center">
			<a href="{{url('app/'.@$restaurant->id)}}" class="header-title">Hours</a>
			<a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
			<a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i class="fas fa-sun"></i></a>
			<a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i class="fas fa-moon"></i></a>
		</div>
		<div id="footer-bar" class="footer-bar-6">
			@include('restaurantapp::default.restaurant_app.footer', ['page' => 'contact'])
		</div>
		<div class="page-title page-title-fixed">
			<div>
				<h1>Hours</h1>
			</div>
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i class="fa fa-moon"></i></a>
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i class="fa fa-lightbulb color-yellow-dark"></i></a>
			{{-- <a href="#" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;">Order Online</a> --}}
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i class="fa fa-bars"></i></a>
		</div>
		<div class="page-title-clear"></div>
		{{-- <div class="card card-fixed mb-n5" data-card-height="350">
			<div class="map-full w-100 h-100" id="map_canvas">
				
			</div>
		</div> --}}
		{{-- <div class="card card-clear" data-card-height="350"></div> --}}
		<div class="page-content pb-3">
			<div class="card card-full rounded-m pb-4">
				{{-- <div class="drag-line"></div> --}}
				<div class="card card-style mt-4">
					{{-- <div data-card-height="120" data-closed-message="Our Friendly Staff is here to help" data-closed-message-under="We'll be back very soon!" data-opened-message="Welcome!" data-opened-message-under="Our Friendly Staff is here to help!" class="business-hours caption shadow-xl bg-red-dark is-business-opened" style="height:120px;">
						<div class="card-top">
						@php
						$ph_number = preg_replace("/[^0-9]/", "", $app->alternate_phone);
						@endphp
							<a href="tel:{{@$ph_number}}" class="float-right btn btn-full btn-sm rounded-s mt-2 font-600 bg-white color-black mr-3" style="height: 40px;">Call Us Now</a>
						</div>
						
						<div class="card-top mt-4 ml-3 pl-1">
							<h1 class="color-white mt-1 font-20 font-700">Welcome!</h1>
							<p class="color-white opacity-90 mt-n2 mb-0">Our Friendly Staff is here to help</p>
						</div>
						<div class="caption-overlay show-business-opened bg-green-dark"></div>
						<div class="caption-overlay show-business-closed bg-red-dark disabled"></div>
					</div> --}}
					<div class="content font-600">
					@if(!empty($weekMap))
						@foreach($weekMap as $wkey => $wval)
	                    @if(isset($restaurant_timings[$wkey]))
						@php 
						$i = 0;
						@endphp
	                    	@foreach($restaurant_timings[$wkey] as $val)
	                            @if($val->opening== 'off' || $val->closing == 'off')
	                            <div class="working-hours {{date('l') == $wval ? 'day-wednesday bg-green-dark' : 'day-monday'}}"> <p>{{@$wval}}</p> <p >-</p> <p>Closed </p></div>
	                            @else
	                            <div class="working-hours {{date('l') == $wval ? 'day-wednesday bg-green-dark' : 'day-monday'}} "> 
									@if($i > 0)<p></p> 
									@else
									 <p>{{@$wval}}</p>
									 @endif 
									 <p> {{date('h:i a',strtotime($val->opening))}}</p> <p>{!!date('h:i a',strtotime($val->closing))!!}</p></div>
	                            @endif
	                            @php 
								$i++;
								@endphp
	                        @endforeach    
	                    @endif
						@endforeach
	                @endif
					</div>
				</div>
				{{-- <div class="content">
					<h1>Contact Us</h1>
					<p>
					{{ $app->contact_info }}
					</p>
					<div class="formValidationError bg-red-dark mb-4 rounded-sm shadow-xl" id="contactNameFieldError">
						<p class="text-center text-uppercase p-2 color-white font-900 ">Name is required!</p>
					</div>
					<div class="formValidationError bg-red-dark mb-4 rounded-sm shadow-xl" id="contactEmailFieldError">
						<p class="text-center text-uppercase p-2 color-white font-900">Mail address required!</p>
					</div>
					<div class="formValidationError bg-red-dark mb-4 rounded-sm shadow-xl" id="contactEmailFieldError2">
						<p class="text-center text-uppercase p-2 color-white font-900">Mail address must be valid!</p>
					</div>
					<div class="formValidationError bg-red-dark mb-4 rounded-sm shadow-xl" id="contactMessageTextareaError">
						<p class="text-center text-uppercase p-2 color-white font-900">Message field is empty!</p>
					</div>
					<div class="formSuccessMessageWrap card card-style">
						<div class="shadow-l rounded-m bg-gradient-green1 mr-n1 ml-n1 mb-n1 ">
							<h1 class="color-white text-center pt-4"><i class="fa fa-check-circle fa-3x shadow-s scale-box rounded-circle"></i></h1>
							<h2 class="color-white bold text-center pt-3">Message Sent</h2>
							<p class="color-white pb-4 text-center mt-n2 mb-0">We'll get back to you shortly.</p>
						</div>
					</div>
					<form action="{{url('app/sendmail/'.$restaurant->id)}}" method="post" class="contactForm" id="contactForm">
						<fieldset>
							<div class="form-field form-name">
								<label class="contactNameField color-theme" for="contactNameField">Name:<span>(required)</span></label>
								<input type="text" name="cf_name" value="" class="contactField round-small requiredField" id="contactNameField" />
							</div>
							<div class="form-field form-email">
								<label class="contactEmailField color-theme" for="contactEmailField">Email:<span>(required)</span></label>
								<input type="text" name="cf_email" value="" class="contactField round-small requiredField requiredEmailField" id="contactEmailField" />
							</div>
							<div class="form-field form-text">
								<label class="contactMessageTextarea color-theme" for="contactMessageTextarea">Message:<span>(required)</span></label>
								<textarea name="cf_message" class="contactTextarea round-small requiredField" id="contactMessageTextarea"></textarea>
							</div>
							<div class="form-button">
								<input type="submit" class="btn bg-highlight text-uppercase font-600 font-12 btn-full rounded-s  shadow-xl contactSubmitButton" value="Send Message" data-formId="contactForm" />
							</div>
						</fieldset>
					</form>
					
				</div>
				--}}
			</div> 
		</div>

	

		<div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
			@include('restaurantapp::default.restaurant_app.header')
		</div>
	</div>

 {{-- <script src="https://maps.googleapis.com/maps/api/js?key=
{{ config('services.GOOGLE_API') }}&libraries=places"></script>
<script type="text/javascript">
            var baseUrl ="{{url('/') }}";
            var transUrl ="{{trans_url('/') }}";
            var vote_restaurant_id = "{!!$restaurant->id!!}";
            var vote_restaurant_slug = "{!!$restaurant->slug!!}";
                $(function(){ 
          
     var map,myLatlng;
      @if(!empty($restaurant->latitude) && !empty($restaurant->longitude))
         myLatlng = new google.maps.LatLng({!! @$restaurant->latitude !!},{!! @$restaurant->longitude !!});
      @else
         myLatlng = new google.maps.LatLng(9.929789275194516,76.27235919804684);
      @endif
      var myOptions = {
         zoom: 10,
         center: myLatlng,
         mapTypeId: google.maps.MapTypeId.ROADMAP,
         disableDefaultUI: true
         }
      map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

      var marker = new google.maps.Marker({
      draggable: true,
      position: myLatlng,
      map: map,
      title: "Your location"
      });

      google.maps.event.addListener(marker, 'dragend', function (event) {
        $("#latitude").val(this.getPosition().lat());
        $("#longitude").val(this.getPosition().lng());
    });
       })

</script> --}}