<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="{{ url('app/address/' . @$restaurant->id) }}" class="header-title">Address</a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>
    <div id="footer-bar" class="footer-bar-6">
        @include('restaurantapp::default.restaurant_app.footer', ['page' => 'account'])
    </div>
    <div class="page-title page-title-fixed">
        <div>
            <h1>Address</h1>
        </div>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i
                class="fa fa-moon"></i></a>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i
                class="fa fa-lightbulb color-yellow-dark"></i></a>
        {{-- <a href="#"
            class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;">Order Online</a>
        --}}
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i
                class="fa fa-bars"></i></a>
    </div>
    <div class="page-title-clear"></div>
    <div class="page-content pb-3">

        <div class="card card-style bg-theme pb-0">
            <div class="content">
                <div class="tab-controls tabs-round tab-animated tabs-small tabs-rounded shadow-xl" data-tab-items="2"
                    data-tab-active="bg-red-dark color-white">
                    <a href="#" data-tab-active="" data-tab="tab-1" class="bg-red-dark color-white no-click"
                        style="width: 25%;">Address</a>
                    {{-- <a href="#" data-tab="tab-2" style="width: 25%;" class="">Shipping Address</a> --}}
                </div>
                <div class="clearfix mb-3"></div>
                <div class="tab-content" id="tab-1" style="display: block;">
                    @php
                    $delivery_address="false";
                    @endphp
                    @forelse($client_address as $data)
                        @if ($data->type == 'delivery')
                        @php
                        $delivery_address="true";
                         @endphp
                            <div class="card card-style">
                                <div class="content mb-1" id="locId">
                                    <div>
                                        <h5 class="font-16 font-600">{{ @$data->title }}</h5>
                                        <span style="color:grey;" class="font-14">{{ @$data->address }}</span>
                                        <br>
                                    </div>
                                    <div class="divider mb-3"></div>
                                </div>
                            </div>
                        @endif
                    @empty
                    @endif
                    @if($delivery_address == 'false')
                    <div class="empty-content-wrap">
                        <h6>Address Not Available!</h6>
                    </div>
                    @endif
                    </div>
                    {{-- <div class="tab-content" id="tab-2" style="display: none;">
                        @php
                            $shipping_address="false";
                        @endphp
                        @forelse($client_address as $data)
                            @if ($data->type == 'billing')
                            @php
                            $shipping_address="true";
                             @endphp
                                <div class="card card-style">
                                    <div class="content mb-1" id="locId">
                                        <div>
                                            <h5 class="font-16 font-600">{{ @$data->title }}</h5>
                                            <span style="color:grey;" class="font-14">{{ @$data->address }}</span>
                                            <br>
                                        </div>
                                        <div class="divider mb-3"></div>
                                    </div>
                                </div>
                            @endif
                        @empty
                        @endif
                        @if($shipping_address == 'false')
                        <div class="empty-content-wrap">
                            <h6>Address Not Available!</h6>
                        </div>
                        @endif
                    </div> --}}
                </div>
            </div>







            </div>
            <div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
                @include('restaurantapp::default.restaurant_app.header')
            </div>
        </div>
