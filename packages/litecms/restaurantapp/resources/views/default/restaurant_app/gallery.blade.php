<style>
	.gallery-columns {
    columns: 3;
    column-gap: 1px;
}
.gallery-columns .gallery-item {
    margin: 0 1px 1px 0;
    display: inline-block;
    width: 100%;
}
</style>
<div id="page">
		<div class="header header-auto-show header-fixed header-logo-center">
			<a href="{{url('app/'.@$restaurant->id)}}" class="header-title">Images</a>
			<a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
			<a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i class="fas fa-sun"></i></a>
			<a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i class="fas fa-moon"></i></a>
		</div>
		<div id="footer-bar" class="footer-bar-6">
			@include('restaurantapp::default.restaurant_app.footer', ['page' => 'gallery'])
		</div>
		<div class="page-title page-title-fixed">
			<div>
				<h1>Images</h1>
			</div>
			{{-- <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i class="fa fa-moon"></i></a>
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i class="fa fa-lightbulb color-yellow-dark"></i></a> --}}
			
			@if (!empty($app->locations))
            @if (count($app->locations) == 1)
			@forelse($app->locations as $data)
			<a href="{{ @$data['url'] }}?client_id={{hashids_encode(user()->id)}}" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;width: 122px;">Order Now</a>
			@break
			@empty
			@endif
            @else
			<a href="{{guard_url('app/locations/'.$restaurant->id)}}" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;width: 122px;">Order Now</a>
            @endif
			@endif
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i class="fa fa-bars"></i></a>
		</div>
		<div class="page-title-clear"></div>
		<div class="page-content">
			<div class="gallery-columns">
				@if(!empty(@$app->gallery))
				@forelse(@$app->gallery as $key=> $image) 
				<div class="gallery-item">
					<a href="{!!url(@$app->defaultImage('gallery' ,'original',@$key))!!}" title="Images" data-lightbox="gallery">
						<img src="{!!url(@$app->defaultImage('gallery' ,'original',@$key))!!}" class="img-fluid border border-transparent">
					</a>
				</div>
				@empty
				@endif
	       	 	@endif
				{{-- <div class="gallery-item">
					<a href="https://zingmyorder.com/image/hm/master/master/2019/08/12/215633544/image/orange-chicken.jpg" title="Crazy Hair" data-lightbox="gallery">
						<img src="https://zingmyorder.com/image/hm/master/master/2019/08/12/215633544/image/orange-chicken.jpg" class="img-fluid border border-transparent">
					</a>
				</div>
				<div class="gallery-item">
					<a href="https://zingmyorder.com/image/hm/master/master/2019/08/13/131123114/image/chicken-momo.jpg" title="Neon Lights" data-lightbox="gallery">
						<img src="https://zingmyorder.com/image/hm/master/master/2019/08/13/131123114/image/chicken-momo.jpg" class="img-fluid border border-transparent">
					</a>
				</div>
				<div class="gallery-item">
					<a href="https://zingmyorder.com/image/hm/master/master/2019/08/20/065036343/image/chicken-noodles.jpg" title="Neon Lights" data-lightbox="gallery">
						<img src="https://zingmyorder.com/image/hm/master/master/2019/08/20/065036343/image/chicken-noodles.jpg" class="img-fluid border border-transparent">
					</a>
				</div>
				<div class="gallery-item">
					<a href="https://zingmyorder.com/image/hm/master/master/2019/08/22/095119949/image/poke-bowl.jpg" title="Neon Lights" data-lightbox="gallery">
						<img src="https://zingmyorder.com/image/hm/master/master/2019/08/22/095119949/image/poke-bowl.jpg" class="img-fluid border border-transparent">
					</a>
				</div>
				<div class="gallery-item">
					<a href="https://zingmyorder.com/image/hm/master/master/2019/09/07/090311283/image/po-boy.jpg" title="Neon Lights" data-lightbox="gallery">
						<img src="https://zingmyorder.com/image/hm/master/master/2019/09/07/090311283/image/po-boy.jpg" class="img-fluid border border-transparent">
					</a>
				</div>
				<div class="gallery-item">
					<a href="https://zingmyorder.com/image/hm/master/master/2019/08/20/081918297/image/chicken-curry.jpg" title="Flexibility Unmatched" data-lightbox="gallery">
						<img src="https://zingmyorder.com/image/hm/master/master/2019/08/20/081918297/image/chicken-curry.jpg" class="img-fluid border border-transparent">
					</a>
				</div>
				<div class="gallery-item">
					<a href="https://zingmyorder.com/image/hm/master/master/2019/08/23/034151339/image/california-roll.jpg" title="Crazy Hair" data-lightbox="gallery">
						<img src="https://zingmyorder.com/image/hm/master/master/2019/08/23/034151339/image/california-roll.jpg" class="img-fluid border border-transparent">
					</a>
				</div>
				<div class="gallery-item">
					<a href="https://zingmyorder.com/image/hm/master/master/2019/09/20/030151756/image/korean-beef-bulgogi.jpg" title="Neon Lights" data-lightbox="gallery">
						<img src="https://zingmyorder.com/image/hm/master/master/2019/09/20/030151756/image/korean-beef-bulgogi.jpg" class="img-fluid border border-transparent">
					</a>
				</div> --}}
			</div>
		</div>


		{{-- <div class="page-title-clear"></div>
		<div class="page-content">
			<div class="row m-0">
				@if(!empty(@$app->gallery))
				@forelse(@$app->gallery as $key=> $image) 
				<div class="col-4 p-0">
					<a href="{!!url(@$app->defaultImage('gallery' ,'original',@$key))!!}" title="Images" data-lightbox="gallery">
						<img src="{!!url(@$app->defaultImage('gallery','original',@$key))!!}" class="img-fluid border border-transparent">
					</a>
				</div>
				@empty
				@endif
	       	 	@endif
			</div>
		</div> --}}

	

		<div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
			@include('restaurantapp::default.restaurant_app.header')
		</div>
	</div>
