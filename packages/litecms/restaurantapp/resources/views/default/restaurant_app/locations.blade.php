<div id="page">
		<div class="header header-auto-show header-fixed header-logo-center">
			<a href="{{url('app/'.@$restaurant->id)}}" class="header-title">Contact</a>
			<a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
			<a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i class="fas fa-sun"></i></a>
			<a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i class="fas fa-moon"></i></a>
		</div>
		<div id="footer-bar" class="footer-bar-6">
			@include('restaurantapp::default.restaurant_app.footer', ['page' => 'locations'])
		</div>
		<div class="page-title page-title-fixed">
			<div>
				<h1>Contact</h1>
			
			</div>
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i class="fa fa-moon"></i></a>
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i class="fa fa-lightbulb color-yellow-dark"></i></a>
			{{-- <a href="#" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;">Order Online</a> --}}
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i class="fa fa-bars"></i></a>
		</div>
		<div class="page-title-clear"></div>
		
		<div class="page-content pb-3">
			@if(!empty($app->locations))
			@forelse($app->locations as $data) 
			<div class="card card-style"}>
				<div class="content mb-1" id="locId">
					<a href="{{@$data['url']}}?client_id={{hashids_encode(user()->id)}}" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3 float-right" style="height: 40px;">Order Online</a>
					 @php
						$ph_number = preg_replace("/[^0-9]/", "", $data['phone']);
						@endphp
						<div>
							<h5 class="font-16 font-600">{{@$data['text']}}</h5>
							<span style="color:grey;" class="font-14">{{@$data['address']}}</span>
							<br>
							<p class="line-height-s mt-1 mb-0 font-14 font-600">
								<button class="sendMail" type="button" style="border: none;background: white;" value="{{@$data['email']}}"><i class="fa fa-envelope fa-lg" aria-hidden="true"></i>&nbsp;&nbsp;Send Mail
								</button>
							</p>
							<a href="tel:{{@$ph_number}}" class="d-flex mb-3 align-items-center">
							<p class="line-height-s mt-1 mb-0 font-14 font-600">
								<button style=" border: none;background: white;"><i class="fa fa-phone fa-lg" aria-hidden="true"></i></button>
								{{@$data['phone']}}
							</p>
						</a>
						</div>
					<div class="divider mb-3" ></div>
				</div>
			</div>
			@empty
			@endif
			@endif
		<div class="divider mb-3"  id="mailContactForm"></div>
		</div>

		<div class="content" id="mailForm">

		</div>
		<br>
		<br>



		<div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
			@include('restaurantapp::default.restaurant_app.header')
		</div>
	</div>
<script type="text/javascript">
	$(document).ready(function() {
	// $("body").delegate(".sendMail", "click", function(){
	$(".sendMail").click(function(event){
		  email = $(this).val();
		  var card = `<form action="{{ guard_url('app/sendmail/'.$restaurant->id) }}" method="post" class="contactForm" id="contactForm">
			@csrf
		 <fieldset>
			 <div class="form-field form-name">
                 <label class="contactNameField color-theme" for="contactNameField">Send To :<span></span></label>
                 <input type="text" name="send_mail" readonly value="${email}" class="contactField round-small requiredField"
                     id="contactNameField" />
             </div>
             <div class="form-field form-name">
                 <label class="contactNameField color-theme" for="contactNameField">Name:<span>(required)</span></label>
                 <input required type="text" name="cf_name" value="" class="contactField round-small requiredField"
                     id="contactNameField" />
             </div>
             <div class="form-field form-email">
                 <label class="contactEmailField color-theme"
                     for="contactEmailField">Email:<span>(required)</span></label>
                 <input required type="text" name="cf_email" value="" class="contactField round-small requiredField requiredEmailField" id="contactEmailField" />
             </div>
             <div class="form-field form-text">
                 <label class="contactMessageTextarea color-theme"
                     for="contactMessageTextarea">Message:<span>(required)</span></label>
                 <textarea name="cf_message" class="contactTextarea round-small requiredField"
                     id="contactMessageTextarea"></textarea>
             </div>
             <div class="form-button">
			 <div class="row">
			 <div class="col-md-1" style="width: 50%;">
                 <input style="height: 40px;" type="submit"
                     class="btn bg-highlight text-uppercase font-600 font-12 btn-full rounded-s  shadow-xl contactSubmitButton"
                     value="Send Message" data-formId="contactForm" />
			 </div>
			 <div class="col-md-1" style="width: 32%;right: 20px;" id="cancelButton">
					 <input style="height: 40px;" type="button"
                     class="btn bg-secondary text-uppercase font-600 font-12 btn-full rounded-s  shadow-xl contactSubmitButton"
                     value="Cancel" data-formId="contactForm" />
					</div>
				</div>


             </div>
         </fieldset>
     </form>`;
	 $('#mailForm').empty();
	 $('#mailForm').html(card);
	 var elmnt = document.getElementById("mailContactForm");
        elmnt.scrollIntoView();
	});

	$("body").delegate("#cancelButton", "click", function(){
		$('#mailForm').empty();
	});
});

</script>

