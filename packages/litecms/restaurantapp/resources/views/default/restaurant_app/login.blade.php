<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="#" class="header-title"><img src="images/logo.png" style="height: 30px;" alt=""></a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>

    <div class="page-content pb-0">
        <div class="card rounded-0" data-card-height="cover">
            <div class="card-center pl-3">
                <div class="text-center mb-3">
                    <h1 class="font-40 font-800 pb-2">
                        @if (!empty($restaurant->logo))
                            <img src="{{ url(@$restaurant->defaultImage('logo')) }}" style="height: 40px;" alt="">
                        @endif
                    </h1>
                </div>
                @if (Session::has('user_exists'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-danger text-left">
                                <span><b> Error - </b> {{ Session::get('user_exists') }}</span>
                            </div>
                        </div>
                    </div>

                    {{ Session::forget('user_exists') }}
                @endif
                @if (Session::has('validation_errors'))
                    <div class="row">
                        @foreach (Session::get('validation_errors') as $val_errors)
                            @foreach ($val_errors as $validation)
                                <div class="col-md-12">
                                    <div class="text-danger text-left">
                                        <span><b> Error - </b> {{ $validation }}</span>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                    {{ Session::forget('validation_errors') }}
                @endif

                {!! Form::vertical_open()
                ->id('login')
                ->method('POST')
                ->action('client/login') !!}

                {{ csrf_field() }}
                <div class="ml-3 mr-4 mb-n3">
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-user-circle"></i>
                        <span class="color-highlight">Your Email</span>
                        <em>(required)</em>
                        {!! Form::email('email')
                        ->required()
                        ->value('')
                        ->autocomplete('off')
                        ->placeholder('Enter Your Email')
                        ->raw() !!}
                    </div>
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-key"></i>
                        <span class="color-highlight">Your Password</span>
                        <em>(required)</em>
                        {!! Form::password('password')
                        ->placeholder('Enter Your Password')
                        ->autocomplete('off')
                        ->required()
                        ->raw() !!}
                        {!! Form::hidden('restaurant_id')->value(@$restaurant->id) !!}
                        {!! Form::hidden('is_app')->value(1) !!}
                        {!! Form::hidden('flag')->value('app') !!}
                        {!! Form::hidden('fcm_token')->value(@$fcm_token) !!}


                    </div>
                    <div class="clearfix"></div>

                    {{-- <a href="{{ url('client/password/reset') }}"
                        class="font-11 float-right color-theme opacity-30">Forgot Password?</a>
                    --}}
                    {{-- <a href="{{ url('app/register/' . $restaurant->id) }}"
                        class="float-right text-link">Sign Up</a> --}}
                    <div class="row pt-3 mb-3">
                        <div class="col-6 text-left">
                            <a href="{{ url('app/password/reset/' . $restaurant->id) }}">Forgot Password?</a>
                        </div>
                        <div class="col-6 text-right">
                            @if(isset($fcm_token))
                            <a href="{{ url('app/register/' . $restaurant->id.'?token='.@$fcm_token) }}">Sign Up Here</a>
                            @else
                            <a href="{{ url('app/register/' . $restaurant->id) }}">Sign Up Here</a>
                            @endif
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="form-group mt-40">
                    <button class="btn btn-center-l bg-highlight rounded-sm btn-l font-13 font-600 mt-5 border-0"
                        type="submit">Login</button>
                </div>
                {{-- <a class="text-center float-center">I forgot my password. <a
                        class="text-link" href="#">Reset Here</a></a>
                <a href="{{ url('app/register/' . $restaurant->id) }}" class="float-center">Sign Up</a>
                <a href="dashboard.html"
                    class="btn btn-center-l bg-highlight rounded-sm btn-l font-13 font-600 mt-5 border-0">Sign In</a>
                --}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>

</div>
