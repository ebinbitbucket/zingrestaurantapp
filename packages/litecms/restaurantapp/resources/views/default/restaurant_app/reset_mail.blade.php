<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ Theme::getMetaTitle() }}</title>
    <meta name="description" content="{{ Theme::getMetaDescription() }}">
    <meta name="keywords" content="{{ Theme::getMetaKeyword() }}">
    <meta name="tags" content="{{ Theme::getMetaTag() }}">
    <meta name="viewport"
        content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />

    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <link href="{{ url('themes/client/assets/css/default/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ url('themes/client/assets/css/default/fontawesome-all.min.css') }}" rel="stylesheet">
    <script src="{{ url('themes/client/assets/js/default/jquery.js') }}"></script>

</head>

<body class="theme-light">
    <div id="preloader">
        <div class="spinner-border color-highlight" role="status"></div>
    </div>

    <div id="page">
        {{-- <div class="header header-auto-show header-fixed header-logo-center">
            <a href="{{ url('app/address/' . @$restaurant->id) }}" class="header-title">Address</a>
            <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
            <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                    class="fas fa-sun"></i></a>
            <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                    class="fas fa-moon"></i></a>
        </div> --}}
        {{-- <div id="footer-bar" class="footer-bar-6">
            @include('restaurantapp::default.restaurant_app.footer', ['page' => 'account'])
        </div> --}}
        <div class="page-title page-title-fixed">
            <div>
                <h1>Reset Password</h1>

            </div>
            {{-- <a href="#"
                class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i
                    class="fa fa-moon"></i></a>
            <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark"
                data-toggle-theme><i class="fa fa-lightbulb color-yellow-dark"></i></a>
            <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i
                    class="fa fa-bars"></i></a> --}}
        </div>
        <div class="page-title-clear"></div>

        <div class="page-content pb-3">
            <div class="card card-style bg-theme pb-0">
                <div class="content">
                    <div class="content">
                        <h3>Reset Password</h3>
                        <div class="empty-content-wrap">
                            <p>Tap the button below to reset your customer account password. If you didn't request a new password, you can safely delete this email.
                            </p>
                            <p>
                              <a href="{{$url}}" target="_blank" class="text-center" style="height: 40px;">
                                <button style="background: #00b08c;border: none;color: white;" type="button" class="btn btn-primary text-center">Reset Password</button>
                              </a>
                            </p>
                            @if (!empty($restaurant_info->logo))
                            <a href="#" target="_blank" style="display: inline-block;">
                            <img style="width: 50%;" class="text-center" src="{{ url(@$restaurant_info->defaultImage('logo')) }}" alt="">
                            </a>
                            @endif

                            <p style="margin: 0;">If that doesn't work, copy and paste the following link in your browser:</p>
                             <p style="margin: 0;"><a href="{{$url}}" target="_blank">{{$url}}</a></p>
                             <p style="margin: 0;">Regards,<br>{{ $restaurant_info->name }}</p>
                        </div>
                    </div>
                </div>
            </div>

            {{-- <div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280"
                data-menu-active="nav-welcome">
                @include('restaurantapp::default.restaurant_app.header')
            </div> --}}
        </div>

    </div>
    </div>


    <script src="{{ url('themes/client/assets/js/default/bootstrap.min.js') }}"></script>
    <script src="{{ url('themes/client/assets/js/default/custom.js') }}"></script>
</body>
{{ Session::put('mail_sent','true') }}
</html>
