<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="#" class="header-title"><img src="images/logo.png" style="height: 30px;" alt=""></a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>

    <div class="page-content pb-0">
        <div class="card rounded-0" data-card-height="cover">
            <div class="card-center pl-3">
                <div class="text-center mb-3">
                    <h1 class="font-40 font-800 pb-2">
                        @if (!empty($restaurant->logo))
                            <img src="{{ url(@$restaurant->defaultImage('logo')) }}" style="height: 40px;" alt="">
                        @endif
                    </h1>
                </div>
                <div class="text-left mb-3">
                    <h6>Forgot Password</h6>
                        <p class="mb20">Enter your registered email address and we will send you a reset code.</p>
                </div>

                @if (Session::has('validation_errors'))
                    <div class="row">
                        @foreach (Session::get('validation_errors') as $val_errors)
                            @foreach ($val_errors as $validation)
                                <div class="col-md-12">
                                    <div class="text-danger text-left">
                                        <span><b> Error - </b> {{ $validation }}</span>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                    </div>
                    {{ Session::forget('validation_errors') }}
                @endif
                @if (Session::has('errors'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-danger text-left">
                                <span><b> Error - </b> @php echo Session::get('errors'); @endphp</span>
                            </div>
                        </div>
                    </div>
                    {{ Session::forget('errors') }}
                @endif
                @if (Session::has('mail_sent'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-success text-left">
                                <span><b>Success - </b> Password Reset request has been sent to your email. Please Check
                                    Email</span>
                            </div>
                        </div>
                    </div>
                    {{ Session::forget('mail_sent') }}
                @endif
                {!! Form::vertical_open()
                ->id('reset')
                ->action(guard_url('password/email'))
                ->method('POST') !!}
                {{ csrf_field() }}
                <div class="input-style has-icon input-style-1 input-required">
                    <i class="input-icon fa fa-envelope"></i>
                    <span class="color-highlight">Your Email</span>
                    <em>(required)</em>
                    {!! Form::text('email')
                    ->required()
                    ->value('')
                    ->autocomplete('off')
                    ->placeholder('Enter Your Email')
                    ->raw() !!}
                    {!! Form::hidden('restaurant_id')->value(@$restaurant->id) !!}
                    {!! Form::hidden('is_app')->value(1) !!}
                    {!! Form::hidden('flag')->value('app') !!}
                </div>
                <div class="clearfix"></div>
                <div class="form-group mt-30">
                    {{-- <button class="btn btn-theme" type="submit">Send
                        Password</button> --}}
                    <button class="btn btn-center-l bg-highlight rounded-sm btn-l font-13 font-600 mt-5 border-0"
                        type="submit">Send Password</button>
                </div>
                {!! Form::close() !!}
                <p class="text-small mt20">Back to <a href="{{ url('app/' . $restaurant->id) }}">&nbsp; Login</a></p>

            </div>
        </div>
    </div>

</div>
