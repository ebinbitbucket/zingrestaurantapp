<div id="page">
	 <div class="header header-auto-show header-fixed header-logo-center">
			<a href="{{url('app/'.$restaurant->id)}}" class="header-title">Menu</a>
			<a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
			<a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i class="fas fa-sun"></i></a>
			<a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i class="fas fa-moon"></i></a>
		</div>
		<div id="footer-bar" class="footer-bar-6">
			@include('restaurantapp::default.restaurant_app.footer', ['page' => 'menu'])
		</div>
		<div class="page-title page-title-fixed">
			<div>
				<h1>Menu</h1>
			</div>
			@if (!empty($app->locations))
            @if (count($app->locations) == 1)
                @forelse($app->locations as $data)
                    <a href="{{ @$data['url'] }}?client_id={{hashids_encode(user()->id)}}" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3"
                        style="height: 40px;width: 122px;">Order Now</a>
                    @break
                @empty
                @endif
            @else
                <a href="{{guard_url('app/locations/'.$restaurant->id)}}" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3"
                    style="height: 40px;width: 122px;">Order Now</a>
            @endif

        @endif
			{{-- <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i class="fa fa-moon"></i></a>
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i class="fa fa-lightbulb color-yellow-dark"></i></a> --}}
			{{-- <a href="#" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;">Order Online</a> --}}
			<a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i class="fa fa-bars"></i></a>
		</div>
		<div class="page-title-clear"></div>
		<div class="page-content">
			<div class="card card-style">
				<div class="content mt-0">
					<div class="accordion" id="accordion-1">
						  @php $i=0; 
						  @endphp
						@foreach($restaurant_category as $category)
                        @if(isset($restaurant_menu[$category->id]) && $restaurant_menu[$category->id]->count() > 1)

						<div class="list-group list-custom-small list-icon-0">
							<a data-toggle="collapse" href="#collapse-{{$category->id}}" aria-expanded="{{$i == 0 ? 'true': 'false'}}">
								<span class="font-16">{{@$category->name}}</span>
								<i class="fa fa-angle-down"></i>
							</a>
						</div>
						<div class="collapse {{$i == 0 ? 'show': ''}}" id="collapse-{{$category->id}}" data-parent="#accordion-1" style="">
							@foreach($restaurant_menu[$category->id] as $menu)
							<div class="row mb-0 mt-3">
								<div class="col-3 pr-0">
									 @if(!empty($menu->image))
		                                <a href="#"><img width="80px" src="{{url($menu->defaultImage('image','sm'))}}" class="rounded-sm shadow-xl img-fluid"></a>
		                            @else
		                            <a href="#"><img width="80px" src="{{theme_asset('img/menu/no_dish_xs.jpg')}}" class="rounded-sm shadow-xl img-fluid"></a>
		                            @endif
								</div>
								<div class="col-9">
									<a href="#">
										<h5 class="mb-0">{{@$menu->name}}</h5>
										<span style="color: grey;" class="font-14">{{@$menu->description}}</span>
									</a>
									<h4 class="mt-1 mb-n2 font-800">${{@$menu->price}}</h4>
								</div>
							</div>
							<div class="row mb-0">
								<div class="w-100 divider divider-margins mb-3 mt-3"></div>
							</div>
							@endforeach
						</div>
						@php 
						$i++; 
						@endphp
						@endif
						
						@endforeach
					</div>
				</div>

			</div>
		</div>

	

		<div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
			@include('restaurantapp::default.restaurant_app.header')
		</div> 
	</div>