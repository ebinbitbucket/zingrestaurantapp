@extends('resource.show')

@php
$links['back'] = guard_url('restaurantapp/restaurant_app');
$links['edit'] = guard_url('restaurantapp/restaurant_app') . '/' . $restaurant_app->getRouteKey() . '/edit';
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('restaurantapp::restaurant_app.title.main') !!}
@stop

@section('sub.title') 
{!! __('restaurantapp::restaurant_app.title.show') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('$restaurantapp/restaurant_app')!!}">{!! __('restaurantapp::restaurant_app.name') !!}</a></li>
  <li>{{ __('app.show') }}</li>
@stop

@section('tabs') 
@stop

@section('tools') 
    <a href="{!!guard_url('$restaurantapp/restaurant_app')!!}" rel="tooltip" class="btn btn-white btn-round btn-simple btn-icon pull-right add-new" data-original-title="" title="">
            <i class="fa fa-chevron-left"></i>
    </a>
@stop

@section('content') 
    @include('restaurantapp::restaurant_app.partial.show', ['mode' => 'show'])
@stop
