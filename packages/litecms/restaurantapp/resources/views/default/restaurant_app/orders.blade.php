<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="{{ url('app/address/' . @$restaurant->id) }}" class="header-title">Orders</a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>
    <div id="footer-bar" class="footer-bar-6">
        @include('restaurantapp::default.restaurant_app.footer', ['page' => 'account'])
    </div>
    <div class="page-title page-title-fixed">
        <div>
            <h1>Orders</h1>

        </div>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i
                class="fa fa-moon"></i></a>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i
                class="fa fa-lightbulb color-yellow-dark"></i></a>
        {{-- <a href="#"
            class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;">Order Online</a>
        --}}
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i
                class="fa fa-bars"></i></a>
    </div>
    <div class="page-title-clear"></div>

    <div class="page-content pb-3">
        <div class="card card-style bg-theme pb-0">
            <div class="content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-controls tabs-round tab-animated tabs-small tabs-rounded shadow-xl"
                            data-tab-items="3" data-tab-active="bg-red-dark color-white">
                            <a href="{{ guard_url('app/orders/currorders/'.$restaurant->id) }}"
                                {{ Request::is('client/app/orders/currorders/'.$restaurant->id) ? 'data-tab-active=""' : '' }}
                                data-tab="tab-1">Current</a>
                            <a href="{{ guard_url('app/orders/pastorders/'.$restaurant->id) }}"
                                {{ Request::is('client/app/orders/pastorders/'.$restaurant->id) ? 'data-tab-active=""' : '' }}
                                data-tab="tab-2">Past</a>
                            <a href="{{ guard_url('app/orders/favourites/' .$restaurant->id) }}"
                                {{ Request::is('client/app/orders/favourites/'.$restaurant->id) ? 'data-tab-active=""' : '' }}
                                data-tab="tab-3">Favorite</a>
                        </div>
                        <div class="clearfix mb-3"></div>
                        <div class="content">
                            @forelse($orders as $order)
                            @php 
                            // $restaurant_fav = Restaurantapp::getFavouriteStatus($order->id);
                            if($head == 'Favorite Orders'){
                                $restaurant_fav = 1; 
                            }

                            @endphp
                                <div class="card border-1">
                                    <div class="content mb-1">
                                        <figure class="float-left">
                                            <a href="#">
                                                <img width="100" height="100" class="img-fluid border border-transparent"
                                                    src="{{ url($order->restaurant->defaultImage('logo')) }}" />
                                            </a>
                                        </figure>
                                        <div>
                                            <h5 class="font-16 font-600">#{{ @$order->id }} -
                                                {{ @$order->restaurant->name }} </h5>
                                                <h6><b>{{$order->order_type}} Order</b><button style="border: none;background: white;" value="{{$order->id}}" id="addFav" class="add-fav-btn {{$restaurant_fav==1 ? 'active':''}}">
                                                    @if($restaurant_fav == 1)
                                                    <i id="favIcon" class="fa fa-heart" style="color: red;"></i>
                                                    @else
                                                    <i id="favIcon" class="fa fa-heart"></i>
                                                    @endif
                                                
                                                </button></h6>
                                            <span style="color:grey;"
                                                class="font-14 float-left">{{ $order->restaurant->address }}</span>
                                            <span style="color:grey;" class="font-14 float-left">{{ $order->order_status }}
                                                on {{ date('D, M d, h:i A', strtotime($order->ready_time)) }} <i
                                                    class="fa fa-check-circle text-success"></i></span>
                                            <br>

                                        </div>
                                        @if ($order->order_status != 'New Orders' && $order->order_status != 'Scheduled' && $order->order_status != 'Preparing')
                                            <div class="button-wrap mt-20">
                                                <a href="{{ guard_url('cart/reorder/' . $order->getRoutekey()) }}"
                                                    class="btn btn-theme">Reorder</a>
                                            </div>
                                        @endif
                                        <div class="menu-items meta-infos float-left">
                                            @foreach ($order->detail as $order_det)
                                                <div class="info pl-0 font-14 font-600">
                                                    @if ($order_det->menu_addon == 'menu' && !empty($order_det->menu))
                                                        {{ $order_det->menu->name }}
                                                    {{-- @elseif(!empty($order_det->addon))
                                                        {{ $order_det->addon->name }} --}}
                                                    @endif
                                                    - {{ $order_det->quantity }} x ${{ $order_det->unit_price }}
                                                </div>
                                            @endforeach
                                        </div>
                                        <div class="float-left font-14 font-600">&nbsp;&nbsp;&nbsp;Total Paid: ${{ $order->total }}</br>Loyalty Points:
                                            @if ($order->loyalty_points == '') 0 @else
                                                    {{ number_format($order->loyalty_points, 2) }} @endif
                                            </div>
                                        <div class="divider mb-3"></div>
                                    </div>
                                </div>
                            @empty
                            <div class="empty-content-wrap">
                                <h2>No orders present at the moment!</h2>
                                <img class="text-center" src="{{theme_asset('img/order-empty.svg')}}" alt="">
                            </div>
                            @endif
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
                @include('restaurantapp::default.restaurant_app.header')
            </div>
        </div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#addFav").click(function(event){
            order_id = $(this).val();
            // $("#favIcon").css("color", "");
            add_fav(order_id);
        });

            function add_fav(order_id){
        var fav_status = $('.add-fav-btn').hasClass('active');
        if(fav_status == false)
            fav_status = 'on';
        else
            fav_status = 'off';

            $.getJSON({
                  type: 'GET',
                  url : "{{guard_url('app/order/addtofavourite')}}"+'/'+order_id+'/'+fav_status,
                  success: function(data) { 
                    if(fav_status == 'on'){
                        $("#favIcon").css("color", "red");
                         $('.add-fav-btn').addClass('active');

                    }
                     else{
                        $("#favIcon").css("color", "black");
                       $('.add-fav-btn').removeClass('active');
                     }
                             
                  },
                  error: function(msg) { 
                   
                  return false;
                  }
              });
     }
     
    });

     </script>
