
<!DOCTYPE doctype html> 
<html lang="en">
   <head>
      <style>@import url('https://fonts.googleapis.com/css?family=Lato:400,700&display=swap'); html { color: #888; font-family: 'Lato', sans-serif; } </style>
   </head>
   <body bgcolor="#EFF2F7" style="background-color: #EFF2F7; padding: 30px 0; margin: 0; width: 100%;">
      <table align="center" border="0" cellpadding="0" cellspacing="0" style="width: 600px; min-width: 600px; border-radius: 6px 6px 6px 6px; margin: 0 auto; margin-top: 30px;" width="600">
         <tbody>
            <tr>
               <td bgcolor="#40b659" class="space" height="40" style="border-radius: 6px 6px 0px 0px;"> </td>
            </tr>
            <tr>
               <td align="center" bgcolor="#40b659" class="frame" style="padding: 0px 0px;">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                     <tbody>
                        <tr>
                           {{-- <td style="text-align: center;"><img src="https://zingmyorder.com/img/logo-round-white.png" style="display: inline-block; width: 80px;"></td> --}}
                           @if (!empty($restaurant->logo))
                           <td style="text-align: center;"><img src="{{ url(@$restaurant->defaultImage('logo')) }}" style="display: inline-block; width: 80px;"></td>
                           @endif
                        </tr>
                        <tr>
                           <td height="10" style="line-height:10px; height:10px;"> </td>
                        </tr>
                        <tr>
                           <td align="center" class="bodyTitle" style="font-size: 28px; line-height: 28px; text-align: center; font-weight: 600; text-decoration: none;"> <span style="color: #fff;">Enquiry From App!</span> </td>
                        </tr>
                        <tr>
                           <td height="40" style="line-height:40px; height:40px;"> </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            
            <tr>
               <td align="center" bgcolor="#ffffff" class="frame" style="padding: 0px 40px;">
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                     <tbody>
                        <tr>
                           <td height="30" style="line-height:30px; height:30px;"> </td>
                        </tr>
                        <tr>
                           <td align="center" class="bodyTitle" style="font-size: 20px; line-height: 28px; color: #3c4858; text-align: left; font-weight: 700; font-style: normal; text-decoration: none;"> <span style="color: #3c4858;"> Hi  {{$restaurant['name']}}, </span> </td>
                        </tr>
                      
                        <tr>
                           <td align="center" class="bodyText" id="content-8" style="color: #3c4858; font-size: 14px; line-height: 24px; font-weight: 300; text-align: left;"><p style="margin-top: 0px; margin-bottom: 10px;">You have an enquiry from your app.</p></td>
                        </tr>
                        <tr>
                           <td height="10" style="line-height:10px; height:10px;">
                           
                            <br>
                           </td>
                        </tr>
                        <tr>
                           {{-- <td align="center" class="bodyText" id="content-8" style="color: #3c4858; font-size: 14px; line-height: 24px; font-weight: 300; text-align: left;"><a href="{{env('BASE_URL')}}/restaurants/.{{ $restaurant['slug'] }}" style="font-weight: 400; font-size: 14px; border-radius: 6px; padding: 5px 20px; display: inline-block; background-color: #28a745; color: #fff; text-decoration: none;">View Restaurant</a></td> --}}
                        </tr>

                        <tr>
                            <td bgcolor="#ffffff">
                               <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                  <tbody>
                                     <tr>
                                        <td bgcolor="#ffffff" class="space" style="border-radius: 0px 0px 6px 6px;  padding-left: 10px; padding-top: 10px;">
                                           <div style="background-color: #f2f4f8; padding: 20px; border-radius: 6px;">
                                              <div style="font-weight: 600; font-size: 24px; display: block; margin-bottom: 5px; color: #212529;">{{ $attributes['cf_name'] }}</div>
                                              <div style="font-weight: 300; font-size: 15px; color: #212529;">Client Name</div>
                                           </div>
                                        </td>
                                     </tr>
                                     <tr>
                                        <td bgcolor="#ffffff" class="space" style="border-radius: 0px 0px 6px 6px;  padding-left: 10px;padding-top: 10px;">
                                          <div style="background-color: #f2f4f8; padding: 20px; border-radius: 6px;">
                                             <div style="font-weight: 600; font-size: 24px; display: block; margin-bottom: 5px; color: #212529;">{{ $attributes['cf_email'] }}</div>
                                             <div style="font-weight: 300; font-size: 15px; color: #212529;">Client Email</div>
                                          </div>
                                       </td>
                                     </tr>
                                     <tr>
                                        <td bgcolor="#ffffff" class="space" style="border-radius: 0px 0px 6px 6px;  padding-left: 10px;padding-top: 10px;">
                                          <div style="background-color: #f2f4f8; padding: 20px; border-radius: 6px;">
                                             <div style="font-weight: 600; font-size: 18px; display: block; margin-bottom: 5px; color: #212529;text-transform: capitalize;">{{ $attributes['cf_message'] }}</div>
                                             <div style="font-weight: 300; font-size: 15px; color: #212529;">Message</div>
                                          </div>
                                       </td>
                                     </tr>
                                  </tbody>
                               </table>
                            </td>
                         </tr>

                  
                     
                        <!-- <tr>
                           <td height="20" style="line-height:20px; height:20px;"> </td>
                        </tr> -->
                       <!--  <tr>
                           <td bgcolor="#ffffff">
                              <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                 <tbody>
                                    <tr>
                                       <td bgcolor="#ffffff" class="space" width="100%" style="border-radius: 0px 0px 6px 6px; padding-right: 10px;">
                                          <div style="background-color: #f2f4f8; padding: 20px; border-radius: 6px;">
                                             <div style="font-weight: 600; font-size: 22px; display: block; margin-bottom: 5px; color: #212529;">SEO & Content</div>
                                             <div style="font-weight: 300; font-size: 14px; line-height: 24px; color: #212529; margin-bottom: 10px;">Your dedicated Zing agent posts 1 - 3 SEO rich content across the internet to boost onsite and off site traffic</div>
                                             <a href="#" style="font-weight: 400; font-size: 14px; border-radius: 6px; padding: 10px 20px; display: inline-block; background-color: #28a745; color: #fff; text-decoration: none;">View Posts</a>
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr> -->
                        <!-- <tr>
                           <td height="20" style="line-height:20px; height:20px;"> </td>
                        </tr>
                        <tr>
                           <td bgcolor="#ffffff">
                              <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                 <tbody>
                                    <tr>
                                       <td bgcolor="#ffffff" class="space" width="100%" style="border-radius: 0px 0px 6px 6px; padding-right: 10px;">
                                          <div style="background-color: #f2f4f8; padding: 20px; border-radius: 6px;">
                                             <div></div>
                                             <div style="font-weight: 600; font-size: 22px; display: block; margin-bottom: 5px; color: #212529;">Re Marketing</div>
                                             <div style="font-weight: 600; font-size: 30px; display: block; margin-bottom: 5px; color: #212529;">1,035</div>
                                             <div style="font-weight: 300; font-size: 15px; color: #212529; font-weight: 500;">Touch points with customers</div>
                                          </div>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </td>
                        </tr> -->
                        
                     </tbody>
                  </table>
               </td>
            </tr>
            <tr>
               <td bgcolor="#ffffff" class="space" height="40" style="border-radius: 0px 0px 6px 6px;"> </td>
            </tr>

            
            
         </tbody>
      </table>
      <table align="center" border="0" cellpadding="0" cellspacing="0" class="devicewidth" style="width: 600px; min-width: 600px; margin-top: 30px;" width="600">
         <tbody>
            <tr>
               <td align="center" class="bodyTitle" id="content-31" style="line-height: 24px; font-size: 14px; color: #8492a6;">
                  <p style="margin-top: 0px; margin-bottom: 10px;"> Copyrignts © 2020 Zing My Order. All Rights Reserved </p>
               </td>
            </tr>
         </tbody>
      </table>
   </body>
</html>