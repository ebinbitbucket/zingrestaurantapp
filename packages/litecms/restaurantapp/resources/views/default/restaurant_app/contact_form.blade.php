 <div class="content">
     <h1>Contact Us</h1>
     <p>
         {{ $app->contact_info }}
     </p>
     <div class="formValidationError bg-red-dark mb-4 rounded-sm shadow-xl" id="contactNameFieldError">
         <p class="text-center text-uppercase p-2 color-white font-900 ">Name is required!</p>
     </div>
     <div class="formValidationError bg-red-dark mb-4 rounded-sm shadow-xl" id="contactEmailFieldError">
         <p class="text-center text-uppercase p-2 color-white font-900">Mail address required!</p>
     </div>
     <div class="formValidationError bg-red-dark mb-4 rounded-sm shadow-xl" id="contactEmailFieldError2">
         <p class="text-center text-uppercase p-2 color-white font-900">Mail address must be valid!</p>
     </div>
     <div class="formValidationError bg-red-dark mb-4 rounded-sm shadow-xl" id="contactMessageTextareaError">
         <p class="text-center text-uppercase p-2 color-white font-900">Message field is empty!</p>
     </div>
     <div class="formSuccessMessageWrap card card-style">
         <div class="shadow-l rounded-m bg-gradient-green1 mr-n1 ml-n1 mb-n1 ">
             <h1 class="color-white text-center pt-4"><i
                     class="fa fa-check-circle fa-3x shadow-s scale-box rounded-circle"></i></h1>
             <h2 class="color-white bold text-center pt-3">Message Sent</h2>
             <p class="color-white pb-4 text-center mt-n2 mb-0">We'll get back to you shortly.</p>
         </div>
     </div>
     <form action="{{ url('app/sendmail/' . $restaurant->id) }}" method="post" class="contactForm" id="contactForm">
         <fieldset>
             <div class="form-field form-name">
                 <label class="contactNameField color-theme" for="contactNameField">Name:<span>(required)</span></label>
                 <input type="text" name="cf_name" value="" class="contactField round-small requiredField"
                     id="contactNameField" />
             </div>
             <div class="form-field form-email">
                 <label class="contactEmailField color-theme"
                     for="contactEmailField">Email:<span>(required)</span></label>
                 <input type="text" name="cf_email" value=""
                     class="contactField round-small requiredField requiredEmailField" id="contactEmailField" />
             </div>
             <div class="form-field form-text">
                 <label class="contactMessageTextarea color-theme"
                     for="contactMessageTextarea">Message:<span>(required)</span></label>
                 <textarea name="cf_message" class="contactTextarea round-small requiredField"
                     id="contactMessageTextarea"></textarea>
             </div>
             <div class="form-button">
                 <input type="submit"
                     class="btn bg-highlight text-uppercase font-600 font-12 btn-full rounded-s  shadow-xl contactSubmitButton"
                     value="Send Message" data-formId="contactForm" />
             </div>
         </fieldset>
     </form>

 </div>
