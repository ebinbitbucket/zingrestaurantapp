<a href="{{guard_url('app/menu/'.@$restaurant->id)}}" class="{{$page == 'menu' ? 'active-nav' : ''}}"><i class="fa fa-utensils"></i><span>Menu</span></a>
<a href="{{guard_url('app/locations/'.@$restaurant->id)}}" class="{{$page == 'locations' ? 'active-nav' : ''}}"><i class="fa fa-store"></i><span>Contact</span></a>
<a href="{{guard_url('app/home/'.@$restaurant->id)}}" class="circle-nav" class="{{$page == 'home' ? 'active-nav' : ''}}"><i class="fa fa-home"></i><span>Welcome</span></a>
<a href="{{guard_url('app/contact/'.@$restaurant->id)}}" class="{{$page == 'contact' ? 'active-nav' : ''}}"><i class="fa fa-address-book"></i><span>Hours</span></a>
<a href="{{guard_url('app/account/'.@$restaurant->id)}}" class="{{$page == 'account' ? 'active-nav' : ''}}"><i class="fa fa-user"></i><span>My Account</span></a>
{{-- <a href="{{url('app/gallery/'.$restaurant->id)}}" class="{{$page == 'gallery' ? 'active-nav' : ''}}"><i class="fa fa-images"></i><span>Images</span></a> --}}
