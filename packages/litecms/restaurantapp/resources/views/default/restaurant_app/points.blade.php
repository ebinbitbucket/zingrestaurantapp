<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="{{ url('app/address/' . @$restaurant->id) }}" class="header-title">Points</a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>
    <div id="footer-bar" class="footer-bar-6">
        @include('restaurantapp::default.restaurant_app.footer', ['page' => 'account'])
    </div>
    <div class="page-title page-title-fixed">
        <div>
            <h1>Points</h1>

        </div>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light" data-toggle-theme><i
                class="fa fa-moon"></i></a>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i
                class="fa fa-lightbulb color-yellow-dark"></i></a>
        {{-- <a href="#"
            class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3" style="height: 40px;">Order Online</a>
        --}}
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i
                class="fa fa-bars"></i></a>
    </div>
    <div class="page-title-clear"></div>

    <div class="page-content pb-3">
        <div class="card card-style bg-theme pb-0">
            <div class="content">
                <div class="content">
                    <h3>Loyalty Points</h3>
                    <div class="empty-content-wrap">
                             <p>You have <span style="color:#2dce89;">{{number_format($points,2)}}</span> Total Points</p>
					   <p>Your points can be redeemed for <span style="color:#2dce89;">${{number_format($points/2000,2)}}</span></p>
                        <img class="text-center" src="{{ theme_asset('img/order-empty.svg') }}" alt="">
                    </div>
                </div>
            </div>
        </div>

        <div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
            @include('restaurantapp::default.restaurant_app.header')
        </div>
    </div>

</div>
</div>
