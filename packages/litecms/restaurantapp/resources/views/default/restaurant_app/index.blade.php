@extends('resource.index')
@php
$links['create'] = guard_url('restaurantapp/restaurant_app/create');
$links['search'] = guard_url('restaurantapp/restaurant_app');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! __('restaurantapp::restaurant_app.title.main') !!}
@stop

@section('sub.title') 
{!! __('restaurantapp::restaurant_app.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{!!guard_url('/')!!}">{{ __('app.home') }}</a></li>
  <li><a href="{!!guard_url('restaurantapp/restaurant_app')!!}">{!! __('restaurantapp::restaurant_app.name') !!}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('restaurantapp::restaurant_app.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {!!$restaurant_apps->links()!!}
@stop

@section('script')

@stop

@section('style')

@stop 
