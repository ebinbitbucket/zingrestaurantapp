<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="{{ url('app/' . $restaurant->id) }}" class="header-title">
            <img src="{{ url(@$restaurant->defaultImage('logo')) }}" class="logo logo-color" style="height: 40px;"
                alt="">
            <img src="{{ url(@$restaurant->defaultImage('logo')) }}" class="logo logo-white" style="height: 40px;"
                alt="">
        </a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>
    <div id="footer-bar" class="footer-bar-6">
        @include('restaurantapp::default.restaurant_app.footer', ['page' => 'home'])
    </div>
    <div class="page-title page-title-fixed">
        <div>
            <h1>
                @if (!empty($restaurant->logo))
                    <img src="{{ url(@$restaurant->defaultImage('logo')) }}" class="logo logo-color"
                        style="height: 50px;" alt="">
                    <img src="{{ url(@$restaurant->defaultImage('logo')) }}" class="logo logo-white"
                        style="height: 50px;" alt="">
                @endif
            </h1>
        </div>

        {{-- <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-light"data-toggle-theme><i class="fa fa-moon"></i></a>
        <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme show-on-theme-dark" data-toggle-theme><i class="fa fa-lightbulb color-yellow-dark"></i></a> --}}
        @if (!empty($app->locations))
            @if (count($app->locations) == 1)
                @forelse($app->locations as $data)
                    <a href="{{ @$data['url'] }}?client_id={{hashids_encode(user()->id)}}" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3"
                        style="height: 40px;width: 122px;">Order Now</a>
                    @break
                @empty
                @endif
            @else
                <a href="{{guard_url('app/locations/'.$restaurant->id)}}" class="btn btn-full btn-sm rounded-s mt-2 font-600 bg-highlight mr-3"
                    style="height: 40px;width: 122px;">Order Now</a>
            @endif

        @endif
            <a href="#" class="page-title-icon shadow-xl bg-theme mt-2 color-theme" data-menu="menu-main"><i
                    class="fa fa-bars"></i></a>
        </div>

        <div class="page-title-clear"></div>
        <div class="page-content">
            <div class="content mt-0">
                <div class="single-slider owl-has-controls owl-carousel owl-no-dots">
                    @if (!empty(@$app->slider_images))
                        @forelse ($app->slider_images as $key => $image)
                            <div data-card-height="200" class="card mb-0 rounded-m shadow-l"
                                style="background-image: url({!!  url($app->defaultImage('slider_images', 'original', $key)) !!})">
                                <div class="card-bottom text-center mb-3">
                                    <h2 class="color-white font-900 mb-0">Welcome to {{ @$restaurant->name }}</h2>
                                    {{-- <p class="under-heading color-white">{{ $restaurant->name }} Restaurant.</p> --}}
                                </div>
                                <div class="card-overlay bg-gradient"></div>
                            </div>
                        @empty
                        @endif
                        @endif

                    </div>
                </div>
                <div class="content mt-0 mb-15">
                    <h2 class="mb-4">Featured Items</h2>
                </div>
                @if (!empty(@$app->featured_food))
                    <div class="single-center-slider owl-carousel owl-no-dots mb-4">
                        @forelse(@$app->featured_food as $key=>$val)
                            <div>
                                <div class="card m-0 card-style"
                                    style="background-image: url('{{ url($app->defaultImage('featured_food', 'original', $key)) }}')"
                                    data-card-height="180">
                                    <div class="card-top">
                                        <a href="#" class="btn btn-xs bg-red-dark rounded-sm font-700 float-right mt-2 mr-2">Best
                                            Seller</a>
                                    </div>
                                    <div class="card-bottom">
                                    </div>
                                    <div class="card-overlay bg-gradient"></div>
                                </div>
                                <h4>{{ @$val['title'] }}</h4>
                            </div>
                        @empty
                    @endif

                </div>
                @endif


                <div class="card card-style">
                    <div class="content mb-0">
                        <p class="mb-n1 font-600 color-highlight">Handpicked by our Chef </p>
                        <h2 class="mb-4">Top Sellers</h2>
                        @forelse (@$restaurant_menu->take(6) as $menu)
                            <div class="row mb-0">
                                <div class="col-5 pr-0">
                                    @if (!empty($menu->image))
                                        <a href="#"><img width="80px" src="{{ url($menu->defaultImage('image', 'xs')) }}"
                                                class="rounded-sm shadow-xl img-fluid"></a>
                                    @else
                                        <a href="#"><img width="80px" src="{{ theme_asset('img/menu/no_dish_xs.jpg') }}"
                                                class="rounded-sm shadow-xl img-fluid"></a>
                                    @endif

                                </div>
                                <div class="col-7">
                                    <a href="#">
                                        <h5 class="mb-0">{{ @$menu->name }}</h5>
                                        <span style="color: grey;" class="font-14">{{ @$menu->description }}</span>
                                    </a>
                                    <h4 class="mt-1 mb-n2 font-800">${{ @$menu->price }}</h4>
                                </div>
                            </div>
                            <div class="row mb-0">
                                <div class="w-100 divider divider-margins mb-3 mt-3"></div>
                            </div>
                        @empty
                            @endif

                            <a href="{{ guard_url('app/menu/' . @$restaurant->id) }}"
                                class="btn btn-full btn-sm rounded-s mb-3 font-600 bg-highlight mt-3">View All Menus</a>
                        </div>

                    </div>
                </div>

                <div id="menu-main" class="menu menu-box-left rounded-0" data-menu-width="280" data-menu-active="nav-welcome">
                    @include('restaurantapp::default.restaurant_app.header')
                </div> 
                </div>


