<div id="page">
    <div class="header header-auto-show header-fixed header-logo-center">
        <a href="" class="header-title"><img src="{{ theme_asset('img/logo.png') }}" style="height: 30px;" alt=""></a>
        <a href="#" data-menu="menu-main" class="header-icon header-icon-1"><i class="fas fa-bars"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-dark"><i
                class="fas fa-sun"></i></a>
        <a href="#" data-toggle-theme class="header-icon header-icon-4 show-on-theme-light"><i
                class="fas fa-moon"></i></a>
    </div>

    <div class="page-content pb-0">
        <div class="card rounded-0" data-card-height="cover">
            <div class="card-center pl-3">
                <div class="text-center mb-3">
                    <h1 class="font-40 font-800 pb-2">
                        @if (!empty($restaurant->logo))
                            <img src="{{ url(@$restaurant->defaultImage('logo')) }}" style="height: 40px;" alt="">
                        @endif
                    </h1>
                </div>
                @if (Session::has('user_exists'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="text-danger text-left">
                                <span><b> Error - </b> {{ Session::get('user_exists') }}</span>
                            </div>
                        </div>
                    </div>

                    {{ Session::forget('user_exists') }}
                @endif
                @if (Session::has('validation_errors'))
                <div class="row">
                    @foreach (Session::get('validation_errors') as $val_errors)
                        @foreach ($val_errors as $validation)
                            <div class="col-md-12">
                                <div class="text-danger text-left">
                                    <span><b> Error - </b> {{ $validation }}</span>
                                </div>
                            </div>
                        @endforeach
                    @endforeach
                </div>
                {{ Session::forget('validation_errors') }}
                @endif
                @if ($errors->has('password'))
                <div class="text-danger text-left">
                    <span><b> Error - </b> {{ $errors->first('password') }}</span>
                </div>
                @endif
                {!! Form::vertical_open()
                ->id('register')
                ->method('POST')
                ->action(url('app/passwordUpdate')) !!}
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="ml-3 mr-4 mb-n3">
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-lock"></i>
                        <span>Enter New Password</span>
                        <em>(required)</em>
                        {{-- <input type="password" placeholder="Choose a Password">
                        --}}
                        {!! Form::password('password')
                        ->required()
                        ->placeholder('New Password')
                        ->raw() !!}
                    </div>
                    <div class="input-style has-icon input-style-1 input-required">
                        <i class="input-icon fa fa-lock"></i>
                        <span>Confirm your Password</span>
                        <em>(required)</em>
                        {!! Form::password('password_confirmation')
                        ->required()
                        ->placeholder('Confirm Password')
                        ->raw() !!}
                        {!! Form::hidden('restaurant_id')->value(@$restaurant->id) !!}
                        {!! Form::hidden('is_app')->value(1) !!}
                        {!! Form::hidden('client_id')->value(@$client_id) !!}

                    </div>
                    <div class="clearfix"></div>
                    <div class="row pt-3 mb-3">
                        <div class="col-6 text-left">
                        </div>
                        <div class="col-6 text-right">
                            {{-- <a href="{{ url('app/'.@$restaurant->id) }}">Sign In Here</a> --}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <button class="btn btn-center-l bg-highlight rounded-sm btn-l font-13 font-600 mt-5 border-0"
                    type="submit">Update Password</button>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

</div>





{{-- @include('notifications')
        <div class="box" style="min-width: 480px;">
            <div class="logo">
                <a href="{{guard_url('/')}}"><img src="{{theme_asset('img/logo/logo.svg')}}" class="img-responsive center-block" alt="logo" title="Lavalite"></a>
            </div>
            <div class="body">
                <h2>Reset Password ff</h2>
                    <form method="POST" action="{{ route('guard.password.update', ['guard' => 'client']) }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-5 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-7">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-7">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-5 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-7">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
            </div>
            <div class="text-center social-links">
                <h3><span class="login">social login</span></h3>
                    <a href="{!!guard_url('login/facebook')!!}"><i class="fab fa-facebook-square" aria-hidden="true"></i></a>
                    <a href="{!!guard_url('login/twitter')!!}"><i class="fab fa-twitter-square" aria-hidden="true"></i></a>
                    <a href="{!!guard_url('login/google')!!}"><i class="fab fa-google-plus-square" aria-hidden="true"></i></a>
                    <a href="{!!guard_url('login/linkedin')!!}"><i class="fab fa-linkedin" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="stripes-wraper">
            <div class="stripes">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </div>
        </div> --}}
