    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#restaurant_app" data-toggle="tab">{!! trans('restaurantapp::restaurant_app.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#restaurantapp-restaurant_app-edit'  data-load-to='#restaurantapp-restaurant_app-entry' data-datatable='#restaurantapp-restaurant_app-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#restaurantapp-restaurant_app-entry' data-href='{{guard_url('restaurantapp/restaurant_app')}}/{{$restaurant_app->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('restaurantapp-restaurant_app-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('restaurantapp/restaurant_app/'. $restaurant_app->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="restaurant_app">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('restaurantapp::restaurant_app.name') !!} [{!!$restaurant_app->name!!}] </div>
                @include('restaurantapp::admin.restaurant_app.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>