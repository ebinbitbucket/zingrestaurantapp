    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">  {!! trans('restaurantapp::restaurant_app.name') !!}</a></li>
            <div class="box-tools pull-right">
                                @include('restaurantapp::admin.restaurant_app.partial.workflow')
                                <button type="button" class="btn btn-success btn-sm" data-action='NEW' data-load-to='#restaurantapp-restaurant_app-entry' data-href='{{guard_url('restaurantapp/restaurant_app/create')}}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }}</button>
                @if($restaurant_app->id )
                <button type="button" class="btn btn-primary btn-sm" data-action="EDIT" data-load-to='#restaurantapp-restaurant_app-entry' data-href='{{ guard_url('restaurantapp/restaurant_app') }}/{{$restaurant_app->getRouteKey()}}/edit'><i class="fa fa-pencil-square"></i> {{ trans('app.edit') }}</button>
                <button type="button" class="btn btn-danger btn-sm" data-action="DELETE" data-load-to='#restaurantapp-restaurant_app-entry' data-datatable='#restaurantapp-restaurant_app-list' data-href='{{ guard_url('restaurantapp/restaurant_app') }}/{{$restaurant_app->getRouteKey()}}' >
                <i class="fa fa-times-circle"></i> {{ trans('app.delete') }}
                </button>
                @endif
            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('restaurantapp-restaurant_app-show')
        ->method('POST')
        ->files('true')
        ->action(guard_url('restaurantapp/restaurant_app'))!!}
            <div class="tab-content clearfix disabled">
                <div class="tab-pan-title"> {{ trans('app.view') }}   {!! trans('restaurantapp::restaurant_app.name') !!}  [{!! $restaurant_app->name !!}] </div>
                <div class="tab-pane active" id="details">
                    @include('restaurantapp::admin.restaurant_app.partial.entry', ['mode' => 'show'])
                </div>
            </div>
        {!! Form::close() !!}
    </div>