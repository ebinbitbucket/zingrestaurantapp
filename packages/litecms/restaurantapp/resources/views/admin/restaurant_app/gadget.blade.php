@forelse($restaurant_app as $key => $val)
<div class="restaurant_app-gadget-box">
    <p>{!!@$val->name!!}</p>
    <p class="text-muted"><small><i class="ion ion-android-person"></i> {!!@$val->user->name!!} at {!! format_date($val->created_at)!!}</small></p>
</div>
@empty
<div class="restaurant_app-gadget-box">
    <p>No RestaurantApp</p>
</div>
@endif