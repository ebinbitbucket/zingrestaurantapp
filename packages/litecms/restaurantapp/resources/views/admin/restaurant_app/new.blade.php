<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">  {!! trans('restaurantapp::restaurant_app.names') !!} [{!! trans('restaurantapp::restaurant_app.text.preview') !!}]</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-primary btn-sm"  data-action='NEW' data-load-to='#restaurantapp-restaurant_app-entry' data-href='{!!guard_url('restaurantapp/restaurant_app/create')!!}'><i class="fa fa-plus-circle"></i> {{ trans('app.new') }} </button>
        </div>
    </div>
</div>