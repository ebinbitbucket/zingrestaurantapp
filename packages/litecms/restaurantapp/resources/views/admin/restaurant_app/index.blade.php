<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('restaurantapp::restaurant_app.name') !!} <small> {!! trans('app.manage') !!} {!! trans('restaurantapp::restaurant_app.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('restaurantapp::restaurant_app.names') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div id='restaurantapp-restaurant_app-entry'>
    </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                    <li class="{!!(request('status') == '')?'active':'';!!}"><a href="{!!guard_url('restaurantapp/restaurant_app')!!}">{!! trans('restaurantapp::restaurant_app.names') !!}</a></li>
                    <li class="{!!(request('status') == 'archive')?'active':'';!!}"><a href="{!!guard_url('restaurantapp/restaurant_app?status=archive')!!}">Archived</a></li>
                    <li class="{!!(request('status') == 'deleted')?'active':'';!!}"><a href="{!!guard_url('restaurantapp/restaurant_app?status=deleted')!!}">Trashed</a></li>
                    <li class="pull-right">
                    <span class="actions">
                    <!--   
                    <a  class="btn btn-xs btn-purple"  href="{!!guard_url('restaurantapp/restaurant_app/reports')!!}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Reports</span></a>
                    @include('restaurantapp::admin.restaurant_app.partial.actions')
                    -->
                    @include('restaurantapp::admin.restaurant_app.partial.filter')
                    @include('restaurantapp::admin.restaurant_app.partial.column')
                    </span> 
                </li>
            </ul>
            <div class="tab-content">
                <table id="restaurantapp-restaurant_app-list" class="table table-striped data-table">
                    <thead class="list_head">
                        <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset" style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input type="checkbox" id="restaurantapp-restaurant_app-check-all"></th>
                        <th data-field="id">{!! trans('restaurantapp::restaurant_app.label.id')!!}</th>
                    <th data-field="restaurant_id">{!! trans('restaurantapp::restaurant_app.label.restaurant_id')!!}</th>
                    <th data-field="point_percentage">{!! trans('restaurantapp::restaurant_app.label.point_percentage')!!}</th>
                    <th data-field="points">{!! trans('restaurantapp::restaurant_app.label.points')!!}</th>
                    <th data-field="title">{!! trans('restaurantapp::restaurant_app.label.title')!!}</th>
                    <th data-field="subtitle">{!! trans('restaurantapp::restaurant_app.label.subtitle')!!}</th>
                    <th data-field="meta_tags">{!! trans('restaurantapp::restaurant_app.label.meta_tags')!!}</th>
                    <th data-field="meta_keywords">{!! trans('restaurantapp::restaurant_app.label.meta_keywords')!!}</th>
                    <th data-field="meta_description">{!! trans('restaurantapp::restaurant_app.label.meta_description')!!}</th>
                    <th data-field="email">{!! trans('restaurantapp::restaurant_app.label.email')!!}</th>
                    <th data-field="alternate_phone">{!! trans('restaurantapp::restaurant_app.label.alternate_phone')!!}</th>
                    <th data-field="gallery">{!! trans('restaurantapp::restaurant_app.label.gallery')!!}</th>
                    <th data-field="slider_images">{!! trans('restaurantapp::restaurant_app.label.slider_images')!!}</th>
                    <th data-field="featured_food">{!! trans('restaurantapp::restaurant_app.label.featured_food')!!}</th>
                    <th data-field="locations">{!! trans('restaurantapp::restaurant_app.label.locations')!!}</th>
                    <th data-field="contact_info">{!! trans('restaurantapp::restaurant_app.label.contact_info')!!}</th>
                    <th data-field="menus">{!! trans('restaurantapp::restaurant_app.label.menus')!!}</th>
                    <th data-field=")">{!! trans('restaurantapp::restaurant_app.label.)')!!}</th>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

var oTable;
var oSearch;
$(document).ready(function(){
    app.load('#restaurantapp-restaurant_app-entry', '{!!guard_url('restaurantapp/restaurant_app/0')!!}');
    oTable = $('#restaurantapp-restaurant_app-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }], 
        
        "responsive" : true,
        "order": [[1, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('restaurantapp/restaurant_app') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'id'},
            {data :'restaurant_id'},
            {data :'point_percentage'},
            {data :'points'},
            {data :'title'},
            {data :'subtitle'},
            {data :'meta_tags'},
            {data :'meta_keywords'},
            {data :'meta_description'},
            {data :'email'},
            {data :'alternate_phone'},
            {data :'gallery'},
            {data :'slider_images'},
            {data :'featured_food'},
            {data :'locations'},
            {data :'contact_info'},
            {data :'menus'},
            {data :')'},
        ],
        "pageLength": 25
    });

    $('#restaurantapp-restaurant_app-list tbody').on( 'click', 'tr', function () {
        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#restaurantapp-restaurant_app-list').DataTable().row( this ).data();
        $('#restaurantapp-restaurant_app-entry').load('{!!guard_url('restaurantapp/restaurant_app')!!}' + '/' + d.id);
    });

    $('#restaurantapp-restaurant_app-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent'); 
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#restaurantapp-restaurant_app-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }
        
    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#restaurantapp-restaurant_app-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#restaurantapp-restaurant_app-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});
</script>