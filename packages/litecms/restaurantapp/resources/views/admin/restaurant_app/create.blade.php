
    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#details" data-toggle="tab">RestaurantApp</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='CREATE' data-form='#restaurantapp-restaurant_app-create'  data-load-to='#restaurantapp-restaurant_app-entry' data-datatable='#restaurantapp-restaurant_app-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CLOSE' data-load-to='#restaurantapp-restaurant_app-entry' data-href='{{guard_url('restaurantapp/restaurant_app/0')}}'><i class="fa fa-times-circle"></i> {{ trans('app.close') }}</button>
            </div>
        </ul>
        <div class="tab-content clearfix">
            {!!Form::vertical_open()
            ->id('restaurantapp-restaurant_app-create')
            ->method('POST')
            ->files('true')
            ->action(guard_url('restaurantapp/restaurant_app'))!!}
            <div class="tab-pane active" id="details">
                <div class="tab-pan-title">  {{ trans('app.new') }}  [{!! trans('restaurantapp::restaurant_app.name') !!}] </div>
                @include('restaurantapp::admin.restaurant_app.partial.entry', ['mode' => 'create'])
            </div>
            {!! Form::close() !!}
        </div>
    </div>