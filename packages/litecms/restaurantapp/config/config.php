<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'litecms',

    /*
     * Package.
     */
    'package'   => 'restaurantapp',

    /*
     * Modules.
     */
    'modules'   => ['restaurant_app'],

    
    'restaurant_app'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\RestaurantApp::class,
            'table'                 => 'restaurant_app',
            'presenter'             => \Litecms\Restaurantapp\Repositories\Presenter\RestaurantAppPresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'restaurant_id',  'point_percentage',  'points',  'title',  'subtitle',  'meta_tags',  'meta_keywords',  'meta_description',  'email',  'alternate_phone',  'gallery',  'slider_images',  'featured_food',  'locations',  'contact_info',  'menus',  'created_at',  ')',  'updated_at',  'deleted_at'],
            'translatables'         => [],
            'upload_folder'         => 'restaurantapp/restaurant_app',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Litecms',
            'package'   => 'Restaurantapp',
            'module'    => 'RestaurantApp',
        ],

        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "RestaurantApp created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "RestaurantApp completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "RestaurantApp verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "RestaurantApp approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "RestaurantApp published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "RestaurantApp unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "RestaurantApp archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "RestaurantApp deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],

    ],
    'favourite'       => [
        'model' => [
            'model'                 => \Litecms\Restaurantapp\Models\Favourite::class,
            'table'                 => 'favourites',
            'dates'                 => [],
            'fillable'              => ['id',  'user_id',  'favourite_id', 'type' , 'created_at' ,'updated_at' ,'deleted_at'],
            'upload_folder'         => 'restaurant/restaurant',
           
        ],
    ],
    
];
