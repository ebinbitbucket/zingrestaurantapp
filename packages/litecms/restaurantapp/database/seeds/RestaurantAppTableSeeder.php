<?php

namespace Litecms\Restaurantapp;

use DB;
use Illuminate\Database\Seeder;

class RestaurantAppTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('restaurant_apps')->insert([
            
        ]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'restaurantapp.restaurant_app.view',
                'name'      => 'View RestaurantApp',
            ],
            [
                'slug'      => 'restaurantapp.restaurant_app.create',
                'name'      => 'Create RestaurantApp',
            ],
            [
                'slug'      => 'restaurantapp.restaurant_app.edit',
                'name'      => 'Update RestaurantApp',
            ],
            [
                'slug'      => 'restaurantapp.restaurant_app.delete',
                'name'      => 'Delete RestaurantApp',
            ],
            
            // Customize this permissions if needed.
            [
                'slug'      => 'restaurantapp.restaurant_app.verify',
                'name'      => 'Verify RestaurantApp',
            ],
            [
                'slug'      => 'restaurantapp.restaurant_app.approve',
                'name'      => 'Approve RestaurantApp',
            ],
            [
                'slug'      => 'restaurantapp.restaurant_app.publish',
                'name'      => 'Publish RestaurantApp',
            ],
            [
                'slug'      => 'restaurantapp.restaurant_app.unpublish',
                'name'      => 'Unpublish RestaurantApp',
            ],
            [
                'slug'      => 'restaurantapp.restaurant_app.cancel',
                'name'      => 'Cancel RestaurantApp',
            ],
            [
                'slug'      => 'restaurantapp.restaurant_app.archive',
                'name'      => 'Archive RestaurantApp',
            ],
            
        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/restaurantapp/restaurant_app',
                'name'        => 'RestaurantApp',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/restaurantapp/restaurant_app',
                'name'        => 'RestaurantApp',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'restaurant_app',
                'name'        => 'RestaurantApp',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Restaurantapp',
                'module'    => 'RestaurantApp',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'restaurantapp.restaurant_app.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */
        ]);
    }
}
