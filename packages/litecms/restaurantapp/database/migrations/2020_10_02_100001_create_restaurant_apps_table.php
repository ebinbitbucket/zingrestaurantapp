<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

class CreateRestaurantAppsTable extends Migration
{
    /*
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {

        /*
         * Table: restaurant_apps
         */
        Schema::create('restaurant_apps', function ($table) {
            $table->increments('id');
            $table->integer('restaurant_id')->nullable();
            $table->string('point_percentage', 255)->nullable();
            $table->string('points', 255)->nullable();
            $table->text('title')->nullable();
            $table->text('subtitle')->nullable();
            $table->text('meta_tags')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('email')->nullable();
            $table->text('alternate_phone')->nullable();
            $table->text('gallery')->nullable();
            $table->text('slider_images')->nullable();
            $table->text('featured_food')->nullable();
            $table->text('locations')->nullable();
            $table->text('contact_info')->nullable();
            $table->text('menus')->nullable();
            $table->increments(')')->nullable();
            $table->softDeletes();
            $table->nullableTimestamps();
        });
    }

    /*
    * Reverse the migrations.
    *
    * @return void
    */

    public function down()
    {
        Schema::drop('restaurant_apps');
    }
}
