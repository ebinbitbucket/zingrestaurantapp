<?php

use Illuminate\Database\Seeder;

class LitecmsExampleTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('examples')->insert([]);

        DB::table('permissions')->insert([
            [
                'slug'      => 'example.example.view',
                'name'      => 'View Example',
            ],
            [
                'slug'      => 'example.example.create',
                'name'      => 'Create Example',
            ],
            [
                'slug'      => 'example.example.edit',
                'name'      => 'Update Example',
            ],
            [
                'slug'      => 'example.example.delete',
                'name'      => 'Delete Example',
            ],

            // Customize this permissions if needed.
            [
                'slug'      => 'example.example.verify',
                'name'      => 'Verify Example',
            ],
            [
                'slug'      => 'example.example.approve',
                'name'      => 'Approve Example',
            ],
            [
                'slug'      => 'example.example.publish',
                'name'      => 'Publish Example',
            ],
            [
                'slug'      => 'example.example.unpublish',
                'name'      => 'Unpublish Example',
            ],
            [
                'slug'      => 'example.example.cancel',
                'name'      => 'Cancel Example',
            ],
            [
                'slug'      => 'example.example.archive',
                'name'      => 'Archive Example',
            ],

        ]);

        DB::table('menus')->insert([

            [
                'parent_id'   => 1,
                'key'         => null,
                'url'         => 'admin/example/example',
                'name'        => 'Example',
                'description' => null,
                'icon'        => 'fa fa-newspaper-o',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 2,
                'key'         => null,
                'url'         => 'user/example/example',
                'name'        => 'Example',
                'description' => null,
                'icon'        => 'icon-book-open',
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

            [
                'parent_id'   => 3,
                'key'         => null,
                'url'         => 'example',
                'name'        => 'Example',
                'description' => null,
                'icon'        => null,
                'target'      => null,
                'order'       => 190,
                'status'      => 1,
            ],

        ]);

        DB::table('settings')->insert([
            // Uncomment  and edit this section for entering value to settings table.
            /*
            [
                'pacakge'   => 'Example',
                'module'    => 'Example',
                'user_type' => null,
                'user_id'   => null,
                'key'       => 'example.example.key',
                'name'      => 'Some name',
                'value'     => 'Some value',
                'type'      => 'Default',
                'control'   => 'text',
            ],
            */]);
    }
}
