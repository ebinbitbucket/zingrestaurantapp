Lavalite package that provides example management facility for the cms.

## Installation

Begin by installing this package through Composer. Edit your project's `composer.json` file to require `litecms/example`.

    "litecms/example": "dev-master"

Next, update Composer from the Terminal:

    composer update

Once this operation completes execute below cammnds in command line to finalize installation.

    Litecms\Example\Providers\ExampleServiceProvider::class,

And also add it to alias

    'Example'  => Litecms\Example\Facades\Example::class,

## Publishing files and migraiting database.

**Migration and seeds**

    php artisan migrate
    php artisan db:seed --class=Litecms\\ExampleTableSeeder

**Publishing configuration**

    php artisan vendor:publish --provider="Litecms\Example\Providers\ExampleServiceProvider" --tag="config"

**Publishing language**

    php artisan vendor:publish --provider="Litecms\Example\Providers\ExampleServiceProvider" --tag="lang"

**Publishing views**

    php artisan vendor:publish --provider="Litecms\Example\Providers\ExampleServiceProvider" --tag="view"


## Usage


