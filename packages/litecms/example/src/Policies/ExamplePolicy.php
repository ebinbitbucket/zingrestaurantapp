<?php

namespace Litecms\Example\Policies;

use Litepie\User\Contracts\UserPolicy;
use Litecms\Example\Models\Example;

class ExamplePolicy
{

    /**
     * Determine if the given user can view the example.
     *
     * @param UserPolicy $user
     * @param Example $example
     *
     * @return bool
     */
    public function view(UserPolicy $user, Example $example)
    {
        if ($user->canDo('example.example.view') && $user->isAdmin()) {
            return true;
        }

        return $example->user_id == user_id() && $example->user_type == user_type();
    }

    /**
     * Determine if the given user can create a example.
     *
     * @param UserPolicy $user
     * @param Example $example
     *
     * @return bool
     */
    public function create(UserPolicy $user)
    {
        return  $user->canDo('example.example.create');
    }

    /**
     * Determine if the given user can update the given example.
     *
     * @param UserPolicy $user
     * @param Example $example
     *
     * @return bool
     */
    public function update(UserPolicy $user, Example $example)
    {
        if ($user->canDo('example.example.edit') && $user->isAdmin()) {
            return true;
        }

        return $example->user_id == user_id() && $example->user_type == user_type();
    }

    /**
     * Determine if the given user can delete the given example.
     *
     * @param UserPolicy $user
     * @param Example $example
     *
     * @return bool
     */
    public function destroy(UserPolicy $user, Example $example)
    {
        return $example->user_id == user_id() && $example->user_type == user_type();
    }

    /**
     * Determine if the given user can verify the given example.
     *
     * @param UserPolicy $user
     * @param Example $example
     *
     * @return bool
     */
    public function verify(UserPolicy $user, Example $example)
    {
        if ($user->canDo('example.example.verify')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the given user can approve the given example.
     *
     * @param UserPolicy $user
     * @param Example $example
     *
     * @return bool
     */
    public function approve(UserPolicy $user, Example $example)
    {
        if ($user->canDo('example.example.approve')) {
            return true;
        }

        return false;
    }

    /**
     * Determine if the user can perform a given action ve.
     *
     * @param [type] $user    [description]
     * @param [type] $ability [description]
     *
     * @return [type] [description]
     */
    public function before($user, $ability)
    {
        if ($user->isSuperuser()) {
            return true;
        }
    }
}
