<?php

namespace Litecms\Example\Forms;

class Example
{
    /**
     * Variable to store form configuration.
     *
     * @var collection
     */
    protected $form;

    /**
     * Variable to store form configuration.
     *
     * @var collection
     */
    protected $element;

    /**
     * Initialize the form.
     *
     * @return null
     */
    public function __construct()
    {
        $this->setForm();
    }

    /**
     * Return form elements.
     *
     * @return array.
     */
    public function form($data = [])
    {
        return $this->form;
    }

    /**
     * Return form elements.
     *
     * @return array.
     */
    public function fields($groupBy = ['group', 'section'])
    {
        $item = collect($this->form->get('fields'));
        if (is_array($groupBy)) {
            return $item->groupBy($groupBy);
        }
        return $item;
    }

    /**
     * Return form elements.
     *
     * @return array.
     */
    public function groups()
    {
        return $this->form->get('groups');
    }

    /**
     * Sets the form and form elements.
     * @return null.
     */
    public function setForm()
    {
        $this->form = collect([
            'status' => [
                [
                    'name' => _('New'),
                    'status' => 'new',
                ],
                [
                    'name' => __('Open'),
                    'status' => 'open',
                ],
                [
                    'name' => __('Close'),
                    'status' => 'active',
                ],
                [
                    'name' => __('Exit'),
                    'status' => 'exit',
                ],
            ],
            'form' => [
                'store' => [],
                'update' => [],
            ],
            'groups' => [
                'main' => trans('example::example.tab.main'),
            ],
            'fields' => [
                'name' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.name'),
                    "placeholder" => trans('example::example.placeholder.name'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'email' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.email'),
                    "placeholder" => trans('example::example.placeholder.email'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'color' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.color'),
                    "placeholder" => trans('example::example.placeholder.color'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'date' => [
                    "type" => 'date_picker',
                    "label" => trans('example::example.label.date'),
                    "placeholder" => trans('example::example.placeholder.date'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'datetime' => [
                    "type" => 'date_time_picker',
                    "label" => trans('example::example.label.datetime'),
                    "placeholder" => trans('example::example.placeholder.datetime'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'file' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.file'),
                    "placeholder" => trans('example::example.placeholder.file'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'files' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.files'),
                    "placeholder" => trans('example::example.placeholder.files'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'image' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.image'),
                    "placeholder" => trans('example::example.placeholder.image'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'images' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.images'),
                    "placeholder" => trans('example::example.placeholder.images'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'month' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.month'),
                    "placeholder" => trans('example::example.placeholder.month'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'password' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.password'),
                    "placeholder" => trans('example::example.placeholder.password'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'range' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.range'),
                    "placeholder" => trans('example::example.placeholder.range'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'search' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.search'),
                    "placeholder" => trans('example::example.placeholder.search'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'tel' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.tel'),
                    "placeholder" => trans('example::example.placeholder.tel'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'time' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.time'),
                    "placeholder" => trans('example::example.placeholder.time'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'url' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.url'),
                    "placeholder" => trans('example::example.placeholder.url'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'week' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.week'),
                    "placeholder" => trans('example::example.placeholder.week'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'date_picker' => [
                    "type" => 'date_picker',
                    "label" => trans('example::example.label.date_picker'),
                    "placeholder" => trans('example::example.placeholder.date_picker'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'time_picker' => [
                    "type" => 'time_picker',
                    "label" => trans('example::example.label.time_picker'),
                    "placeholder" => trans('example::example.placeholder.time_picker'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'date_time_picker' => [
                    "type" => 'date_time_picker',
                    "label" => trans('example::example.label.date_time_picker'),
                    "placeholder" => trans('example::example.placeholder.date_time_picker'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'radios' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.radios'),
                    "placeholder" => trans('example::example.placeholder.radios'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'checkboxes' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.checkboxes'),
                    "placeholder" => trans('example::example.placeholder.checkboxes'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'switch' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.switch'),
                    "placeholder" => trans('example::example.placeholder.switch'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'select' => [
                    "type" => 'radios',
                    "label" => trans('example::example.label.select'),
                    "placeholder" => trans('example::example.placeholder.select'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'model_select' => [
                    "type" => 'radios',
                    "label" => trans('example::example.label.model_select'),
                    "placeholder" => trans('example::example.placeholder.model_select'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'tinyints' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.tinyints'),
                    "placeholder" => trans('example::example.placeholder.tinyints'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'smallints' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.smallints'),
                    "placeholder" => trans('example::example.placeholder.smallints'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'mediumints' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.mediumints'),
                    "placeholder" => trans('example::example.placeholder.mediumints'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'ints' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.ints'),
                    "placeholder" => trans('example::example.placeholder.ints'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'bigints' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.bigints'),
                    "placeholder" => trans('example::example.placeholder.bigints'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'decimals' => [
                    "type" => 'decimal',
                    "label" => trans('example::example.label.decimals'),
                    "placeholder" => trans('example::example.placeholder.decimals'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'floats' => [
                    "type" => 'decimal',
                    "label" => trans('example::example.label.floats'),
                    "placeholder" => trans('example::example.placeholder.floats'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'doubles' => [
                    "type" => 'decimal',
                    "label" => trans('example::example.label.doubles'),
                    "placeholder" => trans('example::example.placeholder.doubles'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'reals' => [
                    "type" => 'decimal',
                    "label" => trans('example::example.label.reals'),
                    "placeholder" => trans('example::example.placeholder.reals'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'bits' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.bits'),
                    "placeholder" => trans('example::example.placeholder.bits'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'booleans' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.booleans'),
                    "placeholder" => trans('example::example.placeholder.booleans'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'dates' => [
                    "type" => 'date_picker',
                    "label" => trans('example::example.label.dates'),
                    "placeholder" => trans('example::example.placeholder.dates'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'datetimes' => [
                    "type" => 'date_time_picker',
                    "label" => trans('example::example.label.datetimes'),
                    "placeholder" => trans('example::example.placeholder.datetimes'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'timestamps' => [
                    "type" => 'date_time_picker',
                    "label" => trans('example::example.label.timestamps'),
                    "placeholder" => trans('example::example.placeholder.timestamps'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'times' => [
                    "type" => 'time_picker',
                    "label" => trans('example::example.label.times'),
                    "placeholder" => trans('example::example.placeholder.times'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'years' => [
                    "type" => 'numeric',
                    "label" => trans('example::example.label.years'),
                    "placeholder" => trans('example::example.placeholder.years'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'chars' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.chars'),
                    "placeholder" => trans('example::example.placeholder.chars'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'varchars' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.varchars'),
                    "placeholder" => trans('example::example.placeholder.varchars'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'tinytexts' => [
                    "type" => 'textarea',
                    "label" => trans('example::example.label.tinytexts'),
                    "placeholder" => trans('example::example.placeholder.tinytexts'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'texts' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.texts'),
                    "placeholder" => trans('example::example.placeholder.texts'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'mediumtexts' => [
                    "type" => 'textarea',
                    "label" => trans('example::example.label.mediumtexts'),
                    "placeholder" => trans('example::example.placeholder.mediumtexts'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'longtexts' => [
                    "type" => 'html_editor',
                    "label" => trans('example::example.label.longtexts'),
                    "placeholder" => trans('example::example.placeholder.longtexts'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'binarys' => [
                    "type" => 'html_editor',
                    "label" => trans('example::example.label.binarys'),
                    "placeholder" => trans('example::example.placeholder.binarys'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'varbinarys' => [
                    "type" => 'html_editor',
                    "label" => trans('example::example.label.varbinarys'),
                    "placeholder" => trans('example::example.placeholder.varbinarys'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'tinyblobs' => [
                    "type" => 'textarea',
                    "label" => trans('example::example.label.tinyblobs'),
                    "placeholder" => trans('example::example.placeholder.tinyblobs'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'mediumblobs' => [
                    "type" => 'textarea',
                    "label" => trans('example::example.label.mediumblobs'),
                    "placeholder" => trans('example::example.placeholder.mediumblobs'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'blobs' => [
                    "type" => 'text',
                    "label" => trans('example::example.label.blobs'),
                    "placeholder" => trans('example::example.placeholder.blobs'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'longblobs' => [
                    "type" => 'html_editor',
                    "label" => trans('example::example.label.longblobs'),
                    "placeholder" => trans('example::example.placeholder.longblobs'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'enums' => [
                    "type" => 'radios',
                    "label" => trans('example::example.label.enums'),
                    "placeholder" => trans('example::example.placeholder.enums'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
                'sets' => [
                    "type" => 'radios',
                    "label" => trans('example::example.label.sets'),
                    "placeholder" => trans('example::example.placeholder.sets'),
                    "rules" => '',
                    "group" => "main",
                    "section" => "first",
                    "attributes" => [
                        'wrapper' => [],
                        "label" => [],
                        "input" => [],

                    ],
                ],
            ],
        ]);
    }
}
