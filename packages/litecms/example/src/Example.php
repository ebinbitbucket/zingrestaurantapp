<?php

namespace Litecms\Example;

use User;

class Example
{
    /**
     * $example object.
     */
    protected $example;

    /**
     * Constructor.
     */
    public function __construct(\Litecms\Example\Interfaces\ExampleRepositoryInterface $example)
    {
        $this->example = $example;
    }

    /**
     * Returns count of example.
     *
     * @param array $filter
     *
     * @return int
     */
    public function count()
    {
        return  0;
    }

    /**
     * Make gadget View
     *
     * @param string $view
     *
     * @param int $count
     *
     * @return View
     */
    public function gadget($view = 'admin.example.gadget', $count = 10)
    {

        if (User::hasRole('user')) {
            $this->example->pushCriteria(new \Litepie\Litecms\Repositories\Criteria\ExampleUserCriteria());
        }

        $example = $this->example->scopeQuery(function ($query) use ($count) {
            return $query->orderBy('id', 'DESC')->take($count);
        })->all();

        return view('example::' . $view, compact('example'))->render();
    }
}
