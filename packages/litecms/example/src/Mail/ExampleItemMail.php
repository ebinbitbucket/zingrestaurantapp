<?php

namespace Litecms\Example\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Litecms\Example\Interfaces\ExampleRepositoryInterface;

class ExampleItemMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * The example instance
     */
    protected $example;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $examples)
    {
        $this->examples       = $examples;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('example::admin.example.mail.item')
                    ->with([
                        'example' =>$this->examples,
                    ]);
    }
}
