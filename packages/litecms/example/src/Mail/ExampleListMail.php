<?php

namespace Litecms\Example\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExampleListMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The list of examples
     */
    var $examplea;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(array $examples)
    {
        $this->examples       = $examples;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('example::example.mail.list');
    }
}
