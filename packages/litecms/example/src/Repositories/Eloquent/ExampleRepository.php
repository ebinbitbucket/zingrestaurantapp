<?php

namespace Litecms\Example\Repositories\Eloquent;

use Litecms\Example\Interfaces\ExampleRepositoryInterface;
use Litepie\Repository\Eloquent\BaseRepository;

class ExampleRepository extends BaseRepository implements ExampleRepositoryInterface
{


    public function boot()
    {
        $this->fieldSearchable = config('litecms.example.example.model.search');

    }

    /**
     * Specify Model class name.
     *
     * @return string
     */
    public function model()
    {
        return config('litecms.example.example.model.model');
    }
}
