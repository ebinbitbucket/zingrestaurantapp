<?php

namespace Litecms\Example\Repositories\Presenter;

use League\Fractal\TransformerAbstract;
use Hashids;

class ExampleItemTransformer extends TransformerAbstract
{
    public function transform(\Litecms\Example\Models\Example $example)
    {
        return [
            'id'                => $example->getRouteKey(),
            'name'              => $example->name,
            'email'             => $example->email,
            'color'             => $example->color,
            'date'              => $example->date,
            'datetime'          => $example->datetime,
            'file'              => $example->file,
            'files'             => $example->files,
            'image'             => $example->image,
            'images'            => $example->images,
            'month'             => $example->month,
            'password'          => $example->password,
            'range'             => $example->range,
            'search'            => $example->search,
            'tel'               => $example->tel,
            'time'              => $example->time,
            'url'               => $example->url,
            'week'              => $example->week,
            'date_picker'       => $example->date_picker,
            'time_picker'       => $example->time_picker,
            'date_time_picker'  => $example->date_time_picker,
            'radios'            => $example->radios,
            'checkboxes'        => $example->checkboxes,
            'switch'            => $example->switch,
            'select'            => $example->select,
            'model_select'      => $example->model_select,
            'tinyints'          => $example->tinyints,
            'smallints'         => $example->smallints,
            'mediumints'        => $example->mediumints,
            'ints'              => $example->ints,
            'bigints'           => $example->bigints,
            'decimals'          => $example->decimals,
            'floats'            => $example->floats,
            'doubles'           => $example->doubles,
            'reals'             => $example->reals,
            'bits'              => $example->bits,
            'booleans'          => $example->booleans,
            'dates'             => $example->dates,
            'datetimes'         => $example->datetimes,
            'timestamps'        => $example->timestamps,
            'times'             => $example->times,
            'years'             => $example->years,
            'chars'             => $example->chars,
            'varchars'          => $example->varchars,
            'tinytexts'         => $example->tinytexts,
            'texts'             => $example->texts,
            'mediumtexts'       => $example->mediumtexts,
            'longtexts'         => $example->longtexts,
            'binarys'           => $example->binarys,
            'varbinarys'        => $example->varbinarys,
            'tinyblobs'         => $example->tinyblobs,
            'mediumblobs'       => $example->mediumblobs,
            'blobs'             => $example->blobs,
            'longblobs'         => $example->longblobs,
            'enums'             => $example->enums,
            'sets'              => $example->sets,
            'status'            => trans('app.'.$example->status),
            'created_at'        => format_date($example->created_at),
            'updated_at'        => format_date($example->updated_at),
        ];
    }
}