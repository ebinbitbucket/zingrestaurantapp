<?php

namespace Litecms\Example\Repositories\Presenter;

use Litepie\Repository\Presenter\FractalPresenter;

class ExampleItemPresenter extends FractalPresenter {

    /**
     * Prepare data to present
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ExampleItemTransformer();
    }
}