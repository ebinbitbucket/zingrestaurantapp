<?php

namespace Litecms\Example\Workflow;

use Litecms\Example\Models\Example;
use Litecms\Example\Notifications\ExampleWorkflow as ExampleNotifyer;
use Notification;

class ExampleNotification
{

    /**
     * Send the notification to the users after complete.
     *
     * @param Example $example
     *
     * @return void
     */
    public function complete(Example $example)
    {
        return Notification::send($example->user, new ExampleNotifyer($example, 'complete'));;
    }

    /**
     * Send the notification to the users after verify.
     *
     * @param Example $example
     *
     * @return void
     */
    public function verify(Example $example)
    {
        return Notification::send($example->user, new ExampleNotifyer($example, 'verify'));;
    }

    /**
     * Send the notification to the users after approve.
     *
     * @param Example $example
     *
     * @return void
     */
    public function approve(Example $example)
    {
        return Notification::send($example->user, new ExampleNotifyer($example, 'approve'));;

    }

    /**
     * Send the notification to the users after publish.
     *
     * @param Example $example
     *
     * @return void
     */
    public function publish(Example $example)
    {
        return Notification::send($example->user, new ExampleNotifyer($example, 'publish'));;
    }

    /**
     * Send the notification to the users after archive.
     *
     * @param Example $example
     *
     * @return void
     */
    public function archive(Example $example)
    {
        return Notification::send($example->user, new ExampleNotifyer($example, 'archive'));;

    }

    /**
     * Send the notification to the users after unpublish.
     *
     * @param Example $example
     *
     * @return void
     */
    public function unpublish(Example $example)
    {
        return Notification::send($example->user, new ExampleNotifyer($example, 'unpublish'));;

    }
}
