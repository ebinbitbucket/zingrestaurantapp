<?php

namespace Litecms\Example\Workflow;

use Litecms\Example\Models\Example;
use Validator;

class ExampleValidator
{

    /**
     * Determine if the given example is valid for complete status.
     *
     * @param Example $example
     *
     * @return bool / Validator
     */
    public function complete(Example $example)
    {
        return Validator::make($example->toArray(), [
            'title' => 'required|min:15',
        ]);
    }

    /**
     * Determine if the given example is valid for verify status.
     *
     * @param Example $example
     *
     * @return bool / Validator
     */
    public function verify(Example $example)
    {
        return Validator::make($example->toArray(), [
            'title'  => 'required|min:15',
            'status' => 'in:complete',
        ]);
    }

    /**
     * Determine if the given example is valid for approve status.
     *
     * @param Example $example
     *
     * @return bool / Validator
     */
    public function approve(Example $example)
    {
        return Validator::make($example->toArray(), [
            'title'  => 'required|min:15',
            'status' => 'in:verify',
        ]);

    }

    /**
     * Determine if the given example is valid for publish status.
     *
     * @param Example $example
     *
     * @return bool / Validator
     */
    public function publish(Example $example)
    {
        return Validator::make($example->toArray(), [
            'title'       => 'required|min:15',
            'description' => 'required|min:50',
            'status'      => 'in:approve,archive,unpublish',
        ]);

    }

    /**
     * Determine if the given example is valid for archive status.
     *
     * @param Example $example
     *
     * @return bool / Validator
     */
    public function archive(Example $example)
    {
        return Validator::make($example->toArray(), [
            'title'       => 'required|min:15',
            'description' => 'required|min:50',
            'status'      => 'in:approve,publish,unpublish',
        ]);

    }

    /**
     * Determine if the given example is valid for unpublish status.
     *
     * @param Example $example
     *
     * @return bool / Validator
     */
    public function unpublish(Example $example)
    {
        return Validator::make($example->toArray(), [
            'title'       => 'required|min:15',
            'description' => 'required|min:50',
            'status'      => 'in:publish',
        ]);

    }
}
