<?php

namespace Litecms\Example\Workflow;

use Exception;
use Litepie\Workflow\Exceptions\WorkflowActionNotPerformedException;

use Litecms\Example\Models\Example;

class ExampleAction
{
    /**
     * Perform the complete action.
     *
     * @param Example $example
     *
     * @return Example
     */
    public function complete(Example $example)
    {
        try {
            $example->status = 'complete';
            return $example->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the verify action.
     *
     * @param Example $example
     *
     * @return Example
     */public function verify(Example $example)
    {
        try {
            $example->status = 'verify';
            return $example->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the approve action.
     *
     * @param Example $example
     *
     * @return Example
     */public function approve(Example $example)
    {
        try {
            $example->status = 'approve';
            return $example->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the publish action.
     *
     * @param Example $example
     *
     * @return Example
     */public function publish(Example $example)
    {
        try {
            $example->status = 'publish';
            return $example->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the archive action.
     *
     * @param Example $example
     *
     * @return Example
     */
    public function archive(Example $example)
    {
        try {
            $example->status = 'archive';
            return $example->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }

    /**
     * Perform the unpublish action.
     *
     * @param Example $example
     *
     * @return Example
     */
    public function unpublish(Example $example)
    {
        try {
            $example->status = 'unpublish';
            return $example->save();
        } catch (Exception $e) {
            throw new WorkflowActionNotPerformedException();
        }
    }
}
