<?php

namespace Litecms\Example\Providers;

use Litepie\Workflow\Providers\WorkflowServiceProvider as ServiceProvider;

class WrokflowServiceProvider extends ServiceProvider
{

    /**
     * Register any package authentication / authorization services.
     *
     * @param \Illuminate\Contracts\Auth\Access\Gate $gate
     *
     * @return void
     */
    public function boot()
    {
        $this->workflow = config('litecms.example.workflows');
        parent::registerWorkflows();
    }
}
