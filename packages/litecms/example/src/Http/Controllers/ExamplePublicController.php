<?php

namespace Litecms\Example\Http\Controllers;

use App\Http\Controllers\PublicController as BaseController;
use Litecms\Example\Interfaces\ExampleRepositoryInterface;

class ExamplePublicController extends BaseController
{
    // use ExampleWorkflow;

    /**
     * Constructor.
     *
     * @param type \Litecms\Example\Interfaces\ExampleRepositoryInterface $example
     *
     * @return type
     */
    public function __construct(ExampleRepositoryInterface $example)
    {
        $this->repository = $example;
        parent::__construct();
    }

    /**
     * Show example's list.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function index()
    {
        $examples = $this->repository
        ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
        ->scopeQuery(function($query){
            return $query->orderBy('id','DESC');
        })->paginate();


        return $this->response->setMetaTitle(trans('$example::example.names'))
            ->view('example::example.index')
            ->data(compact('examples'))
            ->output();
    }


    /**
     * Show example.
     *
     * @param string $slug
     *
     * @return response
     */
    protected function show($slug)
    {
        $example = $this->repository->scopeQuery(function($query) use ($slug) {
            return $query->orderBy('id','DESC')
                         ->where('slug', $slug);
        })->first(['*']);

        return $this->response->setMetaTitle($$example->name . trans('example::example.name'))
            ->view('example::example.show')
            ->data(compact('example'))
            ->output();
    }

}
