<?php

namespace Litecms\Example\Http\Controllers;

use App\Http\Controllers\ResourceController as BaseController;
use Form;
use Litecms\Example\Http\Requests\ExampleRequest;
use Litecms\Example\Interfaces\ExampleRepositoryInterface;
use Litecms\Example\Models\Example;

/**
 * Resource controller class for example.
 */
class ExampleFormController extends BaseController
{

    /**
     * Initialize example resource controller.
     *
     * @param type ExampleRepositoryInterface $example
     *
     * @return null
     */
    public function __construct(ExampleRepositoryInterface $example)
    {
        parent::__construct();
        $this->repository = $example;
        $this->repository
            ->pushCriteria(\Litepie\Repository\Criteria\RequestCriteria::class)
            ->pushCriteria(\Litecms\Example\Repositories\Criteria\ExampleResourceCriteria::class);
    }

    /**
     * Display a list of example.
     *
     * @return Response
     */
    public function index(ExampleRequest $request)
    {
        $view = $this->response->theme->listView();

        if ($this->response->typeIs('json')) {
            $function = camel_case('get-' . $view);
            return $this->repository
                ->setPresenter(\Litecms\Example\Repositories\Presenter\ExamplePresenter::class)
                ->$function();
        }

        $examples = $this->repository->paginate();

        return $this->response->setMetaTitle(trans('example::example.names'))
            ->view('example::example.index', true)
            ->data(compact('examples', 'view'))
            ->output();
    }

    /**
     * Display example.
     *
     * @param Request $request
     * @param Model   $example
     *
     * @return Response
     */
    public function show(ExampleRequest $request, Example $example)
    {

        if ($example->exists) {
            $view = 'example::example.show';
        } else {
            $view = 'example::example.new';
        }

        return $this->response->setMetaTitle(trans('app.view') . ' ' . trans('example::example.name'))
            ->data(compact('example'))
            ->view($view, true)
            ->output();
    }

    /**
     * Show the form for creating a new example.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function create(ExampleRequest $request)
    {

        $example = $this->repository->newInstance([]);
        return $this->response->setMetaTitle(trans('app.new') . ' ' . trans('example::example.name'))
            ->view('example::example.create', true)
            ->data(compact('example'))
            ->output();
    }

    /**
     * Create new example.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function store(ExampleRequest $request)
    {
        try {
            $attributes = $request->all();
            $attributes['user_id'] = user_id();
            $attributes['user_type'] = user_type();
            $example = $this->repository->create($attributes);

            return $this->response->message(trans('messages.success.created', ['Module' => trans('example::example.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('example/example/' . $example->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('/example/example'))
                ->redirect();
        }

    }

    /**
     * Show example for editing.
     *
     * @param Request $request
     * @param Model   $example
     *
     * @return Response
     */
    public function edit(ExampleRequest $request, Example $example)
    {
        return $this->response->setMetaTitle(trans('app.edit') . ' ' . trans('example::example.name'))
            ->view('example::example.edit', true)
            ->data(compact('example'))
            ->output();
    }

    /**
     * Update the example.
     *
     * @param Request $request
     * @param Model   $example
     *
     * @return Response
     */
    public function update(ExampleRequest $request, Example $example)
    {
        try {
            $attributes = $request->all();

            $example->update($attributes);
            return $this->response->message(trans('messages.success.updated', ['Module' => trans('example::example.name')]))
                ->code(204)
                ->status('success')
                ->url(guard_url('example/example/' . $example->getRouteKey()))
                ->redirect();
        } catch (Exception $e) {
            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('example/example/' . $example->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove the example.
     *
     * @param Model   $example
     *
     * @return Response
     */
    public function destroy(ExampleRequest $request, Example $example)
    {
        try {

            $example->delete();
            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('example::example.name')]))
                ->code(202)
                ->status('success')
                ->url(guard_url('example/example/0'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->code(400)
                ->status('error')
                ->url(guard_url('example/example/' . $example->getRouteKey()))
                ->redirect();
        }

    }

    /**
     * Remove multiple example.
     *
     * @param Model   $example
     *
     * @return Response
     */
    public function delete(ExampleRequest $request, $type)
    {
        try {
            $ids = hashids_decode($request->input('ids'));

            if ($type == 'purge') {
                $this->repository->purge($ids);
            } else {
                $this->repository->delete($ids);
            }

            return $this->response->message(trans('messages.success.deleted', ['Module' => trans('example::example.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('example/example'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/example/example'))
                ->redirect();
        }

    }

    /**
     * Restore deleted examples.
     *
     * @param Model   $example
     *
     * @return Response
     */
    public function restore(ExampleRequest $request)
    {
        try {
            $ids = hashids_decode($request->input('ids'));
            $this->repository->restore($ids);

            return $this->response->message(trans('messages.success.restore', ['Module' => trans('example::example.name')]))
                ->status("success")
                ->code(202)
                ->url(guard_url('/example/example'))
                ->redirect();

        } catch (Exception $e) {

            return $this->response->message($e->getMessage())
                ->status("error")
                ->code(400)
                ->url(guard_url('/example/example/'))
                ->redirect();
        }

    }

}
