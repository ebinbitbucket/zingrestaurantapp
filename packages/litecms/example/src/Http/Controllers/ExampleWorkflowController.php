<?php

namespace Litecms\Example\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;
use Litecms\Example\Http\Requests\ExampleRequest;
use Litecms\Example\Models\Example;

/**
 * Admin web controller class.
 */
class ExampleWorkflowController extends BaseController
{

    /**
     * Workflow controller function for example.
     *
     * @param Model   $example
     * @param step    next step for the workflow.
     *
     * @return Response
     */

    public function putWorkflow(ExampleRequest $request, Example $example, $step)
    {

        try {

            $example->updateWorkflow($step);

            return response()->json([
                'message'  => trans('messages.success.changed', ['Module' => trans('example::example.name'), 'status' => trans("app.{$step}")]),
                'code'     => 204,
                'redirect' => trans_url('/admin/example/example/' . $example->getRouteKey()),
            ], 201);

        } catch (Exception $e) {

            return response()->json([
                'message'  => $e->getMessage(),
                'code'     => 400,
                'redirect' => trans_url('/admin/example/example/' . $example->getRouteKey()),
            ], 400);

        }

    }

    /**
     * Workflow controller function for example.
     *
     * @param Model   $example
     * @param step    next step for the workflow.
     * @param user    encrypted user id.
     *
     * @return Response
     */

    public function getWorkflow(Example $example, $step, $user)
    {
        try {
            $user_id = decrypt($user);

            Auth::onceUsingId($user_id);

            $example->updateWorkflow($step);

            $data = [
                'message' => trans('messages.success.changed', ['Module' => trans('example::example.name'), 'status' => trans("app.{$step}")]),
                'status'  => 'success',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('example::admin.example.message', $data)->render();

        } catch (ValidationException $e) {

            $data = [
                'message' => '<b>' . $e->getMessage() . '</b> <br /><br />' . implode('<br />', $e->validator->errors()->all()),
                'status'  => 'error',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('example::admin.example.message', $data)->render();

        } catch (Exception $e) {

            $data = [
                'message' => '<b>' . $e->getMessage() . '</b>',
                'status'  => 'error',
                'step'    => trans("app.{$step}"),
            ];

            return $this->theme->layout('blank')->of('example::admin.example.message', $data)->render();

        }

    }
}
