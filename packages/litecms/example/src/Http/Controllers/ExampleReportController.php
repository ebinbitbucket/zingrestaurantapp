<?php

namespace Litecms\Example\Http\Controllers;

use App\Http\Controllers\ReportController as BaseController;
use Litecms\Example\Http\Requests\ExampleRequest;
use Litecms\Example\Interfaces\ExampleRepositoryInterface;
use Litecms\Example\Models\Example;
use PDF;
use Excel;

/**
 * Admin web controller class.
 */
class ExampleReportController extends BaseController
{

// use ExampleWorkflow;
    /**
     * Initialize example controller.
     *
     * @param type ExampleRepositoryInterface $example
     *
     * @return type
     */
    public function __construct(ExampleRepositoryInterface $example) {
        parent::__construct();
        $this->repository = $example;
    }

    /**
     * Display a list of example.
     *
     * @return Response
     */
    public function index(ExampleRequest $request)
    {

        return $this->response->setMetaTitle(trans('example::example.names'))
            ->view($this->getView('example.report.index', 'example'))
            ->output();

    }

    /**
     * Display a list of example.
     *
     * @return Response
     */
    public function reportAll(ExampleRequest $request)
    {
        if ($request->wantsJson()) {
            $pageLimit = $request->input('pageLimit');
            $examples  = $this->repository
                ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
                ->setPresenter('\\Lavalite\\Example\\Repositories\\Presenter\\ExampleListPresenter')
                ->scopeQuery(function ($query) {
                    return $query->take(500)->orderBy('id', 'DESC');
                })->paginate($pageLimit);

            return response()->json($examples, 200);
        }

    }

    /**
     * Display example.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function reportGraph(ExampleRequest $request)
    {
        $groupBy = $request->input('search.group_by');
        $orderBy = $request->input('search.order_by');

        $examples = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($groupBy, $orderBy) {
                return $query->selectRaw("{$groupBy} as name, count(id) as count")
                    ->orderBy($groupBy, $orderBy)
                    ->groupBy($groupBy);
            })->all();

        $data = [];
        foreach ($examples as $key => $example) {
            $data['categories'][$key]      = @$example['name'];
            $data['series']['count'][$key] = intval(@$example['count']);
        }

        if ($request->has('callback')) {
            return $_GET['callback'] . '(' . json_encode($data) . ')';
        } else {
            return json_encode($data);
        }

    }

    /**
     * Display example.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function reportSummary(ExampleRequest $request)
    {
        $groupBy = $request->input('search.group_by');
        $orderBy = $request->input('search.order_by');

        $examples = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($groupBy, $orderBy) {
                return $query->selectRaw("{$groupBy} as name, count(id) as count")
                    ->orderBy($groupBy, $orderBy)
                    ->groupBy($groupBy);
            })->all();

        return $this->response->setMetaTitle(trans('example::example.names'))
            ->view($this->getView('example.report.summary', 'example'))
            ->data(compact('examples','groupBy'))
            ->output();
    }

    /**
     * Download pdf.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function reportPdf(ExampleRequest $request)
    {
        $groupBy = $request->input('search.group_by');
        $orderBy = $request->input('search.order_by');

        $data['examples']['all'] = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($groupBy, $orderBy) {
                return $query->orderBy($groupBy, $orderBy);
            })->all()->toArray();

        $data['examples']['available'] = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($groupBy, $orderBy) {
                return $query->selectRaw("{$groupBy} as name, count(id) as count")
                    ->orderBy($groupBy, $orderBy)
                    ->groupBy($groupBy);
            })->all()->toArray();

        foreach ($data['examples']['available'] as $key => $value) {
            $data['examples']['available'][$key]['labels'] = @$value['name'];
        }

        $data['examples']['all'] = array_chunk($data['examples']['all'], 35);

        $item = array_reduce($data['examples']['available'], function ($a, $b) {
            return @$a['count'] > $b['count'] ? $a : $b;
        });
        $data['examples']['labels'] = @$item['count'];

        $data['colors'] = ["#ff0db6", "#10910e", "#f52116", "#b10aff", "#f62459", "#bf55ec", "#19b5fe", "#f9690e", "#ffb61e", "#26c281"];

        $old_limit = ini_set("memory_limit", "1000M");
        $pdf       = PDF::loadView($this->getView('example.report.download', 'example'), $data);
        $pdf->stream("report.pdf");

        return $pdf->download('report.pdf');
    }

    /**
     * Download csv.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function reportCsv(ExampleRequest $request)
    {
        $groupBy = $request->input('search.group_by');
        $orderBy = $request->input('search.order_by');

        $examples = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($groupBy, $orderBy) {
                return $query->orderBy($groupBy, $orderBy);
            })->all()->toArray();

        return Excel::create('examples', function ($excel) use ($examples) {
            $excel->sheet('example', function ($sheet) use ($examples) {
                $sheet->fromArray($examples);
            });

        })->download('csv');

    }

/**
 * Download csv.
 *
 * @param Request $request
 *
 * @return Response
 */
    public function reportExcel(ExampleRequest $request)
    {
        $groupBy = $request->input('search.group_by');
        $orderBy = $request->input('search.order_by');

        $examples = $this->repository
            ->pushCriteria(app('Litepie\Repository\Criteria\RequestCriteria'))
            ->scopeQuery(function ($query) use ($groupBy, $orderBy) {
                return $query->orderBy($groupBy, $orderBy);
            })->all()->toArray();

        return Excel::create('examples', function ($excel) use ($examples) {
            $excel->sheet('example', function ($sheet) use ($examples) {
                $sheet->fromArray($examples);
            });

        })->download('xls');

    }

}
