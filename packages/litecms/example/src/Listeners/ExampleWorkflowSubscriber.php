<?php

namespace Litecms\Example\Listeners;

use Litepie\Workflow\Events\GuardEvent;

class ExampleWorkflowSubscriber
{
    /**
     * Handle workflow guard events.
     */
    public function onGuard(GuardEvent $event) {
        $originalEvent = $event->getOriginalEvent();
        

        /** Symfony\Component\Workflow\Event\GuardEvent */

        /** @var Litecms\Example\BlogPost $post */
        $post = $originalEvent->getSubject();
        $title = $post->title;

        if (empty($title)) {
            //$originalEvent->setBlocked(true);
            // Posts with no title should not be allowed
        }
    }

    /**
     * Handle workflow leave event.
     */
    public function onLeave($event) {}

    /**
     * Handle workflow transition event.
     */
    public function onTransition($event) {}

    /**
     * Handle workflow enter event.
     */
    public function onEnter($event) {}

    /**
     * Handle workflow entered event.
     */
    public function onEntered($event) {}

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Litepie\Workflow\Events\GuardEvent',
            'Litecms\Example\Listeners\ExampleWorkflowSubscriber@onGuard'
        );

        $events->listen(
            'Litepie\Workflow\Events\LeaveEvent',
            'Litecms\Example\Listeners\ExampleWorkflowSubscriber@onLeave'
        );

        $events->listen(
            'Litepie\Workflow\Events\TransitionEvent',
            'Litecms\Example\Listeners\ExampleWorkflowSubscriber@onTransition'
        );

        $events->listen(
            'Litepie\Workflow\Events\EnterEvent',
            'Litecms\Example\Listeners\ExampleWorkflowSubscriber@onEnter'
        );

        $events->listen(
            'Litepie\Workflow\Events\EnteredEvent',
            'Litecms\Example\Listeners\ExampleWorkflowSubscriber@onEntered'
        );
    }

}