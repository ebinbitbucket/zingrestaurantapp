<?php

// API routes  for example
Route::prefix('example')->group(function () {
    Route::get('example/form', 'ExampleFormController@form');
    Route::resource('example', 'ExampleResourceController');
});

// Public routes for example
Route::get('examples/', 'ExamplePublicController@index');
Route::get('examples/{slug?}', 'ExamplePublicController@show');

if (Trans::isMultilingual()) {
    Route::group(
        [
            'prefix' => '{trans}',
            'where' => ['trans' => Trans::keys('|')],
        ],
        function () {
            // Guard routes for examples
            Route::prefix('{guard}/example')->group(function () {
                Route::get('example/form/{element}', 'ExampleResourceController@form');
                Route::resource('example', 'ExampleAPIController');
            });
            // Public routes for examples
            Route::get('example/Example', 'ExamplePublicController@getExample');
        }
    );
}
