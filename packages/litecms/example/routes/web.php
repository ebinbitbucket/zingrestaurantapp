<?php

// web routes  for example
Route::prefix('example')->group(function () {
    Route::resource('example', 'ExampleResourceController');
});

// Public routes for example
Route::get('examples/', 'ExamplePublicController@index');
Route::get('examples/{slug?}', 'ExamplePublicController@show');

if (Trans::isMultilingual()) {
    Route::group(
        [
            'prefix' => '{trans}',
            'where'  => ['trans' => Trans::keys('|')],
        ],
        function () {
            // Guard routes for pages
            Route::prefix('{guard}/page')->group(function () {
                Route::apiResource('page', 'ExampleResourceController');
            });
            // Public routes for pages
            Route::get('examples/', 'ExamplePublicController@index');
            Route::get('examples/{slug?}', 'ExamplePublicController@show');
        }
    );
}

