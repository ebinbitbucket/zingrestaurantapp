            @include('example::example.partial.header')

            <section class="single">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3">
                            @include('example::example.partial.aside')
                        </div>
                        <div class="col-md-9 ">
                            <div class="area">
                                <div class="item">
                                    <div class="feature">
                                        <img class="img-responsive center-block" src="{!!url($example->defaultImage('images' , 'xl'))!!}" alt="{{$example->title}}">
                                    </div>
                                    <div class="content">
                                        <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('example::example.label.id') !!}
                </label><br />
                    {!! $example['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('example::example.label.name') !!}
                </label><br />
                    {!! $example['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="email">
                    {!! trans('example::example.label.email') !!}
                </label><br />
                    {!! $example['email'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="color">
                    {!! trans('example::example.label.color') !!}
                </label><br />
                    {!! $example['color'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="date">
                    {!! trans('example::example.label.date') !!}
                </label><br />
                    {!! $example['date'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="datetime">
                    {!! trans('example::example.label.datetime') !!}
                </label><br />
                    {!! $example['datetime'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="file">
                    {!! trans('example::example.label.file') !!}
                </label><br />
                    {!! $example['file'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="files">
                    {!! trans('example::example.label.files') !!}
                </label><br />
                    {!! $example['files'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="image">
                    {!! trans('example::example.label.image') !!}
                </label><br />
                    {!! $example['image'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="images">
                    {!! trans('example::example.label.images') !!}
                </label><br />
                    {!! $example['images'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="month">
                    {!! trans('example::example.label.month') !!}
                </label><br />
                    {!! $example['month'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="password">
                    {!! trans('example::example.label.password') !!}
                </label><br />
                    {!! $example['password'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="range">
                    {!! trans('example::example.label.range') !!}
                </label><br />
                    {!! $example['range'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="search">
                    {!! trans('example::example.label.search') !!}
                </label><br />
                    {!! $example['search'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tel">
                    {!! trans('example::example.label.tel') !!}
                </label><br />
                    {!! $example['tel'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="time">
                    {!! trans('example::example.label.time') !!}
                </label><br />
                    {!! $example['time'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="url">
                    {!! trans('example::example.label.url') !!}
                </label><br />
                    {!! $example['url'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="week">
                    {!! trans('example::example.label.week') !!}
                </label><br />
                    {!! $example['week'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="date_picker">
                    {!! trans('example::example.label.date_picker') !!}
                </label><br />
                    {!! $example['date_picker'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="time_picker">
                    {!! trans('example::example.label.time_picker') !!}
                </label><br />
                    {!! $example['time_picker'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="date_time_picker">
                    {!! trans('example::example.label.date_time_picker') !!}
                </label><br />
                    {!! $example['date_time_picker'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="radios">
                    {!! trans('example::example.label.radios') !!}
                </label><br />
                    {!! $example['radios'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="checkboxes">
                    {!! trans('example::example.label.checkboxes') !!}
                </label><br />
                    {!! $example['checkboxes'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="switch">
                    {!! trans('example::example.label.switch') !!}
                </label><br />
                    {!! $example['switch'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="select">
                    {!! trans('example::example.label.select') !!}
                </label><br />
                    {!! $example['select'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="model_select">
                    {!! trans('example::example.label.model_select') !!}
                </label><br />
                    {!! $example['model_select'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tinyints">
                    {!! trans('example::example.label.tinyints') !!}
                </label><br />
                    {!! $example['tinyints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="smallints">
                    {!! trans('example::example.label.smallints') !!}
                </label><br />
                    {!! $example['smallints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="mediumints">
                    {!! trans('example::example.label.mediumints') !!}
                </label><br />
                    {!! $example['mediumints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="ints">
                    {!! trans('example::example.label.ints') !!}
                </label><br />
                    {!! $example['ints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="bigints">
                    {!! trans('example::example.label.bigints') !!}
                </label><br />
                    {!! $example['bigints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="decimals">
                    {!! trans('example::example.label.decimals') !!}
                </label><br />
                    {!! $example['decimals'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="floats">
                    {!! trans('example::example.label.floats') !!}
                </label><br />
                    {!! $example['floats'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="doubles">
                    {!! trans('example::example.label.doubles') !!}
                </label><br />
                    {!! $example['doubles'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="reals">
                    {!! trans('example::example.label.reals') !!}
                </label><br />
                    {!! $example['reals'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="bits">
                    {!! trans('example::example.label.bits') !!}
                </label><br />
                    {!! $example['bits'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="booleans">
                    {!! trans('example::example.label.booleans') !!}
                </label><br />
                    {!! $example['booleans'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="dates">
                    {!! trans('example::example.label.dates') !!}
                </label><br />
                    {!! $example['dates'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="datetimes">
                    {!! trans('example::example.label.datetimes') !!}
                </label><br />
                    {!! $example['datetimes'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="timestamps">
                    {!! trans('example::example.label.timestamps') !!}
                </label><br />
                    {!! $example['timestamps'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="times">
                    {!! trans('example::example.label.times') !!}
                </label><br />
                    {!! $example['times'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="years">
                    {!! trans('example::example.label.years') !!}
                </label><br />
                    {!! $example['years'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="chars">
                    {!! trans('example::example.label.chars') !!}
                </label><br />
                    {!! $example['chars'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="varchars">
                    {!! trans('example::example.label.varchars') !!}
                </label><br />
                    {!! $example['varchars'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tinytexts">
                    {!! trans('example::example.label.tinytexts') !!}
                </label><br />
                    {!! $example['tinytexts'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="texts">
                    {!! trans('example::example.label.texts') !!}
                </label><br />
                    {!! $example['texts'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="mediumtexts">
                    {!! trans('example::example.label.mediumtexts') !!}
                </label><br />
                    {!! $example['mediumtexts'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="longtexts">
                    {!! trans('example::example.label.longtexts') !!}
                </label><br />
                    {!! $example['longtexts'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="binarys">
                    {!! trans('example::example.label.binarys') !!}
                </label><br />
                    {!! $example['binarys'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="varbinarys">
                    {!! trans('example::example.label.varbinarys') !!}
                </label><br />
                    {!! $example['varbinarys'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tinyblobs">
                    {!! trans('example::example.label.tinyblobs') !!}
                </label><br />
                    {!! $example['tinyblobs'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="mediumblobs">
                    {!! trans('example::example.label.mediumblobs') !!}
                </label><br />
                    {!! $example['mediumblobs'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="blobs">
                    {!! trans('example::example.label.blobs') !!}
                </label><br />
                    {!! $example['blobs'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="longblobs">
                    {!! trans('example::example.label.longblobs') !!}
                </label><br />
                    {!! $example['longblobs'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="enums">
                    {!! trans('example::example.label.enums') !!}
                </label><br />
                    {!! $example['enums'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="sets">
                    {!! trans('example::example.label.sets') !!}
                </label><br />
                    {!! $example['sets'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="slug">
                    {!! trans('example::example.label.slug') !!}
                </label><br />
                    {!! $example['slug'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="status">
                    {!! trans('example::example.label.status') !!}
                </label><br />
                    {!! $example['status'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_id">
                    {!! trans('example::example.label.user_id') !!}
                </label><br />
                    {!! $example['user_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_type">
                    {!! trans('example::example.label.user_type') !!}
                </label><br />
                    {!! $example['user_type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="upload_folder">
                    {!! trans('example::example.label.upload_folder') !!}
                </label><br />
                    {!! $example['upload_folder'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('example::example.label.deleted_at') !!}
                </label><br />
                    {!! $example['deleted_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('example::example.label.created_at') !!}
                </label><br />
                    {!! $example['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('example::example.label.updated_at') !!}
                </label><br />
                    {!! $example['updated_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="PRIMARY">
                    {!! trans('example::example.label.PRIMARY') !!}
                </label><br />
                    {!! $example['PRIMARY'] !!}
            </div>
        </div>
    </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('example::example.label.name'))
                       -> placeholder(trans('example::example.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('email')
                       -> label(trans('example::example.label.email'))
                       -> placeholder(trans('example::example.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('color')
                       -> label(trans('example::example.label.color'))
                       -> placeholder(trans('example::example.placeholder.color'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   <div class='form-group'>
                     <label for='date' class='control-label'>{!!trans('example::example.label.date')!!}</label>
                     <div class='input-group pickdate'>
                        {!! Form::text('date')
                        -> placeholder(trans('example::example.placeholder.date'))
                        ->raw()!!}
                       <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                     </div>
                   </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='datetime' class='control-label'>{!!trans('example::example.label.datetime')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('datetime')
                            -> placeholder(trans('example::example.placeholder.datetime'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('file')
                       -> label(trans('example::example.label.file'))
                       -> placeholder(trans('example::example.placeholder.file'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('files')
                       -> label(trans('example::example.label.files'))
                       -> placeholder(trans('example::example.placeholder.files'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('image')
                       -> label(trans('example::example.label.image'))
                       -> placeholder(trans('example::example.placeholder.image'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('images')
                       -> label(trans('example::example.label.images'))
                       -> placeholder(trans('example::example.placeholder.images'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('month')
                       -> label(trans('example::example.label.month'))
                       -> placeholder(trans('example::example.placeholder.month'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('password')
                       -> label(trans('example::example.label.password'))
                       -> placeholder(trans('example::example.placeholder.password'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('range')
                       -> label(trans('example::example.label.range'))
                       -> placeholder(trans('example::example.placeholder.range'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('search')
                       -> label(trans('example::example.label.search'))
                       -> placeholder(trans('example::example.placeholder.search'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('tel')
                       -> label(trans('example::example.label.tel'))
                       -> placeholder(trans('example::example.placeholder.tel'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('time')
                       -> label(trans('example::example.label.time'))
                       -> placeholder(trans('example::example.placeholder.time'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('url')
                       -> label(trans('example::example.label.url'))
                       -> placeholder(trans('example::example.placeholder.url'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('week')
                       -> label(trans('example::example.label.week'))
                       -> placeholder(trans('example::example.placeholder.week'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   <div class='form-group'>
                     <label for='date_picker' class='control-label'>{!!trans('example::example.label.date_picker')!!}</label>
                     <div class='input-group pickdate'>
                        {!! Form::text('date_picker')
                        -> placeholder(trans('example::example.placeholder.date_picker'))
                        ->raw()!!}
                       <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                     </div>
                   </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='time_picker' class='control-label'>{!!trans('example::example.label.time_picker')!!}</label>
                        <div class='input-group picktime'>
                            {!! Form::text('time_picker')
                            -> placeholder(trans('example::example.placeholder.time_picker'))
                            -> raw()!!}
                            <span class='input-group-addon'><i class='fa fa-clock-o'></i></span>
                        </div>
                    </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='date_time_picker' class='control-label'>{!!trans('example::example.label.date_time_picker')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('date_time_picker')
                            -> placeholder(trans('example::example.placeholder.date_time_picker'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('radios')
                       -> label(trans('example::example.label.radios'))
                       -> placeholder(trans('example::example.placeholder.radios'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('checkboxes')
                       -> label(trans('example::example.label.checkboxes'))
                       -> placeholder(trans('example::example.placeholder.checkboxes'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('switch')
                       -> label(trans('example::example.label.switch'))
                       -> placeholder(trans('example::example.placeholder.switch'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('select')
                   -> radios(trans('example::example.options.select'))
                   -> label(trans('example::example.label.select'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('model_select')
                   -> radios(trans('example::example.options.model_select'))
                   -> label(trans('example::example.label.model_select'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('tinyints')
                       -> label(trans('example::example.label.tinyints'))
                       -> placeholder(trans('example::example.placeholder.tinyints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('smallints')
                       -> label(trans('example::example.label.smallints'))
                       -> placeholder(trans('example::example.placeholder.smallints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('mediumints')
                       -> label(trans('example::example.label.mediumints'))
                       -> placeholder(trans('example::example.placeholder.mediumints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('ints')
                       -> label(trans('example::example.label.ints'))
                       -> placeholder(trans('example::example.placeholder.ints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('bigints')
                       -> label(trans('example::example.label.bigints'))
                       -> placeholder(trans('example::example.placeholder.bigints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('decimals')
                       -> label(trans('example::example.label.decimals'))
                       -> placeholder(trans('example::example.placeholder.decimals'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('floats')
                       -> label(trans('example::example.label.floats'))
                       -> placeholder(trans('example::example.placeholder.floats'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('doubles')
                       -> label(trans('example::example.label.doubles'))
                       -> placeholder(trans('example::example.placeholder.doubles'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('reals')
                       -> label(trans('example::example.label.reals'))
                       -> placeholder(trans('example::example.placeholder.reals'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('bits')
                       -> label(trans('example::example.label.bits'))
                       -> placeholder(trans('example::example.placeholder.bits'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('booleans')
                       -> label(trans('example::example.label.booleans'))
                       -> placeholder(trans('example::example.placeholder.booleans'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   <div class='form-group'>
                     <label for='dates' class='control-label'>{!!trans('example::example.label.dates')!!}</label>
                     <div class='input-group pickdate'>
                        {!! Form::text('dates')
                        -> placeholder(trans('example::example.placeholder.dates'))
                        ->raw()!!}
                       <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                     </div>
                   </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='datetimes' class='control-label'>{!!trans('example::example.label.datetimes')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('datetimes')
                            -> placeholder(trans('example::example.placeholder.datetimes'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='timestamps' class='control-label'>{!!trans('example::example.label.timestamps')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('timestamps')
                            -> placeholder(trans('example::example.placeholder.timestamps'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='times' class='control-label'>{!!trans('example::example.label.times')!!}</label>
                        <div class='input-group picktime'>
                            {!! Form::text('times')
                            -> placeholder(trans('example::example.placeholder.times'))
                            -> raw()!!}
                            <span class='input-group-addon'><i class='fa fa-clock-o'></i></span>
                        </div>
                    </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('years')
                       -> label(trans('example::example.label.years'))
                       -> placeholder(trans('example::example.placeholder.years'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('chars')
                       -> label(trans('example::example.label.chars'))
                       -> placeholder(trans('example::example.placeholder.chars'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('varchars')
                       -> label(trans('example::example.label.varchars'))
                       -> placeholder(trans('example::example.placeholder.varchars'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('tinytexts')
                    -> label(trans('example::example.label.tinytexts'))
                    -> placeholder(trans('example::example.placeholder.tinytexts'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('texts')
                       -> label(trans('example::example.label.texts'))
                       -> placeholder(trans('example::example.placeholder.texts'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('mediumtexts')
                    -> label(trans('example::example.label.mediumtexts'))
                    -> placeholder(trans('example::example.placeholder.mediumtexts'))!!}
                </div>

                <div class='col-md-12'>
                    {!! Form::textarea('longtexts')
                    -> label(trans('example::example.label.longtexts'))
                    -> dataUpload(trans_url($example->getUploadURL('longtexts')))
                    -> addClass('html-editor')
                    -> placeholder(trans('example::example.placeholder.longtexts'))!!}
                </div>
                <div class='col-md-12'>
                    {!! Form::textarea('binarys')
                    -> label(trans('example::example.label.binarys'))
                    -> dataUpload(trans_url($example->getUploadURL('binarys')))
                    -> addClass('html-editor')
                    -> placeholder(trans('example::example.placeholder.binarys'))!!}
                </div>
                <div class='col-md-12'>
                    {!! Form::textarea('varbinarys')
                    -> label(trans('example::example.label.varbinarys'))
                    -> dataUpload(trans_url($example->getUploadURL('varbinarys')))
                    -> addClass('html-editor')
                    -> placeholder(trans('example::example.placeholder.varbinarys'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('tinyblobs')
                    -> label(trans('example::example.label.tinyblobs'))
                    -> placeholder(trans('example::example.placeholder.tinyblobs'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('mediumblobs')
                    -> label(trans('example::example.label.mediumblobs'))
                    -> placeholder(trans('example::example.placeholder.mediumblobs'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('blobs')
                       -> label(trans('example::example.label.blobs'))
                       -> placeholder(trans('example::example.placeholder.blobs'))!!}
                </div>

                <div class='col-md-12'>
                    {!! Form::textarea('longblobs')
                    -> label(trans('example::example.label.longblobs'))
                    -> dataUpload(trans_url($example->getUploadURL('longblobs')))
                    -> addClass('html-editor')
                    -> placeholder(trans('example::example.placeholder.longblobs'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('enums')
                   -> radios(trans('example::example.options.enums'))
                   -> label(trans('example::example.label.enums'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('sets')
                   -> radios(trans('example::example.options.sets'))
                   -> label(trans('example::example.label.sets'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::package::package.formcontrols.KEY('PRIMARY')
                       -> label(trans('example::example.label.PRIMARY'))
                       -> placeholder(trans('example::example.placeholder.PRIMARY'))!!}
                </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



