@extends('resource.show')

@php
$links['back'] = guard_url('example/example');
$links['edit'] = guard_url('example/example') . '/' . $example->getRouteKey() . '/edit';
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! trans('example::example.title.main') !!}
@stop

@section('sub.title') 
{!! trans('example::example.title.show') !!}
@stop

@section('breadcrumb') 
  <li><a href="{{guard_url('/')}}">{{ __('app.home') }}</a></li>
  <li><a href="{{guard_url('example/example')}}">{{ __('example::example.name') }}</a></li>
  <li>{{ __('app.show') }}</li>
@stop

@section('tabs') 
@stop

@section('tools') 
    <a href="{{guard_url('example/example')}}" rel="tooltip" class="btn btn-white btn-round btn-simple btn-icon pull-right add-new" data-original-title="" title="">
            <i class="fa fa-chevron-left"></i>
    </a>
@stop

@section('content') 
    @include('example::example.partial.show', ['mode' => 'show'])
@stop
