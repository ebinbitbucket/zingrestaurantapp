@extends('resource.index')
@php
$links['create'] = guard_url('example/example/create');
$links['search'] = guard_url('example/example');
@endphp

@section('icon') 
<i class="pe-7s-display2"></i>
@stop

@section('title') 
{!! trans('example::example.title.main') !!}
@stop

@section('sub.title') 
{!! trans('example::example.title.list') !!}
@stop

@section('breadcrumb') 
  <li><a href="{{guard_url('/')}}">{{ __('app.home') }}</a></li>
  <li><a href="{{guard_url('example/example')}}">{{ __('example::example.name') }}</a></li>
  <li>{{ __('app.list') }}</li>
@stop

@section('entry') 
<div id="entry-form">

</div>
@stop

@section('list')
    @include('example::example.partial.list.' . $view, ['mode' => 'list'])
@stop

@section('pagination') 
    {{$examples->links()}}
@stop

@section('script')

@stop

@section('style')

@stop 
