            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('example::example.label.name'))
                       -> placeholder(trans('example::example.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('email')
                       -> label(trans('example::example.label.email'))
                       -> placeholder(trans('example::example.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('color')
                       -> label(trans('example::example.label.color'))
                       -> placeholder(trans('example::example.placeholder.color'))!!}
                </div>



                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('datetime')
                       -> label(trans('example::example.label.datetime'))
                       -> placeholder(trans('example::example.placeholder.datetime'))!!}
                </div>

                <div class='col-md-12 col-sm-12'>
                @if ($mode == 'edit' || $mode == 'create')
                    <label for="image" class="control-label text-left">
                        {{trans('example::example.label.image') }}
                    </label>
                    <div class='clearfix'>
                        {!! $example->files('image')
                        ->url($example->getUploadUrl('image'))
                        ->uploader()!!}
                    </div>

                    <label for="images" class="control-label text-left">
                        {{trans('example::example.label.images') }}
                    </label>
                    <div class='clearfix'>
                        {!! $example->files('images')
                        ->url($example->getUploadUrl('images'))
                        ->uploader()!!}
                    </div>
                @elseif ($mode == 'show')
                    <label for="image" class="control-label text-left">
                        {{trans('example::example.label.image') }}
                    </label>
                    <div class='col-md-12'>
                        {!! $example->files('image') !!}
                    </div>
                    <label for="images" class="control-label text-left">
                        {{trans('example::example.label.images') }}
                    </label>
                    <div class='col-md-12'>
                        {!! $example->files('images') !!}
                    </div>
                @endif
                </div>

                <div class='col-md-12 col-sm-12'>
                @if ($mode == 'edit' || $mode == 'create')
                    <label for="file" class="control-label text-left">
                        {{trans('example::example.label.file') }}
                    </label>
                    <div class='clearfix'>
                        {!! $example->files('file')
                        -> url($example->getUploadUrl('file'))
                        -> mime(implode(',.', config('filer.allowed_extensions')))
                        -> uploader()!!}
                    </div>


                @elseif ($mode == 'show')
                    <label for="file" class="control-label text-left">
                        {{trans('example::example.label.file') }}
                    </label>
                    <div class='col-md-12'>
                        {!! $example->files('file') !!}
                    </div>

                @endif
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('month')
                       -> label(trans('example::example.label.month'))
                       -> placeholder(trans('example::example.placeholder.month'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('password')
                       -> label(trans('example::example.label.password'))
                       -> placeholder(trans('example::example.placeholder.password'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('range')
                       -> label(trans('example::example.label.range'))
                       -> placeholder(trans('example::example.placeholder.range'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('search')
                       -> label(trans('example::example.label.search'))
                       -> placeholder(trans('example::example.placeholder.search'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('tel')
                       -> label(trans('example::example.label.tel'))
                       -> placeholder(trans('example::example.placeholder.tel'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('time')
                       -> label(trans('example::example.label.time'))
                       -> placeholder(trans('example::example.placeholder.time'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('url')
                       -> label(trans('example::example.label.url'))
                       -> placeholder(trans('example::example.placeholder.url'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('week')
                       -> label(trans('example::example.label.week'))
                       -> placeholder(trans('example::example.placeholder.week'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('date_picker')
                       -> label(trans('example::example.label.date_picker'))
                       -> placeholder(trans('example::example.placeholder.date_picker'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('time_picker')
                       -> label(trans('example::example.label.time_picker'))
                       -> placeholder(trans('example::example.placeholder.time_picker'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('date_time_picker')
                       -> label(trans('example::example.label.date_time_picker'))
                       -> placeholder(trans('example::example.placeholder.date_time_picker'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('radios')
                       -> label(trans('example::example.label.radios'))
                       -> placeholder(trans('example::example.placeholder.radios'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('checkboxes')
                       -> label(trans('example::example.label.checkboxes'))
                       -> placeholder(trans('example::example.placeholder.checkboxes'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('switch')
                       -> label(trans('example::example.label.switch'))
                       -> placeholder(trans('example::example.placeholder.switch'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('select')
                       -> label(trans('example::example.label.select'))
                       -> placeholder(trans('example::example.placeholder.select'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('model_select')
                       -> label(trans('example::example.label.model_select'))
                       -> placeholder(trans('example::example.placeholder.model_select'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('tinyints')
                       -> label(trans('example::example.label.tinyints'))
                       -> placeholder(trans('example::example.placeholder.tinyints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('smallints')
                       -> label(trans('example::example.label.smallints'))
                       -> placeholder(trans('example::example.placeholder.smallints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('mediumints')
                       -> label(trans('example::example.label.mediumints'))
                       -> placeholder(trans('example::example.placeholder.mediumints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('ints')
                       -> label(trans('example::example.label.ints'))
                       -> placeholder(trans('example::example.placeholder.ints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('bigints')
                       -> label(trans('example::example.label.bigints'))
                       -> placeholder(trans('example::example.placeholder.bigints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('decimals')
                       -> label(trans('example::example.label.decimals'))
                       -> placeholder(trans('example::example.placeholder.decimals'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('floats')
                       -> label(trans('example::example.label.floats'))
                       -> placeholder(trans('example::example.placeholder.floats'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('doubles')
                       -> label(trans('example::example.label.doubles'))
                       -> placeholder(trans('example::example.placeholder.doubles'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('reals')
                       -> label(trans('example::example.label.reals'))
                       -> placeholder(trans('example::example.placeholder.reals'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('bits')
                       -> label(trans('example::example.label.bits'))
                       -> placeholder(trans('example::example.placeholder.bits'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('booleans')
                       -> label(trans('example::example.label.booleans'))
                       -> placeholder(trans('example::example.placeholder.booleans'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('dates')
                       -> label(trans('example::example.label.dates'))
                       -> placeholder(trans('example::example.placeholder.dates'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('datetimes')
                       -> label(trans('example::example.label.datetimes'))
                       -> placeholder(trans('example::example.placeholder.datetimes'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('timestamps')
                       -> label(trans('example::example.label.timestamps'))
                       -> placeholder(trans('example::example.placeholder.timestamps'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('times')
                       -> label(trans('example::example.label.times'))
                       -> placeholder(trans('example::example.placeholder.times'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('years')
                       -> label(trans('example::example.label.years'))
                       -> placeholder(trans('example::example.placeholder.years'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('chars')
                       -> label(trans('example::example.label.chars'))
                       -> placeholder(trans('example::example.placeholder.chars'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('varchars')
                       -> label(trans('example::example.label.varchars'))
                       -> placeholder(trans('example::example.placeholder.varchars'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('tinytexts')
                       -> label(trans('example::example.label.tinytexts'))
                       -> placeholder(trans('example::example.placeholder.tinytexts'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('texts')
                       -> label(trans('example::example.label.texts'))
                       -> placeholder(trans('example::example.placeholder.texts'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('mediumtexts')
                       -> label(trans('example::example.label.mediumtexts'))
                       -> placeholder(trans('example::example.placeholder.mediumtexts'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('longtexts')
                       -> label(trans('example::example.label.longtexts'))
                       -> placeholder(trans('example::example.placeholder.longtexts'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('binarys')
                       -> label(trans('example::example.label.binarys'))
                       -> placeholder(trans('example::example.placeholder.binarys'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('varbinarys')
                       -> label(trans('example::example.label.varbinarys'))
                       -> placeholder(trans('example::example.placeholder.varbinarys'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('tinyblobs')
                       -> label(trans('example::example.label.tinyblobs'))
                       -> placeholder(trans('example::example.placeholder.tinyblobs'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('mediumblobs')
                       -> label(trans('example::example.label.mediumblobs'))
                       -> placeholder(trans('example::example.placeholder.mediumblobs'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('blobs')
                       -> label(trans('example::example.label.blobs'))
                       -> placeholder(trans('example::example.placeholder.blobs'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('longblobs')
                       -> label(trans('example::example.label.longblobs'))
                       -> placeholder(trans('example::example.placeholder.longblobs'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('enums')
                       -> label(trans('example::example.label.enums'))
                       -> placeholder(trans('example::example.placeholder.enums'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('sets')
                       -> label(trans('example::example.label.sets'))
                       -> placeholder(trans('example::example.placeholder.sets'))!!}
                </div>
              </div>