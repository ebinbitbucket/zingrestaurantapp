            <table class="table" id="main-table" data-url="{{guard_url('example/example?withdata=Y')}}">
                <thead>
                    <tr>
                        <th data-field="name">{!! trans('example::example.label.name')!!}</th>
                        <th data-field="email">{!! trans('example::example.label.email')!!}</th>
                        <th data-field="color">{!! trans('example::example.label.color')!!}</th>
                        <th data-field="date">{!! trans('example::example.label.date')!!}</th>
                        <th data-field="actions"  data-formatter="operateFormatter" class="text-right">{{__('app.actions')}}</th>
                    </tr>
                </thead>
            </table>