

    <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="id">
                    {!! trans('example::example.label.id') !!}
                </label><br />
                    {!! $example['id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="name">
                    {!! trans('example::example.label.name') !!}
                </label><br />
                    {!! $example['name'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="email">
                    {!! trans('example::example.label.email') !!}
                </label><br />
                    {!! $example['email'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="color">
                    {!! trans('example::example.label.color') !!}
                </label><br />
                    {!! $example['color'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="date">
                    {!! trans('example::example.label.date') !!}
                </label><br />
                    {!! $example['date'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="datetime">
                    {!! trans('example::example.label.datetime') !!}
                </label><br />
                    {!! $example['datetime'] !!}
            </div>
        </div>
        <div class="col-md-12">
            <label for="file" class="control-label text-left">
                {{trans('example::example.label.file') }}
            </label>
            <div class='col-md-12'>
                {!! $example->files('file') !!}
            </div>        
        </div>
        <div class="col-md-12">

            <label for="files" class="control-label text-left">
                {{trans('example::example.label.files') }}
            </label>
            <div class='col-md-12'>
                {!! $example->files('files') !!}
            </div>
        </div>
        <div class="col-md-12">
            <label for="images" class="control-label text-left">
                {{trans('example::example.label.image') }}
            </label>
            <div class='col-md-12'>
                {!! $example->files('image')->show() !!}
            </div>
        </div>
        <div class="col-md-12">
            <label for="images" class="control-label text-left">
                {{trans('example::example.label.images') }}
            </label>
            <div class='col-md-12'>
                {!! $example->files('images')->show() !!}
            </div>

        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="month">
                    {!! trans('example::example.label.month') !!}
                </label><br />
                    {!! $example['month'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="password">
                    {!! trans('example::example.label.password') !!}
                </label><br />
                    {!! $example['password'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="range">
                    {!! trans('example::example.label.range') !!}
                </label><br />
                    {!! $example['range'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="search">
                    {!! trans('example::example.label.search') !!}
                </label><br />
                    {!! $example['search'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tel">
                    {!! trans('example::example.label.tel') !!}
                </label><br />
                    {!! $example['tel'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="time">
                    {!! trans('example::example.label.time') !!}
                </label><br />
                    {!! $example['time'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="url">
                    {!! trans('example::example.label.url') !!}
                </label><br />
                    {!! $example['url'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="week">
                    {!! trans('example::example.label.week') !!}
                </label><br />
                    {!! $example['week'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="date_picker">
                    {!! trans('example::example.label.date_picker') !!}
                </label><br />
                    {!! $example['date_picker'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="time_picker">
                    {!! trans('example::example.label.time_picker') !!}
                </label><br />
                    {!! $example['time_picker'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="date_time_picker">
                    {!! trans('example::example.label.date_time_picker') !!}
                </label><br />
                    {!! $example['date_time_picker'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="radios">
                    {!! trans('example::example.label.radios') !!}
                </label><br />
                    {!! $example['radios'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="checkboxes">
                    {!! trans('example::example.label.checkboxes') !!}
                </label><br />
                    {!! $example['checkboxes'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="switch">
                    {!! trans('example::example.label.switch') !!}
                </label><br />
                    {!! $example['switch'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="select">
                    {!! trans('example::example.label.select') !!}
                </label><br />
                    {!! $example['select'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="model_select">
                    {!! trans('example::example.label.model_select') !!}
                </label><br />
                    {!! $example['model_select'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tinyints">
                    {!! trans('example::example.label.tinyints') !!}
                </label><br />
                    {!! $example['tinyints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="smallints">
                    {!! trans('example::example.label.smallints') !!}
                </label><br />
                    {!! $example['smallints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="mediumints">
                    {!! trans('example::example.label.mediumints') !!}
                </label><br />
                    {!! $example['mediumints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="ints">
                    {!! trans('example::example.label.ints') !!}
                </label><br />
                    {!! $example['ints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="bigints">
                    {!! trans('example::example.label.bigints') !!}
                </label><br />
                    {!! $example['bigints'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="decimals">
                    {!! trans('example::example.label.decimals') !!}
                </label><br />
                    {!! $example['decimals'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="floats">
                    {!! trans('example::example.label.floats') !!}
                </label><br />
                    {!! $example['floats'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="doubles">
                    {!! trans('example::example.label.doubles') !!}
                </label><br />
                    {!! $example['doubles'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="reals">
                    {!! trans('example::example.label.reals') !!}
                </label><br />
                    {!! $example['reals'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="bits">
                    {!! trans('example::example.label.bits') !!}
                </label><br />
                    {!! $example['bits'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="booleans">
                    {!! trans('example::example.label.booleans') !!}
                </label><br />
                    {!! $example['booleans'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="dates">
                    {!! trans('example::example.label.dates') !!}
                </label><br />
                    {!! $example['dates'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="datetimes">
                    {!! trans('example::example.label.datetimes') !!}
                </label><br />
                    {!! $example['datetimes'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="timestamps">
                    {!! trans('example::example.label.timestamps') !!}
                </label><br />
                    {!! $example['timestamps'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="times">
                    {!! trans('example::example.label.times') !!}
                </label><br />
                    {!! $example['times'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="years">
                    {!! trans('example::example.label.years') !!}
                </label><br />
                    {!! $example['years'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="chars">
                    {!! trans('example::example.label.chars') !!}
                </label><br />
                    {!! $example['chars'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="varchars">
                    {!! trans('example::example.label.varchars') !!}
                </label><br />
                    {!! $example['varchars'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tinytexts">
                    {!! trans('example::example.label.tinytexts') !!}
                </label><br />
                    {!! $example['tinytexts'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="texts">
                    {!! trans('example::example.label.texts') !!}
                </label><br />
                    {!! $example['texts'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="mediumtexts">
                    {!! trans('example::example.label.mediumtexts') !!}
                </label><br />
                    {!! $example['mediumtexts'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="longtexts">
                    {!! trans('example::example.label.longtexts') !!}
                </label><br />
                    {!! $example['longtexts'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="binarys">
                    {!! trans('example::example.label.binarys') !!}
                </label><br />
                    {!! $example['binarys'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="varbinarys">
                    {!! trans('example::example.label.varbinarys') !!}
                </label><br />
                    {!! $example['varbinarys'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="tinyblobs">
                    {!! trans('example::example.label.tinyblobs') !!}
                </label><br />
                    {!! $example['tinyblobs'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="mediumblobs">
                    {!! trans('example::example.label.mediumblobs') !!}
                </label><br />
                    {!! $example['mediumblobs'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="blobs">
                    {!! trans('example::example.label.blobs') !!}
                </label><br />
                    {!! $example['blobs'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="longblobs">
                    {!! trans('example::example.label.longblobs') !!}
                </label><br />
                    {!! $example['longblobs'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="enums">
                    {!! trans('example::example.label.enums') !!}
                </label><br />
                    {!! $example['enums'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="sets">
                    {!! trans('example::example.label.sets') !!}
                </label><br />
                    {!! $example['sets'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="slug">
                    {!! trans('example::example.label.slug') !!}
                </label><br />
                    {!! $example['slug'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="status">
                    {!! trans('example::example.label.status') !!}
                </label><br />
                    {!! $example['status'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_id">
                    {!! trans('example::example.label.user_id') !!}
                </label><br />
                    {!! $example['user_id'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="user_type">
                    {!! trans('example::example.label.user_type') !!}
                </label><br />
                    {!! $example['user_type'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="upload_folder">
                    {!! trans('example::example.label.upload_folder') !!}
                </label><br />
                    {!! $example['upload_folder'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="deleted_at">
                    {!! trans('example::example.label.deleted_at') !!}
                </label><br />
                    {!! $example['deleted_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="created_at">
                    {!! trans('example::example.label.created_at') !!}
                </label><br />
                    {!! $example['created_at'] !!}
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class"form-group">
                <label for="updated_at">
                    {!! trans('example::example.label.updated_at') !!}
                </label><br />
                    {!! $example['updated_at'] !!}
            </div>
        </div>
    </div>