
<table width="570" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td bgcolor="#E2E1DE"><table width="100%" border="0" cellspacing="0" cellpadding="10">
            <tr>
                <td width="13%"><img src="{!! theme_asset('img/logo.svg') !!}" style="padding-left:10px;"></td>
                <td width="87%" align="right" valign="bottom"><h1><span style="color: #270188; font-size: 26px; padding-bottom:10px; font-family: Arial, Helvetica, sans-serif; padding-right: 10px;">Example Details</span></h1></td>
            </tr>
        </table></td>
    </tr>
    <tr>
        <td valign="top" class="content">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="50"><br /><br /><span>Lead ref : {!!@$examples['ref']!!}</span></td>
            </tr>
            <tr>
                <td height="50"><br /><span>{!!nl2br(@$examples['details'])!!}</span><br /><br /></td>
            </tr>
          </table>
          
          {!!@$examples['example']!!}

        </td>
    </tr>
   <tr>
      <td bgcolor="#E2E1DE" style="padding-left:10px;">Footer</td>
   
    </tr>
</table>