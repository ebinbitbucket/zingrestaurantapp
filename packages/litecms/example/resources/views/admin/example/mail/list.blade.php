
<table width="570" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td bgcolor="#E2E1DE"><table width="100%" border="0" cellspacing="0" cellpadding="10">
            <tr>
                <td width="13%"><img src="{!! theme_asset('img/logo.svg') !!}" style="padding-left:10px;"></td>
                <td width="87%" align="right" valign="bottom"><h1><span style="color: #270188; font-size: 26px; padding-bottom:10px; font-family: Arial, Helvetica, sans-serif; padding-right: 10px;">Example Details</span></h1></td>
            </tr>
        </table></td>
    </tr>
    <tr>
        <td valign="top" class="content">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td height="50"><br /><br /><span>Lead ref : {{@$example['ref']}}</span></td>
            </tr>
            <tr>
                <td height="50"><br /><span>{{nl2br(@$details)}}</span><br /><br /></td>
            </tr>
          </table>
          @forelse($examples as $key => $example)
          <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 10px;">
              <td>
                <span>{{ trans('example::example.label.name') }} : {{ @$example['name'] }} </span><br />
                <span>{{ trans('example::example.label.email') }} : {{ @$example['email'] }} </span><br />
                <span>{{ trans('example::example.label.color') }} : {{ @$example['color'] }} </span><br />
                <span>{{ trans('example::example.label.date') }} : {{ @$example['date'] }} </span><br />
                <span>{{ trans('example::example.label.datetime') }} : {{ @$example['datetime'] }} </span><br />
                <span>{{ trans('example::example.label.file') }} : {{ @$example['file'] }} </span><br />
                <span><hr></span>
              </td>
          </table>
          @empty
          @endif


        </td>
    </tr>
   <tr>
      <td bgcolor="#E2E1DE" style="padding-left:10px;"><!-- <img src="{!! theme_asset('img/logo.svg')!!}" > -->Footer</td>
    </tr>
</table>