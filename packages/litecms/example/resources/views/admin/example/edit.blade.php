    <div class="nav-tabs-custom">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs primary">
            <li class="active"><a href="#example" data-toggle="tab">{!! trans('example::example.tab.name') !!}</a></li>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-primary btn-sm" data-action='UPDATE' data-form='#example-example-edit'  data-load-to='#example-example-entry' data-datatable='#example-example-list'><i class="fa fa-floppy-o"></i> {{ trans('app.save') }}</button>
                <button type="button" class="btn btn-default btn-sm" data-action='CANCEL' data-load-to='#example-example-entry' data-href='{{guard_url('example/example')}}/{{$example->getRouteKey()}}'><i class="fa fa-times-circle"></i> {{ trans('app.cancel') }}</button>

            </div>
        </ul>
        {!!Form::vertical_open()
        ->id('example-example-edit')
        ->method('PUT')
        ->enctype('multipart/form-data')
        ->action(guard_url('example/example/'. $example->getRouteKey()))!!}
        <div class="tab-content clearfix">
            <div class="tab-pane active" id="example">
                <div class="tab-pan-title">  {{ trans('app.edit') }}  {!! trans('example::example.name') !!} [{!!$example->name!!}] </div>
                @include('example::admin.example.partial.entry', ['mode' => 'edit'])
            </div>
        </div>
        {!!Form::close()!!}
    </div>