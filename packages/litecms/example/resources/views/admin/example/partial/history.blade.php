@php
  $revisions    = $example->revisionHistory;
@endphp
<div class="modal fade" id="revision-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #dd4b39;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h3 class="modal-title">Update History</h3>
            </div>
            <div class="modal-body">
            <table id="revision-list" class="table table-striped  data-table" width="100%">
                <thead  class="list_head">
                    <th>User Name</th>
                    <th>Field</th>
                    <th>Old Value</th>  
                    <th>New Value</th>
                    <th>Updated</th>
                </thead>
                <tbody>
                    @forelse($revisions as $key => $value)
                    <tr>
                        <td>{!! @$value->user->name !!}</td>
                        <td><strong>{!! trans(@$label.@$value->field) !!}</strong>({!!@$value->field !!})</td>
                        <td>{!! @$value->old_value !!}</td>
                        <td>{!! @$value->new_value !!}</td>            
                        <td>{!! dateformat($value->created_at) !!}</td>            
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">Nothing to display...</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
            </div>
        </div>          
    </div>
</div>