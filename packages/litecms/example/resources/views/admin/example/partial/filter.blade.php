<div class="btn-group example-example">
    <button class="btn btn-xs btn-danger btn-search" type="button">
        <i aria-hidden="true" class="fa fa-search">
        </i>
        <span class="hidden-sm hidden-xs"> Search</span>
    </button>
    <button aria-expanded="false" class="btn btn-xs btn-danger dropdown-toggle" data-toggle="dropdown" type="button">
        <span class="caret">
        </span>
        <span class="sr-only">
            Toggle Dropdown
        </span>
    </button>
    <ul class="dropdown-menu" role="menu">
        <li>
            <a class="btn-search" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-filter">
                </i>
                Show filters
            </a>
        </li>
        <li>
            <a class="btn-reset-filter" style="cursor:pointer;">
                <i class="fa fa-fw fa-ban text-danger">
                </i>
                Clear filters
            </a>
        </li>
        <li class="divider">
        </li>
        <li>
            <a class="btn-save" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-floppy-o">
                </i>
                Save search
            </a>
        </li>
        <li>
            <a class="btn-open" style="cursor:pointer;">
                <i aria-hidden="true" class="fa fa-fw fa-folder-open-o">
                </i>
                Saved searches
            </a>
        </li>
    </ul>
</div>

<div class="modal fade" id="modal-search">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #dd4b39; color: #fff;">
              <button type="button" class="close" data-dismiss="modal" aaria-hidden="true">&times;</button>
              <h4 class="modal-title">Search</h4>
            </div>
              {!!Form::horizontal_open()
              ->id('form-search')
              ->method('POST')
              ->action(guard_url('settings/settings'))!!}
                <div class="modal-body has-form clearfix">
                    <div class="modal-form">
<div class="container-fluid">
                            <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[name]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.name')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[name]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[email]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.email')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[email]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[color]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.color')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[color]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[date]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.date')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[date]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[datetime]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.datetime')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[datetime]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[file]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.file')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[file]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[files]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.files')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[files]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[image]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.image')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[image]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[images]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.images')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[images]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[month]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.month')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[month]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[password]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.password')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[password]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[range]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.range')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[range]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[search]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.search')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[search]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[tel]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.tel')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[tel]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[time]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.time')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[time]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[url]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.url')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[url]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[week]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.week')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[week]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[date_picker]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.date_picker')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[date_picker]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[time_picker]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.time_picker')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[time_picker]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[date_time_picker]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.date_time_picker')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[date_time_picker]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[radios]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.radios')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[radios]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[checkboxes]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.checkboxes')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[checkboxes]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[switch]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.switch')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[switch]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[select]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.select')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[select]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[model_select]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.model_select')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[model_select]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[tinyints]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.tinyints')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[tinyints]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[smallints]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.smallints')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[smallints]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[mediumints]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.mediumints')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[mediumints]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[ints]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.ints')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[ints]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[bigints]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.bigints')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[bigints]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[decimals]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.decimals')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[decimals]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[floats]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.floats')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[floats]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[doubles]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.doubles')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[doubles]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[reals]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.reals')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[reals]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[bits]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.bits')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[bits]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[booleans]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.booleans')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[booleans]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[dates]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.dates')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[dates]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[datetimes]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.datetimes')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[datetimes]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[timestamps]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.timestamps')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[timestamps]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[times]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.times')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[times]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[years]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.years')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[years]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[chars]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.chars')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[chars]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[varchars]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.varchars')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[varchars]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[texts]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.texts')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[texts]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[blobs]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.blobs')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[blobs]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[enums]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.enums')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[enums]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[sets]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.sets')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[sets]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                     
                                    <label for="search[PRIMARY]" class="col-sm-2 control-label">
                                        {!! trans('example::example.label.PRIMARY')!!}
                                    </label>
                                    <div class="col-sm-10">
                                        {!! Form::text('search[PRIMARY]')->raw()!!}
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 col-lg-12">
                        <button aria-label="Close" class="btn pull-right btn-danger" data-dismiss="modal" type="button">
                            <i class="fa fa-times-circle">
                            </i>
                            Close
                        </button>
                        <button class="btn btn-success pull-right " id="btn-apply-search" name="new" style="margin-right:1%" type="button">
                            <i class="fa fa-check-circle">
                            </i>
                            Search
                        </button>
                    </div>
                </div>
              {!!Form::close()!!}
        </div>
    </div>
</div>


<div class="modal fade" id="modal-open">
  <div class="modal-dialog">
    <div class="modal-content" style="max-width:400px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Saved</h4>
      </div>
      <div class="modal-body" style="height:210px; overflow-y: auto;">
        
        <div id="saved-list">
          
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger"  name="Closerep" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close </button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
$(document).ready(function(){

    $(".example-example .btn-open").click(function(){
        toastr.info('This feature will be enabled soon.', 'Coming soon');
        return false;
        $('#open-list').load("{!!guard_url('/settings/setting/search/example.example.search')!!}");
        $('#modal-open').modal("show");
    });

   $(".example-example .btn-search").click(function(){
      $('#modal-search').modal("show");
    });
   
    $('.example-example .btn-save').click(function(e){
        toastr.info('This feature will be enabled soon.', 'Coming soon');
        return false;
        var search = prompt("Please enter name for your search");
        if (search == null) {
            toastr.error('Please enter valid name.', 'Error');
            return false;
        }
        var formData = new FormData();
        formData.append('value', $("#form-search").serialize());
        formData.append('name', search);
        formData.append('key', 'example.example.search');
        formData.append('package', 'Page');
        formData.append('module', 'Page');

        $.ajax({
            url : "{!!guard_url('/settings/setting')!!}",
            type: "POST",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success:function(data, textStatus, jqXHR)
            {
                toastr.success('Search saved successfully.', 'Success');
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                toastr.error('An error occurred while saving.', 'Error');
            }
        });

        e.preventDefault();
    });

    $('#btn-apply-search').click( function() {
        oSearch = {};
        $('#form-search input,#form-search select').each( function () {
          key = $(this).attr('name');
          val = $(this).val();
          oSearch[key] = val;
        });
        oTable.api().draw();
        $('#example-example-list .btn-reset-filter').css('display', '');
        $('#modal-search').modal("hide");
        
      });
    
    $(".btn-reset-filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        oSearch = {};
        $('#form-search input,#form-search select').each( function () {
          key = $(this).attr('name');
          val = $(this).val();
          oSearch[key] = val;
        });
        oTable.api().draw();
        $('#example-example-list .btn-reset-filter').css('display', 'none');

    });

});
</script>