            <div class='row'>
                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('name')
                       -> label(trans('example::example.label.name'))
                       -> placeholder(trans('example::example.placeholder.name'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('email')
                       -> label(trans('example::example.label.email'))
                       -> placeholder(trans('example::example.placeholder.email'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('color')
                       -> label(trans('example::example.label.color'))
                       -> placeholder(trans('example::example.placeholder.color'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   <div class='form-group'>
                     <label for='date' class='control-label'>{!!trans('example::example.label.date')!!}</label>
                     <div class='input-group pickdate'>
                        {!! Form::text('date')
                        -> placeholder(trans('example::example.placeholder.date'))
                        ->raw()!!}
                       <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                     </div>
                   </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='datetime' class='control-label'>{!!trans('example::example.label.datetime')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('datetime')
                            -> placeholder(trans('example::example.placeholder.datetime'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('image')
                       -> label(trans('example::example.label.image'))
                       -> placeholder(trans('example::example.placeholder.image'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('images')
                       -> label(trans('example::example.label.images'))
                       -> placeholder(trans('example::example.placeholder.images'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('month')
                       -> label(trans('example::example.label.month'))
                       -> placeholder(trans('example::example.placeholder.month'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('password')
                       -> label(trans('example::example.label.password'))
                       -> placeholder(trans('example::example.placeholder.password'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('range')
                       -> label(trans('example::example.label.range'))
                       -> placeholder(trans('example::example.placeholder.range'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('search')
                       -> label(trans('example::example.label.search'))
                       -> placeholder(trans('example::example.placeholder.search'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('tel')
                       -> label(trans('example::example.label.tel'))
                       -> placeholder(trans('example::example.placeholder.tel'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('time')
                       -> label(trans('example::example.label.time'))
                       -> placeholder(trans('example::example.placeholder.time'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('url')
                       -> label(trans('example::example.label.url'))
                       -> placeholder(trans('example::example.placeholder.url'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('week')
                       -> label(trans('example::example.label.week'))
                       -> placeholder(trans('example::example.placeholder.week'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   <div class='form-group'>
                     <label for='date_picker' class='control-label'>{!!trans('example::example.label.date_picker')!!}</label>
                     <div class='input-group pickdate'>
                        {!! Form::text('date_picker')
                        -> placeholder(trans('example::example.placeholder.date_picker'))
                        ->raw()!!}
                       <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                     </div>
                   </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='time_picker' class='control-label'>{!!trans('example::example.label.time_picker')!!}</label>
                        <div class='input-group picktime'>
                            {!! Form::text('time_picker')
                            -> placeholder(trans('example::example.placeholder.time_picker'))
                            -> raw()!!}
                            <span class='input-group-addon'><i class='fa fa-clock-o'></i></span>
                        </div>
                    </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='date_time_picker' class='control-label'>{!!trans('example::example.label.date_time_picker')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('date_time_picker')
                            -> placeholder(trans('example::example.placeholder.date_time_picker'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('radios')
                       -> label(trans('example::example.label.radios'))
                       -> placeholder(trans('example::example.placeholder.radios'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('checkboxes')
                       -> label(trans('example::example.label.checkboxes'))
                       -> placeholder(trans('example::example.placeholder.checkboxes'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('switch')
                       -> label(trans('example::example.label.switch'))
                       -> placeholder(trans('example::example.placeholder.switch'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('select')
                   -> radios(trans('example::example.options.select'))
                   -> label(trans('example::example.label.select'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('model_select')
                   -> radios(trans('example::example.options.model_select'))
                   -> label(trans('example::example.label.model_select'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('tinyints')
                       -> label(trans('example::example.label.tinyints'))
                       -> placeholder(trans('example::example.placeholder.tinyints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('smallints')
                       -> label(trans('example::example.label.smallints'))
                       -> placeholder(trans('example::example.placeholder.smallints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('mediumints')
                       -> label(trans('example::example.label.mediumints'))
                       -> placeholder(trans('example::example.placeholder.mediumints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('ints')
                       -> label(trans('example::example.label.ints'))
                       -> placeholder(trans('example::example.placeholder.ints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('bigints')
                       -> label(trans('example::example.label.bigints'))
                       -> placeholder(trans('example::example.placeholder.bigints'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('decimals')
                       -> label(trans('example::example.label.decimals'))
                       -> placeholder(trans('example::example.placeholder.decimals'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('floats')
                       -> label(trans('example::example.label.floats'))
                       -> placeholder(trans('example::example.placeholder.floats'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('doubles')
                       -> label(trans('example::example.label.doubles'))
                       -> placeholder(trans('example::example.placeholder.doubles'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::decimal('reals')
                       -> label(trans('example::example.label.reals'))
                       -> placeholder(trans('example::example.placeholder.reals'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('bits')
                       -> label(trans('example::example.label.bits'))
                       -> placeholder(trans('example::example.placeholder.bits'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('booleans')
                       -> label(trans('example::example.label.booleans'))
                       -> placeholder(trans('example::example.placeholder.booleans'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   <div class='form-group'>
                     <label for='dates' class='control-label'>{!!trans('example::example.label.dates')!!}</label>
                     <div class='input-group pickdate'>
                        {!! Form::text('dates')
                        -> placeholder(trans('example::example.placeholder.dates'))
                        ->raw()!!}
                       <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                     </div>
                   </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='datetimes' class='control-label'>{!!trans('example::example.label.datetimes')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('datetimes')
                            -> placeholder(trans('example::example.placeholder.datetimes'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='timestamps' class='control-label'>{!!trans('example::example.label.timestamps')!!}</label>
                        <div class='input-group pickdatetime'>
                            {!! Form::text('timestamps')
                            -> placeholder(trans('example::example.placeholder.timestamps'))
                            -> addClass('pickdatetime')
                            ->raw()!!}
                           <span class='input-group-addon'><i class='fa fa-calendar'></i></span>
                        </div>
                    </div>
                 </div>

                <div class='col-md-4 col-sm-6'>
                    <div class='form-group'>
                        <label for='times' class='control-label'>{!!trans('example::example.label.times')!!}</label>
                        <div class='input-group picktime'>
                            {!! Form::text('times')
                            -> placeholder(trans('example::example.placeholder.times'))
                            -> raw()!!}
                            <span class='input-group-addon'><i class='fa fa-clock-o'></i></span>
                        </div>
                    </div>
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::numeric('years')
                       -> label(trans('example::example.label.years'))
                       -> placeholder(trans('example::example.placeholder.years'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('chars')
                       -> label(trans('example::example.label.chars'))
                       -> placeholder(trans('example::example.placeholder.chars'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('varchars')
                       -> label(trans('example::example.label.varchars'))
                       -> placeholder(trans('example::example.placeholder.varchars'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('tinytexts')
                    -> label(trans('example::example.label.tinytexts'))
                    -> placeholder(trans('example::example.placeholder.tinytexts'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('texts')
                       -> label(trans('example::example.label.texts'))
                       -> placeholder(trans('example::example.placeholder.texts'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('mediumtexts')
                    -> label(trans('example::example.label.mediumtexts'))
                    -> placeholder(trans('example::example.placeholder.mediumtexts'))!!}
                </div>

                <div class='col-md-12'>
                    {!! Form::textarea('longtexts')
                    -> label(trans('example::example.label.longtexts'))
                    -> dataUpload(trans_url($example->getUploadURL('longtexts')))
                    -> addClass('html-editor')
                    -> placeholder(trans('example::example.placeholder.longtexts'))!!}
                </div>
                <div class='col-md-12'>
                    {!! Form::textarea('binarys')
                    -> label(trans('example::example.label.binarys'))
                    -> dataUpload(trans_url($example->getUploadURL('binarys')))
                    -> addClass('html-editor')
                    -> placeholder(trans('example::example.placeholder.binarys'))!!}
                </div>
                <div class='col-md-12'>
                    {!! Form::textarea('varbinarys')
                    -> label(trans('example::example.label.varbinarys'))
                    -> dataUpload(trans_url($example->getUploadURL('varbinarys')))
                    -> addClass('html-editor')
                    -> placeholder(trans('example::example.placeholder.varbinarys'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('tinyblobs')
                    -> label(trans('example::example.label.tinyblobs'))
                    -> placeholder(trans('example::example.placeholder.tinyblobs'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                    {!! Form::textarea ('mediumblobs')
                    -> label(trans('example::example.label.mediumblobs'))
                    -> placeholder(trans('example::example.placeholder.mediumblobs'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                       {!! Form::text('blobs')
                       -> label(trans('example::example.label.blobs'))
                       -> placeholder(trans('example::example.placeholder.blobs'))!!}
                </div>

                <div class='col-md-12'>
                    {!! Form::textarea('longblobs')
                    -> label(trans('example::example.label.longblobs'))
                    -> dataUpload(trans_url($example->getUploadURL('longblobs')))
                    -> addClass('html-editor')
                    -> placeholder(trans('example::example.placeholder.longblobs'))!!}
                </div>
                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('enums')
                   -> radios(trans('example::example.options.enums'))
                   -> label(trans('example::example.label.enums'))!!}
                </div>

                <div class='col-md-4 col-sm-6'>
                   {!! Form::inline_radios('sets')
                   -> radios(trans('example::example.options.sets'))
                   -> label(trans('example::example.label.sets'))!!}
                </div>


            </div>