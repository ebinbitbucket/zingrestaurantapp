
        @if($example->hasStep('complete'))
            <button type="button" class="btn bg-purple btn-sm" data-action='WORKFLOW' data-method="PUT" data-load-to='#example-example-entry' data-href="{{trans_url('admin/example/example/workflow/'. $example->getRouteKey() .'/complete')}}" data-value="No" data-datatable='#example-example-list'><i class="fa fa-check"></i> Complete</button>
        @endif

        @if($example->hasStep('verify'))
            <button type="button" class="btn bg-olive btn-sm" data-action='WORKFLOW' data-method="PUT" data-load-to='#example-example-entry' data-href="{{trans_url('admin/example/example/workflow/'. $example->getRouteKey() .'/verify')}}" data-value="Yes" data-datatable='#example-example-list'><i class="fa fa-check"></i> Verify</button>
        @endif

        @if($example->hasStep('approve'))
            <button type="button" class="btn bg-aqua btn-sm" data-action='WORKFLOW' data-method="PUT" data-load-to='#example-example-entry' data-href="{{trans_url('admin/example/example/workflow/'. $example->getRouteKey() .'/approve')}}" data-value="Yes" data-datatable='#example-example-list'><i class="fa fa-check"></i> Approve</button>
        @endif

        @if($example->hasStep('publish'))
            <button type="button" class="btn bg-purple btn-sm" data-action='WORKFLOW' data-method="PUT" data-load-to='#example-example-entry' data-href="{{trans_url('admin/example/example/workflow/'. $example->getRouteKey() .'/publish')}}" data-value="Yes" data-datatable='#example-example-list'><i class="fa fa-check"></i> Publish</button>
        @endif

        @if($example->hasStep('unpublish'))
            <button type="button" class="btn bg-maroon btn-sm" data-action='WORKFLOW' data-method="PUT" data-load-to='#example-example-entry' data-href="{{trans_url('admin/example/example/workflow/'. $example->getRouteKey() .'/unpublish')}}" data-value="Yes" data-datatable='#example-example-list'><i class="fa fa-times-circle"></i> Unpublish</button>
        @endif

        @if($example->hasStep('archive'))
            <button type="button" class="btn bg-navy btn-sm" data-action='WORKFLOW' data-method="PUT" data-load-to='#example-example-entry' data-href="{{trans_url('admin/example/example/workflow/'. $example->getRouteKey() .'/archive')}}" data-value="Yes" data-datatable='#example-example-list'><i class="fa fa-file-archive-o "></i> Archive</button>
        @endif


@php
    $workflows    = $example->workflowHistory;
    $status = [
        'completed' => 'label label-success', 
        'pending'   => 'label label-warning', 
        'cancelled' => 'label label-danger'
    ];
@endphp
<div class="modal fade" id="workflow-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg ">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #dd4b39;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" >&times;</button>
                <h3 class="modal-title">Workflow history</h3>
            </div>
            <div class="modal-body">
            <table id="workflowable-list" class="table table-striped  data-table" width="100%">
                <thead class="list_head">
                    <th>Action</th>
                    <th>Status</th>
                    <th>Performed by</th>
                    <th>Comments</th>
                    <th>Date</th>
                </thead>
                <tbody>
                    @if (!empty($workflows))
                    @foreach($workflows as $key => $value)
                    <tr>
                        <td>{!! @$value->action !!}</td>
                        <td><label class="{{@$status[$value->status]}}">{!! @$value->status !!}</label></td>
                        <td>{!! @$value->performable->name !!}</td>            
                        <td>
                            @if(!empty($value->data) && $value->status == 'completed')
                            <a href='#' class='text-danger valueModal' data-id='{{$value->id}}'><input type='hidden' name='workflow-data{{$value->id}}' value='{{json_encode($value->data)}}'><i class='fa fa-file-text'></i></a>
                            @else
                            ---
                            @endif
                        </td>            
                        <td>{!! format_date_time($value->created_at) !!}</td>            
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">Nothing to display...</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div> 
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
            </div>
        </div>          
    </div>
</div>