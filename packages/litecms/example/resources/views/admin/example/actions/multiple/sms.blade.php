
{!!Form::horizontal_open()
  ->id('form-sms')
  ->method('POST')
  ->files('true')
  ->enctype('multipart/form-data')
  ->action(URL::to('admin/example/example/email/sms'))!!}

  <div class="modal-body has-form">
    <div class="modal-form" id="mail-content" style="min-height:200px;">
      <div class="clearfix">
        <div class="col-md-12" style="margin-top:17px;">

          <div class="form-group">
            <div class="col-md-3">
              To
            </div>
            <div class="col-md-9">
             {!! Form::number('to')
             -> required()
             -> style('border:1px solid red;')
             -> raw()!!}
           </div>
         </div>

         <div class="form-group">  
          <div class="col-md-12">
            Message
          </div>                          
          <div class="col-md-12" style="margin-top:20px;">
            {!!Form::textarea('details')
            ->rows(12)
            ->raw()!!}
          </div>
        </div>                      
        {!!Form::close()!!}  
      </div>
    </div>                    
  </div>
</div>
<div class="modal-footer">
  <div class="col-md-12 col-lg-12">
    <button  type="button" class="btn pull-right btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i>  Close</button>                              
    <button  type="button" class="btn btn-primary pull-right " id="btn-submit-sms" name="new" style="margin-right:1%"><i class="fa fa-check-circle"></i> Send SMS</button>
  </div>
</div>
{!!Form::close()!!}

<script type="text/javascript">
  $("#btn-submit-sms").click(function(e){
    var to = $("#to").val();
    if(to == '' || to.length < 1){
      $("#to").css('border','1px solid red');
      return false
    }
    var formData = new FormData();
    params   = $("#form-sms").serializeArray();
    $.each(params, function(i, val) {
        formData.append(val.name, val.value);
    });
    $.ajax({
      url : "{!! URL::to('admin/example/example/action/send/sms') !!}",
      type : "POST",
      data: formData,
      cache: false,
      processData: false,
      contentType: false,
      beforeSend: function() {
       $("#btn-submit-sms i").addClass('fa-spinner');
       $("#btn-submit-sms").prop('disabled',true);
     },
     success : function(data){
      $('#propModalSms').modal('toggle');
      $("#btn-submit-sms").prop('disabled',false);
      toastr.success('SMS has been sent.', 'Sent');
    }
  }); 
    e.preventDefault();  
  });
</script>