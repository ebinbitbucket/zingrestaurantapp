
{!!Form::horizontal_open()
  ->id('form-task')
  ->method('POST')
  ->files('true')
  ->enctype('multipart/form-data')
  ->action(url('admin/task/task'))!!}

    <div class="modal-body has-form clearfix">
        <div class="modal-form">
            <div class="col-md-12">
                {!!Form::hidden('new-status')->forceValue('to_do')!!}
                {!!Form::text('new-task')->forceValue(@$example->name)->required()!!}
                <div class="form-group form-group-sm">
                  <label for="start" class="control-label col-lg-3 col-md-6 col-sm-6"> 
                    {{trans('task::task.label.start')}}<sup>*</sup>
                  </label>
                  <div class="col-lg-4 col-md-6 col-sm-6">
                    {!! Form::date('start')
                    ->forceValue(@$example->created_at)
                    ->placeholder(trans('task::task.placeholder.start'))
                    ->addGroupClass('form-group-sm')
                    ->raw()!!}
                  </div>
                  <label for="time_required" class="control-label col-lg-2 col-md-6 col-sm-6"> 
                    {{trans('task::task.label.time_required')}}
                  </label>
                  <div class="col-lg-3 col-md-6 col-sm-6">
                    {!! Form::time('time_required')
                    ->placeholder(trans('task::task.placeholder.time_required'))
                    ->required()
                    ->addGroupClass('form-group-sm')
                    ->raw()!!}
                  </div>
                </div>
                {!!Form::select('priority')
                ->options(trans('task::task.options.priority'))!!}
                {!!Form::text('created')
                ->forceValue(user('admin.web')->name) 
                ->readonly() !!}

                {!!Form::select('assigned_to')
                ->options(@Task::users())!!}
            </div>

        </div>                 
    </div>
    <div class="modal-footer">
      <div class="col-md-12 col-lg-12">
        <button  type="button" class="btn pull-right btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i>  Close</button>                              
        <button  type="button" class="btn btn-primary pull-right " id="btn-submit-task" name="new" style="margin-right:1%"><i class="fa fa-check-circle"></i> Add Task</button>
      </div>
    </div>
    {!!Form::close()!!}

    <script type="text/javascript">
      $('#btn-submit-task').click( function(e) {
        
        var formData = new FormData();
        params   = $("#form-task").serializeArray();
        $.each(params, function(i, val) {
            formData.append(val.name, val.value);
        });

        $.ajax({
            url : "{!!url('admin/task/task')!!}",
            type: "POST",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            dataType: 'json',
            success:function(data, textStatus, jqXHR)
            {
                $('#action-modal').modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
        e.preventDefault();
      });
    </script>