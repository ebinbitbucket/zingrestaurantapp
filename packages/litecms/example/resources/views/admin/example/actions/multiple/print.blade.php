<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
                    body { margin: 0px; }
                    body,td,th {
                        font-family: Verdana, Geneva, sans-serif;
                        font-size: 12px;
                    }
                    
                    @page { margin: 0px 0px; }

        </style>
        <link rel="stylesheet" type="text/css" href="{{url('css/vendor.css')}}">
    </head>
    <body>
    <table width="100%" class="table">
        <tbody>
            <tr>
                <th>{!! trans('example::example.label.name')!!}</th>
                <th>{!! trans('example::example.label.email')!!}</th>
                <th>{!! trans('example::example.label.color')!!}</th>
                <th>{!! trans('example::example.label.date')!!}</th>
                <th>{!! trans('example::example.label.datetime')!!}</th>
                <th>{!! trans('example::example.label.month')!!}</th>
                <th>{!! trans('example::example.label.password')!!}</th>
                <th>{!! trans('example::example.label.range')!!}</th>
                <th>{!! trans('example::example.label.search')!!}</th>
                <th>{!! trans('example::example.label.tel')!!}</th>
                <th>{!! trans('example::example.label.time')!!}</th>
                <th>{!! trans('example::example.label.url')!!}</th>
                <th>{!! trans('example::example.label.week')!!}</th>
                <th>{!! trans('example::example.label.date_picker')!!}</th>
                <th>{!! trans('example::example.label.time_picker')!!}</th>
                <th>{!! trans('example::example.label.date_time_picker')!!}</th>
                <th>{!! trans('example::example.label.radios')!!}</th>
                <th>{!! trans('example::example.label.checkboxes')!!}</th>
                <th>{!! trans('example::example.label.switch')!!}</th>
                <th>{!! trans('example::example.label.select')!!}</th>
                <th>{!! trans('example::example.label.model_select')!!}</th>
                <th>{!! trans('example::example.label.tinyints')!!}</th>
                <th>{!! trans('example::example.label.smallints')!!}</th>
                <th>{!! trans('example::example.label.mediumints')!!}</th>
                <th>{!! trans('example::example.label.ints')!!}</th>
                <th>{!! trans('example::example.label.bigints')!!}</th>
                <th>{!! trans('example::example.label.decimals')!!}</th>
                <th>{!! trans('example::example.label.floats')!!}</th>
                <th>{!! trans('example::example.label.doubles')!!}</th>
                <th>{!! trans('example::example.label.reals')!!}</th>
                <th>{!! trans('example::example.label.bits')!!}</th>
                <th>{!! trans('example::example.label.booleans')!!}</th>
                <th>{!! trans('example::example.label.dates')!!}</th>
                <th>{!! trans('example::example.label.datetimes')!!}</th>
                <th>{!! trans('example::example.label.timestamps')!!}</th>
                <th>{!! trans('example::example.label.times')!!}</th>
                <th>{!! trans('example::example.label.years')!!}</th>
                <th>{!! trans('example::example.label.chars')!!}</th>
                <th>{!! trans('example::example.label.varchars')!!}</th>
                <th>{!! trans('example::example.label.tinytexts')!!}</th>
                <th>{!! trans('example::example.label.texts')!!}</th>
                <th>{!! trans('example::example.label.mediumtexts')!!}</th>
                <th>{!! trans('example::example.label.longtexts')!!}</th>
                <th>{!! trans('example::example.label.binarys')!!}</th>
                <th>{!! trans('example::example.label.varbinarys')!!}</th>
                <th>{!! trans('example::example.label.tinyblobs')!!}</th>
                <th>{!! trans('example::example.label.mediumblobs')!!}</th>
                <th>{!! trans('example::example.label.blobs')!!}</th>
                <th>{!! trans('example::example.label.longblobs')!!}</th>
                <th>{!! trans('example::example.label.enums')!!}</th>
                <th>{!! trans('example::example.label.sets')!!}</th>
                <th>{!! trans('example::example.label.status')!!}</th>
                <th>{!! trans('example::example.label.created_at')!!}</th>
                <th>{!! trans('example::example.label.updated_at')!!}</th>
            </tr>
            @forelse($examples as $key=>$example)
            <tr>
                <td> {{ @$example->name }} </td>
                <td> {{ @$example->email }} </td>
                <td> {{ @$example->color }} </td>
                <td> {{ @$example->date }} </td>
                <td> {{ @$example->datetime }} </td>
                <td> {{ @$example->month }} </td>
                <td> {{ @$example->password }} </td>
                <td> {{ @$example->range }} </td>
                <td> {{ @$example->search }} </td>
                <td> {{ @$example->tel }} </td>
                <td> {{ @$example->time }} </td>
                <td> {{ @$example->url }} </td>
                <td> {{ @$example->week }} </td>
                <td> {{ @$example->date_picker }} </td>
                <td> {{ @$example->time_picker }} </td>
                <td> {{ @$example->date_time_picker }} </td>
                <td> {{ @$example->radios }} </td>
                <td> {{ @$example->checkboxes }} </td>
                <td> {{ @$example->switch }} </td>
                <td> {{ @$example->select }} </td>
                <td> {{ @$example->model_select }} </td>
                <td> {{ @$example->tinyints }} </td>
                <td> {{ @$example->smallints }} </td>
                <td> {{ @$example->mediumints }} </td>
                <td> {{ @$example->ints }} </td>
                <td> {{ @$example->bigints }} </td>
                <td> {{ @$example->decimals }} </td>
                <td> {{ @$example->floats }} </td>
                <td> {{ @$example->doubles }} </td>
                <td> {{ @$example->reals }} </td>
                <td> {{ @$example->bits }} </td>
                <td> {{ @$example->booleans }} </td>
                <td> {{ @$example->dates }} </td>
                <td> {{ @$example->datetimes }} </td>
                <td> {{ @$example->timestamps }} </td>
                <td> {{ @$example->times }} </td>
                <td> {{ @$example->years }} </td>
                <td> {{ @$example->chars }} </td>
                <td> {{ @$example->varchars }} </td>
                <td> {{ @$example->tinytexts }} </td>
                <td> {{ @$example->texts }} </td>
                <td> {{ @$example->mediumtexts }} </td>
                <td> {{ @$example->longtexts }} </td>
                <td> {{ @$example->binarys }} </td>
                <td> {{ @$example->varbinarys }} </td>
                <td> {{ @$example->tinyblobs }} </td>
                <td> {{ @$example->mediumblobs }} </td>
                <td> {{ @$example->blobs }} </td>
                <td> {{ @$example->longblobs }} </td>
                <td> {{ @$example->enums }} </td>
                <td> {{ @$example->sets }} </td>
                <td> {{ @$example->status }} </td>
                <td> {{ @$example->created_at }} </td>
                <td> {{ @$example->updated_at }} </td>
           </tr>
           @empty
           <tr>
               <td colspan="57">
                   No data
               </td>
           </tr>
           @endif
          

        </tbody>
    </table>

    <script type="text/javascript">
        window.print();
    </script>
</body>
</html>