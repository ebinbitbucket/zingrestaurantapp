
  {!!Form::horizontal_open()
  ->id('form-search')
  ->method('POST')
  ->files('true')
  ->enctype('multipart/form-data')
  ->action(url('admin/search/search'))!!}

    <div class="modal-body has-form clearfix">
        <div class="modal-form">
            <div class="col-sm-4 col-md-4">
                {!! Form::text('search[name]')
                ->label(trans('example::example.label.name'))
                ->placeholder(trans('example::example.placeholder.name'))
                ->addGroupClass('form-group-sm')!!}

                {!! Form::email('search[email]')
                ->label(trans('example::example.label.email'))
                ->placeholder(trans('example::example.placeholder.email'))
                ->addGroupClass('form-group-sm')!!}

                {!! Form::text('search[color]')
                ->label(trans('example::example.label.color'))
                ->placeholder(trans('example::example.placeholder.color'))
                ->addGroupClass('form-group-sm')!!}

                {!! Form::number('search[tel]')
                ->label(trans('example::example.label.tel'))
                ->placeholder(trans('example::example.placeholder.tel'))
                ->addGroupClass('form-group-sm')!!}
                
                {!! Form::url('search[url]')
                ->label(trans('example::example.label.url'))
                ->placeholder(trans('example::example.placeholder.url'))
                ->addGroupClass('form-group-sm')!!}
                            
            </div>
            <div class="col-sm-4 col-md-4">
                

                
                {!! Form::period('search[range]')
                ->label(trans('example::example.label.range'))
                ->placeholder(trans('example::example.placeholder.range'))
                ->addGroupClass('form-group-sm')!!}
                
                {!! Form::date('search[date_picker]')
                ->label(trans('example::example.label.date_picker'))
                ->placeholder(trans('example::example.placeholder.date_picker'))
                ->addGroupClass('form-group-sm')!!}
                
                {!! Form::time('search[time_picker]')
                ->label(trans('example::example.label.time_picker'))
                ->placeholder(trans('example::example.placeholder.time_picker'))
                ->addGroupClass('form-group-sm')!!}
                                
                {!! Form::datetime('search[date_time_picker]')
                ->label(trans('example::example.label.date_time_picker'))
                ->placeholder(trans('example::example.placeholder.date_time_picker'))
                ->addGroupClass('form-group-sm')!!}
                {!! Form::switch('search[switch]')
                ->label(trans('example::example.label.switch'))
                ->placeholder(trans('example::example.placeholder.switch'))
                ->addGroupClass('form-group-sm')!!}
                
<!--                 {!! Form::radio('search[radios]')
->label(trans('example::example.label.radios'))
->placeholder(trans('example::example.placeholder.radios'))
->addGroupClass('form-group-sm')!!}

{!! Form::checkboxes('search[checkboxes][]')
->checkboxes(trans('example::example.options.checkboxes'))
->label(trans('example::example.label.checkboxes'))
->addGroupClass('form-group-sm')!!} -->
                
            </div>
            <div class="col-sm-4 col-md-4">
                
                
                
                {!! Form::select('search[select]')
                ->options(trans('example::example.options.select'))
                ->label(trans('example::example.label.select'))
                ->placeholder(trans('example::example.placeholder.select'))
                ->addGroupClass('form-group-sm')!!}
                
                {!! Form::decimal('search[tinyints]')
                ->label(trans('example::example.label.tinyints'))
                ->placeholder(trans('example::example.placeholder.tinyints'))
                ->addGroupClass('form-group-sm')!!}
                
                {!! Form::text('search[smallints]')
                ->label(trans('example::example.label.smallints'))
                ->placeholder(trans('example::example.placeholder.smallints'))
                ->addGroupClass('form-group-sm')!!}
                
                {!! Form::text('search[mediumints]')
                ->label(trans('example::example.label.mediumints'))
                ->placeholder(trans('example::example.placeholder.mediumints'))
                ->addGroupClass('form-group-sm')!!}
                
            </div>

        </div>                 
    </div>
    <div class="modal-footer">
      <div class="col-md-12 col-lg-12">
        <button  type="button" class="btn pull-right btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i>  Close</button>                              
        <button  type="button" class="btn btn-success pull-right " id="btn-submit-search" name="new" style="margin-right:1%"><i class="fa fa-check-circle"></i> Search Now</button>
      </div>
    </div>
    {!!Form::close()!!}

    <script type="text/javascript">

      $('#btn-submit-search').click( function() {

        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });

        $('#example-example-list .reset_filter').css('display', '');
        $('#modalAdvSearch').modal("hide");
        
      });
    </script>