
<form accept-charset="utf-8" class="form-vertical" id="createTodo" files="true" method="POST">
    <div class="form-body ">
        <div class="form-actions">
            <button type="button" id="SaveTodo" class="save_button"> <i class="fa fa-check"></i> Save &amp; Close</button>
            <div class="showdata" id="showpopupdata"></div>
            <div class="row">
                <div class="infobox" style="margin-bottom:30px;">
                    <div id="title">Add New To-Do</div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 form-label">Title</div>
                                <div class="col-md-10">
                                
                                <input class="form-control required" id="title" type="text" name="title">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 form-label">Ref</div>
                                <div class="col-md-4">
                                    <input class="form-control" disabled="disabled" id="ref" type="text" name="ref" value="DP-R-28420">
                                                    </div>
                                <div class="col-md-2 form-label">
                                    Date Added
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group"><input class="form-control" disabled="disabled" id="date_added" type="datetime" name="date_added" value="26-05-2017 03:11 PM"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2 form-label">Priority</div>
                                <div class="col-md-4">
                                    <div class="form-group"><select class="form-control" id="priority" name="priority"><option value="" disabled="disabled" selected="selected">Select</option><option value="Low">Low</option><option value="Medium">Medium</option><option value="High">High</option></select></div>
                                    
                                </div>
                                <div class="col-md-2 form-label">
                                    Due Date
                                </div>
                                <div class="col-md-4">
                                    <input class="form-control required" id="due_date" type="datetime" name="due_date" value="26-05-2017 03:11 PM">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 form-label">Created by</div>
                                <div class="col-md-7">
                                    <div class="form-group"><input class="form-control" readonly="true" id="created_by" type="text" name="created_by" value="14776"></div>

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 form-label">Assigned To</div>
                                <div class="col-md-7">
                                    <div class="form-group"><select class="form-control required" id="assigned_to" name="assigned_to"><option value="" disabled="disabled" selected="selected">Select</option><option value="">All</option><option value="1448539">Aasim Arafath</option><option value="1448615">Abdelrahman Saudi</option><option value="1448549">Abdukhamid Kayumov</option><option value="1448503">Abdullah Al Ajaji</option><option value="1463">Abdullah Alajaji</option><option value="1448500">Adil Adoui</option><option value="1448602">Adrian Muntean</option><option value="1448548">Ahmed Farid Ahmed</option><option value="14956">Ahmed Fawad</option><option value="1448564">Ainiwaer Nueraili</option><option value="1448642">Ajith  Thanupillai</option><option value="1448542">Akhiz Munawar</option><option value="1448527">Alex Alexander</option><option value="35">Alexander Mazan</option><option value="6495">Ali  Qureshi</option><option value="1447191">Amir Waqas</option><option value="1444585">Amy Duhra</option><option value="33">Amynah Patel</option><option value="20">Anas Houssaini</option><option value="5232">Anastasia Gamurar</option><option value="1448601">Anastasiia Gudkova</option><option value="2567">Angielyn Carzon</option><option value="1448609">anil lal</option><option value="1448577">Ankur Ajit Poddar</option><option value="1448520">Anson Zhang</option><option value="1448521">Anvar Kandy</option><option value="1448616">Ashjan Turki Baha</option><option value="1448568">Assel Djumadylova</option><option value="1448586">Ayaz Khan</option><option value="1448597">Baiju Nazar</option><option value="11080">Bernard Kassis</option><option value="1448316">Botirali  Kholmatov</option><option value="1446263">Carlos Franco</option><option value="1448037">Caroline Knight</option><option value="1448513">Christian Mercado</option><option value="1448592">Christian Jacobs</option><option value="9351">Christophe  D Souza</option><option value="1728">Cristina Georgescu</option><option value="62">Crm Admin</option><option value="65">Crm Teamlead</option><option value="66">Crm Agent</option><option value="1448630">Dania Al Mubarak</option><option value="1448573">Daniel Whitlock</option><option value="12274">Danish Naveed</option><option value="1448583">Daniyal Rehmani</option><option value="1448556">Daria Borzykh</option><option value="1448614">Darya Maslava</option><option value="1448318">Dennis Dick</option><option value="7725">Derek Zhang</option><option value="1448499">Driven Reception</option><option value="15733">Driven Holiday Homes</option><option value="2984">Driven Reception</option><option value="1448627">Driven Properties</option><option value="14764">Edouard Bertrand</option><option value="1448523">Effie Matara</option><option value="1447693">Ekaterina  Dobrovinskaya</option><option value="31">Elena Danylevska</option><option value="1448530">Elena Loktionova</option><option value="32">Elinas Yussupova</option><option value="1448511">Elisa Dela Cruz</option><option value="1448587">Eman Sghaier</option><option value="1448579">Emovon Efosa Ihama</option><option value="27">Erika Rafaj</option><option value="1448572">Eufemia Moreno</option><option value="1448495">Fahdy Ali</option><option value="1909">Fahdy *</option><option value="1448632">Faraz Shafi</option><option value="2303">Fatma Hashim</option><option value="1448593">Fayaz Ahamed</option><option value="1448625">Ferus Mahamed</option><option value="1445346">Fredrik Persson</option><option value="1448508">Gary Walsh</option><option value="1448645">Geeta Narayanan</option><option value="1448517">George Ovidiu Drimbe</option><option value="1448611">george Jhon</option><option value="14797">Geraldine Peyraud</option><option value="1446790">Gezim  Zuesi</option><option value="15088">Ghanima  Abdulmajid</option><option value="1448516">Glenn Lazaraga</option><option value="1448551">Hajer Ameur</option><option value="1448502">Hansa Babu</option><option value="1448505">Harry Richards</option><option value="1448622">Hassan Sibai</option><option value="1448566">Hend Mohyeldin</option><option value="1448507">Hibo Bile</option><option value="2037">Hina Khan</option><option value="11500">Ijang  Yvonne Mbah</option><option value="1448558">Ilse Meijer</option><option value="1448607">Irina Podvornaja</option><option value="1448569">Iulian Paunescu</option><option value="1448651">Jackie Ding</option><option value="1448634">James Hamilton</option><option value="1448529">Janet Fergusone</option><option value="1446105">Janice Cera</option><option value="1448565">Janna  Mae Morales</option><option value="1448557">Jonna Lisa Van Montfort</option><option value="1448637">Junaid Ilyas</option><option value="1448380">Kaizad  Patrawala</option><option value="1447650">Kapil  Tunvor</option><option value="1448582">Katalina Ortiz</option><option value="1448596">Katia Abu Asaf</option><option value="1448591">Kelly Flores</option><option value="47">Khadija El Otmani</option><option value="14776">Kia Darban</option><option value="1448534">Lara El Khoury</option><option value="1448537">Lauren Dickson</option><option value="1448598">Leila Zhilkibayeva</option><option value="1448518">Leonard Realon</option><option value="6258">Lina Khoury</option><option value="1162">Lina Allaoa</option><option value="1446147">Lotfi El Gharbi</option><option value="14767">Louis Mormin</option><option value="1448155">Lovebin  V Thomas</option><option value="1448536">Luca Mameli</option><option value="1447431">Ludovic Ortiz</option><option value="55">Lukas Nyberg</option><option value="1647">Luke Emney</option><option value="1448531">Madhu Nair</option><option value="1448501">Magic Soliman</option><option value="1448519">Mahmoud Mohamed</option><option value="1448599">Mahmoud *</option><option value="2330">Manju Radhamma</option><option value="1448506">Manoj Kumaran</option><option value="2923">Marcela Herrera</option><option value="1447651">Marco Ejrup</option><option value="1448649">Marcus Plugaru</option><option value="1542">Maria Abrosimova</option><option value="1448545">Marianne Samonte</option><option value="1448562">Marijana Markovic</option><option value="1448571">Mariojane Bicaldo</option><option value="1448522">Mary Joy Erroja</option><option value="6255">Mary Balbacal</option><option value="1448504">Maryene Asilo</option><option value="1448636">Masoud Ali Mohammed</option><option value="1448588">Mbengnchang Tataw</option><option value="1448612">Michael Ofole</option><option value="1448575">Mir Mudassir Ali</option><option value="1448538">Mohamed Dewidar</option><option value="1448589">Mohamed Yehia Zaki</option><option value="1448532">Mohammed Nayef</option><option value="1448580">Mohammed Mishal</option><option value="1448647">Mohammed Mukkarum</option><option value="1448626">Monica Garcia</option><option value="1448639">Moustafa Othman</option><option value="1448578">Muhammad Tahir Latif</option><option value="1448606">Nabeel Khan</option><option value="1448613">Naheed  Butt</option><option value="1448533">Nailya Khismatullima</option><option value="1448624">Naji Al Rifai</option><option value="1448559">Nargiza Srazhdinova</option><option value="1448594">Narine Arturova</option><option value="1448515">Natalia Nasonova</option><option value="1448561">Neena Dordevic</option><option value="1448554">Nicolas Barthez</option><option value="1448541">Nikolay Mudrity</option><option value="14656">Nima Monfared Nia</option><option value="60">Nooshin  Mafakher</option><option value="1448570">Noreen Shiraz</option><option value="1448540">Noureddine    Bouti</option><option value="1448638">Nujud Salem Alshammari</option><option value="15559">Numera Amir</option><option value="1448576">Oday Hashem  A. Shoubaki</option><option value="1448620">Olfah Faraj</option><option value="2310">Precilla Nillas</option><option value="1448528">Raj Deo Singh</option><option value="1448544">Ramin Hashemzadeh</option><option value="1448640">Ramona Alexandra</option><option value="1448650">Ray Tsang</option><option value="1448629">Rayyan Al Ajaji</option><option value="1448610">rima ma</option><option value="1445152">Robert Richards</option><option value="59">Robert James Loveridge</option><option value="1448543">Roja Abdulla Jamil</option><option value="1446385">Roza Gurievskikh</option><option value="1448550">Saad Bin Zain</option><option value="1448617">Saadi Abbasi</option><option value="1448600">Sabina Valieva</option><option value="1448648">Sahil Saleh Alomair</option><option value="1448652">Sajeer JP</option><option value="1448595">Salem Al Zahrani</option><option value="9147">Salman Einavi</option><option value="1163">Saloomeh Ainavi</option><option value="1448563">Sanaa Gan Noun</option><option value="1444135">Sanoj Nujumudeen</option><option value="1448641">Sayora Nazarova</option><option value="1448605">Selina Zhang</option><option value="1446064">Serghei Vornices</option><option value="15139">Shafan Rose</option><option value="1448619">Shahab Zareei</option><option value="1448644">Shaik Suleman</option><option value="1448560">Shaju PM</option><option value="2808">Shalini Arora</option><option value="1448039">Shawn  Potter</option><option value="1448585">Sheila Schmeller</option><option value="1448567">Sheraz Hussain</option><option value="1448584">Sherif Ahmad</option><option value="1445862">Sofia Kumsing</option><option value="1448581">Sogol Eniavy</option><option value="1448623">Sohail Shaukat</option><option value="1448608">soumya v</option><option value="1447691">Steve  Minshull</option><option value="1">Super User</option><option value="1448526">Super User</option><option value="1448628">Super Admin</option><option value="1448509">Syed Shah</option><option value="1448555">Syed Imran Mehmood</option><option value="1448553">Syed Muhammad Uzair</option><option value="1448552">Sylvie Secci</option><option value="1448653">Talal Turkamani</option><option value="1448524">Tamara Felicijan</option><option value="1448590">Tarig Mohammed Abdallah</option><option value="1448546">Tarun Chutani</option><option value="1448497">Tester Tester</option><option value="1448547">Tetiana Gavryliuk</option><option value="3030">Thasmila Sayyed</option><option value="1448535">Thomas Antony Le Vezu</option><option value="7935">Thomas Gasmi</option><option value="1448510">Umar Ali</option><option value="1448229">User User</option><option value="1448618">Usman Tariq</option><option value="1448514">Violeta Alkhaja</option><option value="1448646">Viorel Plugaru</option><option value="1265">Vitaly Shugri</option><option value="1448633">Vladimir Nonone</option><option value="1448635">Wael Baroud</option><option value="1448574">Waseem Mfarej</option><option value="1448631">Weam Al Akabani</option><option value="1448604">Wynfred Amoroso</option><option value="1448621">Xeniya Zyryanova</option><option value="1448512">Xiaobin Zhang</option><option value="1448643">Yitayal Menkir</option><option value="1448603">Zac *</option><option value="14773">Zahra Syed</option><option value="1448525">Zhibek Kussainova</option><option value="1446871">Zhibek *</option></select></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 form-label">Status</div>
                                <div class="col-md-7">
                                    <div class="form-group"><select class="form-control required" id="status" name="status"><option value="" disabled="disabled">Select</option><option value="Not Yet Started">Not Yet Started</option><option value="In Progress">In Progress</option><option value="Completed">Completed</option></select></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 form-label">Notes</div>
                                <div class="col-md-7">
                                    <input class="form-control" readonly="true" id="notes" type="text" name="notes" value='[{&quot;user&quot;:&quot;Glenn Lazaraga&quot;,&quot;date&quot;:&quot;2017-03-23&quot;,&quot;note&quot;:&quot;Brought by Kia - listed by Glenn&quot;}]'>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 form-label">Listing Ref</div>
                                <div class="col-md-7">
                                    <div class="form-group"><input class="form-control" readonly="true" id="listing_ref" type="text" name="listing_ref" value="DP-R-28420"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5 form-label">Lead Ref</div>
                                <div class="col-md-7">
                                    <div class="form-group"><input class="form-control" readonly="true" id="lead_ref" type="text" name="lead_ref"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<input class="form-control" type="hidden" name="_token" value="TMpeYX5qRxgD7ZEK44NKrzQsOgLhAREnGpuIYsfx"></form>

<script type="text/javascript">
jQuery.validator.setDefaults({
        debug: true,
        success: "valid",
        errorPlacement: function(error,element) {
            return true;
        }

    });
$(document).ready(function(){
    var form = $( "#createTodo" );
    form.validate();
    $("#SaveTodo").click(function(e)
      {
          if(form.valid() == true) {
            var postData = $("#createTodo").serializeArray();
            var formURL  = "http://dcrm.drivenproperties.ae/todo";
            $.ajax(
            {
                url : formURL,
                type: "POST",
                data : postData,
                success:function(data, textStatus, jqXHR)
                {
                    toastr.success('Successfully added To Do');
                    $('#todo-popup').modal('hide');
                },
                error: function(jqXHR, textStatus, errorThrown)
                {
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        }
       
        });
});    
</script>