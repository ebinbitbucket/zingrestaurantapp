<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
        body { margin: 0px; }
        body,td,th {
            font-family: Verdana, Geneva, sans-serif;
            font-size: 12px;

        }
        tr {
            vertical-align: middle;
        }
        th {
            font-weight: bold;
            border: 1px solid #000;
        }
        @page{ size:A4; margin: 40px 0px 30px 0px; }
        @page Sect{ size:A4 landscape; margin: 40px 0px 30px 0px; }
        div.Sector {page:Sect;}
        .header { position: fixed; left: 0px; top: -40px; right: 0px; height: 40px; background-color: #E2E1DE; text-align: center; border-bottom:#2A0099 solid 2px;}
        .footer { position: fixed; left: 0px; bottom: -20px; right: 0px; height: 30px; background-color: #E2E1DE; border-top:#2A0099 solid 3px;}
        th {
            background-color:#3399FF;
            color: #fff;
        }
        .td1{color:#fff;background: #2184e7;padding: 0px 20px;font-size: 14px;}
        .td2{color:#fff;background: #2184e7;padding: 0px 20px;font-size: 14px;height:25px;}
        </style>
    </head>
    <body>
        <div class="header">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td align="center" class="td1"><h5>Example Report</h5></td>
                </tr>
            </table>
        </div>
        <div class="footer">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td class="td1"><h6>Lavalite</h6></td>
                    <td class="td2"  align="right"><h6>Date: <NOBR>{{date('Y-m-d')}}</NOBR></h6></td>
                </tr>
            </table>
        </div>
        <div class="content">

        
        <p>
            <table width="95%" border="0" cellspacing="0" cellpadding="1" align="center" style="padding-top:20px;" >
                <tr>
                    <td><span style="font-size:22px; color:#330099;">Example Details</span></td>
                </tr>
            </table>

            <table width="95%" border="1" cellspacing="0" cellpadding="10" align="center" style="padding-top:20px;" >
                <tr>
                    <th>{!! trans('example::example.label.name')!!}</th>
                    <th>{!! trans('example::example.label.email')!!}</th>
                    <th>{!! trans('example::example.label.color')!!}</th>
                    <th>{!! trans('example::example.label.date')!!}</th>
                    <th>{!! trans('example::example.label.datetime')!!}</th>
                    <th>{!! trans('example::example.label.month')!!}</th>
                    <th>{!! trans('example::example.label.password')!!}</th>
                    <th>{!! trans('example::example.label.range')!!}</th>
                    <th>{!! trans('example::example.label.search')!!}</th>
                    <th>{!! trans('example::example.label.tel')!!}</th>
                    <th>{!! trans('example::example.label.time')!!}</th>
                    <th>{!! trans('example::example.label.url')!!}</th>
                    <th>{!! trans('example::example.label.week')!!}</th>
                    <th>{!! trans('example::example.label.date_picker')!!}</th>
                    <th>{!! trans('example::example.label.time_picker')!!}</th>
                    <th>{!! trans('example::example.label.date_time_picker')!!}</th>
                    <th>{!! trans('example::example.label.radios')!!}</th>
                    <th>{!! trans('example::example.label.checkboxes')!!}</th>
                    <th>{!! trans('example::example.label.switch')!!}</th>
                    <th>{!! trans('example::example.label.select')!!}</th>
                    <th>{!! trans('example::example.label.model_select')!!}</th>
                    <th>{!! trans('example::example.label.tinyints')!!}</th>
                    <th>{!! trans('example::example.label.smallints')!!}</th>
                    <th>{!! trans('example::example.label.mediumints')!!}</th>
                    <th>{!! trans('example::example.label.ints')!!}</th>
                    <th>{!! trans('example::example.label.bigints')!!}</th>
                    <th>{!! trans('example::example.label.decimals')!!}</th>
                    <th>{!! trans('example::example.label.floats')!!}</th>
                    <th>{!! trans('example::example.label.doubles')!!}</th>
                    <th>{!! trans('example::example.label.reals')!!}</th>
                    <th>{!! trans('example::example.label.bits')!!}</th>
                    <th>{!! trans('example::example.label.booleans')!!}</th>
                    <th>{!! trans('example::example.label.dates')!!}</th>
                    <th>{!! trans('example::example.label.datetimes')!!}</th>
                    <th>{!! trans('example::example.label.timestamps')!!}</th>
                    <th>{!! trans('example::example.label.times')!!}</th>
                    <th>{!! trans('example::example.label.years')!!}</th>
                    <th>{!! trans('example::example.label.chars')!!}</th>
                    <th>{!! trans('example::example.label.varchars')!!}</th>
                    <th>{!! trans('example::example.label.tinytexts')!!}</th>
                    <th>{!! trans('example::example.label.texts')!!}</th>
                    <th>{!! trans('example::example.label.mediumtexts')!!}</th>
                    <th>{!! trans('example::example.label.longtexts')!!}</th>
                    <th>{!! trans('example::example.label.binarys')!!}</th>
                    <th>{!! trans('example::example.label.varbinarys')!!}</th>
                    <th>{!! trans('example::example.label.tinyblobs')!!}</th>
                    <th>{!! trans('example::example.label.mediumblobs')!!}</th>
                    <th>{!! trans('example::example.label.blobs')!!}</th>
                    <th>{!! trans('example::example.label.longblobs')!!}</th>
                    <th>{!! trans('example::example.label.enums')!!}</th>
                    <th>{!! trans('example::example.label.sets')!!}</th>
                    <th>{!! trans('example::example.label.status')!!}</th>
                    <th>{!! trans('example::example.label.created_at')!!}</th>
                    <th>{!! trans('example::example.label.updated_at')!!}</th>
                </tr>

                <tr>
                    <td><span style="font-size:9px;"> {{ @$example->name }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->email }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->color }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->date }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->datetime }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->month }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->password }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->range }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->search }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->tel }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->time }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->url }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->week }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->date_picker }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->time_picker }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->date_time_picker }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->radios }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->checkboxes }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->switch }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->select }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->model_select }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->tinyints }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->smallints }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->mediumints }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->ints }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->bigints }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->decimals }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->floats }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->doubles }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->reals }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->bits }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->booleans }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->dates }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->datetimes }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->timestamps }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->times }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->years }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->chars }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->varchars }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->tinytexts }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->texts }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->mediumtexts }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->longtexts }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->binarys }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->varbinarys }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->tinyblobs }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->mediumblobs }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->blobs }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->longblobs }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->enums }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->sets }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->status }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->created_at }} </span></td>
                    <td><span style="font-size:9px;"> {{ @$example->updated_at }} </span></td>
                </tr>
                
            </table>
               
        </p>

    </div>
</body>
</html>

