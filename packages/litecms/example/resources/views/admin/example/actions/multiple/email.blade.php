
{!!Form::vertical_open()
  ->id('form-mail')
  ->method('POST')
  ->files('true')
  ->enctype('multipart/form-data')
  ->action(URL::to('admin/example/example/send/email'))!!}

  <div class="modal-body has-form clearfix">
    <div class="modal-form" id="mail-content">
      <div class="row">
        <div class="col-md-4 form-group-sm">
            {!!Form::text('to')->required()!!}
            {!!Form::text('cc')!!}
            {!!Form::text('from')
            ->value('test@lavalite.com')
            ->readonly()!!}
            {!!Form::text('subject')
              ->value('Example Email')
            !!}
            {!!Form::textarea('details')
            ->value("Dear Sir \n\n Thank you for giving Lavalite the opportexampley to assist you in your example search.  We are pleased to present the following examples, which match your specific requirements.")
            ->rows(7)!!}
        </div>
        <div class="col-md-8">
          <div style="height:435px;overflow-y:auto;" id="mail-area">
              <table class="table" border="0" align="center" cellpadding="0" cellspacing="0">
                  <tr>
                      <td bgcolor="#E2E1DE"><table width="100%" border="0" cellspacing="0" cellpadding="10">
                          <tr>
                              <td width="13%"><img src="{!! theme_asset('img/logo.svg') !!}" style="padding-left:10px;"></td>
                              <td width="87%" align="right" valign="bottom"><h1><span style="color: #270188; font-size: 26px; padding-bottom:10px; font-family: Arial, Helvetica, sans-serif; padding-right: 10px;">Example Details</span></h1></td>
                          </tr>
                      </table></td>
                  </tr>
                  <tr>
                      <td valign="top" class="content">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                          <tr>
                              <td height="50" style="padding-bottom: 20px;"><span id="details-area"></span></td>
                          </tr>
                        </table>

                        @forelse($examples as $key => $example)
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 10px;">
                            <td>
                              <span>{{ trans('example::example.label.name') }} : {{ @$example['name'] }} </span><br />
                              <span>{{ trans('example::example.label.email') }} : {{ @$example['email'] }} </span><br />
                              <span>{{ trans('example::example.label.color') }} : {{ @$example['color'] }} </span><br />
                              <span>{{ trans('example::example.label.date') }} : {{ @$example['date'] }} </span><br />
                              <span>{{ trans('example::example.label.datetime') }} : {{ @$example['datetime'] }} </span><br />
                              <span>{{ trans('example::example.label.file') }} : {{ @$example['file'] }} </span><br />
                              <span><hr></span>
                            </td>
                        </table>
                        @empty
                        @endif
                      </td>
                  </tr>
                 <tr>
                    <td bgcolor="#E2E1DE" style="padding-left:10px;"><!-- <img src="{!! theme_asset('img/logo.svg')!!}" > -->Footer</td>
                  </tr>
              </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <div class="col-md-12 col-lg-12">
      <button  type="button" class="btn pull-right btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i>  Close</button>                              
      <button  type="button" class="btn btn-primary pull-right" id="btn-submit-mail" name="new" style="margin-right:1%"><i class="fa fa-check-circle"></i> Send Mail</button>
    </div>
  </div>
  {!!Form::close()!!}

    <script type="text/javascript">
    
      $('#details-area').html($('#details').val());

      $('#details').keyup( function() {
        $('#details-area').html($(this).val());
      });

      $('#btn-submit-mail').click( function(e) {

        if($('#to').val() == ''){
          $('#to').css({'border':'1px solid red'});
          return;
        }

        var formData = new FormData();
        params   = $("#form-mail").serializeArray();
        $.each(params, function(i, val) {
            formData.append(val.name, val.value);
        });
        formData.append('example', $("#mail-area").html()); 

        $.ajax({
          url : "{!!url('admin/example/example/action/send/mail')!!}",
          type: "POST",
          data: formData,
          cache: false,
          processData: false,
          contentType: false,
          success:function(data, textStatus, jqXHR)
          {
            $('#div-mail').html("<h3 align='center'>Email sent successfully</h3>");
          },
          error: function(jqXHR, textStatus, errorThrown)
          {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
          }
        });
        e.preventDefault();
      });
    </script>