
{!!Form::horizontal_open()
  ->id('form-calendar')
  ->method('POST')
  ->files('true')
  ->enctype('multipart/form-data')
  ->action(url('admin/calendar/calendar'))!!}

    <div class="modal-body has-form clearfix">
        <div class="modal-form">
            <div class="col-md-12">
                {!!Form::hidden('status')->forceValue('Calendar')!!}
                {!!Form::text('title')
                ->forceValue(@$example->name)
                ->required()!!}
                <div class="form-group form-group-sm">
                  <label for="start" class="control-label col-lg-3 col-md-6 col-sm-6"> 
                    {{trans('calendar::calendar.label.start')}}<sup>*</sup>
                  </label>
                  <div class="col-lg-4 col-md-6 col-sm-6">
                    {!! Form::date('start')
                    ->forceValue(date('Y-m-d',strtotime(@$example->created_at)))
                    ->placeholder(trans('calendar::calendar.placeholder.start'))
                    ->addGroupClass('form-group-sm')
                    ->required()
                    ->raw()!!}
                  </div>
                  <label for="time_required" class="control-label col-lg-2 col-md-6 col-sm-6"> 
                    {{trans('calendar::calendar.label.end')}}
                  </label>
                  <div class="col-lg-3 col-md-6 col-sm-6">
                    {!! Form::date('end')
                    ->forceValue(date('Y-m-d',strtotime(@$example->created_at)))
                    ->placeholder(trans('calendar::calendar.placeholder.end'))
                    ->addGroupClass('form-group-sm')
                    ->required()
                    ->raw()!!}
                  </div>
                </div>
                <div class="form-group form-group-sm">
                  <label for="assigned_to" class="control-label col-lg-3 col-md-6 col-sm-6"> 
                    {{trans('calendar::calendar.label.assignee_id')}}<sup>*</sup>
                  </label>
                  <div class="col-lg-4 col-md-6 col-sm-6">
                    {!!Form::select('assigned_to')
                    ->options(@Calendar::users())
                    ->raw()!!}
                  </div>
                  <label for="location" class="control-label col-lg-2 col-md-6 col-sm-6"> 
                    {{trans('calendar::calendar.label.location')}}
                  </label>
                  <div class="col-lg-3 col-md-6 col-sm-6">
                    {!! Form::text('location')
                    ->placeholder(trans('calendar::calendar.placeholder.location'))
                    ->addGroupClass('form-group-sm')
                    ->raw()!!}
                  </div>
                </div>
                
                {!!Form::textarea('details')
                ->rows(3)!!}

                {!! Form::hidden('color')->id('event-color')->value('#3c8dbc')!!}
                <div class="btn-group text-center" style="width: 100%; margin: 10px 0;">
                    <div class="event-color-block">
                        <ul class="fc-color-picker" id="color-chooser">
                            <li>
                                <a class="event-azure" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-purple" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-blue" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-green" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-orange" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-red" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-rose" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-pink" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-indigo" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a class="event-default" href="#">
                                    <i class="ion ion-record">
                                    </i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>

        </div>                 
    </div>
    <div class="modal-footer">
      <div class="col-md-12 col-lg-12">
        <button  type="button" class="btn pull-right btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle"></i>  Close</button>                              
        <button  type="button" class="btn btn-primary pull-right " id="btn-submit-calendar" name="new" style="margin-right:1%"><i class="fa fa-check-circle"></i> Add Calendar</button>
      </div>
    </div>
    {!!Form::close()!!}

    <script type="text/javascript">
                   
        var currColor = "#3c8dbc";
        $(".fc-color-picker >li >a").click(function (e) {
          e.preventDefault();
          currColor = $(this).attr("class");
          $('#event-color').val(currColor);
          $(".fc-color-picker >li >a").removeClass("active");
          $(this).addClass("active");
        });  

      $('#btn-submit-calendar').click( function(e) {
        
        var formData = new FormData();
        params   = $("#form-calendar").serializeArray();
        $.each(params, function(i, val) {
            formData.append(val.name, val.value);
        });

        $.ajax({
            url : "{!!url('admin/calendar/calendar')!!}",
            type: "POST",
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success:function(data, textStatus, jqXHR)
            {
                $('#action-modal').modal('hide');
            },
            error: function(jqXHR, textStatus, errorThrown)
            {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });
        e.preventDefault();
      });
    </script>