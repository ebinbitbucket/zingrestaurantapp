<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <i class="fa fa-file-text-o"></i> {!! trans('example::example.name') !!} <small> {!! trans('app.manage') !!} {!! trans('example::example.names') !!}</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{!! guard_url('/') !!}"><i class="fa fa-dashboard"></i> {!! trans('app.home') !!} </a></li>
            <li class="active">{!! trans('example::example.names') !!}</li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div id='example-example-entry'>
    </div>
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                    <li class="{!!(request('status') == '')?'active':'';!!}"><a href="{!!guard_url('example/example')!!}">{!! trans('example::example.names') !!}</a></li>
                    <li class="{!!(request('status') == 'archive')?'active':'';!!}"><a href="{!!guard_url('example/example?status=archive')!!}">Archived</a></li>
                    <li class="{!!(request('status') == 'deleted')?'active':'';!!}"><a href="{!!guard_url('example/example?status=deleted')!!}">Trashed</a></li>
                    <li class="pull-right">
                    <span class="actions">
                    <!--   
                    <a  class="btn btn-xs btn-purple"  href="{!!guard_url('example/example/reports')!!}"><i class="fa fa-bar-chart" aria-hidden="true"></i><span class="hidden-sm hidden-xs"> Reports</span></a>
                    @include('example::admin.example.partial.actions')
                    -->
                    @include('example::admin.example.partial.filter')
                    @include('example::admin.example.partial.column')
                    </span> 
                </li>
            </ul>
            <div class="tab-content">
                <table id="example-example-list" class="table table-striped data-table">
                    <thead class="list_head">
                        <th style="text-align: right;" width="1%"><a class="btn-reset-filter" href="#Reset" style="display:none; color:#fff;"><i class="fa fa-filter"></i></a> <input type="checkbox" id="example-example-check-all"></th>
                        <th data-field="name">{!! trans('example::example.label.name')!!}</th>
                    <th data-field="email">{!! trans('example::example.label.email')!!}</th>
                    <th data-field="color">{!! trans('example::example.label.color')!!}</th>
                    <th data-field="date">{!! trans('example::example.label.date')!!}</th>
                    <th data-field="datetime">{!! trans('example::example.label.datetime')!!}</th>
                    <th data-field="file">{!! trans('example::example.label.file')!!}</th>
                    <th data-field="files">{!! trans('example::example.label.files')!!}</th>
                    <th data-field="image">{!! trans('example::example.label.image')!!}</th>
                    <th data-field="images">{!! trans('example::example.label.images')!!}</th>
                    <th data-field="month">{!! trans('example::example.label.month')!!}</th>
                    <th data-field="password">{!! trans('example::example.label.password')!!}</th>
                    <th data-field="range">{!! trans('example::example.label.range')!!}</th>
                    <th data-field="search">{!! trans('example::example.label.search')!!}</th>
                    <th data-field="tel">{!! trans('example::example.label.tel')!!}</th>
                    <th data-field="time">{!! trans('example::example.label.time')!!}</th>
                    <th data-field="url">{!! trans('example::example.label.url')!!}</th>
                    <th data-field="week">{!! trans('example::example.label.week')!!}</th>
                    <th data-field="date_picker">{!! trans('example::example.label.date_picker')!!}</th>
                    <th data-field="time_picker">{!! trans('example::example.label.time_picker')!!}</th>
                    <th data-field="date_time_picker">{!! trans('example::example.label.date_time_picker')!!}</th>
                    <th data-field="radios">{!! trans('example::example.label.radios')!!}</th>
                    <th data-field="checkboxes">{!! trans('example::example.label.checkboxes')!!}</th>
                    <th data-field="switch">{!! trans('example::example.label.switch')!!}</th>
                    <th data-field="select">{!! trans('example::example.label.select')!!}</th>
                    <th data-field="model_select">{!! trans('example::example.label.model_select')!!}</th>
                    <th data-field="tinyints">{!! trans('example::example.label.tinyints')!!}</th>
                    <th data-field="smallints">{!! trans('example::example.label.smallints')!!}</th>
                    <th data-field="mediumints">{!! trans('example::example.label.mediumints')!!}</th>
                    <th data-field="ints">{!! trans('example::example.label.ints')!!}</th>
                    <th data-field="bigints">{!! trans('example::example.label.bigints')!!}</th>
                    <th data-field="decimals">{!! trans('example::example.label.decimals')!!}</th>
                    <th data-field="floats">{!! trans('example::example.label.floats')!!}</th>
                    <th data-field="doubles">{!! trans('example::example.label.doubles')!!}</th>
                    <th data-field="reals">{!! trans('example::example.label.reals')!!}</th>
                    <th data-field="bits">{!! trans('example::example.label.bits')!!}</th>
                    <th data-field="booleans">{!! trans('example::example.label.booleans')!!}</th>
                    <th data-field="dates">{!! trans('example::example.label.dates')!!}</th>
                    <th data-field="datetimes">{!! trans('example::example.label.datetimes')!!}</th>
                    <th data-field="timestamps">{!! trans('example::example.label.timestamps')!!}</th>
                    <th data-field="times">{!! trans('example::example.label.times')!!}</th>
                    <th data-field="years">{!! trans('example::example.label.years')!!}</th>
                    <th data-field="chars">{!! trans('example::example.label.chars')!!}</th>
                    <th data-field="varchars">{!! trans('example::example.label.varchars')!!}</th>
                    <th data-field="texts">{!! trans('example::example.label.texts')!!}</th>
                    <th data-field="blobs">{!! trans('example::example.label.blobs')!!}</th>
                    <th data-field="enums">{!! trans('example::example.label.enums')!!}</th>
                    <th data-field="sets">{!! trans('example::example.label.sets')!!}</th>
                    <th data-field="PRIMARY">{!! trans('example::example.label.PRIMARY')!!}</th>
                    </thead>
                </table>
            </div>
        </div>
    </section>
</div>

<script type="text/javascript">

var oTable;
var oSearch;
$(document).ready(function(){
    app.load('#example-example-entry', '{!!guard_url('example/example/0')!!}');
    oTable = $('#example-example-list').dataTable( {
        'columnDefs': [{
            'targets': 0,
            'searchable': false,
            'orderable': false,
            'className': 'dt-body-center',
            'render': function (data, type, full, meta){
                return '<input type="checkbox" name="id[]" value="' + data.id + '">';
            }
        }], 
        
        "responsive" : true,
        "order": [[1, 'asc']],
        "bProcessing": true,
        "sDom": 'R<>rt<ilp><"clear">',
        "bServerSide": true,
        "sAjaxSource": '{!! guard_url('example/example') !!}',
        "fnServerData" : function ( sSource, aoData, fnCallback ) {

            $.each(oSearch, function(key, val){
                aoData.push( { 'name' : key, 'value' : val } );
            });
            app.dataTable(aoData);
            $.ajax({
                'dataType'  : 'json',
                'data'      : aoData,
                'type'      : 'GET',
                'url'       : sSource,
                'success'   : fnCallback
            });
        },

        "columns": [
            {data :'id'},
            {data :'name'},
            {data :'email'},
            {data :'color'},
            {data :'date'},
            {data :'datetime'},
            {data :'file'},
            {data :'files'},
            {data :'image'},
            {data :'images'},
            {data :'month'},
            {data :'password'},
            {data :'range'},
            {data :'search'},
            {data :'tel'},
            {data :'time'},
            {data :'url'},
            {data :'week'},
            {data :'date_picker'},
            {data :'time_picker'},
            {data :'date_time_picker'},
            {data :'radios'},
            {data :'checkboxes'},
            {data :'switch'},
            {data :'select'},
            {data :'model_select'},
            {data :'tinyints'},
            {data :'smallints'},
            {data :'mediumints'},
            {data :'ints'},
            {data :'bigints'},
            {data :'decimals'},
            {data :'floats'},
            {data :'doubles'},
            {data :'reals'},
            {data :'bits'},
            {data :'booleans'},
            {data :'dates'},
            {data :'datetimes'},
            {data :'timestamps'},
            {data :'times'},
            {data :'years'},
            {data :'chars'},
            {data :'varchars'},
            {data :'texts'},
            {data :'blobs'},
            {data :'enums'},
            {data :'sets'},
            {data :'PRIMARY'},
        ],
        "pageLength": 25
    });

    $('#example-example-list tbody').on( 'click', 'tr td:not(:first-child)', function (e) {
        e.preventDefault();

        oTable.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
        var d = $('#example-example-list').DataTable().row( this ).data();
        $('#example-example-entry').load('{!!guard_url('example/example')!!}' + '/' + d.id);
    });

    $('#example-example-list tbody').on( 'change', "input[name^='id[]']", function (e) {
        e.preventDefault();

        aIds = [];
        $(".child").remove();
        $(this).parent().parent().removeClass('parent'); 
        $("input[name^='id[]']:checked").each(function(){
            aIds.push($(this).val());
        });
    });

    $("#example-example-check-all").on( 'change', function (e) {
        e.preventDefault();
        aIds = [];
        if ($(this).prop('checked')) {
            $("input[name^='id[]']").each(function(){
                $(this).prop('checked',true);
                aIds.push($(this).val());
            });

            return;
        }else{
            $("input[name^='id[]']").prop('checked',false);
        }
        
    });


    $(".reset_filter").click(function (e) {
        e.preventDefault();
        $("#form-search")[ 0 ].reset();
        $('#form-search input,#form-search select').each( function () {
          oTable.search( this.value ).draw();
        });
        $('#example-example-list .reset_filter').css('display', 'none');

    });


    // Add event listener for opening and closing details
    $('#example-example-list tbody').on('click', 'td.details-control', function (e) {
        e.preventDefault();
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

});
</script>