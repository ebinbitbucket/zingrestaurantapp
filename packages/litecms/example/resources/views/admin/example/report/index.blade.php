@extends('admin::curd.index')
@section('heading')
<i class="fa fa-file-text-o"></i> {!! trans('cms.report') !!} <small>  {!! trans('example::example.name') !!} {!! trans('cms.reports') !!}</small>
@stop

@section('title')
{!! trans('cms.reports') !!} of {!! trans('example::example.names') !!} 
@stop

@section('breadcrumb')
<ol class="breadcrumb">
  <li><a href="{!! trans_url('admin') !!}"><i class="fa fa-dashboard"></i> {!! trans('cms.home') !!} </a></li>
  <li class="active">{!! trans('cms.reports') !!} </li>
  <li class="active">{!! trans('example::example.names') !!} </li>
</ol>
@stop

@section('entry')
<div class="nav-tabs-custom">
  <!-- Nav tabs -->
  <ul class="nav nav-tabs primary">
    <li class="active"><a href="#details" data-toggle="tab">Filters</a></li>
    <li class="pull-right">
      <button type="button" class="btn btn-xs btn-primary" id="open-filter"><i class="fa fa-bar-chart""></i> Open</button>
      <button type="button" class="btn btn-xs btn-warning"  id="save-filter"><i class="fa fa-bookmark-o""></i> Save </button>
      <button type="button" class="btn btn-xs btn-danger" id="reset-filter"><i class="fa fa-refresh"></i> Reset </button>
    </li>
  </ul>

  <div class="tab-content active clearfix" id="details">

    {!!Form::horizondal_open()
    ->id('form-report')
    ->secure()
    ->method('POST')
    ->files('true')
    ->enctype('multipart/form-data') !!}
    <div class="col-md-6 col-lg-6">
      <div class="form-group form-group-sm">
        <label for="group_by" class="control-label col-lg-3 col-md-6 col-sm-6"> 
          Group By
        </label>
        <div class="col-lg-4 col-md-4 col-sm-6">

          {!! Form::select('search[group_by]')
          -> addGroupClass('form-group-sm')
          -> placeholder('select')
          -> options(trans('example::example.options.group_by'),'status')
          -> raw()
          !!}
        </div>
        <label for="order_by" class="control-label col-lg-1 col-md-1 col-sm-6"> 
          Order By
        </label>
        <div class="col-lg-4 col-md-4 col-sm-6">
          {!! Form::select('search[order_by]')
          -> placeholder('select')
          -> addGroupClass('form-group-sm')
          -> options(trans('example::example.options.order_by'),'asc')
          -> raw()
          !!}
        </div>
      </div>
    </div>
    <div class="col-md-6 col-lg-6">
      {!! Form::daterange('search[created_at]')
      -> id('date_created_at')
      -> addGroupClass('form-group-sm')
      -> placeholder('From')
      -> label('Date Range')!!}
    </div>
    <div class="col-md-6 col-lg-6">

      {!! Form::select('search[status]')
      -> addGroupClass('form-group-sm')
      -> label('Status')
      -> placeholder('select')
      -> options(['' => 'All'] + trans('example::example.options.status'))
      !!}

      {!! Form::select('search[select]')
      -> label('Select')
      -> placeholder('select')
      -> addGroupClass('form-group-sm')
      -> options(['' => 'All'] + trans('example::example.options.select'))
      !!}

    </div>          
    <div class="col-md-6 col-lg-6">

      {!! Form::select('search[model_select]')
      -> label('model select')
      -> placeholder('model select')
      -> addGroupClass('form-group-sm')
      -> options(['' => 'All'] + trans('example::example.options.model_select'))
      !!}

    </div>

    {!!Form::close()!!}
  </div>
</div>
@stop


@section('content')
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab-graph" data-toggle="tab"> Graph </a></li>
    <li><a href="#tab-summary" data-toggle="tab"> Summary </a></li>
    <li><a href="#tab-fulldata" data-toggle="tab"> Full data </a></li>
    <li class="pull-right">
      <button type="button" class="btn btn-xs btn-pink" id="download-pdf"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</button>
      <button type="button" class="btn btn-xs btn-purple"  id="download-csv"><i class="fa fa-file-excel-o" aria-hidden="true"></i>  CSV</button>
      <button type="button" class="btn btn-xs btn-success"  id="download-excel"><i class="fa fa-file-excel-o" aria-hidden="true"></i>  Excel</button>
    </li>
  </ul>
  <div class="tab-content clearfix">

    <div class="tab-pane active clearfix" id="tab-graph">
      @include('example::admin.example.report.graph')
    </div>

    <div class="tab-pane" id="tab-summary">
    </div>
    <div class="tab-pane" id="tab-fulldata">
      @include('example::admin.example.report.data')
    </div>
  </div>

  <div class="modal fade" id="save-report">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"">&times;</button>
          <h4 class="modal-title">Save Report</h4>
        </div>
        <div class="modal-body clearfix">
          <div class="form-horizontal">
            <div class="popup_description" style="color:red;"><sup>*</sup>Type the name and save report for future use.</div>
            <div class="col-md-12 col-lg-12 m-t-20">
              {!!Form::horizontal_open()
              ->id('report_form')
              ->method('POST')
              ->files('true')
              ->action(trans_url('admin/settings/setting'))!!}

              {!! Form::text('report_name')
              -> placeholder('Enter Name')
              -> label('Report Name')
              -> required()!!}

              {!!Form::close()!!}  
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="btn-save-filter" name="Saverep" value="Send">
           <i class="fa fa-check-circle"></i> Save</button>
           <button type="button" class="btn btn-danger" data-dismiss="modal">
             <i class="fa fa-times-circle"></i> Close</button>
           </div>
         </div>
       </div>
     </div>

     <div class="modal fade" id="show_report">
      <div class="modal-dialog">
        <div class="modal-content" style="max-width:400px;">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"">&times;</button>
            <h4 class="modal-title">Open Saved Reports</h4>
          </div>
          <div class="modal-body" style="height:210px; overflow-y: auto;">

            <div id="saved-reports"></div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger"  name="Closerep" data-dismiss="modal"><i class="fa fa-times-circle"></i> Close</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
  @stop

  @section('script')
  <script type="text/javascript" src="{{ url('packages/highcharts/highcharts.js') }}"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

  <script type="text/javascript">

    $(document).ready(function () {
      var filter = $("#form-report").serialize();
      $('#date_created_at').daterangepicker();

      plotGraph("{{trans_url('admin/example/report/graph')}}" +"?" + filter + "&callback=?");
      
      $('#reset-filter').click(function(){
        $('#form-report')[0].reset();
        oTable.ajax.reload();
        filter = $("#form-report").serialize();      
        plotGraph("{{trans_url('admin/example/report/graph')}}" +"?" + filter + "&callback=?");
      });

      $('#download-pdf').click(function(){
        var filter = $("#form-report").serialize();
        window.location = "{{trans_url('admin/example/report/pdf')}}?" + filter;
      });

      $('#download-csv').click(function(){
        var filter = $("#form-report").serialize();
        window.location = "{{trans_url('admin/example/report/csv')}}?" + filter;
      });

      $('#download-excel').click(function(){
        var filter = $("#form-report").serialize();
        window.location = "{{trans_url('admin/example/report/excel')}}?" + filter;
      });

      $("#save-filter").click(function(){
        $("#name").val('');
        $("#save-report").modal('show');
      });

      $("#open-filter").click(function(){
        $('#saved-reports').load("{!!url('admin/settings/setting/list/main.example.report')!!}");
        $('#show_report').modal("show");
      });

      $('#btn-save-filter').click(function(e){
        if($("#report_form").valid() == false) {
          toastr.error('Please enter valid information.', 'Error');
          return false;
        }
        var formData = new FormData();
        formData.append('value', $("#form-report").serialize());
        formData.append('name', $("#report_name").val());
        formData.append('key', 'main.example.report');
        formData.append('package', 'Example');
        formData.append('module', 'Example');
        
        $.ajax({
          url : "{!!url('admin/settings/setting')!!}",
          type: "POST",
          data: formData,
          cache: false,
          processData: false,
          contentType: false,
          success:function(data, textStatus, jqXHR)
          {
            $('#save-report').modal('hide');
          },
          error: function(jqXHR, textStatus, errorThrown)
          {
            toastr.error('An error occurred while saving.', 'Error');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
          }
        });

        e.preventDefault();
      });

      $(document).on('click', ".nav-tabs li a[href='#tab-summary']", function (e) {
        e.preventDefault();
        var filter = $("#form-report").serialize();
        $('#tab-summary').load("{{trans_url('admin/example/report/summary')}}" +"?" + filter);
      });

      $(document).on('change dp.change keyup', "#form-report select, #form-report input", function (e) {
        e.preventDefault();
        chart.showLoading();
        oTable.search( this.value ).draw();
        var filter = $("#form-report").serialize();
        plotGraph("{{trans_url('admin/example/report/graph')}}" +"?" + filter + "&callback=?");
      });
    });
  </script>

  @stop

  @section('style')
  @stop

