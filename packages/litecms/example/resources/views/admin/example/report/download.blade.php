<!DOCTYPE html>
<html>
    <head>
        <style type="text/css">
        body { margin: 0px; }
        body,td,th {
            font-family: Verdana, Geneva, sans-serif;
            font-size: 12px;

        }
        tr {
            vertical-align: middle;
        }
        th {
            font-weight: bold;
            border: 1px solid #000;
        }
        @page{ size:A4; margin: 40px 0px 30px 0px; }
        @page Sect{ size:A4 landscape; margin: 40px 0px 30px 0px; }
        div.Sector {page:Sect;}
        .header { position: fixed; left: 0px; top: -40px; right: 0px; height: 40px; background-color: #E2E1DE; text-align: center; border-bottom:#2A0099 solid 2px;}
        .footer { position: fixed; left: 0px; bottom: -20px; right: 0px; height: 30px; background-color: #E2E1DE; border-top:#2A0099 solid 3px;}
        th {
            background-color:#3399FF;
            color: #fff;
        }
        .td1{color:#fff;background: #2184e7;padding: 0px 20px;font-size: 14px;}
        .td2{color:#fff;background: #2184e7;padding: 0px 20px;font-size: 14px;height:25px;}
        </style>
    </head>
    <body>
        <div class="header">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td align="center" class="td1"><h5>Example Report</h5></td>
                </tr>
            </table>
        </div>
        <div class="footer">
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
                <tr>
                    <td class="td1"><h6>Lavalite</h6></td>
                    <td class="td2"  align="right"><h6>Date: <NOBR>{{date('d M, Y')}}</NOBR></h6></td>
                </tr>
            </table>
        </div>
        <div class="content">

        <p>
            <br />    
            <table width="100%" border="0" cellspacing="0" cellpadding="10" align="center">
                <tr>
                    <td colspan="4"><h1 style="color:#003399; text-align:center; font-size:38px; margin-top:250px;">Lavalite</h1></td>
                </tr>
                <tr>
                    <td colspan="4"><h3 style="color:#003399; text-align:center; font-size:32px; margin-bottom:400px;">Example Report</h3></td>
                </tr>
               
                
            </table>
        </p>

        <p style="page-break-before:always">
            <br />    
            <table width="80%" border="0" cellspacing="0" cellpadding="10" align="center">
                <tr>
                    <td ><span style="font-size:22px; color:#330099; "> Summary View</span></td>
                </tr>
            </table>

            <table width="80%" border="1" cellspacing="0" cellpadding="10" align="center">
                <tr>
                    <th height="15" style="text-align: center;">Name</th>
                    <th height="15" style="text-align: center;">Count</th>
                </tr>
                @forelse($examples['available'] as $key=>$example)
               <tr>
                    <td style="text-align: center;">{{@$example['name']}}</td>
                    <td style="text-align: center;">{{@$example['count']}}</td>
                </tr>
                @empty
                @endif
            </table>
        </p>

            
        
        <p style="page-break-before:always">
            <br />
            <table width="80%" border="0" cellspacing="0" cellpadding="10" align="center"> 
            <tr>
            <td width="100%">
                <table width="100%" border="0" cellspacing="0" cellpadding="10" >
                    <tr>
                        <td><span style="font-size:22px; color:#330099; ">Examples Graph</span></td>
                    </tr>
                </table>

                <table width="100%" border="0" cellspacing="0" cellpadding="10" align="right" height="{{count($examples['available'])*40}}" align="center">
                    @forelse($examples['available'] as $key=>$example)
                    <tr>
                        <td align="right">{{@$example['labels']}}:{{@$example['count']}} ({{round((@$example['count']/@$examples['labels'])*100,0)}}%)</td>
                        <td style="border-left:solid 1px #000;" colspan="4"><span style="background-color:{{$colors[($key+1)%10]}}; width:{{(@$example['count']/@$examples['labels'])*100}}%; height:10px;display:block;"></span></td>
                    </tr>
                    @empty
                    @endif
                    <tr>
                        <td align="right" width="40%" align="right"></td>
                        <td style="border-top:solid 1px #000;" width="15%"  align="right">{{$examples['labels']/4}}</td>
                        <td style="border-top:solid 1px #000;" width="15%" align="right">{{$examples['labels']/2}}</td>
                        <td style="border-top:solid 1px #000;" width="15%" align="right">{{$examples['labels']/2+$examples['labels']/4}}</td>
                        <td style="border-top:solid 1px #000;" width="15%"  align="right">{{$examples['labels']}}</td>
                    </tr>
                </table>
            </td>    
            </tr>
            </table>
        </p>

        <p>
            @forelse(array_slice($examples['all'], 0, 2) as $key=>$row)
                <span style="page-break-before: always;"></span>
                @if($key == 00)
                    <br />
                    <table width="95%" border="0" cellspacing="0" cellpadding="1" align="center">
                        <tr>
                            <td><span style="font-size:22px; color:#330099;"> Detail View</span></td>
                        </tr>
                    </table>
                @endif
                <table width="95%" border="1" cellspacing="0" cellpadding="1" align="center" style="padding-top:20px;" >
                    <tr>
                        <th>{!! trans('example::example.label.name')!!}</th>
                        <th>{!! trans('example::example.label.email')!!}</th>
                        <th>{!! trans('example::example.label.color')!!}</th>
                        <th>{!! trans('example::example.label.date')!!}</th>
                        <th>{!! trans('example::example.label.datetime')!!}</th>
                        <th>{!! trans('example::example.label.file')!!}</th>
                        <th>{!! trans('example::example.label.files')!!}</th>
                        <th>{!! trans('example::example.label.image')!!}</th>
                        <th>{!! trans('example::example.label.images')!!}</th>
                        <th>{!! trans('example::example.label.month')!!}</th>
                        <th>{!! trans('example::example.label.password')!!}</th>
                        <th>{!! trans('example::example.label.range')!!}</th>
                        <th>{!! trans('example::example.label.search')!!}</th>
                        <th>{!! trans('example::example.label.tel')!!}</th>
                        <th>{!! trans('example::example.label.time')!!}</th>
                        <th>{!! trans('example::example.label.url')!!}</th>
                        <th>{!! trans('example::example.label.week')!!}</th>
                        <th>{!! trans('example::example.label.date_picker')!!}</th>
                        <th>{!! trans('example::example.label.time_picker')!!}</th>
                        <th>{!! trans('example::example.label.date_time_picker')!!}</th>
                        <th>{!! trans('example::example.label.radios')!!}</th>
                        <th>{!! trans('example::example.label.checkboxes')!!}</th>
                        <th>{!! trans('example::example.label.switch')!!}</th>
                        <th>{!! trans('example::example.label.select')!!}</th>
                        <th>{!! trans('example::example.label.model_select')!!}</th>
                        <th>{!! trans('example::example.label.tinyints')!!}</th>
                        <th>{!! trans('example::example.label.smallints')!!}</th>
                        <th>{!! trans('example::example.label.mediumints')!!}</th>
                        <th>{!! trans('example::example.label.ints')!!}</th>
                        <th>{!! trans('example::example.label.bigints')!!}</th>
                        <th>{!! trans('example::example.label.decimals')!!}</th>
                        <th>{!! trans('example::example.label.floats')!!}</th>
                        <th>{!! trans('example::example.label.doubles')!!}</th>
                        <th>{!! trans('example::example.label.reals')!!}</th>
                        <th>{!! trans('example::example.label.bits')!!}</th>
                        <th>{!! trans('example::example.label.booleans')!!}</th>
                        <th>{!! trans('example::example.label.dates')!!}</th>
                        <th>{!! trans('example::example.label.datetimes')!!}</th>
                        <th>{!! trans('example::example.label.timestamps')!!}</th>
                        <th>{!! trans('example::example.label.times')!!}</th>
                        <th>{!! trans('example::example.label.years')!!}</th>
                        <th>{!! trans('example::example.label.chars')!!}</th>
                        <th>{!! trans('example::example.label.varchars')!!}</th>
                        <th>{!! trans('example::example.label.tinytexts')!!}</th>
                        <th>{!! trans('example::example.label.texts')!!}</th>
                        <th>{!! trans('example::example.label.mediumtexts')!!}</th>
                        <th>{!! trans('example::example.label.longtexts')!!}</th>
                        <th>{!! trans('example::example.label.binarys')!!}</th>
                        <th>{!! trans('example::example.label.varbinarys')!!}</th>
                        <th>{!! trans('example::example.label.tinyblobs')!!}</th>
                        <th>{!! trans('example::example.label.mediumblobs')!!}</th>
                        <th>{!! trans('example::example.label.blobs')!!}</th>
                        <th>{!! trans('example::example.label.longblobs')!!}</th>
                        <th>{!! trans('example::example.label.enums')!!}</th>
                        <th>{!! trans('example::example.label.sets')!!}</th>
                        <th>{!! trans('example::example.label.status')!!}</th>
                        <th>{!! trans('example::example.label.created_at')!!}</th>
                        <th>{!! trans('example::example.label.updated_at')!!}</th>
                    </tr>

                    @forelse($row as $key=>$inner_row)
                    
                    <tr>
                        <td><span style="font-size:9px;">{{ @$inner_row['name'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['email'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['color'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['date'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['datetime'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['file'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['files'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['image'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['images'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['month'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['password'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['range'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['search'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['tel'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['time'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['url'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['week'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['date_picker'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['time_picker'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['date_time_picker'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['radios'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['checkboxes'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['switch'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['select'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['model_select'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['tinyints'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['smallints'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['mediumints'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['ints'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['bigints'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['decimals'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['floats'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['doubles'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['reals'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['bits'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['booleans'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['dates'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['datetimes'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['timestamps'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['times'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['years'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['chars'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['varchars'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['tinytexts'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['texts'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['mediumtexts'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['longtexts'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['binarys'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['varbinarys'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['tinyblobs'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['mediumblobs'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['blobs'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['longblobs'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['enums'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['sets'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['status'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['created_at'] }}</span></td>
                        <td><span style="font-size:9px;">{{ @$inner_row['updated_at'] }}</span></td>
                    </tr>                    
                    @empty
                    @endif
                </table>
                
                @empty
                @endif
            
        </p>

    </div>
</body>
</html>
