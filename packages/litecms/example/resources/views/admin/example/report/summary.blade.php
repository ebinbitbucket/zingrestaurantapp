<div class="table-wrapper">
    <table width="100%" border="0" align="center" class="table table-striped display dataTable" cellpadding="0" cellspacing="0">
      <thead>
        <tr>
          <th>Name</th>
          <th>Count</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($examples as $key => $example)
          <tr>
            <td>{{ @$example['name'] }}</td>
            <td>{{ @$example['count'] }}</td>
          </tr>
        @empty
          <tr>
            <td colspan="4">No data</td>
          </tr>
        @endif
      </tbody>
    </table>
</div>