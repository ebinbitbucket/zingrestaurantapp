  <div id="chart_span" style="min-width: 400px; height: 350px; margin: 0 auto"></div>
<script type="text/javascript">
    var chart;
    $(document).ready(function () {
		chart = new Highcharts.Chart({
	        chart: {
	            renderTo: 'chart_span',
	            type: 'column'
	        },
	        title: {
	            text: ''
	        },
	        credits: {
	            enabled: false
	        },
	        xAxis: {
	            categories: [],
	            labels: {
	                rotation: -55,
	                align: 'right',
	                style: {
	                    fontSize: '11px',
	                    fontFamily: 'Verdana, sans-serif'
	                }
	            }
	        },
	        yAxis: {
	            title: {
	                text: 'Value'
	            },
	            plotLines: [{
	                value: 0,
	                width: 2,
	                color: '#808080'
	            }]
	        },
	        tooltip: {
	            formatter: function () {
	                return '' +
	                this.series.name + ': ' + this.y + '';
	            }
	        },
	        credits: {
	            enabled: false
	        },
	        exporting: {
	          enabled: false
	      },
	      series: []
	    });
	});

	function plotGraph(url) {
	    $('.nav-tabs li a[href="#tab-graph"]').tab('show');
	    $.getJSON(url, function (seriesData) {
	        chart.xAxis[0].setCategories(seriesData.categories);
	        while(chart.series.length > 0)
	          chart.series[0].remove(true);
	        
	        $.each(seriesData.series, function (id, key) {
	          chart.addSeries({name: id, data: key}, true);
	        });
	        chart.redraw();
	        chart.hideLoading();
	    });
	}
</script>