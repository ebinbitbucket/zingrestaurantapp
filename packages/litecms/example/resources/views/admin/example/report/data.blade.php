<table id="example-example-report-list" class="table table-striped">
    <thead class="list_head">
         <th>{!! trans('example::example.label.status')!!}</th>
         <th>{!! trans('example::example.label.name')!!}</th>
         <th>{!! trans('example::example.label.email')!!}</th>
         <th>{!! trans('example::example.label.color')!!}</th>
         <th>{!! trans('example::example.label.date')!!}</th>
         <th>{!! trans('example::example.label.datetime')!!}</th>
         <th>{!! trans('example::example.label.file')!!}</th>
         <th>{!! trans('example::example.label.files')!!}</th>
         <th>{!! trans('example::example.label.image')!!}</th>
         <th>{!! trans('example::example.label.images')!!}</th>
         <th>{!! trans('example::example.label.month')!!}</th>
         <th>{!! trans('example::example.label.password')!!}</th>
         <th>{!! trans('example::example.label.range')!!}</th>
         <th>{!! trans('example::example.label.search')!!}</th>
         <th>{!! trans('example::example.label.tel')!!}</th>
         <th>{!! trans('example::example.label.time')!!}</th>
         <th>{!! trans('example::example.label.url')!!}</th>
         <th>{!! trans('example::example.label.week')!!}</th>
         <th>{!! trans('example::example.label.date_picker')!!}</th>
         <th>{!! trans('example::example.label.time_picker')!!}</th>
         <th>{!! trans('example::example.label.date_time_picker')!!}</th>
         <th>{!! trans('example::example.label.radios')!!}</th>
         <th>{!! trans('example::example.label.checkboxes')!!}</th>
         <th>{!! trans('example::example.label.switch')!!}</th>
         <th>{!! trans('example::example.label.select')!!}</th>
         <th>{!! trans('example::example.label.model_select')!!}</th>
         <th>{!! trans('example::example.label.tinyints')!!}</th>
         <th>{!! trans('example::example.label.smallints')!!}</th>
         <th>{!! trans('example::example.label.mediumints')!!}</th>
         <th>{!! trans('example::example.label.ints')!!}</th>
         <th>{!! trans('example::example.label.bigints')!!}</th>
         <th>{!! trans('example::example.label.decimals')!!}</th>
         <th>{!! trans('example::example.label.floats')!!}</th>
         <th>{!! trans('example::example.label.doubles')!!}</th>
         <th>{!! trans('example::example.label.reals')!!}</th>
         <th>{!! trans('example::example.label.bits')!!}</th>
         <th>{!! trans('example::example.label.booleans')!!}</th>
         <th>{!! trans('example::example.label.dates')!!}</th>
         <th>{!! trans('example::example.label.datetimes')!!}</th>
         <th>{!! trans('example::example.label.timestamps')!!}</th>
         <th>{!! trans('example::example.label.times')!!}</th>
         <th>{!! trans('example::example.label.years')!!}</th>
         <th>{!! trans('example::example.label.chars')!!}</th>
         <th>{!! trans('example::example.label.varchars')!!}</th>
         <th>{!! trans('example::example.label.tinytexts')!!}</th>
         <th>{!! trans('example::example.label.texts')!!}</th>
         <th>{!! trans('example::example.label.mediumtexts')!!}</th>
         <th>{!! trans('example::example.label.longtexts')!!}</th>
         <th>{!! trans('example::example.label.binarys')!!}</th>
         <th>{!! trans('example::example.label.varbinarys')!!}</th>
         <th>{!! trans('example::example.label.tinyblobs')!!}</th>
         <th>{!! trans('example::example.label.mediumblobs')!!}</th>
         <th>{!! trans('example::example.label.blobs')!!}</th>
         <th>{!! trans('example::example.label.longblobs')!!}</th>
         <th>{!! trans('example::example.label.enums')!!}</th>
         <th>{!! trans('example::example.label.sets')!!}</th>
         <th>{!! trans('example::example.label.status')!!}</th>
         <th>{!! trans('example::example.label.created_at')!!}</th>
         <th>{!! trans('example::example.label.updated_at')!!}</th>
    </thead>

</table>

<script type="text/javascript">
var oTable;
$(document).ready(function () {
     oTable = $('#example-example-report-list').DataTable( {
            "order": [[2, 'asc']],
            'bFilter': false,
            "ajax": '{!! trans_url('/admin/example/report/all') !!}',
            "columns": [
                {data :'status'},
                {data :'name'},
                {data :'email'},
                {data :'color'},
                {data :'date'},
                {data :'datetime'},
                {data :'file'},
                {data :'files'},
                {data :'image'},
                {data :'images'},
                {data :'month'},
                {data :'password'},
                {data :'range'},
                {data :'search'},
                {data :'tel'},
                {data :'time'},
                {data :'url'},
                {data :'week'},
                {data :'date_picker'},
                {data :'time_picker'},
                {data :'date_time_picker'},
                {data :'radios'},
                {data :'checkboxes'},
                {data :'switch'},
                {data :'select'},
                {data :'model_select'},
                {data :'tinyints'},
                {data :'smallints'},
                {data :'mediumints'},
                {data :'ints'},
                {data :'bigints'},
                {data :'decimals'},
                {data :'floats'},
                {data :'doubles'},
                {data :'reals'},
                {data :'bits'},
                {data :'booleans'},
                {data :'dates'},
                {data :'datetimes'},
                {data :'timestamps'},
                {data :'times'},
                {data :'years'},
                {data :'chars'},
                {data :'varchars'},
                {data :'tinytexts'},
                {data :'texts'},
                {data :'mediumtexts'},
                {data :'longtexts'},
                {data :'binarys'},
                {data :'varbinarys'},
                {data :'tinyblobs'},
                {data :'mediumblobs'},
                {data :'blobs'},
                {data :'longblobs'},
                {data :'enums'},
                {data :'sets'},
                {data :'status'},
                {data :'created_at'},
                {data :'updated_at'},
            ],
            "pageLength": 25
        });
});
</script>