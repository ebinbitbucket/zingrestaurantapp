<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label language files for Example package
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default labels for example module,
    | and it is used by the template/view files in this module
    |
    */

    'name'          => 'Example',
    'names'         => 'Examples',
];
