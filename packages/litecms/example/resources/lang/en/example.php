<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Language files for example in example package
    |--------------------------------------------------------------------------
    |
    | The following language lines are  for  example module in example package
    | and it is used by the template/view files in this module
    |
    */

    /**
     * Singlular and plural name of the module
     */
    'name'          => 'Example',
    'names'         => 'Examples',
    
    /**
     * Singlular and plural name of the module
     */
    'title'         => [
        'main'  => 'Examples',
        'sub'   => 'Examples',
        'list'  => 'List of examples',
        'edit'  => 'Edit example',
        'create'    => 'Create new example'
    ],

    /**
     * Options for select/radio/check.
     */
    'options'       => [
            'select'              => ['option1','option2','option3'],
            'model_select'        => ['ms1','ms2','ms3'],
            'enums'               => ['1','2'],
            'sets'                => ['3','4'],
            'status'              => ['draft','complete','verify','approve','publish','unpublish','archive'],
    ],

    /**
     * Placeholder for inputs
     */
    'placeholder'   => [
        'id'                         => 'Please enter id',
        'name'                       => 'Please enter name',
        'email'                      => 'Please enter email',
        'color'                      => 'Please enter color',
        'date'                       => 'Please select date',
        'datetime'                   => 'Please select datetime',
        'file'                       => 'Please enter file',
        'files'                      => 'Please enter files',
        'image'                      => 'Please enter image',
        'images'                     => 'Please enter images',
        'month'                      => 'Please enter month',
        'password'                   => 'Please enter password',
        'range'                      => 'Please enter range',
        'search'                     => 'Please enter search',
        'tel'                        => 'Please enter tel',
        'time'                       => 'Please enter time',
        'url'                        => 'Please enter url',
        'week'                       => 'Please enter week',
        'date_picker'                => 'Please select date picker',
        'time_picker'                => 'Please select time picker',
        'date_time_picker'           => 'Please select date time picker',
        'radios'                     => 'Please enter radios',
        'checkboxes'                 => 'Please enter checkboxes',
        'switch'                     => 'Please enter switch',
        'select'                     => 'Please select select',
        'model_select'               => 'Please select model select',
        'tinyints'                   => 'Please enter tinyints',
        'smallints'                  => 'Please enter smallints',
        'mediumints'                 => 'Please enter mediumints',
        'ints'                       => 'Please enter ints',
        'bigints'                    => 'Please enter bigints',
        'decimals'                   => 'Please enter decimals',
        'floats'                     => 'Please enter floats',
        'doubles'                    => 'Please enter doubles',
        'reals'                      => 'Please enter reals',
        'bits'                       => 'Please enter bits',
        'booleans'                   => 'Please enter booleans',
        'dates'                      => 'Please select dates',
        'datetimes'                  => 'Please select datetimes',
        'timestamps'                 => 'Please select timestamps',
        'times'                      => 'Please select times',
        'years'                      => 'Please enter years',
        'chars'                      => 'Please enter chars',
        'varchars'                   => 'Please enter varchars',
        'tinytexts'                  => 'Please enter tinytexts',
        'texts'                      => 'Please enter texts',
        'mediumtexts'                => 'Please enter mediumtexts',
        'longtexts'                  => 'Please enter longtexts',
        'binarys'                    => 'Please enter binarys',
        'varbinarys'                 => 'Please enter varbinarys',
        'tinyblobs'                  => 'Please enter tinyblobs',
        'mediumblobs'                => 'Please enter mediumblobs',
        'blobs'                      => 'Please enter blobs',
        'longblobs'                  => 'Please enter longblobs',
        'enums'                      => 'Please select enums',
        'sets'                       => 'Please select sets',
        'slug'                       => 'Please enter slug',
        'status'                     => 'Please select status',
        'user_id'                    => 'Please enter user id',
        'user_type'                  => 'Please enter user type',
        'upload_folder'              => 'Please enter upload folder',
        'deleted_at'                 => 'Please select deleted at',
        'created_at'                 => 'Please select created at',
        'updated_at'                 => 'Please select updated at',
        'PRIMARY'                    => 'Please select PRIMARY',
    ],

    /**
     * Labels for inputs.
     */
    'label'         => [
        'id'                         => 'Id',
        'name'                       => 'Name',
        'email'                      => 'Email',
        'color'                      => 'Color',
        'date'                       => 'Date',
        'datetime'                   => 'Datetime',
        'file'                       => 'File',
        'files'                      => 'Files',
        'image'                      => 'Image',
        'images'                     => 'Images',
        'month'                      => 'Month',
        'password'                   => 'Password',
        'range'                      => 'Range',
        'search'                     => 'Search',
        'tel'                        => 'Tel',
        'time'                       => 'Time',
        'url'                        => 'Url',
        'week'                       => 'Week',
        'date_picker'                => 'Date picker',
        'time_picker'                => 'Time picker',
        'date_time_picker'           => 'Date time picker',
        'radios'                     => 'Radios',
        'checkboxes'                 => 'Checkboxes',
        'switch'                     => 'Switch',
        'select'                     => 'Select',
        'model_select'               => 'Model select',
        'tinyints'                   => 'Tinyints',
        'smallints'                  => 'Smallints',
        'mediumints'                 => 'Mediumints',
        'ints'                       => 'Ints',
        'bigints'                    => 'Bigints',
        'decimals'                   => 'Decimals',
        'floats'                     => 'Floats',
        'doubles'                    => 'Doubles',
        'reals'                      => 'Reals',
        'bits'                       => 'Bits',
        'booleans'                   => 'Booleans',
        'dates'                      => 'Dates',
        'datetimes'                  => 'Datetimes',
        'timestamps'                 => 'Timestamps',
        'times'                      => 'Times',
        'years'                      => 'Years',
        'chars'                      => 'Chars',
        'varchars'                   => 'Varchars',
        'tinytexts'                  => 'Tinytexts',
        'texts'                      => 'Texts',
        'mediumtexts'                => 'Mediumtexts',
        'longtexts'                  => 'Longtexts',
        'binarys'                    => 'Binarys',
        'varbinarys'                 => 'Varbinarys',
        'tinyblobs'                  => 'Tinyblobs',
        'mediumblobs'                => 'Mediumblobs',
        'blobs'                      => 'Blobs',
        'longblobs'                  => 'Longblobs',
        'enums'                      => 'Enums',
        'sets'                       => 'Sets',
        'slug'                       => 'Slug',
        'status'                     => 'Status',
        'user_id'                    => 'User id',
        'user_type'                  => 'User type',
        'upload_folder'              => 'Upload folder',
        'deleted_at'                 => 'Deleted at',
        'created_at'                 => 'Created at',
        'updated_at'                 => 'Updated at',
        'PRIMARY'                    => 'PRIMARY',
    ],

    /**
     * Columns array for show hide checkbox.
     */
    'cloumns'         => [
        'name'                       => ['name' => 'Name', 'data-column' => 1, 'checked'],
        'email'                      => ['name' => 'Email', 'data-column' => 2, 'checked'],
        'color'                      => ['name' => 'Color', 'data-column' => 3, 'checked'],
        'date'                       => ['name' => 'Date', 'data-column' => 4, 'checked'],
        'datetime'                   => ['name' => 'Datetime', 'data-column' => 5, 'checked'],
        'file'                       => ['name' => 'File', 'data-column' => 6, 'checked'],
        'files'                      => ['name' => 'Files', 'data-column' => 7, 'checked'],
        'image'                      => ['name' => 'Image', 'data-column' => 8, 'checked'],
        'images'                     => ['name' => 'Images', 'data-column' => 9, 'checked'],
        'month'                      => ['name' => 'Month', 'data-column' => 10, 'checked'],
        'password'                   => ['name' => 'Password', 'data-column' => 11, 'checked'],
        'range'                      => ['name' => 'Range', 'data-column' => 12, 'checked'],
        'search'                     => ['name' => 'Search', 'data-column' => 13, 'checked'],
        'tel'                        => ['name' => 'Tel', 'data-column' => 14, 'checked'],
        'time'                       => ['name' => 'Time', 'data-column' => 15, 'checked'],
        'url'                        => ['name' => 'Url', 'data-column' => 16, 'checked'],
        'week'                       => ['name' => 'Week', 'data-column' => 17, 'checked'],
        'date_picker'                => ['name' => 'Date picker', 'data-column' => 18, 'checked'],
        'time_picker'                => ['name' => 'Time picker', 'data-column' => 19, 'checked'],
        'date_time_picker'           => ['name' => 'Date time picker', 'data-column' => 20, 'checked'],
        'radios'                     => ['name' => 'Radios', 'data-column' => 21, 'checked'],
        'checkboxes'                 => ['name' => 'Checkboxes', 'data-column' => 22, 'checked'],
        'switch'                     => ['name' => 'Switch', 'data-column' => 23, 'checked'],
        'select'                     => ['name' => 'Select', 'data-column' => 24, 'checked'],
        'model_select'               => ['name' => 'Model select', 'data-column' => 25, 'checked'],
        'tinyints'                   => ['name' => 'Tinyints', 'data-column' => 26, 'checked'],
        'smallints'                  => ['name' => 'Smallints', 'data-column' => 27, 'checked'],
        'mediumints'                 => ['name' => 'Mediumints', 'data-column' => 28, 'checked'],
        'ints'                       => ['name' => 'Ints', 'data-column' => 29, 'checked'],
        'bigints'                    => ['name' => 'Bigints', 'data-column' => 30, 'checked'],
        'decimals'                   => ['name' => 'Decimals', 'data-column' => 31, 'checked'],
        'floats'                     => ['name' => 'Floats', 'data-column' => 32, 'checked'],
        'doubles'                    => ['name' => 'Doubles', 'data-column' => 33, 'checked'],
        'reals'                      => ['name' => 'Reals', 'data-column' => 34, 'checked'],
        'bits'                       => ['name' => 'Bits', 'data-column' => 35, 'checked'],
        'booleans'                   => ['name' => 'Booleans', 'data-column' => 36, 'checked'],
        'dates'                      => ['name' => 'Dates', 'data-column' => 37, 'checked'],
        'datetimes'                  => ['name' => 'Datetimes', 'data-column' => 38, 'checked'],
        'timestamps'                 => ['name' => 'Timestamps', 'data-column' => 39, 'checked'],
        'times'                      => ['name' => 'Times', 'data-column' => 40, 'checked'],
        'years'                      => ['name' => 'Years', 'data-column' => 41, 'checked'],
        'chars'                      => ['name' => 'Chars', 'data-column' => 42, 'checked'],
        'varchars'                   => ['name' => 'Varchars', 'data-column' => 43, 'checked'],
        'texts'                      => ['name' => 'Texts', 'data-column' => 44, 'checked'],
        'blobs'                      => ['name' => 'Blobs', 'data-column' => 45, 'checked'],
        'enums'                      => ['name' => 'Enums', 'data-column' => 46, 'checked'],
        'sets'                       => ['name' => 'Sets', 'data-column' => 47, 'checked'],
        'PRIMARY'                    => ['name' => 'PRIMARY', 'data-column' => 48, 'checked'],
    ],

    /**
     * Tab labels
     */
    'tab'           => [
        'main'  => 'Examples',
    ],

    /**
     * Texts  for the module
     */
    'text'          => [
        'preview' => 'Click on the below list for preview',
    ],
];
