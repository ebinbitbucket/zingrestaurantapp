<?php

return [

    /**
     * Provider.
     */
    'provider'  => 'litecms',

    /*
     * Package.
     */
    'package'   => 'example',

    /*
     * Modules.
     */
    'modules'   => ['example'],

    
    'example'       => [
        'model' => [
            'model'                 => \Litecms\Example\Models\Example::class,
            'table'                 => 'examples',
            'presenter'             => \Litecms\Example\Repositories\Presenter\ExamplePresenter::class,
            'hidden'                => [],
            'visible'               => [],
            'guarded'               => ['*'],
            'slugs'                 => ['slug' => 'name'],
            'dates'                 => ['deleted_at', 'createdat', 'updated_at'],
            'appends'               => [],
            'fillable'              => ['id',  'name',  'email',  'color',  'date',  'datetime',  'file',  'files',  'image',  'images',  'month',  'password',  'range',  'search',  'tel',  'time',  'url',  'week',  'date_picker',  'time_picker',  'date_time_picker',  'radios',  'checkboxes',  'switch',  'select',  'model_select',  'tinyints',  'smallints',  'mediumints',  'ints',  'bigints',  'decimals',  'floats',  'doubles',  'reals',  'bits',  'booleans',  'dates',  'datetimes',  'timestamps',  'times',  'years',  'chars',  'varchars',  'tinytexts',  'texts',  'mediumtexts',  'longtexts',  'binarys',  'varbinarys',  'tinyblobs',  'mediumblobs',  'blobs',  'longblobs',  'enums',  'sets',  'slug',  'status',  'user_id',  'user_type',  'upload_folder',  'deleted_at',  'created_at',  'updated_at',  'PRIMARY'],
            'translatables'         => [],
            'upload_folder'         => 'example/example',
            'uploads'               => [
            /*
                    'images' => [
                        'count' => 10,
                        'type'  => 'image',
                    ],
                    'file' => [
                        'count' => 1,
                        'type'  => 'file',
                    ],
            */
            ],

            'casts'                 => [
            /*
                'images'    => 'array',
                'file'      => 'array',
            */
            ],

            'revision'              => [],
            'perPage'               => '20',
            'search'        => [
                'name'  => 'like',
                'status',
            ]
        ],

        'controller' => [
            'provider'  => 'Litecms',
            'package'   => 'Example',
            'module'    => 'Example',
        ],

        'workflow'      => [
            'points' => [
                'start' => 'draft',
                'end'   => ['delete'],
            ],
            'steps'  => [
                'draft'     => [
                    'label'  => "Example created",
                    'action' => ['setStatus', 'draft'],
                    'next'   => ['complete'],
                ],
                'complete'  => [
                    'label'  => "Example completed",
                    'status' => ['setStatus', 'complete'],
                    'next'   => ['verify'],
                ],
                'verify'    => [
                    'label'  => "Example verified",
                    'action' => ['setStatus', 'verify'],
                    'next'   => ['approve'],
                ],
                'approve'   => [
                    'label'  => "Example approved",
                    'action' => ['setStatus', 'approve'],
                    'next'   => ['publish'],
                ],
                'publish'   => [
                    'label'  => "Example published",
                    'action' => ['setStatus', 'publish'],
                    'next'   => ['unpublish', 'delete', 'target', 'archive'],
                ],
                'unpublish' => [
                    'label'  => "Example unpublished",
                    'action' => ['setStatus', 'unpublish'],
                    'next'   => ['publish', 'target', 'archive', 'delete'],
                ],
                'archive'   => [
                    'label'  => "Example archived",
                    'action' => ['setStatus', 'archive'],
                    'next'   => ['publish', 'delete'],
                ],
                'delete'    => [
                    'Label'  => "Example deleted",
                    'status' => ['delete', 'archive'],
                ],
            ],
        ],

    ],
];
