<?php

namespace Litepie\User\Traits\Auth;

use Illuminate\Foundation\Auth\RegistersUsers as IlluminateRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Role;
use Validator;
use Session;
use App\Client;
trait RegistersUsers
{
    use IlluminateRegistersUsers, ThrottlesLogins;

    /**
     * Show the form for creating a new user.
     *
     * @return Response
     */
    public function showRegistrationForm()
    {
        $this->canRegister();

        return $this->response->setMetaTitle('Register')
            ->view('auth.register')
            ->layout('auth')
            ->output();
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data)
    {
        if (array_key_exists("is_app",$data))
        {
            $restaurant_id = $data['restaurant_id'];
            $user_email = $data['email'];
            if (!empty($data['email'])) {

                $data['email'] = $restaurant_id . "." . $data['email'];
                $data['user_email'] = $user_email;
            }
        }

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:' . $this->getGuardTable(),
            'password' => 'required|min:6|confirmed',
            'mobile' => 'min:10|regex:/^([0-9\s\-\+\(\)]*)$/',
            'phone' => 'regex:/^([0-9\s\-\+\(\)]*)$/',
        ];

        if (config('recaptcha.enable')) {
            $rules['g-recaptcha-response'] = 'required|recaptcha';
        }

        return Validator::make($data, $rules);
        
    
    }
     /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        if (empty($request['password'])) {
            $request['password'] = '123456';
            $request['password_confirmation'] = '123456';
        }

        $validation = $this->validator($request->all());

        if ($validation->fails()) {
            Session::put('validation_errors', $validation->errors()->messages());
            if ($request->filled('is_app')) {
                $restaurant_id = $request['restaurant_id'];
                return redirect()->to(url('app/register/'.$restaurant_id));
            }
            return redirect()->route('register');
        }


        if ($request->filled('is_app')) {
            $is_app = $request['is_app'];
            $restaurant_id = $request['restaurant_id'];
            $user_email = $request['email'];
            if (!empty($request['email'])) {
                $request['email'] = $restaurant_id . "." . $request['email'];
                $request['user_email'] = $user_email;

            }
        }


        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);
        if ($user->is_app == 1) {
            if(isset($request->fcm_token)){
                Client::where('id', user_id())->update(array('fcm_token' => $request->fcm_token));
            }
            // if(Session::has('fcm_token')){
            //     Client::where('id', user_id())->update(array('fcm_token' => Session::get('fcm_token')));
            //     Session::forget('fcm_token');
            // }
            return redirect()->to(guard_url('app/home/' . $user->restaurant_id));

        } else {
            return $this->registered($request, $user)
            ?: redirect()->intended($this->redirectPath());

        }
}




    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */
    public function create(array $data)
    {
        $this->canRegister();

        $data['api_token'] = Str::random(60);

        $model = $this->getAuthModel();
        $user = $model::create($data);
        $this->attachRoles($user);
        return $user;
    }

    public function canRegister()
    {
        $guard = $this->getGuardRoute();

        if (in_array($guard, config('auth.register.allowed'))) {
            return true;
        }

        return abort(403, "You are not allowed to register as [$guard]");
    }

    public function attachRoles($user)
    {
        $guard = $this->getGuardRoute();
        $roles = config('auth.register.roles.' . $guard, null);

        if ($roles == null) {
            return;
        }

        foreach ($roles as $role) {
            $roleId = Role::findBySlug($role)->id;
            $user->attachRole($roleId);
        }

        return true;
    }

}
