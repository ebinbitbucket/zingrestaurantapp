<?php

namespace Litepie\User\Notifications;

use Illuminate\Auth\Notifications\ResetPassword as IlluminateResetPassword;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use Litecms\Restaurantapp\Models\Restaurant;
use Session;

class ResetPassword extends IlluminateResetPassword
{

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable, $this->token);
        }

        $restaurant_id = $notifiable->restaurant_id;
        $client_id = $notifiable->id;

        $restaurant_info = Restaurant::where('id',$restaurant_id)->select('logo','name','address','email')->first();
        $email = $restaurant_info->email;
        $restaurant_name = $restaurant_info->name;

        if($email == ""){
            $email = "support@zingmyorder.com";
        }
        if($restaurant_name == ""){
            $restaurant_name = "Restaurant";
        }

        $url = trans_url('/password/change/'.$this->token.'/'.$restaurant_id.'/'.$client_id);

        // dd($restaurant_info);
        

        return (new MailMessage)->view('restaurantapp::default.restaurant_app.reset_mail', ['user' => $notifiable,'restaurant_info'=> $restaurant_info,'url'=> $url])->subject('Reset Password Notification')->from(@$email, @$restaurant_name);

        // return (new MailMessage)
        //     ->subject('Reset Password Notification')
        //     ->line('You are receiving this email because we received a password reset request for your account.')
        //     ->action('Reset Password',trans_url('/password/change/'.$this->token.'/'.$restaurant_id.'/'.$client_id))
        //     ->line('This password reset link will expire in :count minutes.', ['count' => config('auth.passwords.users.expire')])
        //     ->line('If you did not request a password reset, no further action is required.');

            // return (new MailMessage)
            // ->subject(Lang::getFromJson('Reset Password Notification'))
            // ->line(Lang::getFromJson('You are receiving this email because we received a password reset request for your account.'))
            // ->action(Lang::getFromJson('Reset Password'),$this->passwordUrl())
            // ->line(Lang::getFromJson('This password reset link will expire in :count minutes.', ['count' => config('auth.passwords.users.expire')]))
            // ->line(Lang::getFromJson('If you did not request a password reset, no further action is required.'));


    
    }

    /**
     * Get the verification URL for the given notifiable.
     *
     * @param  mixed  $notifiable
     * @return string
     */
    protected function passwordUrl()
    {
        return url(
            trans_url('/') . route(
                'guard.password.reset',
                [
                    'token' => $this->token,
                    'guard' => current(explode('.', guard())),
                ]
                , false
            )
        );
    }

}
